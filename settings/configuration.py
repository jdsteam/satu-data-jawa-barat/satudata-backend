''' Doc: setting configuration'''
import os
import sentry_sdk
# from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
from datetime import timedelta
from dotenv import load_dotenv, find_dotenv
from functools import lru_cache


# pylint: disable=invalid-envvar-default
# Load environment
load_dotenv(find_dotenv())

# get absolute path static directory in root project
LOG_FOLDER = os.path.abspath(os.path.join(
    os.path.dirname(os.path.dirname(__file__)), 'log'))

JWT_ALGORITHM = os.getenv("JWT_ALGORITHM")
JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")

POSTGRE_HOST = os.getenv("POSTGRE_HOST")
POSTGRE_PORT = os.getenv("POSTGRE_PORT")
POSTGRE_DB = os.getenv("POSTGRE_DB")
POSTGRE_DB_BIGDATA = os.getenv("POSTGRE_DB_BIGDATA")
POSTGRE_USER = os.getenv("POSTGRE_USER")
POSTGRE_PASSWORD = os.getenv("POSTGRE_PASSWORD")

SQLALCHEMY_POOL_SIZE = int(os.getenv("SQLALCHEMY_POOL_SIZE"))
SQLALCHEMY_POOL_TIMEOUT = int(os.getenv("SQLALCHEMY_POOL_TIMEOUT"))
SQLALCHEMY_MAX_OVERFLOW = int(os.getenv("SQLALCHEMY_MAX_OVERFLOW"))
SQLALCHEMY_POOL_RECYCLE = int(os.getenv("SQLALCHEMY_POOL_RECYCLE"))
# SQLALCHEMY_ENGINE_OPTIONS = os.getenv("SQLALCHEMY_ENGINE_OPTIONS")

APP_CODE = os.getenv("APP_CODE")
SOCKET_URL = os.getenv("SOCKET_URL")
LOG_URL = os.getenv("LOG_URL")
REQUEST_URL = os.getenv("REQUEST_URL")

# SATUDATA
SIAP_JABAR_USERNAME = os.getenv("SIAP_JABAR_USERNAME")
SIAP_JABAR_PASSWORD = os.getenv("SIAP_JABAR_PASSWORD")

SSO_JABAR_CLIENT_ID = os.getenv("SSO_JABAR_CLIENT_ID")
SSO_JABAR_CLIENT_SECRET = os.getenv("SSO_JABAR_CLIENT_SECRET")

SSO_DIGITEAM_CLIENT_ID = os.getenv("SSO_DIGITEAM_CLIENT_ID")
SSO_DIGITEAM_CLIENT_SECRET = os.getenv("SSO_DIGITEAM_CLIENT_SECRET")

# EDJ ADMIN
SSO_JABAR_EDJ_CLIENT_ID = os.getenv("SSO_JABAR_EDJ_CLIENT_ID")
SSO_JABAR_EDJ_CLIENT_SECRET = os.getenv("SSO_JABAR_EDJ_CLIENT_SECRET")

SSO_DIGITEAM_EDJ_CLIENT_ID = os.getenv("SSO_DIGITEAM_EDJ_CLIENT_ID")
SSO_DIGITEAM_EDJ_CLIENT_SECRET = os.getenv("SSO_DIGITEAM_EDJ_CLIENT_SECRET")

MESSAGE_DATA_SUCCESS = "Get data successfull"
MESSAGE_DATA_NOTFOUND = "Data not found"
MESSAGE_INTERNAL_ERROR = "Internal Server Error. "
MESSAGE_APP_JSON = "application/json"
MESSAGE_UPLOAD_PATH = "static/upload/temp/"

MEILISEARCH_HOST = os.getenv("MEILISEARCH_HOST")
MEILISEARCH_API_KEY = os.getenv("MEILISEARCH_API_KEY")
MEILISEARCH_INDEX = os.getenv("MEILISEARCH_INDEX")

RECAPTCHA_ADMIN_SITEKEY = os.getenv("RECAPTCHA_ADMIN_SITEKEY")
RECAPTCHA_ADMIN_SECRETKEY = os.getenv("RECAPTCHA_ADMIN_SECRETKEY")
RECAPTCHA_OPENDATA_SITEKEY = os.getenv("RECAPTCHA_OPENDATA_SITEKEY")
RECAPTCHA_OPENDATA_SECRETKEY = os.getenv("RECAPTCHA_OPENDATA_SECRETKEY")
RECAPTCHA_SATUDATA_SITEKEY = os.getenv("RECAPTCHA_SATUDATA_SITEKEY")
RECAPTCHA_SATUDATA_SECRETKEY = os.getenv("RECAPTCHA_SATUDATA_SECRETKEY")
RECAPTCHA_SATUPETA_SITEKEY = os.getenv("RECAPTCHA_SATUPETA_SITEKEY")
RECAPTCHA_SATUPETA_SECRETKEY = os.getenv("RECAPTCHA_SATUPETA_SECRETKEY")

MAIL_BODY_HEAD_CSS = ''
MAIL_BODY_LOGO = ''
MAIL_BODY_INFO = ''
MAIL_BODY_QNA = ''
MAIL_BODY_FOOTER = ''
MAIL_BODY_COPYRIGHT = ''
MAIL_BODY_END = ''

GEOSERVER_USERNAME = os.getenv("GEOSERVER_USERNAME")
GEOSERVER_PASSWORD = os.getenv("GEOSERVER_PASSWORD")

class Configuration(object):
    ''' Doc: class configuration'''
    # Basic
    DEBUG = os.getenv("DEBUG") == "True"
    PORT = int(os.getenv("PORT", 5000))
    THREADED = os.getenv("THREADED") == "False"
    JSON_SORT_KEYS = False

    # JWT
    JWT_ALGORITHM = os.getenv("JWT_ALGORITHM")
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
    JWT_AUTH_URL_RULE = None
    JWT_EXPIRATION_DELTA = timedelta(days=7)  # token expired in 1 weeks

    # POSTGRESQL
    SQLALCHEMY_DATABASE_URI = "postgresql://"+POSTGRE_USER+":" + POSTGRE_PASSWORD+"@"+POSTGRE_HOST+":"+POSTGRE_PORT+"/"+POSTGRE_DB
    SQLALCHEMY_DATABASE_BIGDATA_URI = "postgresql://"+POSTGRE_USER+":" + POSTGRE_PASSWORD+"@"+POSTGRE_HOST+":"+POSTGRE_PORT+"/"+POSTGRE_DB_BIGDATA
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv("POSTGRE_TRACK_MODIFICATIONS")
    SQLALCHEMY_BINDS = {
        'satudata': "postgresql://"+POSTGRE_USER+":" + POSTGRE_PASSWORD+"@"+POSTGRE_HOST+":"+POSTGRE_PORT+"/"+POSTGRE_DB,
        'bigdata': "postgresql://"+POSTGRE_USER+":" + POSTGRE_PASSWORD+"@"+POSTGRE_HOST+":"+POSTGRE_PORT+"/"+POSTGRE_DB_BIGDATA
    }
    SQLALCHEMY_POOL_SIZE = SQLALCHEMY_POOL_SIZE
    SQLALCHEMY_POOL_TIMEOUT = SQLALCHEMY_POOL_TIMEOUT
    SQLALCHEMY_MAX_OVERFLOW = SQLALCHEMY_MAX_OVERFLOW
    SQLALCHEMY_POOL_RECYCLE = SQLALCHEMY_POOL_RECYCLE
    # SQLALCHEMY_ENGINE_OPTIONS = SQLALCHEMY_ENGINE_OPTIONS

    # Sentry
    SENTRY_ENV = os.getenv("SENTRY_ENV")
    SENTRY_DSN = os.getenv("SENTRY_DSN")
    SENTRY_TRACES_SAMPLE_RATE = float(os.getenv("SENTRY_TRACES_SAMPLE_RATE"))
    SENTRY_PROFILES_SAMPLE_RATE = float(os.getenv("SENTRY_PROFILES_SAMPLE_RATE"))

    # Email
    MAIL_SERVER = os.getenv("MAIL_SERVER")
    MAIL_PORT = int(os.getenv("MAIL_PORT"))
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")

    # siap jabar
    SIAP_JABAR_USERNAME = os.getenv("SIAP_JABAR_USERNAME")
    SIAP_JABAR_PASSWORD = os.getenv("SIAP_JABAR_PASSWORD")

    # sso jabar
    SSO_JABAR_CLIENT_ID = os.getenv("SSO_JABAR_CLIENT_ID")
    SSO_JABAR_CLIENT_SECRET = os.getenv("SSO_JABAR_CLIENT_SECRET")

    # GeoServer
    GEOSERVER_USERNAME = os.getenv("GEOSERVER_USERNAME")
    GEOSERVER_PASSWORD = os.getenv("GEOSERVER_PASSWORD")

    # Meilisearch
    MEILISEARCH_HOST = MEILISEARCH_HOST
    MEILISEARCH_API_KEY = MEILISEARCH_API_KEY
    MEILISEARCH_INDEX = MEILISEARCH_INDEX

    # reCaptcha
    RECAPTCHA_ADMIN_SITEKEY = RECAPTCHA_ADMIN_SITEKEY
    RECAPTCHA_ADMIN_SECRETKEY = RECAPTCHA_ADMIN_SECRETKEY
    RECAPTCHA_OPENDATA_SITEKEY = RECAPTCHA_OPENDATA_SITEKEY
    RECAPTCHA_OPENDATA_SECRETKEY = RECAPTCHA_OPENDATA_SECRETKEY
    RECAPTCHA_SATUDATA_SITEKEY = RECAPTCHA_SATUDATA_SITEKEY
    RECAPTCHA_SATUDATA_SECRETKEY = RECAPTCHA_SATUDATA_SECRETKEY
    RECAPTCHA_SATUPETA_SITEKEY = RECAPTCHA_SATUPETA_SITEKEY
    RECAPTCHA_SATUPETA_SECRETKEY = RECAPTCHA_SATUPETA_SECRETKEY

    if os.getenv("SENTRY_ENV") == 'staging':
        path_image_logo = "https://data.ekosistemdatajabar.id/api-backend/static/upload/opendata-black.png"
        path_image_content = "https://data.ekosistemdatajabar.id/api-backend/static/upload/illustration-email.png"
    else:
        path_image_logo = "https://data.jabarprov.go.id/api-backend/static/upload/opendata-black.png"
        path_image_content = "https://data.jabarprov.go.id/api-backend/static/upload/illustration-email.png"

    MAIL_BODY_HEAD_CSS = '''
    <html>
        <meta charset="UTF-8">
        <head>
            <style>
                /* COMPONENT */
                body {
                    background-color: #F6F6F6;
                    font-family: 'Lato';
                    font-size: 14px;
                }

                .container {
                    max-width: 600px;
                    border-spacing: 0px;
                    padding: 0px;
                    width: 100%;
                }

                .white-container {
                    max-width: 600px;
                    border-spacing: 0px;
                    background-color: #ffffff;
                    width: 100%;

                    padding: 32px;
                    padding-bottom: 16px;
                }

                .grey-container {
                    max-width: 600px;
                    border-spacing: 0px;
                    background: #424242;
                    width: 100%;

                    padding: 32px;
                    color: #FAFAFA;

                    font-family: 'Roboto';
                }

                .content {
                    padding-top: 16px;
                    padding-bottom: 16px;

                    line-height: 26px;
                    color: #212121;
                }

                .intro {
                    font-size: 14px;
                    line-height: 23px;
                    font-family: 'Roboto';
                    font-weight: 400;
                }

                .jds-table {
                    border: none;
                    border-collapse: collapse;
                    border-spacing: 0;
                }

                .tableHead {
                    background: #424242;
                    border-top: solid 1px #eaeaea;
                    border-bottom: solid 1px #eaeaea;
                    padding: 10px;
                    text-align: left;

                    font-weight: bold;
                    font-size: 12px;
                    line-height: 19px;
                    color: #FAFAFA;
                }

                .tableCell {
                    border-top: solid 1px #eaeaea;
                    border-bottom: solid 1px #eaeaea;
                    color: #212121;
                    padding: 10px;
                    text-shadow: 1px 1px 1px #fff;
                    vertical-align: top;

                    font-size: 14px;
                    line-height: 23px;
                }

                .ticket-title {
                    color: #757575;
                    font-size: 12px;
                    line-height: 19px;
                    margin-bottom: 0;
                }

                .ticket-no {
                    margin-top: 0px;
                    color: #212121;
                    font-weight: bold;
                    font-size: 16px;
                    line-height: 26px;
                }

                .feedback-card {
                    z-index: 1;
                    position: relative;

                    padding: 24px;

                    background: rgba(230, 246, 236, 0.6);

                    border-top-left-radius: 0px;
                    border-top-right-radius: 0px;
                    border-bottom-left-radius: 10px;
                    border-bottom-right-radius: 10px;

                    font-family: 'Roboto';
                    font-style: normal;
                    font-weight: 500;
                    font-size: 14px;
                    line-height: 23px;
                    color: #212121;
                    margin-top: -10px !important;
                }

                .ii a[href] {
                    color: #fff !important;
                }

                .qna-card {
                    background: #F5F5F5;
                    border-radius: 10px;

                    font-family: 'Roboto';

                    padding-left: 24px;
                    padding-right: 24px;
                    padding-top: 32px;
                    padding-bottom: 24px;
                }

                .qna-title {
                    font-size: 21px;
                    line-height: 34px;
                    color: #212121;
                }

                .qna-list {
                    margin-left: 0;
                    padding-left: 14px;
                    color: #212121;


                    font-weight: bold;
                    font-size: 14px;
                    line-height: 23px;
                }

                .qna-list-title {
                    color: #212121;
                }

                .qna-child-list {

                    list-style-type: disc;
                    margin-left: 0;
                    padding-left: 14px;

                    font-weight: normal;
                    color: #424242;

                    padding-top: 12px;
                    padding-bottom: 16px;
                }

                .footer-text {
                    font-size: 14px;
                    line-height: 23px;

                    margin-bottom: 8px;
                }

                .footer-list {
                    font-size: 14px;
                    line-height: 23px;

                    margin-top: 4px;
                    margin-bottom: 4px;
                }

                .copyright {
                    font-size: 12px;
                    line-height: 19px;

                    color: #757575;
                }

                /* SIMPLE */
                .no-border {
                    border: none;
                }

                .w-100 {
                    width: 100%;
                }

                .logo-padding {
                    padding-top: 10px;
                }

                .py-32 {
                    padding-top: 32px;
                    padding-bottom: 32px;
                }

                .px-0 {
                    padding-top: 0px;
                    padding-bottom: 0px;
                }
            </style>
        </head>
    '''

    MAIL_BODY_LOGO = '''
        <tr>
            <td align="center">
                <table class="container">
                    <tr>
                        <td align="left" class="logo-padding">
                            <img style="width:100px; margin-bottom: -45px;margin-left: 20px;" src="'''+path_image_logo+'''"\>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    '''

    MAIL_BODY_INFO = '''
        <!-- Feedback -->
        <tr>
            <td class="content" align="center">
                <img style="margin-bottom:-10px !important;" src="'''+path_image_content+'''"\>
                <div class="feedback-card">
                    Sampaikan pendapat dan pengalaman Anda dalam menggunakan layanan Request Data di Open
                    Data Jabar untuk layanan yang lebih baik lagi <a style="color:#1f86e9;" href="https://hipaa.jotform.com/212898915546067">disini.</a>
                </div>
            </td>
        </tr>
    '''

    MAIL_BODY_QNA = '''
        <!-- QNA -->
        <tr>
            <td class="content">
                <div class="qna-card">
                    <div class="qna-title">Yang sering ditanyakan</div>
                    <ol class="qna-list">
                        <li>
                            <div class="qna-list-title">Seperti apa tahapan dari request dataset yang saya ajukan?</div>
                            <ul class="qna-child-list">
                                <li>
                                    <b>Persetujuan Walidata: </b>Pengajuan request dataset telah masuk dan menunggu untuk proses verifikasi oleh Walidata.
                                </li>
                                <li><b>Request dataset sedang diproses: </b>Pengajuan request dataset dalam proses pengecekan ketersediaan dan klasifikasi dataset oleh Walidata.</li>
                                <li><b>Request dataset ditolak: </b>Pengajuan request dataset ditolak oleh Walidata karena suatu alasan.</li>
                                <li><b>Request dataset Diakomodir: </b>Pengajuan request dataset diakomodir oleh Walidata dan dataset akan dibagikan kepada pemohon.</li>
                            </ul>
                        </li>
                        <li>
                            <div class="qna-list-title">Kapan saya bisa mendapatkan konfirmasi ketersediaan dataset yang saya request?</div>
                            <ul class="qna-child-list">
                                <li style="text-align:justify;">Walidata akan melakukan verifikasi terhadap pengajuan request dataset anda dalam 1x24 jam. Namun proses penyediaan dataset dapat memakan waktu yang berbeda bergantung dengan kondisi dari data yang diajukan.</li>
                                <li style="text-align:justify;">Proses penyediaan dataset berlangsung maksimal 30 hari. Apabila dataset yang diajukan masih belum tersedia, maka Walidata akan memberikan informasi terkait status request data yang diajukan.</li>
                            </ul>
                        </li>
                        <li>
                            <div class="qna-list-title">Mengapa pengajuan request dataset saya ditolak?
                            </div>
                            <ul class="qna-child-list">
                                <li style="text-align:justify;"> Pengajuan request dataset Anda dapat ditolak apabila data tidak/belum tersedia. data yang diajukan bukan merupakan data dengan klasifikasi terbuka/publik atau data yang diajukan tidak mencakup kewenangan dari Pemerintah Provinsi Jawa Barat.</li>
                            </ul>
                        </li>
                        <li>
                            <div class="qna-list-title">Mengapa status pengajuan request dataset saya masih dalam proses?</div>
                            <ul class="qna-child-list">
                                <li style="text-align:justify;">
                                    Walidata akan berusaha mengakomodir dataset yang diajukan dengan melakukan pengecekan terhadap koleksi dataset hingga berkoordinasi dengan Organisasi Perangkat Daerah yang berkaitan dengan dataset yang dibutuhkan. Proses tersebut memerlukan waktu yang bervariasi berdasarkan kondisi dan ketersediaan data yang diajukan.
                                </li>
                            </ul>
                        </li>
                    </ol>
                </div>
            </td>
        </tr>
    '''

    MAIL_BODY_FOOTER = '''
        <!-- FOOTER -->
        <tr>
            <td align="center">
                <table class="grey-container">
                    <tr>
                        <td>
                            <div class="footer-text">
                                Jika Anda memiliki pertanyaan lebih lanjut, silakan hubungi hotline Open
                                Data Jabar
                                melalui:
                            </div>
                            <div class="footer-list">
                                Whatsapp: <b>0821-2603-0038</b>
                            </div>
                            <div style="color:#fff !important;">
                                Email: <b style="color:#fff !important;">
                                <a style="color:#fff !important;">data@jabarprov.go.id
                                </a></b>
                            </div>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    '''

    MAIL_BODY_COPYRIGHT = '''
        <!-- COPYRIGHT -->
        <tr>
            <td align="center">
                <table class="container">
                    <tr>
                        <td align="center" class="py-32 copyright">
                            © 2021 Open Data Jabar | Jabar Digital Service
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    '''

    MAIL_BODY_END = '''
    </html>
    '''

@lru_cache(maxsize=256)
def get_config():
    return Configuration()


config = get_config()