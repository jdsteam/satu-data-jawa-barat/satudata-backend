''' Doc: http  '''
from flask import Flask
from flask_cors import CORS
from flask_compress import Compress
from flask_socketio import SocketIO
from flask_swagger_ui import get_swaggerui_blueprint
from flask_mail import Mail

from helpers import jwtmanager
from helpers import postgre_alchemy
from helpers import migrate
from helpers import executor
import routes
from .configuration import config

from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
import sentry_sdk


CORS = CORS()
COMPRESS = Compress()
MAIL = Mail()
SOCKETIO = SocketIO(async_mode='threading', ping_timeout=7200)


def create_app():
    ''' Doc: function create app  '''

    app = Flask(__name__.split(',')[0], static_url_path='/static',
                static_folder='../static', template_folder='../template')

    # swagger specific server
    swagger_url = '/api-backend'
    api_url = '/api-backend/static/swagger.json'
    swaggerui_blueprint = get_swaggerui_blueprint(
        swagger_url,
        api_url,
        config={
            'app_name': "Satu Data API",
            'base_url': ''
        }
    )

    # swagger specific local
    # swagger_url = ''
    # api_url = '/static/swagger.json'
    # swaggerui_blueprint = get_swaggerui_blueprint(
    #     swagger_url,
    #     api_url,
    #     config={
    #         'app_name': "Satu Data API",
    #         'base_url': ''
    #     }
    # )

    # register route blueprint
    app.register_blueprint(swaggerui_blueprint, url_prefix="/")
    app.register_blueprint(routes.index.BP)
    app.register_blueprint(routes.error.BP)
    app.register_blueprint(routes.sektoral.BP)
    app.register_blueprint(routes.regional.BP)
    app.register_blueprint(routes.regional_level.BP)
    app.register_blueprint(routes.skpd.BP)
    app.register_blueprint(routes.skpdsub.BP)
    app.register_blueprint(routes.skpdunit.BP)
    app.register_blueprint(routes.dataset_class.BP)
    app.register_blueprint(routes.dataset_type.BP)
    app.register_blueprint(routes.dataset_tag.BP)
    app.register_blueprint(routes.dataset.BP)
    app.register_blueprint(routes.mapset.BP)
    app.register_blueprint(routes.mapset_source.BP)
    app.register_blueprint(routes.mapset_type.BP)
    app.register_blueprint(routes.metadata_type.BP)
    app.register_blueprint(routes.metadata.BP)
    app.register_blueprint(routes.license.BP)
    app.register_blueprint(routes.notification.BP)
    app.register_blueprint(routes.review.BP)
    app.register_blueprint(routes.history.BP)
    app.register_blueprint(routes.search.BP)
    app.register_blueprint(routes.dashboard.BP)
    app.register_blueprint(routes.upload.BP)
    app.register_blueprint(routes.visualization.BP)
    app.register_blueprint(routes.visualization_dataset.BP)
    app.register_blueprint(routes.visualization_access.BP)
    app.register_blueprint(routes.metadata_visualization.BP)
    app.register_blueprint(routes.history_login.BP)
    app.register_blueprint(routes.history_draft.BP)
    app.register_blueprint(routes.history_request.BP)
    app.register_blueprint(routes.request.BP)
    app.register_blueprint(routes.indikator.BP)
    app.register_blueprint(routes.indikator_category.BP)
    app.register_blueprint(routes.indikator_class.BP)
    app.register_blueprint(routes.indikator_history.BP)
    app.register_blueprint(routes.indikator_progress.BP)
    app.register_blueprint(routes.satuan.BP)
    app.register_blueprint(routes.infographic.BP)
    app.register_blueprint(routes.infographic_detail.BP)
    app.register_blueprint(routes.highlight.BP)
    app.register_blueprint(routes.history_download.BP)
    app.register_blueprint(routes.feedback.BP)
    app.register_blueprint(routes.feedback_masalah.BP)
    app.register_blueprint(routes.mapset_metadata.BP)
    app.register_blueprint(routes.mapset_metadata_type.BP)
    app.register_blueprint(routes.request_public.BP)
    app.register_blueprint(routes.history_request_public.BP)
    app.register_blueprint(routes.request_private.BP)
    app.register_blueprint(routes.history_request_private.BP)
    app.register_blueprint(routes.feedback_private.BP)
    app.register_blueprint(routes.feedback_map.BP)
    app.register_blueprint(routes.feedback_map_masalah.BP)
    app.register_blueprint(routes.auth.BP)
    app.register_blueprint(routes.role.BP)
    app.register_blueprint(routes.user.BP)
    app.register_blueprint(routes.app.BP)
    app.register_blueprint(routes.app_service.BP)
    app.register_blueprint(routes.user_permission.BP)
    app.register_blueprint(routes.user_app_role.BP)
    app.register_blueprint(routes.agreement.BP)
    app.register_blueprint(routes.struktur_organisasi.BP)
    app.register_blueprint(routes.bigdata.BP)
    app.register_blueprint(routes.geojson.BP)
    app.register_blueprint(routes.article_topic.BP)
    app.register_blueprint(routes.article.BP)
    app.register_blueprint(routes.refresh.BP)
    app.register_blueprint(routes.jabatan.BP)
    app.register_blueprint(routes.mapset_program.BP)
    app.register_blueprint(routes.dashboard_feedback.BP)
    app.register_blueprint(routes.dashboard_feedback_private.BP)
    app.register_blueprint(routes.dashboard_feedback_map.BP)
    app.register_blueprint(routes.dashboard_history_download.BP)
    app.register_blueprint(routes.dashboard_request_private.BP)
    app.register_blueprint(routes.dashboard_request_public.BP)
    app.register_blueprint(routes.mapset_law.BP)
    app.register_blueprint(routes.mapset_award.BP)
    app.register_blueprint(routes.mapset_story.BP)
    app.register_blueprint(routes.bidang_urusan.BP)
    app.register_blueprint(routes.mapset_thematic.BP)
    app.register_blueprint(routes.nps.BP)
    app.register_blueprint(routes.dashboard_nps.BP)
    app.register_blueprint(routes.csat.BP)
    app.register_blueprint(routes.dashboard_csat.BP)
    app.register_blueprint(routes.mapset_area_coverage.BP)
    app.register_blueprint(routes.regional_integration.BP)
    app.register_blueprint(routes.bidang_urusan_skpd.BP)
    app.register_blueprint(routes.bidang_urusan_indikator.BP)
    app.register_blueprint(routes.bidang_urusan_indikator_skpd.BP)
    app.register_blueprint(routes.history_ewalidata_assignment.BP)
    app.register_blueprint(routes.ewalidata_data.BP)
    app.register_blueprint(routes.indikator_datadasar.BP)
    app.register_blueprint(routes.mapset_webgis.BP)
    app.register_blueprint(routes.mapset_thematic_story_search.BP)
    app.register_blueprint(routes.changelog.BP)

    # load configuration
    app.config.from_object(config)
    app.config['SQLALCHEMY_ECHO'] = False  
    app.config['EXECUTOR_TYPE'] = 'thread'
    app.config['EXECUTOR_MAX_WORKERS'] = 10
    # init app
    CORS.init_app(app, resources={r"/*": {"origins": "*"}})
    COMPRESS.init_app(app)
    MAIL.init_app(app)
    SOCKETIO.init_app(app)
    jwtmanager.init_app(app)
    postgre_alchemy.init_app(app)
    migrate.init_app(app, postgre_alchemy)
    executor.init_app(app)
    

    sentry_sdk.init(
            dsn=config.SENTRY_DSN,
            integrations=[FlaskIntegration(), SqlalchemyIntegration()],
            send_default_pii=True,
            traces_sample_rate=config.SENTRY_TRACES_SAMPLE_RATE,
            profiles_sample_rate=config.SENTRY_PROFILES_SAMPLE_RATE,
        )

    
   

    return app
