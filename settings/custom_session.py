from sqlalchemy import create_engine as create_sync_engine
from typing import Any, Dict, Generator
from .configuration import config
from sqlalchemy.orm import scoped_session, sessionmaker


ENGINE_ARGS: Dict[str, Any] = {
    "echo": True if config.DEBUG else False,
    "pool_size": config.SQLALCHEMY_POOL_SIZE,
    "pool_timeout": config.SQLALCHEMY_POOL_TIMEOUT,
    "pool_recycle": config.SQLALCHEMY_POOL_RECYCLE,
    "max_overflow": config.SQLALCHEMY_MAX_OVERFLOW,
}


def create_db_engine(url: str, **extra_args):
    """
    Factory function untuk membuat database engine dengan konfigurasi yang optimal
    """
    engine_args = ENGINE_ARGS.copy()
    engine_args.update(extra_args)


    return create_sync_engine(url=url, **engine_args)

engine = create_db_engine(url=config.SQLALCHEMY_DATABASE_URI)
session = scoped_session(sessionmaker(bind=engine, autocommit=False, autoflush=False))
bigdata_engine = create_db_engine(url=config.SQLALCHEMY_DATABASE_BIGDATA_URI)
bigdata_session = scoped_session(sessionmaker(bind=bigdata_engine, autocommit=False, autoflush=False))
