''' Doc: controller sektoral  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import SektoralModel
from models import DatasetModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import distinct
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class SektoralController(object):
    ''' Doc: class sektoral  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip, admin):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            attributes = []
            result = db.session.query(SektoralModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),)
            for attr, value in where.items():
                result = result.filter(getattr(SektoralModel, attr) == value)
                attributes.append(attr)
            # check public or private
            jwt = HELPER.read_jwt()
            if jwt:
                if admin == False:
                    if 'is_satupeta' in attributes: 
                        result = result.filter(SektoralModel.is_satupeta == True)
                    else:
                        result = result.filter(SektoralModel.is_satudata == True)
            else:
                if 'is_satupeta' in attributes: 
                    result = result.filter(SektoralModel.is_satupeta == True)
                else:
                    result = result.filter(SektoralModel.is_opendata == True)
            result = result.filter(or_(cast(getattr(SektoralModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in SektoralModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            sektoral = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                sektoral.append(temp)

            # check if empty
            sektoral = list(sektoral)
            if sektoral:
                return True, sektoral
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_distinct_skpd(self, kode_skpd, where: dict, sort):
        ''' Doc: function get distinct skpd  '''
        try:
            # query postgresql
            result = db.session.query(distinct(DatasetModel.sektoral_id).label('id'), SektoralModel.name.label('name'))
            result = result.join(SektoralModel)
            result = result.filter(getattr(DatasetModel, "kode_skpd") == kode_skpd)
            result = result.filter(getattr(DatasetModel, "is_deleted") == False)
            for attr, value in where.items():
                result = result.filter(getattr(DatasetModel, attr) == value)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.all()

            sektoral = []
            for res in result:
                # get relation to dataset_tag
                temp = db.session.query(SektoralModel)
                temp = temp.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("muid"),
                    defer("mdate"),)
                temp = temp.get(res[0])

                if temp:
                    temp = temp.__dict__
                    temp.pop('_sa_instance_state', None)
                else:
                    temp = {}

                sektoral.append(temp)

            # check if empty
            sektoral = list(sektoral)
            if sektoral:
                return True, sektoral
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search, admin):
        ''' Doc: function get cout  '''
        try:
            # query postgresql
            result = db.session.query(SektoralModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(SektoralModel, attr) == value)
            # check public or private
            jwt = HELPER.read_jwt()
            if jwt:
                if admin == False:
                    result = result.filter(SektoralModel.is_satudata == True)
            else:
                result = result.filter(SektoralModel.is_opendata == True)
            result = result.filter(or_(cast(getattr(SektoralModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in SektoralModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(SektoralModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            sektoral = result

            # check if empty
            if sektoral:
                return True, sektoral
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = SektoralModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, sektoral = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, sektoral
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function uupdate  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(SektoralModel)
                for attr, value in where.items():
                    result = result.filter(getattr(SektoralModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, sektoral = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, sektoral
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(SektoralModel)
                for attr, value in where.items():
                    result = result.filter(getattr(SektoralModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, sektoral = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_trending(self, sort):
        ''' Doc: function get trending  '''
        try:
            # query postgresql
            sql = text(
                'SELECT skt.*, (SELECT count(dataset.sektoral_id) FROM history_download ' +
                'INNER JOIN dataset ON history_download.category_id = dataset.id ' +
                'INNER JOIN sektoral ON dataset.sektoral_id = sektoral.id '+
                'where sektoral.id = skt.id and history_download.category = \'dataset\') ' +
                'from sektoral as skt ORDER BY '+sort[0]+' '+sort[1]+' limit 5'
            )
            result = db.engine.execute(sql)

            # change into dict
            output = []

            for res in [dict(row) for row in result]:
                row_append = res
                sektoral_id = res['id']
                sql = text(
                    'select to_char(datetime, \'YYYY-MM-DD\') as date , count(datetime) as total '+
                    'from history_download ' +
                    'inner join dataset on history_download.category_id = dataset.id ' +
                    'inner JOIN sektoral ON dataset.sektoral_id = sektoral.id ' +
                    'where dataset.sektoral_id = '+str(sektoral_id)+' and history_download.category = \'dataset\' '+
                    'group by 1  ORDER BY date desc limit 30'
                )
                res_tgl = db.engine.execute(sql)
                row_append.update({"datecount":[row['total'] for row in res_tgl]})
                row_append['datecount'] = row_append['datecount'][::-1]
                str_check = ",".join(str(x) for x in row_append['datecount'])
                if not str_check:
                    counts = 0
                else:
                    counts = str_check
                row_append.update({"datecount":counts})
                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
