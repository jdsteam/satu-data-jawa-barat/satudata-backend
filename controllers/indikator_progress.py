''' Doc: controller indikator progress  '''
from helpers import Helper
from exceptions import ErrorMessage
from models import IndikatorProgressModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_
import sentry_sdk

HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=singleton-comparison, unused-variable, unused-argument
class IndikatorProgressController(object):
    ''' Doc: class indikator progress  '''

    def __init__(self, **kwargs):
        ''' Doc: function init '''

    def query(self, where: dict, search, types=None):
        ''' Doc: function query '''
        try:
            # query postgresql
            result = db.session.query(IndikatorProgressModel)

            if types:
                result = result.filter(IndikatorProgressModel.capaian != None)

            if 'index' in where:
                condition = where['index'].lower()
                if condition == 'awal':
                    result = result.filter(IndikatorProgressModel.index < 99)
                else:
                    result = result.filter(IndikatorProgressModel.index > 98)
                if 'indikator_id' in where:
                    result = result.filter(IndikatorProgressModel.indikator_id == where['indikator_id'])
            else:
                for attr, value in where.items():
                    is_array = isinstance(value, list)
                    if is_array:
                        result = result.filter(or_(getattr(IndikatorProgressModel, attr) == val for val in value))
                    else:
                        result = result.filter(getattr(IndikatorProgressModel, attr) == value)

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip, types):
        ''' Doc: function get all '''
        try:
            # query postgresql
            result = self.query(where, search, types)
            result = result.order_by(text("indikator_progress."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            indikator_progress = []
            for res in result:
                temp = res.__dict__
                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATETIME)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATETIME)
                temp.pop('_sa_instance_state', None)

                indikator_progress.append(temp)

            # check if empty
            if indikator_progress:
                return True, indikator_progress
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search, types):
        ''' Doc: function get count '''
        try:
            # query postgresql
            result = self.query(where, search, types)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id '''
        try:
            # execute database
            result = db.session.query(IndikatorProgressModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result['cdate'] = result['cdate'].strftime(FORMAT_DATETIME)
                result['mdate'] = result['mdate'].strftime(FORMAT_DATETIME)
                result.pop('_sa_instance_state', None)

            else:
                result = {}
            # check if empty
            if result:
                return True, result, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all_by_id(self, where, sort=None, limit=None, types=None):
        ''' Doc: function get all by id '''
        try:
            # query postgresql
            result = self.query(where, '', types)
            if sort:
                result = result.order_by(text("indikator_progress."+sort[0]+" "+sort[1]))
            if limit:
                result = result.offset(0).limit(limit)
            result = result.all()

            # change into dict
            indikator_history = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                indikator_history.append(temp)
            # check if empty

            return indikator_history

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, json, condition):
        ''' Doc: function post param '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cuid"] = jwt['id']
            json_send["cdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']
            json_send["mdate"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create '''
        try:
            # prepare data model
            param = self.post_param(json, 'add')
            result = IndikatorProgressModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple '''
        try:
            # generate json data
            output = []
            jwt = HELPER.read_jwt()
            for res in json:
                res["cuid"] = jwt['id']
                res["cdate"] = HELPER.local_date_server()
                res["muid"] = jwt['id']
                res["mdate"] = HELPER.local_date_server()
                output.append(res)

            db.session.execute(
                IndikatorProgressModel.__table__.insert().values(output)
            )
            db.session.commit()
            history_class = self.get_all_by_id({'indikator_id':output[0]['indikator_id']})
            # check if exist

            if history_class:
                return True, history_class
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update '''
        try:
            # generate json data
            param = self.post_param(json, 'edit')

            try:
                # prepare data model
                result = db.session.query(IndikatorProgressModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorProgressModel, attr) == value)

                # execute database
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()
                res, request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete '''
        try:

            try:
                # prepare data model
                result = db.session.query(IndikatorProgressModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorProgressModel, attr) == value)
                result = result.delete()

                # execute database
                db.session.commit()

                return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
