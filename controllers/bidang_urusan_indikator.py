''' Doc: controller bidang_urusan_indikator  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import BidangUrusanIndikatorModel, BidangUrusanIndikatorViewModel
from models import EwalidataDataModel, HistoryEwalidataAssignmentModel
from models import UserModel, SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
import helpers.postgre_psycopg as db2
import requests
import os
import json
CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class BidangUrusanIndikatorController(object):
    ''' Doc: class bidang_urusan_indikator  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip, kode_skpd):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(BidangUrusanIndikatorViewModel)
            if kode_skpd:
                result = result.filter(getattr(BidangUrusanIndikatorViewModel, 'kode_skpd') == kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(BidangUrusanIndikatorViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(BidangUrusanIndikatorViewModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanIndikatorViewModel, col.name), sqlalchemy.String).ilike('%' + search + '%') for col in BidangUrusanIndikatorViewModel.__table__.columns))
            result = result.order_by(text(sort[0] + " " + sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            datas = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                if temp['cdate']:
                    temp.update({"cdate":temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                if temp['mdate']:
                    temp.update({"mdate":temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")})

                temp['assignment_user_name'] = ''
                temp['assignment_skpd_nama_skpd'] = ''
                temp['history'] = []
                result_history = db.session.query(
                    HistoryEwalidataAssignmentModel.id, HistoryEwalidataAssignmentModel.bidang_urusan_indikator_skpd_id,
                    HistoryEwalidataAssignmentModel.datetime, HistoryEwalidataAssignmentModel.status_assignment,
                    HistoryEwalidataAssignmentModel.cuid, HistoryEwalidataAssignmentModel.cdate,
                    HistoryEwalidataAssignmentModel.notes, UserModel.name, SkpdModel.nama_skpd
                )
                result_history = result_history.join(UserModel, UserModel.id == HistoryEwalidataAssignmentModel.cuid)
                result_history = result_history.join(SkpdModel, SkpdModel.kode_skpd == UserModel.kode_skpd)
                result_history = result_history.filter(getattr(HistoryEwalidataAssignmentModel, 'bidang_urusan_indikator_skpd_id') == temp['id_assignment'])
                result_history = result_history.order_by(text("history_ewalidata_assignment.id asc"))
                result_history = result_history.all()

                for res_his in result_history:
                    temp_his = res_his._asdict()
                    temp_his.pop('_sa_instance_state', None)
                    if temp_his['datetime']:
                        temp_his.update({"datetime":temp_his['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                    if temp_his['cdate']:
                        temp_his.update({"cdate":temp_his['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                    temp['history'].append(temp_his)
                    temp['assignment_user_name'] = temp_his['name']
                    temp['assignment_skpd_nama_skpd'] = temp_his['nama_skpd']

                datas.append(temp)

            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count(self, where: dict, search, kode_skpd):
        ''' Doc: function count  '''
        try:
            # query postgresql
            result = db.session.query(BidangUrusanIndikatorViewModel.id)
            if kode_skpd:
                result = result.filter(getattr(BidangUrusanIndikatorViewModel, 'kode_skpd') == kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(BidangUrusanIndikatorViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(BidangUrusanIndikatorViewModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanIndikatorViewModel, col.name), sqlalchemy.String).ilike('%' + search + '%') for col in BidangUrusanIndikatorViewModel.__table__.columns))
            result = result.count()

            # change into dict
            datas = {}
            datas['count'] = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_progress(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            jwt = HELPER.read_jwt()
            # query postgresql
            result = db.session.query(BidangUrusanIndikatorModel)
            for attr, value in where.items():
                result = result.filter(getattr(BidangUrusanIndikatorModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanIndikatorModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in BidangUrusanIndikatorModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()
            # change into dict
            bidang_urusan_indikator = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp['cdate'] = temp['cdate'].strftime(DATETIME_FORMAT)
                success, edata = db2.query("SELECT kode_indikator,kode_pemda,tahun, data, status_verifikasi_walidata FROM ewalidata_data WHERE kode_indikator = '"+res.kode_indikator+"' and status = 'AKTIF' ")
                ewalidata_data = []
                if success:
                    for ed in edata:
                        o = {}
                        o['kode_indikator'] = ed[0]
                        o['kode_pemda'] = ed[1]
                        o['tahun'] = ed[2]
                        o['data'] = ed[3]
                        o['status_verifikasi_walidata'] = ed[4]
                        ewalidata_data.append(o)
                    temp['ewalidata_data'] = ewalidata_data
                else:
                    temp['ewalidata_data'] = []
                # temp['ewalidata_data'] = ewalidata_data
                bidang_urusan_indikator.append(temp)
            # check if empty
            bidang_urusan_indikator = list(bidang_urusan_indikator)
            if bidang_urusan_indikator:
                return True, bidang_urusan_indikator
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_progress_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            jwt = HELPER.read_jwt()
            # query postgresql
            result = db.session.query(BidangUrusanIndikatorModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(BidangUrusanIndikatorModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanIndikatorModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in BidangUrusanIndikatorModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_monitor(self, where: dict, search, sort, limit, skip, kode_skpd):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            sql = f'''
                select
                bui.id,
                bui.kode_bidang_urusan,
                bu.nama_bidang_urusan,
                bui.kode_indikator,
                bui.nama_indikator,
                bui.satuan,
                bui.definisi_operasional,
                bui.uraian_indikator,
                bui.status,
                bui.cuid,
                bui.cdate,
                bui.mdate,
                bui.muid,
                buis.kode_skpd,
                skpd.nama_skpd,
                buis.id as id_assignment,
                buis.status_assignment
                from bidang_urusan_indikator as bui
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                left join bidang_urusan_indikator_skpd as buis on buis.kode_indikator = bui.kode_indikator
                left join skpd on skpd.kode_skpd = buis.kode_skpd
            '''

            sql += f'''WHERE 1=1 '''

            if kode_skpd:
                sql += f'''AND buis.kode_skpd = '{kode_skpd}' '''

            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    for idx, val in enumerate(value):
                        if idx == 0:
                            sql += f'''AND '''
                        else:
                            sql += f'''OR '''

                        if attr == 'kode_skpd':
                            sql += f'''buis.kode_skpd = '{val}' '''
                        elif attr == 'nama_skpd':
                            sql += f'''skpd.nama_skpd = '{val}' '''
                        elif attr == 'nama_bidang_urusan':
                            sql += f'''bu.nama_bidang_urusan = '{val}' '''
                        elif attr == 'status_assignment':
                            sql += f'''buis.status_assignment = '{val}' '''
                        else:
                            sql += f'''bui.{attr} = '{val}' '''
                else:
                    if attr == 'kode_skpd':
                        sql += f'''AND buis.kode_skpd = '{value}' '''
                    if attr == 'nama_skpd':
                        sql += f'''AND skpd.nama_skpd = '{value}' '''
                    if attr == 'nama_bidang_urusan':
                        sql += f'''AND bu.nama_bidang_urusan = '{value}' '''
                    elif attr == 'status_assignment':
                        sql += f'''AND buis.status_assignment = '{value}' '''
                    else:
                        sql += f'''AND bui.{attr} = '{value}' '''

            if search:
                sql += f'''AND nama_bidang_urusan ILIKE '%{search}%' OR nama_indikator ILIKE '%{search}%' OR nama_skpd ILIKE '%{search}%' '''

            if sort:
                sql += f'''ORDER BY {str(sort[0])} {str(sort[1])} '''

            sql += f'''OFFSET {skip} LIMIT {limit} '''

            # execute the query with bind parameters
            success, result = db2.query_dict_array(sql, None)

            if success:
                # change into dict
                bidang_urusan = []
                for res in result:
                    temp = res
                    if res['cdate']:
                        temp['cdate'] = res['cdate'].strftime(DATETIME_FORMAT)
                    if res['mdate']:
                        temp['mdate'] = res['mdate'].strftime(DATETIME_FORMAT)

                    temp['ewalidata_data'] = []
                    sql2 = f'''select * from ewalidata_data where kode_indikator = '{temp['kode_indikator']}' order by tahun asc'''
                    success2, result2 = db2.query_dict_array(sql2, None)
                    if success2:
                        for res2 in result2:
                            temp['ewalidata_data'].append(res2)

                    bidang_urusan.append(temp)

                # check if empty
                bidang_urusan = list(bidang_urusan)

                if bidang_urusan:
                    return True, bidang_urusan
                else:
                    return False, []
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_monitor_count(self, where: dict, search, kode_skpd):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            sql = f'''
                select count(bui.id)
                from bidang_urusan_indikator as bui
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                left join bidang_urusan_indikator_skpd as buis on buis.kode_indikator = bui.kode_indikator
                left join skpd on skpd.kode_skpd = buis.kode_skpd
            '''

            sql += f'''WHERE 1=1 '''

            if kode_skpd:
                sql += f'''AND buis.kode_skpd = '{kode_skpd}' '''

            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    for idx, val in enumerate(value):
                        if idx == 0:
                            sql += f'''AND '''
                        else:
                            sql += f'''OR '''

                        if attr == 'kode_skpd':
                            sql += f'''buis.kode_skpd = '{val}' '''
                        elif attr == 'nama_skpd':
                            sql += f'''skpd.nama_skpd = '{val}' '''
                        elif attr == 'nama_bidang_urusan':
                                sql += f'''bu.nama_bidang_urusan = '{val}' '''
                        elif attr == 'status_assignment':
                            sql += f'''buis.status_assignment = '{val}' '''
                        else:
                            sql += f'''bui.{attr} = '{val}' '''
                else:
                    if attr == 'kode_skpd':
                        sql += f'''AND buis.kode_skpd = '{value}' '''
                    if attr == 'nama_skpd':
                        sql += f'''AND skpd.nama_skpd = '{value}' '''
                    if attr == 'nama_bidang_urusan':
                        sql += f'''AND bu.nama_bidang_urusan = '{value}' '''
                    elif attr == 'status_assignment':
                        sql += f'''AND buis.status_assignment = '{value}' '''
                    else:
                        sql += f'''AND bui.{attr} = '{value}' '''

            if search:
                sql += f'''AND nama_bidang_urusan ILIKE '%{search}%' OR nama_indikator ILIKE '%{search}%' OR nama_skpd ILIKE '%{search}%' '''

            print(sql)

            success, result = db2.query_dict_single(sql)

            # change into dict
            if success:
                data = {}
                data['count'] = result['count']
                return True, data
            else:
                data = {}
                data['count'] = 0
                return False, data

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(BidangUrusanIndikatorModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['cdate']:
                    result['cdate'] = result['cdate'].strftime(DATETIME_FORMAT)
                if result['mdate']:
                    result['mdate'] = result['mdate'].strftime(DATETIME_FORMAT)
            else:
                result = {}

            # change into dict
            bidang_urusan = result

            # check if empty
            if bidang_urusan:
                return True, bidang_urusan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = BidangUrusanIndikatorModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, bidang_urusan = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, bidang_urusan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(BidangUrusanIndikatorModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanIndikatorModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, bidang_urusan = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, bidang_urusan
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(BidangUrusanIndikatorModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanIndikatorModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, bidang_urusan = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def submit(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            data = []
            json_send['kodeindikator'] = json_send['kode_indikator']
            json_send['kodepemda'] = json_send['kode_pemda']
            json_send.pop('kode_indikator', None)
            json_send.pop('kode_pemda', None)
            if json_send['data'] == "":
                json_send['data'] = "0"
            data.append(json_send)
            pushedToEwalidata = self.send_data_to_sipd(data)
            if pushedToEwalidata:
                # prepare data model
                result = db.session.query(EwalidataDataModel).filter(EwalidataDataModel.kode_indikator == json_send['kodeindikator']).filter(EwalidataDataModel.tahun == json_send['tahun']).first()
                if result:
                    result.data = json_send['data']
                    result.mdate = HELPER.local_date_server()
                    result.muid = jwt['id']
                else:
                    json_send['kode_indikator'] = json_send['kodeindikator']
                    json_send['kode_pemda'] = json_send['kodepemda']
                    json_send.pop('kodeindikator', None)
                    json_send.pop('kodepemda', None)
                    json_send["cuid"] = jwt['id']
                    json_send["cdate"] = HELPER.local_date_server()
                    json_send["status"] = "AKTIF"
                    result = EwalidataDataModel(**json_send)
                    db.session.add(result)
                db.session.commit()

                return True , {},"Push data successfully."
            else:
                return False, {}, "Failed to push data to ewalidata."

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {}, err)


    def send_data_to_sipd(self,datas):
        try:
            url = os.getenv('EWALIDATA_URL','https://jabar.sipd.go.id/ewalidata/serv') + '/push_dssd'
            # url = URL_API + 'push_dssd_final'
            session = requests.Session()
            payload = json.dumps({
                "data": datas
            })
            req = session.post(
                url,
                headers={
                    "Content-Type": "application/json",
                    "Authorization": "Bearer 24c5d9aa25d516c013896ca247bca6f7"
                },
                data=payload
            )
            # response = req.json()
            # check if empty
            if req.status_code == 200:
                return True
            else:
                return False

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

