''' Doc: controller user app role  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import UserPermissionModel
from models import AppModel
from models import AppServiceModel
from models import UserModel
from models import RoleModel
from models import DatasetModel
from models import SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable, singleton-comparison, broad-except
class UserPermissionController(object):
    ''' Doc: controller user permission  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(UserPermissionModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"))
            for attr, value in where.items():
                result = result.filter(getattr(UserPermissionModel, attr) == value)
            result = result.filter(or_(cast(getattr(UserPermissionModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in UserPermissionModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            user_permission = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app = app.get(temp['app_id'])

                if app:
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                temp['app'] = app

                # get relation to app_service
                app_service = db.session.query(AppServiceModel)
                app_service = app_service.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app_service = app_service.get(temp['app_service_id'])

                if app_service:
                    app_service = app_service.__dict__
                    app_service.pop('_sa_instance_state', None)
                else:
                    app_service = {}

                temp['app_service'] = app_service

                # get relation to user
                user = db.session.query(UserModel)
                user = user.options(
                    defer("password"),
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                user = user.get(temp['user_id'])

                if user:
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                else:
                    user = {}

                temp['user'] = user

                user_permission.append(temp)

            # check if empty
            user_permission = list(user_permission)
            if user_permission:
                return True, user_permission
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(UserPermissionModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(UserPermissionModel, attr) == value)
            result = result.filter(or_(cast(getattr(UserPermissionModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in UserPermissionModel.__table__.columns))
            result = result.count()

            # change into dict
            user_permission = {}
            user_permission['count'] = result

            # check if empty
            if user_permission:
                return True, user_permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(UserPermissionModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"))
            result = result.get(_id)
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app = app.get(result['app_id'])

                if app:
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                result['app'] = app

                # get relation to app_service
                app_service = db.session.query(AppServiceModel)
                app_service = app_service.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app_service = app_service.get(result['app_service_id'])

                if app_service:
                    app_service = app_service.__dict__
                    app_service.pop('_sa_instance_state', None)
                else:
                    app_service = {}

                result['app_service'] = app_service

                try:
                    # get relation to user
                    user = db.session.query(UserModel)
                    user = user.options(
                        defer("password"),
                        defer("cuid"),
                        defer("cdate"),
                        defer("is_deleted"),
                        defer("is_active"))
                    user = user.get(result['user_id'])

                    if user:
                        user = user.__dict__
                        if '_sa_instance_state' in user:
                            user.pop('_sa_instance_state', None)
                    else:
                        user = {}

                    result['user'] = user
                except Exception as err:

                    result['user'] = {}


            else:
                result = {}

            # change into dict
            user_permission = result

            # check if empty
            if user_permission:
                return True, user_permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = UserPermissionModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, user_permission = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, user_permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json
                    # json_send["cdate"] = helper.local_date_server()

                    # prepare data model
                    result = UserPermissionModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, user_permission = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        # return True, dataset_tag
                        results.append(user_permission)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = helper.local_date_server()

            try:
                # prepare data model
                result = db.session.query(UserPermissionModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserPermissionModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, user_permission = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, user_permission
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(UserPermissionModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserPermissionModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                # res, user_permission = self.get_by_id(where["id"])
                res = True

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_skpd(self, dataset_id):
        ''' Doc: function get skpd  '''
        try:
            data_skpd = []

            # query postgresql
            result_dataset = db.session.query(DatasetModel)
            result_dataset = result_dataset.get(dataset_id)

            if result_dataset:
                result_dataset = result_dataset.__dict__

                # query postgresql
                result_user_permission = db.session.query(UserPermissionModel.user_id.distinct())
                result_user_permission = result_user_permission.filter(getattr(UserPermissionModel, 'app_id') == result_dataset['app_id'])
                result_user_permission = result_user_permission.filter(getattr(UserPermissionModel, 'app_service_id') == result_dataset['app_service_id'])
                result_user_permission = result_user_permission.all()

                if result_user_permission:
                    for rup in result_user_permission:

                        # query postgresql
                        result_user = db.session.query(UserModel)
                        result_user = result_user.get(rup)

                        if result_user:
                            result_user = result_user.__dict__

                            # query postgresql
                            result_skpd = db.session.query(SkpdModel)
                            result_skpd = result_skpd.filter(getattr(SkpdModel, 'kode_skpd') == result_user['kode_skpd'])
                            result_skpd = result_skpd.one()

                            if result_skpd:
                                result_skpd = result_skpd.__dict__
                                result_skpd.pop('_sa_instance_state', None)
                                data_skpd.append(result_skpd)

            # remove duplicate
            new_data_skpd = []
            for ds_ in data_skpd:
                if ds_ not in new_data_skpd:
                    new_data_skpd.append(ds_)

            if new_data_skpd:
                return True, new_data_skpd
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_private(self, json: dict):
        ''' Doc: function create private  '''
        try:
            # generate json data
            if('dataset_id' in json and 'kode_skpd' in json):

                # user_permission = json
                user_permission = []
                res = False

                result_dataset = db.session.query(DatasetModel)
                result_dataset = result_dataset.get(json['dataset_id'])
                if result_dataset:
                    result_dataset = result_dataset.__dict__
                    result_dataset.pop('_sa_instance_state', None)

                    self.insert_walidata(result_dataset['app_id'], result_dataset['app_service_id'])

                result_user = db.session.query(UserModel)
                result_user = result_user.filter(getattr(UserModel, 'kode_skpd') == json['kode_skpd'])
                result_user = result_user.all()

                temp_send = []
                for res_us in result_user:
                    temp_user = res_us.__dict__
                    # temp_user.pop('_sa_instance_state', None)

                    temp = {}
                    temp['user_id'] = temp_user['id']
                    temp['app_id'] = result_dataset['app_id']
                    temp['app_service_id'] = result_dataset['app_service_id']
                    temp['enable'] = True
                    temp_send.append(temp)

                for ts_ in temp_send:
                    re_, da_ = self.create(ts_)
                    res = re_
                    if re_:
                        user_permission.append(da_)
            else:
                user_permission = {}
                res = False

            # check if exist
            if res:
                return True, user_permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_private_multiple(self, json: dict):
        ''' Doc: function create private multiple  '''
        try:
            jsons = json
            results = []
            temp_send = []

            i = 0
            for json in jsons:
                i = i + 1

                try:
                    # generate json data
                    if('dataset_id' in json and 'kode_skpd' in json):

                        res = False
                        result_dataset = None
                        result_dataset = db.session.query(DatasetModel)
                        result_dataset = result_dataset.get(json['dataset_id'])
                        if result_dataset:
                            result_dataset = result_dataset.__dict__
                            result_dataset.pop('_sa_instance_state', None)

                            if i == 1:
                                self.insert_walidata(result_dataset['app_id'], result_dataset['app_service_id'])

                        result_user = None
                        result_user = db.session.query(UserModel)
                        result_user = result_user.filter(getattr(UserModel, 'kode_skpd') == json['kode_skpd'])
                        result_user = result_user.all()

                        for res_us in result_user:
                            temp_user = res_us.__dict__
                            # temp_user.pop('_sa_instance_state', None)

                            temp = {}
                            temp['user_id'] = temp_user['id']
                            temp['app_id'] = result_dataset['app_id']
                            temp['app_service_id'] = result_dataset['app_service_id']
                            temp['enable'] = True
                            temp_send.append(temp)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            for ts_ in temp_send:
                re_, da_ = self.create(ts_)
                res = re_
                if re_:
                    results.append(da_)

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_private(self, json: dict, dataset_id):
        ''' Doc: function update private  '''
        try:
            # generate json data
            if(dataset_id and 'kode_skpd' in json['old'] and 'kode_skpd' in json['new']):

                # user_permission = json
                user_permission = []
                res = False

                result_dataset = db.session.query(DatasetModel)
                result_dataset = result_dataset.get(dataset_id)
                if result_dataset:
                    result_dataset = result_dataset.__dict__
                    result_dataset.pop('_sa_instance_state', None)

                result_user_old = db.session.query(UserModel)
                result_user_old = result_user_old.filter(getattr(UserModel, 'kode_skpd') == json['old']['kode_skpd'])
                result_user_old = result_user_old.all()

                # delete old data
                for res_us_old in result_user_old:
                    temp_user_old = res_us_old.__dict__
                    lookup_user_permission = db.session.query(UserPermissionModel)
                    lookup_user_permission = lookup_user_permission.filter(getattr(UserPermissionModel, 'user_id') == temp_user_old['id'])
                    lookup_user_permission = lookup_user_permission.filter(getattr(UserPermissionModel, 'app_id') == result_dataset['app_id'])
                    lookup_user_permission = lookup_user_permission.filter(getattr(UserPermissionModel, 'app_service_id') == result_dataset['app_service_id'])
                    lookup_user_permission = lookup_user_permission.delete()

                # insert new
                result_user_new = db.session.query(UserModel)
                result_user_new = result_user_new.filter(getattr(UserModel, 'kode_skpd') == json['new']['kode_skpd'])
                result_user_new = result_user_new.all()

                temp_send = []
                for res_us_new in result_user_new:
                    temp_user_new = res_us_new.__dict__

                    temp = {}
                    temp['user_id'] = temp_user_new['id']
                    temp['app_id'] = result_dataset['app_id']
                    temp['app_service_id'] = result_dataset['app_service_id']
                    temp['enable'] = True
                    temp_send.append(temp)

                for td_ in temp_send:
                    re_, da_ = self.create(td_)
                    res = re_
                    if re_:
                        user_permission.append(da_)

                self.insert_walidata(result_dataset['app_id'], result_dataset['app_service_id'])

            else:
                user_permission = {}
                res = False

            # check if exist
            if res:
                return True, user_permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_private_multiple(self, json_: dict, dataset_id):
        ''' Doc: function update private multiple  '''
        try:
            results = []

            result_dataset = None
            result_dataset = db.session.query(DatasetModel)
            result_dataset = result_dataset.get(dataset_id)
            if result_dataset:
                result_dataset = result_dataset.__dict__
                result_dataset.pop('_sa_instance_state', None)


            # remove old
            jsons_old = json_['old']
            for json in jsons_old:

                try:
                    # generate json data
                    if dataset_id and 'kode_skpd' in json:

                        res = False
                        result_user_old = None
                        result_user_old = db.session.query(UserModel)
                        result_user_old = result_user_old.filter(getattr(UserModel, 'kode_skpd') == json['kode_skpd'])
                        result_user_old = result_user_old.all()

                        for res_us_old in result_user_old:
                            temp_user_old = res_us_old.__dict__
                            # temp_user.pop('_sa_instance_state', None)
                            lookup_user_permission = db.session.query(UserPermissionModel)
                            lookup_user_permission = lookup_user_permission.filter(getattr(UserPermissionModel, 'user_id') == temp_user_old['id'])
                            lookup_user_permission = lookup_user_permission.filter(getattr(UserPermissionModel, 'app_id') == result_dataset['app_id'])
                            lookup_user_permission = lookup_user_permission.filter(getattr(UserPermissionModel, 'app_service_id') == result_dataset['app_service_id'])
                            lookup_user_permission = lookup_user_permission.delete()

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})


            # insert new
            jsons_new = json_['new']
            temp_send = []
            for json in jsons_new:

                try:
                    # generate json data
                    if(dataset_id and 'kode_skpd' in json):

                        result_user_new = None
                        result_user_new = db.session.query(UserModel)
                        result_user_new = result_user_new.filter(getattr(UserModel, 'kode_skpd') == json['kode_skpd'])
                        result_user_new = result_user_new.all()

                        for res_us_new in result_user_new:
                            temp_user_new = res_us_new.__dict__
                            # temp_user.pop('_sa_instance_state', None)

                            temp = {}
                            temp['user_id'] = temp_user_new['id']
                            temp['app_id'] = result_dataset['app_id']
                            temp['app_service_id'] = result_dataset['app_service_id']
                            temp['enable'] = True
                            temp_send.append(temp)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            for ts_ in temp_send:
                re_, da_ = self.create(ts_)
                res = re_
                if re_:
                    results.append(da_)

            self.insert_walidata(result_dataset['app_id'], result_dataset['app_service_id'])

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def insert_walidata(self, app_id, app_service_id):
        ''' Doc: function get skpd  '''
        try:
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)
            data_user = data_user.filter(RoleModel.name == 'walidata')
            data_user = data_user.all()

            for res_us in data_user:
                res_us = res_us.__dict__
                res_us.pop('_sa_instance_state', None)

                temp = {}
                temp['user_id'] = res_us['id']
                temp['app_id'] = app_id
                temp['app_service_id'] = app_service_id
                temp['enable'] = True

                self.create(temp)

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
