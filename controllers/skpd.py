''' Doc: controller skpd  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import SkpdModel
from models import RegionalModel
from models import DatasetModel
from models import VisualizationModel
from models import IndikatorModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
from settings import configuration
import sqlalchemy
import sentry_sdk
import re

CONFIGURATION = Configuration()
HELPER = Helper()
CONFIG = configuration.Configuration()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long


class SkpdController(object):
    ''' Doc: class skpd  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(SkpdModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),)
            for attr, value in where.items():
                result = result.filter(getattr(SkpdModel, attr) == value)
            # result = result.filter(or_(cast(getattr(SkpdModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in SkpdModel.__table__.columns))
            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        # cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+src+'%'),
                        # cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+src+'%'),
                        cast(getattr(SkpdModel, "nama_skpd"),sqlalchemy.String).match(src),
                        cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).match(src),
                    ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            skpd = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to regional
                regional = db.session.query(RegionalModel)
                regional = regional.options(
                    defer("cuid"),
                    defer("cdate"),)
                regional = regional.get(temp['regional_id'])

                if regional:
                    regional = regional.__dict__
                    regional.pop('_sa_instance_state', None)
                else:
                    regional = {}

                temp['regional'] = regional

                # count dataset
                count_dataset = db.session.query(DatasetModel)
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'kode_skpd') == temp['kode_skpd'])
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'is_deleted') == False)
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'is_active') == True)
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'is_validate') == 3)
                count_dataset = count_dataset.count()
                temp['count_dataset'] = count_dataset

                # count dataset public
                count_dataset_public = db.session.query(DatasetModel)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'kode_skpd') == temp['kode_skpd'])
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'is_deleted') == False)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'is_active') == True)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'is_validate') == 3)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'dataset_class_id') == 3)
                count_dataset_public = count_dataset_public.count()
                temp['count_dataset_public'] = count_dataset_public

                # count visualization
                count_visualization = db.session.query(VisualizationModel)
                count_visualization = count_visualization.filter(
                    getattr(VisualizationModel, 'kode_skpd') == temp['kode_skpd'])
                count_visualization = count_visualization.filter(
                    getattr(VisualizationModel, 'is_deleted') == False)
                count_visualization = count_visualization.filter(
                    getattr(VisualizationModel, 'is_active') == True)
                count_visualization = count_visualization.count()
                temp['count_visualization'] = count_visualization

                # count visualization public
                count_visualization_public = db.session.query(
                    VisualizationModel)
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'kode_skpd') == temp['kode_skpd'])
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'is_deleted') == False)
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'is_active') == True)
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'dataset_class_id') == 3)
                count_visualization_public = count_visualization_public.count()
                temp['count_visualization_public'] = count_visualization_public

                # count infographic
                temp['count_infographic'] = 0
                temp['count_infographic_public'] = 0

                # count indikator
                count_indikator = db.session.query(IndikatorModel)
                count_indikator = count_indikator.filter(
                    getattr(IndikatorModel, 'kode_skpd') == temp['kode_skpd'])
                count_indikator = count_indikator.filter(
                    getattr(IndikatorModel, 'is_deleted') == False)
                count_indikator = count_indikator.filter(
                    getattr(IndikatorModel, 'is_active') == True)
                count_indikator = count_indikator.count()
                temp['count_indikator'] = count_indikator
                temp['count_indikator_public'] = count_indikator

                skpd.append(temp)

            # check if empty
            skpd = list(skpd)
            if skpd:
                return True, skpd
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(SkpdModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(SkpdModel, attr) == value)
            result = result.filter(or_(cast(getattr(SkpdModel, col.name), sqlalchemy.String).ilike(
                '%'+search+'%') for col in SkpdModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        # handle id=title, integer or string
        try:
            temp = int(_id)
            _id = temp
        except Exception as err:
            temp = _id
            result = db.session.query(SkpdModel.id)
            result = result.filter(SkpdModel.title == temp)
            result = result.all()
            if result:
                result = result[0]
                _id = result[0]
            else:
                _id = 0

        try:
            # execute database
            result = db.session.query(SkpdModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to regional
                regional = db.session.query(RegionalModel)
                regional = regional.options(
                    defer("cuid"),
                    defer("cdate"))
                regional = regional.get(result['regional_id'])

                if regional:
                    regional = regional.__dict__
                    regional.pop('_sa_instance_state', None)
                else:
                    regional = {}

                result['regional'] = regional

                # count dataset
                count_dataset = db.session.query(DatasetModel)
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'kode_skpd') == result['kode_skpd'])
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'is_deleted') == False)
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'is_active') == True)
                count_dataset = count_dataset.filter(
                    getattr(DatasetModel, 'is_validate') == 3)
                count_dataset = count_dataset.count()
                result['count_dataset'] = count_dataset

                # count dataset public
                count_dataset_public = db.session.query(DatasetModel)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'kode_skpd') == result['kode_skpd'])
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'is_deleted') == False)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'is_active') == True)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'is_validate') == 3)
                count_dataset_public = count_dataset_public.filter(
                    getattr(DatasetModel, 'dataset_class_id') == 3)
                count_dataset_public = count_dataset_public.count()
                result['count_dataset_public'] = count_dataset_public

                # count visualization
                count_visualization = db.session.query(VisualizationModel)
                count_visualization = count_visualization.filter(
                    getattr(VisualizationModel, 'kode_skpd') == result['kode_skpd'])
                count_visualization = count_visualization.filter(
                    getattr(VisualizationModel, 'is_deleted') == False)
                count_visualization = count_visualization.filter(
                    getattr(VisualizationModel, 'is_active') == True)
                count_visualization = count_visualization.count()
                result['count_visualization'] = count_visualization

                # count visualization public
                count_visualization_public = db.session.query(
                    VisualizationModel)
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'kode_skpd') == result['kode_skpd'])
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'is_deleted') == False)
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'is_active') == True)
                count_visualization_public = count_visualization_public.filter(
                    getattr(VisualizationModel, 'dataset_class_id') == 3)
                count_visualization_public = count_visualization_public.count()
                result['count_visualization_public'] = count_visualization_public

                # count infographic
                result['count_infographic'] = 0
                result['count_infographic_public'] = 0

                # count indikator
                count_indikator = db.session.query(IndikatorModel)
                count_indikator = count_indikator.filter(
                    getattr(IndikatorModel, 'kode_skpd') == result['kode_skpd'])
                count_indikator = count_indikator.filter(
                    getattr(IndikatorModel, 'is_deleted') == False)
                count_indikator = count_indikator.filter(
                    getattr(IndikatorModel, 'is_active') == True)
                count_indikator = count_indikator.count()
                result['count_indikator'] = count_indikator
                result['count_indikator_public'] = count_indikator
            else:
                result = {}

            # change into dict
            skpd = result

            # check if empty
            if skpd:
                return True, skpd
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            # title
            json_send["title"] = json_send["nama_skpd"].lower().replace(
                ' ', '-')
            json_send["title"] = re.sub(
                '[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(SkpdModel.id)
            res_count = res_count.filter(SkpdModel.title == json_send["title"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + \
                    '-' + str(res_count + 1)

            # nama_skpd
            json_send["nama_skpd_alias"] = json_send["nama_skpd_alias"].lower()
            res_count2 = db.session.query(SkpdModel.id)
            res_count2 = res_count2.filter(
                SkpdModel.nama_skpd_alias == json_send["nama_skpd_alias"])
            res_count2 = res_count2.count()
            if res_count2 > 0:
                json_send["nama_skpd_alias"] = json_send["nama_skpd_alias"] + \
                    '-' + str(res_count2 + 1)

            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = SkpdModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, skpd = self.get_by_id(result['id'])

            # create schema
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI, pool_size=configuration.SQLALCHEMY_POOL_SIZE, max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW)
            engine.connect()
            if not engine.dialect.has_schema(engine, json_send["nama_skpd_alias"]):
                engine.execute(sqlalchemy.schema.CreateSchema(
                    json_send["nama_skpd_alias"]))

            # check if exist
            if res:
                return True, skpd
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            # title
            if 'title' in json_send:
                json_send["title"] = json_send["nama_skpd"].lower().replace(
                    ' ', '-')
                json_send["title"] = re.sub(
                    '[^a-zA-Z0-9-]', '', json_send["title"])
                res_count = db.session.query(SkpdModel.id)
                res_count = res_count.filter(
                    SkpdModel.title == json_send["title"])
                res_count = res_count.count()
                if res_count > 0:
                    json_send["title"] = json_send["title"] + \
                        '-' + str(res_count + 1)

            # nama_skpd
            if 'nama_skpd_alias' in json_send:
                json_send["nama_skpd_alias"] = json_send["nama_skpd_alias"].lower()
                res_count2 = db.session.query(SkpdModel.id)
                res_count2 = res_count2.filter(
                    SkpdModel.nama_skpd_alias == json_send["nama_skpd_alias"])
                res_count2 = res_count2.count()
                if res_count2 > 0:
                    json_send["nama_skpd_alias"] = json_send["nama_skpd_alias"] + \
                        '-' + str(res_count2 + 1)

            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(SkpdModel)
                for attr, value in where.items():
                    result = result.filter(getattr(SkpdModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, skpd = self.get_by_id(where["id"])

                # create schema
                engine = sqlalchemy.create_engine(
                    CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI, pool_size=configuration.SQLALCHEMY_POOL_SIZE, max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW)
                engine.connect()
                if not engine.dialect.has_schema(engine, skpd["nama_skpd_alias"]):
                    engine.execute(sqlalchemy.schema.CreateSchema(
                        skpd["nama_skpd_alias"]))

                # check if empty
                if res:
                    return True, skpd
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(SkpdModel)
                for attr, value in where.items():
                    result = result.filter(getattr(SkpdModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, skpd = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_trending(self, sort):
        ''' Doc: function geet trending  '''
        try:
            # query postgresql
            sql = text(
                'SELECT org.*,(SELECT count(history_download.id) FROM history_download ' +
                'INNER JOIN dataset ON history_download.category_id = dataset.id ' +
                'INNER JOIN skpd ON dataset.kode_skpd = skpd.kode_skpd ' +
                'WHERE skpd.id = org.id and history_download.category = \'dataset\') ' +
                'FROM skpd AS org WHERE org.is_external = FALSE ORDER BY ' +
                sort[0]+' '+sort[1]+' LIMIT 5'
            )
            result = db.engine.execute(sql)

            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                kd_skpd = "'"+res['kode_skpd']+"'"
                sql = text(
                    'select to_char(datetime, \'YYYY-MM-DD\') as date , count(datetime) as total ' +
                    'from history_download ' +
                    'inner join dataset on history_download.category_id = dataset.id ' +
                    'where dataset.kode_skpd = '+kd_skpd+' and history_download.category = \'dataset\' ' +
                    'group by 1 ORDER BY date desc limit 30'
                )
                res_tgl = db.engine.execute(sql)
                row_append.update(
                    {"datecount": [row['total'] for row in res_tgl]})
                row_append['datecount'] = row_append['datecount'][::-1]
                str_check = ",".join(str(x) for x in row_append['datecount'])
                if not str_check:
                    counts = 0
                else:
                    counts = str_check
                row_append.update({"datecount": counts})
                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
