''' Doc: controller app service  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import AppServiceModel
from models import AppModel
from models import DatasetModel
from models import DatasetClassModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable
class AppServiceController(object):
    ''' Doc: controller app service  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip, enable_id):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(AppServiceModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),
                defer("is_deleted"),
                defer("is_active"))
            for attr, value in where.items():
                result = result.filter(getattr(AppServiceModel, attr) == value)
            result = result.filter(or_(cast(getattr(AppServiceModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AppServiceModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            app_service = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to dataset_class
                if temp['dataset_class_id']:
                    # get relation to skpd
                    dataset_class = db.session.query(DatasetClassModel)
                    dataset_class = dataset_class.get(temp['dataset_class_id'])

                    if dataset_class:
                        dataset_class = dataset_class.__dict__
                        dataset_class.pop('_sa_instance_state', None)
                    else:
                        dataset_class = {}

                    temp['dataset_class'] = dataset_class
                else:
                    temp['dataset_class'] = {}

                # get relation to dataset
                dataset = db.session.query(DatasetModel)
                dataset = dataset.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                dataset = dataset.filter(getattr(DatasetModel, 'app_service_id') == temp['id'])
                dataset = dataset.all()
                if dataset:
                    if str(enable_id) == str(temp['id']):
                        temp['is_used'] = False
                    else:
                        temp['is_used'] = True
                else:
                    temp['is_used'] = False

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app = app.get(temp['app_id'])

                if app:
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                temp['app'] = app
                app_service.append(temp)

            # check if empty
            app_service = list(app_service)
            if app_service:
                return True, app_service
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(AppServiceModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(AppServiceModel, attr) == value)
            result = result.filter(or_(cast(getattr(AppServiceModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AppServiceModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(AppServiceModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),
                defer("is_deleted"),
                defer("is_active"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to dataset_class
                if result['dataset_class_id']:
                    # get relation to skpd
                    dataset_class = db.session.query(DatasetClassModel)
                    dataset_class = dataset_class.get(result['dataset_class_id'])

                    if dataset_class:
                        dataset_class = dataset_class.__dict__
                        dataset_class.pop('_sa_instance_state', None)
                    else:
                        dataset_class = {}

                    result['dataset_class'] = dataset_class
                else:
                    result['dataset_class'] = {}

                # get relation to dataset
                dataset = db.session.query(DatasetModel)
                dataset = dataset.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                dataset = dataset.filter(getattr(DatasetModel, 'app_service_id') == result['id'])
                dataset = dataset.all()
                if dataset:
                    result['is_used'] = True
                else:
                    result['is_used'] = False

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("is_deleted"),
                    defer("is_active"))
                app = app.get(result['app_id'])

                if app:
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                result['app'] = app

            else:
                result = {}

            # change into dict
            app_service = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            # prepare data model
            result = AppServiceModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, app_service = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = AppServiceModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, app_service = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(app_service)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(AppServiceModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AppServiceModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, app_service = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, app_service
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(AppServiceModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AppServiceModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, app_service = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all_permission(self, app_id, user_id):
        ''' Doc: function get all permisiion  '''
        try:
            # query postgresql
            sql = text(
                "SELECT "+
                "app_service.*, "+
                "(SELECT enable from users_permission where users_permission.app_service_id = app_service.id and users_permission.user_id = '" + user_id + "' and users_permission.app_id = '" + app_id + "') "+
                "from app_service "+
                "where app_service.app_id = '" + app_id + "'" +
                "order by controller asc"
            )
            result = db.engine.execute(sql)
            app_service = [dict(row) for row in result]

            # check if empty
            app_service = list(app_service)
            if app_service:
                return True, app_service
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
