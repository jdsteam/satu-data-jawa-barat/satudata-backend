''' Doc: controller mapset  '''
from settings.configuration import Configuration
from settings import configuration as conf
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetModel
from models import RegionalModel
from models import SektoralModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from models import MapsetTypeModel
from models import MapsetSourceModel
from models import AppModel
from models import AppServiceModel
from models import DatasetClassModel
from models import MapsetMetadataModel
from models import MapsetMetadataTypeModel
from models import MapsetProgramModel
from models import MapsetAreaCoverageModel
from models import HistoryDraftModel
from models import UserModel
from models import RoleModel
from models import MapsetMetadataKugiModel
from controllers.history import HistoryController
from controllers.history_draft import HistoryDraftController
from controllers.mapset_metadata import MapsetMetadataController
from controllers.notification import NotificationController
from controllers.user import UserController
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
from sqlalchemy import func
import sqlalchemy, sentry_sdk
import itertools
from operator import itemgetter
import re
import requests
import json
from flask import request
import pandas as pd
import urllib

CONFIGURATION = Configuration()
HELPER = Helper()
HISTORY_CONTROLLER = HistoryController()
HISTORY_DRAFT_CONTROLLER = HistoryDraftController()
MAPSET_METADATA_CONTROLLER = MapsetMetadataController()
NOTIFICATION_CONTROLLER = NotificationController()
USER_CONTROLLER = UserController()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
MESSAGE_RECEIVER = "?receiver="
MESSAGE_MEMBUTUHKAN_VERIFIKASI = "</b> membutuhkan verifikasi."
MESSAGE_DARI = " dari <b>"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MapsetController(object):
    ''' Doc: class mapset  '''

    def __init__(self, **kwargs):
        ''' Doc: function init '''

    def query(self, where: dict, search):
        ''' Doc: function query '''
        try:
            # query postgresql
            result = db.session.query(
                MapsetModel.id, MapsetModel.name, MapsetModel.title, MapsetModel.description,
                MapsetModel.sektoral_id, MapsetModel.mapset_type_id, MapsetModel.mapset_source_id, MapsetModel.dataset_class_id,
                MapsetModel.regional_id, MapsetModel.kode_skpd, MapsetModel.kode_skpdsub, MapsetModel.kode_skpdunit,
                MapsetModel.app_id, MapsetModel.app_service_id, MapsetModel.app_link, MapsetModel.metadata_xml, MapsetModel.notes,
                MapsetModel.owner, MapsetModel.owner_email, MapsetModel.maintainer, MapsetModel.maintainer_email,
                MapsetModel.count_view, MapsetModel.count_access, MapsetModel.count_view_private, MapsetModel.count_access_private,
                MapsetModel.count_download_dataset, MapsetModel.count_download_image, MapsetModel.count_download_shp, MapsetModel.count_download_geojson,
                MapsetModel.cuid, MapsetModel.cdate, MapsetModel.muid, MapsetModel.mdate,
                MapsetModel.is_active, MapsetModel.is_popular, MapsetModel.is_deleted, MapsetModel.is_validate, MapsetModel.is_permanent,
                MapsetModel.scale, MapsetModel.update_period, MapsetModel.revision_date, MapsetModel.metadata_created_date, MapsetModel.data_version, MapsetModel.owner_address, MapsetModel.owner_telephone, MapsetModel.organization_manual, MapsetModel.area_coverage, MapsetModel.access_organization, MapsetModel.shp,
                SektoralModel.name.label('sektoral_name'), SektoralModel.notes.label('sektoral_notes'),
                SektoralModel.picture.label('sektoral_picture'),
                SektoralModel.picture_thumbnail.label('sektoral_picture_thumbnail'),
                SektoralModel.is_opendata.label('sektoral_is_opendata'),
                SektoralModel.is_satudata.label('sektoral_is_satudata'),
                MapsetTypeModel.name.label('mapset_type_name'), MapsetTypeModel.notes.label('mapset_type_notes'),
                MapsetSourceModel.name.label('mapset_source_name'), MapsetTypeModel.notes.label('mapset_source_notes'),
                DatasetClassModel.name.label('dataset_class_name'), DatasetClassModel.notes.label('dataset_class_notes'),
                RegionalModel.regional_level_id.label('regional_regional_level_id'), RegionalModel.kode_bps.label('regional_kode_bps'),
                RegionalModel.kode_kemendagri.label('regional_kode_kemendagri'), RegionalModel.nama_bps.label('regional_nama_bps'),
                RegionalModel.nama_kemendagri.label('regional_nama_kemendagri'), RegionalModel.notes.label('regional_notes'),
                SkpdModel.id.label('skpd_id'),
                SkpdModel.nama_skpd.label('skpd_nama_skpd'), SkpdModel.nama_skpd_alias.label('skpd_nama_skpd_alias'),
                SkpdModel.title.label('skpd_title'),
                SkpdModel.logo.label('skpd_logo'), SkpdModel.count_dataset.label('skpd_count_dataset'),
                SkpdModel.count_indikator.label('skpd_count_indikator'),
                SkpdModel.count_visualization.label('skpd_count_visualization'), SkpdModel.is_satupeta.label('skpd_is_satupeta'),
                SkpdSubModel.nama_skpdsub.label('skpdsub_nama_skpdsub'),
                SkpdUnitModel.nama_skpdunit.label('skpdunit_nama_skpdunit'),
                AppModel.name.label('app_name'), AppModel.code.label('app_code'), AppModel.url.label('app_url'),
                AppModel.kode_skpd.label('app_kode_skpd'),
                AppModel.notes.label('app_notes'), AppModel.is_active.label('app_is_active'), AppModel.is_deleted.label('app_is_deleted'),
                AppServiceModel.app_id.label('app_service_app_id'), AppServiceModel.dataset_class_id.label('app_service_dataset_class_id'),
                AppServiceModel.controller.label('app_service_controller'), AppServiceModel.action.label('app_service_action'),
                AppServiceModel.notes.label('app_service_notes'), AppServiceModel.is_active.label('app_service_is_active'),
                AppServiceModel.is_deleted.label('app_service_is_deleted'), AppServiceModel.is_backend.label('app_service_is_backend'),
                MapsetModel.mapset_program_id, MapsetModel.skpd_id, MapsetProgramModel.name.label('mapset_program_name'),
                MapsetModel.mapset_area_coverage_id, MapsetAreaCoverageModel.name.label('mapset_area_coverage_name')
            )
            result = result.join(SektoralModel, MapsetModel.sektoral_id == SektoralModel.id)
            result = result.join(MapsetTypeModel, MapsetModel.mapset_type_id == MapsetTypeModel.id, isouter=True)
            result = result.join(MapsetSourceModel, MapsetModel.mapset_source_id == MapsetSourceModel.id, isouter=True)
            result = result.join(DatasetClassModel, MapsetModel.dataset_class_id == DatasetClassModel.id, isouter=True)
            result = result.join(RegionalModel, MapsetModel.regional_id == RegionalModel.id, isouter=True)
            result = result.join(SkpdModel, MapsetModel.kode_skpd == SkpdModel.kode_skpd, isouter=True)
            result = result.join(SkpdSubModel, MapsetModel.kode_skpdsub == SkpdSubModel.kode_skpdsub, isouter=True)
            result = result.join(SkpdUnitModel, MapsetModel.kode_skpdunit == SkpdUnitModel.kode_skpdunit, isouter=True)
            result = result.join(AppModel, MapsetModel.app_id == AppModel.id, isouter=True)
            result = result.join(AppServiceModel, MapsetModel.app_service_id == AppServiceModel.id, isouter=True)
            result = result.join(MapsetProgramModel, MapsetModel.mapset_program_id == MapsetProgramModel.id, isouter=True)
            result = result.join(MapsetAreaCoverageModel, MapsetModel.mapset_area_coverage_id == MapsetAreaCoverageModel.id, isouter=True)

            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(MapsetModel.is_deleted == False)
                result = result.filter(MapsetModel.is_active == True)
                result = result.filter(MapsetModel.dataset_class_id == 3)
            else:
                if jwt["role_id"] not in {1, 10}:
                    if jwt["role_id"] == 24:
                        result = result.filter(MapsetModel.dataset_class_id != 4)
                    else:
                        result = result.filter(or_( cast(getattr(MapsetModel, "access_organization"), sqlalchemy.String).ilike('%'+jwt["kode_skpd"]+'%'), MapsetModel.dataset_class_id != 4))

            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(MapsetModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(MapsetModel, attr) == value)

            # search
            result = result.filter(or_(
                cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SektoralModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(MapsetModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(MapsetModel, "description"), sqlalchemy.String).ilike('%'+search+'%')
                ))
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_data(self, temp):
        ''' Doc: function populate data '''
        try:
            temp['cdate'] = temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")
            temp['mdate'] = temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")

            # get relation to sektoral
            temp['sektoral'] = {}
            temp['sektoral']['id'] = temp['sektoral_id']
            temp['sektoral']['name'] = temp['sektoral_name']
            temp['sektoral']['notes'] = temp['sektoral_notes']
            temp['sektoral']['is_opendata'] = temp['sektoral_is_opendata']
            temp['sektoral']['is_satudata'] = temp['sektoral_is_satudata']
            temp['sektoral']['picture'] = temp['sektoral_picture']
            temp['sektoral']['picture_thumbnail'] = temp['sektoral_picture_thumbnail']
            # temp.pop('sektoral_id', None)
            temp.pop('sektoral_name', None)
            temp.pop('sektoral_notes', None)
            temp.pop('sektoral_is_opendata', None)
            temp.pop('sektoral_is_satudata', None)
            temp.pop('sektoral_picture', None)
            temp.pop('sektoral_picture_thumbnail', None)

            # get relation to mapset_type
            temp['mapset_type'] = {}
            temp['mapset_type']['id'] = temp['mapset_type_id']
            temp['mapset_type']['name'] = temp['mapset_type_name']
            temp['mapset_type']['notes'] = temp['mapset_type_notes']
            temp.pop('mapset_type_id', None)
            temp.pop('mapset_type_name', None)
            temp.pop('mapset_type_notes', None)

            # get relation to mapset_source
            temp['mapset_source'] = {}
            temp['mapset_source']['id'] = temp['mapset_source_id']
            temp['mapset_source']['name'] = temp['mapset_source_name']
            temp['mapset_source']['notes'] = temp['mapset_source_notes']
            temp.pop('mapset_source_id', None)
            temp.pop('mapset_source_name', None)
            temp.pop('mapset_source_notes', None)

            # get relation to dataset_class
            temp['dataset_class'] = {}
            temp['dataset_class']['id'] = temp['dataset_class_id']
            temp['dataset_class']['name'] = temp['dataset_class_name']
            temp['dataset_class']['notes'] = temp['dataset_class_notes']
            temp.pop('dataset_class_id', None)
            temp.pop('dataset_class_name', None)
            temp.pop('dataset_class_notes', None)

            # get relation to regional
            temp['regional'] = {}
            temp['regional']['id'] = temp['regional_id']
            temp['regional']['regional_level_id'] = temp['regional_regional_level_id']
            temp['regional']['kode_bps'] = temp['regional_kode_bps']
            temp['regional']['kode_kemendagri'] = temp['regional_kode_kemendagri']
            temp['regional']['nama_bps'] = temp['regional_nama_bps']
            temp['regional']['nama_kemendagri'] = temp['regional_nama_kemendagri']
            temp['regional']['notes'] = temp['regional_notes']
            # temp.pop('regional_id', None)
            temp.pop('regional_regional_level_id', None)
            temp.pop('regional_kode_bps', None)
            temp.pop('regional_kode_kemendagri', None)
            temp.pop('regional_nama_bps', None)
            temp.pop('regional_nama_kemendagri', None)
            temp.pop('regional_notes', None)

            # get relation to skpd
            temp['skpd'] = {}
            temp['skpd']['kode_skpd'] = temp['kode_skpd']
            temp['skpd']['nama_skpd'] = temp['skpd_nama_skpd']
            temp['skpd']['nama_skpd_alias'] = temp['skpd_nama_skpd_alias']
            temp['skpd']['title'] = temp['skpd_title']
            temp['skpd']['logo'] = temp['skpd_logo']
            temp['skpd']['count_dataset'] = temp['skpd_count_dataset']
            temp['skpd']['count_indikator'] = temp['skpd_count_indikator']
            temp['skpd']['count_visualization'] = temp['skpd_count_visualization']
            temp['skpd']['is_satupeta'] = temp['skpd_is_satupeta']
            # temp.pop('kode_skpd', None)
            temp.pop('skpd_nama_skpd', None)
            temp.pop('skpd_nama_skpd_alias', None)
            temp.pop('skpd_title', None)
            temp.pop('skpd_logo', None)
            temp.pop('skpd_count_dataset', None)
            temp.pop('skpd_count_indikator', None)
            temp.pop('skpd_count_visualization', None)
            temp.pop('skpd_is_satupeta', None)

            # get relation to skpdsub
            temp['skpdsub'] = {}
            temp['skpdsub']['kode_skpdsub'] = temp['kode_skpdsub']
            temp['skpdsub']['nama_skpdsub'] = temp['skpdsub_nama_skpdsub']
            temp.pop('kode_skpdsub', None)
            temp.pop('skpdsub_nama_skpdsub', None)

            # get relation to skpdunit
            temp['skpdunit'] = {}
            temp['skpdunit']['kode_skpdunit'] = temp['kode_skpdunit']
            temp['skpdunit']['nama_skpdunit'] = temp['skpdunit_nama_skpdunit']
            temp.pop('kode_skpdunit', None)
            temp.pop('skpdunit_nama_skpdunit', None)

            # get relation to app
            temp['app'] = {}
            temp['app']['id'] = temp['app_id']
            temp['app']['name'] = temp['app_name']
            temp['app']['code'] = temp['app_code']
            temp['app']['url'] = temp['app_url']
            temp['app']['kode_skpd'] = temp['app_kode_skpd']
            temp['app']['notes'] = temp['app_notes']
            temp['app']['is_active'] = temp['app_is_active']
            temp['app']['is_deleted'] = temp['app_is_deleted']
            temp.pop('app_id', None)
            temp.pop('app_name', None)
            temp.pop('app_code', None)
            temp.pop('app_url', None)
            temp.pop('app_kode_skpd', None)
            temp.pop('app_notes', None)
            temp.pop('app_is_active', None)
            temp.pop('app_is_deleted', None)

            # get relation to app_service
            temp['app_service'] = {}
            temp['app_service']['id'] = temp['app_service_id']
            temp['app_service']['app_id'] = temp['app_service_app_id']
            temp['app_service']['dataset_class_id'] = temp['app_service_dataset_class_id']
            temp['app_service']['controller'] = temp['app_service_controller']
            temp['app_service']['action'] = temp['app_service_action']
            temp['app_service']['notes'] = temp['app_service_notes']
            temp['app_service']['is_active'] = temp['app_service_is_active']
            temp['app_service']['is_deleted'] = temp['app_service_is_deleted']
            temp['app_service']['is_backend'] = temp['app_service_is_backend']
            temp['app_service']['dataset_class'] = {}
            temp.pop('app_service_id', None)
            temp.pop('app_service_app_id', None)
            temp.pop('app_service_dataset_class_id', None)
            temp.pop('app_service_controller', None)
            temp.pop('app_service_action', None)
            temp.pop('app_service_notes', None)
            temp.pop('app_service_is_active', None)
            temp.pop('app_service_is_deleted', None)
            temp.pop('app_service_is_backend', None)
            if temp['app_service']['dataset_class_id']:
                dataset_class = db.session.query(DatasetClassModel)
                dataset_class = dataset_class.get(temp['app_service']['dataset_class_id'])
                if dataset_class:
                    dataset_class = dataset_class.__dict__
                    dataset_class.pop('_sa_instance_state', None)
                else:
                    dataset_class = {}
                temp['app_service']['dataset_class'] = dataset_class

            # get relation to mapset_program
            temp['mapset_program'] = {}
            temp['mapset_program']['id'] = temp['mapset_program_id']
            temp['mapset_program']['name'] = temp['mapset_program_name']
            # temp.pop('mapset_program_id', None)
            # temp.pop('mapset_program_name', None)

            # get relation to mapset_area_coverage
            temp['mapset_area_coverage'] = {}
            temp['mapset_area_coverage']['id'] = temp['mapset_area_coverage_id']
            temp['mapset_area_coverage']['name'] = temp['mapset_area_coverage_name']
            temp.pop('mapset_area_coverage_name', None)

            # get relation to history_draft
            temp = self.populate_relation_history_draft(temp)

            # get metadata
            temp['metadata'] = []
            metadata = db.session.query(
                MapsetMetadataModel.id, MapsetMetadataModel.mapset_metadata_type_id,
                MapsetMetadataTypeModel.name.label('mapset_metadata_type_name'),
                MapsetMetadataModel.mapset_id, MapsetMetadataModel.key, MapsetMetadataModel.value
            )
            metadata = metadata.join(MapsetMetadataModel, MapsetMetadataModel.mapset_metadata_type_id == MapsetMetadataTypeModel.id)
            metadata = metadata.filter(getattr(MapsetMetadataModel, "mapset_id") == temp['id'])
            metadata = metadata.order_by(text("mapset_metadata.id asc"))
            metadata = metadata.all()
            for md_ in metadata:
                md_ = md_._asdict()
                temp['metadata'].append(md_)

            return temp
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip, groupby):
        ''' Doc: function get all '''
        try:
            # change sorting key
            if sort[0] == 'mdate':
                sort[0] = "mapset.mdate"
            elif sort[0] == 'name':
                sort[0] = "mapset.name"
            elif sort[0] == 'organization':
                sort[0] = "skpd.nama_skpd"
            elif sort[0] == 'mapset_type':
                sort[0] = "mapset_type.name"
            elif sort[0] == 'mapset_source':
                sort[0] = "mapset_source.name"
            elif sort[0] == 'classification':
                sort[0] = "dataset_class.name"
            elif sort[0] == 'is_active':
                sort[0] = "mapset.is_active"
            elif sort[0] == 'is_popular':
                sort[0] = "mapset.is_popular"
            else:
                sort[0] = "mapset." + sort[0]

            result = self.query(where, search)

            # filter regional
            if groupby == 'regional':
                result = result.filter(getattr(MapsetModel, 'regional_id') != '1')

            # filter program
            if groupby == 'program':
                result = result.filter(getattr(MapsetModel, 'mapset_program_id') != None)

            # filter limit
            if groupby == "":
                if sort[0] == 'id':
                    sort[0] = 'mapset.id'
                result = result.order_by(text(sort[0]+" "+sort[1]))
                result = result.offset(skip).limit(limit)

            # get all
            result = result.all()

            # change into dict
            mapset = []
            mapset_final = []
            for res in result:
                temp = res._asdict()
                temp = self.populate_data(temp)
                mapset.append(temp)

            if groupby == 'skpd':
                mapset_final = self.populate_group_skpd(mapset)

            elif groupby == 'sektoral':
                mapset_final = self.populate_group_sektoral(mapset)

            elif groupby == 'regional':
                mapset_final = self.populate_group_regional(mapset)

            elif groupby == 'program':
                mapset_final = self.populate_group_program(mapset)

            else:
                mapset_final = mapset

            # check if empty
            mapset_final = list(mapset_final)
            if mapset_final:
                return True, mapset_final
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_group_skpd(self, mapset):
        ''' Doc: function populate_group_skpd '''
        mapset_final = []
        mapset = sorted(mapset, key=itemgetter('kode_skpd'))
        for key, value in itertools.groupby(mapset, key=itemgetter('kode_skpd')):
            temps = {}
            temps['kode_skpd'] = key
            temps['nama_skpd'] = ''
            temps['nama_skpd_alias'] = ''
            temps['title'] = ''
            temps['logo'] = ''
            temps['count_dataset'] = 0
            temps['count_indikator'] = 0
            temps['count_visualization'] = 0
            temps['mapset'] = []
            for i in value:
                temps['nama_skpd'] = i['skpd']['nama_skpd']
                temps['nama_skpd_alias'] = i['skpd']['nama_skpd_alias']
                temps['title'] = i['skpd']['title']
                temps['logo'] = i['skpd']['logo']
                temps['count_dataset'] = i['skpd']['count_dataset']
                temps['count_indikator'] = i['skpd']['count_indikator']
                temps['count_visualization'] = i['skpd']['count_visualization']
                if i['skpd']['is_satupeta'] == True:
                    temps['mapset'].append(i)
            if temps['kode_skpd'] != '':
                mapset_final.append(temps)

        return mapset_final

    def populate_group_sektoral(self, mapset):
        ''' Doc: function populate_group_sektoral '''
        mapset_final = []
        mapset = sorted(mapset, key=itemgetter('sektoral_id'))
        for key, value in itertools.groupby(mapset, key=itemgetter('sektoral_id')):
            temps = {}
            temps['id'] = key
            temps['name'] = ''
            temps['notes'] = ''
            temps['picture'] = ''
            temps['picture_thumbnail'] = ''
            temps['is_opendata'] = ''
            temps['is_satudata'] = ''
            temps['mapset'] = []
            for i in value:
                temps['name'] = i['sektoral']['name']
                temps['notes'] = i['sektoral']['notes']
                temps['picture'] = i['sektoral']['picture']
                temps['picture_thumbnail'] = i['sektoral']['picture_thumbnail']
                temps['is_opendata'] = i['sektoral']['is_opendata']
                temps['is_satudata'] = i['sektoral']['is_satudata']
                if i['skpd']['is_satupeta'] == True:
                    temps['mapset'].append(i)
            mapset_final.append(temps)

        return mapset_final

    def populate_group_regional(self, mapset):
        ''' Doc: function populate_group_regional '''
        mapset_final = []
        mapset = sorted(mapset, key=itemgetter('regional_id'))
        for key, value in itertools.groupby(mapset, key=itemgetter('regional_id')):
            temps = {}
            temps['id'] = key
            temps['regional_level_id'] = ''
            temps['kode_bps'] = ''
            temps['kode_kemendagri'] = ''
            temps['nama_bps'] = ''
            temps['nama_kemendagri'] = ''
            temps['notes'] = ''
            temps['mapset'] = []
            for i in value:
                temps['regional_level_id'] = i['regional']['regional_level_id']
                temps['kode_bps'] = i['regional']['kode_bps']
                temps['kode_kemendagri'] = i['regional']['kode_kemendagri']
                temps['nama_bps'] = i['regional']['nama_bps']
                temps['nama_kemendagri'] = i['regional']['nama_kemendagri']
                temps['notes'] = i['regional']['notes']
                if i['skpd']['is_satupeta'] == True:
                    temps['mapset'].append(i)
            mapset_final.append(temps)

        return mapset_final

    def populate_group_program(self, mapset):
        ''' Doc: function populate_group_program '''
        mapset_final = []
        mapset = sorted(mapset, key=itemgetter('mapset_program_id'))
        # print(mapset)
        for key, value in itertools.groupby(mapset, key=itemgetter('mapset_program_id')):
            temps = {}
            temps['id'] = key
            temps['mapset_program_id'] = ''
            temps['mapset_program_name'] = ''
            temps['mapset'] = []
            for i in value:
                temps['mapset_program_id'] = i['mapset_program']['id']
                temps['mapset_program_name'] = i['mapset_program']['name']
                temps['mapset'].append(i)
            mapset_final.append(temps)

        return mapset_final

    def get_count(self, where: dict, search):
        ''' Doc: function get count '''
        try:
            # query postgresql
            result = db.session.query(MapsetModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(MapsetModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(MapsetModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MapsetModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id, where):
        ''' Doc: function get by id '''
        try:
            # execute database
            result = self.query({}, '')

            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(MapsetModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(MapsetModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            if jwt:
                result = result.filter(MapsetModel.id == _id)
                result = result.all()
            else:
                result = result.filter(MapsetModel.dataset_class_id == 3)
                result = result.filter(MapsetModel.id == _id)
                result = result.all()

            if result:
                result = result[0]._asdict()
                result = self.populate_data(result)
            else:
                result = {}

            # change into dict
            mapset = result

            # check if empty
            if mapset:
                return True, mapset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            # cuid
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt["id"]

            # title
            json_send["title"] = json_send["name"].lower().replace(' ', '-')
            json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(MapsetModel.id)
            res_count = res_count.filter(MapsetModel.title == json_send["title"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            # area_coverage
            if "mapset_area_coverage_id" in json_send:
                if json_send['mapset_area_coverage_id'] == 1:
                    json_send['area_coverage'] = 'province'
                elif json_send['mapset_area_coverage_id'] == 2:
                    json_send['area_coverage'] = 'city'

            # prepare data model
            result = MapsetModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, mapset = self.get_by_id(result['id'], {})

            # check if exist
            if res:
                return True, mapset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict, update_time=bool):
        ''' Doc: function update '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            # cuid
            if update_time == True:
                json_send["mdate"] = HELPER.local_date_server()

            # muid
            if jwt:
                json_send["muid"] = jwt["id"]

            try:
                # prepare data model
                result = db.session.query(MapsetModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, mapset = self.get_by_id(where["id"], {})

                # check if empty
                if res:
                    return True, mapset
                else:
                    return False, {}

            except Exception as err:
                # fail response
                print(str(err))
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete '''
        try:

            try:
                # prepare data model
                result = db.session.query(MapsetModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, mapset = self.get_by_id(where["id"], {})

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_statistik(self):
        ''' Doc: function get_statistik '''
        try:
            # query postgresql
            count_mapset = db.session.query(MapsetModel.id)
            count_mapset = count_mapset.filter(getattr(MapsetModel, 'is_active') == True)
            count_mapset = count_mapset.filter(getattr(MapsetModel, 'dataset_class_id') == 3)
            count_mapset = count_mapset.filter(getattr(MapsetModel, 'is_validate') == 3)
            count_mapset = count_mapset.count()

            count_skpd = db.session.query(func.distinct(MapsetModel.kode_skpd))
            count_skpd = count_skpd.filter(getattr(MapsetModel, 'is_active') == True)
            count_skpd = count_skpd.filter(getattr(MapsetModel, 'dataset_class_id') == 3)
            count_skpd = count_skpd.filter(getattr(MapsetModel, 'kode_skpd') != '3.07.01')
            count_skpd = count_skpd.count()

            count_regional = db.session.query(func.distinct(MapsetModel.regional_id))
            count_regional = count_regional.filter(getattr(MapsetModel, 'is_active') == True)
            count_regional = count_regional.filter(getattr(MapsetModel, 'dataset_class_id') == 3)
            count_regional = count_regional.count()

            # count_metadata =
            req = requests.get(
                'https://satupeta.jabarprov.go.id/sikambing/api/harvestings/',
                headers={"Content-Type": 'application/json'}
            )
            res = req.json()
            if res:
                count_metadata = len(res['data'])
            else:
                count_metadata = 0

            # change into dict
            statistik = {}
            statistik['mapset'] = count_mapset
            statistik['skpd'] = count_skpd
            statistik['regional'] = count_regional
            statistik['metadata_sikambing'] = count_metadata

            # check if empty
            if statistik:
                return True, statistik
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_relation_history_draft(self, temp):
        ''' Doc: function populate relation history draft  '''
        # get relation to history_draft
        if temp['is_validate'] is not None:
            try:
                history_draft = db.session.query(HistoryDraftModel)
                history_draft = history_draft.filter(getattr(HistoryDraftModel, "type") == 'mapset')
                history_draft = history_draft.filter(getattr(HistoryDraftModel, "type_id") == temp['id'])
                if temp['is_validate'] == '1':
                    history_draft = history_draft.filter(getattr(HistoryDraftModel, "category") == 'draft')
                elif temp['is_validate'] == '2':
                    history_draft = history_draft.filter(getattr(HistoryDraftModel, "category") == 'revision')
                elif temp['is_validate'] == '3':
                    history_draft = history_draft.filter(getattr(HistoryDraftModel, "category") == 'approve')
                history_draft = history_draft.order_by(text("id desc"))
                history_draft = history_draft.limit(1)
                history_draft = history_draft.one()

                if history_draft:
                    history_draft = history_draft.__dict__
                    history_draft.pop('_sa_instance_state', None)
                    history_draft['datetime'] = history_draft['datetime'].strftime(DATETIME_FORMAT)
                else:
                    history_draft = {}
            except Exception:
                history_draft = {}

            temp['history_draft'] = history_draft
        else:
            temp['history_draft'] = {}

        return temp

    def route_insert_history_draft(self, history_draft, mapset):
        ''' Doc: function route_insert_history_draft  '''

        jwt = HELPER.read_jwt()
        json_history_draft = {}
        json_history_draft['type'] = 'mapset'
        json_history_draft['type_id'] = mapset['id']
        json_history_draft['user_id'] = jwt['id']
        json_history_draft['datetime'] = HELPER.local_date_server()
        json_history_draft['category'] = history_draft['category']
        json_history_draft['notes'] = history_draft['notes']

        result, result_data = HISTORY_DRAFT_CONTROLLER.create(json_history_draft)

    def route_create_metadata(self, temp_metadata, dataset):
        ''' Doc: function route_create_metadata  '''
        try:
            for metadata in temp_metadata:
                temp = metadata
                temp['mapset_id'] = dataset['id']
                res, res_metadata = MAPSET_METADATA_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_update_metadata(self, temp_metadata, dataset):
        ''' Doc: function route_update_metadata  '''
        try:
            if temp_metadata:
                result_metadata = db.session.query(MapsetMetadataModel)
                result_metadata = result_metadata.filter(getattr(MapsetMetadataModel, 'mapset_id') == dataset['id'])
                result_metadata = result_metadata.all()
                for res_met in result_metadata:
                    db.session.delete(res_met)
                    db.session.commit()
                # insert metadata
                for metadata in temp_metadata:
                    temp = metadata
                    temp['mapset_id'] = dataset['id']
                    # print(temp)
                    res, res_metadata = MAPSET_METADATA_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_insert_history(self, res, mapset, category):
        ''' Doc: function route_insert_history  '''
        if res and mapset['is_deleted'] == False:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'mapset'
            json_history['type_id'] = mapset['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = category
            result, history = HISTORY_CONTROLLER.create(json_history)

    def route_insert_notification(self, res, json_request, mapset):
        """Doc: function route_insert_notification"""
        if res and "is_validate" in json_request and mapset["is_deleted"] == False:
            # get jwt
            jwt = HELPER.read_jwt()

            # default data
            send_notif = False
            notification = None
            json_notification = {}
            json_notification["feature"] = 2
            json_notification["type"] = "mapset"
            json_notification["type_id"] = mapset["id"]
            json_notification["sender"] = jwt["id"]
            json_notification["title"] = mapset["name"]
            json_notification["is_read"] = False
            json_notification["cdate"] = HELPER.local_date_server()
            json_notification["mdate"] = HELPER.local_date_server()

            # default get user
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)

            # jika walidata
            if jwt["role"]["name"] == "walidata":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["title"] = "Dataset " + mapset["name"]
                    json_notification[
                        "content"
                    ] = " yang ditambahkan Walidata membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == mapset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "title"
                    ] = "Walidata melakukan publikasi terhadap dataset "
                    json_notification["content"] = (
                        "<b>" + mapset["name"] + "</b> dari Organisasi Anda."
                    )
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # jika opd
            elif jwt["role"]["name"] == "opd":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == mapset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == mapset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " sudah diverifikasi dan dipublikasikan."
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # send notif
            if send_notif:
                data_user = data_user.all()
                for dr_ in data_user:
                    temp = dr_._asdict()
                    user_id = temp["id"]
                    json_notification["receiver"] = user_id
                    result, notification = NOTIFICATION_CONTROLLER.create(json_notification)
                    self.route_notification_count(send_notif, user_id, notification)

    def route_insert_notification_update(self, res, json_request, mapset, param_from):
        """Doc: function route_insert_notification_update"""
        if res and "is_validate" in json_request and mapset["is_deleted"] == False:
            # get jwt
            jwt = HELPER.read_jwt()

            # default data
            send_notif = False
            notification = None
            json_notification = {}
            json_notification["feature"] = 2
            json_notification["type"] = "mapset"
            json_notification["type_id"] = mapset["id"]
            json_notification["sender"] = jwt["id"]
            json_notification["title"] = mapset["name"]
            json_notification["is_read"] = False
            json_notification["cdate"] = HELPER.local_date_server()
            json_notification["mdate"] = HELPER.local_date_server()
            if param_from == "edit":
                json_notification["notes"] = "edit"
            else:
                json_notification["notes"] = "new"

            # default get user
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)

            # jika walidata
            if jwt["role"]["name"] == "walidata":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["title"] = "Dataset " + mapset["name"]
                    json_notification[
                        "content"
                    ] = " yang ditambahkan Walidata membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == mapset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "title"
                    ] = "Walidata melakukan publikasi terhadap dataset "
                    json_notification["content"] = (
                        "<b>" + mapset["name"] + "</b> dari Organisasi Anda."
                    )
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # jika opd
            elif jwt["role"]["name"] == "opd":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == mapset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == mapset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " sudah diverifikasi dan dipublikasikan."
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + mapset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # send notif
            if send_notif:
                data_user = data_user.all()
                for dr_ in data_user:
                    temp = dr_._asdict()
                    user_id = temp["id"]
                    json_notification["receiver"] = user_id
                    result, notification = NOTIFICATION_CONTROLLER.create(json_notification)
                    self.route_notification_count(send_notif, user_id, notification)

    def route_notification_count(self, send_notif, user_id, notification):
        """Doc: function route_insert_notification_count"""
        if send_notif == True:
            # update notif
            param = {}
            param["count_notif"] = 0
            res_, user = USER_CONTROLLER.get_by_id(user_id)
            if res_:
                if user["count_notif"] == None:
                    user["count_notif"] = 0
                param["count_notif"] = user["count_notif"] + 1
                # execute database
                result = db.session.query(UserModel)
                result = result.filter(getattr(UserModel, "id") == user_id)
                result = result.update(param, synchronize_session="fetch")
                result = db.session.commit()

            # try:
            #     socket_url = conf.SOCKET_URL
            #     token = request.headers.get("Authorization", None)
            #     req_ = requests.get(
            #         socket_url
            #         + "notification"
            #         + MESSAGE_RECEIVER
            #         + str(notification["receiver"]),
            #         headers={
            #             "Authorization": token,
            #             "Content-Type": conf.MESSAGE_APP_JSON,
            #         },
            #         data=json.dumps(notification),
            #     )
            #     res_ = req_.json()
            # except Exception as err:
            #     print(str(err))

    def get_highlight_arcgis(self, url):
        try:
            return_result = []

            urls = url.split('?')
            cont = True
            offset = 0
            columns = {}
            columns_alias = {}
            datas = []

            # get data
            while cont:
                data_url = urls[0] + '/query?where=1=1&outFields=*&returnGeometry=false&f=json&resultOffset=' + str(offset)
                req = requests.get(data_url, headers={"Content-Type": 'application/json'})
                res = req.json()

                if len(res['features']) == 0:
                    cont = False
                for feat in res['features']:
                    datas.append(feat['attributes'])

                columns = res['fieldAliases']
                offset += 1000

            # insight
            return_result.append({
                'title': 'Total Data',
                'value': len(datas)
            })

            # get alias
            for col in columns.keys():
                result_metadata = db.session.query(MapsetMetadataKugiModel)
                result_metadata = result_metadata.filter(func.lower(MapsetMetadataKugiModel.ptMemberName) == func.lower(col)).limit(1).all()
                if result_metadata:
                    for res_md in result_metadata:
                        temp = res_md.__dict__
                        columns_alias[col] = temp['ptDefinition']

            # data to pandas
            pandas_data = pd.DataFrame.from_dict(datas)

            # check group / kategori
            columns_group = []
            for col in columns.keys():
                tempcol = pd.unique(pandas_data[col])
                if len(tempcol) > 1 and len(tempcol) < len(pandas_data)/5:
                    columns_group.append(col)

            # get max & min in group / kategori
            for colname in columns_group:
                tempdata = pandas_data.groupby(colname).size().reset_index(name='count').sort_values(['count'], ascending=False, ignore_index=True)

                # alias group
                colalias = ''
                if colname in columns_alias:
                    colalias = f" / {columns_alias[colname]}"

                # max data
                if type(tempdata.loc[0, colname]) is str:
                    max = {
                        'title': f"Data paling banyak kategori '{colname}{colalias}'",
                        'value': f"Adalah '{str(tempdata.loc[0, colname])}', dengan jumlah : {str(tempdata.loc[0, 'count'])}",
                    }
                    return_result.append(max)

                # min data
                if type(tempdata.loc[len(tempdata)-1, colname]) is str:
                    min = {
                        'title': f"Data paling sedikit kategori '{colname}{colalias}'",
                        'value': f"Adalah '{str(tempdata.loc[len(tempdata)-1, colname])}', dengan jumlah : {str(tempdata.loc[len(tempdata)-1, 'count'])}",
                    }
                    return_result.append(min)

            return True, return_result

        except Exception as err:
            print(str(err))
            return False, []

    def get_highlight_geoserver(self, url):
        try:
            return_result = []

            url_parsed = urllib.parse.urlparse(url)
            url_only = url_parsed._replace(query=None).geturl()
            url_only_split = url_only.split('/')
            params = urllib.parse.parse_qs(url_parsed.query)

            data_url = url_only.replace('wms', 'ows')
            data_url += '?service=WFS'
            data_url += '&request=GetFeature'
            data_url += '&outputFormat=application/json'
            data_url += '&version=' + params['version'][0]
            data_url += '&typeName=' + params['layers'][0]

            columns = {}
            columns_alias = {}
            datas = []

            # get data
            req = requests.get(data_url)
            res = req.json()

            for feat in res['features']:
                datas.append(feat['properties'])

            if len(datas) > 0:
                for col in datas[0].keys():
                    columns[col] = ''

            # insight
            return_result.append({
                'title': 'Total Data',
                'value': len(datas),
            })

            # get alias
            for col in columns.keys():
                result_metadata = db.session.query(MapsetMetadataKugiModel)
                result_metadata = result_metadata.filter(func.lower(MapsetMetadataKugiModel.ptMemberName) == func.lower(col)).limit(1).all()
                if result_metadata:
                    for res_md in result_metadata:
                        temp = res_md.__dict__
                        columns_alias[col] = temp['ptDefinition']

            # data to pandas
            pandas_data = pd.DataFrame.from_dict(datas)

            # check group / kategori
            columns_group = []
            for col in columns.keys():
                tempcol = pd.unique(pandas_data[col])
                if len(tempcol) > 1 and len(tempcol) < len(pandas_data)/5:
                    columns_group.append(col)

            # get max & min in group / kategori
            for colname in columns_group:
                tempdata = pandas_data.groupby(colname).size().reset_index(name='count').sort_values(['count'], ascending=False, ignore_index=True)

                # alias group
                colalias = ''
                if colname in columns_alias:
                    colalias = f" / {columns_alias[colname]}"

                # max data
                if type(tempdata.loc[0, colname]) is str:
                    max = {
                        'title': f"Data paling banyak kategori '{colname}{colalias}'",
                        'value': f"Adalah '{str(tempdata.loc[0, colname])}', dengan jumlah : {str(tempdata.loc[0, 'count'])}",
                    }
                    return_result.append(max)

                # min data
                if type(tempdata.loc[len(tempdata)-1, colname]) is str:
                    min = {
                        'title': f"Data paling sedikit kategori '{colname}{colalias}'",
                        'value': f"Adalah '{str(tempdata.loc[len(tempdata)-1, colname])}', dengan jumlah : {str(tempdata.loc[len(tempdata)-1, 'count'])}",
                    }
                    return_result.append(min)

            return True, return_result

        except Exception as err:
            print(str(err))
            return False, []
