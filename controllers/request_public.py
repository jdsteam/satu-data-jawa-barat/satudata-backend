''' Doc: controller request public  '''
from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from exceptions import ErrorMessage
from models import RequestPublicModel, HistoryRequestPublicModel
from models import RoleModel
from models import UserModel
from models import DatasetModel
from controllers.notification import NotificationController
from controllers import UserController
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy
import sentry_sdk
import locale
import json
import os
import requests
from settings import configuration as conf
from dateutil import parser

USER_CONTROLLER = UserController()
NOTIFICATION_CONTROLLER = NotificationController()
CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long


class RequestPublicController(object):
    ''' Doc: class request public  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(RequestPublicModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(RequestPublicModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(RequestPublicModel, attr) == value)

            if search.lower() == 'persetujuan walidata':
                categorys = '1'
            elif search.lower() == 'dataset sedang diproses':
                categorys = '2'
            elif search.lower() == 'dataset tersedia':
                categorys = '4'
            elif search.lower() == 'dataset ditolak':
                categorys = '3'
            else:
                categorys = search

            result = result.filter(or_(
                cast(getattr(RequestPublicModel, "id"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "email"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "pekerjaan"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "bidang"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "judul_data"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "nama_skpd"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "kebutuhan_data"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "tujuan_data"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPublicModel, "status"),
                     sqlalchemy.String).ilike('%'+categorys+'%'),
            ))
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            if sort[0] != 'datetime':
                result = result.order_by(
                    text("request_public."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            request = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATETIME)})
                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate": temp['mdate'].strftime(FORMAT_DATETIME)})

                # get history_request_public
                temp['history'] = []
                history = db.session.query(HistoryRequestPublicModel)
                history = history.filter(
                    HistoryRequestPublicModel.request_public_id == temp['id'])
                history = history.order_by(
                    text("history_request_public.id asc"))
                history = history.all()
                for res_hist in history:
                    temp_history = res_hist.__dict__
                    temp_history.pop('_sa_instance_state', None)
                    temp_history.update({"datetime": temp_history['datetime'].strftime(FORMAT_DATETIME)})
                    if temp_history['odj_link']:
                        if '"[' in temp_history['odj_link']:
                            temp_history['odj_link'] = temp_history['odj_link'].strip('"')
                            temp_history['odj_link'] = temp_history['odj_link'].replace('\\', '')
                            temp_history.update({"odj_link":json.loads(temp_history['odj_link'])})
                        elif '["' in temp_history['odj_link']:
                            temp_history.update({"odj_link":json.loads(temp_history['odj_link'])})
                    temp['history'].append(temp_history)

                    # populate user
                    user = db.session.query(UserModel)
                    user = user.get(temp_history['cuid'])
                    if user:
                        user = user.__dict__
                        temp_history['username'] = user['username']
                        temp_history['name'] = user['name']
                    else:
                        temp_history['username'] = ''
                        temp_history['name'] = ''

                request.append(temp)

            # check if empty
            if request:
                return True, request
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id': _id}, '')
            result = result.all()

            # change into dict
            request = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate": temp['mdate'].strftime(FORMAT_DATETIME)})

                # get history_request_public
                temp['history'] = []
                history = db.session.query(HistoryRequestPublicModel)
                history = history.filter(
                    HistoryRequestPublicModel.request_public_id == temp['id'])
                history = history.order_by(
                    text("history_request_public.id asc"))
                history = history.all()
                for res_hist in history:
                    temp_history = res_hist.__dict__
                    temp_history.pop('_sa_instance_state', None)
                    temp_history.update({"datetime": temp_history['datetime'].strftime(FORMAT_DATETIME)})
                    if temp_history['odj_link']:
                        if '"[' in temp_history['odj_link']:
                            temp_history['odj_link'] = temp_history['odj_link'].strip('"')
                            temp_history['odj_link'] = temp_history['odj_link'].replace('\\', '')
                            temp_history.update({"odj_link":json.loads(temp_history['odj_link'])})
                        elif '["' in temp_history['odj_link']:
                            temp_history.update({"odj_link":json.loads(temp_history['odj_link'])})
                    temp['history'].append(temp_history)

                    # populate user
                    user = db.session.query(UserModel)
                    user = user.get(temp_history['cuid'])
                    if user:
                        user = user.__dict__
                        temp_history['username'] = user['username']
                        temp_history['name'] = user['name']
                    else:
                        temp_history['username'] = ''
                        temp_history['name'] = ''

                request.append(temp)

            # check if empty
            if request:
                return True, request[0], "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, _json, condition):
        ''' Doc: function post param  '''
        try:
            # generate json data
            json_send = {}
            json_send = _json
            if condition == 'add':
                json_send["cdate"] = HELPER.local_date_server()
            else:
                json_send["mdate"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # prepare data model
            param = self.post_param(_json, 'add')
            result = RequestPublicModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            param = self.post_param(_json, 'edit')

            try:
                # prepare data model
                result = db.session.query(RequestPublicModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(RequestPublicModel, attr) == value)

                # execute database
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()
                res, request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(RequestPublicModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(RequestPublicModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def send_mail(self, data):
        ''' Doc: function send mail  '''
        if os.getenv("SENTRY_ENV") == 'Staging':
            request_page_url = "https://opendata.ekosistemdatajabar.id/id/dataset/request"
        else:
            request_page_url = "https://opendata.jabarprov.go.id/id/dataset/request"

        intro_message = ''
        if data['status'] == 1:
            intro_message = '''
                <tr>
                    <td class="content px-0">
                        <div class="intro">
                            <p>Yth. <b>'''+data['nama']+''',</b></p>
                            <p>
                                Terima kasih telah menggunakan layanan Request Dataset di Open Data Jabar.<br>
                                Berikut adalah informasi mengenai pengajuan dataset dengan judul <b>'''+data['judul_data']+'''.</b>
                            </p>
                        </div>
                    </td>
                </tr>
            '''
        elif data['status'] == 2:
            intro_message = '''
                <tr>
                    <td class="content px-0">
                        <div class="intro">
                            <p>Yth. <b>'''+data['nama']+''',</b></p>
                            <p>
                                Terima kasih telah menggunakan layanan Request Dataset di Open Data Jabar.<br>
                                Berikut adalah informasi mengenai pengajuan dataset dengan judul <b>'''+data['judul_data']+'''.</b>
                            </p>
                        </div>
                    </td>
                </tr>
            '''
        elif data['status'] == 3:
            intro_message = '''
                <tr>
                    <td class="content px-0">
                        <div class="intro">
                            <p>Yth. <b>'''+data['nama']+''',</b></p>
                            <p>
                                Terima kasih telah menggunakan layanan Request Dataset di Open Data Jabar.<br>
                                Berikut adalah informasi mengenai pengajuan dataset dengan judul <b>'''+data['judul_data']+'''.</b>
                                <br><br>
                                Dengan berat hati kami mohon maaf karena permohonan dataset Anda <b>ditolak karena '''+data['history'][-1]['notes_type']+'''.</b>
                                Berikut adalah informasi mengenai permohonan Anda.
                            </p>
                        </div>
                    </td>
                </tr>
            '''
        elif data['status'] == 4:
            intro_message = '''
                <tr>
                    <td class="content px-0">
                        <div class="intro">
                            <p>Yth. <b>'''+data['nama']+''',</b></p>
                            <p>
                                Terima kasih telah menggunakan layanan Request Dataset di Open Data Jabar.<br>
                                Berikut adalah informasi mengenai pengajuan dataset dengan judul <b>'''+data['judul_data']+'''.</b>
                                <br><br>
                                Permohonan dataset Anda <b>Telah diakomodir</b> dan dapat diakses melalui link berikut beserta informasi mengenai permohonan Anda.
                                <br><br>
            '''
            if data['history'][-1]['dataset_source'] == 'Open Data Jabar':
                i = 0
                for link in data['history'][-1]['odj_link']:
                    links = link.split('/')
                    dataset = db.session.query(DatasetModel).filter(DatasetModel.title == links[-1]).limit(1).all()
                    datasets = {}
                    for res_data in dataset:
                        temp = res_data.__dict__
                        datasets = temp

                    if datasets:
                        i += 1
                        intro_message += 'Link dataset ' + str(i) + ' : <br>'
                        intro_message += '<a target="_blank" href="' + request_page_url + '/' + str(data['id']) + '?email=' + data['email'] + '&url=' + link + '">'
                        intro_message += datasets['name']
                        intro_message += '</a><br><br>'

                if data['history'][-1]['odj_notes']:
                    intro_message += '<span style="color: red;">Catatan : ' + data['history'][-1]['odj_notes'] + '</span>'

            elif data['history'][-1]['dataset_source'] == 'Jabar Drive':
                if data['history'][-1]['jd_link']:
                    intro_message += 'Link dataset : <br>'
                    intro_message += '<a target="_blank" href="' + request_page_url + '/' + str(data['id']) + '?email=' + data['email'] + '&url=' + data['history'][-1]['jd_link'] + '">'
                    intro_message += data['history'][-1]['jd_judul']
                    intro_message += '</a><br><br>'

                if data['history'][-1]['jd_notes']:
                    intro_message += '<span style="color: red;">Catatan : ' + data['history'][-1]['jd_notes'] + '</span>'

            elif data['history'][-1]['dataset_source'] == 'Sumber Lain':
                if data['history'][-1]['sl_notes']:
                    intro_message += 'Berikut informasi data : <br>'
                    intro_message += data['history'][-1]['sl_notes']

            intro_message += '''
                            </p>
                        </div>
                    </td>
                </tr>
            '''

        table_tracking = '''
            <table class="jds-table w-100">
                <thead>
                    <tr>
                        <th width="40%" class="tableHead">
                            TANGGAL
                        </th>
                        <th width="60%" class="tableHead">
                            STATUS
                        </th>
                    </tr>
                </thead>
                <tbody>
        '''
        for row in data['history']:
            str_date = parser.parse(row['datetime'])
            local_date = str_date.strftime("%d %B %Y, %H:%M")

            status = ''
            if row['status'] == 1:
                status = 'Menunggu persetujuan Walidata'
            elif row['status'] == 2:
                status = 'Request dataset sedang diproses'
            elif row['status'] == 3:
                status = 'Request dataset ditolak '
            elif row['status'] == 4:
                status = 'Request dataset telah diakomodir'

            notes = ''
            if row['notes'] is not None:
                notes = '<p>' + row['notes'] + '</p>'

            table_tracking += '''
                    <tr>
                        <td class="tableCell">
                            '''+local_date+'''
                        </td>
                        <td class="tableCell">
                            <b>''' + status + '''</b>''' + notes + '''
                        </td>
                    </tr>
            '''
        table_tracking += '''
                </tbody>
            </table>
        '''

        info = CONFIGURATION.MAIL_BODY_INFO

        try:
            html = CONFIGURATION.MAIL_BODY_HEAD_CSS + '''
                <body>
                    <!-- MAIN CONTAINER -->
                    <table class="no-border w-100" cellpadding="0" cellspacing="0">

                        <!-- LOGO -->
                        ''' + CONFIGURATION.MAIL_BODY_LOGO + '''

                        <!-- MAIN -->
                        <tr>
                            <td align="center">
                                <table class="white-container">
                                    <!-- Opening -->
                                    ''' + intro_message + '''

                                    <!-- Ticket -->
                                    <tr>
                                        <td class="content">
                                            <p class="ticket-title">NOMOR TIKET</p>
                                            <p class="ticket-no">'''+str(data['id'])+'''</p>
                                        </td>
                                    </tr>

                                    <!-- History Table -->
                                    <tr>
                                        <td class="content">
                                            ''' + table_tracking + '''
                                        </td>
                                    </tr>

                                    <!-- Feedback -->
                                    ''' + info + '''

                                    ''' + CONFIGURATION.MAIL_BODY_QNA + '''
                                </table>
                            </td>
                        </tr>

                        ''' + CONFIGURATION.MAIL_BODY_FOOTER + '''

                        ''' + CONFIGURATION.MAIL_BODY_COPYRIGHT + '''
                    </table>
                </body> ''' +  CONFIGURATION.MAIL_BODY_END

            # print(html)
            # normal send mail
            HELPER.send_email("Request Dataset Open Data Jabar", [data['email']], html)

            # using request service
            # data_mail = {
            #     "subject": "Request Dataset Open Data Jabar",
            #     "email": data['email'],
            #     "message": html
            # }
            # req_ = requests.post(
            #     conf.REQUEST_URL + 'mail',
            #     headers={"Content-Type": conf.MESSAGE_APP_JSON},
            #     data=json.dumps(data_mail)
            # )
            # resp_ = req_.json()['data']

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def insert_notification(self, request_public, request):
        ''' Doc: function insert_notification  '''
        # get user
        data_user = db.session.query(UserModel.id)
        data_user = data_user.join(
            RoleModel, RoleModel.id == UserModel.role_id)
        data_user = data_user.filter(RoleModel.name == 'walidata')
        data_user = data_user.all()

        for dr_ in data_user:
            temp = dr_._asdict()
            user_id = temp['id']
            json_notification = {}
            json_notification['feature'] = 4
            json_notification['type'] = 'request_public'
            json_notification['type_id'] = request_public['id']
            json_notification['sender'] = None
            json_notification['receiver'] = user_id
            json_notification['title'] = "Terdapat permohonan dataset "
            json_notification['content'] = "<b>" + \
                request_public['judul_data'] + "</b> dari " + \
                "Open Data Jabar."
            json_notification['is_read'] = False
            json_notification['cdate'] = HELPER.local_date_server()
            json_notification['mdate'] = HELPER.local_date_server()
            result, notification = NOTIFICATION_CONTROLLER.create(
                json_notification)

            # update notif
            param = {}
            param['count_notif'] = 0
            res_, user = USER_CONTROLLER.get_by_id(user_id)
            if res_:
                if not user['count_notif']:
                    user['count_notif'] = 0
                param['count_notif'] = user['count_notif'] + 1
                # execute database
                result = db.session.query(UserModel)
                result = result.filter(getattr(UserModel, 'id') == user_id)
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()

            # try:
            #     socket_url = conf.SOCKET_URL
            #     token = request.headers.get('Authorization', None)
            #     requests.get(
            #         socket_url + 'notification' + '?receiver=' +
            #         str(notification['receiver']) + '&from=opendata',
            #         headers={"Authorization": token,
            #                  "Content-Type": "application/json"},
            #         data=json.dumps(notification)
            #     )
            # except Exception as err:
            #     print(err)

    def check_recaptcha(self, json_data: dict):
        try:
            secretkey = conf.RECAPTCHA_OPENDATA_SECRETKEY

            if 'recaptcha' in json_data:
                url = "https://www.google.com/recaptcha/api/siteverify"
                payload = {
                    'secret': secretkey,
                    'response': json_data['recaptcha']
                }
                headers = {}

                request = requests.post(url, headers=headers, data=payload)
                response = request.json()

                if 'success' in response:
                    if response['success'] == True :
                        return True, 'Response reCaptcha ok'
                    else:
                        return False, 'Response reCaptcha failed, ' + ' '.join(response['error-codes'])
                else:
                    return False, 'Response reCaptcha error.'
            else:
                return False, 'reCaptcha token not found in body.'
        except Exception as err:
            print(str(err))
            return False, str(err)
