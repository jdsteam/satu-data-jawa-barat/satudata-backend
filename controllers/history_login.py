''' Doc: controller history login  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryLoginModel, UserModel, SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=singleton-comparison, unused-variable, unused-argument
class HistoryLoginController(object):
    ''' Doc: class history login  '''

    def __init__(self, **kwargs):
        ''' Doc: class history login  '''

    def query(self, where: dict, search, start_date, end_date):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                HistoryLoginModel.id, HistoryLoginModel.user_id, HistoryLoginModel.datetime,
                UserModel.kode_skpd, UserModel.username, SkpdModel.nama_skpd
            )
            result = result.join(UserModel, HistoryLoginModel.user_id == UserModel.id)
            result = result.join(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryLoginModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryLoginModel, attr) == value)

            if start_date and end_date:
                result = result.filter(text("history_login.datetime::date >='" + start_date + "'"))
                result = result.filter(text("history_login.datetime::date <='" + end_date + "'"))

            result = result.filter(or_(
                cast(getattr(HistoryLoginModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+search+'%')
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip, start_date, end_date):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.order_by(text("history_login."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_login = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                history_login.append(temp)

            # check if empty
            if history_login:
                return True, history_login
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search, start_date, end_date):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id':_id}, '', '', '')
            result = result.all()

            # change into dict
            history_login = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                history_login.append(temp)

            # check if empty
            if history_login:
                return True, history_login, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            json_send["datetime"] = HELPER.local_date_server()
            # prepare data model
            result = HistoryLoginModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history_login, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, history_login
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            json_send["datetime"] = HELPER.local_date_server()

            try:
                # prepare data model
                result = db.session.query(HistoryLoginModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryLoginModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history_login, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history_login
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryLoginModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryLoginModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history_login, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
