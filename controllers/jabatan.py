''' Doc: controller jabatan  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import JabatanModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable, singleton-comparison
class JabatanController(object):
    ''' Doc: controller jabatan  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(JabatanModel)
            for attr, value in where.items():
                result = result.filter(getattr(JabatanModel, attr) == value)
            result = result.filter(or_(cast(getattr(JabatanModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in JabatanModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            jabatan = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)
                jabatan.append(res)

            # check if empty
            jabatan = list(jabatan)
            if jabatan:
                return True, jabatan
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(JabatanModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(JabatanModel, attr) == value)
            result = result.filter(or_(cast(getattr(JabatanModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in JabatanModel.__table__.columns))
            result = result.count()

            # change into dict
            jabatan = {}
            jabatan['count'] = result

            # check if empty
            if jabatan:
                return True, jabatan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(JabatanModel).get(_id)
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            jabatan = result

            # check if empty
            if jabatan:
                return True, jabatan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
