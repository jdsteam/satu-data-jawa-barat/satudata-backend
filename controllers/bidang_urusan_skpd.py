''' Doc: controller bidang_urusan_skpd  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import BidangUrusanSkpdModel, BidangUrusanModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
import helpers.postgre_psycopg as db2

CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class BidangUrusanSkpdController(object):
    ''' Doc: class bidang_urusan_skpd  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip,tahun):
        ''' Doc: function get all  '''
        try:
        # query postgresql
            jwt = HELPER.read_jwt()
            where_tahun = ""
            parameters = {}
            if tahun:
                where_tahun = " AND ewalidata_data.tahun = %(tahun)s "
                parameters['tahun'] = tahun

            sql = f'''SELECT DISTINCT bidang_urusan.*, skpd.nama_skpd,
                    (SELECT count(*) FROM ewalidata_data
                    INNER JOIN bidang_urusan_indikator ON bidang_urusan_indikator.kode_indikator=ewalidata_data.kode_indikator
                    WHERE bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                    AND bidang_urusan_indikator.status = 'AKTIF' {where_tahun})::float
                    /
                    NULLIF((SELECT count(*) FROM bidang_urusan_indikator
                    WHERE kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                    AND status = 'AKTIF'), 0)::float * 100 AS progress,
                    (SELECT max(mdate) FROM ewalidata_data
                    INNER JOIN bidang_urusan_indikator ON bidang_urusan_indikator.kode_indikator = ewalidata_data.kode_indikator
                    WHERE bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan) AS last_update
                FROM bidang_urusan
                LEFT JOIN bidang_urusan_skpd ON bidang_urusan_skpd.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                LEFT JOIN skpd ON skpd.kode_skpd = bidang_urusan_skpd.kode_skpd
                WHERE 1 = 1 '''

            sql += '''AND bidang_urusan_skpd.kode_skpd = %(skpd)s '''
            parameters['skpd'] = jwt['kode_skpd']

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += '''AND (LOWER(bidang_urusan.nama_bidang_urusan) LIKE :search OR LOWER(skpd.nama_skpd) LIKE %(search)s) '''
                parameters['search'] = f"%{search.lower()}%"

            if sort:
                sql += f'''ORDER BY {str(sort[0])} {str(sort[1])} '''

            sql += '''OFFSET %(skip)s LIMIT %(limit)s '''
            parameters['skip'] = skip
            parameters['limit'] = limit

            # execute the query with bind parameters
            success, result = db2.query_dict_array(sql, parameters)

            if success:
                # change into dict
                bidang_urusan = []
                for res in result:
                    temp = {}
                    temp['kode_bidang_urusan'] = res['kode_bidang_urusan']
                    temp['nama_bidang_urusan'] = res['nama_bidang_urusan']
                    temp['nama_skpd'] = res['nama_skpd']
                    temp['notes'] = res['notes']
                    if res['progress']:
                        temp['progress'] = round(res['progress'], 2)
                    else:
                        temp['progress'] = 0
                    temp['nama_skpd'] = res['nama_skpd']
                    if res['cdate']:
                        temp['cdate'] = res['cdate'].strftime(DATETIME_FORMAT)
                    if res['mdate']:
                        temp['mdate'] = res['mdate'].strftime(DATETIME_FORMAT)
                    if res['last_update']:
                        temp['last_update'] = res['last_update'].strftime(DATETIME_FORMAT)
                    bidang_urusan.append(temp)

                # check if empty
                bidang_urusan = list(bidang_urusan)
                if bidang_urusan:
                    return True, bidang_urusan
                else:
                    return False, []
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            jwt = HELPER.read_jwt()
            jwt = HELPER.read_jwt()
            parameters = {}
            sql = f'''SELECT count(distinct bidang_urusan.kode_bidang_urusan)
                FROM bidang_urusan
                LEFT JOIN bidang_urusan_skpd ON bidang_urusan_skpd.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                LEFT JOIN skpd ON skpd.kode_skpd = bidang_urusan_skpd.kode_skpd
                WHERE 1 = 1 '''

            sql += '''AND bidang_urusan_skpd.kode_skpd = %(skpd)s '''
            parameters['skpd'] = jwt['kode_skpd']

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += '''AND (LOWER(bidang_urusan.nama_bidang_urusan) LIKE :search OR LOWER(skpd.nama_skpd) LIKE %(search)s) '''
                parameters['search'] = f"%{search.lower()}%"

            # execute the query with bind parameters
            success, result = db2.query_dict_single(sql, parameters)

            # change into dict
            datas = {}
            datas['count'] = result['count']

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(BidangUrusanSkpdModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['cdate']:
                    result['cdate'] = result['cdate'].strftime(DATETIME_FORMAT)
            else:
                result = {}

            # change into dict
            datas = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']
            is_exist = self.check_data_exist(json_send["kode_skpd"],json_send["kode_bidang_urusan"])
            if is_exist:
                return False, {}, "Data already exist."

            # prepare data model
            result = BidangUrusanSkpdModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, datas = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, datas, "Create data successfully."
            else:
                return False, {}, "Failed to assign data."

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {}, err)

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(BidangUrusanSkpdModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanSkpdModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, datas = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, datas
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(BidangUrusanSkpdModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanSkpdModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, datas = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def check_data_exist(self, _kode_skpd,_kode_bidang_urusan):
        ''' Doc: function check data exist '''
        try:
            # execute database
            result = db.session.query(BidangUrusanSkpdModel)
            result = result.filter(BidangUrusanSkpdModel.kode_bidang_urusan == _kode_bidang_urusan)
            result = result.filter(BidangUrusanSkpdModel.kode_skpd == _kode_skpd)
            result = result.count()
            if result == 0: return False
            else: return True

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def assign(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']
            is_exist = self.check_data_exist(json_send["kode_skpd"],json_send["kode_bidang_urusan"])
            if is_exist:
                return False, {}, "Data already exist."

            # prepare data model
            result = BidangUrusanSkpdModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, datas = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, datas, "Create data successfully."
            else:
                return False, {}, "Failed to assign data."

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {}, err)
