""" Doc: controller dataset  """
from settings.configuration import Configuration
from settings import configuration as conf
from helpers import Helper
from exceptions import ErrorMessage
from models import DatasetModel
from models import StrukturOrganisasiModel
from models import DatasetViewModel
from models import DatasetViewAllModel
from models import DatasetTypeModel
from models import DatasetClassModel
from models import DatasetTagModel
from models import AppModel
from models import AppServiceModel
from models import SektoralModel
from models import RegionalModel
from models import LicenseModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from models import ReviewModel
from models import HistoryModel
from models import HistoryDraftModel
from models import HighlightModel
from models import UserModel
from models import UserPermissionModel
from models import RoleModel
from models import MetadataModel
from models import DatasetPermissionModel
from models import DatasetQualityResultModel
from helpers.postgre_alchemy import postgre_alchemy as db
# import helpers.postgre_psycopg as db2
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import func
from sqlalchemy import cast
from sqlalchemy import and_
import sqlalchemy
import requests
import pandas as pd
import re
import json
import sentry_sdk
import datetime
from flask import request
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from string import punctuation
from helpers import decorator
from helpers.meilisearch import AdvanceSearch
import uuid
import helpers.postgre_psycopg as db2

# from threading import Thread

from controllers.dataset_tag import DatasetTagController
from controllers.metadata import MetadataController
from controllers.history import HistoryController
from controllers.history_draft import HistoryDraftController
from controllers.user import UserController
from controllers.notification import NotificationController
from controllers.bigdata import BigdataController
from apscheduler.schedulers.background import BackgroundScheduler
import math

DATASET_TAG_CONTROLLER = DatasetTagController()
METADATA_CONTROLLER = MetadataController()
HISTORY_CONTROLLER = HistoryController()
USER_CONTROLLER = UserController()
NOTIFICATION_CONTROLLER = NotificationController()
HISTORY_DRAFT_CONTROLLER = HistoryDraftController()
BIGDATA_CONTROLLER = BigdataController()

CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
MESSAGE_RECEIVER = "?receiver="
MESSAGE_MEMBUTUHKAN_VERIFIKASI = "</b> membutuhkan verifikasi."
MESSAGE_DARI = " dari <b>"

meilisearch = AdvanceSearch()
scheduler = BackgroundScheduler()
scheduler.add_job(
    func=meilisearch.schedule_update_settings, trigger="interval", seconds=60
)
scheduler.start()
namespace = uuid.UUID("6ba7b810-9dad-11d1-80b4-00c04fd430c8")


# pylint: disable=line-too-long, too-many-lines, singleton-comparison, unused-variable, broad-except, unused-argument, too-many-function-args


class DatasetController(object):
    """Doc: controller dataset"""

    def __init__(self, **kwargs):
        """Doc: function init"""

    # def refresh_suggestion_dataset(self):
    #     try:
    #         sql_query = "REFRESH MATERIALIZED VIEW suggestion_dataset"
    #         sql = text(sql_query)
    #         db.engine.execute(sql)
    #     except Exception as err:
    #         sentry_sdk.capture_exception(err)
    #         raise ErrorMessage(str(err), 500, 1, {})

    # def refresh_dataset_view_all(self):
    #     try:
    #         sql_query = "REFRESH MATERIALIZED VIEW dataset_view_all"
    #         sql = text(sql_query)
    #         db.engine.execute(sql)
    #     except Exception as err:
    #         sentry_sdk.capture_exception(err)
    #         raise ErrorMessage(str(err), 500, 1, {})

    def query(self, where: dict, search):
        """Doc: function query"""
        try:
            # query postgresql
            result = db.session.query(
                DatasetViewAllModel.id,
                DatasetViewAllModel.dataset_type_id,
                DatasetViewAllModel.dataset_class_id,
                DatasetViewAllModel.app_id,
                DatasetViewAllModel.app_service_id,
                DatasetViewAllModel.sektoral_id,
                DatasetViewAllModel.regional_id,
                DatasetViewAllModel.license_id,
                DatasetViewAllModel.kode_skpd,
                DatasetViewAllModel.kode_skpdsub,
                DatasetViewAllModel.kode_skpdunit,
                DatasetViewAllModel.name,
                DatasetViewAllModel.title,
                DatasetViewAllModel.year,
                DatasetViewAllModel.image,
                DatasetViewAllModel.description,
                DatasetViewAllModel.owner,
                DatasetViewAllModel.owner_email,
                DatasetViewAllModel.maintainer,
                DatasetViewAllModel.maintainer_email,
                DatasetViewAllModel.notes,
                DatasetViewAllModel.count_column,
                DatasetViewAllModel.count_row,
                DatasetViewAllModel.count_rating,
                DatasetViewAllModel.count_view,
                DatasetViewAllModel.count_access,
                DatasetViewAllModel.count_share_fb,
                DatasetViewAllModel.count_share_tw,
                DatasetViewAllModel.count_share_wa,
                DatasetViewAllModel.count_share_link,
                DatasetViewAllModel.count_download_xls,
                DatasetViewAllModel.count_download_csv,
                DatasetViewAllModel.count_download_api,
                DatasetViewAllModel.count_download_xls_filter,
                DatasetViewAllModel.count_download_csv_filter,
                DatasetViewAllModel.count_download_copy,
                DatasetViewAllModel.count_view_private,
                DatasetViewAllModel.count_access_private,
                DatasetViewAllModel.count_download_xls_private,
                DatasetViewAllModel.count_download_csv_private,
                DatasetViewAllModel.count_download_api_private,
                DatasetViewAllModel.is_active,
                DatasetViewAllModel.is_deleted,
                DatasetViewAllModel.is_validate,
                DatasetViewAllModel.is_permanent,
                DatasetViewAllModel.is_realtime,
                DatasetViewAllModel.is_discontinue,
                DatasetViewAllModel.cdate,
                DatasetViewAllModel.cuid,
                DatasetViewAllModel.mdate,
                DatasetViewAllModel.muid,
                DatasetViewAllModel.datetime,
                DatasetTypeModel.name.label("dataset_type_name"),
                DatasetTypeModel.notes.label("dataset_type_notes"),
                DatasetClassModel.name.label("dataset_class_name"),
                DatasetClassModel.notes.label("dataset_class_notes"),
                # AppModel.name.label("app_name"),
                # AppModel.code.label("app_code"),
                # AppModel.url.label("app_url"),
                # AppModel.kode_skpd.label("app_kode_skpd"),
                # AppModel.notes.label("app_notes"),
                # AppModel.is_active.label("app_is_active"),
                # AppModel.is_deleted.label("app_is_deleted"),
                # AppServiceModel.app_id.label("app_service_app_id"),
                # AppServiceModel.dataset_class_id.label("app_service_dataset_class_id"),
                # AppServiceModel.controller.label("app_service_controller"),
                # AppServiceModel.action.label("app_service_action"),
                # AppServiceModel.notes.label("app_service_notes"),
                # AppServiceModel.is_active.label("app_service_is_active"),
                # AppServiceModel.is_deleted.label("app_service_is_deleted"),
                # AppServiceModel.is_backend.label("app_service_is_backend"),
                SektoralModel.name.label("sektoral_name"),
                SektoralModel.notes.label("sektoral_notes"),
                SektoralModel.picture.label("sektoral_picture"),
                SektoralModel.picture_thumbnail.label("sektoral_picture_thumbnail"),
                SektoralModel.is_opendata.label("sektoral_is_opendata"),
                SektoralModel.is_satudata.label("sektoral_is_satudata"),
                SektoralModel.picture_thumbnail_1.label("sektoral_picture_thumbnail_1"),
                SektoralModel.picture_thumbnail_2.label("sektoral_picture_thumbnail_2"),
                SektoralModel.picture_thumbnail_3.label("sektoral_picture_thumbnail_3"),
                SektoralModel.picture_thumbnail_4.label("sektoral_picture_thumbnail_4"),
                SektoralModel.picture_thumbnail_5.label("sektoral_picture_thumbnail_5"),
                SektoralModel.picture_banner_1.label("sektoral_picture_banner_1"),
                SektoralModel.picture_banner_2.label("sektoral_picture_banner_2"),
                SektoralModel.picture_banner_3.label("sektoral_picture_banner_3"),
                SektoralModel.picture_banner_4.label("sektoral_picture_banner_4"),
                SektoralModel.picture_banner_5.label("sektoral_picture_banner_5"),
                LicenseModel.name.label("license_name"),
                LicenseModel.notes.label("license_notes"),
                RegionalModel.regional_level_id.label("regional_regional_level_id"),
                RegionalModel.kode_bps.label("regional_kode_bps"),
                RegionalModel.kode_kemendagri.label("regional_kode_kemendagri"),
                RegionalModel.nama_bps.label("regional_nama_bps"),
                RegionalModel.nama_kemendagri.label("regional_nama_kemendagri"),
                RegionalModel.notes.label("regional_notes"),
                SkpdModel.nama_skpd.label("skpd_nama_skpd"),
                SkpdModel.nama_skpd_alias.label("skpd_nama_skpd_alias"),
                SkpdModel.title.label("skpd_title"),
                SkpdModel.logo.label("skpd_logo"),
                SkpdModel.count_dataset.label("skpd_count_dataset"),
                SkpdModel.count_indikator.label("skpd_count_indikator"),
                SkpdModel.count_visualization.label("skpd_count_visualization"),
                SkpdModel.description.label("skpd_description"),
                SkpdModel.address.label("skpd_address"),
                SkpdModel.phone.label("skpd_phone"),
                SkpdModel.email.label("skpd_email"),
                SkpdModel.media_website.label("skpd_media_website"),
                SkpdSubModel.nama_skpdsub.label("skpdsub_nama_skpdsub"),
                SkpdUnitModel.nama_skpdunit.label("skpdunit_nama_skpdunit"),
                DatasetViewAllModel.schema,
                DatasetViewAllModel.table,
                DatasetViewAllModel.json,
                UserModel.username,
                UserModel.name.label("user_name"),
                DatasetViewAllModel.category,
                DatasetViewAllModel.period,
                DatasetViewAllModel.kode_bidang_urusan,
                DatasetViewAllModel.kode_indikator,
            )
            result = result.join(
                SkpdModel,
                DatasetViewAllModel.kode_skpd == SkpdModel.kode_skpd,
                isouter=True,
            )
            result = result.join(
                SkpdSubModel,
                DatasetViewAllModel.kode_skpdsub == SkpdSubModel.kode_skpdsub,
                isouter=True,
            )
            result = result.join(
                SkpdUnitModel,
                DatasetViewAllModel.kode_skpdunit == SkpdUnitModel.kode_skpdunit,
                isouter=True,
            )
            result = result.join(
                SektoralModel,
                DatasetViewAllModel.sektoral_id == SektoralModel.id,
                isouter=True,
            )
            result = result.join(
                DatasetTypeModel,
                DatasetViewAllModel.dataset_type_id == DatasetTypeModel.id,
                isouter=True,
            )
            result = result.join(
                DatasetClassModel,
                DatasetViewAllModel.dataset_class_id == DatasetClassModel.id,
                isouter=True,
            )
            # result = result.join(
            #     AppModel, DatasetViewAllModel.app_id == AppModel.id, isouter=True
            # )
            # result = result.join(
            #     AppServiceModel,
            #     DatasetViewAllModel.app_service_id == AppServiceModel.id,
            #     isouter=True,
            # )
            result = result.join(
                LicenseModel,
                DatasetViewAllModel.license_id == LicenseModel.id,
                isouter=True,
            )
            result = result.join(
                RegionalModel,
                DatasetViewAllModel.regional_id == RegionalModel.id,
                isouter=True,
            )
            result = result.join(
                UserModel,
                DatasetViewAllModel.cuid == UserModel.id,
                isouter=True
            )
            # result = result.filter(DatasetViewAllModel.kode_skpd == SkpdModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(DatasetViewAllModel, attr) == val for val in value)
                    )
                else:
                    result = result.filter(getattr(DatasetViewAllModel, attr) == value)
            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(DatasetViewAllModel.dataset_class_id == 3)
                result = result.filter(DatasetViewAllModel.is_deleted == False)
                result = result.filter(DatasetViewAllModel.is_active == True)
            if search:
                search = search.split(" ")
                for src in search:
                    result = result.filter(
                        or_(
                            # cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(SektoralModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(DatasetViewAllModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(DatasetViewAllModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                            cast(
                                getattr(SkpdModel, "nama_skpd"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(SektoralModel, "name"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(DatasetViewAllModel, "name"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(DatasetViewAllModel, "name"), sqlalchemy.String
                            ).match(src),
                        )
                    )
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query_table(self, where: dict, search):
        """Doc: function query_table"""
        try:
            # query postgresql
            result = db.session.query(
                DatasetViewModel.id,
                DatasetViewModel.dataset_type_id,
                DatasetViewModel.dataset_class_id,
                DatasetViewModel.app_id,
                DatasetViewModel.app_service_id,
                DatasetViewModel.sektoral_id,
                DatasetViewModel.regional_id,
                DatasetViewModel.license_id,
                DatasetViewModel.kode_skpd,
                DatasetViewModel.kode_skpdsub,
                DatasetViewModel.kode_skpdunit,
                DatasetViewModel.name,
                DatasetViewModel.title,
                DatasetViewModel.year,
                DatasetViewModel.image,
                DatasetViewModel.description,
                DatasetViewModel.owner,
                DatasetViewModel.owner_email,
                DatasetViewModel.maintainer,
                DatasetViewModel.maintainer_email,
                DatasetViewModel.notes,
                DatasetViewModel.count_column,
                DatasetViewModel.count_row,
                DatasetViewModel.count_rating,
                DatasetViewModel.count_view,
                DatasetViewModel.count_access,
                DatasetViewModel.count_share_fb,
                DatasetViewModel.count_share_tw,
                DatasetViewModel.count_share_wa,
                DatasetViewModel.count_share_link,
                DatasetViewModel.count_download_xls,
                DatasetViewModel.count_download_csv,
                DatasetViewModel.count_download_api,
                DatasetViewModel.count_download_xls_filter,
                DatasetViewModel.count_download_csv_filter,
                DatasetViewModel.count_download_copy,
                DatasetViewModel.count_view_private,
                DatasetViewModel.count_access_private,
                DatasetViewModel.count_download_xls_private,
                DatasetViewModel.count_download_csv_private,
                DatasetViewModel.count_download_api_private,
                DatasetViewModel.is_active,
                DatasetViewModel.is_deleted,
                DatasetViewModel.is_validate,
                DatasetViewModel.is_permanent,
                DatasetViewModel.is_realtime,
                DatasetViewModel.is_discontinue,
                DatasetViewModel.cdate,
                DatasetViewModel.cuid,
                DatasetViewModel.mdate,
                DatasetViewModel.muid,
                DatasetViewModel.datetime,
                DatasetTypeModel.name.label("dataset_type_name"),
                DatasetTypeModel.notes.label("dataset_type_notes"),
                DatasetClassModel.name.label("dataset_class_name"),
                DatasetClassModel.notes.label("dataset_class_notes"),
                # AppModel.name.label("app_name"),
                # AppModel.code.label("app_code"),
                # AppModel.url.label("app_url"),
                # AppModel.kode_skpd.label("app_kode_skpd"),
                # AppModel.notes.label("app_notes"),
                # AppModel.is_active.label("app_is_active"),
                # AppModel.is_deleted.label("app_is_deleted"),
                # AppServiceModel.app_id.label("app_service_app_id"),
                # AppServiceModel.dataset_class_id.label("app_service_dataset_class_id"),
                # AppServiceModel.controller.label("app_service_controller"),
                # AppServiceModel.action.label("app_service_action"),
                # AppServiceModel.notes.label("app_service_notes"),
                # AppServiceModel.is_active.label("app_service_is_active"),
                # AppServiceModel.is_deleted.label("app_service_is_deleted"),
                # AppServiceModel.is_backend.label("app_service_is_backend"),
                SektoralModel.name.label("sektoral_name"),
                SektoralModel.notes.label("sektoral_notes"),
                SektoralModel.picture.label("sektoral_picture"),
                SektoralModel.picture_thumbnail.label("sektoral_picture_thumbnail"),
                SektoralModel.is_opendata.label("sektoral_is_opendata"),
                SektoralModel.is_satudata.label("sektoral_is_satudata"),
                SektoralModel.picture_thumbnail_1.label("sektoral_picture_thumbnail_1"),
                SektoralModel.picture_thumbnail_2.label("sektoral_picture_thumbnail_2"),
                SektoralModel.picture_thumbnail_3.label("sektoral_picture_thumbnail_3"),
                SektoralModel.picture_thumbnail_4.label("sektoral_picture_thumbnail_4"),
                SektoralModel.picture_thumbnail_5.label("sektoral_picture_thumbnail_5"),
                SektoralModel.picture_banner_1.label("sektoral_picture_banner_1"),
                SektoralModel.picture_banner_2.label("sektoral_picture_banner_2"),
                SektoralModel.picture_banner_3.label("sektoral_picture_banner_3"),
                SektoralModel.picture_banner_4.label("sektoral_picture_banner_4"),
                SektoralModel.picture_banner_5.label("sektoral_picture_banner_5"),
                LicenseModel.name.label("license_name"),
                LicenseModel.notes.label("license_notes"),
                RegionalModel.regional_level_id.label("regional_regional_level_id"),
                RegionalModel.kode_bps.label("regional_kode_bps"),
                RegionalModel.kode_kemendagri.label("regional_kode_kemendagri"),
                RegionalModel.nama_bps.label("regional_nama_bps"),
                RegionalModel.nama_kemendagri.label("regional_nama_kemendagri"),
                RegionalModel.notes.label("regional_notes"),
                SkpdModel.nama_skpd.label("skpd_nama_skpd"),
                SkpdModel.nama_skpd_alias.label("skpd_nama_skpd_alias"),
                SkpdModel.title.label("skpd_title"),
                SkpdModel.logo.label("skpd_logo"),
                SkpdModel.count_dataset.label("skpd_count_dataset"),
                SkpdModel.count_indikator.label("skpd_count_indikator"),
                SkpdModel.count_visualization.label("skpd_count_visualization"),
                SkpdModel.description.label("skpd_description"),
                SkpdModel.address.label("skpd_address"),
                SkpdModel.phone.label("skpd_phone"),
                SkpdModel.email.label("skpd_email"),
                SkpdModel.media_website.label("skpd_media_website"),
                SkpdSubModel.nama_skpdsub.label("skpdsub_nama_skpdsub"),
                SkpdUnitModel.nama_skpdunit.label("skpdunit_nama_skpdunit"),
                DatasetViewModel.schema,
                DatasetViewModel.table,
                DatasetViewModel.json,
                UserModel.username,
                UserModel.name.label("user_name"),
                DatasetViewModel.category,
                DatasetViewModel.period,
                DatasetViewModel.kode_bidang_urusan,
                DatasetViewModel.kode_indikator,
            )
            result = result.join(
                SkpdModel,
                DatasetViewModel.kode_skpd == SkpdModel.kode_skpd,
                isouter=True,
            )
            result = result.join(
                SkpdSubModel,
                DatasetViewModel.kode_skpdsub == SkpdSubModel.kode_skpdsub,
                isouter=True,
            )
            result = result.join(
                SkpdUnitModel,
                DatasetViewModel.kode_skpdunit == SkpdUnitModel.kode_skpdunit,
                isouter=True,
            )
            result = result.join(
                SektoralModel,
                DatasetViewModel.sektoral_id == SektoralModel.id,
                isouter=True,
            )
            result = result.join(
                DatasetTypeModel,
                DatasetViewModel.dataset_type_id == DatasetTypeModel.id,
                isouter=True,
            )
            result = result.join(
                DatasetClassModel,
                DatasetViewModel.dataset_class_id == DatasetClassModel.id,
                isouter=True,
            )
            # result = result.join(
            #     AppModel,
            #     DatasetViewModel.app_id == AppModel.id,
            #     isouter=True
            # )
            # result = result.join(
            #     AppServiceModel,
            #     DatasetViewModel.app_service_id == AppServiceModel.id,
            #     isouter=True,
            # )
            result = result.join(
                LicenseModel,
                DatasetViewModel.license_id == LicenseModel.id,
                isouter=True,
            )
            result = result.join(
                RegionalModel,
                DatasetViewModel.regional_id == RegionalModel.id,
                isouter=True,
            )
            result = result.join(
                UserModel,
                DatasetViewModel.cuid == UserModel.id,
                isouter=True
            )
            # result = result.filter(DatasetViewModel.kode_skpd == SkpdModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(DatasetViewModel, attr) == val for val in value)
                    )
                else:
                    result = result.filter(getattr(DatasetViewModel, attr) == value)
            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(DatasetViewModel.dataset_class_id == 3)
                result = result.filter(DatasetViewModel.is_deleted == False)
                result = result.filter(DatasetViewModel.is_active == True)
            if search:
                search = search.split(" ")
                for src in search:
                    result = result.filter(
                        or_(
                            # cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(SektoralModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(DatasetViewModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                            # cast(getattr(DatasetViewModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                            cast(
                                getattr(SkpdModel, "nama_skpd"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(SektoralModel, "name"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(DatasetViewModel, "name"), sqlalchemy.String
                            ).match(src),
                            cast(
                                getattr(DatasetViewModel, "name"), sqlalchemy.String
                            ).match(src),
                        )
                    )
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_data(self, temp, single):
        """Doc: function populate data"""
        try:
            temp["cdate"] = temp["cdate"].strftime(DATETIME_FORMAT)
            temp["mdate"] = temp["mdate"].strftime(DATETIME_FORMAT)
            if temp["period"] is not None:
                temp["period"] = temp["period"].strftime(DATETIME_FORMAT)
            if temp["datetime"] is not None:
                temp["datetime"] = temp["datetime"].strftime(DATETIME_FORMAT)
            else:
                temp["datetime"] = ""

            # get relation to dataset_type
            temp["dataset_type"] = {}
            temp["dataset_type"]["id"] = temp["dataset_type_id"]
            temp["dataset_type"]["name"] = temp["dataset_type_name"]
            temp["dataset_type"]["notes"] = temp["dataset_type_notes"]
            temp.pop("dataset_type_id", None)
            temp.pop("dataset_type_name", None)
            temp.pop("dataset_type_notes", None)

            # get relation to dataset_class
            temp["dataset_class"] = {}
            temp["dataset_class"]["id"] = temp["dataset_class_id"]
            temp["dataset_class"]["name"] = temp["dataset_class_name"]
            temp["dataset_class"]["notes"] = temp["dataset_class_notes"]
            temp.pop("dataset_class_id", None)
            temp.pop("dataset_class_name", None)
            temp.pop("dataset_class_notes", None)

            # get relation to app
            # temp = self.populate_relation_app(temp)

            # get relation to app_service
            # temp = self.populate_relation_app_service(temp)

            # get can_access_dataset
            # if temp['app']['id'] and temp['app_service']['id']:
            #     temp['can_access'], temp['can_access_info'] = HELPER.can_access_dataset(temp['app'], temp['app_service'])
            # else:
            #     temp['can_access'] = False
            #     temp['can_access_info'] = {'data': {}, 'error': 1, 'message': 'App and App Service Not Found'}

            # get can_access_dataset
            token = request.headers.get("Authorization", None)
            if temp["schema"] and temp["table"]:
                (
                    temp["can_access_new"],
                    temp["can_access_new_info"],
                ) = decorator.check_bigdata(
                    "bigdata", temp["schema"], temp["table"], token
                )
            else:
                temp["can_access_new"] = False
                temp["can_access_new_info"] = {
                    "data": {},
                    "error": 1,
                    "message": "Schema and Table not found.",
                }

            # get relation to sektoral
            temp["sektoral"] = {}
            temp["sektoral"]["id"] = temp["sektoral_id"]
            temp["sektoral"]["name"] = temp["sektoral_name"]
            temp["sektoral"]["notes"] = temp["sektoral_notes"]
            temp["sektoral"]["is_opendata"] = temp["sektoral_is_opendata"]
            temp["sektoral"]["is_satudata"] = temp["sektoral_is_satudata"]
            temp["sektoral"]["picture"] = temp["sektoral_picture"]
            temp["sektoral"]["picture_thumbnail"] = temp["sektoral_picture_thumbnail"]
            temp["sektoral"]["picture_thumbnail_1"] = temp[
                "sektoral_picture_thumbnail_1"
            ]
            temp["sektoral"]["picture_thumbnail_2"] = temp[
                "sektoral_picture_thumbnail_2"
            ]
            temp["sektoral"]["picture_thumbnail_3"] = temp[
                "sektoral_picture_thumbnail_3"
            ]
            temp["sektoral"]["picture_thumbnail_4"] = temp[
                "sektoral_picture_thumbnail_4"
            ]
            temp["sektoral"]["picture_thumbnail_5"] = temp[
                "sektoral_picture_thumbnail_5"
            ]
            temp["sektoral"]["picture_banner_1"] = temp["sektoral_picture_banner_1"]
            temp["sektoral"]["picture_banner_2"] = temp["sektoral_picture_banner_2"]
            temp["sektoral"]["picture_banner_3"] = temp["sektoral_picture_banner_3"]
            temp["sektoral"]["picture_banner_4"] = temp["sektoral_picture_banner_4"]
            temp["sektoral"]["picture_banner_5"] = temp["sektoral_picture_banner_5"]
            temp.pop("sektoral_id", None)
            temp.pop("sektoral_name", None)
            temp.pop("sektoral_notes", None)
            temp.pop("sektoral_is_opendata", None)
            temp.pop("sektoral_is_satudata", None)
            temp.pop("sektoral_picture", None)
            temp.pop("sektoral_picture_thumbnail", None)
            temp.pop("sektoral_picture_thumbnail_1", None)
            temp.pop("sektoral_picture_thumbnail_2", None)
            temp.pop("sektoral_picture_thumbnail_3", None)
            temp.pop("sektoral_picture_thumbnail_4", None)
            temp.pop("sektoral_picture_thumbnail_5", None)
            temp.pop("sektoral_picture_banner_1", None)
            temp.pop("sektoral_picture_banner_2", None)
            temp.pop("sektoral_picture_banner_3", None)
            temp.pop("sektoral_picture_banner_4", None)
            temp.pop("sektoral_picture_banner_5", None)

            # get relation to license
            temp["license"] = {}
            temp["license"]["id"] = temp["license_id"]
            temp["license"]["name"] = temp["license_name"]
            temp["license"]["notes"] = temp["license_notes"]
            temp.pop("license_id", None)
            temp.pop("license_name", None)
            temp.pop("license_notes", None)

            # get relation to regional
            temp["regional"] = {}
            temp["regional"]["id"] = temp["regional_id"]
            temp["regional"]["regional_level_id"] = temp["regional_regional_level_id"]
            temp["regional"]["kode_bps"] = temp["regional_kode_bps"]
            temp["regional"]["kode_kemendagri"] = temp["regional_kode_kemendagri"]
            temp["regional"]["nama_bps"] = temp["regional_nama_bps"]
            temp["regional"]["nama_kemendagri"] = temp["regional_nama_kemendagri"]
            temp["regional"]["notes"] = temp["regional_notes"]
            temp.pop("regional_id", None)
            temp.pop("regional_regional_level_id", None)
            temp.pop("regional_kode_bps", None)
            temp.pop("regional_kode_kemendagri", None)
            temp.pop("regional_nama_bps", None)
            temp.pop("regional_nama_kemendagri", None)
            temp.pop("regional_notes", None)

            # get relation to dataset_tag
            dataset_tag = db.session.query(
                func.distinct(func.lower(DatasetTagModel.tag)).label("tag")
            )
            dataset_tag = dataset_tag.filter(
                getattr(DatasetTagModel, "dataset_id") == temp["id"]
            )
            dataset_tag = dataset_tag.all()

            dataset_tags = []
            dataset_tagss = []
            for tag in dataset_tag:
                temp_tag = {}
                temp_tag["tag"] = tag[0]
                if single:
                    dataset_tags.append(tag[0])
                else:
                    dataset_tags.append(temp_tag)
                dataset_tagss.append(tag[0])

            temp["dataset_tag"] = dataset_tags
            temp["dataset_tags"] = dataset_tagss
            temp["dataset_tag"] = sorted(temp["dataset_tag"], key=len)
            temp["dataset_tags"] = sorted(temp["dataset_tags"], key=len)

            # get relation to skpd
            temp["skpd"] = {}
            temp["skpd"]["kode_skpd"] = temp["kode_skpd"]
            temp["skpd"]["nama_skpd"] = temp["skpd_nama_skpd"]
            temp["skpd"]["nama_skpd_alias"] = temp["skpd_nama_skpd_alias"]
            temp["skpd"]["title"] = temp["skpd_title"]
            temp["skpd"]["logo"] = temp["skpd_logo"]
            temp["skpd"]["count_dataset"] = temp["skpd_count_dataset"]
            temp["skpd"]["count_indikator"] = temp["skpd_count_indikator"]
            temp["skpd"]["count_visualization"] = temp["skpd_count_visualization"]
            temp["skpd"]["description"] = temp["skpd_description"]
            temp["skpd"]["address"] = temp["skpd_address"]
            temp["skpd"]["phone"] = temp["skpd_phone"]
            temp["skpd"]["email"] = temp["skpd_email"]
            temp["skpd"]["media_website"] = temp["skpd_media_website"]
            temp.pop("kode_skpd", None)
            temp.pop("skpd_nama_skpd", None)
            temp.pop("skpd_nama_skpd_alias", None)
            temp.pop("skpd_title", None)
            temp.pop("skpd_logo", None)
            temp.pop("skpd_count_dataset", None)
            temp.pop("skpd_count_indikator", None)
            temp.pop("skpd_count_visualization", None)
            temp.pop("skpd_description", None)
            temp.pop("skpd_address", None)
            temp.pop("skpd_phone", None)
            temp.pop("skpd_email", None)
            temp.pop("skpd_media_website", None)

            # get relation to skpdsub
            temp["skpdsub"] = {}
            temp["skpdsub"]["kode_skpdsub"] = temp["kode_skpdsub"]
            temp["skpdsub"]["nama_skpdsub"] = temp["skpdsub_nama_skpdsub"]
            temp.pop("kode_skpdsub", None)
            temp.pop("skpdsub_nama_skpdsub", None)

            # get relation to skpdunit
            temp["skpdunit"] = {}
            temp["skpdunit"]["kode_skpdunit"] = temp["kode_skpdunit"]
            temp["skpdunit"]["nama_skpdunit"] = temp["skpdunit_nama_skpdunit"]
            temp.pop("kode_skpdunit", None)
            temp.pop("skpdunit_nama_skpdunit", None)

            # get picture
            rand_code = (temp["id"] % 5) + 1
            rand_thumb = "picture_thumbnail_" + str(rand_code)
            rand_banner = "picture_banner_" + str(rand_code)
            temp["picture_thumbnail"] = temp["sektoral"][rand_thumb]
            temp["picture_banner"] = temp["sektoral"][rand_banner]

            # get relation to history_draft
            temp = self.populate_relation_history_draft(temp)

            if temp["schema"] and temp["table"]:
                temp["bigdata_url"] = "/bigdata/" + temp["schema"] + "/" + temp["table"]
            else:
                temp["bigdata_url"] = "/bigdata/"

            # get relation to dataset_permission
            temp = self.populate_dataset_permission(temp)

            # get relation to metadata
            temp = self.populate_metadata(temp)

            temp["quality_score_point"] = 0
            temp["quality_score_label"] = "Belum ada score"
            jwt = HELPER.read_jwt()
            if jwt:
                # get metadata
                res, result_metadata, result_data = BIGDATA_CONTROLLER.get_dashboard_insight(temp["schema"], temp["table"])

                # get quality score
                quality_result = self.get_quality_score(temp["id"])
                if quality_result:
                    if quality_result["final_result"]:
                        temp["quality_score_point"] = round(quality_result["final_result"], 1)
                        if quality_result["final_result"] >= 90:
                            temp["quality_score_label"] = "Sangat Baik"
                        elif quality_result["final_result"] >= 75:
                            temp["quality_score_label"] = "Baik"
                        elif quality_result["final_result"] >= 60:
                            temp["quality_score_label"] = "Cukup"
                        else:
                            temp["quality_score_label"] = "Perlu Diperbaiki"

                if single:
                    temp["quality_score"] = {}
                    temp["quality_score"]["success"] = "False"
                    temp["quality_score"]["message"] = ""
                    temp["quality_score"]["result"] = {}
                    temp["quality_score"]["result"]["final"] = None
                    temp["quality_score"]["result"]["conformity"] = None
                    temp["quality_score"]["result"]["uniqueness"] = None
                    temp["quality_score"]["result"]["consistency"] = None
                    temp["quality_score"]["result"]["timeliness"] = None
                    temp["quality_score"]["result"]["completeness"] = None
                    temp["quality_score"]["conformity"] = {}
                    temp["quality_score"]["conformity"]["result"] = None
                    temp["quality_score"]["conformity"]["explain"] = {}
                    temp["quality_score"]["conformity"]["code_area"] = {}
                    temp["quality_score"]["conformity"]["measurement"] = {}
                    temp["quality_score"]["conformity"]["serving_rate"] = {}
                    temp["quality_score"]["conformity"]["scope"] = {}
                    temp["quality_score"]["uniqueness"] = {}
                    temp["quality_score"]["uniqueness"]["result"] = None
                    temp["quality_score"]["uniqueness"]["duplication"] = {}
                    temp["quality_score"]["consistency"] = {}
                    temp["quality_score"]["consistency"]["result"] = None
                    temp["quality_score"]["consistency"]["unit"] = {}
                    temp["quality_score"]["consistency"]["listing_province"] = {}
                    temp["quality_score"]["consistency"]["time_series"] = {}
                    temp["quality_score"]["timeliness"] = {}
                    temp["quality_score"]["timeliness"]["result"] = None
                    temp["quality_score"]["timeliness"]["year"] = []
                    temp["quality_score"]["completeness"] = {}
                    temp["quality_score"]["completeness"]["result"] = None
                    temp["quality_score"]["completeness"]["filled"] = {}

                    if quality_result:
                        temp["quality_score"]["success"] = quality_result["is_config_complete"]
                        temp["quality_score"]["message"] = quality_result["error"]

                        if quality_result["final_result"]:
                            temp["quality_score"]["result"]["final"] = round(quality_result["final_result"], 1)
                            temp["quality_score"]["result"]["conformity"] = round(quality_result["conformity_result"], 1)
                            temp["quality_score"]["result"]["conformity_weight"] = "30%"
                            temp["quality_score"]["result"]["uniqueness"] = round(quality_result["uniqueness_result"], 1)
                            temp["quality_score"]["result"]["uniqueness_weight"] = "25%"
                            temp["quality_score"]["result"]["consistency"] = round(quality_result["consistency_result"], 1)
                            temp["quality_score"]["result"]["completeness_weight"] = "10%"
                            temp["quality_score"]["result"]["timeliness"] = round(quality_result["timeliness_result"], 1)
                            temp["quality_score"]["result"]["consistency_weight"] = "25%"
                            temp["quality_score"]["result"]["completeness"] = round(quality_result["completeness_result"], 1)
                            temp["quality_score"]["result"]["timeliness_weight"] = "10%"
                            temp["quality_score"]["conformity"]["result"] = round(quality_result["conformity_result"], 1)
                            temp["quality_score"]["uniqueness"]["result"] = round(quality_result["uniqueness_result"], 1)
                            temp["quality_score"]["consistency"]["result"] = round(quality_result["consistency_result"], 1)
                            temp["quality_score"]["timeliness"]["result"] = round(quality_result["timeliness_result"], 1)
                            temp["quality_score"]["completeness"]["result"] = round(quality_result["completeness_result"], 1)

                            try:
                                temp["quality_score"]["conformity"]["explain"]["column_description"] = quality_result["conformity_explain_columns_total_valid"]
                                temp["quality_score"]["conformity"]["explain"]["column_valid"] = quality_result["conformity_explain_columns_total_columns"]
                                temp["quality_score"]["conformity"]["explain"]["result"] = round(quality_result["conformity_explain_columns_quality_result"]/ 100, 1)
                                temp["quality_score"]["conformity"]["explain"]["weight"] = "20%"
                                temp["quality_score"]["conformity"]["explain"]["result_weight"] = round(20 / 100 * quality_result["conformity_explain_columns_quality_result"], 1)
                                temp["quality_score"]["conformity"]["explain"]["warning"] = quality_result["conformity_explain_columns_warning"]
                            except Exception as err:
                                temp["quality_score"]["conformity"]["explain"]["result"] = 0

                            try:
                                temp["quality_score"]["conformity"]["code_area"]["result"] = round(quality_result["conformity_code_area_quality_result"] / 100, 1)
                                temp["quality_score"]["conformity"]["code_area"]["weight"] = "20%"
                                temp["quality_score"]["conformity"]["code_area"]["result_weight"] = round(20 / 100 * quality_result["conformity_code_area_quality_result"], 1)
                                temp["quality_score"]["conformity"]["code_area"]["warning"] = quality_result["conformity_code_area_warning"]
                            except Exception as err:
                                temp["quality_score"]["conformity"]["code_area"]["result"] = 0

                            try:
                                temp["quality_score"]["conformity"]["measurement"]["result"] = round(quality_result["conformity_measurement_quality_result"] / 100, 1)
                                temp["quality_score"]["conformity"]["measurement"]["weight"] = "20%"
                                temp["quality_score"]["conformity"]["measurement"]["result_weight"] = round(20 / 100 * quality_result["conformity_measurement_quality_result"], 1)
                                temp["quality_score"]["conformity"]["measurement"]["warning"] = quality_result["conformity_measurement_warning"]
                            except Exception as err:
                                temp["quality_score"]["conformity"]["measurement"]["result"] = 0

                            try:
                                temp["quality_score"]["conformity"]["serving_rate"]["result"] = round(quality_result["conformity_serving_rate_quality_result"] / 100, 1)
                                temp["quality_score"]["conformity"]["serving_rate"]["weight"] = "20%"
                                temp["quality_score"]["conformity"]["serving_rate"]["result_weight"] = round(20 / 100 * quality_result["conformity_serving_rate_quality_result"], 1)
                                temp["quality_score"]["conformity"]["serving_rate"]["warning"] = quality_result["conformity_serving_rate_warning"]
                            except Exception as err:
                                temp["quality_score"]["conformity"]["serving_rate"]["result"] = 0

                            try:
                                temp["quality_score"]["conformity"]["scope"]["result"] = round(quality_result["conformity_scope_quality_result"] / 100, 1)
                                temp["quality_score"]["conformity"]["scope"]["weight"] = "20%"
                                temp["quality_score"]["conformity"]["scope"]["result_weight"] = round(20 / 100 * quality_result["conformity_scope_quality_result"], 1)
                                temp["quality_score"]["conformity"]["scope"]["warning"] = quality_result["conformity_scope_warning"]
                            except Exception as err:
                                temp["quality_score"]["conformity"]["scope"]["result"] = 0

                            try:
                                temp["quality_score"]["uniqueness"]["duplication"]["result"] = round(quality_result["uniqueness_duplicated_quality_result"] / 100, 1)
                                temp["quality_score"]["uniqueness"]["duplication"]["weight"] = "100%"
                                temp["quality_score"]["uniqueness"]["duplication"]["result_weight"] = round(100 / 100* quality_result["uniqueness_duplicated_quality_result"], 1)
                                temp["quality_score"]["uniqueness"]["duplication"]["warning"] = quality_result["uniqueness_duplicated_warning"]
                            except Exception as err:
                                temp["quality_score"]["uniqueness"]["duplication"]["result"] = 0

                            try:
                                temp["quality_score"]["consistency"]["unit"]["result"] = round(quality_result["consistency_unit_quality_result"] / 100, 1)
                                temp["quality_score"]["consistency"]["unit"]["weight"] = "40%"
                                temp["quality_score"]["consistency"]["unit"]["result_weight"] = round(40 / 100 * quality_result["consistency_unit_quality_result"], 1)
                                temp["quality_score"]["consistency"]["unit"]["warning"] = quality_result["consistency_unit_warning"]
                            except Exception as err:
                                temp["quality_score"]["consistency"]["unit"]["result"] = 0

                            try:
                                temp["quality_score"]["consistency"]["listing_province"]["result"] = round(quality_result["consistency_listing_province_quality_result"] / 100, 1)
                                temp["quality_score"]["consistency"]["listing_province"]["weight"] = "40%"
                                temp["quality_score"]["consistency"]["listing_province"]["result_weight"] = round(40 / 100 * quality_result["consistency_listing_province_quality_result"], 1)
                                temp["quality_score"]["consistency"]["listing_province"]["warning"] = quality_result["consistency_listing_province_warning"]
                            except Exception as err:
                                temp["quality_score"]["consistency"]["listing_province"]["result"] = 0

                            try:
                                temp["quality_score"]["consistency"]["time_series"]["result"] = round(quality_result["consistency_time_series_quality_result"] / 100, 1)
                                temp["quality_score"]["consistency"]["time_series"]["weight"] = "20%"
                                temp["quality_score"]["consistency"]["time_series"]["result_weight"] = round(20 / 100 * quality_result["consistency_time_series_quality_result"], 1)
                                temp["quality_score"]["consistency"]["time_series"]["warning"] = quality_result["consistency_time_series_warning"]
                            except Exception as err:
                                temp["quality_score"]["consistency"]["time_series"]["result"] = 0

                            try:
                                year_arr = []
                                now = datetime.date.today().year - 1
                                for i in range(now - 4, now + 1):
                                    year_arr.append(str(i))

                                # if quality_result["timeliness_updated_warning"] == None:
                                #     year_warning = []
                                # elif (type(quality_result["timeliness_updated_warning"])== list):
                                #     year_warning = quality_result["timeliness_updated_warning"]
                                # else:
                                #     year_warning = quality_result["timeliness_updated_warning"].replace("{", "").replace("}", "").split(",")

                                if result_metadata:
                                    for year in year_arr:
                                        temp_year = {}
                                        temp_year["year"] = int(year)
                                        temp_year["weight"] = round(100 / 5, 1)
                                        temp_year["result"] = 0
                                        if 'years' in result_metadata['year']:
                                            if len(result_metadata['year']['years']) > 0:
                                                for yd in result_metadata['year']['years']:
                                                    if str(yd) == str(year):
                                                        temp_year["result"] = 1
                                        temp_year["result_weight"] = round(temp_year["result"] * temp_year["weight"], 1)
                                        temp["quality_score"]["timeliness"]["year"].append(temp_year)
                                temp["quality_score"]["timeliness"]["year"].sort(key=lambda x: x["year"])
                            except Exception as err:
                                temp["quality_score"]["timeliness"]["year"]["result"] = 0

                            try:
                                temp["quality_score"]["completeness"]["filled"]["cell_total"] = quality_result["completeness_filled_total_rows"]
                                temp["quality_score"]["completeness"]["filled"]["cell_valid"] = quality_result["completeness_filled_total_valid"]
                                temp["quality_score"]["completeness"]["filled"]["result"] = round(quality_result["completeness_filled_quality_result"]/ 100, 1)
                                temp["quality_score"]["completeness"]["filled"]["weight"] = "100%"
                                temp["quality_score"]["completeness"]["filled"]["result_weight"] = round(100 / 100 * quality_result["completeness_filled_quality_result"], 1)
                            except Exception as err:
                                temp["quality_score"]["completeness"]["filled"]["result"] = 0

            return temp
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_relation_history_draft(self, temp):
        """Doc: function populate relation history draft"""
        # get relation to history_draft
        if temp["is_validate"] is not None:
            # select last history
            try:
                history_draft = db.session.query(HistoryDraftModel)
                history_draft = history_draft.filter(
                    getattr(HistoryDraftModel, "type") == "dataset"
                )
                history_draft = history_draft.filter(
                    getattr(HistoryDraftModel, "type_id") == temp["id"]
                )
                if temp["is_validate"] == "1":
                    history_draft = history_draft.filter(
                        getattr(HistoryDraftModel, "category") == "draft"
                    )
                elif temp["is_validate"] == "2":
                    history_draft = history_draft.filter(
                        getattr(HistoryDraftModel, "category") == "revision"
                    )
                elif temp["is_validate"] == "3":
                    history_draft = history_draft.filter(
                        getattr(HistoryDraftModel, "category") == "approve"
                    )
                history_draft = history_draft.order_by(text("id desc"))
                history_draft = history_draft.limit(1)
                history_draft = history_draft.one()

                if history_draft:
                    history_draft = history_draft.__dict__
                    history_draft.pop("_sa_instance_state", None)
                    history_draft["datetime"] = history_draft["datetime"].strftime(
                        DATETIME_FORMAT
                    )
                else:
                    history_draft = {}
            except Exception:
                history_draft = {}

            # select history before
            if history_draft:
                try:
                    history_draft_before = db.session.query(HistoryDraftModel)
                    history_draft_before = history_draft_before.filter(
                        getattr(HistoryDraftModel, "type") == "dataset"
                    )
                    history_draft_before = history_draft_before.filter(
                        getattr(HistoryDraftModel, "type_id") == temp["id"]
                    )
                    history_draft_before = history_draft_before.order_by(text("id desc"))
                    history_draft_before = history_draft_before.offset(1)
                    history_draft_before = history_draft_before.limit(1)
                    history_draft_before = history_draft_before.one()

                    if history_draft_before:
                        history_draft_before = history_draft_before.__dict__
                        history_draft_before.pop("_sa_instance_state", None)
                        history_draft["category_before"] = history_draft_before['category']
                    else:
                        history_draft["category_before"] = ''
                except Exception:
                    history_draft["category_before"] = ''

            temp["history_draft"] = history_draft
        else:
            temp["history_draft"] = {}

        return temp

    def populate_dataset_permission(self, temp):
        """Doc: function populate relation dataset permission"""
        # get relation to history_draft
        try:
            dataset_permission = db.session.query(
                DatasetPermissionModel.id,
                StrukturOrganisasiModel.jabatan_id,
                StrukturOrganisasiModel.jabatan_nama,
                StrukturOrganisasiModel.kode_skpd,
                StrukturOrganisasiModel.satuan_kerja_id,
                StrukturOrganisasiModel.satuan_kerja_nama,
                StrukturOrganisasiModel.lv1_unit_kerja_id,
                StrukturOrganisasiModel.lv1_unit_kerja_nama,
                StrukturOrganisasiModel.lv2_unit_kerja_id,
                StrukturOrganisasiModel.lv2_unit_kerja_nama,
                StrukturOrganisasiModel.lv3_unit_kerja_id,
                StrukturOrganisasiModel.lv3_unit_kerja_nama,
                StrukturOrganisasiModel.lv4_unit_kerja_id,
                StrukturOrganisasiModel.lv4_unit_kerja_nama,
            )
            dataset_permission = dataset_permission.join(
                StrukturOrganisasiModel,
                StrukturOrganisasiModel.jabatan_id == DatasetPermissionModel.jabatan_id,
            )
            dataset_permission = dataset_permission.filter(
                getattr(DatasetPermissionModel, "dataset_id") == temp["id"]
            )
            dataset_permission = dataset_permission.all()

            if dataset_permission:
                temp["dataset_permission"] = []
                for dp_ in dataset_permission:
                    dp_ = dp_._asdict()
                    dp_.pop("_sa_instance_state", None)
                    temp["dataset_permission"].append(dp_)
            else:
                temp["dataset_permission"] = []
        except Exception:
            temp["dataset_permission"] = []

        return temp

    def populate_metadata(self, temp):
        """Doc: function populate relation dataset permission"""
        # get relation to history_draft
        try:
            res_metadata, data_metadata = METADATA_CONTROLLER.get_all(
                {"dataset_id": temp["id"]}, "", ["id", "asc"], 100, 0
            )
            if res_metadata:
                temp["metadata"] = data_metadata
            else:
                temp["metadata"] = []
        except Exception:
            temp["metadata"] = []

        return temp

    def get_all(self, where: dict, search, sort, limit, skip):
        """Doc: function get all"""
        try:
            if "tag" not in where:
                result = self.query(where, search)
                result = result.order_by(text("dataset_view_all.regional_id asc"))
                result = result.order_by(
                    text("dataset_view_all." + sort[0] + " " + sort[1])
                )
                result = result.offset(skip).limit(limit)
                result = result.all()

                # change into dict
                dataset = self.all_without_tag(result)

                # check if empty
                dataset = list(dataset)

            else:
                value = where["tag"]
                is_array = isinstance(value, list)
                # query postgresql
                result = db.session.query(DatasetTagModel)
                result = result.distinct(DatasetTagModel.dataset_id)
                if is_array:
                    result = result.filter(
                        or_(DatasetTagModel.tag.ilike("%" + val + "%") for val in value)
                    )
                else:
                    result = result.filter(DatasetTagModel.tag.ilike("%" + value + "%"))

                # change into dict
                dataset = self.all_with_tag(result, where, search, sort, limit, skip)

            if dataset:
                return True, dataset
            else:
                return False, []
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all_table(self, where: dict, search, sort, limit, skip):
        """Doc: function get all"""
        try:
            if "tag" not in where:
                result = self.query_table(where, search)
                result = result.order_by(text("dataset_view.regional_id asc"))
                result = result.order_by(
                    text("dataset_view." + sort[0] + " " + sort[1])
                )
                result = result.offset(skip).limit(limit)
                result = result.all()

                # change into dict
                dataset = self.all_without_tag_table(result)

                # check if empty
                dataset = list(dataset)

            else:
                value = where["tag"]
                is_array = isinstance(value, list)
                # query postgresql
                result = db.session.query(DatasetTagModel)
                result = result.distinct(DatasetTagModel.dataset_id)
                if is_array:
                    result = result.filter(
                        or_(DatasetTagModel.tag.ilike("%" + val + "%") for val in value)
                    )
                else:
                    result = result.filter(DatasetTagModel.tag.ilike("%" + value + "%"))

                # change into dict
                dataset = self.all_with_tag_table(
                    result, where, search, sort, limit, skip
                )

            if dataset:
                return True, dataset
            else:
                return False, []
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def all_without_tag(self, result):
        """Doc: function all_without_tag"""
        dataset = []
        for res in result:
            temp = res._asdict()
            temp = self.populate_data(temp, False)
            dataset.append(temp)

        return dataset

    def all_without_tag_table(self, result):
        """Doc: function all_without_tag_table"""
        dataset = []
        for res in result:
            temp = res._asdict()
            temp = self.populate_data(temp, False)
            dataset.append(temp)

        return dataset

    def all_with_tag(self, result, where, search, sort, limit, skip):
        """Doc: function all_without_tag"""
        datasets = []
        for res in result:
            temp = res.__dict__
            where.pop("tag", None)
            where["id"] = temp["dataset_id"]
            res, dataset = self.get_all(where, search, sort, limit, skip)

            # check if exist
            if res:
                datasets.append(dataset[0])

        return datasets

    def all_with_tag_table(self, result, where, search, sort, limit, skip):
        """Doc: function all_without_tag_table"""
        datasets = []
        for res in result:
            temp = res.__dict__
            where.pop("tag", None)
            where["id"] = temp["dataset_id"]
            res, dataset = self.get_all_table(where, search, sort, limit, skip)

            # check if exist
            if res:
                datasets.append(dataset[0])

        return datasets

    def get_count(self, where: dict, search):
        """Doc: function get count"""
        try:
            if "tag" not in where:
                dataset = self.count_without_tag(where, search)
            else:
                dataset = self.count_with_tag(where, search)

            # check if empty
            if dataset:
                return True, dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count_table(self, where: dict, search):
        """Doc: function get count"""
        try:
            if "tag" not in where:
                dataset = self.count_without_tag_table(where, search)
            else:
                dataset = self.count_with_tag_table(where, search)

            # check if empty
            if dataset:
                return True, dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def count_without_tag(self, where, search):
        """Doc: function count without tag"""
        result = self.query(where, search)
        result = result.count()

        # change into dict
        dataset = {}
        dataset["count"] = result

        return dataset

    def count_without_tag_table(self, where, search):
        """Doc: function count without tag"""
        result = self.query_table(where, search)
        result = result.count()

        # change into dict
        dataset = {}
        dataset["count"] = result

        return dataset

    def count_with_tag(self, where, search):
        """Doc: function count with tag"""
        value = where["tag"]
        is_array = isinstance(value, list)
        # query postgresql
        result = db.session.query(DatasetTagModel)
        result = result.distinct(DatasetTagModel.dataset_id)
        if is_array:
            result = result.filter(
                or_(DatasetTagModel.tag.ilike("%" + val + "%") for val in value)
            )
        else:
            result = result.filter(DatasetTagModel.tag.ilike("%" + value + "%"))

        # change into dict
        datasets_count = 0
        for res in result:
            temp = res.__dict__
            # temp.pop('_sa_instance_state', None)
            where.pop("tag", None)
            where["id"] = temp["dataset_id"]
            res, dataset = self.get_count(where, search)

            # check if exist
            if res:
                datasets_count = datasets_count + dataset["count"]
                # datasets.append(dataset[0])

        # change into dict
        dataset = {}
        dataset["count"] = datasets_count

        return dataset

    def count_with_tag_table(self, where, search):
        """Doc: function count with tag"""
        value = where["tag"]
        is_array = isinstance(value, list)
        # query postgresql
        result = db.session.query(DatasetTagModel)
        result = result.distinct(DatasetTagModel.dataset_id)
        if is_array:
            result = result.filter(
                or_(DatasetTagModel.tag.ilike("%" + val + "%") for val in value)
            )
        else:
            result = result.filter(DatasetTagModel.tag.ilike("%" + value + "%"))

        # change into dict
        datasets_count = 0
        for res in result:
            temp = res.__dict__
            # temp.pop('_sa_instance_state', None)
            where.pop("tag", None)
            where["id"] = temp["dataset_id"]
            res, dataset = self.get_count_table(where, search)

            # check if exist
            if res:
                datasets_count = datasets_count + dataset["count"]
                # datasets.append(dataset[0])

        # change into dict
        dataset = {}
        dataset["count"] = datasets_count

        return dataset

    def get_by_id(self, _id, where: dict):
        """Doc: function get by id"""
        # handle id=title, integer or string
        try:
            temp = int(_id)
            _id = temp
        except Exception:
            temp = _id
            result_dataset = db.session.query(DatasetViewAllModel.id)
            result_dataset = result_dataset.filter(DatasetViewAllModel.title == temp)
            result_dataset = result_dataset.all()
            if result_dataset:
                result_dataset = result_dataset[0]
                _id = result_dataset[0]
            else:
                _id = 0

        try:
            # execute database
            result = self.query({"id": _id}, "")
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(DatasetViewAllModel, attr) == val for val in value)
                    )
                else:
                    result = result.filter(getattr(DatasetViewAllModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            if jwt:
                result = result.all()
            else:
                result = result.filter(DatasetViewAllModel.dataset_class_id == 3)
                result = result.filter(DatasetViewAllModel.id == _id)
                result = result.filter(DatasetViewAllModel.is_deleted == False)
                result = result.filter(DatasetViewAllModel.is_active == True)
                result = result.all()

            if result:
                result = result[0]._asdict()
                result = self.populate_data(result, True)

            else:
                result = {}

            # change into dict
            dataset = result

            # check if empty
            if dataset:
                return True, dataset, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id_table(self, _id, where: dict):
        """Doc: function get by id"""
        # handle id=title, integer or string
        try:
            temp = int(_id)
            _id = temp
        except Exception:
            temp = _id
            result_dataset = db.session.query(DatasetViewModel.id)
            result_dataset = result_dataset.filter(DatasetViewModel.title == temp)
            result_dataset = result_dataset.all()
            if result_dataset:
                result_dataset = result_dataset[0]
                _id = result_dataset[0]
            else:
                _id = 0

        try:
            # execute database
            result = self.query_table({"id": _id}, "")
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(DatasetViewModel, attr) == val for val in value)
                    )
                else:
                    result = result.filter(getattr(DatasetViewModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            if jwt:
                result = result.all()
            else:
                result = result.filter(DatasetViewModel.dataset_class_id == 3)
                result = result.filter(DatasetViewModel.id == _id)
                result = result.filter(DatasetViewModel.is_deleted == False)
                result = result.filter(DatasetViewModel.is_active == True)
                result = result.all()

            if result:
                result = result[0]._asdict()
                result = self.populate_data(result, True)
            else:
                result = {}

            # change into dict
            dataset = result

            # check if empty
            if dataset:
                return True, dataset, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def review(self, _id):
        """Doc: function review"""
        try:
            jwt = HELPER.read_jwt()

            result = db.session.query(DatasetModel)
            result = result.options(
                defer("cuid"), defer("muid"), defer("cdate"), defer("mdate")
            )
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop("_sa_instance_state", None)

                if result["kode_skpd"] == jwt["kode_skpd"]:
                    return (
                        False,
                        {},
                        "Anda tidak dapat memberi ulasan pada dataset Anda sendiri.",
                    )

                else:
                    result_history = db.session.query(HistoryModel)
                    result_history = result_history.filter(
                        getattr(HistoryModel, "type") == "dataset"
                    )
                    result_history = result_history.filter(
                        getattr(HistoryModel, "type_id") == _id
                    )
                    result_history = result_history.filter(
                        getattr(HistoryModel, "category") == "access"
                    )
                    result_history = result_history.filter(
                        getattr(HistoryModel, "user_id") == jwt["id"]
                    )
                    result_history = result_history.count()

                    if result_history < 1:
                        return (
                            False,
                            {},
                            "Anda tidak dapat memberi ulasan, anda belum melakukan akses / download pada dataset ini.",
                        )

                    else:
                        result_review = db.session.query(ReviewModel)
                        result_review = result_review.filter(
                            getattr(ReviewModel, "type") == "dataset"
                        )
                        result_review = result_review.filter(
                            getattr(ReviewModel, "type_id") == _id
                        )
                        result_review = result_review.filter(
                            getattr(ReviewModel, "user_id") == jwt["id"]
                        )
                        result_review = result_review.count()

                        if result_review >= 1:
                            return (
                                False,
                                {},
                                "Anda tidak dapat memberi ulasan, anda sudah melakukan ulasan pada dataset ini.",
                            )

                        else:
                            return True, {}, "Silakan memberi ulasan pada dataset ini."

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        """Doc: function create"""
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            # title
            json_send["title"] = json_send["name"].lower().replace(" ", "-")
            json_send["title"] = re.sub("[^a-zA-Z0-9-]", "", json_send["title"])
            res_count = db.session.query(DatasetModel.id)
            res_count = res_count.filter(DatasetModel.title.ilike("%" + json_send["title"] + "%"))
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + "-" + str(res_count + 1)

            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt["id"]
            json_send["count_column"] = 0
            json_send["count_row"] = 0
            json_send["count_rating"] = "0.0"
            json_send["count_view"] = 0
            json_send["count_access"] = 0
            json_send["count_share_fb"] = 0
            json_send["count_share_tw"] = 0
            json_send["count_share_wa"] = 0
            json_send["count_share_link"] = 0
            json_send["count_download_xls"] = 0
            json_send["count_download_csv"] = 0
            json_send["count_download_api"] = 0
            json_send["count_download_xls_filter"] = 0
            json_send["count_download_csv_filter"] = 0
            json_send["count_download_copy"] = 0
            json_send["count_view_private"] = 0
            json_send["count_access_private"] = 0
            json_send["count_download_xls_private"] = 0
            json_send["count_download_csv_private"] = 0
            json_send["count_download_api_private"] = 0

            # prepare data model
            result = DatasetModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, dataset, message = self.get_by_id_table(result["id"], {})

            # update materialized view
            # thread1 = Thread(target=self.refresh_suggestion_dataset)
            # thread1.daemon = True
            # thread1.start()
            # thread2 = Thread(target=self.refresh_dataset_view_all)
            # thread2.daemon = True
            # thread2.start()

            # check if exist
            if res:
                return True, dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        """Doc: function update"""
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = self.update_json_send(where, _json, jwt)

            try:
                # prepare data model
                result = db.session.query(DatasetModel)
                for attr, value in where.items():
                    result = result.filter(getattr(DatasetModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session="fetch")
                result = db.session.commit()
                res, dataset, message = self.get_by_id_table(where["id"], {})

                # generate dataset recommendation
                # if "is_validate" in json_send:
                #     if json_send["is_validate"] == 3:
                #         self.generate_dataset_recomendation()

                # update materialized view
                # thread1 = Thread(target=self.refresh_suggestion_dataset)
                # thread1.daemon = True
                # thread1.start()
                # thread2 = Thread(target=self.refresh_dataset_view_all)
                # thread2.daemon = True
                # thread2.start()

                # check if empty
                if res:
                    return True, dataset
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_json_send(self, where, _json, jwt):
        """Doc: function update_json_send"""
        json_send = {}
        json_send = _json
        if (
            "count_rating" in _json
            or "count_view" in _json
            or "count_access" in _json
            or "count_share_fb" in _json
            or "count_share_tw" in _json
            or "count_share_wa" in _json
            or "count_share_link" in _json
            or "count_download_xls" in _json
            or "count_download_csv" in _json
            or "count_download_api" in _json
            or "count_download_xls_filter" in _json
            or "count_download_csv_filter" in _json
            or "count_download_copy" in _json
            or "count_view_private" in _json
            or "count_access_private" in _json
            or "count_download_xls_private" in _json
            or "count_download_csv_private" in _json
            or "count_download_api_private" in _json
        ):
            json_send = _json
        else:
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt["id"]
            if "name" in json_send:
                json_send["title"] = json_send["name"].lower().replace(" ", "-")
                json_send["title"] = re.sub("[^a-zA-Z0-9-]", "", json_send["title"])

        return json_send

    def delete(self, where: dict):
        """Doc: function delete"""
        try:
            try:
                # prepare data model
                result = db.session.query(DatasetModel)
                for attr, value in where.items():
                    result = result.filter(getattr(DatasetModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, dataset, message = self.get_by_id_table(where["id"], {})

                # update materialized view
                # thread1 = Thread(target=self.refresh_suggestion_dataset)
                # thread1.daemon = True
                # thread1.start()
                # thread2 = Thread(target=self.refresh_dataset_view_all)
                # thread2.daemon = True
                # thread2.start()

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def count_rating(self, dataset_id):
        """Doc: function count rating"""
        try:
            # query postgresql
            result = db.session.query(ReviewModel)
            result = result.join(DatasetModel, DatasetModel.id == ReviewModel.type_id)
            result = result.filter(getattr(DatasetModel, "is_deleted") == False)
            result = result.filter(getattr(ReviewModel, "type_id") == dataset_id)
            result = result.all()
            total_rating = 0
            # change into dict
            for res in result:
                temp = res.__dict__
                total_rating = total_rating + int(temp["rating"])

            if total_rating > 0:
                count_rating = total_rating / len(result)
            else:
                count_rating = 0

            where = {"id": str(dataset_id)}
            payload = {"count_rating": str(round(count_rating, 1))}
            rating = self.update(where, payload)

            # check if empty
            if rating:
                return True, rating
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_alls(self, where: dict, search, sort, limit, skip):
        """Doc: function get alls"""
        try:
            # query exclude
            result_exclude = db.session.query(HighlightModel.category_id)
            result_exclude = result_exclude.filter(
                getattr(HighlightModel, "category") == "dataset"
            )
            result_exclude = result_exclude.all()

            # query postgresql
            result = db.session.query(DatasetModel.id, DatasetModel.name, DatasetModel.title)

            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(DatasetModel, attr) == val for val in value)
                    )
                else:
                    result = result.filter(getattr(DatasetModel, attr) == value)
            # where exclude
            for row_exclude in result_exclude:
                result = result.filter(getattr(DatasetModel, "id") != row_exclude[0])

            # sort
            result = result.order_by(text(sort[0] + " " + sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            dataset = []
            for res in result:
                temp = res._asdict()
                temp.pop("_sa_instance_state", None)

                dataset.append(temp)

            # check if empty
            dataset = list(dataset)
            if dataset:
                return True, dataset
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_opendata_recomendation(self, _id):
        """Doc: function opendata recomendation"""

        # handle id=title, integer or string
        try:
            temp = int(_id)
            _id = temp
        except Exception:
            temp = _id
            result = db.session.query(DatasetViewAllModel.id)
            result = result.filter(DatasetViewAllModel.title == temp)
            result = result.all()
            if result:
                result = result[0]
                _id = result[0]
            else:
                _id = 0

        # get recomendation
        try:
            numRecommendation = 5

            cosine_sim_df = pd.read_pickle("static/model/dataset_recommendation.pkl")

            index = (
                cosine_sim_df.loc[:, _id]
                .to_numpy()
                .argpartition(range(-1, -numRecommendation, -1))
            )

            # Mengambil data dengan similarity terbesar dari index yang ada
            closest = cosine_sim_df.columns[index[-1 : -(numRecommendation + 2) : -1]]

            # Drop nama_dataset agar nama dataset yang dicari tidak muncul dalam daftar rekomendasi
            closest = closest.drop(_id, errors="ignore")

            # get detail data
            result_recomendation = []
            for rec in closest:
                res, data, msg = self.get_by_id(int(rec), {})
                if res:
                    result_recomendation.append(data)

            # get list
            result_recomendation = list(result_recomendation)

            if result_recomendation:
                return True, result_recomendation
            else:
                return False, []

        except Exception as err:
            # fail response
            print("error recomendation")
            print(err)
            return False, []

    def generate_dataset_recomendation(self):
        # get data
        dataset = db.session.query(DatasetModel)
        dataset = dataset.filter(DatasetModel.dataset_class_id == "3")
        dataset = dataset.filter(DatasetModel.is_active == True)
        dataset = dataset.filter(DatasetModel.is_deleted == False)
        dataset = dataset.filter(DatasetModel.is_validate == "3")
        dataset = dataset.all()

        sektoral = db.session.query(SektoralModel)
        sektoral = sektoral.filter(SektoralModel.is_deleted == False)
        sektoral = sektoral.filter(SektoralModel.is_opendata == True)
        sektoral = sektoral.all()

        skpd = db.session.query(SkpdModel)
        skpd = skpd.filter(SkpdModel.is_deleted == False)
        skpd = skpd.filter(SkpdModel.is_opendata == True)
        skpd = skpd.all()

        # convert to array
        datasets = []
        for res1 in dataset:
            temp1 = res1.__dict__
            temp1["dataset_name"] = temp1["name"]
            temp1.pop("_sa_instance_state", None)
            temp1.pop("name", None)
            datasets.append(temp1)
        # print(datasets)

        sektorals = []
        for res2 in sektoral:
            temp2 = res2.__dict__
            temp2["sektoral_id"] = temp2["id"]
            temp2["sektoral_name"] = temp2["name"]
            temp2.pop("_sa_instance_state", None)
            temp2.pop("id", None)
            temp2.pop("name", None)
            sektorals.append(temp2)
        # print(sektorals)

        skpds = []
        for res3 in skpd:
            temp3 = res3.__dict__
            temp3["skpd_id"] = temp3["id"]
            temp3["skpd_name"] = temp3["nama_skpd"]
            temp3.pop("_sa_instance_state", None)
            temp3.pop("id", None)
            skpds.append(temp3)
        # print(skpds)

        # convert to dataframe
        pd_dataset = pd.DataFrame.from_records(datasets)
        pd_sektoral = pd.DataFrame.from_records(sektorals)
        pd_skpd = pd.DataFrame.from_records(skpds)

        # join data
        pd_data = pd_dataset.merge(pd_sektoral.iloc[:, :], how="left", on="sektoral_id")
        pd_data = pd_data.merge(pd_skpd.iloc[:, :], how="left", on="kode_skpd")

        # select columns and make new columns
        df_ = pd_data.loc[
            :,
            pd_data.columns.intersection(
                ["id", "dataset_name", "sektoral_name", "skpd_name"]
            ),
        ]
        df_.columns = ["id", "name_data", "sektoral", "skpd"]
        df_["text"] = (df_["name_data"].str.cat(df_["sektoral"], sep="-")).str.cat(
            df_["skpd"], sep="-"
        )

        # clean dataframe's text column
        df_["text"] = df_["text"].apply(self.processdata)
        # print(df_)

        # remove null data
        df_ = df_[df_["text"].notna()]

        # Inisialisasi TfidfVectorizer
        tf = TfidfVectorizer()

        # Melakukan perhitungan idf pada data topic
        tf.fit(df_["text"])

        # Mapping array dari fitur index integer ke fitur nama
        tf.get_feature_names_out()

        # Melakukan fit lalu ditransformasikan ke bentuk matrix
        tfidf_matrix = tf.fit_transform(df_["text"])

        cosine_sim = cosine_similarity(tfidf_matrix)

        cosine_sim_df = pd.DataFrame(
            cosine_sim, index=df_["name_data"], columns=df_["id"]
        )

        cosine_sim_df.to_pickle("static/model/dataset_recommendation.pkl")

        # check if empty
        if len(cosine_sim_df) > 0:
            return True, 1
        else:
            return False, {}

    # helper function to clean data
    def processdata(self, data):
        """Doc: function process data"""
        try:
            # strip remove
            data = data.strip().replace("\n", " ").replace("\r", " ")
            # Remove tickers
            data = re.sub(r"\$\w*", "", data)
            # To lowercase
            data = data.lower()
            # Remove Punctuation and split 's, 't, 've with a space for filter
            data = re.sub(r"[" + punctuation.replace("@", "") + "]+", " ", data)
            # Remove whitespace (including new line characters)
            data = re.sub(r"\s\s+", " ", data)
            # Remove single space remaining at the front of the tweet.
            data = data.lstrip(" ")
            # Remove characters beyond Basic Multilingual Plane (BMP) of Unicode:
            data = "".join(c for c in data if c <= "\uFFFF")
            return data
        except Exception:
            pass

    def switch(self, category, dataset):
        """Doc: function switch"""
        if not category:
            category = "view"
        return getattr(self, "case_" + str(category), lambda: dataset)(dataset)

    def case_facebook(self, dataset):
        """Doc: function switch"""
        count_share_fb = dataset["count_share_fb"]
        if not count_share_fb:
            count_share_fb = "0"
        count_share_fb = int(count_share_fb) + 1
        payload = {"count_share_fb": int(count_share_fb)}
        dataset["count_share_fb"] = count_share_fb
        return dataset, payload

    def case_twitter(self, dataset):
        """Doc: function case_twitter"""
        count_share_tw = dataset["count_share_tw"]
        if not count_share_tw:
            count_share_tw = "0"
        count_share_tw = int(count_share_tw) + 1
        payload = {"count_share_tw": int(count_share_tw)}
        dataset["count_share_tw"] = count_share_tw
        return dataset, payload

    def case_whatsapp(self, dataset):
        """Doc: function case_whatsapp"""
        count_share_wa = dataset["count_share_wa"]
        if not count_share_wa:
            count_share_wa = "0"
        count_share_wa = int(count_share_wa) + 1
        payload = {"count_share_wa": int(count_share_wa)}
        dataset["count_share_wa"] = count_share_wa
        return dataset, payload

    def case_link(self, dataset):
        """Doc: function case_view"""
        count_share_link = dataset["count_share_link"]
        if not count_share_link:
            count_share_link = "0"
        count_share_link = int(count_share_link) + 1
        payload = {"count_share_link": int(count_share_link)}
        dataset["count_share_link"] = count_share_link
        return dataset, payload

    def case_view(self, dataset):
        """Doc: function case_view"""
        count_view = dataset["count_view"]
        if not count_view:
            count_view = "0"
        count_view = int(count_view) + 1
        payload = {"count_view": int(count_view)}
        dataset["count_view"] = count_view
        return dataset, payload

    def case_access(self, dataset):
        """Doc: function case_access"""
        count_access = dataset["count_access"]
        if not count_access:
            count_access = "0"
        count_access = int(count_access) + 1
        payload = {"count_access": int(count_access)}
        dataset["count_access"] = count_access
        return dataset, payload

    def case_excel(self, dataset):
        """Doc: function case_excel"""
        count_download_xls = dataset["count_download_xls"]
        if not count_download_xls:
            count_download_xls = "0"
        count_download_xls = int(count_download_xls) + 1
        payload = {"count_download_xls": int(count_download_xls)}
        dataset["count_download_xls"] = count_download_xls
        return dataset, payload

    def case_csv(self, dataset):
        """Doc: function case_csv"""
        count_download_csv = dataset["count_download_csv"]
        if not count_download_csv:
            count_download_csv = "0"
        count_download_csv = int(count_download_csv) + 1
        payload = {"count_download_csv": int(count_download_csv)}
        dataset["count_download_csv"] = count_download_csv
        return dataset, payload

    def case_api(self, dataset):
        """Doc: function case_api"""
        count_download_api = dataset["count_download_api"]
        if not count_download_api:
            count_download_api = "0"
        count_download_api = int(count_download_api) + 1
        payload = {"count_download_api": int(count_download_api)}
        dataset["count_download_api"] = count_download_api
        return dataset, payload

    def case_excel_filter(self, dataset):
        """Doc: function case_excel_filter"""
        count_download_xls_filter = dataset["count_download_xls_filter"]
        if not count_download_xls_filter:
            count_download_xls_filter = "0"
        count_download_xls_filter = int(count_download_xls_filter) + 1
        payload = {"count_download_xls_filter": int(count_download_xls_filter)}
        dataset["count_download_xls_filter"] = count_download_xls_filter
        return dataset, payload

    def case_csv_filter(self, dataset):
        """Doc: function case_csv_filter"""
        count_download_csv_filter = dataset["count_download_csv_filter"]
        if not count_download_csv_filter:
            count_download_csv_filter = "0"
        count_download_csv_filter = int(count_download_csv_filter) + 1
        payload = {"count_download_csv_filter": int(count_download_csv_filter)}
        dataset["count_download_csv_filter"] = count_download_csv_filter
        return dataset, payload

    def case_copy(self, dataset):
        """Doc: function case_copy"""
        count_download_copy = dataset["count_download_copy"]
        if not count_download_copy:
            count_download_copy = "0"
        count_download_copy = int(count_download_copy) + 1
        payload = {"count_download_copy": int(count_download_copy)}
        dataset["count_download_copy"] = count_download_copy
        return dataset, payload

    def case_view_private(self, dataset):
        """Doc: function case_view_private"""
        count_view_private = dataset["count_view_private"]
        if not count_view_private:
            count_view_private = "0"
        count_view_private = int(count_view_private) + 1
        payload = {"count_view_private": int(count_view_private)}
        dataset["count_view_private"] = count_view_private
        return dataset, payload

    def case_access_private(self, dataset):
        """Doc: function case_access_private"""
        count_access_private = dataset["count_access_private"]
        if not count_access_private:
            count_access_private = "0"
        count_access_private = int(count_access_private) + 1
        payload = {"count_access_private": int(count_access_private)}
        dataset["count_access_private"] = count_access_private
        return dataset, payload

    def case_excel_private(self, dataset):
        """Doc: function case_excel_private"""
        count_download_xls_private = dataset["count_download_xls_private"]
        if not count_download_xls_private:
            count_download_xls_private = "0"
        count_download_xls_private = int(count_download_xls_private) + 1
        payload = {"count_download_xls_private": int(count_download_xls_private)}
        dataset["count_download_xls_private"] = count_download_xls_private
        return dataset, payload

    def case_csv_private(self, dataset):
        """Doc: function case_csv_private"""
        count_download_csv_private = dataset["count_download_csv_private"]
        if not count_download_csv_private:
            count_download_csv_private = "0"
        count_download_csv_private = int(count_download_csv_private) + 1
        payload = {"count_download_csv_private": int(count_download_csv_private)}
        dataset["count_download_csv_private"] = count_download_csv_private
        return dataset, payload

    def case_api_private(self, dataset):
        """Doc: function case_api_private"""
        count_download_api_private = dataset["count_download_api_private"]
        if not count_download_api_private:
            count_download_api_private = "0"
        count_download_api_private = int(count_download_api_private) + 1
        payload = {"count_download_api_private": int(count_download_api_private)}
        dataset["count_download_api_private"] = count_download_api_private
        return dataset, payload

    def route_create_dataset_tag(self, temp_tag, dataset):
        """Doc: function route_create_dataset_tag"""
        try:
            for tag in temp_tag:
                temp = {}
                temp["tag"] = tag
                temp["dataset_id"] = dataset["id"]
                res, res_tag = DATASET_TAG_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_create_metadata(self, temp_metadata, dataset):
        """Doc: function route_create_metadata"""
        try:
            for metadata in temp_metadata:
                temp = metadata
                temp["dataset_id"] = dataset["id"]
                res, res_metadata = METADATA_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_create_count_dataset(self, json_request):
        """Doc: function route_create_count_dataset"""
        if "kode_skpd" in json_request:
            # public only
            result, count = self.get_count(
                {
                    "kode_skpd": json_request["kode_skpd"],
                    "is_deleted": False,
                    "is_active": True,
                    "dataset_class_id": 3,
                    "is_validate": 3,
                },
                "",
            )
            update_ = db.session.query(SkpdModel)
            update_ = update_.filter(
                getattr(SkpdModel, "kode_skpd") == json_request["kode_skpd"]
            )
            update_ = update_.update(
                {"count_dataset_public": count["count"]}, synchronize_session="fetch"
            )
            db.session.commit()
            # with private
            result, count = self.get_count(
                {
                    "kode_skpd": json_request["kode_skpd"],
                    "is_deleted": False,
                    "is_active": True,
                    "is_validate": 3,
                },
                "",
            )
            update__ = db.session.query(SkpdModel)
            update__ = update__.filter(
                getattr(SkpdModel, "kode_skpd") == json_request["kode_skpd"]
            )
            update__ = update__.update(
                {"count_dataset": count["count"]}, synchronize_session="fetch"
            )
            db.session.commit()

    def route_create_jabatan_access(self, dataset, jabatan_access):
        """Doc: function route_create_user_permission"""
        try:
            if dataset["dataset_class"]["id"] == 4:
                # insert user jabatan
                for ja_ in jabatan_access:
                    try:
                        dataset_permission = {}
                        dataset_permission["dataset_id"] = dataset["id"]
                        dataset_permission["jabatan_id"] = ja_
                        dataset_permission["enable"] = True
                        dataset_permission = DatasetPermissionModel(
                            **dataset_permission
                        )
                        db.session.add(dataset_permission)
                        db.session.commit()

                    except Exception as err:
                        print(str(err))

        except Exception as err:
            print(str(err))

    def route_insert_history(self, res, dataset, category):
        """Doc: function route_insert_history"""
        if res and dataset["is_deleted"] == False:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history["type"] = "dataset"
            json_history["type_id"] = dataset["id"]
            json_history["user_id"] = jwt["id"]
            json_history["datetime"] = HELPER.local_date_server()
            json_history["category"] = category
            result, history = HISTORY_CONTROLLER.create(json_history)

    def route_insert_history_draft(self, history_draft, dataset):
        """Doc: function route_insert_history_draft"""

        jwt = HELPER.read_jwt()
        json_history_draft = {}
        json_history_draft["type"] = "dataset"
        json_history_draft["type_id"] = dataset["id"]
        json_history_draft["user_id"] = jwt["id"]
        json_history_draft["datetime"] = HELPER.local_date_server()
        json_history_draft["category"] = history_draft["category"]
        json_history_draft["notes"] = history_draft["notes"]

        result, result_data = HISTORY_DRAFT_CONTROLLER.create(json_history_draft)

    def route_insert_notification(self, res, json_request, dataset):
        """Doc: function route_insert_notification"""
        if res and "is_validate" in json_request and dataset["is_deleted"] == False:
            # get jwt
            jwt = HELPER.read_jwt()

            # default data
            send_notif = False
            notification = None
            json_notification = {}
            json_notification["feature"] = 2
            json_notification["type"] = "dataset"
            json_notification["type_id"] = dataset["id"]
            json_notification["sender"] = jwt["id"]
            json_notification["title"] = dataset["name"]
            json_notification["is_read"] = False
            json_notification["cdate"] = HELPER.local_date_server()
            json_notification["mdate"] = HELPER.local_date_server()

            # default get user
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)

            # jika walidata
            if jwt["role"]["name"] == "walidata":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["title"] = "Dataset " + dataset["name"]
                    json_notification[
                        "content"
                    ] = " yang ditambahkan Walidata membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == dataset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "title"
                    ] = "Walidata melakukan publikasi terhadap dataset "
                    json_notification["content"] = (
                        "<b>" + dataset["name"] + "</b> dari Organisasi Anda."
                    )
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # jika opd
            elif jwt["role"]["name"] == "opd":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == dataset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == dataset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " sudah diverifikasi dan dipublikasikan."
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # send notif
            if send_notif:
                data_user = data_user.all()
                for dr_ in data_user:
                    temp = dr_._asdict()
                    user_id = temp["id"]
                    json_notification["receiver"] = user_id
                    result, notification = NOTIFICATION_CONTROLLER.create(
                        json_notification
                    )
                    self.route_notification_count(send_notif, user_id, notification)

    def route_insert_notification_update(self, res, json_request, dataset, param_from):
        """Doc: function route_insert_notification_update"""
        if res and "is_validate" in json_request and dataset["is_deleted"] == False:
            # get jwt
            jwt = HELPER.read_jwt()

            # default data
            send_notif = False
            notification = None
            json_notification = {}
            json_notification["feature"] = 2
            json_notification["type"] = "dataset"
            json_notification["type_id"] = dataset["id"]
            json_notification["sender"] = jwt["id"]
            json_notification["title"] = dataset["name"]
            json_notification["is_read"] = False
            json_notification["cdate"] = HELPER.local_date_server()
            json_notification["mdate"] = HELPER.local_date_server()
            if param_from == "edit":
                json_notification["notes"] = "edit"
            else:
                json_notification["notes"] = "new"

            # default get user
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)

            # jika walidata
            if jwt["role"]["name"] == "walidata":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["title"] = "Dataset " + dataset["name"]
                    json_notification[
                        "content"
                    ] = " yang ditambahkan Walidata membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == dataset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "title"
                    ] = "Walidata melakukan publikasi terhadap dataset "
                    json_notification["content"] = (
                        "<b>" + dataset["name"] + "</b> dari Organisasi Anda."
                    )
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # jika opd
            elif jwt["role"]["name"] == "opd":
                # is_validate 0 = draft
                if json_request["is_validate"] == 0:
                    send_notif = False
                # is_validate 1 = waiting
                elif json_request["is_validate"] == 1:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # is_validate 2 = revisi
                elif json_request["is_validate"] == 2:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == dataset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " membutuhkan revisi sebelum dipublikasikan."
                    send_notif = True
                # is_validate 3 = approve
                elif json_request["is_validate"] == 3:
                    data_user = data_user.filter(RoleModel.name == "opd")
                    data_user = data_user.filter(
                        UserModel.kode_skpd == dataset["skpd"]["kode_skpd"]
                    )
                    json_notification[
                        "content"
                    ] = " sudah diverifikasi dan dipublikasikan."
                    send_notif = True
                # is_validate 4 = edit
                elif json_request["is_validate"] == 4:
                    data_user = data_user.filter(RoleModel.name == "walidata")
                    json_notification["content"] = (
                        MESSAGE_DARI
                        + dataset["skpd"]["nama_skpd"]
                        + MESSAGE_MEMBUTUHKAN_VERIFIKASI
                    )
                    send_notif = True
                # lainnya
                else:
                    send_notif = False

            # send notif
            if send_notif:
                data_user = data_user.all()
                for dr_ in data_user:
                    temp = dr_._asdict()
                    user_id = temp["id"]
                    json_notification["receiver"] = user_id
                    result, notification = NOTIFICATION_CONTROLLER.create(
                        json_notification
                    )
                    self.route_notification_count(send_notif, user_id, notification)

    def route_notification_count(self, send_notif, user_id, notification):
        """Doc: function route_insert_notification_count"""
        if send_notif == True:
            # update notif
            param = {}
            param["count_notif"] = 0
            res_, user = USER_CONTROLLER.get_by_id(user_id)
            if res_:
                if user["count_notif"] == None:
                    user["count_notif"] = 0
                param["count_notif"] = user["count_notif"] + 1
                # execute database
                result = db.session.query(UserModel)
                result = result.filter(getattr(UserModel, "id") == user_id)
                result = result.update(param, synchronize_session="fetch")
                result = db.session.commit()

            # try:
            #     socket_url = conf.SOCKET_URL
            #     token = request.headers.get("Authorization", None)
            #     req_ = requests.get(
            #         socket_url
            #         + "notification"
            #         + MESSAGE_RECEIVER
            #         + str(notification["receiver"]),
            #         headers={
            #             "Authorization": token,
            #             "Content-Type": conf.MESSAGE_APP_JSON,
            #         },
            #         data=json.dumps(notification),
            #     )
            #     res_ = req_.json()
            # except Exception as err:
            #     print(str(err))

    # def route_restart_coredata(self, temp_json):
    #     ''' Doc: function route_restart_coredata  '''
    #     if temp_json['data']:
    #         try:
    #             result_restart = requests.get(conf.COREDATA_URL+"restart/", headers={"Content-Type": conf.MESSAGE_APP_JSON})
    #             result_restart = result_restart.json()
    #         except Exception as err:
    #             print(str(err))

    def route_update_dataset_tag(self, temp_tag, dataset):
        """Doc: function route_update_dataset_tag"""
        try:
            if temp_tag:
                result_tag = db.session.query(DatasetTagModel)
                result_tag = result_tag.filter(
                    getattr(DatasetTagModel, "dataset_id") == dataset["id"]
                )
                result_tag = result_tag.all()
                for res_tag in result_tag:
                    db.session.delete(res_tag)
                    db.session.commit()
                # insert tag
                for tag in temp_tag:
                    temp = {}
                    temp["tag"] = tag
                    temp["dataset_id"] = dataset["id"]
                    # print(temp)
                    res, res_tag = DATASET_TAG_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_update_metadata(self, temp_metadata, dataset):
        """Doc: function route_update_metadata"""
        try:
            if temp_metadata:
                result_metadata = db.session.query(MetadataModel)
                result_metadata = result_metadata.filter(
                    getattr(MetadataModel, "dataset_id") == dataset["id"]
                )
                result_metadata = result_metadata.all()
                for res_met in result_metadata:
                    db.session.delete(res_met)
                    db.session.commit()
                # insert metadata
                for metadata in temp_metadata:
                    temp = metadata
                    temp["dataset_id"] = dataset["id"]
                    # print(temp)
                    res, res_metadata = METADATA_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_update_user_permission(self, temp_user_permission, dataset):
        """Doc: function route_update_user_permission"""
        try:
            if temp_user_permission and dataset["dataset_class"]["id"] == 4:
                # delete user_permission
                result_user_permission = db.session.query(UserPermissionModel)
                result_user_permission = result_user_permission.filter(
                    getattr(UserPermissionModel, "app_id") == dataset["app"]["id"]
                )
                result_user_permission = result_user_permission.filter(
                    getattr(UserPermissionModel, "app_service_id")
                    == dataset["app_service"]["id"]
                )
                result_user_permission = result_user_permission.all()
                for res_usper in result_user_permission:
                    db.session.delete(res_usper)
                    db.session.commit()
                # create user_permission
                for permission in temp_user_permission:
                    result_user = db.session.query(UserModel)
                    result_user = result_user.filter(
                        getattr(UserModel, "kode_skpd") == permission
                    )
                    result_user = result_user.all()
                    for res_user in result_user:
                        temp_user = res_user.to_dict()

                        # insert user_permission
                        try:
                            user_permission = {}
                            user_permission["user_id"] = temp_user["id"]
                            user_permission["app_id"] = dataset["app"]["id"]
                            user_permission["app_service_id"] = dataset["app_service"][
                                "id"
                            ]
                            user_permission["enable"] = True
                            user_permission = UserPermissionModel(**user_permission)
                            db.session.add(user_permission)
                            db.session.commit()
                            result_user_permission = user_permission.to_dict()

                        except Exception as err:
                            print(str(err))
        except Exception as err:
            print(str(err))

    def route_update_count_dataset(self, json_request, old_skpd, new_skpd):
        """Doc: function route_update_count_dataset"""
        if "kode_skpd" in json_request:
            # old skpd
            # public only
            result1, count1 = self.get_count(
                {
                    "kode_skpd": old_skpd,
                    "is_deleted": False,
                    "is_active": True,
                    "dataset_class_id": 3,
                    "is_validate": 3,
                },
                "",
            )
            update1 = db.session.query(SkpdModel)
            update1 = update1.filter(getattr(SkpdModel, "kode_skpd") == old_skpd)
            update1 = update1.update(
                {"count_dataset_public": count1["count"]}, synchronize_session="fetch"
            )
            db.session.commit()
            # with private
            result1, count1 = self.get_count(
                {
                    "kode_skpd": old_skpd,
                    "is_deleted": False,
                    "is_active": True,
                    "is_validate": 3,
                },
                "",
            )
            update1 = db.session.query(SkpdModel)
            update1 = update1.filter(getattr(SkpdModel, "kode_skpd") == old_skpd)
            update1 = update1.update(
                {"count_dataset": count1["count"]}, synchronize_session="fetch"
            )
            db.session.commit()

            # new skpd
            # public only
            result2, count2 = self.get_count(
                {
                    "kode_skpd": new_skpd,
                    "is_deleted": False,
                    "is_active": True,
                    "dataset_class_id": 3,
                    "is_validate": 3,
                },
                "",
            )
            update2 = db.session.query(SkpdModel)
            update2 = update2.filter(getattr(SkpdModel, "kode_skpd") == new_skpd)
            update2 = update2.update(
                {"count_dataset_public": count2["count"]}, synchronize_session="fetch"
            )
            db.session.commit()
            # with private
            result2, count2 = self.get_count(
                {
                    "kode_skpd": new_skpd,
                    "is_deleted": False,
                    "is_active": True,
                    "is_validate": 3,
                },
                "",
            )
            update2 = db.session.query(SkpdModel)
            update2 = update2.filter(getattr(SkpdModel, "kode_skpd") == new_skpd)
            update2 = update2.update(
                {"count_dataset": count2["count"]}, synchronize_session="fetch"
            )
            db.session.commit()

    def route_update_jabatan_access(self, dataset, jabatan_access):
        """Doc: function route_create_user_permission"""
        try:
            if dataset["dataset_class"]["id"] == 4:
                # delete user jabatan
                result = db.session.query(DatasetPermissionModel)
                result = result.filter(
                    getattr(DatasetPermissionModel, "dataset_id") == dataset["id"]
                )
                result = result.all()
                for res in result:
                    db.session.delete(res)
                    db.session.commit()

                # insert user jabatan
                for ja_ in jabatan_access:
                    try:
                        dataset_permission = {}
                        dataset_permission["dataset_id"] = dataset["id"]
                        dataset_permission["jabatan_id"] = ja_
                        dataset_permission["enable"] = True
                        dataset_permission = DatasetPermissionModel(
                            **dataset_permission
                        )
                        db.session.add(dataset_permission)
                        db.session.commit()
                    except Exception as err:
                        print(str(err))

        except Exception as err:
            print(str(err))

    def get_quality_score(self, dataset_id):
        try:
            result = db.session.query(DatasetQualityResultModel)
            result = result.get(dataset_id)

            if result:
                result = result.__dict__
                result.pop("_sa_instance_state", None)
            else:
                result = {}

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def advance_search(self, where, sort, limit, skip, keyword):
        try:
            search_result = meilisearch.do(
                keyword, {"filter": where, "sort": sort, "limit": limit, "offset": skip}
            )
            dataset_result = search_result["hits"]
            for i, d in enumerate(dataset_result):
                dataset_result[i]["metadata"] = json.loads(d["metadata"].replace("\'", '')) if d.get("metadata") else []
            meta = {
                "skip": skip,
                "limit": limit,
                "total_record": search_result["estimatedTotalHits"],
                "total_page": math.ceil(search_result["estimatedTotalHits"] / limit),
            }

            return dataset_result, meta
        except Exception as err:
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_dcat(self):
        sql = '''
            SELECT
                dataset.ID AS id,
                dataset.NAME AS name,
                dataset.title AS title,
                dataset.SCHEMA AS schema,
                dataset."table" AS table,
                dataset.cdate AS cdate,
                dataset.mdate AS mdate,
                dataset.description AS description,
                array((select tag from dataset_tag where dataset_id = dataset.Id)) as tags,
                metadata.VALUE AS accrual_periodicity,
                skpd.nama_skpd AS organisasi_name,
                skpd.email AS organisasi_email
            FROM
                dataset
                JOIN skpd ON dataset.kode_skpd = skpd.kode_skpd
                JOIN metadata ON dataset.ID = metadata.dataset_id AND metadata.KEY = 'Frekuensi Dataset'
            WHERE
                dataset.is_active = TRUE
                AND dataset.is_deleted = FALSE
                AND dataset.dataset_class_id = 3
        '''
        success, result = db2.query_dict_array(sql)

        dataset = []
        for data in result:
            datatag = []
            for tag in data['tags']:
                tg = tag.split(' ')
                for t in tg:
                    datatag.append(t)
            datatag = list(dict.fromkeys(datatag))

            temp = {
                "@type": "dcat:Dataset",
                "accessLevel": "Publik",
                "contactPoint": {
                    "fn": "Dinas Komunikasi dan Informatika Jawa Barat",
                    "hasEmail": "diskominfo@jabarprov.go.id"
                },
                "distribution": [
                    {
                        "@type": "dcat:Distribution",
                        "downloadURL": f"https://data.jabarprov.go.id/api-backend/bigdata/{data['schema']}/{data['table']}?download=csv",
                        "mediaType": "text/csv",
                        "format": "csv",
                        "title": data['name'],
                    }
                ],
                "identifier": str(uuid.uuid5(namespace, str(data['id']))),
                "issued": data['cdate'],
                "landingPage": f"https://opendata.jabarprov.go.id/id/dataset/{data['title']}",
                "modified": data['mdate'],
                "accrualPeriodicity": data['accrual_periodicity'],
                "publisher": {
                    "@type": "org:Organization",
                    "name": data['organisasi_name'],
                },
                "title": data['name'],
                "description": data['description'],
                "keyword": datatag,
            }
            dataset.append(temp)

        return {
            "@context": "https://project-open-data.cio.gov/v1.1/schema/catalog.jsonld",
            "@id": "data.jabarprov.go.id/api-backend/data.json",
            "@type": "dcat:Catalog",
            "conformsTo": "https://project-open-data.cio.gov/v1.1/schema",
            "describedBy": "https://project-open-data.cio.gov/v1.1/schema/catalog.json",
            "dataset": dataset,
        }
