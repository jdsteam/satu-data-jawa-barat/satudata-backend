''' Doc: controller review  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import ReviewModel
from models import UserModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class ReviewController(object):
    ''' Doc: class review  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(ReviewModel)
            for attr, value in where.items():
                result = result.filter(getattr(ReviewModel, attr) == value)
            result = result.filter(or_(cast(getattr(ReviewModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in ReviewModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            review = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to user
                user = db.session.query(UserModel)
                user = user.get(temp['user_id'])

                if user:
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                    user.pop('password', None)
                    user.pop('is_active', None)
                    user.pop('is_deleted', None)
                    user.pop('cdate', None)
                    user.pop('cuid', None)

                    user = self.populate_skpd(user)
                    user = self.populate_skpdsub(user)
                    user = self.populate_skpdunit(user)

                else:
                    user = {}

                temp['user'] = user
                # temp.pop('user_id', None)

                review.append(temp)

            # check if empty
            review = list(review)
            if review:
                return True, review
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_skpd(self, user):
        ''' Doc: function populate_skpd  '''
        if user['kode_skpd']:
            # get relation to skpd
            skpd = db.session.query(SkpdModel)
            skpd = skpd.options(
                defer("cuid"),
                defer("cdate"))
            skpd = skpd.filter(getattr(SkpdModel, "kode_skpd") == user['kode_skpd'])
            skpd = skpd.one()

            if skpd:
                skpd = skpd.__dict__
                skpd.pop('_sa_instance_state', None)
            else:
                skpd = {}

            user['skpd'] = skpd
            # temp.pop('user_id', None)
        else:
            user['skpd'] = {}

        return user

    def populate_skpdsub(self, user):
        ''' Doc: function populate_skpdsub  '''
        if user['kode_skpdsub']:
            # get relation to skpdsub
            skpdsub = db.session.query(SkpdSubModel)
            skpdsub = skpdsub.options(
                defer("cuid"),
                defer("cdate"))
            skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpdsub") == user['kode_skpdsub'])
            skpdsub = skpdsub.one()

            if skpdsub:
                skpdsub = skpdsub.__dict__
                skpdsub.pop('_sa_instance_state', None)
            else:
                skpdsub = {}

            user['skpdsub'] = skpdsub
            # temp.pop('user_id', None)
        else:
            user['skpdsub'] = {}

        return user

    def populate_skpdunit(self, user):
        ''' Doc: function populate_skpdunit  '''
        if user['kode_skpdunit']:
            # get relation to skpdunit
            skpdunit = db.session.query(SkpdUnitModel)
            skpdunit = skpdunit.options(
                defer("cuid"),
                defer("cdate"))
            skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdunit") == user['kode_skpdunit'])
            skpdunit = skpdunit.one()

            if skpdunit:
                skpdunit = skpdunit.__dict__
                skpdunit.pop('_sa_instance_state', None)
            else:
                skpdunit = {}

            user['skpdunit'] = skpdunit
            # temp.pop('user_id', None)
        else:
            user['skpdunit'] = {}

        return user

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(ReviewModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(ReviewModel, attr) == value)
            result = result.filter(or_(cast(getattr(ReviewModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in ReviewModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(ReviewModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}

            # change into dict
            review = result

            # check if empty
            if review:
                return True, review
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()

            # prepare data model
            result = ReviewModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, review = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, review
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()

            try:
                # prepare data model
                result = db.session.query(ReviewModel)
                for attr, value in where.items():
                    result = result.filter(getattr(ReviewModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, review = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, review
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(ReviewModel)
                for attr, value in where.items():
                    result = result.filter(getattr(ReviewModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, review = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
