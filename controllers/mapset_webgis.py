''' Doc: controller mapset webgis  '''
from models.skpd import SkpdModel
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetWebgisModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MapsetWebgisController(object):
    ''' Doc: class mapset webgis  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
              MapsetWebgisModel.id.label('id'),
              MapsetWebgisModel.skpd_id.label('skpd_id'), MapsetWebgisModel.name.label('name'), MapsetWebgisModel.url.label('url'), MapsetWebgisModel.count_view.label('count_view'),MapsetWebgisModel.is_active.label('is_active'), MapsetWebgisModel.is_deleted.label('is_deleted'),
              MapsetWebgisModel.cdate.label('cdate'), MapsetWebgisModel.mdate.label('mdate'),
              SkpdModel.nama_skpd,
              SkpdModel.kode_skpd,
              SkpdModel.logo.label('logo_skpd')
            )
            result = result.join(SkpdModel, MapsetWebgisModel.skpd_id == SkpdModel.id, isouter=True)

            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(MapsetWebgisModel.is_deleted == False)
                result = result.filter(MapsetWebgisModel.is_active == True)

            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(MapsetWebgisModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(MapsetWebgisModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(MapsetWebgisModel, "name"), sqlalchemy.String).match(src)
                    ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            if result:
                result_data = []
                for res in result:
                    temp = res._asdict()
                    result_data.append(temp)
                return True, result_data
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(MapsetWebgisModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(MapsetWebgisModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(MapsetWebgisModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetWebgisModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MapsetWebgisModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(MapsetWebgisModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['cdate'] is not None:
                    result['cdate'] =  result['cdate'].strftime("%Y-%m-%d %H:%M:%S")

                if result['mdate'] is not None:
                    result['mdate'] =  result['mdate'].strftime("%Y-%m-%d %H:%M:%S")
            else:
                result = {}

            # change into dict
            mapset_webgis = result

            # check if empty
            if mapset_webgis:
                return True, mapset_webgis
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = MapsetWebgisModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, mapset_webgis = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, mapset_webgis
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(MapsetWebgisModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetWebgisModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, mapset_webgis = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, mapset_webgis
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(MapsetWebgisModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetWebgisModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, _ = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def counter(self, _id:str):
        ''' Doc: function update counter  '''
        _id = str(_id)
        try:
            sql_query = "UPDATE mapset_webgis SET count_view = count_view + 1 WHERE id = '%s'" % (_id)
            sql = text(sql_query)
            db.engine.execute(sql)

            # check if empty
            return True, {}

        except Exception as err:
            print(err)
            # fail response
            if err.__class__.__name__  == "ProgrammingError":
                err = "column %s not exists"

            return False, err
