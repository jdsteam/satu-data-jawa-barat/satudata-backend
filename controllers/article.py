''' Doc: controller article topic  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import ArticleModel, ArticleTopicModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast, func
import sqlalchemy, re, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

class ArticleController(object):
    ''' Doc: class article topic  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def refresh(self):
        try:
            sql_query_article = "REFRESH MATERIALIZED VIEW suggestion_article"
            sql_article = text(sql_query_article)
            db.engine.execute(sql_article)
        except Exception as err:
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
              ArticleModel.id, ArticleModel.article_topic.label('topic'),
              ArticleModel.creator, ArticleModel.name, ArticleModel.title, ArticleModel.image,
              ArticleModel.content_short, ArticleModel.notes, ArticleModel.organization,
              ArticleModel.keywords, ArticleModel.count_view, ArticleModel.is_active, ArticleModel.is_deleted,
              ArticleModel.cdate, ArticleModel.mdate
            )
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(ArticleModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(ArticleModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(ArticleModel.is_deleted == False)
                result = result.filter(ArticleModel.is_active == True)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(ArticleModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(ArticleModel, "creator"), sqlalchemy.String).match(src),
                    ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query_detail(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
              ArticleModel.id, ArticleModel.article_topic.label('topic'),
              ArticleModel.creator, ArticleModel.name, ArticleModel.title, ArticleModel.image,
              ArticleModel.content_short, ArticleModel.content_long, ArticleModel.notes, ArticleModel.organization,
              ArticleModel.keywords, ArticleModel.count_view, ArticleModel.is_active, ArticleModel.is_deleted,
              ArticleModel.cdate, ArticleModel.mdate
            )
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(ArticleModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(ArticleModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(ArticleModel.is_deleted == False)
                result = result.filter(ArticleModel.is_active == True)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(ArticleModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(ArticleModel, "creator"), sqlalchemy.String).match(src),
                    ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("article."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            results = []
            for res in result:
                temp = res._asdict()
                temp.pop('_sa_instance_state', None)
                results.append(temp)

            if results:
                return True, results
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_random(self):
        ''' Doc: function get all  '''
        try:
            where = {
                "is_active": True,
                "is_deleted": False
            }
            # query postgresql
            result = self.query(where, None)
            result = result.order_by(func.random())
            result = result.offset(0).limit(3)
            result = result.all()

            if result:
                result_data = []
                for res in result:
                    temp = res._asdict()
                    result_data.append(temp)
                return True, result_data
            else:
                return False, [], 0

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query_detail({'title':_id}, '')
            result = result.first()
            # convert to dict result._asdict()

            # check if empty
            if result:
                return True, result._asdict(), "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_ids(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(ArticleModel)
            result = result.get(_id)
            # convert to dict result._asdict()

            # check if empty
            if result:
                return True, result.__dict__, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            # title
            json_send["title"] = json_send["name"].lower().replace(' ', '-')
            json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(ArticleModel.id)
            res_count = res_count.filter(ArticleModel.title == json_send["title"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            json_send["count_view"] = 0
            json_send["count_share_fb"] = 0
            json_send["count_share_tw"] = 0
            json_send["count_share_wa"] = 0
            json_send["count_share_link"] = 0
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = ArticleModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            self.refresh()

            if result:
                return True, json_send
            else:
                return False, {}

        except Exception as err:
            # fail response
            if (err.__class__.__name__ == 'IntegrityError'):
                err = "duplicate name %s" % json_send["name"]

            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            if 'name' in json_send:
                json_send["title"] = json_send["name"].lower().replace(' ', '-')
                json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])

            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            # prepare data model
            result = db.session.query(ArticleModel)
            for attr, value in where.items():
                result = result.filter(getattr(ArticleModel, attr) == value)

            # execute database
            result = result.update(json_send, synchronize_session='fetch')
            check = result
            result = db.session.commit()
            self.refresh()
            # check if empty
            if check:
                return True, json_send
            else:
                return False, "Unique Id %s not found" % where['id']

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            json_send = {}
            json_send['is_deleted'] = True

            jwt = HELPER.read_jwt()
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            status, result = self.update(where, json_send)

            return status, result
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def counter(self, _id:str, category):
        ''' Doc: function update counter  '''
        _id = str(_id)
        try:
            sql_query = "UPDATE article SET %s = %s + 1 WHERE title = '%s'" % (category, category, _id)
            sql = text(sql_query)
            db.engine.execute(sql)

            self.refresh()
            # check if empty
            return True, {}

        except Exception as err:
            print(err)
            # fail response
            if err.__class__.__name__  == "ProgrammingError":
                err = "column %s not exists" % category

            return False, err

    def get_organization_all(self):
        ''' Doc: function get owner all  '''
        try:
            # query postgresql
            result = db.session.query(ArticleModel.organization)
            result = result.distinct(ArticleModel.organization)
            result = result.filter(getattr(ArticleModel, 'is_deleted') == False)
            result = result.filter(getattr(ArticleModel, 'is_active') == True)
            result = result.filter(getattr(ArticleModel, 'organization') != None)
            result = result.all()

            result = list(dict.fromkeys(result))

            # change into dict
            organization = []
            for res in result:
                temp = {}
                temp['organization'] = res[0]
                organization.append(temp)

            # check if empty
            if organization:
                return True, organization
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_topic_all(self):
        ''' Doc: function get owner all  '''
        try:
            # query postgresql
            result = db.session.query(ArticleModel.article_topic)
            result = result.distinct(ArticleModel.article_topic)
            result = result.filter(getattr(ArticleModel, 'is_deleted') == False)
            result = result.filter(getattr(ArticleModel, 'is_active') == True)
            result = result.filter(getattr(ArticleModel, 'article_topic') != None)
            result = result.all()

            result = list(dict.fromkeys(result))

            # change into dict
            topic = []
            for res in result:
                temp = {}
                temp['article_topic'] = res[0]
                topic.append(temp)

            # check if empty
            if topic:
                return True, topic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
