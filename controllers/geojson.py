'''doc string'''
import math
import numpy
from colour import Color
import itertools
from operator import itemgetter

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html

class GeojsonController(object):
    '''doc string'''

    def create_range(self, res_metadata, res_data, filter_key, filter_value, group_kode, group_value):
        '''doc string'''

        stat_multiple_by_100 = False

        # filter data
        data_temp = []
        for rd_ in res_data:
            if str(rd_[filter_key]) == str(filter_value):
                data_temp.append(rd_)

        # grouping if category more than one
        data = []
        data_temp = sorted(data_temp, key=itemgetter(group_kode))
        for key, value in itertools.groupby(data_temp, key=itemgetter(group_kode)):
            temp = {}
            temp[group_kode] = key
            temp[group_value] = 0
            for val_ in value:
                temp[group_value] += float(val_[group_value])
            # resize value if too small
            if temp[group_value] < 1:
                stat_multiple_by_100 = True


        for key, value in itertools.groupby(data_temp, key=itemgetter(group_kode)):
            temp = {}
            temp[group_kode] = key
            temp[group_value] = 0
            for val_ in value:
                for md_ in res_metadata:
                    if md_ != group_value and md_ != group_kode:
                        temp[md_] = val_[md_]
                temp[group_value] += float(val_[group_value])
            # resize value if too small
            if stat_multiple_by_100:
                temp[group_value] = round(temp[group_value] * 100, 2)
            else:
                temp[group_value] = round(temp[group_value], 2)
            data.append(temp)

        # define data for generate cluster
        arr_tobe_percentile = []
        for row in data:
            arr_tobe_percentile.append(row[group_value])
        arr_tobe_percentile.sort()
        arr_tobe_percentile = list(filter(lambda num: num != 0, arr_tobe_percentile))

        # prepare hierarchical clustering
        count = 5
        is_duplicate = False
        arr_percentile = []

        if len(arr_tobe_percentile) > 1:
            # looping hierarchical clustering
            while count >= 1:
                # define differencial per cluster
                diff = 100 / count

                # count percentile
                for j in range(count+1):
                    perc = math.ceil(numpy.percentile(arr_tobe_percentile, j * diff))
                    arr_percentile.append(perc)

                # check if data percentile is duplicate (not valid)
                is_duplicate = self.check_if_duplicates(arr_percentile)
                if is_duplicate:
                    # if duplicate then looping again (hierarchical)
                    count = count - 1
                    arr_tobe_percentile = arr_percentile
                    arr_percentile = []
                else:
                    # if valid then finish
                    break
        elif len(arr_tobe_percentile) == 1:
            # define hierarchical clustering
            arr_percentile = arr_tobe_percentile
        else:
            pass

        # create range color
        color = ['#ddffed', '#006430']

        if len(arr_percentile) > 1:
            colors = list(Color(color[0]).range_to(Color(color[1]), len(arr_percentile)-1))

            # create range
            rangelist = []
            rangelist.append({"from": 0, "to": 0, "color": '#FFFFFFFF', "total_cluster": 0})
            for i in range(len(arr_percentile)-1):
                if i == 0:
                    rangelist.append({"from": arr_percentile[i], "to": arr_percentile[i + 1], "color": colors[i].hex, "total_cluster": 0})
                else:
                    rangelist.append({"from": arr_percentile[i]+1, "to": arr_percentile[i + 1], "color": colors[i].hex, "total_cluster": 0})
        elif len(arr_percentile) == 1:
            colors = list(Color(color[0]).range_to(Color(color[1]), 1))

            # create range
            rangelist = []
            rangelist.append({"from": 0, "to": 0, "color": '#FFFFFFFF', "total_cluster": 0})
            rangelist.append({"from": arr_percentile[0], "to": arr_percentile[0], "color": colors[0].hex, "total_cluster": 0})
        else:
            # create range
            rangelist = []
            rangelist.append({"from": 0, "to": 0, "color": '#FFFFFFFF', "total_cluster": 0})

        # add value & color to data
        result = []
        for k in range(len(data)):
            temp = {}
            temp = data[k]
            temp['value'] = data[k][group_value]
            for j in range(len(rangelist)):
                if temp['value'] >= rangelist[j]['from'] and temp['value'] <= rangelist[j]['to']:
                    temp['color'] = rangelist[j]['color']
                    rangelist[j]['total_cluster'] += 1
            result.append(temp)

        # restore number value
        result_final = []
        for res in result:
            if stat_multiple_by_100:
                res['value'] = res['value'] / 100
                res[group_value] = res[group_value] / 100
            result_final.append(res)

        rangelist_final = []
        for rl in rangelist:
            if stat_multiple_by_100:
                rl['from'] = rl['from'] / 100
                rl['to'] = rl['to'] / 100
            rangelist_final.append(rl)


        return result_final, rangelist_final


    def check_if_duplicates(self, data):
        '''doc string'''
        if len(data) == len(set(data)):
            return False
        else:
            return True


    def generate_range(self, data):
        '''doc string'''

        # define data for generate cluster
        arr_tobe_percentile = []
        for row in data:
            arr_tobe_percentile.append(row['value'])
        arr_tobe_percentile.sort()
        arr_tobe_percentile = list(filter(lambda num: num != 0, arr_tobe_percentile))

        # prepare hierarchical clustering
        count = 5
        is_duplicate = False
        arr_percentile = []

        if len(arr_tobe_percentile) > 1:
            # looping hierarchical clustering
            while count >= 1:
                # define differencial per cluster
                diff = 100 / count

                # count percentile
                for j in range(count+1):
                    perc = math.ceil(numpy.percentile(arr_tobe_percentile, j * diff))
                    arr_percentile.append(perc)

                # check if data percentile is duplicate (not valid)
                is_duplicate = self.check_if_duplicates(arr_percentile)
                if is_duplicate:
                    # if duplicate then looping again (hierarchical)
                    count = count - 1
                    arr_tobe_percentile = arr_percentile
                    arr_percentile = []
                else:
                    # if valid then finish
                    break
        elif len(arr_tobe_percentile) == 1:
            # define hierarchical clustering
            arr_percentile = arr_tobe_percentile
        else:
            pass

        # create range color
        color = ['#ddffed', '#006430']

        if len(arr_percentile) > 1:
            colors = list(Color(color[0]).range_to(Color(color[1]), len(arr_percentile)-1))
            # create range
            rangelist = []
            rangelist.append({"from": 0, "to": 0, "color": '#FFFFFFFF', "total_cluster": 0})
            for i in range(len(arr_percentile)-1):
                if i == 0:
                    rangelist.append({"from": arr_percentile[i], "to": arr_percentile[i + 1], "color": colors[i].hex, "total_cluster": 0})
                else:
                    rangelist.append({"from": arr_percentile[i]+1, "to": arr_percentile[i + 1], "color": colors[i].hex, "total_cluster": 0})
        elif len(arr_percentile) == 1:
            colors = list(Color(color[0]).range_to(Color(color[1]), 1))
            # create range
            rangelist = []
            rangelist.append({"from": 0, "to": 0, "color": '#FFFFFFFF', "total_cluster": 0})
            rangelist.append({"from": arr_percentile[0], "to": arr_percentile[0], "color": colors[0].hex, "total_cluster": 0})
        else:
            # create range
            rangelist = []
            rangelist.append({"from": 0, "to": 0, "color": '#FFFFFFFF', "total_cluster": 0})

        # add value & color to data
        result = []
        for k in range(len(data)):
            temp = {}
            temp = data[k]
            for j in range(len(rangelist)):
                if temp['value'] >= rangelist[j]['from'] and temp['value'] <= rangelist[j]['to']:
                    temp['color'] = rangelist[j]['color']
                    rangelist[j]['total_cluster'] += 1
            result.append(temp)

        return result, rangelist
