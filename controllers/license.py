''' Doc: class license  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import LicenseModel
from models import DatasetModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import distinct
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class LicenseController(object):
    ''' Doc: class license  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(LicenseModel)
            for attr, value in where.items():
                result = result.filter(getattr(LicenseModel, attr) == value)
            result = result.filter(or_(cast(getattr(LicenseModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in LicenseModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            license_ = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                license_.append(temp)

            # check if empty
            license_ = list(license_)
            if license_:
                return True, license_
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_distinct_skpd(self, kode_skpd, where: dict, sort):
        ''' Doc: function get dinstinc skpd  '''
        try:
            # query postgresql
            result = db.session.query(distinct(DatasetModel.license_id).label('id'), LicenseModel.name.label('name'))
            result = result.join(LicenseModel)
            result = result.filter(getattr(DatasetModel, "kode_skpd") == kode_skpd)
            result = result.filter(getattr(DatasetModel, "is_deleted") == False)
            for attr, value in where.items():
                result = result.filter(getattr(DatasetModel, attr) == value)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.all()

            licenses = []
            for res in result:
                # get relation to dataset_tag
                temp = db.session.query(LicenseModel)
                temp = temp.get(res[0])

                if temp:
                    temp = temp.__dict__
                    temp.pop('_sa_instance_state', None)
                else:
                    temp = {}

                licenses.append(temp)

            # check if empty
            licenses = list(licenses)
            if licenses:
                return True, licenses
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(LicenseModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(LicenseModel, attr) == value)
            result = result.filter(or_(cast(getattr(LicenseModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in LicenseModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(LicenseModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            license_ = result

            # check if empty
            if license_:
                return True, license_
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            # prepare data model
            result = LicenseModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, license_ = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, license_
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(LicenseModel)
                for attr, value in where.items():
                    result = result.filter(getattr(LicenseModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, license_ = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, license_
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(LicenseModel)
                for attr, value in where.items():
                    result = result.filter(getattr(LicenseModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, license_ = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
