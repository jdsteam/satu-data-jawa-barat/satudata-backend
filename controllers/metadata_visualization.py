''' Doc: controller metadata visualization  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MetadataVisualizationModel, MetadataTypeModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MetadataVisualizationController(object):
    ''' Doc: class metadata visualization  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(MetadataVisualizationModel)
            for attr, value in where.items():
                result = result.filter(getattr(MetadataVisualizationModel, attr) == value)
            result = result.filter(or_(cast(getattr(MetadataVisualizationModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MetadataVisualizationModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            metadata_visualization = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to metadata_type
                metadata_type = db.session.query(MetadataTypeModel)
                metadata_type = metadata_type.get(temp['metadata_type_id'])

                if metadata_type:
                    metadata_type = metadata_type.__dict__
                    metadata_type.pop('_sa_instance_state', None)
                else:
                    metadata_type = {}

                temp['metadata_type'] = metadata_type

                metadata_visualization.append(temp)

            # check if empty
            metadata_visualization = list(metadata_visualization)
            if metadata_visualization:
                return True, metadata_visualization
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(MetadataVisualizationModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(MetadataVisualizationModel, attr) == value)
            result = result.filter(or_(cast(getattr(MetadataVisualizationModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MetadataVisualizationModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(MetadataVisualizationModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to metadata
                metadata_type = db.session.query(MetadataTypeModel)
                metadata_type = metadata_type.get(result['metadata_type_id'])

                if metadata_type:
                    metadata_type = metadata_type.__dict__
                    metadata_type.pop('_sa_instance_state', None)
                else:
                    metadata_type = {}

                result['metadata_type'] = metadata_type

            else:
                result = {}

            # change into dict
            metadata_visualization = result

            # check if empty
            if metadata_visualization:
                return True, metadata_visualization
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            # prepare data model
            result = MetadataVisualizationModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, metadata_visualization = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, metadata_visualization
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []

            for json in jsons:
                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = MetadataVisualizationModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, metadata_visualization = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(metadata_visualization)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            try:
                # prepare data model
                result = db.session.query(MetadataVisualizationModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MetadataVisualizationModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, metadata_visualization = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, metadata_visualization
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_multiple(self, visualization_id, json: dict):
        ''' Doc: function update multiple  '''
        try:
            # filter data
            result = db.session.query(MetadataVisualizationModel)
            result = result.filter(MetadataVisualizationModel.visualization_id == visualization_id)
            result = result.all()

            for res in result:
                db.session.delete(res)
                db.session.commit()

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = MetadataVisualizationModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, metadata_visualization = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(metadata_visualization)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(MetadataVisualizationModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MetadataVisualizationModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, metadata = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
