''' Doc: controller mapset thematic  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetThematicModel
from models import SektoralModel
from models import BidangUrusanModel
from models import MapsetThematicSourceModel
from models import MapsetThematicMetadataModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

from controllers.mapset_thematic_source import MapsetThematicSourceController
from controllers.mapset_thematic_metadata import MapsetThematicMetadataController

MAPSET_THEMATIC_SOURCE_CONTROLLER = MapsetThematicSourceController()
MAPSET_THEMATIC_METADATA_CONTROLLER = MapsetThematicMetadataController()

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MapsetThematicController(object):
    ''' Doc: class mapset thematic  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''
      
    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
              MapsetThematicModel.id, MapsetThematicModel.title, MapsetThematicModel.image, MapsetThematicModel.description, MapsetThematicModel.image, MapsetThematicModel.year, MapsetThematicModel.map_type, MapsetThematicModel.count_view, MapsetThematicModel.is_active, MapsetThematicModel.is_deleted,
              MapsetThematicModel.cdate, MapsetThematicModel.mdate, MapsetThematicModel.url, MapsetThematicModel.data_processor,
              SektoralModel.id.label('sektoral_id'), SektoralModel.name.label('sektoral_name'), SektoralModel.notes.label('sektoral_notes'),
              SektoralModel.picture.label('sektoral_picture'), SektoralModel.picture_thumbnail.label('sektoral_picture_thumbnail'),
              BidangUrusanModel.id.label('bidang_urusan_id'),
              BidangUrusanModel.kode_bidang_urusan.label('kode_bidang_urusan'),
              BidangUrusanModel.nama_bidang_urusan.label('nama_bidang_urusan')
            )
            result = result.join(
                SektoralModel, MapsetThematicModel.sektoral_id == SektoralModel.id, isouter=True)
            result = result.join(
                BidangUrusanModel, MapsetThematicModel.kode_bidang_urusan == BidangUrusanModel.kode_bidang_urusan, isouter=True)
            
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(MapsetThematicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(MapsetThematicModel, attr) == value)
                    
            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(MapsetThematicModel, "title"), sqlalchemy.String).match(src),
                        cast(getattr(MapsetThematicModel, "description"), sqlalchemy.String).match(src),
                        cast(getattr(MapsetThematicModel, "map_type"), sqlalchemy.String).match(src),
                    ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_data(self, temp):
      ''' Doc: function populate data '''
      try:
        temp['cdate'] = temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")
        temp['mdate'] = temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")
      
        # get relation to sektoral
        temp['sektoral'] = {}
        temp['sektoral']['id'] = temp['sektoral_id']
        temp['sektoral']['name'] = temp['sektoral_name']
        temp['sektoral']['picture'] = temp['sektoral_picture']
        temp['sektoral']['picture_thumbnail'] = temp['sektoral_picture_thumbnail']
        temp.pop('sektoral_name', None)
        temp.pop('sektoral_notes', None)
        temp.pop('sektoral_is_opendata', None)
        temp.pop('sektoral_is_satudata', None)
        temp.pop('sektoral_picture', None)
        temp.pop('sektoral_picture_thumbnail', None)
        
        # get relation to bidang urusan
        temp['bidang_urusan'] = {}
        temp['bidang_urusan']['id'] = temp['bidang_urusan_id']
        temp['bidang_urusan']['kode_bidang_urusan'] = temp['kode_bidang_urusan']
        temp['bidang_urusan']['nama_bidang_urusan'] = temp['nama_bidang_urusan']
        temp.pop('bidang_urusan_id', None)
        temp.pop('nama_bidang_urusan', None)
        temp.pop('kode_bidang_urusan', None)
        
        # get relation to source
        temp = self.populate_source(temp)
        
        # get relation to metadata
        temp = self.populate_metadata(temp)
        
        return temp
      except Exception as err:
        # fail response
        sentry_sdk.capture_exception(err)
        raise ErrorMessage(str(err), 500, 1, {})
    
    def populate_source(self, temp):
        ''' Doc: function populate relation source  '''
        # get relation to history_draft
        try:
            res_source, data_source = MAPSET_THEMATIC_SOURCE_CONTROLLER.get_all(
                {'mapset_thematic_id': temp['id']}, 100, 0)
            
            if res_source:
                temp['source'] = data_source
            else:
                temp['source'] = []
        except Exception:
            temp['source'] = []

        return temp
    
    def populate_metadata(self, temp):
        ''' Doc: function populate relation metadata  '''
        # get relation to history_draft
        try:
            res_metadata, data_metadata = MAPSET_THEMATIC_METADATA_CONTROLLER.get_all(
                {'mapset_thematic_id': temp['id']}, '', ['id', 'asc'], 100, 0)
            if res_metadata:
                temp['metadata'] = data_metadata
            else:
                temp['metadata'] = []
        except Exception:
            temp['metadata'] = []

        return temp
    
    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
           # change sorting key
            if sort[0] == 'mdate':
                sort[0] = "mapset_thematic.mdate"
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            mapset_thematic = []
            for res in result:
                temp = res._asdict()
                temp = self.populate_data(temp)
                mapset_thematic.append(temp)
            
            # check if empty
            mapset_thematic = list(mapset_thematic)
            if mapset_thematic:
                return True, mapset_thematic
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({"id": _id}, '')
            result = result.all()

            if result:
                result = result[0]._asdict()
                result = self.populate_data(result)
                    
            else:
                result = {}

            # change into dict
            mapset_thematic = result

            # check if empty
            if mapset_thematic:
                return True, mapset_thematic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            jwt = HELPER.read_jwt()
            
            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = MapsetThematicModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, mapset_thematic = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, mapset_thematic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()
            
            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(MapsetThematicModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetThematicModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, mapset_thematic = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, mapset_thematic
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(MapsetThematicModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetThematicModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, _ = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
    
    def route_create_mapset_thematic_source(self, temp_source, mapset_thematic):
        ''' Doc: function route_create_mapset_thematic_source  '''
        try:
            for source in temp_source:
                temp = source
                temp['mapset_thematic_id'] = mapset_thematic['id']
                _, _ = MAPSET_THEMATIC_SOURCE_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))
            
    def route_update_mapset_thematic_source(self, temp_source, mapset_thematic):
        ''' Doc: function route_update_mapset_thematic_source  '''
        try:
            if temp_source:
                result_source = db.session.query(MapsetThematicSourceModel)
                result_source = result_source.filter(
                    getattr(MapsetThematicSourceModel, 'mapset_thematic_id') == mapset_thematic['id'])
                result_source = result_source.all()
                for res_source in result_source:
                    db.session.delete(res_source)
                    db.session.commit()
                # insert source
                for source in temp_source:
                    temp = source
                    temp['mapset_thematic_id'] = mapset_thematic['id']
                    # print(temp)
                    _, res_source = MAPSET_THEMATIC_SOURCE_CONTROLLER.create(
                        temp)
        except Exception as err:
            print(str(err))

    def route_create_mapset_thematic_metadata(self, temp_metadata, mapset_thematic):
        ''' Doc: function route_create_mapset_thematic_metadata  '''
        try:
            for metadata in temp_metadata:
                temp = metadata
                temp['mapset_thematic_id'] = mapset_thematic['id']
                _, _ = MAPSET_THEMATIC_METADATA_CONTROLLER.create(temp)
        except Exception as err:
            print(str(err))

    def route_update_mapset_thematic_metadata(self, temp_metadata, mapset_thematic):
        ''' Doc: function route_update_mapset_thematic_metadata  '''
        try:
            if temp_metadata:
                result_metadata = db.session.query(MapsetThematicMetadataModel)
                result_metadata = result_metadata.filter(
                    getattr(MapsetThematicMetadataModel, 'mapset_thematic_id') == mapset_thematic['id'])
                result_metadata = result_metadata.all()
                for res_metadata in result_metadata:
                    db.session.delete(res_metadata)
                    db.session.commit()
                # insert source
                for metadata in temp_metadata:
                    temp = metadata
                    temp['mapset_thematic_id'] = mapset_thematic['id']
                    # print(temp)
                    _, res_metadata = MAPSET_THEMATIC_METADATA_CONTROLLER.create(
                        temp)
        except Exception as err:
            print(str(err))

    def counter(self, _id:str):
        ''' Doc: function update counter  '''
        _id = str(_id)
        try:
            sql_query = "UPDATE mapset_thematic SET count_view = count_view + 1 WHERE id = '%s'" % (_id)
            sql = text(sql_query)
            db.engine.execute(sql)

            # check if empty
            return True, {}

        except Exception as err:
            print(err)
            # fail response
            if err.__class__.__name__  == "ProgrammingError":
                err = "column %s not exists"

            return False, err