''' Doc: controller history request public  '''
from requests.api import request
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryRequestPublicModel
from models import UserModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk
import json

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
# pylint: disable=singleton-comparison, unused-variable, unused-argument
class HistoryRequestPublicController(object):
    ''' Doc: class history request public  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(HistoryRequestPublicModel)

            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryRequestPublicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryRequestPublicModel, attr) == value)

            if search.lower() == 'persetujuan walidata':
                categorys = '1'
            elif search.lower() == 'dataset sedang diproses':
                categorys = '2'
            elif search.lower() == 'dataset tersedia':
                categorys = '4'
            elif search.lower() == 'dataset ditolak':
                categorys = '3'
            else:
                categorys = search

            result = result.filter(or_(
                cast(getattr(HistoryRequestPublicModel, "status"), sqlalchemy.String).ilike('%'+categorys+'%'),
                cast(getattr(HistoryRequestPublicModel, "notes"), sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("history_request_public."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_request = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                if temp['odj_link']:
                    if '"[' in temp['odj_link']:
                        temp['odj_link'] = temp['odj_link'].strip('"')
                        temp['odj_link'] = temp['odj_link'].replace('\\', '')
                        temp.update({"odj_link":json.loads(temp['odj_link'])})
                    elif '["' in temp['odj_link']:
                        temp.update({"odj_link":json.loads(temp['odj_link'])})

                # populate user
                user = db.session.query(UserModel)
                user = user.get(temp['cuid'])
                if user:
                    user = user.__dict__
                    temp['username'] = user['username']
                    temp['name'] = user['name']
                else:
                    temp['username'] = ''
                    temp['name'] = ''

                history_request.append(temp)

            # check if empty
            if history_request:
                return True, history_request
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # check if empty
            if result:
                return True, result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            result = self.query({'id':_id}, '')
            result = result.all()

            # change into dict
            history_request = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                if temp['odj_link']:
                    if '"[' in temp['odj_link']:
                        temp['odj_link'] = temp['odj_link'].strip('"')
                        temp['odj_link'] = temp['odj_link'].replace('\\', '')
                        temp.update({"odj_link":json.loads(temp['odj_link'])})
                    elif '["' in temp['odj_link']:
                        temp.update({"odj_link":json.loads(temp['odj_link'])})

                # populate user
                user = db.session.query(UserModel)
                user = user.get(temp['cuid'])
                if user:
                    user = user.__dict__
                    temp['username'] = user['username']
                    temp['name'] = user['name']
                else:
                    temp['username'] = ''
                    temp['name'] = ''

                history_request.append(temp)

            # check if empty
            if history_request:
                return True, history_request[0], "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, json, condition):
        ''' Doc: function post param  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["datetime"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = self.post_param(json, 'add')
            # prepare data model
            result = HistoryRequestPublicModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history_request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, history_request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = self.post_param(json, 'edit')
            try:
                # prepare data model
                result = db.session.query(HistoryRequestPublicModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryRequestPublicModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history_request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history_request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryRequestPublicModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryRequestPublicModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history_request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_download(self, start_date, end_date, sort):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            if start_date:
                cdate_start_date = "WHERE cdate::date >= '" + start_date + "'"
            else:
                cdate_start_date = ""
            if end_date:
                cdate_end_date = " and cdate::date <= '" + end_date + "'"
            else:
                cdate_end_date = ""
            sort_sql = " order by "+sort[0]+" "+sort[1]
            # change into dict
            sql = text('SELECT ticket, nama, email,telp, pekerjaan, instansi, bidang, judul_data, tau_skpd,' +
            'nama_skpd, kebutuhan_data, tujuan_data, status, cdate, mdate, notes, bersedia_dihubungi '+
            'FROM request_public_view '+ cdate_start_date + ' ' + cdate_end_date + sort_sql
            )
            result = db.engine.execute(sql)

            # check if empty
            request_public = []
            for res in result:
                temps = {}
                temps['Nomor Tiket'] = res['ticket']
                temps['Nama Pemohon'] = res['nama']
                temps['Email'] = res['email']
                temps['Telepon'] = res['telp']
                temps['Instansi'] = res['instansi']
                temps['Pekerjaan'] = res['pekerjaan']
                temps['Bidang'] = res['bidang']
                temps['Judul Data'] = res['judul_data']
                temps['Mengetahui OPD Sumber'] = res['tau_skpd']
                temps['Nama OPD'] = res['nama_skpd']
                temps['Deskripsi Kebutuhan Dataset'] = res['kebutuhan_data']
                temps['Tujuan Dataset'] = res['tujuan_data']
                temps['Status'] = res['status']
                temps['Tanggal Dibuat'] = res['cdate'].strftime(FORMAT_DATE)
                temps['Tanggal Diperbarui'] = res['mdate'].strftime(FORMAT_DATE)
                temps['Catatan Terakhir'] = res['notes']
                temps['Bersedia Dihubungi'] = res['bersedia_dihubungi']

                request_public.append(temps)

            # check if empty
            request_public = list(request_public)
            if request_public:
                return True, request_public
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

