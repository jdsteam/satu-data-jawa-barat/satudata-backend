''' Doc: controller indikator  '''
from helpers import Helper
from exceptions import ErrorMessage
from models import IndikatorModel, IndikatorCategoryModel, IndikatorHistoryModel, IndikatorClassModel, SkpdSubModel, SatuanModel, SkpdModel, DatasetModel
from models import IndikatorProgramModel
from models import IndikatorTerhubungModel
from models import IndikatorDatasetModel
from models import IndikatorDatadasarModel
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers.history import HistoryController
from controllers.indikator_progress import IndikatorProgressController
from sqlalchemy import text, or_, cast, and_
import sqlalchemy, sentry_sdk
import re
from flask import send_file
import helpers.postgre_psycopg_bigdata as db2

HISTORY_CONTROLLER = HistoryController()
INDIKATOR_PROGRESS_CONTROLLER = IndikatorProgressController()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d"
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument
class IndikatorController(object):
    ''' Doc: class indikator  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                IndikatorModel.id, IndikatorModel.name, IndikatorModel.title, IndikatorModel.dataset_type_id,
                IndikatorModel.indikator_category_id, IndikatorModel.kode_skpd, IndikatorModel.notes,
                IndikatorModel.datetime, IndikatorModel.cdate, IndikatorModel.mdate, IndikatorModel.is_active,
                IndikatorModel.is_deleted, IndikatorModel.count_view, IndikatorModel.rumus, IndikatorModel.data_dasar,
                IndikatorModel.count_access, IndikatorModel.satuan_id, IndikatorModel.kode_skpd, IndikatorModel.description,
                IndikatorModel.source, IndikatorModel.interpretation, IndikatorModel.kode_skpd_sub,
                IndikatorCategoryModel.name.label('category_name'), SkpdSubModel.nama_skpdsub,
                SatuanModel.name.label('satuan_name'), SkpdModel.nama_skpd_alias,
                SkpdModel.nama_skpd, SkpdModel.phone, SkpdModel.email, SkpdModel.logo,
                SkpdModel.id.label('id_skpd')
            )
            result = result.outerjoin(IndikatorCategoryModel, IndikatorModel.indikator_category_id == IndikatorCategoryModel.id)
            result = result.outerjoin(
                SkpdSubModel, and_(
                    IndikatorModel.kode_skpd == SkpdSubModel.kode_skpd,
                    IndikatorModel.kode_skpd_sub == SkpdSubModel.kode_skpdsub
                )
            )
            result = result.outerjoin(SatuanModel, IndikatorModel.satuan_id == SatuanModel.id)
            result = result.outerjoin(SkpdModel, IndikatorModel.kode_skpd == SkpdModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(IndikatorModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(IndikatorModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(IndikatorModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                        cast(getattr(IndikatorModel, "kode_skpd"), sqlalchemy.String).ilike('%'+src+'%'),
                        cast(getattr(SkpdSubModel, "nama_skpdsub"), sqlalchemy.String).ilike('%'+src+'%')
                    ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def history_class(self, _id):
        ''' Doc: function history class  '''
        try:
            # get relation to history class
            indikator_history = db.session.query(IndikatorHistoryModel.indikator_class_id.label('id_class'), IndikatorClassModel.name)
            indikator_history = indikator_history.join(
                IndikatorClassModel,
                IndikatorHistoryModel.indikator_class_id == IndikatorClassModel.id
            )
            indikator_history = indikator_history.filter(IndikatorHistoryModel.indikator_id == _id)
            indikator_history = indikator_history.all()

            return indikator_history
        except Exception as err:
            return str(err)

    def datadasar(self, _id):
        ''' Doc: function history class  '''
        try:
            # get relation to history class
            datadasar = db.session.query(IndikatorDatadasarModel)
            datadasar = datadasar.filter(IndikatorDatadasarModel.indikator_id == _id)
            datadasar = datadasar.all()

            return datadasar
        except Exception as err:
            return str(err)

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # change sorting key
            if sort[0] == 'organization':
                sort[0] = "skpd.nama_skpd"
            elif sort[0] == 'category':
                sort[0] = "indikator_category.name"
            elif sort[0] == 'release-month':
                sort[0] = "indikator.datetime"
            elif sort[0] == 'mdate':
                sort[0] = "indikator.mdate"
            elif sort[0] == 'cdate':
                sort[0] = "indikator.cdate"
            else:
                sort[0] = "indikator." + sort[0]

            # query postgresql
            if 'classification' not in where:
                result = self.query(where, search)
                result = result.order_by(text(sort[0]+" "+sort[1]))
                result = result.offset(skip).limit(limit)
                result = result.all()

                # change into dict
                indikator = self.populate_without_classification(result)

            else:
                value = where['classification']
                is_array = isinstance(value, list)
                # query postgresql
                result = db.session.query(IndikatorClassModel)
                if is_array:
                    result = result.filter(or_(IndikatorClassModel.name.ilike('%'+val+'%') for val in value))
                else:
                    result = result.filter(IndikatorClassModel.name.ilike('%'+value+'%'))

                # change into dict
                indikator = self.populate_with_classification(result, where, search, sort, limit, skip)

            # check if empty
            remove_duplicate = [i for n, i in enumerate(indikator) if i not in indikator[n + 1:]]
            if(sort[1] == 'ASC') or (sort[1] == 'asc'):
                sort[1] = False
            else:
                sort[1] = True
            if sort[0] == 'id' or sort[0] == 'count_view':
                remove_duplicate = sorted(remove_duplicate, key=lambda k: k.get(sort[0], 0), reverse=sort[1])
            else:
                remove_duplicate = sorted(remove_duplicate, key=lambda k: str(k.get(sort[0], 0)).lower(), reverse=sort[1])
            if remove_duplicate:
                return True, remove_duplicate
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_without_classification(self, result):
        ''' Doc: function populate_without_classification  '''
        indikator = []
        for res in result:
            temp = res._asdict()
            temp.update({"datetime":temp['datetime'].strftime(FORMAT_DATE)})
            temp.update({"cdate":temp['cdate'].strftime(FORMAT_DATETIME)})
            temp.update({"mdate":temp['mdate'].strftime(FORMAT_DATETIME)})

            # get relation to history class
            indikator_history = self.history_class(temp['id'])
            in_class = []
            in_class2 = []
            for ih_ in indikator_history:
                row = ih_._asdict()
                in_class.append(row)
                in_class2.append(row['name'])
            temp['history_class'] = in_class
            temp['history_class_string'] = ','.join(in_class2)

            # get relation to datadasar
            datadasar = self.datadasar(temp['id'])
            datadasars = []
            for dd in datadasar:
                row = dd.__dict__
                row.pop('_sa_instance_state', None)
                row.pop('cuid', None)
                row.pop('cdate', None)
                row.pop('notes', None)
                datadasars.append(row)
            temp['data_dasar_array'] = datadasars

            temp = self.get_relation_indikator(temp)

            temp = self.populate_relation_indikator(temp)

            indikator.append(temp)
        return indikator

    def populate_with_classification(self, result, where, search, sort, limit, skip):
        ''' Doc: function populate_with_classification  '''
        indikator = []
        for res in result:
            temp = res.__dict__
            result_history = db.session.query(IndikatorHistoryModel)
            result_history = result_history.distinct(IndikatorHistoryModel.indikator_id)
            result_history = result_history.filter(IndikatorHistoryModel.indikator_class_id == temp['id'])
            # result_history = result_history.offset(skip).limit(limit)
            result_history = result_history.all()

            for rh_ in result_history:
                temp_history = rh_.__dict__
                where.pop('classification', None)
                where['id'] = temp_history['indikator_id']
                result_in = self.query(where, search)
                result_in = result_in.order_by(text(sort[0]+" "+sort[1]))
                result_in = result_in.all()

                for i in result_in:
                    temp_in = i._asdict()
                    temp_in.update({"datetime":temp_in['datetime'].strftime(FORMAT_DATE)})
                    temp_in.update({"cdate":temp_in['cdate'].strftime(FORMAT_DATETIME)})
                    temp_in.update({"mdate":temp_in['mdate'].strftime(FORMAT_DATETIME)})

                    # get relation to history class
                    indikator_history = self.history_class(temp_in['id'])
                    in_class = []
                    in_class2 = []
                    for j in indikator_history:
                        row = j._asdict()
                        in_class.append(row)
                        in_class2.append(row['name'])
                    temp_in['history_class'] = in_class
                    temp_in['history_class_string'] = ','.join(in_class2)

                    # get relation to datadasar
                    datadasar = self.datadasar(temp_in['id'])
                    datadasars = []
                    for dd in datadasar:
                        row = dd.__dict__
                        row.pop('_sa_instance_state', None)
                        row.pop('cuid', None)
                        row.pop('cdate', None)
                        row.pop('notes', None)
                        datadasars.append(row)
                    temp_in['data_dasar_array'] = datadasars

                    temp_in = self.get_relation_indikator(temp_in)

                    temp_in = self.populate_relation_indikator(temp_in)

                    indikator.append(temp_in)

        return indikator

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            datas = {}
            datas['count'] = 0

            if 'classification' not in where:
                # query postgresql
                result = self.query(where, search)
                result = result.count()

                # change into dict
                datas['count'] = result
            else:
                value = where['classification']
                is_array = isinstance(value, list)
                # query postgresql
                result = db.session.query(IndikatorClassModel)
                if is_array:
                    result = result.filter(or_(IndikatorClassModel.name.ilike('%'+val+'%') for val in value))
                else:
                    result = result.filter(IndikatorClassModel.name.ilike('%'+value+'%'))

                # change into dict
                indikator = 0
                for res in result:
                    temp = res.__dict__
                    result_history = db.session.query(IndikatorHistoryModel)
                    result_history = result_history.distinct(IndikatorHistoryModel.indikator_id)
                    result_history = result_history.filter(IndikatorHistoryModel.indikator_class_id == temp['id'])
                    result_history = result_history.all()

                    for rh_ in result_history:
                        temp_history = rh_.__dict__
                        where.pop('classification', None)
                        where['id'] = temp_history['indikator_id']
                        result_in = self.query(where, search)
                        result_in = result_in.count()
                        indikator = indikator + result_in

                    datas['count'] = indikator

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id, where: dict):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id':_id}, '')
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(IndikatorModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(IndikatorModel, attr) == value)

            result = result.all()

            indikator = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime(FORMAT_DATE)})
                temp.update({"cdate":temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate":temp['mdate'].strftime(FORMAT_DATETIME)})

                # get relation to history class
                indikator_history = self.history_class(temp['id'])
                in_class = []
                in_class2 = []
                for ih_ in indikator_history:
                    row = ih_._asdict()
                    in_class.append(row)
                    in_class2.append(row['name'])
                temp['history_class'] = in_class
                temp['history_class_string'] = ','.join(in_class2)

                # get relation to datadasar
                datadasar = self.datadasar(temp['id'])
                datadasars = []
                for dd in datadasar:
                    row = dd.__dict__
                    row.pop('_sa_instance_state', None)
                    row.pop('cuid', None)
                    row.pop('cdate', None)
                    row.pop('notes', None)
                    datadasars.append(row)
                temp['data_dasar_array'] = datadasars

                temp = self.get_relation_indikator(temp)

                temp = self.populate_relation_indikator(temp)

                indikator.append(temp)

            # check if empty
            if indikator:
                return True, indikator, "Get detail data successfull"
                # for change object to array
                # return True, next(x for x in indikator) , "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, json, condition):
        ''' Doc: function post param  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            if 'name' in json:
                # title
                json_send["title"] = json_send["name"].lower().replace(' ', '-')
                json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
                res_count = db.session.query(IndikatorModel.id)
                res_count = res_count.filter(IndikatorModel.title == json_send["title"])
                res_count = res_count.count()
                if res_count > 0:
                    json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            if condition == "add":
                json_send["cuid"] = jwt['id']
                json_send["cdate"] = HELPER.local_date_server()
            else:
                if 'count_view' in json or 'count_access' in json:
                    json_send = json
                else:
                    json_send["muid"] = jwt['id']
                    json_send["mdate"] = HELPER.local_date_server()

            json_send["dataset_type_id"] = 4
            json_send["regional_id"] = 1

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # prepare data model
            param = self.post_param(json, 'add')
            result = IndikatorModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, indikator, message = self.get_by_id(result['id'], {})

            # check if exist
            if res:
                return True, indikator
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            param = self.post_param(json, 'edit')

            try:
                # prepare data model
                result = db.session.query(IndikatorModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorModel, attr) == value)

                # execute database
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()
                res, request, msg = self.get_by_id(where["id"], {})

                # check if empty
                if res:
                    return True, request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(IndikatorModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorModel, attr) == value)
                result = result.delete()

                # execute database
                db.session.commit()

                return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def counter(self, category, indikator):
        ''' Doc: function counter'''
        try:
            if category == 'view':
                count_view = indikator['count_view']
                if not count_view:
                    count_view = '0'
                count_view = int(count_view) + 1
                payload = {'count_view': int(count_view)}
                indikator['count_view'] = count_view
            elif category == 'access':
                count_access = indikator['count_access']
                if not count_access:
                    count_access = '0'
                count_access = int(count_access) + 1
                payload = {'count_access': int(count_access)}
                indikator['count_access'] = count_access
            where = {'id': int(id)}
            res, data1 = self.update(where, payload)
        except Exception as err:
            print(err)

    def insert_history(self, category):
        ''' Doc: function insert_history'''
        try:
            jwt = HELPER.read_jwt()
            if category == 'view':
                payload = {}
                payload['type'] = 'indikator'
                payload['type_id'] = id
                payload['user_id'] = jwt['id']
                payload['category'] = 'view'
                res2, data2 = HISTORY_CONTROLLER.create(payload)
            elif category == 'access':
                payload = {}
                payload['type'] = 'indikator'
                payload['type_id'] = id
                payload['user_id'] = jwt['id']
                payload['category'] = 'access'
                res2, data2 = HISTORY_CONTROLLER.create(payload)
        except Exception as err:
            print(err)

    def column_first(self, _id):
        ''' Doc: function column first '''
        try:
            result_awal = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id, 'index':'awal'}, ['year', 'asc'])
            res, data, message = self.get_by_id(_id, {})
            ouput_data = []
            for row in data:
                if result_awal:
                    for ra_ in result_awal:
                        row.update({'Kondisi Awal '+str(ra_['year']):ra_['capaian']})
                row.pop('id', None)
                row.pop('id_skpd', None)
                row.pop('name', None)
                row.pop('title', None)
                row.pop('dataset_type_id', None)
                row.pop('indikator_category_id', None)
                row.pop('kode_skpd', None)
                row.pop('notes', None)
                row.pop('datetime', None)
                row.pop('cdate', None)
                row.pop('mdate', None)
                row.pop('is_active', None)
                row.pop('is_deleted', None)
                row.pop('count_view', None)
                row.pop('rumus', None)
                row.pop('data_dasar', None)
                row.pop('count_access', None)
                row.pop('satuan_id', None)
                row.pop('category_name', None)
                row.pop('kode_skpd_sub', None)
                row.pop('nama_skpdsub', None)
                row.pop('satuan_name', None)
                row.pop('nama_skpd_alias', None)
                row.pop('nama_skpd', None)
                row.pop('history_class', None)
                row.pop('history_class_string', None)
                row.pop('description', None)
                row.pop('phone', None)
                row.pop('email', None)
                row.pop('logo', None)

                ouput_data.append(row)

            col_first = []
            for exc in next(x for x in sorted(ouput_data)):
                col_first.append(exc)

            return HELPER.sort_list(col_first)

        except Exception as err:

            return str(err)

    def column_target(self, _id):
        ''' Doc: function column target '''
        try:
            result_akhir = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id}, ['year', 'asc'])
            res, data, message = self.get_by_id(_id, {})
            ouput_data = []
            for row in data:
                if result_akhir:
                    for target in result_akhir:
                        row.update({'Target '+str(target['year']):target['target']})
                row.pop('id', None)
                row.pop('id_skpd', None)
                row.pop('name', None)
                row.pop('title', None)
                row.pop('dataset_type_id', None)
                row.pop('indikator_category_id', None)
                row.pop('kode_skpd', None)
                row.pop('notes', None)
                row.pop('datetime', None)
                row.pop('cdate', None)
                row.pop('mdate', None)
                row.pop('is_active', None)
                row.pop('is_deleted', None)
                row.pop('count_view', None)
                row.pop('rumus', None)
                row.pop('data_dasar', None)
                row.pop('count_access', None)
                row.pop('satuan_id', None)
                row.pop('category_name', None)
                row.pop('kode_skpd_sub', None)
                row.pop('nama_skpdsub', None)
                row.pop('satuan_name', None)
                row.pop('nama_skpd_alias', None)
                row.pop('nama_skpd', None)
                row.pop('history_class', None)
                row.pop('history_class_string', None)
                row.pop('description', None)
                row.pop('phone', None)
                row.pop('email', None)
                row.pop('logo', None)

                ouput_data.append(row)

            col_target = []
            for exc in next(x for x in sorted(ouput_data)):
                col_target.append(exc)

            return HELPER.sort_list(col_target)

        except Exception as err:

            return str(err)

    def column_result(self, _id):
        ''' Doc: function column result '''
        try:
            result_akhir = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id}, ['year', 'asc'])
            res, data, message = self.get_by_id(_id, {})
            ouput_data = []
            for row in data:
                if result_akhir:
                    for target in result_akhir:
                        row.update({'Pencapaian '+str(target['year']):target['capaian']})
                row.pop('id', None)
                row.pop('id_skpd', None)
                row.pop('name', None)
                row.pop('title', None)
                row.pop('dataset_type_id', None)
                row.pop('indikator_category_id', None)
                row.pop('kode_skpd', None)
                row.pop('notes', None)
                row.pop('datetime', None)
                row.pop('cdate', None)
                row.pop('mdate', None)
                row.pop('is_active', None)
                row.pop('is_deleted', None)
                row.pop('count_view', None)
                row.pop('rumus', None)
                row.pop('data_dasar', None)
                row.pop('count_access', None)
                row.pop('satuan_id', None)
                row.pop('category_name', None)
                row.pop('kode_skpd_sub', None)
                row.pop('nama_skpdsub', None)
                row.pop('satuan_name', None)
                row.pop('nama_skpd_alias', None)
                row.pop('nama_skpd', None)
                row.pop('history_class', None)
                row.pop('history_class_string', None)
                row.pop('description', None)
                row.pop('phone', None)
                row.pop('email', None)
                row.pop('logo', None)

                ouput_data.append(row)

            col_result = []
            for exc in next(x for x in sorted(ouput_data)):
                col_result.append(exc)

            return HELPER.sort_list(col_result)

        except Exception as err:

            return str(err)

    def column_last(self, _id):
        ''' Doc: function column last '''
        try:
            last = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id}, ['index', 'desc'], 1, True)
            res, data, message = self.get_by_id(_id, {})
            ouput_data = []
            for row in data:
                if last:
                    row.update({'Kondisi Akhir':last[0]['capaian']})
                row.pop('id', None)
                row.pop('id_skpd', None)
                row.pop('name', None)
                row.pop('title', None)
                row.pop('dataset_type_id', None)
                row.pop('indikator_category_id', None)
                row.pop('kode_skpd', None)
                row.pop('notes', None)
                row.pop('datetime', None)
                row.pop('cdate', None)
                row.pop('mdate', None)
                row.pop('is_active', None)
                row.pop('is_deleted', None)
                row.pop('count_view', None)
                row.pop('rumus', None)
                row.pop('data_dasar', None)
                row.pop('count_access', None)
                row.pop('satuan_id', None)
                row.pop('category_name', None)
                row.pop('kode_skpd_sub', None)
                row.pop('nama_skpdsub', None)
                row.pop('satuan_name', None)
                row.pop('nama_skpd_alias', None)
                row.pop('nama_skpd', None)
                row.pop('history_class', None)
                row.pop('history_class_string', None)
                row.pop('description', None)
                row.pop('phone', None)
                row.pop('email', None)
                row.pop('logo', None)

                ouput_data.append(row)

            col_last = []
            for exc in next(x for x in sorted(ouput_data)):
                col_last.append(exc)

            return HELPER.sort_list(col_last)

        except Exception as err:

            return str(err)

    def column_title(self):
        ''' Doc: function column title '''
        return [
            'No', 'Judul Indikator', 'Klasifikasi Indikator', 'Dinas/Badan',
            'Bidang', 'Satuan', 'Rumus', 'Data Dasar', 'Kategori',
            'Keterangan', 'Tanggal Rilis'
        ]

    def update_count_access(self, row, skpd_kode):
        ''' Doc: function update_count_access '''
        if row['kode_skpd'] != skpd_kode:
            count_access = row['count_access']
            count_access = int(count_access) + 1
            where = {'id': int(row['id'])}
            payload = {'count_access': int(count_access)}
            self.update(where, payload)

    def export_bulk(self, data, skpd_kode):
        ''' Doc: function export bulk '''
        try:
            ouput_data = []
            number = 0
            for row in data:
                number += 1
                row['No'] = number
                row['Judul Indikator'] = row['name']
                row['Klasifikasi Indikator'] = row['history_class_string']
                row['Dinas/Badan'] = row['nama_skpd']
                row['Bidang'] = row['nama_skpdsub']
                row['Satuan'] = row['satuan_name']
                row['Rumus'] = row['rumus']
                row['Data Dasar'] = row['data_dasar']
                row['Kategori'] = row['category_name']
                row['Keterangan'] = row['description']
                row['Tanggal Rilis'] = row['datetime']

                self.update_count_access(row, skpd_kode)

                result_awal = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':row['id'], 'index':'awal'}, ['year', 'asc'])
                result_akhir = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':row['id']}, ['year', 'asc'])
                last = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':row['id']}, ['index', 'desc'], 1, True)

                if result_awal:
                    for ra_ in result_awal:
                        row.update({'Kondisi Awal '+str(ra_['year']):ra_['capaian']})
                if result_akhir:
                    for target in result_akhir:
                        row.update({'Target '+str(target['year']):target['target']})
                        row.update({'Pencapaian '+str(target['year']):target['capaian']})
                if last:
                    row.update({'Kondisi Akhir':last[0]['capaian']})

                row.pop('id', None)
                row.pop('id_skpd', None)
                row.pop('name', None)
                row.pop('title', None)
                row.pop('dataset_type_id', None)
                row.pop('indikator_category_id', None)
                row.pop('kode_skpd', None)
                row.pop('notes', None)
                row.pop('datetime', None)
                row.pop('cdate', None)
                row.pop('mdate', None)
                row.pop('is_active', None)
                row.pop('is_deleted', None)
                row.pop('count_view', None)
                row.pop('rumus', None)
                row.pop('data_dasar', None)
                row.pop('count_access', None)
                row.pop('satuan_id', None)
                row.pop('category_name', None)
                row.pop('kode_skpd_sub', None)
                row.pop('nama_skpdsub', None)
                row.pop('satuan_name', None)
                row.pop('nama_skpd_alias', None)
                row.pop('nama_skpd', None)
                row.pop('history_class', None)
                row.pop('history_class_string', None)
                row.pop('description', None)
                row.pop('phone', None)
                row.pop('email', None)
                row.pop('logo', None)

                ouput_data.append(row)

            return ouput_data

        except Exception as err:

            return str(err)

    def export_by_id(self, _id, data):
        ''' Doc: function export by id '''
        try:
            result_awal = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id, 'index':'awal'}, ['year', 'asc'])
            result_akhir = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id}, ['year', 'asc'])
            last = INDIKATOR_PROGRESS_CONTROLLER.get_all_by_id({'indikator_id':_id}, ['index', 'desc'], 1, True)

            ouput_data = []
            number = 0
            for row in data:
                number += 1
                row['No'] = number
                row['Judul Indikator'] = row['name']
                row['Klasifikasi Indikator'] = row['history_class_string']
                row['Dinas/Badan'] = row['nama_skpd']
                row['Bidang'] = row['nama_skpdsub']
                row['Satuan'] = row['satuan_name']
                row['Rumus'] = ""
                row['Data Dasar'] = row['data_dasar']
                row['Kategori'] = row['category_name']
                row['Keterangan'] = row['description']
                row['Tanggal Rilis'] = row['datetime']
                if result_awal:
                    for ra_ in result_awal:
                        row.update({'Kondisi Awal '+str(ra_['year']):ra_['capaian']})
                if result_akhir:
                    for target in result_akhir:
                        row.update({'Target '+str(target['year']):target['target']})
                        row.update({'Pencapaian '+str(target['year']):target['capaian']})
                if last:
                    row.update({'Kondisi Akhir':last[0]['capaian']})
                row.pop('id', None)
                row.pop('id_skpd', None)
                row.pop('name', None)
                row.pop('title', None)
                row.pop('dataset_type_id', None)
                row.pop('indikator_category_id', None)
                row.pop('kode_skpd', None)
                row.pop('notes', None)
                row.pop('datetime', None)
                row.pop('cdate', None)
                row.pop('mdate', None)
                row.pop('is_active', None)
                row.pop('is_deleted', None)
                row.pop('count_view', None)
                row.pop('rumus', None)
                row.pop('data_dasar', None)
                row.pop('count_access', None)
                row.pop('satuan_id', None)
                row.pop('category_name', None)
                row.pop('nama_skpdsub', None)
                row.pop('kode_skpd_sub', None)
                row.pop('satuan_name', None)
                row.pop('nama_skpd_alias', None)
                row.pop('nama_skpd', None)
                row.pop('history_class', None)
                row.pop('history_class_string', None)
                row.pop('description', None)
                row.pop('phone', None)
                row.pop('email', None)
                row.pop('logo', None)

                ouput_data.append(row)

            return ouput_data

        except Exception as err:

            return str(err)

    def get_program_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            sql = '''
                select id_program, upper(kode_program) as kode_program, upper(nama_program) as nama_program
                from sipd.ebudgeting_data_belanja_v2
                where 1=1
            '''

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += f'''AND nama_program ILIKE '%{search}%' '''

            sql += f'''GROUP BY id_program, kode_program, nama_program '''

            if sort:
                sql += f'''ORDER BY {str(sort[0])} {str(sort[1])} '''

            sql += f'''OFFSET {str(skip)} LIMIT {str(limit)} '''

            # execute the query with bind parameters
            success, result = db2.query_dict_array(sql)

            if success:
                # change into dict
                datas = []
                for res in result:
                    temp = {}
                    temp['kode_program'] = res['kode_program']
                    temp['nama_program'] = res['nama_program']
                    temp['id_program'] = res['id_program']
                    datas.append(temp)

                # check if empty
                datas = list(datas)
                if datas:
                    return True, datas
                else:
                    return False, []
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_program_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            sql = '''
                select count(DISTINCT concat(upper(kode_program), upper(nama_program)))
                from sipd.ebudgeting_data_belanja_v2
                where 1=1
            '''

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += f'''AND nama_program ILIKE '%{search}%' '''

            # execute the query with bind parameters
            success, result = db2.query_dict_single(sql)

            if success:
                # change into dict
                datas = result

                # check if empty
                if datas:
                    return True, datas
                else:
                    return False, []
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_terhubung_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            result = db.session.query(IndikatorModel.id, IndikatorModel.name, IndikatorModel.is_active, IndikatorModel.is_deleted)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(IndikatorModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(IndikatorModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(IndikatorModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                    ))

            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            datas = []
            for res in result:
                temp = res._asdict()
                datas.append(temp)

            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_terhubung_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            datas = {}
            datas['count'] = 0

            result = db.session.query(IndikatorModel.id, IndikatorModel.name, IndikatorModel.is_active, IndikatorModel.is_deleted)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(IndikatorModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(IndikatorModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(IndikatorModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                    ))

            # change into dict
            result = result.count()
            datas['count'] = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_dataset_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            result = db.session.query(DatasetModel.id, DatasetModel.name, DatasetModel.is_active, DatasetModel.is_deleted, DatasetModel.is_validate)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(DatasetModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(DatasetModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(DatasetModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                    ))

            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            datas = []
            for res in result:
                temp = res._asdict()
                if temp['is_validate'] == 0:
                    temp['validate'] = 'draft'
                elif temp['is_validate'] == 1:
                    temp['validate'] = 'waiting'
                elif temp['is_validate'] == 2:
                    temp['validate'] = 'revisi'
                elif temp['is_validate'] == 3:
                    temp['validate'] = 'approve'
                elif temp['is_validate'] == 4:
                    temp['validate'] = 'edit'
                else:
                    temp['validate'] = ''
                datas.append(temp)

            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_dataset_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            datas = {}
            datas['count'] = 0

            result = db.session.query(DatasetModel.id, DatasetModel.name, DatasetModel.is_active, DatasetModel.is_deleted, DatasetModel.is_validate)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(DatasetModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(DatasetModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(DatasetModel, "name"), sqlalchemy.String).ilike('%'+src+'%'),
                    ))

            # change into dict
            result = result.count()
            datas['count'] = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_relation_indikator(self, temp):
        ''' Doc: function get all  '''
        try:
            # get relation to program
            temp['indikator_program'] =  []
            indikator_program = db.session.query(IndikatorProgramModel.indikator_id, IndikatorProgramModel.kode_program)
            indikator_program = indikator_program.filter(getattr(IndikatorProgramModel, 'indikator_id') == temp['id'])
            indikator_program = indikator_program.all()
            for ip in indikator_program:
                ip = ip._asdict()
                tip = {}
                tip['indikator_id'] = ip['indikator_id']
                tip['kode_program'] = ip['kode_program']
                temp['indikator_program'].append(tip)

            # get relation to terhubung
            temp['indikator_terhubung'] =  []
            indikator_terhubung = db.session.query(IndikatorTerhubungModel.indikator_id, IndikatorTerhubungModel.indikator_terhubung_id)
            indikator_terhubung = indikator_terhubung.filter(getattr(IndikatorTerhubungModel, 'indikator_id') == temp['id'])
            indikator_terhubung = indikator_terhubung.all()
            for it in indikator_terhubung:
                it = it._asdict()
                tit = {}
                tit['indikator_id'] = it['indikator_id']
                tit['indikator_terhubung_id'] = it['indikator_terhubung_id']
                temp['indikator_terhubung'].append(tit)

            # get relation to dataset
            temp['indikator_dataset'] =  []
            indikator_dataset = db.session.query(IndikatorDatasetModel.indikator_id, IndikatorDatasetModel.dataset_id)
            indikator_dataset = indikator_dataset.filter(getattr(IndikatorDatasetModel, 'indikator_id') == temp['id'])
            indikator_dataset = indikator_dataset.all()
            for id in indikator_dataset:
                id = id._asdict()
                tid = {}
                tid['indikator_id'] = id['indikator_id']
                tid['dataset_id'] = id['dataset_id']
                temp['indikator_dataset'].append(tid)

            return temp

        except Exception as err:
            # fail response
            return temp

    def populate_relation_indikator(self, temp):
        ''' Doc: function get all  '''
        try:
            # get relation to program
            for i, program in enumerate(temp['indikator_program']):
                result_program, data_program = self.get_program_all({'kode_program': program['kode_program']}, '', ['kode_program','ASC'], '500', 0)
                if result_program:
                    temp['indikator_program'][i]['nama_program'] = data_program[0]['nama_program']
                    temp['indikator_program'][i]['id_program'] = data_program[0]['id_program']

            # get relation to terhubung
            for j, terhubung in enumerate(temp['indikator_terhubung']):
                result_terhubung, data_terhubung = self.get_terhubung_all({'id': terhubung['indikator_terhubung_id']}, '', ['id','ASC'], '500', 0)
                if result_terhubung:
                    temp['indikator_terhubung'][j]['indikator_terhubung_nama'] = data_terhubung[0]['name']

            # get relation to dataset
            for k, dataset in enumerate(temp['indikator_dataset']):
                result_dataset, data_dataset = self.get_dataset_all({'id': dataset['dataset_id']}, '', ['id','ASC'], '5000', 0)
                if result_dataset:
                    temp['indikator_dataset'][k]['dataset_nama'] = data_dataset[0]['name']

            return temp

        except Exception as err:
            # fail response
            return temp
