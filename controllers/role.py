''' Doc: controller role  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import RoleModel
from models import AppModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable, singleton-comparison
class RoleController(object):
    ''' Doc: controller role  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(RoleModel)
            for attr, value in where.items():
                result = result.filter(getattr(RoleModel, attr) == value)
            result = result.filter(or_(cast(getattr(RoleModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in RoleModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            role = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)

                # get relation to app
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("muid"),
                    defer("mdate"),
                    defer("is_deleted"))
                app = app.get(res['app_id'])

                if app:
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                res['app'] = app
                role.append(res)

            # check if empty
            role = list(role)
            if role:
                return True, role
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(RoleModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(RoleModel, attr) == value)
            result = result.filter(or_(cast(getattr(RoleModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in RoleModel.__table__.columns))
            result = result.count()

            # change into dict
            role = {}
            role['count'] = result

            # check if empty
            if role:
                return True, role
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(RoleModel).get(_id)
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to role
                app = db.session.query(AppModel)
                app = app.options(
                    defer("cuid"),
                    defer("cdate"),
                    defer("muid"),
                    defer("mdate"),
                    defer("is_deleted"))
                app = app.get(result['app_id'])

                if app:
                    app = app.__dict__
                    app.pop('_sa_instance_state', None)
                else:
                    app = {}

                result['app'] = app

            else:
                result = {}

            # change into dict
            role = result

            # check if empty
            if role:
                return True, role
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["cdate"] = helper.local_date_server()

            # prepare data model
            result = RoleModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, role = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, role
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json
                    # json_send["cdate"] = helper.local_date_server()

                    # prepare data model
                    result = RoleModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, role = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(role)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = helper.local_date_server()

            try:
                # prepare data model
                result = db.session.query(RoleModel)
                for attr, value in where.items():
                    result = result.filter(getattr(RoleModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, role = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, role
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(RoleModel)
                for attr, value in where.items():
                    result = result.filter(getattr(RoleModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, role = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
