''' Doc: controller dashboard  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
import sentry_sdk
import math
from datetime import timedelta
from dateutil import parser

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison


class DashboardCsatController(object):
    ''' Doc: constructor dashboard  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def filter_type(self, start_date, end_date):
        ''' Doc: function filter date type  '''
        try:
            start_date_parser = parser.parse(start_date)
            end_date_parser = parser.parse(end_date)
            diff = end_date_parser - start_date_parser
            distance_day = (diff.days + 1)

            start_date_format = (start_date_parser - timedelta(days=distance_day)).strftime("%Y-%m-%d")
            end_date_format = (start_date_parser - timedelta(days=1)).strftime("%Y-%m-%d")

            return start_date_format, end_date_format, distance_day

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            return err, False


    def get_csat_total(self, start_date, end_date):
        ''' Doc: function get_csat_total'''
        try:
            # distance day
            start_date_past, end_date_past, distance_day = self.filter_type(start_date, end_date)

            # total now
            sql_now = text(
                "select count(*) from csat where " +
                "is_active = true and is_deleted = false "+
                "and datetime::date >='"+ start_date +"' and datetime::date <='"+ end_date +"' "
            )
            result_now = db.engine.execute(sql_now)
            res_now = [row for row in result_now]

            # total past
            sql_past = text(
                "select count(*) from csat where " +
                "is_active = true and is_deleted = false "+
                "and datetime::date >= '" + start_date_past + "' and datetime::date <= '" + end_date_past + "'"
            )
            result_past = db.engine.execute(sql_past)
            res_past = [row for row in result_past]

            # declare variable
            now_count = 0
            if res_now:
                now_count = res_now[0][0]

            past_count = 0
            if res_past:
                past_count = res_past[0][0]

            try:
                growth = round((now_count - past_count), 2)
                percent_growth = round(((growth / past_count) * 100), 2)
            except ZeroDivisionError:
                growth = 0
                percent_growth = 0

            # check if empty
            datas = {
                "csat_now_total": now_count,
                "csat_now_date": start_date + ' s/d ' + end_date,
                "csat_past_total": past_count,
                "csat_past_date": start_date_past + ' s/d ' + end_date_past,
                "csat_growth": growth,
                "csat_growth_percent": percent_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_average(self, start_date, end_date):
        ''' Doc: function get feedback average  '''
        try:
            # distance day
            start_date_past, end_date_past, distance_day = self.filter_type(start_date, end_date)

            # total now
            sql_now = text(
                "select count(*) from csat where " +
                "is_active = true and is_deleted = false "+
                "and datetime::date >='"+ start_date +"' and datetime::date <='"+ end_date +"' "
            )
            result_now = db.engine.execute(sql_now)
            res_now = [row for row in result_now]

            # total past
            sql_past = text(
                "select count(*) from csat where " +
                "is_active = true and is_deleted = false "+
                "and datetime::date >= '" + start_date_past + "' and datetime::date <= '" + end_date_past + "'"
            )
            result_past = db.engine.execute(sql_past)
            res_past = [row for row in result_past]

            # declare variable
            now_count = 0
            now_average = 0
            if res_now:
                now_count = res_now[0][0]
                now_average = math.floor(now_count / distance_day)

            past_count = 0
            past_average = 0
            if res_past:
                past_count = res_past[0][0]
                past_average = math.floor(past_count / distance_day)

            try:
                growth = round((now_average - past_average), 2)
                percent_growth = round(((growth / past_average) * 100), 2)
            except ZeroDivisionError:
                growth = 0
                percent_growth = 0

            # check if empty
            datas = {
                "csat_now_average": now_average,
                "csat_now_date": start_date + ' s/d ' + end_date,
                "csat_past_average": past_average,
                "csat_past_date": start_date_past + ' s/d ' + end_date_past,
                "csat_growth": growth,
                "csat_growth_percent": percent_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_trend(self, start_date, end_date, group):
        ''' Doc: function get feedback trend  '''
        try:
            # query default
            sql_series = '''SELECT (generate_series(( SELECT csat.datetime FROM csat where score is not null ORDER BY csat.datetime LIMIT 1), (now() + '1 day'::interval), '1 day'::interval))::date AS date'''
            sql_csat = '''
                SELECT (csat.datetime)::date AS datetime, count(*) as count
                FROM csat
                WHERE csat.is_active = true AND csat.is_deleted = false
                GROUP BY csat.datetime::date
                ORDER BY ((csat.datetime)::date)
            '''
            sql_day = '''
                SELECT series.date, COALESCE(csat.count, 0) AS count  FROM
                (''' + sql_series + ''') as series
                LEFT JOIN
                (''' + sql_csat + ''') as csat
                ON series.date = csat.datetime
            '''
            sql_filter = " WHERE date >='"+start_date+"' and date <='"+end_date+"' "

            # check group
            if group == 'day':
                sql = sql_day + sql_filter
            elif group == 'week':
                sql = '''
                SELECT (date_trunc('week'::text, (csat_harian.date)::timestamp with time zone))::date AS date,
                COALESCE(sum(csat_harian.count), 0) AS count
                FROM
                ( ''' + sql_day + sql_filter + ''') as csat_harian
                GROUP BY ((date_trunc('week'::text, (csat_harian.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('week'::text, (csat_harian.date)::timestamp with time zone))::date)
                '''
            elif group == 'month':
                sql = '''
                SELECT (date_trunc('month'::text, (csat_harian.date)::timestamp with time zone))::date AS date,
                COALESCE(sum(csat_harian.count), 0) AS count
                FROM
                ( ''' + sql_day + sql_filter + ''') as csat_harian
                GROUP BY ((date_trunc('month'::text, (csat_harian.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('month'::text, (csat_harian.date)::timestamp with time zone))::date)
                '''
            result = db.engine.execute(sql)

            output = []
            for res in [dict(row) for row in result]:
                # declare variable
                temp = {
                    "date": res['date'].strftime("%Y-%m-%d"),
                    "total": int(res['count']),
                }
                output.append(temp)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_csat_total(self, start_date, end_date):
        ''' Doc: function get_csat_total'''
        try:
            # distance day
            start_date_past, end_date_past, distance_day = self.filter_type(start_date, end_date)

            # total now
            sql_now = text(
                "select count(*), COALESCE(sum(score), 0) from csat where " +
                "is_active = true and is_deleted = false and score is not null "+
                "and datetime::date >='"+ start_date +"' and datetime::date <='"+ end_date +"' "
            )
            result_now = db.engine.execute(sql_now)
            res_now = [row for row in result_now]

            # total past
            sql_past = text(
                "select count(*), COALESCE(sum(score), 0) from csat where " +
                "is_active = true and is_deleted = false and score is not null "+
                "and datetime::date >= '" + start_date_past + "' and datetime::date <= '" + end_date_past + "'"
            )
            result_past = db.engine.execute(sql_past)
            res_past = [row for row in result_past]

            # declare variable
            now_count = 0
            now_sum = 0
            now_score_max = 0
            now_score = 0
            if res_now:
                now_count = res_now[0][0]
                now_sum = res_now[0][1]
                now_score_max = now_count * 4

            past_count = 0
            past_sum = 0
            past_score_max = 0
            past_score = 0
            if res_past:
                past_count = res_past[0][0]
                past_sum = res_past[0][1]
                past_score_max = past_count * 4

            try:
                now_score = round(((now_sum / now_score_max) * 100), 2)
                past_score = round(((past_sum / past_score_max) * 100), 2)
                growth = round((now_score - past_score), 2)
                percent_growth = round(((growth / past_score) * 100), 2)
            except ZeroDivisionError:
                growth = 0
                percent_growth = 0

            # check if empty
            datas = {
                "csat_now_count": now_count,
                "csat_now_score": now_score,
                "csat_now_date": start_date + ' s/d ' + end_date,
                "csat_past_count": past_count,
                "csat_past_score": past_score,
                "csat_past_date": start_date_past + ' s/d ' + end_date_past,
                "csat_growth": growth,
                "csat_growth_percent": percent_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_csat_trend(self, start_date, end_date, group):
        ''' Doc: function get feedback trend  '''
        try:
            # query default
            sql_series = '''SELECT (generate_series(( SELECT csat.datetime FROM csat where score is not null ORDER BY csat.datetime LIMIT 1), (now() + '1 day'::interval), '1 day'::interval))::date AS date'''
            sql_csat = '''
                SELECT (csat.datetime)::date AS datetime, count(*) as count, sum(csat.score) AS score
                FROM csat
                WHERE csat.is_active = true AND csat.is_deleted = false AND csat.score is not null
                GROUP BY csat.datetime::date
                ORDER BY ((csat.datetime)::date)
            '''
            sql_day = '''
                SELECT series.date, COALESCE(csat.count, 0) AS count, COALESCE(csat.score, 0) as score FROM
                (''' + sql_series + ''') as series
                LEFT JOIN
                (''' + sql_csat + ''') as csat
                ON series.date = csat.datetime
            '''
            sql_filter = " WHERE date >='"+start_date+"' and date <='"+end_date+"' "

            # check group
            if group == 'day':
                sql = sql_day + sql_filter
            elif group == 'week':
                sql = '''
                SELECT (date_trunc('week'::text, (csat_harian.date)::timestamp with time zone))::date AS date,
                COALESCE(sum(csat_harian.count), 0) AS count,
                COALESCE(sum(csat_harian.score), 0) AS score
                FROM
                ( ''' + sql_day + sql_filter + ''') as csat_harian
                GROUP BY ((date_trunc('week'::text, (csat_harian.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('week'::text, (csat_harian.date)::timestamp with time zone))::date)
                '''
            elif group == 'month':
                sql = '''
                SELECT (date_trunc('month'::text, (csat_harian.date)::timestamp with time zone))::date AS date,
                COALESCE(sum(csat_harian.count), 0) AS count,
                COALESCE(sum(csat_harian.score), 0) AS score
                FROM
                ( ''' + sql_day + sql_filter + ''') as csat_harian
                GROUP BY ((date_trunc('month'::text, (csat_harian.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('month'::text, (csat_harian.date)::timestamp with time zone))::date)
                '''
            result = db.engine.execute(sql)

            output = []
            for res in [dict(row) for row in result]:
                # declare variable
                count = res['count']
                score_max = count * 4
                score = 0

                try:
                    score = round(((res['score'] / score_max) * 100), 2)
                except Exception as err:
                    score = 0

                temp = {
                    "date": res['date'].strftime("%Y-%m-%d"),
                    "count": int(count),
                    "score": float(score),
                }
                output.append(temp)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_tujuan(self, start_date, end_date):
        ''' Doc: function get csat tujuan  '''
        try:
            value_condition = [
                'Referensi Kajian Bisnis',
                'Referensi Pembuatan Kebijakan',
                'Referensi Pembuatan Kurikulum/ Bahan Ajar',
                'Referensi Tugas/ Karya Ilmiah',
                'Referensi Pribadi'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select tujuan, count(tujuan) as total from csat "
            select_from_not = "select 'Lainnya' as tujuan, count(tujuan) as total from csat "
            condition = "where tujuan is not null and datetime::date >= '"+start_date + "' and datetime::date <= '"+end_date+"' and tujuan "
            group_by = " group by tujuan "

            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" + group_by +
                " union " +
                select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_sektor(self, start_date, end_date):
        ''' Doc: function get csat sektor  '''
        try:
            value_condition = [
                'Industri/bisnis',
                'Media',
                'Organisasi non profit/sosial',
                'Pemerintahan',
                'Peneliti/akademisi',
                'Lainnya (tulis secara spesifik)'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select sektor, count(sektor) as total from csat "
            select_from_not = "select 'Lainnya' as sektor, count(sektor) as total from csat "
            condition = "where sektor is not null and datetime::date >= '"+start_date + "' and datetime::date <= '"+end_date+"' and sektor "
            group_by = " group by sektor "

            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" + group_by +
                " union " +
                select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_data_ditemukan(self, start_date, end_date):
        ''' Doc: function get csat data ditemukan  '''
        try:
            sql_filter = " WHERE datetime >='"+start_date+"' and datetime <='"+end_date+"' "
            sql = text(
                '''
                SELECT TRUE as data_ditemukan, count(*) FROM csat ''' + sql_filter + ''' and data_ditemukan = true
                UNION
                SELECT FALSE as data_ditemukan, count(*) FROM csat ''' + sql_filter + ''' and data_ditemukan = false
                '''
            )
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_data_masalah(self, start_date, end_date):
        ''' Doc: function get csat data_masalah  '''
        try:
            value_condition = [
                'Halaman tidak dapat diakses',
                'Informasi dirasa terlalu membingungkan',
                'Informasi yang dicari tidak dapat ditemukan',
                'Tidak tau jika informasi yang tersedia akurat/terbaru',
                'Lainnya (tulis secara spesifik)'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select data_masalah, count(data_masalah) as total from csat "
            select_from_not = "select 'Lainnya' as data_masalah, count(data_masalah) as total from csat "
            condition = "where data_masalah is not null and datetime::date >= '"+start_date + "' and datetime::date <= '"+end_date+"' and data_masalah "
            group_by = " group by data_masalah "

            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" + group_by +
                " union " +
                select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_mudah_didapatkan(self, start_date, end_date):
        ''' Doc: function get csat mudah_didapatkan  '''
        try:
            sql_filter = " WHERE datetime >='"+start_date+"' and datetime <='"+end_date+"' "
            sql = text(
                '''
                SELECT TRUE as mudah_didapatkan, count(*) FROM csat ''' + sql_filter + ''' and mudah_didapatkan = true
                UNION
                SELECT FALSE as mudah_didapatkan, count(*) FROM csat ''' + sql_filter + ''' and mudah_didapatkan = false
                '''
            )
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_csat_top_download(self, start_date, end_date):
        ''' Doc: function get csat top_download  '''
        try:

            # dataset
            query_dataset = text(
                "SELECT dataset.id, dataset.name, count(csat.id) " +
                "FROM csat " +
                "JOIN dataset on csat.category_id = dataset.id " +
                "WHERE datetime::date >='" + start_date +
                "' AND datetime::date <='"+end_date+"'"
                "GROUP BY dataset.id, dataset.name " +
                "ORDER BY count desc " +
                "LIMIT 10"
            )
            result_dataset = db.engine.execute(query_dataset)

            # sektoral
            query_sektoral = text(
                "SELECT sektoral.id, sektoral.name, count(csat.id) " +
                "FROM csat " +
                "JOIN dataset on csat.category_id = dataset.id " +
                "JOIN sektoral on dataset.sektoral_id = sektoral.id " +
                "WHERE datetime::date >='" + start_date +
                "' AND datetime::date <='"+end_date+"'"
                "GROUP BY sektoral.id, sektoral.name " +
                "ORDER BY count desc " +
                "LIMIT 10"
            )
            result_sektoral = db.engine.execute(query_sektoral)

            # skpd
            query_skpd = text(
                "SELECT skpd.kode_skpd, skpd.nama_skpd, count(csat.id)" +
                "FROM csat " +
                "JOIN dataset on csat.category_id = dataset.id " +
                "JOIN skpd on dataset.kode_skpd = skpd.kode_skpd " +
                "WHERE datetime::date >='" + start_date +
                "' AND datetime::date <='"+end_date+"'"
                "GROUP BY skpd.kode_skpd, skpd.nama_skpd " +
                "ORDER BY count desc " +
                "LIMIT 10"
            )
            result_skpd = db.engine.execute(query_skpd)

            # variable
            output = {}
            output['top_dataset'] = []
            output['top_sektoral'] = []
            output['top_skpd'] = []

            # maping object
            for res in [dict(row) for row in result_dataset]:
                row_append = res
                output['top_dataset'].append(row_append)
            for res in [dict(row) for row in result_sektoral]:
                row_append = res
                output['top_sektoral'].append(row_append)
            for res in [dict(row) for row in result_skpd]:
                row_append = res
                output['top_skpd'].append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
