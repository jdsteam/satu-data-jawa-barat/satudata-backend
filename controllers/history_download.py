''' Doc: controller history download '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryDownloadModel
from models import DatasetModel
from models import InfographicModel
from models import VisualizationModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, func, cast
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
FORMAT_DATE_MORE = "history_download.datetime::date >= "
FORMAT_DATE_LESS = "history_download.datetime::date <= "

# pylint: disable=singleton-comparison, unused-variable, unused-argument


class HistoryDownloadController(object):
    ''' Doc: class history download  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search, start_date, end_date):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                HistoryDownloadModel.id, HistoryDownloadModel.datetime, HistoryDownloadModel.type,
                HistoryDownloadModel.email, HistoryDownloadModel.pekerjaan, HistoryDownloadModel.bidang,
                HistoryDownloadModel.tujuan, HistoryDownloadModel.lokasi, HistoryDownloadModel.category,
                HistoryDownloadModel.category_id, DatasetModel.name.label(
                    'category_name')
            )
            result = result.join(
                DatasetModel, DatasetModel.id == HistoryDownloadModel.category_id)
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    if attr == 'category_name':
                        result = result.filter(
                            or_(getattr(DatasetModel, 'name') == val for val in value))
                    else:
                        result = result.filter(
                            or_(getattr(HistoryDownloadModel, attr) == val for val in value))
                else:
                    if attr == 'category_name':
                        result = result.filter(
                            getattr(DatasetModel, 'name') == value)
                    else:
                        result = result.filter(
                            getattr(HistoryDownloadModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(HistoryDownloadModel, "datetime"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDownloadModel, "type"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDownloadModel, "email"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDownloadModel, "pekerjaan"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDownloadModel, "bidang"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDownloadModel, "tujuan"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDownloadModel, "lokasi"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(DatasetModel, "name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip, start_date, end_date):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.order_by(
                text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                # temp['category_name'] = ''
                # if temp['category'] == 'dataset':
                #     temp = self.populate_dataset(temp)

                # elif temp['category'] == 'infographic':
                #     temp = self.populate_infographic(temp)

                # elif temp['category'] == 'visualization':
                #     temp = self.populate_visualization(temp)

                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_dataset(self, temp):
        ''' Doc: function populate_dataset  '''
        result_category = db.session.query(DatasetModel.name)
        result_category = result_category.filter(
            getattr(DatasetModel, 'id') == temp['category_id'])
        result_category = result_category.all()
        for rc_ in result_category:
            temp['category_name'] = rc_[0]

        return temp

    def populate_infographic(self, temp):
        ''' Doc: function populate_infographic  '''
        result_category = db.session.query(InfographicModel.name)
        result_category = result_category.filter(
            getattr(InfographicModel, 'id') == temp['category_id'])
        result_category = result_category.all()
        for rc_ in result_category:
            temp['category_name'] = rc_[0]

        return temp

    def populate_visualization(self, temp):
        ''' Doc: function populate_visualization  '''
        result_category = db.session.query(VisualizationModel.name)
        result_category = result_category.filter(
            getattr(VisualizationModel, 'id') == temp['category_id'])
        result_category = result_category.all()
        for rc_ in result_category:
            temp['category_name'] = rc_[0]

        return temp

    def get_count(self, where: dict, search, start_date, end_date):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id': _id}, '', '', '')
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temp['category_name'] = ''
                if temp['category'] == 'dataset':
                    temp = self.populate_dataset(temp)

                elif temp['category'] == 'infographic':
                    temp = self.populate_infographic(temp)

                elif temp['category'] == 'visualization':
                    temp = self.populate_visualization(temp)

                history_download.append(temp)

            # check if empty
            if history_download:
                return True, history_download, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_search_email(self, email):
        ''' Doc: function get search email  '''
        try:
            # execute database
            result = db.session.query(
                func.distinct(HistoryDownloadModel.email).label('email'),
                HistoryDownloadModel.pekerjaan, HistoryDownloadModel.bidang
            )
            result = result.filter(cast(
                getattr(HistoryDownloadModel, "email"), sqlalchemy.String).ilike('%'+email+'%'))
            print(result)
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                print(temp)
                history_download.append(temp)

            # check if empty
            if history_download:
                return True, history_download, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["datetime"] = HELPER.local_date_server()
            # prepare data model
            result = HistoryDownloadModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history_download, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, history_download
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            json_send["datetime"] = HELPER.local_date_server()

            try:
                # prepare data model
                result = db.session.query(HistoryDownloadModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(HistoryDownloadModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history_download, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history_download
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryDownloadModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(HistoryDownloadModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history_download, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_pekerjaan(self, start_date, end_date):
        ''' Doc: function get chart pekerjaan  '''
        try:
            # query postgresql
            result = db.session.query(
                HistoryDownloadModel.pekerjaan,
                func.count(HistoryDownloadModel.id).label('count')
            )
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.group_by(HistoryDownloadModel.pekerjaan)
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_bidang(self, start_date, end_date):
        ''' Doc: function get chart bidang  '''
        try:
            # query postgresql
            result = db.session.query(
                HistoryDownloadModel.pekerjaan, HistoryDownloadModel.bidang,
                func.count(HistoryDownloadModel.id).label('count')
            )
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.group_by(
                HistoryDownloadModel.pekerjaan, HistoryDownloadModel.bidang)
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan(self, start_date, end_date):
        ''' Doc: function get chart tujuan  '''
        try:
            # query postgresql
            result = db.session.query(HistoryDownloadModel.tujuan, func.count(
                HistoryDownloadModel.id).label('count'))
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.group_by(HistoryDownloadModel.tujuan)
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_harian(self, start_date, end_date):
        ''' Doc: function get chart harian  '''
        try:
            # query postgresql
            result = db.session.query(
                func.date_trunc(
                    'day', HistoryDownloadModel.datetime).label('date'),
                func.count(HistoryDownloadModel.id).label('count')
            )
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.group_by(func.date_trunc(
                'day', HistoryDownloadModel.datetime))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                temp.update({"date": temp['date'].strftime("%Y-%m-%d")})
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_dataset(self, start_date, end_date):
        ''' Doc: function get chart dataset  '''
        try:
            # query postgresql
            result = db.session.query(
                HistoryDownloadModel.category, HistoryDownloadModel.category_id,
                func.count(HistoryDownloadModel.id).label('count')
            )
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.group_by(
                HistoryDownloadModel.category, HistoryDownloadModel.category_id)
            result = result.order_by(text("count desc"))
            result = result.limit(25)
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()

                temp['category_name'] = ''
                if temp['category'] == 'dataset':
                    temp = self.populate_dataset(temp)

                elif temp['category'] == 'infographic':
                    temp = self.populate_infographic(temp)

                elif temp['category'] == 'visualization':
                    temp = self.populate_visualization(temp)

                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_download(self, start_date, end_date):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            result = self.query({}, "", start_date, end_date)
            result = result.order_by(text("history_download.datetime asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
