# ''' Doc: controller dataset  '''
from settings.configuration import Configuration
from settings import configuration as conf
from helpers import Helper
from exceptions import ErrorMessage
from bs4 import BeautifulSoup
from claming import Cleansing, Matching
from datasae import Comformity, Uniqueness, Consistency, Completeness, Timeliness
from models import DatasetQualityResultModel
from models import DatasetQualityResultHistoryModel
from helpers.postgre_alchemy import postgre_alchemy as db
import pandas as pd
import simplejson as json
import requests
import unidecode


CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
MESSAGE_RECEIVER = '?receiver='
MESSAGE_MEMBUTUHKAN_VERIFIKASI = "</b> membutuhkan verifikasi."
MESSAGE_DARI = " dari <b>"

# pylint: disable=line-too-long, too-many-lines, singleton-comparison, unused-variable, broad-except, unused-argument, too-many-function-args


class DatasetQualityScoreController(object):
    ''' Doc: controller dataset  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def create_quality_score(self, dataset_info, data_, metadata_):

        try:
            # read dataset
            dataset = pd.DataFrame(data_)

            # cleansing
            try:
                dataset["kode_provinsi"] = dataset["kode_provinsi"].astype("str")
                dataset["kode_kabupaten_kota"] = dataset["kode_kabupaten_kota"].astype("str")
            except Exception as err:
                error = "Error cleansing, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            # read metadata
            try:
                metadata = {}
                for i in metadata_:
                    if i["key"] in ["Pengukuran Dataset", "Tingkat Penyajian Dataset", "Cakupan Dataset", "Satuan Dataset", "Frekuensi Dataset"]:
                        var = {i["key"].lower().replace(" ", "_"): i["value"]}
                        metadata.update(var)
            except Exception as err:
                error = "Error read metadata, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            # kode bps
            kode_bps = [{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3201","nama_kabupaten_kota":"KABUPATEN BOGOR"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3202","nama_kabupaten_kota":"KABUPATEN SUKABUMI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3203","nama_kabupaten_kota":"KABUPATEN CIANJUR"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3204","nama_kabupaten_kota":"KABUPATEN BANDUNG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3205","nama_kabupaten_kota":"KABUPATEN GARUT"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3206","nama_kabupaten_kota":"KABUPATEN TASIKMALAYA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3207","nama_kabupaten_kota":"KABUPATEN CIAMIS"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3208","nama_kabupaten_kota":"KABUPATEN KUNINGAN"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3209","nama_kabupaten_kota":"KABUPATEN CIREBON"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3210","nama_kabupaten_kota":"KABUPATEN MAJALENGKA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3211","nama_kabupaten_kota":"KABUPATEN SUMEDANG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3212","nama_kabupaten_kota":"KABUPATEN INDRAMAYU"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3213","nama_kabupaten_kota":"KABUPATEN SUBANG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3214","nama_kabupaten_kota":"KABUPATEN PURWAKARTA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3215","nama_kabupaten_kota":"KABUPATEN KARAWANG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3216","nama_kabupaten_kota":"KABUPATEN BEKASI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3217","nama_kabupaten_kota":"KABUPATEN BANDUNG BARAT"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3218","nama_kabupaten_kota":"KABUPATEN PANGANDARAN"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3271","nama_kabupaten_kota":"KOTA BOGOR"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3272","nama_kabupaten_kota":"KOTA SUKABUMI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3273","nama_kabupaten_kota":"KOTA BANDUNG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3274","nama_kabupaten_kota":"KOTA CIREBON"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3275","nama_kabupaten_kota":"KOTA BEKASI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3276","nama_kabupaten_kota":"KOTA DEPOK"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3277","nama_kabupaten_kota":"KOTA CIMAHI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3278","nama_kabupaten_kota":"KOTA TASIKMALAYA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3279","nama_kabupaten_kota":"KOTA BANJAR"}]

            # generate config
            try:
                columns = dataset.columns
                config = {
                    "name": dataset_info['name'],
                    "description": self.clean_description(dataset_info['description']),
                    "tag": dataset_info['dataset_tags'],
                    "category": dataset_info['category'],
                    "metadata": metadata,
                    "unit": metadata['satuan_dataset'].split(',') if 'satuan_dataset' in metadata.keys() else None,
                    "unit_column": self.get_unit_column(columns),
                    "value_column": self.get_value_column(columns, metadata['pengukuran_dataset']) if 'pengukuran_dataset' in metadata.keys() else None,
                    "time_series_type": self.time_series(columns, metadata['frekuensi_dataset'])['time_series_type'] if 'frekuensi_dataset' in metadata.keys() else None,
                    "column_time_series": self.time_series(columns, metadata['frekuensi_dataset'])['column_time_series'] if 'frekuensi_dataset' in metadata.keys() else {'years_column': None, 'months_column': None, 'dates_column': None},
                    "code_area": kode_bps,
                    "code_area_level": self.get_code_area_level(columns)
                }
            except Exception as err:
                error = "Error create config, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            try:
                comformity = Comformity(
                    data=dataset,
                    title=config["name"],
                    description=config["description"],
                    tag=config["tag"],
                    metadata=config["metadata"],
                    category=config["category"],
                    code_area=config["code_area"],
                    code_area_level=config["code_area_level"]
                )
                comformity_quality = comformity.comformity(
                    comformity_explain_columns=20,
                    comformity_code_area=20,
                    comformity_measurement=20,
                    comformity_serving_rate=20,
                    comformity_scope=20
                )
                # print(comformity_quality)
            except Exception as err:
                error = "Error create conformity, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            try:
                uniqueness = Uniqueness(
                    data=dataset
                )
                uniqueness_quality = uniqueness.uniqueness(
                    uniqeness_duplicated=100
                )
                # print(uniqueness_quality)
            except Exception as err:
                error = "Error create uniqueness, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            try:
                consistency = Consistency(
                    data=dataset,
                    unit=config["unit"],
                    unit_column=config["unit_column"],
                    value_column=config["value_column"],
                    time_series_type=config["time_series_type"],
                    column_time_series=config["column_time_series"]
                )
                consistency_quality = consistency.consistency(
                    consistency_unit=40,
                    consistency_time_series=20,
                    consistency_listing_province=40
                )
                # print(consistency_quality)
            except Exception as err:
                error = "Error create consistency, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            try:
                completeness = Completeness(
                    data=dataset
                )
                completeness_quality = completeness.completeness(
                    completeness_filled=100
                )
                # print(completeness_quality)
            except Exception as err:
                error = "Error create completeness, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            try:
                timeliness = Timeliness(
                    data=dataset,
                    time_series_type=config["time_series_type"],
                    column_time_series=config["column_time_series"]
                )
                timeliness_quality = timeliness.timeliness(
                    timeliness_updated=100
                )
                # print(timeliness_quality)
            except Exception as err:
                error = "Error create timeliness, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)

            try:
                try:
                    comformity_quality
                except NameError:
                    comformity_quality = {}
                    comformity_quality["result"] = 0
                try:
                    uniqueness_quality
                except NameError:
                    uniqueness_quality = {}
                    uniqueness_quality["result"] = 0
                try:
                    consistency_quality
                except NameError:
                    consistency_quality = {}
                    consistency_quality["result"] = 0
                try:
                    completeness_quality
                except NameError:
                    completeness_quality = {}
                    completeness_quality["result"] = 0
                try:
                    timeliness_quality
                except NameError:
                    timeliness_quality = {}
                    timeliness_quality["result"] = 0

                quality_result = (
                    (comformity_quality["result"] * 0.30) +
                    (uniqueness_quality["result"] * 0.25) +
                    (consistency_quality["result"] * 0.25) +
                    (completeness_quality["result"] * 0.10) +
                    (timeliness_quality["result"] * 0.10)
                )

                final_result = {}
                final_result.update({'final_result': quality_result})
                final_result.update(comformity_quality)
                final_result.update(consistency_quality)
                final_result.update(completeness_quality)
                final_result.update(timeliness_quality)
                final_result.update(uniqueness_quality)

                # return self.save_or_update(dataset_info, config, final_result)
                self.save_or_update(dataset_info, config, final_result)

            except Exception as err:
                error = "Error create result, " + str(err)
                self.generate_exception_message(dataset_info, metadata_, error)


        except Exception as err:
            # fail response
            # raise ErrorMessage(str(err), 500, 1, {})
            error = "Error create quality, " + str(err)
            self.generate_exception_message(dataset_info, metadata_, error)
            # return False, {}, "Error create quality, " + str(err)

    def generate_exception_message(self, dataset_info, metadata_, err):
        temp = {}
        temp['id'] = dataset_info['id']
        temp['name'] = dataset_info['name']
        temp['schema'] = dataset_info['schema']
        temp['table'] = dataset_info['table']
        temp['tag'] = dataset_info['dataset_tags']
        temp['description'] = dataset_info['description']
        temp['metadata_'] = json.dumps(metadata_)
        temp['category'] = dataset_info['category']
        temp['error'] = err
        temp['is_config_complete'] = False

        result = db.session.query(DatasetQualityResultModel)
        result = result.filter(DatasetQualityResultModel.id == temp['id'])
        result = result.count()
        if result > 0:
            # update
            result_update = db.session.query(DatasetQualityResultModel)
            result_update = result_update.filter(DatasetQualityResultModel.id == temp['id'])
            result_update = result_update.update(temp, synchronize_session='fetch')
            result_update = db.session.commit()
        else:
            # insert
            result_insert = DatasetQualityResultModel(**temp)
            db.session.add(result_insert)
            db.session.commit()

    def save_or_update(self, dataset_info, config, quality_result):
        try:
            temp = {}
            temp['id'] = dataset_info['id']
            temp['unit'] = config['unit']
            temp['unit_column'] = config['unit_column']
            temp['value_column'] = config['value_column']
            temp['name'] = config['name']
            temp['schema'] = dataset_info['schema']
            temp['table'] = dataset_info['table']
            temp['tag'] = config['tag']
            temp['description'] = config['description']
            temp['metadata_'] = json.dumps(config['metadata'])
            temp['category'] = config['category']
            temp['code_area_level'] = config['code_area_level']
            temp['column_time_series'] = json.dumps(config['column_time_series'])
            temp['time_series_type'] = config['time_series_type']
            temp['error'] = ''
            temp['is_config_complete'] = True

            try:
                temp['conformity_code_area_total_rows'] = quality_result['comformity_code_area']['total_rows']
                temp['conformity_code_area_total_columns'] = quality_result['comformity_code_area']['total_columns']
                temp['conformity_code_area_total_cells'] = quality_result['comformity_code_area']['total_cells']
                temp['conformity_code_area_total_valid'] = quality_result['comformity_code_area']['total_valid']
                temp['conformity_code_area_total_not_valid'] = quality_result['comformity_code_area']['total_not_valid']
                temp['conformity_code_area_warning'] = quality_result['comformity_code_area']['warning']
                temp['conformity_code_area_quality_result'] = quality_result['comformity_code_area']['quality_result']
            except Exception as err:
                quality_result['comformity_code_area'] = {}
                quality_result['comformity_code_area']['quality_result'] = 0

            try:
                temp['conformity_explain_columns_total_rows'] = quality_result['comformity_explain_columns']['total_rows']
                temp['conformity_explain_columns_total_columns'] = quality_result['comformity_explain_columns']['total_columns']
                temp['conformity_explain_columns_total_cells'] = quality_result['comformity_explain_columns']['total_cells']
                temp['conformity_explain_columns_total_valid'] = quality_result['comformity_explain_columns']['total_valid']
                temp['conformity_explain_columns_total_not_valid'] = quality_result['comformity_explain_columns']['total_not_valid']
                temp['conformity_explain_columns_warning'] = quality_result['comformity_explain_columns']['warning']
                temp['conformity_explain_columns_quality_result'] = quality_result['comformity_explain_columns']['quality_result']
            except Exception as err:
                quality_result['comformity_explain_columns'] = {}
                quality_result['comformity_explain_columns']['quality_result'] = 0

            try:
                temp['conformity_measurement_total_rows'] = quality_result['comformity_measurement']['total_rows']
                temp['conformity_measurement_total_columns'] = quality_result['comformity_measurement']['total_columns']
                temp['conformity_measurement_total_cells'] = quality_result['comformity_measurement']['total_cells']
                temp['conformity_measurement_total_valid'] = quality_result['comformity_measurement']['total_valid']
                temp['conformity_measurement_total_not_valid'] = quality_result['comformity_measurement']['total_not_valid']
                temp['conformity_measurement_warning'] = quality_result['comformity_measurement']['warning']
                temp['conformity_measurement_quality_result'] = quality_result['comformity_measurement']['quality_result']
            except Exception as err:
                quality_result['comformity_measurement'] = {}
                quality_result['comformity_measurement']['quality_result'] = 0

            try:
                temp['conformity_serving_rate_total_rows'] = quality_result['comformity_serving_rate']['total_rows']
                temp['conformity_serving_rate_total_columns'] = quality_result['comformity_serving_rate']['total_columns']
                temp['conformity_serving_rate_total_cells'] = quality_result['comformity_serving_rate']['total_cells']
                temp['conformity_serving_rate_total_valid'] = quality_result['comformity_serving_rate']['total_valid']
                temp['conformity_serving_rate_total_not_valid'] = quality_result['comformity_serving_rate']['total_not_valid']
                temp['conformity_serving_rate_warning'] = quality_result['comformity_serving_rate']['warning']
                temp['conformity_serving_rate_quality_result'] = quality_result['comformity_serving_rate']['quality_result']
            except Exception as err:
                quality_result['comformity_serving_rate'] = {}
                quality_result['comformity_serving_rate']['quality_result'] = 0

            try:
                temp['conformity_scope_total_rows'] = quality_result['comformity_scope']['total_rows']
                temp['conformity_scope_total_columns'] = quality_result['comformity_scope']['total_columns']
                temp['conformity_scope_total_cells'] = quality_result['comformity_scope']['total_cells']
                temp['conformity_scope_total_valid'] = quality_result['comformity_scope']['total_valid']
                temp['conformity_scope_total_not_valid'] = quality_result['comformity_scope']['total_not_valid']
                temp['conformity_scope_warning'] = quality_result['comformity_scope']['warning']
                temp['conformity_scope_quality_result'] = quality_result['comformity_scope']['quality_result']
            except Exception as err:
                quality_result['comformity_scope'] = {}
                quality_result['comformity_scope']['quality_result'] = 0

            try:
                temp['uniqueness_duplicated_total_rows'] = quality_result['uniqeness_duplicated']['total_rows']
                temp['uniqueness_duplicated_total_columns'] = quality_result['uniqeness_duplicated']['total_columns']
                temp['uniqueness_duplicated_total_cells'] = quality_result['uniqeness_duplicated']['total_cells']
                temp['uniqueness_duplicated_total_valid'] = quality_result['uniqeness_duplicated']['total_valid']
                temp['uniqueness_duplicated_total_not_valid'] = quality_result['uniqeness_duplicated']['total_not_valid']
                temp['uniqueness_duplicated_warning'] = quality_result['uniqeness_duplicated']['warning']
                temp['uniqueness_duplicated_quality_result'] = quality_result['uniqeness_duplicated']['quality_result']
            except Exception as err:
                quality_result['uniqeness_duplicated'] = {}
                quality_result['uniqeness_duplicated']['quality_result'] = 0

            try:
                temp['completeness_filled_total_rows'] = quality_result['completeness_filled']['total_rows']
                temp['completeness_filled_total_columns'] = quality_result['completeness_filled']['total_columns']
                temp['completeness_filled_total_cells'] = quality_result['completeness_filled']['total_cells']
                temp['completeness_filled_total_valid'] = quality_result['completeness_filled']['total_valid']
                temp['completeness_filled_total_not_valid'] = quality_result['completeness_filled']['total_not_valid']
                temp['completeness_filled_warning'] = quality_result['completeness_filled']['warning']
                temp['completeness_filled_quality_result'] = quality_result['completeness_filled']['quality_result']
            except Exception as err:
                quality_result['completeness_filled'] = {}
                quality_result['completeness_filled']['quality_result'] = 0

            try:
                temp['consistency_unit_total_rows'] = quality_result['consistency_unit']['total_rows']
                temp['consistency_unit_total_columns'] = quality_result['consistency_unit']['total_columns']
                temp['consistency_unit_total_cells'] = quality_result['consistency_unit']['total_cells']
                temp['consistency_unit_total_valid'] = quality_result['consistency_unit']['total_valid']
                temp['consistency_unit_total_not_valid'] = quality_result['consistency_unit']['total_not_valid']
                temp['consistency_unit_warning'] = quality_result['consistency_unit']['warning']
                temp['consistency_unit_quality_result'] = quality_result['consistency_unit']['quality_result']
            except Exception as err:
                quality_result['consistency_unit'] = {}
                quality_result['consistency_unit']['quality_result'] = 0

            try:
                temp['consistency_listing_province_total_cells'] = quality_result['consistency_listing_province']['total_rows']
                temp['consistency_listing_province_total_columns'] = quality_result['consistency_listing_province']['total_columns']
                temp['consistency_listing_province_total_not_valid'] = quality_result['consistency_listing_province']['total_cells']
                temp['consistency_listing_province_total_rows'] = quality_result['consistency_listing_province']['total_valid']
                temp['consistency_listing_province_total_valid'] = quality_result['consistency_listing_province']['total_not_valid']
                temp['consistency_listing_province_warning'] = quality_result['consistency_listing_province']['warning']
                temp['consistency_listing_province_quality_result'] = quality_result['consistency_listing_province']['quality_result']
            except Exception as err:
                quality_result['consistency_listing_province'] = {}
                quality_result['consistency_listing_province']['quality_result'] = 0

            try:
                temp['consistency_time_series_total_rows'] = quality_result['consistency_time_series']['total_rows']
                temp['consistency_time_series_total_columns'] = quality_result['consistency_time_series']['total_columns']
                temp['consistency_time_series_total_cells'] = quality_result['consistency_time_series']['total_cells']
                temp['consistency_time_series_total_valid'] = quality_result['consistency_time_series']['total_valid']
                temp['consistency_time_series_total_not_valid'] = quality_result['consistency_time_series']['total_not_valid']
                temp['consistency_time_series_warning'] = quality_result['consistency_time_series']['warning']
                temp['consistency_time_series_quality_result'] = quality_result['consistency_time_series']['quality_result']
            except Exception as err:
                quality_result['consistency_time_series'] = {}
                quality_result['consistency_time_series']['quality_result'] = 0

            try:
                temp['timeliness_updated_total_rows'] = quality_result['timeliness_updated']['total_rows']
                temp['timeliness_updated_total_columns'] = quality_result['timeliness_updated']['total_columns']
                temp['timeliness_updated_total_cells'] = quality_result['timeliness_updated']['total_cells']
                temp['timeliness_updated_total_valid'] = quality_result['timeliness_updated']['total_valid']
                temp['timeliness_updated_total_not_valid'] = quality_result['timeliness_updated']['total_not_valid']
                temp['timeliness_updated_warning'] = quality_result['timeliness_updated']['warning']
                temp['timeliness_updated_quality_result'] = quality_result['timeliness_updated']['quality_result']
            except Exception as err:
                quality_result['timeliness_updated'] = {}
                quality_result['timeliness_updated']['quality_result'] = 0

            temp['conformity_result'] = (0.2 * quality_result['comformity_code_area']['quality_result']) + (0.2 * quality_result['comformity_explain_columns']['quality_result'])
            temp['conformity_result'] += (0.2 * quality_result['comformity_measurement']['quality_result']) + (0.2 * quality_result['comformity_serving_rate']['quality_result'])
            temp['conformity_result'] += (0.2 * quality_result['comformity_scope']['quality_result'])
            temp['uniqueness_result'] = quality_result['uniqeness_duplicated']['quality_result']
            temp['completeness_result'] = quality_result['completeness_filled']['quality_result']
            temp['consistency_result'] = (quality_result['consistency_unit']['quality_result'] * 0.4) + (quality_result['consistency_listing_province']['quality_result'] * 0.4) + (quality_result['consistency_time_series']['quality_result'] * 0.2)
            temp['timeliness_result'] = quality_result['timeliness_updated']['quality_result']
            temp['final_result'] = (0.30 * temp['conformity_result']) + (0.25 * temp['uniqueness_result']) + (0.10 * temp['completeness_result']) + (0.25 * temp['consistency_result']) + (0.10 * temp['timeliness_result'])

            result = db.session.query(DatasetQualityResultModel)
            result = result.filter(DatasetQualityResultModel.id == temp['id'])
            result = result.count()
            if result > 0:
                # update
                result_update = db.session.query(DatasetQualityResultModel)
                result_update = result_update.filter(DatasetQualityResultModel.id == temp['id'])
                result_update = result_update.update(temp, synchronize_session='fetch')
                result_update = db.session.commit()
                # insert history
                self.save_history(temp)
                # return True, temp, 'Sukses Update Dataset Quality Result'
            else:
                # insert
                result_insert = DatasetQualityResultModel(**temp)
                db.session.add(result_insert)
                db.session.commit()
                # insert history
                self.save_history(temp)
                # return True, temp, 'Sukses Insert Dataset Quality Result'

        except Exception as err:
            # fail response
            # raise ErrorMessage(str(err), 500, 1, {})

            temp = {}
            temp['id'] = dataset_info['id']
            temp['unit'] = config['unit']
            temp['unit_column'] = config['unit_column']
            temp['value_column'] = config['value_column']
            temp['name'] = config['name']
            temp['schema'] = dataset_info['schema']
            temp['table'] = dataset_info['table']
            temp['tag'] = config['tag']
            temp['description'] = config['description']
            temp['metadata_'] = json.dumps(config['metadata'])
            temp['category'] = config['category']
            temp['code_area_level'] = config['code_area_level']
            temp['column_time_series'] = json.dumps(config['column_time_series'])
            temp['time_series_type'] = config['time_series_type']
            temp['error'] = "Error save quality, " + str(err)
            temp['is_config_complete'] = False

            result = db.session.query(DatasetQualityResultModel)
            result = result.filter(DatasetQualityResultModel.id == temp['id'])
            result = result.count()
            if result > 0:
                # update
                result_update = db.session.query(DatasetQualityResultModel)
                result_update = result_update.filter(DatasetQualityResultModel.id == temp['id'])
                result_update = result_update.update(temp, synchronize_session='fetch')
                result_update = db.session.commit()
                # insert history
                self.save_history(temp)
            else:
                # insert
                result_insert = DatasetQualityResultModel(**temp)
                db.session.add(result_insert)
                db.session.commit()
                # insert history
                self.save_history(temp)

            # return False, {}, "Error save quality, " + str(err)

    def save_history(self, temp):
        try:
            temps = temp
            temps['dataset_id'] = temps['id']
            temps['cdate'] = HELPER.local_date_server()
            temps['mdate'] = HELPER.local_date_server()
            del temps['id']

            # insert
            result_insert = DatasetQualityResultHistoryModel(**temps)
            db.session.add(result_insert)
            db.session.commit()
        except Exception as err:
            pass

    def get_unit_column(self, columns):
        unit_column = 'satuan' if 'satuan' in columns else None
        return unit_column

    def get_value_column(self, columns, pengukuran_dataset):
        match = Matching()
        must_word = [
            "agregat", "akumulasi", "alokasi", "anggaran", "angka", "average", "bed", "dana",
            "distribusi", "gini", "gross", "indeks", "jumlah", "kepadatan", "keterisian", "laju",
            "laporan", "lebar", "luas", "net", "nilai", "ntp", "panjang", "peningkatan", "perbandingan",
            "perkembangan", "persentase", "pertumbuhan", "populasi", "produk", "produksi", "produktivitas",
            "proporsi", "proyeksi", "rasio", "rata_rata", "realisasi", "rekap", "rekapitulasi", "tingkat",
            "tren", "turn", "volume",
            "jarak", "waktu", "frekuensi"
        ]
        columns_filter = []
        for word in must_word:
            for column in columns:
                if word in column:
                    columns_filter.append(column)
        result_match = [match.levenshtein_match(column, pengukuran_dataset)['score'] for column in columns_filter]
        try:
            indices = [index for index, item in enumerate(result_match) if item == max(result_match)][0]
            return columns_filter[indices]
        except Exception as e:
            print(e)
            return None

    def filter_columns_time_series(self, columns, keyword):
        list_column = [col for col in columns if keyword in col]
        if len(list_column) > 1:
            list_column = [col for col in list_column if 'kode' not in col]
        filtered_columns = list_column[0] if len(list_column) > 0 else None
        return filtered_columns

    def time_series(self, columns, frekuensi_dataset):
        if 'tahun' in frekuensi_dataset.lower() or 'semester' in frekuensi_dataset.lower():
            result = {
                'time_series_type':'years',
                'column_time_series': {
                    'years_column':  self.filter_columns_time_series(columns, 'tahun'),
                    'months_column': None,
                    'dates_column': None
                }
            }
        elif 'bulan' in frekuensi_dataset.lower():
            result = {
                'time_series_type':'months',
                'column_time_series': {
                    'years_column':  self.filter_columns_time_series(columns, 'tahun'),
                    'months_column': self.filter_columns_time_series(columns, 'bulan'),
                    'dates_column': None
                }
            }
        elif 'hari' in frekuensi_dataset.lower():
            result = {
                'time_series_type':'dates',
                'column_time_series': {
                    'years_column': None,
                    'months_column': None,
                    'dates_column': self.filter_columns_time_series(columns, 'tanggal')
                }
            }
        else:
            result = {
                'time_series_type': None,
                'column_time_series': {
                    'years_column': None,
                    'months_column': None,
                    'dates_column': None
                }
            }
        return result

    def get_code_area_level(self, columns):
        if 'nama_kabupaten_kota' in columns and 'kode_kabupaten_kota' in columns:
            code_area_level = 'city'
        elif 'nama_provinsi' in columns and 'kode_provinsi' in columns:
            code_area_level = 'province'
        else:
            code_area_level = None
        return code_area_level

    def clean_description(self, description):
        description = BeautifulSoup(description, features="html.parser").get_text().strip()
        description = unidecode.unidecode(description).split()
        return ' '.join(description)
