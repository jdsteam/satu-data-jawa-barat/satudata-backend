''' Doc: controller bidang_urusan_indikator_skpd  '''
from helpers import Helper
from exceptions import ErrorMessage
from models import BidangUrusanIndikatorSkpdModel, BidangUrusanIndikatorModel, BidangUrusanModel
from models import UserModel, RoleModel, SkpdModel
from controllers.history_ewalidata_assignment import HistoryEwalidataAssignmentController
from controllers.notification import NotificationController
from controllers.user import UserController
from settings import configuration
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
import requests
import json
from flask import request

from threading import Thread

CONFIG = configuration.Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
HISTORY_EWALIDATA_ASSIGNMENT_CONTROLLER = HistoryEwalidataAssignmentController()
NOTIFICATION_CONTROLLER = NotificationController()
USER_CONTROLLER = UserController()
MESSAGE_RECEIVER = "?receiver="

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class BidangUrusanIndikatorSkpdController(object):
    ''' Doc: class bidang_urusan_indikator_skpd  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            jwt = HELPER.read_jwt()
            # query postgresql
            result = db.session.query(
                BidangUrusanIndikatorSkpdModel.id,
                BidangUrusanIndikatorSkpdModel.kode_indikator,
                BidangUrusanIndikatorModel.nama_indikator,
                BidangUrusanIndikatorModel.kode_bidang_urusan,
                BidangUrusanModel.nama_bidang_urusan,
                BidangUrusanIndikatorSkpdModel.kode_skpd,
                SkpdModel.nama_skpd,
                BidangUrusanIndikatorSkpdModel.status_assignment,
                BidangUrusanIndikatorSkpdModel.notes,
                BidangUrusanIndikatorSkpdModel.cuid,
                BidangUrusanIndikatorSkpdModel.cdate)
            result = result.outerjoin(BidangUrusanIndikatorModel, BidangUrusanIndikatorModel.kode_indikator == BidangUrusanIndikatorSkpdModel.kode_indikator)
            result = result.outerjoin(BidangUrusanModel, BidangUrusanModel.kode_bidang_urusan == BidangUrusanIndikatorModel.kode_bidang_urusan)
            result = result.outerjoin(SkpdModel, SkpdModel.kode_skpd == BidangUrusanIndikatorSkpdModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(BidangUrusanIndikatorSkpdModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(BidangUrusanIndikatorSkpdModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanIndikatorSkpdModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in BidangUrusanIndikatorSkpdModel.__table__.columns))
            if sort[0] == 'id' or sort[0] == 'kode_indikator' or sort[0] == 'cdate':
                sort[0] = 'bidang_urusan_indikator_skpd.' + sort[0]
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            datas = []
            for res in result:
                temp = res._asdict()
                temp.pop('_sa_instance_state', None)
                if temp['cdate']:
                    temp['cdate'] = temp['cdate'].strftime(DATETIME_FORMAT)
                datas.append(temp)
            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            jwt = HELPER.read_jwt()
            # query postgresql
            result = db.session.query(BidangUrusanIndikatorSkpdModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(BidangUrusanIndikatorSkpdModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(BidangUrusanIndikatorSkpdModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanIndikatorSkpdModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in BidangUrusanIndikatorSkpdModel.__table__.columns))
            result = result.count()

            # change into dict
            datas = {}
            datas['count'] = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database# query postgresql
            result = db.session.query(
                BidangUrusanIndikatorSkpdModel.id,
                BidangUrusanIndikatorSkpdModel.kode_indikator,
                BidangUrusanIndikatorModel.nama_indikator,
                BidangUrusanIndikatorModel.kode_bidang_urusan,
                BidangUrusanModel.nama_bidang_urusan,
                BidangUrusanIndikatorSkpdModel.kode_skpd,
                SkpdModel.nama_skpd,
                BidangUrusanIndikatorSkpdModel.status_assignment,
                BidangUrusanIndikatorSkpdModel.notes,
                BidangUrusanIndikatorSkpdModel.cuid,
                BidangUrusanIndikatorSkpdModel.cdate)
            result = result.outerjoin(BidangUrusanIndikatorModel, BidangUrusanIndikatorModel.kode_indikator == BidangUrusanIndikatorSkpdModel.kode_indikator)
            result = result.outerjoin(BidangUrusanModel, BidangUrusanModel.kode_bidang_urusan == BidangUrusanIndikatorModel.kode_bidang_urusan)
            result = result.outerjoin(SkpdModel, SkpdModel.kode_skpd == BidangUrusanIndikatorSkpdModel.kode_skpd)
            result = result.filter(BidangUrusanIndikatorSkpdModel.id == _id)
            result = result.all()
            if result:
                result = result[0]

            if result:
                result = result._asdict()
                result.pop('_sa_instance_state', None)
                if result['cdate']:
                    result['cdate'] = result['cdate'].strftime(DATETIME_FORMAT)
            else:
                result = {}

            # change into dict
            datas = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            json_history = {}
            if 'history' in json_send:
                json_history = json_send['history']
                del json_send['history']

            is_exist = self.check_data_exist(json_send["kode_skpd"], json_send["kode_indikator"])
            if is_exist:
                return False, {}, "Data already exist."

            # insert bidang indikator skpd
            result = BidangUrusanIndikatorSkpdModel(**json_send)
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, datas = self.get_by_id(result['id'])

            # insert notification
            self.insert_notification(datas)

            # insert history
            json_history['bidang_urusan_indikator_skpd_id'] = datas['id']
            json_history['status_assignment'] = datas['status_assignment']
            result_history, datas_history = HISTORY_EWALIDATA_ASSIGNMENT_CONTROLLER.create(json_history)
            datas['history'] = datas_history

            # check if exist
            if res:
                return True, datas, "Create data successfully."
            else:
                return False, {}, "Failed to assign data."

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            json_history = {}
            if 'history' in json_send:
                json_history = json_send['history']
                del json_send['history']

            try:
                # update bidang indikator skpd
                result = db.session.query(BidangUrusanIndikatorSkpdModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanIndikatorSkpdModel, attr) == value)
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, datas = self.get_by_id(where["id"])

                # insert notification
                self.insert_notification(datas)

                # insert history
                json_history['bidang_urusan_indikator_skpd_id'] = datas['id']
                json_history['status_assignment'] = datas['status_assignment']
                result_history, datas_history = HISTORY_EWALIDATA_ASSIGNMENT_CONTROLLER.create(json_history)
                datas['history'] = datas_history

                # check if empty
                if res:
                    return True, datas
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage(str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(BidangUrusanIndikatorSkpdModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanIndikatorSkpdModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, datas = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def check_data_exist(self, _kode_skpd,_kode_indikator):
        ''' Doc: function check data exist '''
        try:
            # execute database
            result = db.session.query(BidangUrusanIndikatorSkpdModel)
            result = result.filter(BidangUrusanIndikatorSkpdModel.kode_indikator == _kode_indikator)
            result = result.filter(BidangUrusanIndikatorSkpdModel.kode_skpd == _kode_skpd)
            result = result.count()
            if result == 0: return False
            else: return True

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def assign(self, json: dict):
        ''' Doc: function create  '''
        # try:
            # get jwt payload
        jwt = HELPER.read_jwt()

        # generate json data
        json_send = {}
        json_send = json
        json_send["cdate"] = HELPER.local_date_server()
        json_send["cuid"] = jwt['id']
        is_exist = self.check_data_exist(json_send["kode_skpd"],json_send["kode_indikator"])
        if is_exist:
            return False, {}, "Data already exist."
        # prepare data model
        result = BidangUrusanIndikatorSkpdModel(**json_send)

        # execute database
        db.session.add(result)
        db.session.commit()
        result = result.to_dict()
        res, bidang_urusan_indikator_skpd = self.get_by_id(result['id'])

        # insert notification
        self.insert_notification(bidang_urusan_indikator_skpd)

        # check if exist
        if res:
            return True, bidang_urusan_indikator_skpd, "Create data successfully."
        else:
            return False, {}, "Failed to assign data."


    def insert_notification(self, data):
        ''' Doc: function insert_notification  '''
        jwt = HELPER.read_jwt()

        # default data
        send_notif = False
        notification = None
        json_notification = {}
        json_notification["feature"] = 5
        json_notification["type"] = "ewalidata"
        json_notification["type_id"] = data["id"]
        json_notification["sender"] = jwt["id"]
        json_notification["title"] = f"E-Walidata dengan Bidang Urusan <i>{data['nama_bidang_urusan'].title()[0:100]}</i> dan Uraian <i>{data['nama_indikator'].title()[0:100]}</i>, "
        json_notification["is_read"] = False
        json_notification["is_enable"] = True
        json_notification["cdate"] = HELPER.local_date_server()
        json_notification["mdate"] = HELPER.local_date_server()
        notes = {
            'kode_bidang_urusan': data['kode_bidang_urusan'],
            'nama_bidang_urusan': data['nama_bidang_urusan'],
            'kode_indikator': data['kode_indikator'],
            'nama_indikator': data['nama_indikator']
        }
        json_notification["notes"] = json.dumps(notes, indent=4)

        # default get user
        data_user = db.session.query(UserModel.id)
        data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)

        # jika walidata
        if jwt["role"]["name"] == "walidata":
            if data["status_assignment"] == 'Ditugaskan':
                send_notif = True
                json_notification["content"] = '<b>Ditugaskan </b>kepada ' + data['nama_skpd'].upper() + '.'
            elif data["status_assignment"] == 'Dikembalikan':
                send_notif = False
            elif data["status_assignment"] == 'Diterima':
                send_notif = False
            elif data["status_assignment"] == 'Pengajuan Nonaktif':
                send_notif = False
            elif data["status_assignment"] == 'Nonaktif':
                send_notif = True
                json_notification["content"] = '<b>Nonaktif </b>oleh WALIDATA.'

            data_user = data_user.filter(UserModel.kode_skpd == data['kode_skpd'])

        # jika opd
        elif jwt["role"]["name"] == "opd":
            if data["status_assignment"] == 'Ditugaskan':
                send_notif = False
            elif data["status_assignment"] == 'Dikembalikan':
                send_notif = True
                json_notification["content"] = '<b>Dikembalikan </b>kepada walidata oleh ' + data['nama_skpd'] + '.'
            elif data["status_assignment"] == 'Diterima':
                send_notif = True
                json_notification["content"] = '<b>Diterima </b> oleh ' + data['nama_skpd'] + '.'
            elif data["status_assignment"] == 'Pengajuan Nonaktif':
                send_notif = True
                json_notification["content"] = '<b>Pengajuan Nonaktif </b> oleh ' + data['nama_skpd'] + '.'
            elif data["status_assignment"] == 'Nonaktif':
                send_notif = False

            data_user = data_user.filter(RoleModel.name == 'walidata')

        # send notif
        thread1 = Thread(target=self.notifsend, args=(send_notif, data_user, json_notification))
        thread1.daemon = True
        thread1.start()

    def notifsend(self, send_notif, data_user, json_notification):
        if send_notif:
            # using raw query because sqlalchemy not working on thread
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_URI,
                pool_size=configuration.SQLALCHEMY_POOL_SIZE,
                max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW
            )
            connection = engine.connect()

            data_user = data_user.all()
            for dr_ in data_user:

                # create notif
                temp = dr_._asdict()
                user_id = temp["id"]
                json_notification["receiver"] = user_id
                try:
                    query_insert = "insert into notification ("
                    query_insert += "type, type_id, sender, receiver, title, content, feature, "
                    query_insert += "is_read, is_enable, cdate, mdate, notes"
                    query_insert += ") values ("
                    query_insert += "%s, %s, %s, %s, %s, %s, %s,"
                    query_insert += "%s, %s, %s, %s, %s "
                    query_insert += ")"
                    value_insert = (
                        json_notification["type"],
                        json_notification["type_id"],
                        json_notification["sender"],
                        json_notification["receiver"],
                        json_notification["title"],
                        json_notification["content"],
                        json_notification["feature"],
                        json_notification["is_read"],
                        json_notification["is_enable"],
                        json_notification["cdate"],
                        json_notification["mdate"],
                        json_notification["notes"],
                    )
                    connection.execute(query_insert, value_insert)

                except Exception as err:
                    # fail response
                    print(str(err))

                # update notif
                param = {}
                param["count_notif"] = 0
                try:
                    sql_user = "select count_notif from users where id = '" + str(user_id) + "'"
                    result_user = connection.execute(sql_user)
                    if result_user:
                        res_user = [row for row in result_user]
                        param["count_notif"] = res_user[0][0]
                        param["count_notif"] += 1

                        query_update = "update users set count_notif = %s where id = %s"
                        value_update = (
                            param["count_notif"],
                            user_id,
                        )
                        connection.execute(query_update, value_update)

                except Exception as err:
                    # fail response
                    print(str(err))

                # socket notif
                # try:
                #     socket_url = CONFIG.SOCKET_URL
                #     token = request.headers.get("Authorization", None)
                #     req_ = requests.get(
                #         socket_url
                #         + "notification"
                #         + MESSAGE_RECEIVER
                #         + str(notification["receiver"]),
                #         headers={
                #             "Authorization": token,
                #             "Content-Type": CONFIG.MESSAGE_APP_JSON,
                #         },
                #         data=json.dumps(notification),
                #     )
                #     res_ = req_.json()
                # except Exception as err:
                #     print(str(err))
