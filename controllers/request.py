''' Doc: controller request  '''
from sqlalchemy.orm import aliased
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import RequestModel, SkpdModel, UserModel, HistoryRequestModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"
VAR_DATA_TERSEDIA = "Data Tersedia"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class RequestController(object):
    ''' Doc: class request   '''

    def __init__(self, **kwargs):
        ''' Doc: function init '''

    def query(self, where: dict, search):
        ''' Doc: function QUERY '''
        try:
            # query postgresql
            owner_skpd = aliased(SkpdModel)
            result = db.session.query(
                RequestModel.id, RequestModel.title, RequestModel.notes, RequestModel.cdate, RequestModel.mdate,
                SkpdModel.nama_skpd, SkpdModel.nama_skpd_alias, UserModel.username, UserModel.name,
                owner_skpd.nama_skpd_alias.label('skpd_request')
            )
            result = result.join(SkpdModel, RequestModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.join(UserModel, RequestModel.user_id == UserModel.id)
            result = result.join(owner_skpd, owner_skpd.kode_skpd == UserModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(RequestModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(RequestModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(RequestModel, "id"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestModel, "title"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestModel, "notes"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(owner_skpd, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%')
            ))
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all '''
        try:
            # query postgresql
            result = self.query(where, search)
            if sort[0] != 'datetime':
                result = result.order_by(text("request."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            request = []
            for res in result:
                temp = res._asdict()
                temp.update({"cdate":temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate":temp['mdate'].strftime(FORMAT_DATETIME)})
                try:
                    history_request = db.session.query(HistoryRequestModel)
                    history_request = history_request.filter(getattr(HistoryRequestModel, "type") == 'request')
                    history_request = history_request.filter(getattr(HistoryRequestModel, "type_id") == temp['id'])
                    history_request = history_request.order_by(text("id desc"))

                    if sort[0] == 'datetime':
                        history_request = history_request.order_by(text("history_request."+sort[0]+" "+sort[1]))

                    # populate categorys
                    categorys = self.populate_categorys(search)

                    history_request = history_request.filter(or_(
                        cast(getattr(HistoryRequestModel, "datetime"), sqlalchemy.String).ilike('%'+categorys+'%'),
                        cast(getattr(HistoryRequestModel, "category"), sqlalchemy.String).ilike('%'+categorys+'%'),
                        cast(getattr(HistoryRequestModel, "type_id"), sqlalchemy.String).ilike('%'+categorys+'%')
                    ))
                    history_request = history_request.limit(1)
                    history_request = history_request.one()

                    # populate history_request
                    history_request = self.populate_history_request(history_request)

                except Exception as err:
                    history_request = {}

                temp['history_request'] = history_request

                request.append(temp)

            # check if empty
            if request:
                return True, request
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_categorys(self, search):
        ''' Doc: function populate_categorys '''
        if search.lower() == 'persetujuan walidata':
            categorys = '1'
        elif search.lower() == 'proses opd':
            categorys = '2'
        elif search.lower() == 'ditolak':
            categorys = '4'
        elif search.lower() == VAR_DATA_TERSEDIA:
            categorys = '3'
        else:
            categorys = search

        return categorys

    def populate_history_request(self, history_request):
        ''' Doc: function populate_history_request '''
        if history_request:
            history_request = history_request.__dict__
            history_request.pop('_sa_instance_state', None)
            history_request['datetime'] = history_request['datetime'].strftime(FORMAT_DATETIME)
            history_request['category_id'] = history_request['category']
            if int(history_request['category']) == int(1):
                history_request['category'] = 'Persetujuan Walidata'
            elif int(history_request['category']) == int(2):
                history_request['category'] = 'Proses OPD'
            elif int(history_request['category']) == int(4):
                history_request['category'] = 'Ditolak'
            elif int(history_request['category']) == int(3):
                history_request['category'] = VAR_DATA_TERSEDIA
            else:
                history_request['category'] = None
        else:
            history_request = {}

        return history_request

    def get_count(self, where: dict, search):
        ''' Doc: function get count '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id '''
        try:
            # execute database
            result = self.query({'id':_id}, '')
            result = result.all()

            # change into dict
            request = []
            for res in result:
                temp = res._asdict()
                temp.update({"cdate":temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate":temp['mdate'].strftime(FORMAT_DATETIME)})
                try:
                    history_request = db.session.query(HistoryRequestModel)
                    history_request = history_request.filter(getattr(HistoryRequestModel, "type") == 'request')
                    history_request = history_request.filter(getattr(HistoryRequestModel, "type_id") == temp['id'])
                    history_request = history_request.order_by(text("id desc"))
                    history_request = history_request.limit(1)
                    history_request = history_request.one()

                    # populate history request
                    history_request = self.populate_history_request_single(history_request)
                except Exception as err:
                    history_request = {}

                temp['history_request'] = history_request

                request.append(temp)

            # check if empty
            if request:
                return True, request[0], "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_history_request_single(self, history_request):
        ''' Doc: function populate_history_request_single '''
        if history_request:
            history_request = history_request.__dict__
            history_request.pop('_sa_instance_state', None)
            history_request['datetime'] = history_request['datetime'].strftime(FORMAT_DATETIME)
            if int(history_request['category']) == int(1):
                history_request['category'] = 'Persetujuan Walidata'
            elif int(history_request['category']) == int(2):
                history_request['category'] = 'Proses OPD'
            elif int(history_request['category']) == int(4):
                history_request['category'] = 'Ditolak'
            elif int(history_request['category']) == int(3):
                history_request['category'] = VAR_DATA_TERSEDIA
            else:
                history_request['category'] = None
        else:
            history_request = {}

        return history_request

    def post_param(self, json, condition):
        ''' Doc: function post param '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            if json['title']:
                json_send["title"] = json['title'][0].upper()+json['title'][1:]
            if json['notes']:
                json_send["notes"] = json['notes'][0].upper()+json['notes'][1:]

            if condition == 'add':
                json_send["cuid"] = jwt['id']
                json_send["cdate"] = HELPER.local_date_server()
            else:
                json_send["muid"] = jwt['id']
                json_send["mdate"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create '''
        try:
            # prepare data model
            param = self.post_param(json, 'add')
            result = RequestModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update '''
        try:
            # generate json data
            param = self.post_param(json, 'edit')

            try:
                # prepare data model
                result = db.session.query(RequestModel)
                for attr, value in where.items():
                    result = result.filter(getattr(RequestModel, attr) == value)

                # execute database
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()
                res, request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete '''
        try:

            try:
                # prepare data model
                result = db.session.query(RequestModel)
                for attr, value in where.items():
                    result = result.filter(getattr(RequestModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
