''' Doc: controller mapset metadata  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetMetadataModel
from models import MapsetMetadataTypeModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MapsetMetadataController(object):
    ''' Doc: class mapset metadata  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(MapsetMetadataModel)
            for attr, value in where.items():
                result = result.filter(getattr(MapsetMetadataModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetMetadataModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MapsetMetadataModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            mapset_metadata = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to mapset_metadata_type
                mapset_metadata_type = db.session.query(MapsetMetadataTypeModel)
                mapset_metadata_type = mapset_metadata_type.get(temp['mapset_metadata_type_id'])

                if mapset_metadata_type:
                    mapset_metadata_type = mapset_metadata_type.__dict__
                    mapset_metadata_type.pop('_sa_instance_state', None)
                else:
                    mapset_metadata_type = {}

                temp['mapset_metadata_type'] = mapset_metadata_type

                mapset_metadata.append(temp)

            # check if empty
            mapset_metadata = list(mapset_metadata)
            if mapset_metadata:
                return True, mapset_metadata
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(MapsetMetadataModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(MapsetMetadataModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetMetadataModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MapsetMetadataModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(MapsetMetadataModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to mapset_metadata
                mapset_metadata_type = db.session.query(MapsetMetadataTypeModel)
                mapset_metadata_type = mapset_metadata_type.get(result['mapset_metadata_type_id'])

                if mapset_metadata_type:
                    mapset_metadata_type = mapset_metadata_type.__dict__
                    mapset_metadata_type.pop('_sa_instance_state', None)
                else:
                    mapset_metadata_type = {}

                result['mapset_metadata_type'] = mapset_metadata_type

            else:
                result = {}

            # change into dict
            mapset_metadata = result

            # check if empty
            if mapset_metadata:
                return True, mapset_metadata
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            # prepare data model
            result = MapsetMetadataModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, mapset_metadata = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, mapset_metadata
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []

            for json in jsons:
                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = MapsetMetadataModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, mapset_metadata = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(mapset_metadata)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(MapsetMetadataModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetMetadataModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, mapset_metadata = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, mapset_metadata
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_multiple(self, mapset_id, json: dict):
        ''' Doc: function update multiple  '''
        try:
            result = db.session.query(MapsetMetadataModel)
            result = result.filter(MapsetMetadataModel.mapset_id == mapset_id)
            result = result.all()

            for res in result:
                db.session.delete(res)
                db.session.commit()

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = MapsetMetadataModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, mapset_metadata = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(mapset_metadata)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(MapsetMetadataModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetMetadataModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, mapset_metadata = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
