import json

from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage, BadRequest
from models import ChangelogModel
from helpers.postgre_alchemy import postgre_alchemy as db

import sqlalchemy, sentry_sdk


class ChangelogController:
    def __init__(self):
        self.config = Configuration()
        self.helper = Helper()

    def get_changelog(self, id: int):
        try:
            query = db.session.query(ChangelogModel).filter(ChangelogModel.id == id, ChangelogModel.is_deleted.is_(False))
            
            user = self.helper.read_jwt()
            if not user:
                query.filter(ChangelogModel.is_public.is_(True))
                
            data = query.first()
            
            if not data:
                raise BadRequest(f"Data not found with id: {id}")
            
            return self.helper.serialize(data)
        except BadRequest as e:
            raise
        except sqlalchemy.exc.SQLAlchemyError as e:
            raise ErrorMessage(e.args[0])

    def get_changelogs(self, where: str = r"{}", search: str = "", sort: str = "id:asc", limit: int = 100, offset: int = 0, count: bool = False):
        try:
            where_conditions = json.loads(where)
            if ':' not in sort:
                raise BadRequest("Invalid sort format. Expected format: 'column:order'")
            sort_column, sort_order = sort.split(':')
            if sort_order.lower() not in ["asc", "desc"]:
                raise BadRequest(f"Invalid sort order: {sort_order}. Expected 'asc' or 'desc'")
            if not hasattr(ChangelogModel, sort_column):
                raise BadRequest(f"Invalid sort column: {sort_column}")

            query = db.session.query(ChangelogModel)

            version_query = db.session.query(ChangelogModel.version).distinct()
            timestamp_query = db.session.query(ChangelogModel.timestamp).distinct()
            filters = [ChangelogModel.is_deleted.is_(False)]

            user = self.helper.read_jwt()
            if not user:
                filters.append(ChangelogModel.is_public.is_(True))

            for col, value in where_conditions.items():
                if not hasattr(ChangelogModel, col):
                    raise ErrorMessage(f"Invalid filter column: {col}")
                if isinstance(value, list):
                    filters.append(getattr(ChangelogModel, col).in_(value))
                else:
                    filters.append(getattr(ChangelogModel, col) == value)

            if search:
                filters.append(ChangelogModel.title.ilike(f'%{search}%'))

            if filters:
                query = query.filter(*filters)
                version_query = version_query.filter(*filters)
                timestamp_query = timestamp_query.filter(*filters)

            sort_attr = getattr(ChangelogModel, sort_column)
            sort_expr = sort_attr.desc() if sort_order == "desc" else sort_attr.asc()
            query = query.order_by(sort_expr)
            if count:
                return query.count()

            if offset > 0:
                query = query.offset(offset)
            if limit > 0:
                query = query.limit(limit)

            data = query.all()

            try:
                versions = [v[0] for v in version_query.all() if v[0] is not None]
                timestamps = [t[0] for t in timestamp_query.all() if t[0] is not None]
            except Exception:
                versions = []
                timestamps = []

            return {
                "data": [self.helper.serialize(item) for item in data],
                "metadata": {
                    "version": versions,
                    "timestamp": timestamps
                }
            }
        except BadRequest as e:
            raise
        except json.JSONDecodeError:
            raise BadRequest("Invalid JSON in request body")
        except sqlalchemy.exc.SQLAlchemyError as e:
            sentry_sdk.capture_exception(e)
            raise
        except Exception as e:
            sentry_sdk.capture_exception(e)
            raise

    def create_changelog(self, title: str, description: str, version: str, timestamp: str, is_public: bool):
        try:
            if not title or len(title) > 255:
                raise BadRequest("Title is required and must not exceed 255 characters")

            if not description:
                raise BadRequest("Description is required")

            if not version or len(version) > 50:
                raise BadRequest("Version is required and must not exceed 50 characters")

            if not timestamp:
                raise BadRequest("Timestamp is required")

            if not isinstance(is_public, bool):
                raise BadRequest("is_public must be a boolean value")

            data = ChangelogModel(title, description, version, timestamp, is_public)
            db.session.add(data)
            db.session.commit()
            return self.helper.serialize(data)
        except BadRequest as e:
            raise
        except sqlalchemy.exc.SQLAlchemyError as e:
            db.session.rollback()
            sentry_sdk.capture_exception(e)
            raise ErrorMessage(e.args[0])
        except Exception as e:
            db.session.rollback()
            sentry_sdk.capture_exception(e)
            raise ErrorMessage(f"Unexpected error: {str(e)}")

    def update_changelog(self, id: int, data_to_update: dict):
        try:
            data = db.session.query(ChangelogModel).filter(ChangelogModel.id == id).first()
            if not data:
                raise BadRequest(f"Data not found with id: {id}")

            allowed_fields = ['title', 'description', 'version', 'timestamp', 'is_public']
            invalid_fields = [field for field in data_to_update.keys() if field not in allowed_fields]
            if invalid_fields:
                raise BadRequest(f"Invalid fields for update: {', '.join(invalid_fields)}")

            if 'title' in data_to_update:
                if not data_to_update['title'] or len(data_to_update['title']) > 255:
                    raise BadRequest("Title is required and must not exceed 255 characters")

            if 'description' in data_to_update:
                if not data_to_update['description']:
                    raise BadRequest("Description is required")

            if 'version' in data_to_update:
                if not data_to_update['version'] or len(data_to_update['version']) > 50:
                    raise BadRequest("Version is required and must not exceed 50 characters")

            if 'timestamp' in data_to_update:
                if not data_to_update['timestamp']:
                    raise BadRequest("Timestamp is required")

            if 'is_public' in data_to_update:
                if not isinstance(data_to_update['is_public'], bool):
                    raise BadRequest("is_public must be a boolean value")

            for field, value in data_to_update.items():
                setattr(data, field, value)
            db.session.commit()
            return self.helper.serialize(data)
        except BadRequest as e:
            raise
        except sqlalchemy.exc.SQLAlchemyError as e:
            db.session.rollback()
            sentry_sdk.capture_exception(e)
            raise ErrorMessage(f"Database error: {str(e)}")
        except Exception as e:
            db.session.rollback()
            sentry_sdk.capture_exception(e)
            raise ErrorMessage(f"Unexpected error: {str(e)}")

    def delete_changelog(self, id: int, permanent: bool = False):
        changelog = db.session.query(ChangelogModel).filter(ChangelogModel.id == id).first()
        if not changelog:
            raise BadRequest(f"Data not found with id: {id}")

        if permanent:
            data_to_delete = changelog
            db.session.delete(data_to_delete)
            db.session.delete(changelog)
        else:
            changelog.is_deleted = True

        db.session.commit()
        return self.helper.serialize(changelog)