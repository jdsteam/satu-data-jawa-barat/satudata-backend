''' Doc: controller search  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import SearchModel
from helpers.postgre_alchemy import postgre_alchemy as db
import helpers.postgre_psycopg as db2
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class SearchController(object):
    ''' Doc: class search  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(SearchModel)
            for attr, value in where.items():
                result = result.filter(getattr(SearchModel, attr) == value)
            result = result.filter(or_(cast(getattr(SearchModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in SearchModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            search = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                search.append(temp)

            # check if empty
            search = list(search)
            if search:
                return True, search
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(SearchModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(SearchModel, attr) == value)
            result = result.filter(or_(cast(getattr(SearchModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in SearchModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(SearchModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}

            # change into dict
            search = result

            # check if empty
            if search:
                return True, search
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["datetime"] = HELPER.local_date_server()

            # prepare data model
            result = SearchModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, search = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, search
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(SearchModel)
                for attr, value in where.items():
                    result = result.filter(getattr(SearchModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, search = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, search
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(SearchModel)
                for attr, value in where.items():
                    result = result.filter(getattr(SearchModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, search = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_global_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            limit = round(int(limit) / 5)

            # default result
            data = []

            # feature
            list_data = ['dataset', 'mapset', 'visualization', 'infographic', 'article']

            list_query = {
                'dataset' :
                    '''
                        SELECT
                            dataset_view_all.id, dataset_view_all.name, dataset_view_all.title,
                            sektoral_id, topic as sektoral_name, sektoral.picture as sektoral_picture,
                            sektoral.picture_thumbnail as picture_thumbnail,
                            dataset_view_all.regional_id, regional.kode_bps as kode_bps, regional.nama_kemendagri as regional_name,
                            regional.nama_kemendagri as regional_nama_kemendagri,
                            dataset_view_all.kode_skpd, dataset_view_all.organization as nama_skpd, skpd.title as skpd_title,
                            count_view, count_access,
                            dataset_view_all.is_active, dataset_view_all.is_deleted,
                            dataset_view_all.is_validate, dataset_view_all.is_permanent,
                            dataset_view_all.cuid, dataset_view_all.cdate,
                            dataset_view_all.muid, dataset_view_all.mdate,
                            metadata
                        FROM
                            (
                            SELECT
                                dataset_view_all.id,
                                json_agg(metadata) as metadata
                            FROM
                                dataset_view_all
                            JOIN metadata on
                                dataset_view_all.id = metadata.dataset_id
                            GROUP BY
                                dataset_view_all.id
                                ) as mtd
                        JOIN dataset_view_all on dataset_view_all.id = mtd.id
                        JOIN sektoral on dataset_view_all.sektoral_id = sektoral.id
                        JOIN skpd on dataset_view_all.kode_skpd = skpd.kode_skpd
                        JOIN regional on dataset_view_all.regional_id = regional.id
                        WHERE 1=1 and dataset_class_id = 3 and is_validate = 3
                    ''',
                'mapset' :
                    '''
                        SELECT
                            mapset.id, mapset.name, mapset.title,
                            mapset.mapset_source_id, mapset_source.name as mapset_source_name, mapset.app_link,
                            mapset.mapset_type_id, mapset_type.name as mapset_type_name,
                            mapset.sektoral_id, sektoral.name as sektoral_name,
                            mapset.regional_id, regional.nama_kemendagri as regional_name,
                            mapset.kode_skpd, skpd.nama_skpd,
                            mapset.tahun, mapset.count_view, mapset.count_access,
                            mapset.is_active, mapset.is_deleted,
                            mapset.is_validate, mapset.is_permanent,
                            mapset.cuid, mapset.cdate,
                            mapset.muid, mapset.mdate,
                            row_to_json(mapset_source) as mapset_source,
                            row_to_json(sektoral) as sektoral,
                            row_to_json(skpd) as skpd,
                            row_to_json(mapset_type) as mapset_type
                        FROM mapset
                        JOIN sektoral on mapset.sektoral_id = sektoral.id
                        JOIN mapset_source on mapset.mapset_source_id = mapset_source.id
                        JOIN mapset_type on mapset.mapset_type_id = mapset_type.id
                        JOIN regional on mapset.regional_id = regional.id
                        JOIN skpd on mapset.kode_skpd = skpd.kode_skpd
                        WHERE 1=1 and dataset_class_id = 3 and is_validate = 3
                    ''',
                'visualization' :
                    '''
                        SELECT
                            visualization.id, visualization.name, visualization.title, visualization.image,
                            visualization.sektoral_id, sektoral.name as sektoral_name,
                            visualization.regional_id, regional.nama_kemendagri as regional_name,
                            visualization.kode_skpd, skpd.nama_skpd,
                            visualization.description,
                            visualization.count_view,
                            visualization.is_active, visualization.is_deleted, visualization.is_validate,
                            visualization.cuid, visualization.cdate,
                            visualization.muid, visualization.mdate,
                            row_to_json(sektoral) as sektoral,
                            row_to_json(skpd) as skpd,
                            row_to_json(regional) as regional
                        FROM visualization
                        JOIN sektoral on visualization.sektoral_id = sektoral.id
                        JOIN regional on visualization.regional_id = regional.id
                        JOIN skpd on visualization.kode_skpd = skpd.kode_skpd
                        WHERE 1=1 and dataset_class_id = 3 and is_validate = true
                    ''',
                'infographic' :
                    '''
                        SELECT
                            infographic.id, infographic.name, infographic.title, infographic.image,
                            infographic.sektoral_id, sektoral.name as sektoral_name,
                            infographic.regional_id, regional.nama_kemendagri as regional_name,
                            infographic.owner,
                            infographic.description,
                            infographic.count_view,
                            infographic.is_active, infographic.is_deleted, infographic.is_validate,
                            infographic.cuid, infographic.cdate,
                            infographic.muid, infographic.mdate,
                            row_to_json(sektoral) as sektoral
                        FROM infographic
                        JOIN sektoral on infographic.sektoral_id = sektoral.id
                        JOIN regional on infographic.regional_id = regional.id
                        WHERE 1=1 and dataset_class_id = 3 and is_validate = true
                    ''',
                'article' :
                    '''
                        SELECT
                            article.id, article.name, article.title, article.image,
                            article.article_topic as topic, article.creator, article.organization,
                            article.count_view,
                            article.content_short,
                            article.is_active, article.is_deleted,
                            article.cuid, article.cdate,
                            article.muid, article.mdate
                        FROM article
                        WHERE 1=1
                    '''
            }

            # query
            for list in list_data:
                if list == 'dataset':
                    list2 = 'dataset_view_all'
                else:
                    list2 = list

                sql = list_query[list]

                for attr, value in where.items():
                    if attr == 'regional_id' and list == 'article':
                        pass
                    else:
                        sql += f'''AND {list2}.{attr} = '{value}' '''

                if search:
                    sql += f'''AND {list2}.name ILIKE '%{search}%' '''

                if sort:
                    sql += f'''ORDER BY {list2}.{sort[0]} {str(sort[1])} '''

                sql += f'''OFFSET {skip} LIMIT {limit} '''

                success, result = db2.query_dict_array(sql, None)

                if success:
                    for res in result:
                        temp = res
                        temp['cdate'] = res['cdate'].strftime(DATETIME_FORMAT)
                        temp['mdate'] = res['mdate'].strftime(DATETIME_FORMAT)
                        data.append({'category': list, 'category_data': temp})

            return True, data

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_global_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # default result
            data = {
                'dataset': 0,
                'mapset': 0,
                'visualization': 0,
                'infographic': 0,
                'article': 0,
            }

            # feature
            list_data = ['dataset', 'mapset', 'visualization', 'infographic', 'article']

            # query
            for list in list_data:
                sql = f'''select count(*) from {list} '''
                sql += f'''WHERE 1=1 '''

                for attr, value in where.items():
                    if attr == 'regional_id' and list == 'article':
                        pass
                    else:
                        sql += f'''AND {attr} = '{value}' '''

                if search:
                    sql += f'''AND name ILIKE '%{search}%' '''

                success, result = db2.query_dict_single(sql, None)

                if success:
                    data[list] = result['count']

            return True, data

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
