''' Doc: controller visualization dataset  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import VisualizationDatasetModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class VisualizationDatasetController(object):
    ''' Doc: class visualization dataset  '''

    def __init__(self, **kwargs):
        ''' Doc: function init '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all '''
        try:
            # query postgresql
            result = db.session.query(VisualizationDatasetModel)
            for attr, value in where.items():
                result = result.filter(getattr(VisualizationDatasetModel, attr) == value)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            visualization_dataset = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate":temp['cdate'].strftime(FORMAT_DATETIME)})
                visualization_dataset.append(temp)

            # check if empty
            visualization_dataset = list(visualization_dataset)
            if visualization_dataset:
                return True, visualization_dataset
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count '''
        try:
            # query postgresql
            result = db.session.query(VisualizationDatasetModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(VisualizationDatasetModel, attr) == value)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id '''
        try:
            # execute database
            result = db.session.query(VisualizationDatasetModel)
            result = result.options(
                defer("cuid"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                result['cdate'] = result['cdate'].strftime(FORMAT_DATETIME)
            else:
                result = {}

            # change into dict
            visualization_dataset = result

            # check if empty
            if visualization_dataset:
                return True, visualization_dataset, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all_by_id(self, _id):
        ''' Doc: function get all by id '''
        try:
            # query postgresql
            result = db.session.query(VisualizationDatasetModel)
            result = result.filter(VisualizationDatasetModel.visualization_id == _id)
            result = result.all()

            # change into dict
            visualization_dataset = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate":temp['cdate'].strftime(FORMAT_DATETIME)})
                visualization_dataset.append(temp)

            # check if empty
            visualization_dataset = list(visualization_dataset)
            if visualization_dataset:
                return visualization_dataset
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cuid"] = jwt['id']
            json_send["cdate"] = HELPER.local_date_server()
            # prepare data model
            result = VisualizationDatasetModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, visualization_dataset, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, visualization_dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            output = []
            for res in json:
                res.update({"cuid":jwt['id']})
                res.update({"cdate":HELPER.local_date_server()})
                output.append(res)

            db.session.execute(
                VisualizationDatasetModel.__table__.insert().values(output)
            )
            db.session.commit()
            visualization_dataset = self.get_all_by_id(output[0]['visualization_id'])
            # check if exist

            if visualization_dataset:
                return True, visualization_dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update '''
        try:
            # prepare data model
            result = db.session.query(VisualizationDatasetModel)
            for attr, value in where.items():
                result = result.filter(getattr(VisualizationDatasetModel, attr) == value)
            # execute database
            result = result.update(json, synchronize_session='fetch')
            result = db.session.commit()
            res, visualization_dataset, message = self.get_by_id(where["id"])

            # check if empty
            if res:
                return True, visualization_dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            raise ErrorMessage("Id not found " + str(err), 500, 1, {})

    def update_multiple(self, where: dict, json: dict):
        ''' Doc: function update multiple '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            output = []
            for res in json:
                res.update({"cuid":jwt['id']})
                res.update({"cdate":HELPER.local_date_server()})
                output.append(res)

            #delete data before insert
            try:
                result = db.session.query(VisualizationDatasetModel)
                for attr, value in where.items():
                    result = result.filter(getattr(VisualizationDatasetModel, attr) == value)
                result = result.delete()
            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

            db.session.execute(
                VisualizationDatasetModel.__table__.insert().values(output)
            )
            db.session.commit()
            visualization_dataset = self.get_all_by_id(output[0]['visualization_id'])

            # check if exist
            if visualization_dataset:
                return True, visualization_dataset
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete '''
        try:

            try:
                # prepare data model
                result = db.session.query(VisualizationDatasetModel)
                for attr, value in where.items():
                    result = result.filter(getattr(VisualizationDatasetModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, visualization_dataset, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
