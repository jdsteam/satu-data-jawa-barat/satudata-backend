''' Doc: controller dashboard  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
import sentry_sdk, math
from datetime import datetime, timedelta

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison
class DashboardNpsController(object):
    ''' Doc: constructor dashboard  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_nps_score(self, filter_type, filter_value):
        ''' Doc: function get nps score  '''
        try:
            this_year = datetime.today().strftime("%Y")

            # now
            final_result_now = {
                "nps_total_count": 0,
                "nps_promoters_count": 0,
                "nps_dectators_count": 0,
                "nps_promoters_percent": 0,
                "nps_dectators_percent": 0,
                "nps_score": 0,
            }

            sql_now = "with nps_series as (select generate_series(0,10) as score) "
            sql_now += "select nps_series.score, COALESCE(count(nps.id), 0) as count "
            sql_now += "from nps_series left join nps on nps_series.score = nps.score and "
            sql_now += "date_part('year', nps.datetime) = " + this_year + " and "
            sql_now += "date_part('"+filter_type+"', nps.datetime) = " + filter_value + " and "
            sql_now += "nps.is_active = true and nps.is_deleted = false "
            sql_now += "GROUP BY nps_series.score "
            sql_now += "ORDER BY nps_series.score asc "

            result_now = db.engine.execute(sql_now)

            for res_now in result_now:
                temp = res_now._asdict()
                final_result_now['nps_total_count'] += temp['count']
                if temp['score'] >= 0 and temp['score'] <= 6:
                    final_result_now['nps_dectators_count'] += temp['count']
                elif temp['score'] >= 7 and temp['score'] <= 10:
                    final_result_now['nps_promoters_count'] += temp['count']

            try:
                final_result_now['nps_promoters_percent'] = round(((final_result_now['nps_promoters_count'] / final_result_now['nps_total_count']) * 100), 2)
                final_result_now['nps_dectators_percent'] = round(((final_result_now['nps_dectators_count'] / final_result_now['nps_total_count']) * 100), 2)
                final_result_now['nps_score'] = round((final_result_now['nps_promoters_percent'] - final_result_now['nps_dectators_percent']), 2)
            except ZeroDivisionError:
                final_result_now['nps_promoters_percent'] = 0
                final_result_now['nps_dectators_percent'] = 0
                final_result_now['nps_score'] = 0

            # before
            final_result_before = {
                "nps_total_count": 0,
                "nps_promoters_count": 0,
                "nps_dectators_count": 0,
                "nps_promoters_percent": 0,
                "nps_dectators_percent": 0,
                "nps_score": 0,
            }

            if filter_type == 'month':
                if filter_value == 1:
                    this_year = int(this_year) - 1
                    filter_value = 12
                else:
                    filter_value = int(filter_value) - 1
            elif filter_type == 'quarter':
                if filter_value == 1:
                    this_year = int(this_year) - 1
                    filter_value = 4
                else:
                    filter_value = int(filter_value) - 1

            sql_before = "with nps_series as (select generate_series(0,10) as score) "
            sql_before += "select nps_series.score, COALESCE(count(nps.id), 0) as count "
            sql_before += "from nps_series left join nps on nps_series.score = nps.score and "
            sql_before += "date_part('year', nps.datetime) = " + str(this_year) + " and "
            sql_before += "date_part('"+filter_type+"', nps.datetime) = " + str(filter_value) + " and "
            sql_before += "nps.is_active = true and nps.is_deleted = false "
            sql_before += "GROUP BY nps_series.score "
            sql_before += "ORDER BY nps_series.score asc "

            result_before = db.engine.execute(sql_before)

            for res_before in result_before:
                temp = res_before._asdict()
                final_result_before['nps_total_count'] += temp['count']
                if temp['score'] >= 0 and temp['score'] <= 6:
                    final_result_before['nps_dectators_count'] += temp['count']
                elif temp['score'] >= 7 and temp['score'] <= 10:
                    final_result_before['nps_promoters_count'] += temp['count']

            try:
                final_result_before['nps_promoters_percent'] = round(((final_result_before['nps_promoters_count'] / final_result_before['nps_total_count']) * 100), 2)
                final_result_before['nps_dectators_percent'] = round(((final_result_before['nps_dectators_count'] / final_result_before['nps_total_count']) * 100), 2)
                final_result_before['nps_score'] = round((final_result_before['nps_promoters_percent'] - final_result_before['nps_dectators_percent']), 2)
            except ZeroDivisionError:
                final_result_before['nps_promoters_percent'] = 0
                final_result_before['nps_dectators_percent'] = 0
                final_result_before['nps_score'] = 0

            # final
            final_result = {
                "nps_total_count": final_result_now['nps_total_count'],
                "nps_promoters_count": final_result_now['nps_promoters_count'],
                "nps_dectators_count": final_result_now['nps_dectators_count'],
                "nps_promoters_percent": final_result_now['nps_promoters_percent'],
                "nps_dectators_percent": final_result_now['nps_dectators_percent'],
                "nps_score": final_result_now['nps_score'],
            }

            try:
                final_result["growth"] = round((((final_result_now['nps_score'] - final_result_before['nps_score']) / final_result_before['nps_score']) * 100), 2)
            except ZeroDivisionError:
                final_result["growth"] = 100

            if filter_type == 'month':
                final_result['growth_message'] = "dari bulan lalu"
            elif filter_type == 'quarter':
                final_result['growth_message'] = "dari kuartal lalu"
            else:
                final_result['growth_message'] = "dari xxx lalu"

            # check if empty
            if final_result:
                return True, final_result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_nps_distribution(self, filter_type, filter_value):
        ''' Doc: function get nps distribution  '''
        try:
            this_year = datetime.today().strftime("%Y")

            # now
            final_result = []

            sql = "with nps_series as (select generate_series(0,10) as score) "
            sql += "select nps_series.score, COALESCE(count(nps.id), 0) as count "
            sql += "from nps_series left join nps on nps_series.score = nps.score and "
            sql += "date_part('year', nps.datetime) = " + this_year + " and "
            sql += "date_part('"+filter_type+"', nps.datetime) = " + filter_value + " and "
            sql += "nps.is_active = true and nps.is_deleted = false "
            sql += "GROUP BY nps_series.score "
            sql += "ORDER BY nps_series.score asc "

            result = db.engine.execute(sql)

            for res in result:
                temp = res._asdict()
                if temp['score'] >= 0 and temp['score'] <= 6:
                    temp['type'] = 'Dectators'
                elif temp['score'] >= 7 and temp['score'] <= 10:
                    temp['type'] = 'Promoters'
                final_result.append(temp)

            # check if empty
            if final_result:
                return True, final_result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_nps_trend(self, filter_type):
        ''' Doc: function get nps trend  '''
        try:
            this_year = datetime.today().strftime("%Y")

            # now
            final_result = []

            if filter_type == 'month':
                max_range = 13
            elif filter_type == 'quarter':
                max_range = 5

            for value in range(1, max_range):
                temp_result = {}
                temp_result['filter_type'] = filter_type
                temp_result['filter_value'] = this_year + '-' + str(value)
                temp_result['nps_total_count'] = 0
                temp_result['nps_dectators_count'] = 0
                temp_result['nps_promoters_count'] = 0
                temp_result['nps_dectators_percent'] = 0
                temp_result['nps_promoters_percent'] = 0
                temp_result['nps_score'] = 0

                sql = "with nps_series as (select generate_series(0,10) as score) "
                sql += "select nps_series.score, COALESCE(count(nps.id), 0) as count "
                sql += "from nps_series left join nps on nps_series.score = nps.score and "
                sql += "date_part('year', nps.datetime) = " + this_year + " and "
                sql += "date_part('"+filter_type+"', nps.datetime) = " + str(value) + " and "
                sql += "nps.is_active = true and nps.is_deleted = false "
                sql += "GROUP BY nps_series.score "
                sql += "ORDER BY nps_series.score asc "

                result = db.engine.execute(sql)

                for res in result:
                    temp = res._asdict()
                    temp_result['nps_total_count'] += temp['count']
                    if temp['score'] >= 0 and temp['score'] <= 6:
                        temp_result['nps_dectators_count'] += temp['count']
                    elif temp['score'] >= 7 and temp['score'] <= 10:
                        temp_result['nps_promoters_count'] += temp['count']

                try:
                    temp_result['nps_promoters_percent'] = round(((temp_result['nps_promoters_count'] / temp_result['nps_total_count']) * 100), 2)
                    temp_result['nps_dectators_percent'] = round(((temp_result['nps_dectators_count'] / temp_result['nps_total_count']) * 100), 2)
                    temp_result['nps_score'] = round((temp_result['nps_promoters_percent'] - temp_result['nps_dectators_percent']), 2)
                except ZeroDivisionError:
                    temp_result['nps_promoters_percent'] = 0
                    temp_result['nps_dectators_percent'] = 0
                    temp_result['nps_score'] = 0

                final_result.append(temp_result)

            # check if empty
            if final_result:
                return True, final_result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
