''' Doc: controller dashboard  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
import sentry_sdk, math
from datetime import timedelta
from dateutil import parser

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison
class DashboardFeedbackPrivateController(object):
    ''' Doc: constructor dashboard  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def filter_type(self, start_date, end_date, types):
        ''' Doc: function filter date type  '''
        try:
            start = parser.parse(start_date)
            end = parser.parse(end_date)
            diff = end - start
            distance_day = (diff.days + 1)
            max_days = 30

            parse_growth_end = parser.parse(start_date)
            parser_end_date = parse_growth_end - timedelta(days = 1)
            end_date_growth = parser_end_date.strftime("%Y-%m-%d")

            if types == "default":
                growth_date = start - timedelta(days = max_days)
                total_days = max_days
            else:
                growth_date = start - timedelta(days = distance_day)
                total_days = distance_day

            start_date_growth = growth_date.strftime("%Y-%m-%d")

            return start_date_growth, total_days, end_date_growth
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            return err, False

    def filter_trend(self, group):
        ''' Doc: function filter query trend  '''
        try:
            if group == "month":
                select = "date_trunc('month',datetime) as date, "
                group_by = "group by date_trunc('month',datetime) "
                order_by = "order by date asc"
            elif group == "week":
                select = "date_trunc('week',datetime) as date, "
                group_by = "group by date_trunc('week',datetime) "
                order_by = "order by date asc"
            elif group == "day":
                select = "date_trunc('day',datetime) as date, "
                group_by = "group by date_trunc('day',datetime) "
                order_by = "order by date asc"
            else:
                select = "date(datetime), "
                group_by = "group by date(datetime) "
                order_by = "order by date(datetime) asc"

            return select, group_by, order_by
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            return err

    def get_feedback_total(self, start_date, end_date, types):
        ''' Doc: function get feedback_private total  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select count(tujuan) as total, "+
                "(select count(tujuan) from feedback_private where is_active = true "+
                "and is_deleted = false "+
                "and datetime::date >= '" + start + "'" +
                "and datetime::date <= '" + end + "'" +
                ") as growth from feedback_private " +
                "where is_active = true and is_deleted = false "+ filters
            )
            result = db.engine.execute(sql)
            res = [row for row in result]

            try:
                total_growth = ((res[0].total - res[0].growth) / res[0].growth) * 100
            except ZeroDivisionError:
                total_growth = 0

            # check if empty
            app_service = {
                "total": res[0].total,
                "growth": total_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_average(self, start_date, end_date, types):
        ''' Doc: function get feedback_private average  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select count(tujuan) as total, "+
                "(select count(tujuan) from feedback_private where is_active = true "+
                "and is_deleted = false "+
                "and datetime::date >= '" + start + "'" +
                "and datetime::date <= '" + end + "'" +
                ") as growth from feedback_private " +
                "where is_active = true and is_deleted = false " + filters
            )

            result = db.engine.execute(sql)
            res = [row for row in result]

            #logic growth bulatkan ke bawah
            total = math.floor(res[0].total / distance_day)
            total_growth_data = math.floor(res[0].growth / distance_day)

            try:
                total_growth = ((total - total_growth_data) / total_growth_data) * 100
            except ZeroDivisionError:
                total_growth = 0

            # check if empty
            app_service = {
                "total": total,
                "growth": total_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_trend(self, start_date, end_date, group):
        ''' Doc: function get feedback_private trend  '''
        try:
            select, group_by, order_by = self.filter_trend(group)
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select "+ select +"count(tujuan) as total from feedback_private where is_active = true " +
                "and is_deleted = false " + filters + group_by + order_by
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                row_append.update({"date":res['date'].strftime("%Y-%m-%d")})

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_trend2(self, start_date, end_date, group):
        ''' Doc: function get feedback_private trend  '''
        try:
            # query default
            sql_day = '''
            SELECT series.date, COALESCE(feedback_private.count, (0)::bigint) AS total
            FROM feedback_private_series_view series
            LEFT JOIN feedback_private_count_view feedback_private ON series.date = feedback_private.datetime
            '''
            filters = " WHERE date >='"+start_date+"' and date <='"+end_date+"' "

            # check group
            if group == 'day':
                sql = sql_day + filters
            elif group == 'week':
                sql = '''
                SELECT (date_trunc('week'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date AS date,
                sum(feedback_private_harian_view.total) AS total
                FROM
                ( ''' + sql_day + ''') as feedback_private_harian_view ''' + filters + '''
                GROUP BY ((date_trunc('week'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('week'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                '''
            elif group == 'month':
                sql = '''
                SELECT (date_trunc('month'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date AS date,
                sum(feedback_private_harian_view.total) AS total
                FROM
                ( ''' + sql_day + ''') as feedback_private_harian_view ''' + filters + '''
                GROUP BY ((date_trunc('month'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('month'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                '''
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                row_append.update({"date":res['date'].strftime("%Y-%m-%d")})
                row_append.update({"total": round(res['total'])})

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_satisfaction(self, start_date, end_date, types, goal):
        ''' Doc: function get feedback_private total  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select count("+goal+") as total, "+
                "(select count("+goal+") from feedback_private where is_active = true and "+goal+" in (true) "+
                "and is_deleted = false "+
                "and datetime::date >= '" + start + "'" +
                "and datetime::date <= '" + end + "'" +
                ") as growth, " +
                "(select count(tujuan) from feedback_private where is_active = true "+
                "and is_deleted = false "+ filters +
                ") as total_data from feedback_private " +
                "where is_active = true and is_deleted = false and "+goal+" in (true) " + filters
            )
            result = db.engine.execute(sql)
            res = [row for row in result]
            #logic growth bulatkan ke bawah
            total = math.floor(res[0].total / distance_day)
            total_growth_data = math.floor(res[0].growth / distance_day)

            try:
                total_growth = ((total - total_growth_data) / total_growth_data) * 100
            except ZeroDivisionError:
                total_growth = 0

            try:
                final_total = round(res[0].total / res[0].total_data * 100 ,2)
            except ZeroDivisionError:
                final_total = 0

            # check if empty
            app_service = {
                "total": final_total,
                "growth": total_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_satisfaction2(self, start_date, end_date, types, goal):
        ''' Doc: function get feedback total  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            filter_now = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            filter_past = "and datetime::date >='%s' and datetime::date <='%s'" % (start, end)

            # now
            sql_count_now = text('''
                select count('''+goal+''') as total
                from feedback_private
                where is_active = true and is_deleted = false
                and '''+goal+''' = true ''' + filter_now
                )
            result_count_now = db.engine.execute(sql_count_now)
            res_count_now = [row for row in result_count_now]
            sql_total_now = text('''
                select count(id) as total
                from feedback_private
                where is_active = true and is_deleted = false ''' + filter_now
                )
            result_total_now = db.engine.execute(sql_total_now)
            res_total_now = [row for row in result_total_now]
            try:
                satisfaction_now_count = round(res_count_now[0][0])
                satisfaction_now_total = round(res_total_now[0][0])
                satisfaction_now_persentation = round((satisfaction_now_count / satisfaction_now_total) * 100, 2)
            except Exception:
                satisfaction_now_count = 0
                satisfaction_now_total = 0
                satisfaction_now_persentation = 0

            # past
            sql_count_past = text('''
                select count('''+goal+''') as total
                from feedback_private
                where is_active = true and is_deleted = false
                and '''+goal+''' = true ''' + filter_past
                )
            result_count_past = db.engine.execute(sql_count_past)
            res_count_past = [row for row in result_count_past]
            sql_total_past = text('''
                select count(id) as total
                from feedback_private
                where is_active = true and is_deleted = false ''' + filter_past
                )
            result_total_past = db.engine.execute(sql_total_past)
            res_total_past = [row for row in result_total_past]
            try:
                satisfaction_past_count = round(res_count_past[0][0])
                satisfaction_past_total = round(res_total_past[0][0])
                satisfaction_past_persentation = round((satisfaction_past_count / satisfaction_past_total) * 100, 2)
            except Exception:
                satisfaction_past_count = 0
                satisfaction_past_total = 0
                satisfaction_past_persentation = 0

            # check if empty
            app_service = {
                "total": satisfaction_now_persentation,
                "growth": round(satisfaction_now_persentation - satisfaction_past_persentation, 2),
                "message": "dalam %s hari terakhir" % distance_day,
                "satisfaction_now_persentation": satisfaction_now_persentation,
                "satisfaction_past_persentation": satisfaction_past_persentation,
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_csat(self, start_date, end_date, types):
        ''' Doc: function get feedback_private total  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select count(score) as total, "+
                "(select count(score) from feedback_private where is_active = true and score BETWEEN 0 and 4 "+
                "and is_deleted = false "+
                "and datetime::date >= '" + start + "'" +
                "and datetime::date <= '" + end + "'" +
                ") as growth, " +
                "(select count(score) from feedback_private where is_active = true and score BETWEEN 0 and 4 "+
                "and is_deleted = false"+
                ") as total_data, "+
                "(select sum(score) from feedback_private where is_active = true and score BETWEEN 0 and 4 "+
                "and is_deleted = false "+ filters +
                ") as total_score from feedback_private " +
                "where is_active = true and is_deleted = false " +
                "and score BETWEEN 0 and 4 "+ filters
            )

            result = db.engine.execute(sql)
            res = [row for row in result]

            #logic growth bulatkan ke bawah
            total = math.floor(res[0].total / distance_day)
            total_growth_data = math.floor(res[0].growth / distance_day)

            try:
                total_growth = ((total - total_growth_data) / total_growth_data) * 100
            except ZeroDivisionError:
                total_growth = 0

            try:
                total_data = res[0].total * 4
                average_csat = (res[0].total_score / total_data) * 100
                final_total = float(round(average_csat, 2))
            except Exception:
                final_total = 0

            # check if empty
            app_service = {
                "total": final_total,
                "growth": total_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_csat2(self, start_date, end_date, types):
        ''' Doc: function get feedback total  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            filter_now = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            filter_past = "and datetime::date >='%s' and datetime::date <='%s'" % (start, end)

            sql_count_now = text('''
                select count(score) as total
                from feedback_private
                where is_active = true and is_deleted = false ''' + filter_now
                )
            result_count_now = db.engine.execute(sql_count_now)
            res_count_now = [row for row in result_count_now]
            sql_score_now = text('''
                select sum(score) as total
                from feedback_private
                where is_active = true and is_deleted = false ''' + filter_now
                )
            result_score_now = db.engine.execute(sql_score_now)
            res_score_now = [row for row in result_score_now]
            try:
                csat_now_count = round(res_count_now[0][0])
                csat_now_score = round(res_score_now[0][0])
                csat_now_score_max = csat_now_count * 4
                csat_now_persentation = round((csat_now_score / csat_now_score_max) * 100, 2)
            except Exception:
                csat_now_count = 0
                csat_now_score = 0
                csat_now_score_max = 0
                csat_now_persentation = 0

            sql_count_past = text('''
                select count(score) as total
                from feedback_private
                where is_active = true and is_deleted = false ''' + filter_past
                )
            result_count_past = db.engine.execute(sql_count_past)
            res_count_past = [row for row in result_count_past]
            sql_score_past = text('''
                select sum(score) as total
                from feedback_private
                where is_active = true and is_deleted = false ''' + filter_past
                )
            result_score_past = db.engine.execute(sql_score_past)
            res_score_past = [row for row in result_score_past]
            try:
                csat_past_count = round(res_count_past[0][0])
                csat_past_score = round(res_score_past[0][0])
                csat_past_score_max = csat_past_count * 4
                csat_past_persentation = round((csat_past_score / csat_past_score_max) * 100, 2)
            except Exception:
                csat_past_count = 0
                csat_past_score = 0
                csat_past_score_max = 0
                csat_past_persentation = 0

            # check if empty
            app_service = {
                "total": csat_now_persentation,
                "growth": round(csat_now_persentation - csat_past_persentation, 2),
                "message": "dalam %s hari terakhir" % distance_day,
                "csat_now_count": csat_now_count,
                "csat_now_score": csat_now_score,
                "csat_now_score_max": csat_now_score_max,
                "csat_now_persentation": csat_now_persentation,
                "csat_past_count": csat_past_count,
                "csat_past_score": csat_past_score,
                "csat_past_score_max": csat_past_score_max,
                "csat_past_persentation": csat_past_persentation,
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_csat_trend(self, start_date, end_date, group):
        ''' Doc: function get feedback_private csat trend  '''
        try:
            select, group_by, order_by = self.filter_trend(group)
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select "+ select +" sum(score) as score, count(score) from feedback_private " +
                "where is_active = true and is_deleted = false and score BETWEEN 0 and 4 " + filters + group_by + order_by
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                res['max'] = res['count'] * 4
                res['total'] = round((res['score'] / res['max']) * 100, 2)
                res['notes'] = 'total = csat dalam persen'
                row_append = res
                row_append.update({"date":res['date'].strftime("%Y-%m-%d")})
                row_append.update({"score": round(res['score'])})
                row_append.update({"total": round(res['total'])})

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_csat_trend2(self, start_date, end_date, group):
        ''' Doc: function get feedback_private csat trend  '''
        try:
            # query default
            sql_day = '''
            SELECT series.date, COALESCE(feedback_private.score, (0)::bigint) AS score, COALESCE(feedback_private.count, (0)::bigint) AS count
            FROM feedback_private_series_view series
            LEFT JOIN feedback_private_count_view feedback_private ON series.date = feedback_private.datetime
            '''
            filters = " WHERE date >='"+start_date+"' and date <='"+end_date+"' "

            # check group
            if group == 'day':
                sql = sql_day + filters
            elif group == 'week':
                sql = '''
                SELECT (date_trunc('week'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date AS date,
                sum(feedback_private_harian_view.score) AS score,
                sum(feedback_private_harian_view.count) AS count
                FROM
                ( ''' + sql_day + ''') as feedback_private_harian_view ''' + filters + '''
                GROUP BY ((date_trunc('week'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('week'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                '''
            elif group == 'month':
                sql = '''
                SELECT (date_trunc('month'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date AS date,
                sum(feedback_private_harian_view.score) AS score,
                sum(feedback_private_harian_view.count) AS count
                FROM
                ( ''' + sql_day + ''') as feedback_private_harian_view ''' + filters + '''
                GROUP BY ((date_trunc('month'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('month'::text, (feedback_private_harian_view.date)::timestamp with time zone))::date)
                '''
            result = db.engine.execute(sql)

            # maping object
            output = []
            i = 0
            for res in [dict(row) for row in result]:
                if res['count'] > 0:
                    res['count'] = round(res['count'])
                    res['score'] = round(res['score'])
                    res['max'] = res['count'] * 4
                    if res['max'] > 0:
                        res['total'] = round((res['score'] / res['max']) * 100, 2)
                        res['notes'] = 'ada feedback, score sesuai hasil'
                    else:
                        res['total'] = 0
                        res['notes'] = 'ada feedback, score 0'
                else:
                    if i > 0:
                        res['count'] = 0
                        res['score'] = 0
                        res['max'] = 0
                        res['total'] = output[i-1]['total']
                        res['notes'] = 'tidak ada feedback, csat disamakan dengan hari sebelumnya'
                    else:
                        res['count'] = 0
                        res['score'] = 0
                        res['max'] = 0
                        res['total'] = 0
                        res['notes'] = 'tidak ada feedback, csat 0'

                row_append = res
                row_append.update({"date":res['date'].strftime("%Y-%m-%d")})
                row_append.update({"total": round(res['total'])})

                i = i + 1
                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_group(self, start_date, end_date, types):
        ''' Doc: function get feedback_private csat trend  '''
        try:
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            if types == "tujuan":
                value_condition = [
                    'Mencari data Pemdaprov Jawa Barat untuk kepentingan pelaksanaan program/ kegiatan atau perumusan kebijakan.',
                    'Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu.',
                    'Mempelajari lebih lanjut terkait data yang dimiliki oleh organisasi perangkat daerah di Pemdaprov Jawa Barat.',
                    'Lainnya (tulis secara spesifik).'
                ]
                manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
                select_from_in = "select "+types+", count("+types+") as total from feedback_private "
                select_from_not = "select 'Lainnya' as "+types+", count("+types+") as total from feedback_private "
                condition = "where is_active = true and is_deleted = false " +filters+ " and "+types
                group_by = "group by "+types+""
                sql = text(
                    select_from_in + condition + " in " + "(" +manipulate_array+ ")" +
                    group_by + " union " + select_from_not + condition + " not in " + "(" +manipulate_array+ ")" +
                    " order by total desc;"
                )
            else:
                sql = text(
                    '''select skpd.nama_skpd as organisasi, skpd.nama_skpd_alias as organisasi_alias, count(skpd.nama_skpd) as total
                    from feedback_private
                    join users on feedback_private.user_id = users.id
                    join skpd on skpd.kode_skpd = users.kode_skpd
                    where feedback_private.is_active = true and feedback_private.is_deleted = false ''' + filters + " " +
                    '''GROUP BY skpd.nama_skpd, skpd.nama_skpd_alias
                    ORDER BY total desc;'''
                )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_problem_total(self, start_date, end_date):
        ''' Doc: function get feedback_private total problem '''
        try:
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql1 = text(
                '''select count(user_id)
                from feedback_private
                where is_active = true and is_deleted = false and masalah is not null ''' + filters
            )

            sql2 = text(
                '''select count(id)
                from feedback_private
                where is_active = true and is_deleted = false ''' + filters
            )
            result1 = db.engine.execute(sql1)
            result2 = db.engine.execute(sql2)

            output = {
                "kendala": 0,
                "feedback": 0,
                "persentasi_kendala_feedback": 0
            }
            for row1 in result1:
                output["kendala"] = row1['count']

            for row2 in result2:
                output["feedback"] = row2['count']

            try:
                output["persentasi_kendala_feedback"] = round((output["kendala"] / output["feedback"]) * 100, 2)
            except ZeroDivisionError:
                output["persentasi_kendala_feedback"] = 0

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_feedback_problem_detail(self, start_date, end_date):
        ''' Doc: function get feedback_private csat trend  '''
        try:
            filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            value_condition = [
                'Halaman tidak dapat diakses',
                'Informasi dirasa terlalu membingungkan',
                'Informasi yang dicari tidak dapat ditemukan',
                'Tidak tau jika informasi yang tersedia akurat atau terbaru',
                'Lainnya (tulis secara spesifik)'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select masalah, count(masalah) as total from feedback_private "
            select_from_not = "select 'Lainnya' as masalah, count(masalah) as total from feedback_private "
            condition = "where is_active = true and is_deleted = false and masalah is not null " +filters+ " and masalah"
            group_by = "group by masalah"
            sql = text(
                select_from_in + condition + " in " + "(" +manipulate_array+ ")" +
                group_by + " union " + select_from_not + condition + " not in " + "(" +manipulate_array+ ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
