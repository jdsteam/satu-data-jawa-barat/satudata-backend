''' Doc: controller feedback map  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import FeedbackMapModel
from models import FeedbackMapMasalahModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, func, cast
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
FORMAT_DATE_MORE = "feedback_map.datetime::date >="
FORMAT_DATE_LESS = "feedback_map.datetime::date <="
MESSAGE_DTST_PT_TDK_DKSS = "Dataset/Peta tidak dapat diakses"
MESSAGE_INFMS_TRLL_MMBNGUKN = "Informasi dirasa terlalu membingungkan"
MESSAGE_INFMS_TDK_DTMKN = "Informasi yang dicari tidak dapat ditemukan"
MESSAGE_TDK_INFMS_AKRT = "Tidak tau jika informasi yang tersedia akurat atau terbaru"

# pylint: disable=singleton-comparison, unused-variable


class FeedbackMapController(object):
    ''' Doc: class feedback map  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                FeedbackMapModel.id, FeedbackMapModel.datetime, FeedbackMapModel.score,
                FeedbackMapModel.tujuan, FeedbackMapModel.tujuan_tercapai, FeedbackMapModel.tujuan_mudah_ditemukan, FeedbackMapModel.sektor,
                FeedbackMapModel.saran, FeedbackMapModel.is_active, FeedbackMapModel.is_deleted
            )
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(FeedbackMapModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(FeedbackMapModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(FeedbackMapModel, "datetime"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(FeedbackMapModel, "tujuan"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(FeedbackMapModel, "sektor"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(FeedbackMapModel, "saran"),
                     sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("feedback_map."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            feedback_map = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temp['masalah'] = []
                result_masalah = db.session.query(
                    FeedbackMapMasalahModel.masalah)
                result_masalah = result_masalah.filter(
                    getattr(FeedbackMapMasalahModel, 'feedback_map_id') == temp['id'])
                result_masalah = result_masalah.all()
                for rm_ in result_masalah:
                    rm_ = rm_._asdict()
                    temp['masalah'].append(rm_['masalah'])

                feedback_map.append(temp)

            # check if empty
            feedback_map = list(feedback_map)
            if feedback_map:
                return True, feedback_map
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id': _id}, '')
            result = result.all()

            # change into dict
            feedback_map = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temp['masalah'] = []
                result_masalah = db.session.query(
                    FeedbackMapMasalahModel.masalah)
                result_masalah = result_masalah.filter(
                    getattr(FeedbackMapMasalahModel, 'feedback_map_id') == temp['id'])
                result_masalah = result_masalah.all()
                for rm_ in result_masalah:
                    rm_ = rm_._asdict()
                    temp['masalah'].append(rm_['masalah'])

                feedback_map.append(temp)

            # check if empty
            if feedback_map:
                return True, feedback_map, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = _json
            json_send["datetime"] = HELPER.local_date_server()
            # prepare data model
            result = FeedbackMapModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, feedback_map, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, feedback_map
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = _json

            try:
                # prepare data model
                result = db.session.query(FeedbackMapModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(FeedbackMapModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, feedback_map, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, feedback_map
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(FeedbackMapModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(FeedbackMapModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, feedback_map, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_total(self, start_date, end_date):
        ''' Doc: function get chart total  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMapModel)
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.count()

            if result:
                return True, result
            else:
                return False, 0

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_score(self, start_date, end_date):
        ''' Doc: function get chart score  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMapModel.score, func.count(
                FeedbackMapModel.id).label('count'))
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.group_by(FeedbackMapModel.score)
            result = result.order_by(text("feedback_map.score asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan(self, start_date, end_date):
        ''' Doc: function get chart tujuan  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMapModel.tujuan, func.count(
                FeedbackMapModel.id).label('count'))
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.group_by(FeedbackMapModel.tujuan)
            result = result.order_by(text("feedback_map.tujuan asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan_tercapai(self, start_date, end_date):
        ''' Doc: function get chart tujuan tercapai  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMapModel.tujuan_tercapai, func.count(
                FeedbackMapModel.id).label('count'))
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.group_by(FeedbackMapModel.tujuan_tercapai)
            result = result.order_by(text("feedback_map.tujuan_tercapai asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan_mudah_ditemukan(self, start_date, end_date):
        ''' Doc: function get chart tujuan mudah ditemukan  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMapModel.tujuan_mudah_ditemukan, func.count(
                FeedbackMapModel.id).label('count'))
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.group_by(FeedbackMapModel.tujuan_mudah_ditemukan)
            result = result.order_by(
                text("feedback_map.tujuan_mudah_ditemukan asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_sektor(self, start_date, end_date):
        ''' Doc: function get chart sektor  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMapModel.sektor, func.count(
                FeedbackMapModel.id).label('count'))
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.group_by(FeedbackMapModel.sektor)
            result = result.order_by(text("feedback_map.sektor asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_harian(self, start_date, end_date):
        ''' Doc: function get chart harian  '''
        try:
            # query postgresql
            result = db.session.query(
                func.date_trunc(
                    'day', FeedbackMapModel.datetime).label('date'),
                func.count(FeedbackMapModel.id).label('count')
            )
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(
                getattr(FeedbackMapModel, 'is_active') == True)
            result = result.filter(
                getattr(FeedbackMapModel, 'is_deleted') == False)
            result = result.group_by(func.date_trunc(
                'day', FeedbackMapModel.datetime))
            result = result.order_by(text("date asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                temp.update({"date": temp['date'].strftime("%Y-%m-%d")})
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_download(self, start_date, end_date):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            result = self.query({}, '')
            if start_date:
                result = result.filter(
                    text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(
                    text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.order_by(text("feedback_map.id asc"))
            result = result.all()

            # change into dict
            feedback_map = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temps = {}
                temps['Id'] = temp['id']
                temps['Datetime'] = temp['datetime']
                temps['Score'] = temp['score']
                temps['Tujuan'] = temp['tujuan']
                temps['Tujuan Tercapai'] = temp['tujuan_tercapai']
                temps['Tujuan Mudah Ditemukan'] = temp['tujuan_mudah_ditemukan']
                temps['Sektor'] = temp['sektor']
                temps['Saran'] = temp['saran']
                temps[MESSAGE_DTST_PT_TDK_DKSS] = ''
                temps[MESSAGE_INFMS_TRLL_MMBNGUKN] = ''
                temps[MESSAGE_INFMS_TDK_DTMKN] = ''
                temps[MESSAGE_TDK_INFMS_AKRT] = ''
                temps['Lainnya'] = ''
                result_masalah = db.session.query(
                    FeedbackMapMasalahModel.masalah)
                result_masalah = result_masalah.filter(
                    getattr(FeedbackMapMasalahModel, 'feedback_map_id') == temp['id'])
                result_masalah = result_masalah.all()
                for rm_ in result_masalah:
                    rm_ = rm_._asdict()
                    if rm_['masalah'] == MESSAGE_DTST_PT_TDK_DKSS:
                        temps[MESSAGE_DTST_PT_TDK_DKSS] = 'Ya'
                    elif rm_['masalah'] == MESSAGE_INFMS_TRLL_MMBNGUKN:
                        temps[MESSAGE_INFMS_TRLL_MMBNGUKN] = 'Ya'
                    elif rm_['masalah'] == MESSAGE_INFMS_TDK_DTMKN:
                        temps[MESSAGE_INFMS_TDK_DTMKN] = 'Ya'
                    elif rm_['masalah'] == MESSAGE_TDK_INFMS_AKRT:
                        temps[MESSAGE_TDK_INFMS_AKRT] = 'Ya'
                    else:
                        temps['Lainnya'] = rm_['masalah']
                temps['is_active'] = temp['is_active']
                temps['is_deleted'] = temp['is_deleted']

                feedback_map.append(temps)

            # check if empty
            feedback_map = list(feedback_map)
            if feedback_map:
                return True, feedback_map
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
