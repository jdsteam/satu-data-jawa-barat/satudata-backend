''' Doc: controller history  '''
from settings.configuration import Configuration
from helpers import Helper
from flask import send_file
from exceptions import ErrorMessage
from models import HistoryModel, UserModel
from models import DatasetModel
from models import VisualizationModel
from models import DatasetTypeModel
from models import DatasetClassModel
from models import DatasetTagModel
from models import IndikatorModel
from models import IndikatorCategoryModel
from models import AppModel
from models import AppServiceModel
from models import SektoralModel
from models import RegionalModel
from models import LicenseModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from models import VisualizationAccessModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer, aliased
from sqlalchemy import or_
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
from flask import request
from helpers import decorator

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"

# pylint: disable=singleton-comparison, unused-variable, unused-argument, line-too-long, len-as-condition
class HistoryController(object):
    ''' Doc: class history  '''

    def __init__(self, **kwargs):
        ''' Doc: function history  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            result = db.session.query(HistoryModel)
            for attr, value in where.items():
                result = result.filter(getattr(HistoryModel, attr) == value)
            result = result.filter(or_(cast(getattr(HistoryModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in HistoryModel.__table__.columns))
            result = result.order_by(text("history."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp["datetime"] = temp['datetime'].strftime(FORMAT_DATE)
                history.append(temp)

            # check if empty
            history = list(history)
            if history:
                return True, history
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(HistoryModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(HistoryModel, attr) == value)
            result = result.filter(or_(cast(getattr(HistoryModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in HistoryModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query(self, where: dict, search, start_date, end_date):
        ''' Doc: function query  '''
        try:
            jwt = HELPER.read_jwt()
            owner_skpd = aliased(SkpdModel)
            if where['type'] == 'visualization':
                result = self.query_visualization(where, search, jwt, owner_skpd)
            elif where['type'] == 'indikator':
                result = self.query_indikator(where, search, jwt, owner_skpd)
            else:
                result = self.query_dataset(where, search, jwt, owner_skpd)

            if start_date and end_date:
                result = result.filter(text("history.datetime::date >='" + start_date + "'"))
                result = result.filter(text("history.datetime::date <='" + end_date + "'"))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query_dataset(self, where, search, jwt, owner_skpd):
        ''' Doc: function query dataset '''
        # query postgresql
        result = db.session.query(
            HistoryModel.id, HistoryModel.user_id, HistoryModel.category, HistoryModel.type,
            HistoryModel.type_id, HistoryModel.datetime, DatasetModel.name, UserModel.username,
            SkpdModel.nama_skpd_alias.label('origin_opd'), owner_skpd.nama_skpd_alias.label('owner_opd')
        )
        result = result.join(DatasetModel, DatasetModel.id == HistoryModel.type_id)
        result = result.join(UserModel, HistoryModel.user_id == UserModel.id)
        result = result.join(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
        result = result.join(owner_skpd, DatasetModel.kode_skpd == owner_skpd.kode_skpd)
        if jwt['role']['name'] != 'walidata':
            result = result.filter(DatasetModel.kode_skpd == jwt['kode_skpd'])
        for attr, value in where.items():
            result = result.filter(getattr(HistoryModel, attr) == value)
        result = result.filter(or_(
            cast(getattr(DatasetModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(owner_skpd, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%')
        ))

        return result

    def query_visualization(self, where, search, jwt, owner_skpd):
        ''' Doc: function query visualization '''
        # query postgres
        result = db.session.query(
            HistoryModel.id, HistoryModel.user_id, HistoryModel.category, HistoryModel.type,
            HistoryModel.type_id, HistoryModel.datetime, VisualizationModel.name, UserModel.username,
            SkpdModel.nama_skpd_alias.label('origin_opd'), owner_skpd.nama_skpd_alias.label('owner_opd')
        )
        result = result.join(VisualizationModel, VisualizationModel.id == HistoryModel.type_id)
        result = result.join(UserModel, HistoryModel.user_id == UserModel.id)
        result = result.join(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
        result = result.join(owner_skpd, VisualizationModel.kode_skpd == owner_skpd.kode_skpd)
        if jwt['role']['name'] != 'walidata':
            result = result.filter(VisualizationModel.kode_skpd == jwt['kode_skpd'])
        for attr, value in where.items():
            result = result.filter(getattr(HistoryModel, attr) == value)
        result = result.filter(or_(
            cast(getattr(VisualizationModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(owner_skpd, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%')
        ))

        return result

    def query_indikator(self, where, search, jwt, owner_skpd):
        ''' Doc: function query indikator '''
        # query postgresql
        result = db.session.query(
            HistoryModel.id, HistoryModel.user_id, HistoryModel.category, HistoryModel.type,
            HistoryModel.type_id, HistoryModel.datetime, IndikatorModel.name, UserModel.username,
            SkpdModel.nama_skpd_alias.label('origin_opd'), owner_skpd.nama_skpd_alias.label('owner_opd')
        )
        result = result.join(IndikatorModel, IndikatorModel.id == HistoryModel.type_id)
        result = result.join(UserModel, HistoryModel.user_id == UserModel.id)
        result = result.join(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
        result = result.join(owner_skpd, IndikatorModel.kode_skpd == owner_skpd.kode_skpd)
        if jwt['role']['name'] != 'walidata':
            result = result.filter(IndikatorModel.kode_skpd == jwt['kode_skpd'])
        for attr, value in where.items():
            result = result.filter(getattr(HistoryModel, attr) == value)
        result = result.filter(or_(
            cast(getattr(IndikatorModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
            cast(getattr(owner_skpd, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%')
        ))

        return result

    def get_activity(self, where: dict, search, sort, limit, skip, start_date, end_date):
        ''' Doc: function get activity  '''
        try:
            result = self.query(where, search, start_date, end_date)
            result = result.order_by(text("history."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime(FORMAT_DATE)})
                history.append(temp)

            # check if empty
            history = list(history)
            if history:
                return True, history
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_activity_count(self, where: dict, search, start_date, end_date):
        ''' Doc: function get activity count  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_distinct(self, where: dict, where_dataset: dict, search, sort, limit, skip):
        ''' Doc: function get distinct  '''
        try:
            jwt = HELPER.read_jwt()

            # query postgresql
            result = db.session.query(func.distinct(func.DATE(HistoryModel.datetime)).label('datetime'))
            for attr, value in where.items():
                result = result.filter(getattr(HistoryModel, attr) == value)
            if sort[0] == 'datetime':
                result = result.order_by(text(sort[0]+" "+sort[1]))
            else:
                result = result.order_by(text("datetime"+" "+"desc"))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history = []
            for res in result:
                temp = {}
                temp['datetime'] = str(res[0])
                temp['detail'] = []

                # get relation to detail_history
                detail_history_array = self.populate_history_array(res, where, where_dataset, search, sort, jwt)
                temp['detail'] = detail_history_array

                history.append(temp)

            # check if empty
            history = list(history)

            history = self.populate_remove_unused(where, history)

            if history:
                return True, history
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_history_array(self, res, where, where_dataset, search, sort, jwt):
        ''' Doc: function populate_history_array  '''
        detail_history = db.session.query(HistoryModel)
        detail_history = detail_history.filter(func.DATE(HistoryModel.datetime) == str(res[0]))
        for attr, value in where.items():
            detail_history = detail_history.filter(getattr(HistoryModel, attr) == value)
        if sort[0] == 'datetime':
            detail_history = detail_history.order_by(text(sort[0]+" "+sort[1]))
        else:
            detail_history = detail_history.order_by(text("datetime"+" "+"desc"))
        detail_history = detail_history.all()

        detail_history_array = []
        for dh_ in detail_history:
            dh_ = dh_.__dict__
            dh_.pop('_sa_instance_state', None)
            dh_.update({'datetime':dh_['datetime'].strftime(FORMAT_DATE)})
            detail_history_array.append(dh_)

            if dh_['type'].lower() == 'dataset':
                dh_ = self.populate_dataset(dh_, where_dataset, search)
            elif dh_['type'].lower() == 'visualization':
                dh_ = self.populate_visualization(dh_, where_dataset, search, jwt)
            elif dh_['type'].lower() == 'indikator':
                dh_ = self.populate_indikator(dh_, where_dataset, search)

        return detail_history_array

    def get_distinct_count(self, where: dict, where_dataset: dict, search, sort, limit, skip):
        ''' Doc: function get distinct count  '''
        try:
            # query postgresql
            result = db.session.query(func.distinct(func.DATE(HistoryModel.datetime)).label('datetime'))
            result = result.join(DatasetModel, DatasetModel.id == HistoryModel.type_id)
            for attr, value in where.items():
                result = result.filter(getattr(HistoryModel, attr) == value)
            result = result.filter(getattr(DatasetModel, 'is_deleted') == False)
            result = result.filter(or_(cast(getattr(DatasetModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in DatasetModel.__table__.columns))
            if sort[0] == 'datetime':
                result = result.order_by(text(sort[0]+" "+sort[1]))
            else:
                result = result.order_by(text("datetime"+" "+"desc"))
            result = result.count()

            # change into dict
            history = {}
            history['count'] = result

            # check if empty
            if history:
                return True, history
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(HistoryModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}

            # change into dict
            history = result

            # check if empty
            if history:
                return True, history
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["datetime"] = HELPER.local_date_server()

            # prepare data model
            result = HistoryModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, history
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(HistoryModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_dataset(self, dh_, where_dataset, search):
        ''' Doc: function populate dataset  '''
        # get relation to user
        result = db.session.query(DatasetModel)
        result = result.filter(getattr(DatasetModel, 'id') == dh_['type_id'])
        for attr, value in where_dataset.items():
            result = result.filter(getattr(DatasetModel, attr) == value)
        result = result.filter(or_(cast(getattr(DatasetModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in DatasetModel.__table__.columns))
        result = result.all()

        if result:
            result = result[0].__dict__
            result.pop('_sa_instance_state', None)
            result.pop('period', None)
            result['cdate'] = result['cdate'].strftime(FORMAT_DATE)
            result['mdate'] = result['mdate'].strftime(FORMAT_DATE)

            result = self.populate_regional(result)
            result = self.populate_dataset_type(result)
            result = self.populate_dataset_class(result)

            # get relation to app
            # app = self.populate_app(result)
            # result['app'] = app
            # result.pop('app_id', None)

            # get relation to app_service
            # app_service = self.populate_app_service(result)
            # result['app_service'] = app_service
            # result.pop('app_service_id', None)

            # get can_access_dataset
            # result['can_access'], result['can_access_info'] = HELPER.can_access_dataset(result['app'], result['app_service'])

            # get can_access_dataset
            token = request.headers.get('Authorization', None)
            if result['schema'] and result['table']:
                result['can_access_new'], result['can_access_new_info'] = decorator.check_bigdata('bigdata', result['schema'], result['table'], token)
            else:
                result['can_access_new'] = False
                result['can_access_new_info'] = {'data': {}, 'error': 1, 'message': 'Schema and Table not found.'}

            # get relation to dataset_tag
            dataset_tag = db.session.query(func.distinct(func.lower(DatasetTagModel.tag)).label('tag'))
            dataset_tag = dataset_tag.filter(getattr(DatasetTagModel, "dataset_id") == result['id'])
            dataset_tag = dataset_tag.all()

            dataset_tags = []
            dataset_tagss = []
            for tag in dataset_tag:
                temp_tag = {}
                temp_tag['tag'] = tag[0]
                dataset_tags.append(tag[0])
                dataset_tagss.append(temp_tag)

            result['dataset_tag'] = dataset_tags
            result['dataset_tags'] = dataset_tagss

            result = self.populate_sektoral(result)
            result = self.populate_lisence(result)
            result = self.populate_skpd(result)
            result = self.populate_skpdsub(result)
            result = self.populate_skpdunit(result)

            dh_['dataset'] = result
        else:
            dh_['dataset'] = {}

        return dh_

    # def populate_app(self, result):
    #     ''' Doc: function populate_app  '''
    #     app = db.session.query(AppModel)
    #     app = app.options(
    #         defer("cuid"),
    #         defer("cdate"))
    #     app = app.get(result['app_id'])

    #     if app:
    #         app = app.__dict__
    #         app.pop('_sa_instance_state', None)
    #     else:
    #         app = {}

    #     return app

    # def populate_app_service(self, result):
    #     ''' Doc: function populate_app_service  '''
    #     app_service = db.session.query(AppServiceModel)
    #     app_service = app_service.options(
    #         defer("cuid"),
    #         defer("cdate"))
    #     app_service = app_service.get(result['app_service_id'])

    #     if app_service:
    #         app_service = app_service.__dict__
    #         app_service.pop('_sa_instance_state', None)

    #         if app_service['dataset_class_id']:
    #             # get relation to skpd
    #             dataset_class = db.session.query(DatasetClassModel)
    #             dataset_class = dataset_class.get(app_service['dataset_class_id'])

    #             if dataset_class:
    #                 dataset_class = dataset_class.__dict__
    #                 dataset_class.pop('_sa_instance_state', None)
    #             else:
    #                 dataset_class = {}

    #             app_service['dataset_class'] = dataset_class

    #         else:
    #             app_service['dataset_class'] = {}
    #     else:
    #         app_service = {}

    #     return app_service

    def populate_visualization(self, dh_, where_dataset, search, jwt):
        ''' Doc: function populate visualization  '''
        # get relation to user
        result = db.session.query(VisualizationModel)
        result = result.filter(getattr(VisualizationModel, 'id') == dh_['type_id'])
        for attr, value in where_dataset.items():
            result = result.filter(getattr(VisualizationModel, attr) == value)
        result = result.filter(or_(cast(getattr(VisualizationModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in VisualizationModel.__table__.columns))
        result = result.all()

        if result:
            result = result[0].__dict__
            result.pop('_sa_instance_state', None)
            result['cdate'] = result['cdate'].strftime(FORMAT_DATE)
            result['mdate'] = result['mdate'].strftime(FORMAT_DATE)

            result = self.populate_regional(result)
            result = self.populate_dataset_class(result)

            # get relation to visualization access
            visualization_access = db.session.query(VisualizationAccessModel)
            visualization_access = visualization_access.distinct(VisualizationAccessModel.user_id)
            visualization_access = visualization_access.options(
                defer("cuid"), defer("cdate"), defer("enable"),
                defer("visualization_id")
            )
            visualization_access = visualization_access.filter(VisualizationAccessModel.visualization_id == result['id'])
            visualization_access = visualization_access.filter(VisualizationAccessModel.user_id == jwt['id'])
            visualization_access = visualization_access.all()

            if int(result['dataset_class']['id']) == 3 or int(result['dataset_class']['id']) == 5:
                result['can_access'] = True
            else:
                if visualization_access:
                    result['can_access'] = True
                else:
                    result['can_access'] = False

            result = self.populate_sektoral(result)
            result = self.populate_lisence(result)
            result = self.populate_skpd(result)
            result = self.populate_skpdsub(result)
            result = self.populate_skpdunit(result)

            dh_['visualization'] = result
        else:
            dh_['visualization'] = {}

        return dh_

    def populate_indikator(self, dh_, where_dataset, search):
        ''' Doc: function populate indikator  '''
        # get relation to user
        result = db.session.query(IndikatorModel)
        result = result.filter(getattr(IndikatorModel, 'id') == dh_['type_id'])
        for attr, value in where_dataset.items():
            result = result.filter(getattr(IndikatorModel, attr) == value)
        result = result.filter(or_(cast(getattr(IndikatorModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in DatasetModel.__table__.columns))
        result = result.all()

        if result:
            result = result[0].__dict__
            result.pop('_sa_instance_state', None)
            result['cdate'] = result['cdate'].strftime(FORMAT_DATE)
            result['mdate'] = result['mdate'].strftime(FORMAT_DATE)

            # get relation to indikator_category
            indikator_category = db.session.query(IndikatorCategoryModel)
            indikator_category = indikator_category.get(result['indikator_category_id'])

            if indikator_category:
                indikator_category = indikator_category.__dict__
                indikator_category.pop('_sa_instance_state', None)
            else:
                indikator_category = {}

            result['indikator_category'] = indikator_category
            result.pop('indikator_category_id', None)

            result = self.populate_dataset_type(result)
            result = self.populate_skpd(result)
            result = self.populate_skpdsub(result)
            result = self.populate_skpdunit(result)

            dh_['indikator'] = result
        else:
            dh_['indikator'] = {}

        return dh_

    def populate_skpd(self, result):
        ''' Doc: function populate skpd '''
        if result['kode_skpd']:
            # get relation to skpd
            skpd = db.session.query(SkpdModel)
            skpd = skpd.options(
                defer("cuid"),
                defer("cdate"))
            skpd = skpd.filter(getattr(SkpdModel, "kode_skpd") == result['kode_skpd'])
            skpd = skpd.one()

            if skpd:
                skpd = skpd.__dict__
                skpd.pop('_sa_instance_state', None)
            else:
                skpd = {}

            result['skpd'] = skpd
        else:
            result['skpd'] = {}
            result.pop('kode_skpd', None)

        return result

    def populate_skpdsub(self, result):
        ''' Doc: function populate skpdsub '''
        if result['kode_skpdsub']:
            # get relation to skpdsub
            skpdsub = db.session.query(SkpdSubModel)
            skpdsub = skpdsub.options(
                defer("cuid"),
                defer("cdate"))
            skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpdsub") == result['kode_skpdsub'])
            skpdsub = skpdsub.one()

            if skpdsub:
                skpdsub = skpdsub.__dict__
                skpdsub.pop('_sa_instance_state', None)
            else:
                skpdsub = {}

            result['skpdsub'] = skpdsub
        else:
            result['skpdsub'] = {}
            result.pop('kode_skpdsub', None)

        return result

    def populate_skpdunit(self, result):
        ''' Doc: function populate skpdunit '''
        if result['kode_skpdunit']:
            # get relation to skpdunit
            skpdunit = db.session.query(SkpdUnitModel)
            skpdunit = skpdunit.options(
                defer("cuid"),
                defer("cdate"))
            skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdunit") == result['kode_skpdunit'])
            skpdunit = skpdunit.one()

            if skpdunit:
                skpdunit = skpdunit.__dict__
                skpdunit.pop('_sa_instance_state', None)
            else:
                skpdunit = {}

            result['skpdunit'] = skpdunit
        else:
            result['skpdunit'] = {}
            result.pop('kode_skpdunit', None)

        return result

    def populate_dataset_class(self, result):
        ''' Doc: function populate dataset_class '''
        # get relation to dataset_class
        dataset_class = db.session.query(DatasetClassModel)
        dataset_class = dataset_class.get(result['dataset_class_id'])

        if dataset_class:
            dataset_class = dataset_class.__dict__
            dataset_class.pop('_sa_instance_state', None)
        else:
            dataset_class = {}

        result['dataset_class'] = dataset_class
        result.pop('dataset_class_id', None)

        return result

    def populate_sektoral(self, result):
        ''' Doc: function populate sektoral '''
        # get relation to sektoral
        sektoral = db.session.query(SektoralModel)
        sektoral = sektoral.options(
            defer("cuid"),
            defer("cdate"))
        sektoral = sektoral.get(result['sektoral_id'])

        if sektoral:
            sektoral = sektoral.__dict__
            sektoral.pop('_sa_instance_state', None)
        else:
            sektoral = {}

        result['sektoral'] = sektoral
        result.pop('sektoral_id', None)

        return result

    def populate_lisence(self, result):
        ''' Doc: function populate lisence '''
        if result['license_id']:
            # get relation to license
            licenses = db.session.query(LicenseModel)
            licenses = licenses.get(result['license_id'])

            if licenses:
                licenses = licenses.__dict__
                licenses.pop('_sa_instance_state', None)
            else:
                licenses = {}

            result['license'] = licenses
            result.pop('license_id', None)
        else:
            result['license'] = {}
            result.pop('license_id', None)

        return result

    def populate_dataset_type(self, result):
        ''' Doc: function populate dataset_type '''
        # get relation to dataset_type
        dataset_type = db.session.query(DatasetTypeModel)
        dataset_type = dataset_type.get(result['dataset_type_id'])

        if dataset_type:
            dataset_type = dataset_type.__dict__
            dataset_type.pop('_sa_instance_state', None)
        else:
            dataset_type = {}

        result['dataset_type'] = dataset_type
        result.pop('dataset_type_id', None)

        return result

    def populate_regional(self, result):
        ''' Doc: function populate regional '''
        # get relation to regional
        regional = db.session.query(RegionalModel)
        regional = regional.options(
            defer("cuid"),
            defer("cdate"))
        regional = regional.get(result['regional_id'])

        if regional:
            regional = regional.__dict__
            regional.pop('_sa_instance_state', None)
        else:
            regional = {}

        result['regional'] = regional
        result.pop('regional_id', None)

        return result

    def populate_remove_unused(self, where, history):
        ''' Doc: function populate remove unused '''
        for i in range(len(history)-1, -1, -1):
            for j in range(len(history[i]['detail'])-1, -1, -1):
                if len(history[i]['detail'][j][where['type']]) == 0:
                    history[i]['detail'].pop(j)

        for k in range(len(history)-1, -1, -1):
            if len(history[k]['detail']) == 0:
                history.pop(k)

        return history

    def download(self, download, where, search, start_date, end_date, sort, skip):
        ''' Doc: function download '''
        if download and download.lower() == 'xls':
            res_count, history_count = self.get_activity_count(where, search, start_date, end_date)
            res, history = self.get_activity(where, search, sort, history_count['count'], skip, start_date, end_date)
            output_xls = []
            number = 0
            for row in history:
                number += 1
                row['No'] = number
                row['Waktu'] = row['datetime']
                row['Judul'] = row['name']
                row['Username'] = row['username']
                row['Asal OPD'] = row['origin_opd']
                row['OPD Pemilik'] = row['owner_opd']
                del row['id'], row['user_id'], row['category'], row['type'], row['type_id'], row['datetime']
                del row['name'], row['username'], row['origin_opd'], row['owner_opd']
                output_xls.append(row)

            columns_title = ['No', 'Waktu', 'Judul', 'Username', 'Asal OPD', 'OPD Pemilik']
            filename = HELPER.convert_xls('static/download/history-activity.xlsx', output_xls, columns_title)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='history-activity.xlsx',
                as_attachment=True
            )
