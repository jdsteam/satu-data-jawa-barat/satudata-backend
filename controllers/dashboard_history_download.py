''' Doc: controller dashboard  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
import sentry_sdk
import math
from datetime import timedelta
from dateutil import parser

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison


class DashboardHistoryDownloadController(object):
    ''' Doc: constructor dashboard  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def filter_type(self, start_date, end_date):
        ''' Doc: function filter date type  '''
        try:
            start = parser.parse(start_date)
            end = parser.parse(end_date)
            diff = end - start
            distance_day = (diff.days + 1)

            parse_growth_end = parser.parse(start_date)
            parser_end_date = parse_growth_end - timedelta(days=1)
            end_date_growth = parser_end_date.strftime("%Y-%m-%d")

            growth_date = start - timedelta(days=distance_day)
            total_days = distance_day

            start_date_growth = growth_date.strftime("%Y-%m-%d")

            return start_date_growth, total_days, end_date_growth
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            return err, False

    def get_total(self, start_date, end_date):
        ''' Doc: function get history_download total  '''
        try:
            start_date_last, distance_day, end_date_last = self.filter_type(
                start_date, end_date)

            sql_total = text(
                "select count(id) as total from history_download " +
                "where datetime::date >='"+start_date +
                "' and datetime::date <='"+end_date+"'"
            )
            result_total = db.engine.execute(sql_total)
            data_total = [row for row in result_total]

            sql_total_last = text(
                "select count(id) as growth from history_download " +
                "where datetime::date >='"+start_date_last +
                "' and datetime::date <='"+end_date_last+"'"
            )
            result_total_last = db.engine.execute(sql_total_last)
            data_total_last = [row for row in result_total_last]

            try:
                data_growth = data_total[0][0] - data_total_last[0][0]
                percen_growth = round(
                    ((data_growth / data_total_last[0][0]) * 100), 2)
            except ZeroDivisionError:
                percen_growth = 0

            # check if empty
            data_dashboard = {
                "total": data_total[0][0],
                "start_date": start_date,
                "end_date": end_date,
                "total_last_periode": data_total_last[0][0],
                "last_periode_start_date": start_date_last,
                "last_periode_end_date": end_date_last,
                "total_growth": data_growth,
                "percent_growth": percen_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if data_dashboard:
                return True, data_dashboard
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_average(self, start_date, end_date):
        ''' Doc: function get history_download average  '''
        try:
            start_date_last, distance_day, end_date_last = self.filter_type(
                start_date, end_date)

            sql_total = text(
                "select count(id) as total from history_download " +
                "where datetime::date >='"+start_date +
                "' and datetime::date <='"+end_date+"'"
            )
            result_total = db.engine.execute(sql_total)
            data_total = [row for row in result_total]

            sql_total_last = text(
                "select count(id) as growth from history_download " +
                "where datetime::date >='"+start_date_last +
                "' and datetime::date <='"+end_date_last+"'"
            )
            result_total_last = db.engine.execute(sql_total_last)
            data_total_last = [row for row in result_total_last]

            try:
                data_growth = round((data_total[0][0] / distance_day) -
                                    (data_total_last[0][0] / distance_day), 2)
                percen_growth = round(
                    (data_growth / (data_total_last[0][0] / distance_day)) * 100, 2)
            except ZeroDivisionError:
                percen_growth = 0

            # check if empty
            data_dashboard = {
                "average": round(data_total[0][0] / distance_day, 2),
                "start_date": start_date,
                "end_date": end_date,
                "average_last_periode": round(data_total_last[0][0] / distance_day, 2),
                "last_periode_start_date": start_date_last,
                "last_periode_end_date": end_date_last,
                "average_growth": data_growth,
                "percent_growth": percen_growth,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if data_dashboard:
                return True, data_dashboard
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_trend(self, start_date, end_date, group):
        ''' Doc: function get history_download trend  '''
        try:
            # query default
            sql_day = '''
            SELECT series.date, COALESCE(history_download.count, (0)::bigint) AS total
            FROM history_download_series_view series
            LEFT JOIN history_download_count_view history_download ON series.date = history_download.datetime
            '''
            filters = " WHERE date >='"+start_date+"' and date <='"+end_date+"' "

            # check group
            if group == 'day':
                sql = sql_day + filters
            elif group == 'week':
                sql = '''
                SELECT (date_trunc('week'::text, (history_download_harian_view.date)::timestamp with time zone))::date AS date,
                sum(history_download_harian_view.total) AS total
                FROM
                ( ''' + sql_day + ''') as history_download_harian_view ''' + filters + '''
                GROUP BY ((date_trunc('week'::text, (history_download_harian_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('week'::text, (history_download_harian_view.date)::timestamp with time zone))::date)
                '''
            elif group == 'month':
                sql = '''
                SELECT (date_trunc('month'::text, (history_download_harian_view.date)::timestamp with time zone))::date AS date,
                sum(history_download_harian_view.total) AS total
                FROM
                ( ''' + sql_day + ''') as history_download_harian_view ''' + filters + '''
                GROUP BY ((date_trunc('month'::text, (history_download_harian_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('month'::text, (history_download_harian_view.date)::timestamp with time zone))::date)
                '''
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                row_append.update({"date": res['date'].strftime("%Y-%m-%d")})
                row_append.update({"total": round(res['total'])})

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_tujuan(self, start_date, end_date):
        ''' Doc: function get history_download tujuan  '''
        try:
            value_condition = [
                'Referensi Kajian Bisnis',
                'Referensi Pembuatan Kebijakan',
                'Referensi Pembuatan Kurikulum/ Bahan Ajar',
                'Referensi Tugas/ Karya Ilmiah',
                'Referensi Pribadi'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select tujuan, count(tujuan) as total from history_download "
            select_from_not = "select 'Lainnya' as tujuan, count(tujuan) as total from history_download "
            condition = "where datetime::date >= '"+start_date + \
                "' and datetime::date <= '"+end_date+"' and tujuan "
            group_by = " group by tujuan "
            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" +
                group_by + " union " + select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_pekerjaan(self, start_date, end_date):
        ''' Doc: function get history_download pekerjaan  '''
        try:
            value_condition = [
                'Pelajar/ Mahasiswa/ Akademisi',
                'Profesional',
                'Wirausaha',
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select pekerjaan, count(pekerjaan) as total from history_download "
            select_from_not = "select 'Lainnya' as pekerjaan, count(pekerjaan) as total from history_download "
            condition = "where datetime::date >= '"+start_date + \
                "' and datetime::date <= '"+end_date+"' and pekerjaan "
            group_by = " group by pekerjaan "
            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" +
                group_by + " union " + select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_bidang_ilmu(self, start_date, end_date):
        ''' Doc: function get history_download bidang_ilmu  '''
        try:
            value_condition = [
                'Agama dan Filsafat',
                'Bahasa, Sosial, dan Humaniora',
                'Ekonomi',
                'Hewani',
                'Kedokteran dan Kesehatan',
                'Matematika dan Pengetahuan Alam',
                'Pendidikan',
                'Seni, Desain, dan Media',
                'Tanaman',
                'Teknik'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select bidang, count(bidang) as total from history_download "
            select_from_not = "select 'Lainnya' as bidang, count(bidang) as total from history_download "
            condition = "where pekerjaan = 'Pelajar/ Mahasiswa/ Akademisi' " \
                "and datetime::date >= '"+start_date + "'" \
                "and datetime::date <= '"+end_date + "' and bidang "
            group_by = " group by bidang "
            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" +
                group_by + " union " + select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_bidang_usaha(self, start_date, end_date):
        ''' Doc: function get history_download bidang_ilmu  '''
        try:
            value_condition = [
                'Pertanian (Agrikultur)',
                'Pertambangan',
                'Pabrikasi (Manufaktur)',
                'Kontruksi',
                'Perdagangan',
                'Jasa Keuangan',
                'Jasa Perorangan',
                'Jasa Umum',
                'Jasa Wisata'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select bidang, count(bidang) as total from history_download "
            select_from_not = "select 'Lainnya' as bidang, count(bidang) as total from history_download "
            condition = "where pekerjaan != 'Pelajar/ Mahasiswa/ Akademisi' " \
                "and datetime::date >= '"+start_date + "'" \
                "and datetime::date <= '"+end_date + "' and bidang "
            group_by = " group by bidang "
            sql = text(
                select_from_in + condition + " in " + "(" + manipulate_array + ")" +
                group_by + " union " + select_from_not + condition + " not in " + "(" + manipulate_array + ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)
            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_top_download(self, start_date, end_date):
        ''' Doc: function get history_download top_download  '''
        try:

            # dataset
            query_dataset = text(
                "SELECT dataset.id, dataset.name, count(history_download.id) " +
                "FROM history_download " +
                "JOIN dataset on history_download.category_id = dataset.id " +
                "WHERE datetime::date >='" + start_date +
                "' AND datetime::date <='"+end_date+"'"
                "GROUP BY dataset.id, dataset.name " +
                "ORDER BY count desc " +
                "LIMIT 10"
            )
            result_dataset = db.engine.execute(query_dataset)

            # sektoral
            query_sektoral = text(
                "SELECT sektoral.id, sektoral.name, count(history_download.id) " +
                "FROM history_download " +
                "JOIN dataset on history_download.category_id = dataset.id " +
                "JOIN sektoral on dataset.sektoral_id = sektoral.id " +
                "WHERE datetime::date >='" + start_date +
                "' AND datetime::date <='"+end_date+"'"
                "GROUP BY sektoral.id, sektoral.name " +
                "ORDER BY count desc " +
                "LIMIT 10"
            )
            result_sektoral = db.engine.execute(query_sektoral)

            # skpd
            query_skpd = text(
                "SELECT skpd.kode_skpd, skpd.nama_skpd, count(history_download.id)" +
                "FROM history_download " +
                "JOIN dataset on history_download.category_id = dataset.id " +
                "JOIN skpd on dataset.kode_skpd = skpd.kode_skpd " +
                "WHERE datetime::date >='" + start_date +
                "' AND datetime::date <='"+end_date+"'"
                "GROUP BY skpd.kode_skpd, skpd.nama_skpd " +
                "ORDER BY count desc " +
                "LIMIT 10"
            )
            result_skpd = db.engine.execute(query_skpd)

            # variable
            output = {}
            output['top_dataset'] = []
            output['top_sektoral'] = []
            output['top_skpd'] = []

            # maping object
            for res in [dict(row) for row in result_dataset]:
                row_append = res
                output['top_dataset'].append(row_append)
            for res in [dict(row) for row in result_sektoral]:
                row_append = res
                output['top_sektoral'].append(row_append)
            for res in [dict(row) for row in result_skpd]:
                row_append = res
                output['top_skpd'].append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
