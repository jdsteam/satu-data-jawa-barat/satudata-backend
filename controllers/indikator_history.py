''' Doc: controller indikator history  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import IndikatorHistoryModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=singleton-comparison, unused-variable, unused-argument
class IndikatorHistoryController(object):
    ''' Doc: class indikator history  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(IndikatorHistoryModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(IndikatorHistoryModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(IndikatorHistoryModel, attr) == value)

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("indikator_history_class."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            indikator_history = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                indikator_history.append(temp)

            # check if empty
            if indikator_history:
                return True, indikator_history
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(IndikatorHistoryModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}
            # check if empty
            if result:
                return True, result, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all_by_id(self, _id):
        ''' Doc: function get all by id  '''
        try:
            # query postgresql
            result = db.session.query(IndikatorHistoryModel)
            result = result.filter(IndikatorHistoryModel.indikator_id == _id)
            result = result.all()

            # change into dict
            indikator_history = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                indikator_history.append(temp)
            # check if empty

            if indikator_history:
                return indikator_history
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, json, condition):
        ''' Doc: function init  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # prepare data model
            param = self.post_param(json, 'add')
            result = IndikatorHistoryModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            # generate json data
            output = []
            for res in json:
                output.append(res)

            db.session.execute(
                IndikatorHistoryModel.__table__.insert().values(output)
            )
            db.session.commit()
            history_class = self.get_all_by_id(output[0]['indikator_id'])
            # check if exist

            if history_class:
                return True, history_class
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(IndikatorHistoryModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorHistoryModel, attr) == value)
                result = result.delete()

                # execute database
                db.session.commit()

                return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
