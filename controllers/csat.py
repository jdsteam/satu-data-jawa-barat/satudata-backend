''' Doc: controller csat '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import CsatModel
from models import DatasetModel
from models import InfographicModel
from models import VisualizationModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, func, cast
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
FORMAT_DATE_MORE = "csat.datetime::date >= "
FORMAT_DATE_LESS = "csat.datetime::date <= "

# pylint: disable=singleton-comparison, unused-variable, unused-argument


class CsatController(object):
    ''' Doc: class csat  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search, start_date, end_date):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                CsatModel.id, CsatModel.datetime,
                CsatModel.type, CsatModel.email, CsatModel.category, CsatModel.category_id,
                CsatModel.tujuan, CsatModel.sektor, CsatModel.data_ditemukan, CsatModel.data_masalah,
                CsatModel.mudah_didapatkan, CsatModel.saran, CsatModel.score, CsatModel.notes,
                CsatModel.is_active, CsatModel.is_deleted, DatasetModel.name.label('category_name')
            )
            result = result.join(DatasetModel, DatasetModel.id == CsatModel.category_id)
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    if attr == 'category_name':
                        result = result.filter(
                            or_(getattr(DatasetModel, 'name') == val for val in value))
                    else:
                        result = result.filter(
                            or_(getattr(CsatModel, attr) == val for val in value))
                else:
                    if attr == 'category_name':
                        result = result.filter(
                            getattr(DatasetModel, 'name') == value)
                    else:
                        result = result.filter(
                            getattr(CsatModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(CsatModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(CsatModel, "type"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(CsatModel, "email"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(CsatModel, "tujuan"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(CsatModel, "sektor"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(CsatModel, "data_masalah"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(CsatModel, "saran"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(DatasetModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip, start_date, end_date):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            csat = []
            for res in result:
                temp = res._asdict()
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                # temp['category_name'] = ''
                # if temp['category'] == 'dataset':
                #     temp = self.populate_dataset(temp)

                # elif temp['category'] == 'infographic':
                #     temp = self.populate_infographic(temp)

                # elif temp['category'] == 'visualization':
                #     temp = self.populate_visualization(temp)

                csat.append(temp)

            # check if empty
            csat = list(csat)
            if csat:
                return True, csat
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_dataset(self, temp):
        ''' Doc: function populate_dataset  '''
        result_category = db.session.query(DatasetModel.name)
        result_category = result_category.filter(getattr(DatasetModel, 'id') == temp['category_id'])
        result_category = result_category.all()
        for rc_ in result_category:
            temp['category_name'] = rc_[0]

        return temp

    def populate_infographic(self, temp):
        ''' Doc: function populate_infographic  '''
        result_category = db.session.query(InfographicModel.name)
        result_category = result_category.filter(getattr(InfographicModel, 'id') == temp['category_id'])
        result_category = result_category.all()
        for rc_ in result_category:
            temp['category_name'] = rc_[0]

        return temp

    def populate_visualization(self, temp):
        ''' Doc: function populate_visualization  '''
        result_category = db.session.query(VisualizationModel.name)
        result_category = result_category.filter(getattr(VisualizationModel, 'id') == temp['category_id'])
        result_category = result_category.all()
        for rc_ in result_category:
            temp['category_name'] = rc_[0]

        return temp

    def get_count(self, where: dict, search, start_date, end_date):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search, start_date, end_date)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id': _id}, '', '', '')
            result = result.all()

            # change into dict
            csat = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temp['category_name'] = ''
                if temp['category'] == 'dataset':
                    temp = self.populate_dataset(temp)

                elif temp['category'] == 'infographic':
                    temp = self.populate_infographic(temp)

                elif temp['category'] == 'visualization':
                    temp = self.populate_visualization(temp)

                csat.append(temp)

            # check if empty
            if csat:
                return True, csat, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_search_email(self, email):
        ''' Doc: function get search email  '''
        try:
            # execute database
            result = db.session.query(
                func.distinct(CsatModel.email).label('email'),
                CsatModel.tujuan, CsatModel.sektor
            )
            result = result.filter(cast(
                getattr(CsatModel, "email"), sqlalchemy.String).ilike('%'+email+'%'))
            print(result)
            result = result.all()

            # change into dict
            csat = []
            for res in result:
                temp = res._asdict()
                print(temp)
                csat.append(temp)

            # check if empty
            if csat:
                return True, csat, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["datetime"] = HELPER.local_date_server()
            # prepare data model
            result = CsatModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, csat, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, csat
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(CsatModel)
                for attr, value in where.items():
                    result = result.filter(getattr(CsatModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, csat, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, csat
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(CsatModel)
                for attr, value in where.items():
                    result = result.filter(getattr(CsatModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, csat, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_download(self, start_date, end_date):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            result = self.query({}, "", start_date, end_date)
            result = result.order_by(text("csat.datetime asc"))
            result = result.all()

            # change into dict
            csat = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime": temp['datetime'].strftime(FORMAT_DATE)})
                csat.append(temp)

            # check if empty
            csat = list(csat)
            if csat:
                return True, csat
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
