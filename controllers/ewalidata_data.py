''' Doc: controller ewalidata_data  '''
import json
import os

import requests
from urllib3 import Retry
from requests.adapters import HTTPAdapter
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import EwalidataDataModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class EwalidataDataController(object):
    ''' Doc: class ewalidata_data  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            jwt = HELPER.read_jwt()
            # query postgresql
            result = db.session.query(EwalidataDataModel)
            for attr, value in where.items():
                result = result.filter(getattr(EwalidataDataModel, attr) == value)
            result = result.filter(or_(cast(getattr(EwalidataDataModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in EwalidataDataModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            ewalidata_data = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp['cdate'] = temp['cdate'].strftime(DATETIME_FORMAT)
                ewalidata_data.append(temp)

            # check if empty
            ewalidata_data = list(ewalidata_data)
            if ewalidata_data:
                return True, ewalidata_data
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            jwt = HELPER.read_jwt()
            # query postgresql
            result = db.session.query(EwalidataDataModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(EwalidataDataModel, attr) == value)
            result = result.filter(or_(cast(getattr(EwalidataDataModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in EwalidataDataModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(EwalidataDataModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                result['cdate'] = result['cdate'].strftime(DATETIME_FORMAT)
            else:
                result = {}

            # change into dict
            ewalidata_data = result

            # check if empty
            if ewalidata_data:
                return True, ewalidata_data
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = EwalidataDataModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, ewalidata_data = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, ewalidata_data
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {}, err)

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(EwalidataDataModel)
                for attr, value in where.items():
                    result = result.filter(getattr(EwalidataDataModel, attr) == value)

                # execute database
                result.update(json_send, synchronize_session='fetch')
                db.session.commit()

                # get last data
                result = db.session.query(EwalidataDataModel)
                for attr, value in where.items():
                    result = result.filter(getattr(EwalidataDataModel, attr) == value)
                result = result.one()
                result = result.to_dict()

                res, ewalidata_data = self.get_by_id(result["id"])

                # check if empty
                if res:
                    return True, ewalidata_data
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(EwalidataDataModel)
                for attr, value in where.items():
                    result = result.filter(getattr(EwalidataDataModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, ewalidata_data = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def check_data_exist(self, kode_pemda, kode_indikator, tahun):
        ''' Doc: function check data exist '''
        try:
            # execute database
            result = db.session.query(EwalidataDataModel)
            result = result.filter(EwalidataDataModel.kode_pemda == kode_pemda)
            result = result.filter(EwalidataDataModel.kode_indikator == kode_indikator)
            result = result.filter(EwalidataDataModel.tahun == tahun)
            result = result.count()
            if result == 0: return False
            else: return True

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def send_data_to_sipd(self, datas: dict):
        try:
            session = requests.Session()
            retry = Retry(connect=1, backoff_factor=0)
            adapter = HTTPAdapter(max_retries=retry)
            session.mount('http://', adapter)
            session.mount('https://', adapter)

            url = os.getenv('EWALIDATA_URL','https://jabar.sipd.go.id/ewalidata/serv') + '/push_dssd'
            payload = json.dumps({"data": datas})
            headers = {
                "Content-Type": "application/json",
                "Authorization": "Bearer 24c5d9aa25d516c013896ca247bca6f7"
            }
            req = session.post(url, headers=headers, data=payload, timeout=5)

            if req.status_code == 200:
                return True
            else:
                return False

        except Exception as err:
            print(err)

