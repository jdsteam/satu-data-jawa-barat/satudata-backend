''' Doc: controller request private  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import RequestPrivateModel, HistoryRequestPrivateModel
from models import UserModel
from models import UserViewModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
from sqlalchemy.orm import defer
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long


class RequestPrivateController(object):
    ''' Doc: class request private  '''

    def __init__(self, **kwargs):
        ''' Doc: function init '''

    def query(self, where: dict, search):
        ''' Doc: function query '''
        try:
            # query postgresql
            result = db.session.query(
                RequestPrivateModel.id, RequestPrivateModel.user_id, RequestPrivateModel.nama, RequestPrivateModel.telp, RequestPrivateModel.email,
                RequestPrivateModel.judul_data, RequestPrivateModel.tau_skpd, RequestPrivateModel.nama_skpd,
                RequestPrivateModel.kebutuhan_data, RequestPrivateModel.tujuan_data, RequestPrivateModel.datetime, RequestPrivateModel.status,
                RequestPrivateModel.notes, RequestPrivateModel.is_active, RequestPrivateModel.is_deleted, RequestPrivateModel.cdate, RequestPrivateModel.mdate,
                UserViewModel.satuan_kerja_nama, UserViewModel.lv1_unit_kerja_nama, UserViewModel.lv2_unit_kerja_nama, UserViewModel.lv3_unit_kerja_nama, UserViewModel.lv4_unit_kerja_nama
            )
            result = result.join(
                UserViewModel, RequestPrivateModel.user_id == UserViewModel.id)
            # result = result.join(UserModel, RequestPrivateModel.user_id == UserModel.id)
            # result = result.join(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(RequestPrivateModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(RequestPrivateModel, attr) == value)

            if search.lower() == 'persetujuan walidata':
                categorys = '1'
            elif search.lower() == 'dataset sedang diproses':
                categorys = '2'
            elif search.lower() == 'dataset tersedia':
                categorys = '4'
            elif search.lower() == 'dataset ditolak':
                categorys = '3'
            else:
                categorys = search

            result = result.filter(or_(
                cast(getattr(RequestPrivateModel, "id"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "email"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "judul_data"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "nama_skpd"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "kebutuhan_data"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "tujuan_data"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(RequestPrivateModel, "status"),
                     sqlalchemy.String).ilike('%'+categorys+'%'),
            ))
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all '''
        try:
            # change sorting key
            if sort[0] == 'organization':
                sort[0] = "skpd.nama_skpd"
            else:
                sort[0] = "request_private." + sort[0]

            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            request = []
            for res in result:
                # temp = res.__dict__
                temp = res._asdict()
                temp.pop('_sa_instance_state', None)
                temp.update(
                    {"datetime": temp['datetime'].strftime(FORMAT_DATETIME)})
                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate": temp['mdate'].strftime(FORMAT_DATETIME)})

                # get history_request_private
                temp['history'] = self.populate_history_request_private(temp)

                # get relation to user
                user = db.session.query(UserModel)
                user = user.get(temp['user_id'])

                if user:
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                    user.pop('password', None)
                    user.pop('is_active', None)
                    user.pop('is_deleted', None)
                    user.pop('cdate', None)
                    user.pop('cuid', None)

                    user = self.populate_skpd(user)
                    user = self.populate_skpdsub(user)
                    user = self.populate_skpdunit(user)

                else:
                    user = {}

                temp['user'] = user

                request.append(temp)

            # check if empty
            if request:
                return True, request
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_history_request_private(self, temp):
        ''' Doc: function get all '''
        temp['history'] = []
        history = db.session.query(HistoryRequestPrivateModel)
        history = history.filter(
            HistoryRequestPrivateModel.request_private_id == temp['id'])
        history = history.order_by(text("history_request_private.id asc"))
        history = history.all()
        for res_hist in history:
            temp_history = res_hist.__dict__
            temp_history.pop('_sa_instance_state', None)
            temp_history.update(
                {"datetime": temp_history['datetime'].strftime(FORMAT_DATETIME)})

            # populate user
            user = db.session.query(UserModel)
            user = user.get(temp_history['cuid'])
            if user:
                user = user.__dict__
                temp_history['username'] = user['username']
                temp_history['name'] = user['name']
            else:
                temp_history['username'] = ''
                temp_history['name'] = ''
            temp['history'].append(temp_history)

        return temp['history']

    def populate_skpd(self, user):
        ''' Doc: function populate_skpd '''
        if user['kode_skpd']:
            # get relation to skpd
            skpd = db.session.query(SkpdModel)
            skpd = skpd.options(
                defer("cuid"),
                defer("cdate"))
            skpd = skpd.filter(
                getattr(SkpdModel, "kode_skpd") == user['kode_skpd'])
            skpd = skpd.one()

            if skpd:
                skpd = skpd.__dict__
                skpd.pop('_sa_instance_state', None)
            else:
                skpd = {}

            user['skpd'] = skpd
            # temp.pop('user_id', None)
        else:
            user['skpd'] = {}

        return user

    def populate_skpdsub(self, user):
        ''' Doc: function populate_skpdsub '''
        try:
            if user['kode_skpdsub']:
                # get relation to skpdsub
                skpdsub = db.session.query(SkpdSubModel)
                skpdsub = skpdsub.options(
                    defer("cuid"),
                    defer("cdate"))
                skpdsub = skpdsub.filter(
                    getattr(SkpdSubModel, "kode_skpdsub") == user['kode_skpdsub'])
                skpdsub = skpdsub.one()

                if skpdsub:
                    skpdsub = skpdsub.__dict__
                    skpdsub.pop('_sa_instance_state', None)
                else:
                    skpdsub = {}

                user['skpdsub'] = skpdsub
                # temp.pop('user_id', None)
            else:
                user['skpdsub'] = {}
        except Exception as err:
            user['skpdsub'] = {}

        return user

    def populate_skpdunit(self, user):
        ''' Doc: function populate_skpdunit '''
        try:
            if user['kode_skpdunit']:
                # get relation to skpdunit
                skpdunit = db.session.query(SkpdUnitModel)
                skpdunit = skpdunit.options(
                    defer("cuid"),
                    defer("cdate"))
                skpdunit = skpdunit.filter(
                    getattr(SkpdUnitModel, "kode_skpdunit") == user['kode_skpdunit'])
                skpdunit = skpdunit.one()

                if skpdunit:
                    skpdunit = skpdunit.__dict__
                    skpdunit.pop('_sa_instance_state', None)
                else:
                    skpdunit = {}

                user['skpdunit'] = skpdunit
                # temp.pop('user_id', None)
            else:
                user['skpdunit'] = {}

        except Exception as err:
            user['skpdunit'] = {}

        return user

    def get_count(self, where: dict, search):
        ''' Doc: function get count '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id '''
        try:
            # execute database
            result = self.query({'id': _id}, '')
            result = result.all()

            # change into dict
            request = []
            for res in result:
                # temp = res.__dict__
                temp = res._asdict()
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATETIME)})
                temp.update({"mdate": temp['mdate'].strftime(FORMAT_DATETIME)})

                # get history_request_private
                temp['history'] = self.populate_history_request_private(temp)

                # get relation to user
                user = db.session.query(UserModel)
                user = user.get(temp['user_id'])

                if user:
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                    user.pop('password', None)
                    user.pop('is_active', None)
                    user.pop('is_deleted', None)
                    user.pop('cdate', None)
                    user.pop('cuid', None)

                    user = self.populate_skpd(user)
                    user = self.populate_skpdsub(user)
                    user = self.populate_skpdunit(user)

                else:
                    user = {}

                temp['user'] = user

                request.append(temp)

            # check if empty
            if request:
                return True, request[0], "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, json, condition):
        ''' Doc: function post param '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            if condition == 'add':
                json_send["cdate"] = HELPER.local_date_server()
            else:
                json_send["mdate"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create '''
        try:
            # prepare data model
            param = self.post_param(json, 'add')
            result = RequestPrivateModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update '''
        try:
            # generate json data
            param = self.post_param(json, 'edit')

            try:
                # prepare data model
                result = db.session.query(RequestPrivateModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(RequestPrivateModel, attr) == value)

                # execute database
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()
                res, request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete '''
        try:

            try:
                # prepare data model
                result = db.session.query(RequestPrivateModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(RequestPrivateModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
