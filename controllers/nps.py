''' Doc: controller nps  '''
from settings.configuration import Configuration
from helpers import Helper
from flask import send_file
from exceptions import ErrorMessage
from models import NpsModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer, aliased
from sqlalchemy import or_
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
from flask import request
from helpers import decorator

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
WHERE_DATE_MORE = "nps.datetime::date >= "
WHERE_DATE_LESS = "nps.datetime::date <= "
FORMAT_DATE_MORE = "nps.datetime::date >="
FORMAT_DATE_LESS = "nps.datetime::date <="

# pylint: disable=singleton-comparison, unused-variable, unused-argument, line-too-long, len-as-condition
class NpsController(object):
    ''' Doc: class nps  '''

    def __init__(self, **kwargs):
        ''' Doc: function nps  '''

    def get_all(self, where: dict, search, sort, limit, skip, start_date, end_date):
        ''' Doc: function get all  '''
        try:
            result = db.session.query(NpsModel)
            if start_date:
                result = result.filter(text(WHERE_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(WHERE_DATE_LESS + "'" + end_date + "'"))
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(NpsModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(NpsModel, attr) == value)
            result = result.filter(or_(cast(getattr(NpsModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in NpsModel.__table__.columns))
            result = result.order_by(text("nps."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            nps = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp["datetime"] = temp['datetime'].strftime(FORMAT_DATE)
                nps.append(temp)

            # check if empty
            nps = list(nps)
            if nps:
                return True, nps
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search, start_date, end_date):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(NpsModel.id)
            if start_date:
                result = result.filter(text(WHERE_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(WHERE_DATE_LESS + "'" + end_date + "'"))
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(NpsModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(NpsModel, attr) == value)
            result = result.filter(or_(cast(getattr(NpsModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in NpsModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(NpsModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}

            # change into dict
            nps = result

            # check if empty
            if nps:
                return True, nps
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            if 'datetime' not in json_send:
                json_send["datetime"] = HELPER.local_date_server()

            # prepare data model
            result = NpsModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, nps = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, nps
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(NpsModel)
                for attr, value in where.items():
                    result = result.filter(getattr(NpsModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, nps = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, nps
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(NpsModel)
                for attr, value in where.items():
                    result = result.filter(getattr(NpsModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, nps = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_download(self, start_date, end_date):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            result = db.session.query(NpsModel)
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.order_by(text("nps.id asc"))
            result = result.all()

            # change into dict
            nps = []
            for res in result:
                temp = res.__dict__
                temp.update({"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temps = {}
                temps['Id'] = temp['id']
                temps['Datetime'] = temp['datetime']
                temps['Score'] = temp['score']
                temps['Message'] = temp['message']
                temps['Page Source'] = temp['page_source']
                temps['is_active'] = temp['is_active']
                temps['is_deleted'] = temp['is_deleted']

                nps.append(temps)

            # check if empty
            nps = list(nps)
            if nps:
                return True, nps
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
