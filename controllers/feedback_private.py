''' Doc: controller feedback private  '''
from controllers.feedback_map import FORMAT_DATE_MORE
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import FeedbackPrivateModel
from models import UserModel
from models import SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, func, cast
from sqlalchemy.orm import defer
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
FORMAT_DATE_MORE = "feedback_private.datetime::date >= "
FORMAT_DATE_LESS = "feedback_private.datetime::date <= "

# pylint: disable=singleton-comparison, unused-variable
class FeedbackPrivateController(object):
    ''' Doc: class feedback private  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                FeedbackPrivateModel.id, FeedbackPrivateModel.datetime, FeedbackPrivateModel.score,
                FeedbackPrivateModel.tujuan, FeedbackPrivateModel.tujuan_tercapai, FeedbackPrivateModel.tujuan_mudah_ditemukan,
                FeedbackPrivateModel.masalah, FeedbackPrivateModel.saran, FeedbackPrivateModel.source_url, FeedbackPrivateModel.source_access,
                FeedbackPrivateModel.is_active, FeedbackPrivateModel.is_deleted, FeedbackPrivateModel.user_id
            )
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(FeedbackPrivateModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(FeedbackPrivateModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(FeedbackPrivateModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(FeedbackPrivateModel, "tujuan"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(FeedbackPrivateModel, "saran"), sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip, start_date, end_date):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)

            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))

            result = result.order_by(text("feedback_private."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            feedback_private = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime(FORMAT_DATE)})

                # get relation to user
                user = db.session.query(UserModel)
                user = user.get(temp['user_id'])

                if user:
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                    user.pop('password', None)
                    user.pop('is_active', None)
                    user.pop('is_deleted', None)
                    user.pop('cdate', None)
                    user.pop('cuid', None)

                    user = self.populate_skpd(user)

                else:
                    user = {}

                temp['user'] = user

                feedback_private.append(temp)

            # check if empty
            feedback_private = list(feedback_private)
            if feedback_private:
                return True, feedback_private
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search, start_date, end_date):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)

            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))

            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id':_id}, '')
            result = result.all()

            # change into dict
            feedback_private = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime(FORMAT_DATE)})

                # get relation to user
                user = db.session.query(UserModel)
                user = user.get(temp['user_id'])

                if user:
                    user = user.__dict__
                    user.pop('_sa_instance_state', None)
                    user.pop('password', None)
                    user.pop('is_active', None)
                    user.pop('is_deleted', None)
                    user.pop('cdate', None)
                    user.pop('cuid', None)

                    user = self.populate_skpd(user)

                else:
                    user = {}

                temp['user'] = user

                feedback_private.append(temp)

            # check if empty
            if feedback_private:
                return True, feedback_private, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            jwt = HELPER.read_jwt()
            json_send = {}
            json_send = _json
            json_send["datetime"] = HELPER.local_date_server()
            json_send['user_id'] = jwt['id']
            # prepare data model
            result = FeedbackPrivateModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, feedback_private, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, feedback_private
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = _json

            try:
                # prepare data model
                result = db.session.query(FeedbackPrivateModel)
                for attr, value in where.items():
                    result = result.filter(getattr(FeedbackPrivateModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, feedback_private, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, feedback_private
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(FeedbackPrivateModel)
                for attr, value in where.items():
                    result = result.filter(getattr(FeedbackPrivateModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, feedback_private, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_skpd(self, user):
        ''' Doc: function populate_skpd '''
        if user['kode_skpd']:
            # get relation to skpd
            skpd = db.session.query(SkpdModel)
            skpd = skpd.options(
                defer("cuid"),
                defer("cdate"))
            skpd = skpd.filter(getattr(SkpdModel, "kode_skpd") == user['kode_skpd'])
            skpd = skpd.one()

            if skpd:
                skpd = skpd.__dict__
                skpd.pop('_sa_instance_state', None)
            else:
                skpd = {}

            user['skpd'] = skpd
            # temp.pop('user_id', None)
        else:
            user['skpd'] = {}

        return user

    def get_chart_total(self, start_date, end_date):
        ''' Doc: function get chart total  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackPrivateModel)
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.count()

            if result:
                return True, result
            else:
                return False, 0

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_score(self, start_date, end_date):
        ''' Doc: function get chart score  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackPrivateModel.score, func.count(FeedbackPrivateModel.id).label('count'))
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.group_by(FeedbackPrivateModel.score)
            result = result.order_by(text("feedback_private.score asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan(self, start_date, end_date):
        ''' Doc: function get chart tujuan  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackPrivateModel.tujuan, func.count(FeedbackPrivateModel.id).label('count'))
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.group_by(FeedbackPrivateModel.tujuan)
            result = result.order_by(text("feedback_private.tujuan asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan_tercapai(self, start_date, end_date):
        ''' Doc: function get chart tujuan tercapai  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackPrivateModel.tujuan_tercapai, func.count(FeedbackPrivateModel.id).label('count'))
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.group_by(FeedbackPrivateModel.tujuan_tercapai)
            result = result.order_by(text("feedback_private.tujuan_tercapai asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_tujuan_mudah_ditemukan(self, start_date, end_date):
        ''' Doc: function get chart tujuan mudah diterima  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackPrivateModel.tujuan_mudah_ditemukan, func.count(FeedbackPrivateModel.id).label('count'))
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.group_by(FeedbackPrivateModel.tujuan_mudah_ditemukan)
            result = result.order_by(text("feedback_private.tujuan_mudah_ditemukan asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_masalah(self, start_date, end_date):
        ''' Doc: function get chart masalah  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackPrivateModel.masalah, func.count(FeedbackPrivateModel.id).label('count'))
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.group_by(FeedbackPrivateModel.masalah)
            result = result.order_by(text("feedback_private.masalah asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_harian(self, start_date, end_date):
        ''' Doc: function get chart harian  '''
        try:
            # query postgresql
            result = db.session.query(
                func.date_trunc('day', FeedbackPrivateModel.datetime).label('date'),
                func.count(FeedbackPrivateModel.id).label('count')
            )
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.filter(getattr(FeedbackPrivateModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackPrivateModel, 'is_deleted') == False)
            result = result.group_by(func.date_trunc('day', FeedbackPrivateModel.datetime))
            result = result.order_by(text("date asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                temp.update({"date":temp['date'].strftime("%Y-%m-%d")})
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_download(self, start_date, end_date):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            result = self.query({}, '')
            if start_date:
                result = result.filter(text(FORMAT_DATE_MORE + "'" + start_date + "'"))
            if end_date:
                result = result.filter(text(FORMAT_DATE_LESS + "'" + end_date + "'"))
            result = result.order_by(text("feedback_private.id asc"))
            result = result.all()

            # change into dict
            feedback = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime(FORMAT_DATE)})

                temps = {}
                temps['Id'] = temp['id']
                temps['Datetime'] = temp['datetime']
                temps['Score'] = temp['score']
                temps['Tujuan'] = temp['tujuan']
                temps['Tujuan Tercapai'] = temp['tujuan_tercapai']
                temps['Tujuan Mudah Ditemukan'] = temp['tujuan_mudah_ditemukan']
                temps['Masalah'] = temp['masalah']
                temps['Saran'] = temp['saran']
                temps['is_active'] = temp['is_active']
                temps['is_deleted'] = temp['is_deleted']

                feedback.append(temps)

            # check if empty
            feedback = list(feedback)
            if feedback:
                return True, feedback
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
