''' Doc: controller visualization  '''
from helpers import Helper
from helpers.sql_global import SqlGlobal
from exceptions import ErrorMessage
from models import VisualizationModel
from models import VisualizationAccessModel
from models import VisualizationDatasetModel
from models import VisualizationPermissionModel
from models import StrukturOrganisasiModel
from models import UserModel
from models import SektoralModel, SkpdModel, HighlightModel
from models import DatasetClassModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
from sqlalchemy.orm import defer
import json
import sqlalchemy, sentry_sdk
import re

HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long, bare-except, too-many-function-args
class VisualizationController(object):
    ''' Doc: class visualization  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def refresh(self):
        try:
            sql_query = "REFRESH MATERIALIZED VIEW suggestion_visualization"
            sql = text(sql_query)
            db.engine.execute(sql)
        except Exception as err:
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(VisualizationModel)
            result = result.join(SkpdModel, VisualizationModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.join(SektoralModel, VisualizationModel.sektoral_id == SektoralModel.id)
            result = result.join(DatasetClassModel, VisualizationModel.dataset_class_id == DatasetClassModel.id, isouter=True)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(VisualizationModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(VisualizationModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        # cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+search+'%'),
                        # cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                        # cast(getattr(SektoralModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                        # cast(getattr(VisualizationModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                        # cast(getattr(VisualizationModel, "description"), sqlalchemy.String).ilike('%'+search+'%'),
                        cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).match(src),
                        cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).match(src),
                        cast(getattr(SektoralModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(VisualizationModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(VisualizationModel, "description"), sqlalchemy.String).match(src),
                    ))

            return result

        except Exception as err:
            return str(err)

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # change sorting key
            if sort[0] == 'organization':
                sort[0] = "skpd.nama_skpd"
            elif sort[0] == 'topic':
                sort[0] = "sektoral.name"
            elif sort[0] == 'classification':
                sort[0] = "dataset_class.name"
            else:
                sort[0] = "visualization." + sort[0]

            # query postgresql
            result = self.query(where, search)

            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(VisualizationModel.dataset_class_id == 3)
                result = result.filter(VisualizationModel.is_deleted == False)
                result = result.filter(VisualizationModel.is_active == True)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            visualization = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATETIME)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATETIME)

                # get populate keywords
                temp = self.populate_keywords(temp)


                # old way will be deleted
                # get relation to visualization access
                if bool(jwt):
                    visualization_access = self.visualization_access_sql(temp['id'], jwt['id'])
                else:
                    visualization_access = []

                # get populate can_access
                temp = self.populate_can_access(temp, visualization_access)

                # new way
                # get relation to visualization permission
                if bool(jwt):
                    visualization_permission = self.visualization_permission_sql(temp['id'], jwt['jabatan_id'])
                else:
                    visualization_permission = []

                # get populate can_permission
                temp = self.populate_can_permission(temp, visualization_permission)

                temp = self.populate_visualization_permission(temp)


                # get relation to visualization dataset
                visualization_dataset = self.visualization_dataset_sql(temp['id'])

                # get populate visualization_dataset
                temp['visualization_dataset'] = self.populate_visualization_dataset(visualization_dataset)

                # populate dataset_class
                temp = self.populate_dataset_class(temp)

                # populate sektoral
                temp = self.populate_sektoral(temp)

                # populate license
                temp = self.populate_license(temp)

                # populate regional
                temp = self.populate_regional(temp)

                # populate skpd
                temp = self.populate_skpd(temp)

                # populate skpdsub
                temp = self.populate_skpdsub(temp)

                # populate skpdunit
                temp = self.populate_skpdunit(temp)

                visualization.append(temp)

            # check if empty
            if visualization:
                return True, visualization
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_keywords(self, temp):
        ''' Doc: function populate_keywords  '''
        if temp['keywords']:
            if isinstance(json.loads(temp['keywords']), list):
                temp['keywords'] = json.loads(temp['keywords'])
            else:
                temp['keywords'] = json.loads(json.loads(temp['keywords']))
        else:
            temp['keywords'] = []

        return temp

    def populate_can_access(self, temp, visualization_access):
        ''' Doc: function populate_can_access  '''
        if int(temp['dataset_class_id']) == 3 or int(temp['dataset_class_id']) == 5:
            temp['can_access'] = True
        else:
            if visualization_access:
                temp['can_access'] = True
            else:
                temp['can_access'] = False

        return temp

    def populate_can_permission(self, temp, visualization_permission):
        ''' Doc: function populate_can_access  '''
        if int(temp['dataset_class_id']) == 3 or int(temp['dataset_class_id']) == 5:
            temp['can_access_new'] = True
        else:
            if visualization_permission:
                temp['can_access_new'] = True
            else:
                temp['can_access_new'] = False

        return temp


    def populate_visualization_dataset(self, visualization_dataset):
        ''' Doc: function populate_visualization_dataset  '''
        vd_arr = []
        for vd_ in visualization_dataset:
            vd_ = vd_.__dict__
            vd_.pop('_sa_instance_state', None)

            # get relation to dataset
            dataset = SqlGlobal.dataset(vd_['dataset_id'])

            if dataset:
                dataset = dataset.__dict__
                dataset.pop('_sa_instance_state', None)
                dataset.pop('cdate', None)
                dataset.pop('mdate', None)
                dataset.pop('period', None)

            else:
                dataset = {}

            # get relation to dataset tag
            dataset_tag = SqlGlobal.dataset_tag(vd_['dataset_id'])

            dataset_tags = []
            for tag in dataset_tag:
                temp_tag = {}
                temp_tag['tag'] = tag[0]
                dataset_tags.append(temp_tag)

            vd_.update({"dataset_tag":dataset_tags})
            vd_.update({"dataset":dataset})

            vd_arr.append(vd_)

        return vd_arr

    def populate_dataset_class(self, temp):
        ''' Doc: function populate_dataset_class  '''
        # get relation to dataset_class
        dataset_class = SqlGlobal.dataset_class(temp['dataset_class_id'])

        if dataset_class:
            dataset_class = dataset_class.__dict__
            dataset_class.pop('_sa_instance_state', None)
            dataset_class.pop('cdate', None)
            dataset_class.pop('mdate', None)
        else:
            dataset_class = {}

        temp['dataset_class'] = dataset_class
        temp.pop('dataset_class_id', None)

        return temp

    def populate_sektoral(self, temp):
        ''' Doc: function populate_sektoral  '''
        # get relation to sektoral
        sektoral = SqlGlobal.sektoral(temp['sektoral_id'])

        if sektoral:
            sektoral = sektoral.__dict__
            sektoral.pop('_sa_instance_state', None)
            sektoral.pop('cdate', None)
            sektoral.pop('mdate', None)
        else:
            sektoral = {}

        temp['sektoral'] = sektoral
        temp.pop('sektoral_id', None)

        return temp

    def populate_license(self, temp):
        ''' Doc: function populate_license  '''
        if temp['license_id']:
            # get relation to license
            licenses = SqlGlobal.licenses(temp['license_id'])

            if licenses:
                licenses = licenses.__dict__
                licenses.pop('_sa_instance_state', None)
                licenses.pop('cdate', None)
                licenses.pop('mdate', None)
            else:
                licenses = {}

            temp['license'] = licenses
            temp.pop('license_id', None)
        else:
            temp['license'] = {}
            temp.pop('license_id', None)

        return temp

    def populate_regional(self, temp):
        ''' Doc: function populate_regional  '''
        # get relation to regional
        regional = SqlGlobal.regional(temp['regional_id'])

        if regional:
            regional = regional.__dict__
            regional.pop('_sa_instance_state', None)
            regional.pop('cdate', None)
            regional.pop('mdate', None)
        else:
            regional = {}

        temp['regional'] = regional
        temp.pop('regional_id', None)

        return temp

    def populate_skpd(self, temp):
        ''' Doc: function populate_skpd  '''
        # get relation to skpd
        skpd = SqlGlobal.skpd(temp['kode_skpd'])

        if skpd:
            skpd = skpd.__dict__
            skpd.pop('_sa_instance_state', None)
            skpd.pop('cdate', None)
            skpd.pop('mdate', None)
        else:
            skpd = {}

        temp['skpd'] = skpd

        return temp

    def populate_skpdsub(self, temp):
        ''' Doc: function populate_skpdsub  '''
        if temp['kode_skpdsub']:
            try:
                # get relation to skpdsub
                skpdsub = SqlGlobal.skpd_sub(temp['kode_skpdsub'])

                if skpdsub:
                    skpdsub = skpdsub.__dict__
                    skpdsub.pop('_sa_instance_state', None)
                    skpdsub.pop('cdate', None)
                    skpdsub.pop('mdate', None)
                else:
                    skpdsub = {}
            except Exception as err:
                print(str(err))
                skpdsub = {}

            temp['skpdsub'] = skpdsub
        else:
            temp['skpdsub'] = {}

        return temp

    def populate_skpdunit(self, temp):
        ''' Doc: function populate_skpdunit  '''
        if temp['kode_skpdunit']:
            try:
                # get relation to skpdunit
                skpdunit = SqlGlobal.skpd_unit(temp['kode_skpd'], temp['kode_skpdsub'], temp['kode_skpdunit'])

                if skpdunit:
                    skpdunit = skpdunit.__dict__
                    skpdunit.pop('_sa_instance_state', None)
                    skpdunit.pop('cdate', None)
                    skpdunit.pop('mdate', None)
                else:
                    skpdunit = {}
            except Exception as err:
                print(str(err))
                skpdunit = {}

            temp['skpdunit'] = skpdunit
        else:
            temp['skpdunit'] = {}

        return temp

    def visualization_access_sql(self, param, jwt):
        ''' Doc: function visualizaton access sql  '''
        try:
            visualization_access = db.session.query(VisualizationAccessModel)
            visualization_access = visualization_access.distinct(VisualizationAccessModel.user_id)
            visualization_access = visualization_access.options(
                defer("cuid"), defer("cdate"), defer("enable"),
                defer("visualization_id")
            )
            visualization_access = visualization_access.filter(VisualizationAccessModel.visualization_id == param)
            visualization_access = visualization_access.filter(VisualizationAccessModel.user_id == jwt)
            visualization_access = visualization_access.all()
            return visualization_access
        except Exception as err:
            return str(err)

    def visualization_permission_sql(self, param, jwt):
        ''' Doc: function visualizaton permission sql  '''
        try:
            visualization_permission = db.session.query(VisualizationPermissionModel)
            visualization_permission = visualization_permission.distinct(VisualizationPermissionModel.jabatan_id)
            visualization_permission = visualization_permission.options(
                defer("cuid"), defer("cdate"), defer("enable"),
                defer("visualization_id")
            )
            visualization_permission = visualization_permission.filter(VisualizationPermissionModel.visualization_id == param)
            visualization_permission = visualization_permission.filter(VisualizationPermissionModel.jabatan_id == jwt)
            visualization_permission = visualization_permission.all()
            return visualization_permission
        except Exception as err:
            return str(err)

    def visualization_dataset_sql(self, param):
        ''' Doc: function visualization dataset sq; '''
        try:
            visualization_dataset = db.session.query(VisualizationDatasetModel)
            visualization_dataset = visualization_dataset.distinct(VisualizationDatasetModel.dataset_id)
            visualization_dataset = visualization_dataset.options(
                defer("cuid"), defer("cdate"), defer("visualization_id")
            )
            visualization_dataset = visualization_dataset.filter(VisualizationDatasetModel.visualization_id == param)
            visualization_dataset = visualization_dataset.all()
            return visualization_dataset
        except Exception as err:
            return str(err)

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id, where: dict):
        ''' Doc: function get by id  '''
        # handle id=title, integer or string
        try:
            temp = int(_id)
            _id = temp
        except Exception as err:
            temp = _id
            result = db.session.query(VisualizationModel.id)
            result = result.filter(VisualizationModel.title == temp)
            result = result.all()
            if result:
                result = result[0]
                _id = result[0]
            else:
                _id = 0

        try:
            # execute database
            result = db.session.query(VisualizationModel)
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(VisualizationModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(VisualizationModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            result = self.populate_check_private(result, jwt, _id)

            # change into dict
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                result['cdate'] = result['cdate'].strftime(FORMAT_DATETIME)
                result['mdate'] = result['mdate'].strftime(FORMAT_DATETIME)

                # get populate keywords
                result = self.populate_keywords(result)


                # old way will be deleted
                # get relation to visualization access
                if bool(jwt):
                    visualization_access = self.visualization_access_sql(result['id'], jwt['id'])
                else:
                    visualization_access = []

                # get populate can_access
                result = self.populate_can_access(result, visualization_access)

                # new way
                # get relation to visualization permission
                if bool(jwt):
                    visualization_permission = self.visualization_permission_sql(result['id'], jwt['jabatan_id'])
                else:
                    visualization_permission = []

                # get populate can_permission
                result = self.populate_can_permission(result, visualization_permission)

                result = self.populate_visualization_permission(result)


                # get relation to visualization dataset
                visualization_dataset = self.visualization_dataset_sql(result['id'])

                # get populate visualization_dataset
                result['visualization_dataset'] = self.populate_visualization_dataset(visualization_dataset)

                # populate dataset_class
                result = self.populate_dataset_class(result)

                # populate sektoral
                result = self.populate_sektoral(result)

                # populate license
                result = self.populate_license(result)

                # populate regional
                result = self.populate_regional(result)

                # populate skpd
                result = self.populate_skpd(result)

                # populate skpdsub
                result = self.populate_skpdsub(result)

                # populate skpdunit
                result = self.populate_skpdunit(result)

            else:
                result = {}

            # check if empty
            if result:
                return True, result, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_check_private(self, result, jwt, _id):
        ''' Doc: function populate_check_private  '''
        if jwt:
            # result = result.get(_id)
            result = result.filter(VisualizationModel.id == _id)
        else:
            result = result.filter(VisualizationModel.dataset_class_id == 3)
            result = result.filter(VisualizationModel.id == _id)
            result = result.filter(VisualizationModel.is_deleted == False)
            result = result.filter(VisualizationModel.is_active == True)

        result = result.all()
        if result:
            result = result[0]

        return result

    def populate_visualization_permission(self, temp):
        ''' Doc: function populate relation dataset permission  '''
        # get relation to history_draft
        try:
            visualization_permission = db.session.query(
                VisualizationPermissionModel.id,
                StrukturOrganisasiModel.jabatan_id, StrukturOrganisasiModel.jabatan_nama, StrukturOrganisasiModel.kode_skpd,
                StrukturOrganisasiModel.satuan_kerja_id, StrukturOrganisasiModel.satuan_kerja_nama,
                StrukturOrganisasiModel.lv1_unit_kerja_id, StrukturOrganisasiModel.lv1_unit_kerja_nama,
                StrukturOrganisasiModel.lv2_unit_kerja_id, StrukturOrganisasiModel.lv2_unit_kerja_nama,
                StrukturOrganisasiModel.lv3_unit_kerja_id, StrukturOrganisasiModel.lv3_unit_kerja_nama,
                StrukturOrganisasiModel.lv4_unit_kerja_id, StrukturOrganisasiModel.lv4_unit_kerja_nama,
            )
            visualization_permission = visualization_permission.join(StrukturOrganisasiModel, StrukturOrganisasiModel.jabatan_id == VisualizationPermissionModel.jabatan_id)
            visualization_permission = visualization_permission.filter(getattr(VisualizationPermissionModel, "visualization_id") == temp['id'])
            visualization_permission = visualization_permission.all()

            if visualization_permission:
                temp['visualization_permission'] = []
                for dp_ in visualization_permission:
                    dp_ = dp_._asdict()
                    dp_.pop('_sa_instance_state', None)
                    temp['visualization_permission'].append(dp_)
            else:
                temp['visualization_permission'] = []
        except Exception:
            temp['visualization_permission'] = []

        return temp

    def route_create_jabatan_access(self, visualization, jabatan_access):
        ''' Doc: function route_create_user_permission  '''
        try:
            if visualization['dataset_class']['id'] == 4:
                # insert for walidata
                result_user = db.session.query(UserModel.id, UserModel.jabatan_id)
                result_user = result_user.filter(getattr(UserModel, 'role_id') == 10)
                result_user = result_user.all()
                for res_user in result_user:
                    temp_user = res_user._asdict()
                    try:
                        visualization_permission = {}
                        visualization_permission['visualization_id'] = visualization['id']
                        visualization_permission['jabatan_id'] = temp_user['jabatan_id']
                        visualization_permission['enable'] = True
                        visualization_permission = VisualizationPermissionModel(**visualization_permission)
                        db.session.add(visualization_permission)
                        db.session.commit()
                    except Exception as err:
                        print(str(err))

                # insert user jabatan
                for ja_ in jabatan_access:
                    try:
                        visualization_permission = {}
                        visualization_permission['visualization_id'] = visualization['id']
                        visualization_permission['jabatan_id'] = ja_
                        visualization_permission['enable'] = True
                        visualization_permission = VisualizationPermissionModel(**visualization_permission)
                        db.session.add(visualization_permission)
                        db.session.commit()

                    except Exception as err:
                        print(str(err))

        except Exception as err:
            print(str(err))

    def route_update_jabatan_access(self, visualization, jabatan_access):
        ''' Doc: function route_create_user_permission  '''
        try:
            if visualization['dataset_class']['id'] == 4:
                # delete user jabatan
                result = db.session.query(VisualizationPermissionModel)
                result = result.filter(getattr(VisualizationPermissionModel, 'visualization_id') == visualization['id'])
                result = result.all()
                for res in result:
                    db.session.delete(res)
                    db.session.commit()

                # insert for walidata
                result_user = db.session.query(UserModel.id, UserModel.jabatan_id)
                result_user = result_user.filter(getattr(UserModel, 'role_id') == 10)
                result_user = result_user.all()
                for res_user in result_user:
                    temp_user = res_user._asdict()
                    try:
                        visualization_permission = {}
                        visualization_permission['visualization_id'] = visualization['id']
                        visualization_permission['jabatan_id'] = temp_user['jabatan_id']
                        visualization_permission['enable'] = True
                        visualization_permission = VisualizationPermissionModel(**visualization_permission)
                        db.session.add(visualization_permission)
                        db.session.commit()
                    except Exception as err:
                        print(str(err))

                # insert user jabatan
                for ja_ in jabatan_access:
                    try:
                        visualization_permission = {}
                        visualization_permission['visualization_id'] = visualization['id']
                        visualization_permission['jabatan_id'] = ja_
                        visualization_permission['enable'] = True
                        visualization_permission = VisualizationPermissionModel(**visualization_permission)
                        db.session.add(visualization_permission)
                        db.session.commit()
                    except Exception as err:
                        print(str(err))

        except Exception as err:
            print(str(err))

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            # title
            json_send["title"] = json_send["name"].lower().replace(' ', '-')
            json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(VisualizationModel.id)
            res_count = res_count.filter(VisualizationModel.name == json_send["name"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            json_send["cuid"] = jwt['id']
            json_send["count_view"] = 0
            json_send["count_rating"] = 0
            json_send["count_share_fb"] = 0
            json_send["count_share_tw"] = 0
            json_send["count_share_wa"] = 0
            json_send["count_share_link"] = 0
            json_send["count_download_img"] = 0
            json_send["count_download_pdf"] = 0
            json_send["cdate"] = HELPER.local_date_server()
            # prepare data model

            result = VisualizationModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, visualization, message = self.get_by_id(result['id'], {})
            self.refresh()

            # check if exist
            if res:
                return True, visualization
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            if 'name' in json_send:
                json_send["title"] = json_send["name"].lower().replace(' ', '-')
                json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])

            if 'count_share_fb' in _json or 'count_share_tw' in _json or 'count_share_wa' in _json or 'count_share_link' in _json or 'count_download_img' in _json or 'count_download_pdf' in _json or 'count_view' in _json or 'count_download_img_private' in _json or 'count_download_pdf_private' in _json or 'count_view_private' in _json:
                json_send = _json
            else:
                json_send["mdate"] = HELPER.local_date_server()
                json_send["muid"] = jwt['id']
            try:
                # prepare data model
                result = db.session.query(VisualizationModel)
                for attr, value in where.items():
                    result = result.filter(getattr(VisualizationModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, dataset, message = self.get_by_id(where["id"], {})
                self.refresh()

                # check if empty
                if res:
                    return True, dataset
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(VisualizationModel)
                for attr, value in where.items():
                    result = result.filter(getattr(VisualizationModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, dataset, message = self.get_by_id(where["id"], {})
                self.refresh()

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_alls(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get alls  '''
        try:
            # query exclude
            result_exclude = db.session.query(HighlightModel.category_id)
            result_exclude = result_exclude.filter(getattr(HighlightModel, 'category') == 'visualisasi')
            result_exclude = result_exclude.all()

            # query postgresql
            result = db.session.query(VisualizationModel)
            result = result.options(
                defer("cuid"),
                defer("muid"))
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(VisualizationModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(VisualizationModel, attr) == value)
            # where exclude
            for row_exclude in result_exclude:
                result = result.filter(getattr(VisualizationModel, 'id') != row_exclude[0])

            # sort
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            visualization = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATETIME)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATETIME)

                visualization.append(temp)

            # check if empty
            visualization = list(visualization)
            if visualization:
                return True, visualization
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def switch(self, category, visualization):
        ''' Doc: function switch  '''
        if not category:
            category = 'view'
        return getattr(self, 'case_' + str(category), lambda: visualization)(visualization)
    def case_facebook(self, visualization):
        ''' Doc: function switch  '''
        count_share_fb = visualization['count_share_fb']
        if not count_share_fb:
            count_share_fb = '0'
        count_share_fb = int(count_share_fb) + 1
        payload = {'count_share_fb': int(count_share_fb)}
        visualization['count_share_fb'] = count_share_fb
        return visualization, payload
    def case_twitter(self, visualization):
        ''' Doc: function case_twitter  '''
        count_share_tw = visualization['count_share_tw']
        if not count_share_tw:
            count_share_tw = '0'
        count_share_tw = int(count_share_tw) + 1
        payload = {'count_share_tw': int(count_share_tw)}
        visualization['count_share_tw'] = count_share_tw
        return visualization, payload
    def case_whatsapp(self, visualization):
        ''' Doc: function case_whatsapp  '''
        count_share_wa = visualization['count_share_wa']
        if not count_share_wa:
            count_share_wa = '0'
        count_share_wa = int(count_share_wa) + 1
        payload = {'count_share_wa': int(count_share_wa)}
        visualization['count_share_wa'] = count_share_wa
        return visualization, payload
    def case_link(self, visualization):
        ''' Doc: function case_view  '''
        count_share_link = visualization['count_share_link']
        if not count_share_link:
            count_share_link = '0'
        count_share_link = int(count_share_link) + 1
        payload = {'count_share_link': int(count_share_link)}
        visualization['count_share_link'] = count_share_link
        return visualization, payload
    def case_image(self, visualization):
        ''' Doc: function case_view  '''
        count_download_img = visualization['count_download_img']
        if not count_download_img:
            count_download_img = '0'
        count_download_img = int(count_download_img) + 1
        payload = {'count_download_img': int(count_download_img)}
        visualization['count_download_img'] = count_download_img
        return visualization, payload
    def case_pdf(self, visualization):
        ''' Doc: function case_access  '''
        count_download_pdf = visualization['count_download_pdf']
        if not count_download_pdf:
            count_download_pdf = '0'
        count_download_pdf = int(count_download_pdf) + 1
        payload = {'count_download_pdf': int(count_download_pdf)}
        visualization['count_download_pdf'] = count_download_pdf
        return visualization, payload
    def case_view(self, visualization):
        ''' Doc: function case_access  '''
        count_view = visualization['count_view']
        if not count_view:
            count_view = '0'
        count_view = int(count_view) + 1
        payload = {'count_view': int(count_view)}
        visualization['count_view'] = count_view
        return visualization, payload
    def case_image_private(self, visualization):
        ''' Doc: function case_view_private  '''
        count_download_img_private = visualization['count_download_img_private']
        if not count_download_img_private:
            count_download_img_private = '0'
        count_download_img_private = int(count_download_img_private) + 1
        payload = {'count_download_img_private': int(count_download_img_private)}
        visualization['count_download_img_private'] = count_download_img_private
        return visualization, payload
    def case_pdf_private(self, visualization):
        ''' Doc: function case_access_private  '''
        count_download_pdf_private = visualization['count_download_pdf_private']
        if not count_download_pdf_private:
            count_download_pdf_private = '0'
        count_download_pdf_private = int(count_download_pdf_private) + 1
        payload = {'count_download_pdf_private': int(count_download_pdf_private)}
        visualization['count_download_pdf_private'] = count_download_pdf_private
        return visualization, payload
    def case_view_private(self, visualization):
        ''' Doc: function case_access_private  '''
        count_view_private = visualization['count_view_private']
        if not count_view_private:
            count_view_private = '0'
        count_view_private = int(count_view_private) + 1
        payload = {'count_view_private': int(count_view_private)}
        visualization['count_view_private'] = count_view_private
        return visualization, payload
