''' Doc: controller mapset thematic source  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetThematicSourceModel
from models import SkpdModel
from models import BidangUrusanModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy
import sentry_sdk


CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long


class MapsetThematicSourceController(object):
    ''' Doc: class mapset thematic source  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                MapsetThematicSourceModel.id, MapsetThematicSourceModel.name, MapsetThematicSourceModel.datatype, MapsetThematicSourceModel.year, MapsetThematicSourceModel.mapset_thematic_id, MapsetThematicSourceModel.external_org_name, MapsetThematicSourceModel.kode_skpd,
                SkpdModel.id.label('skpd_id'), SkpdModel.nama_skpd.label(
                    'nama_skpd'),
                    MapsetThematicSourceModel.cuid,MapsetThematicSourceModel.cdate,MapsetThematicSourceModel.muid,MapsetThematicSourceModel.mdate
            )

            result = result.join(
                SkpdModel, MapsetThematicSourceModel.kode_skpd == SkpdModel.kode_skpd, isouter=True)
            
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(MapsetThematicSourceModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(MapsetThematicSourceModel, attr) == value)


            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_data(self, temp):
      ''' Doc: function populate data '''
      try:
        # get relation to sektoral
        temp['skpd'] = {}
        temp['skpd']['id'] = temp['skpd_id']
        temp['skpd']['kode_skpd'] = temp['kode_skpd']
        temp['skpd']['nama_skpd'] = temp['nama_skpd']
        temp.pop('nama_skpd', None)
        temp.pop('skpd_id', None)

        return temp
      except Exception as err:
        # fail response
        sentry_sdk.capture_exception(err)
        raise ErrorMessage(str(err), 500, 1, {})


    def get_all(self, where: dict, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where)
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            source = []
            for res in result:
                temp = res._asdict()
                temp = self.populate_data(temp)
                source.append(temp)
            
            if source:
                return True, source
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(MapsetThematicSourceModel.id)
            for attr, value in where.items():
                result = result.filter(
                    getattr(MapsetThematicSourceModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetThematicSourceModel, col.title), sqlalchemy.String).ilike(
                '%'+search+'%') for col in MapsetThematicSourceModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(MapsetThematicSourceModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            mapset_thematic_source = result

            # check if empty
            if mapset_thematic_source:
                return True, mapset_thematic_source
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = MapsetThematicSourceModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, mapset_thematic_source = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, mapset_thematic_source
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            jwt = HELPER.read_jwt()

            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            # generate json data
            json_send = {}
            json_send = json
            try:
                # prepare data model
                result = db.session.query(MapsetThematicSourceModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(MapsetThematicSourceModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, mapset_thematic_source = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, mapset_thematic_source
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(MapsetThematicSourceModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(MapsetThematicSourceModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, _ = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
