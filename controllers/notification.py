''' Doc: controller notification  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import NotificationModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class NotificationController(object):
    ''' Doc: class notification  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(NotificationModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(NotificationModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(NotificationModel, attr) == value)
            result = result.filter(or_(cast(getattr(NotificationModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in NotificationModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            notification = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                notification.append(temp)

            # check if empty
            notification = list(notification)
            if notification:
                return True, notification
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(NotificationModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(NotificationModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(NotificationModel, attr) == value)
            result = result.filter(or_(cast(getattr(NotificationModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in NotificationModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(NotificationModel)
            result = result.options(
                defer("mdate"),
                defer("cdate"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}

            # change into dict
            notification = result

            # check if empty
            if notification:
                return True, notification
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()

            # prepare data model
            result = NotificationModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, notification = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, notification
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()

            try:
                # prepare data model
                result = db.session.query(NotificationModel)
                for attr, value in where.items():
                    result = result.filter(getattr(NotificationModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, notification = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, notification
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update_read(self, _type, type_id, receiver, json: dict):
        ''' Doc: function update read  '''
        try:
            result = db.session.query(NotificationModel)
            result = result.filter(NotificationModel.type == _type)
            result = result.filter(NotificationModel.type_id == type_id)
            result = result.filter(NotificationModel.receiver == receiver)
            result = result.update(json, synchronize_session='fetch')
            result = db.session.commit()

            result = db.session.query(NotificationModel)
            result = result.filter(NotificationModel.type == _type)
            result = result.filter(NotificationModel.type_id == type_id)
            result = result.filter(NotificationModel.receiver == receiver)
            result = result.all()

            notifications = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                notifications.append(temp)

            return True, notifications

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(NotificationModel)
                for attr, value in where.items():
                    result = result.filter(getattr(NotificationModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, notification = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
