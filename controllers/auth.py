''' Doc: controller auth  '''
import string
import random, sentry_sdk
import requests
import json
import datetime
import sqlalchemy
from datetime import timedelta
from pytz import timezone
from helpers import Helper
from exceptions import ErrorMessage
from models import AppModel, RegionalModel
from models import RegionalLevelModel, RoleModel, SkpdModel
from models import SkpdSubModel, SkpdUnitModel, UserModel
from models import UserAppRoleModel
from models import AgreementModel, StrukturOrganisasiModel, JabatanModel
from models import LoginAttemptModel
from models import DashboardRoleModel, DashboardRolePermissionModel, DashboardAppModel, DashboardAppServiceModel
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers import UserController
from settings import configuration as conf
from sentry_sdk import set_user
USER_CONTROLLER = UserController()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable, singleton-comparison
class AuthController(object):
    ''' Doc: controller auth  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def login(self, json_data: dict):
        ''' Doc: function init  '''
        try:
            # query postgresql
            if "username" not in json_data:
                raise ErrorMessage("Username is missing", 500, 1, {})

            if "password" not in json_data:
                raise ErrorMessage("Password is missing", 500, 1, {})

            if "app_code" not in json_data:
                raise ErrorMessage("App Code is missing", 500, 1, {})

            # query
            result = db.session.query(
                UserAppRoleModel.id.label('user_app_role_id'),
                UserModel.id, UserModel.email, UserModel.username, UserModel.password, UserModel.nip,
                UserModel.kode_kemendagri, UserModel.kode_skpd, UserModel.kode_skpdsub, UserModel.kode_skpdunit,
                UserModel.name, UserModel.profile_pic, UserModel.notes, UserModel.last_login,
                UserModel.cdate, UserModel.cuid, UserModel.is_active, UserModel.is_deleted,
                RoleModel.id.label('role_id'), RoleModel.name.label('role_name'),
                AppModel.id.label('app_id'), AppModel.name.label('app_name'), AppModel.url.label('app_url'),
                AppModel.code.label('app_code'),
                RegionalLevelModel.name.label('regional_level'), RegionalModel.nama_kemendagri,
                SkpdModel.nama_skpd, SkpdSubModel.nama_skpdsub, SkpdUnitModel.nama_skpdunit, SkpdModel.is_external,
                UserModel.profile_pic, SkpdModel.logo,
                UserModel.agreement_id
            )
            result = result.outerjoin(UserModel, UserAppRoleModel.user_id == UserModel.id)
            result = result.outerjoin(AppModel, UserAppRoleModel.app_id == AppModel.id)
            result = result.outerjoin(RoleModel, UserAppRoleModel.role_id == RoleModel.id)
            result = result.outerjoin(RegionalModel, UserModel.kode_kemendagri == RegionalModel.kode_kemendagri)
            result = result.outerjoin(RegionalLevelModel, RegionalModel.regional_level_id == RegionalLevelModel.id)
            result = result.outerjoin(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.outerjoin(SkpdSubModel, UserModel.kode_skpdsub == SkpdSubModel.kode_skpdsub)
            result = result.outerjoin(SkpdUnitModel, UserModel.kode_skpdunit == SkpdUnitModel.kode_skpdunit)
            result = result.filter(UserModel.username == json_data['username'])
            result = result.filter(AppModel.code == json_data['app_code'])
            result = result.filter(UserModel.is_deleted == False)
            result = result.filter(UserModel.is_active == True)
            result = result.all()


            user = []
            for res in result:
                temp = {}
                temp['user_app_role_id'] = res[0]
                temp['id'] = res[1]
                temp['email'] = res[2]
                temp['username'] = res[3]
                temp['password'] = res[4]
                temp['nip'] = res[5]
                temp['kode_kemendagri'] = res[6]
                temp['kode_skpd'] = res[7]
                temp['kode_skpdsub'] = res[8]
                temp['kode_skpdunit'] = res[9]
                temp['name'] = res[10]
                temp['profile_pic'] = res[11]
                temp['notes'] = res[12]
                temp['last_login'] = res[13]
                temp['cdate'] = res[14]
                temp['cuid'] = res[15]
                temp['is_active'] = res[16]
                temp['is_deleted'] = res[17]
                temp['role_id'] = res[18]
                temp['role_name'] = res[19]
                temp['app_id'] = res[20]
                temp['app_name'] = res[21]
                temp['app_url'] = res[22]
                temp['app_code'] = res[23]
                temp['regional_level'] = res[24]
                temp['nama_kemendagri'] = res[25]
                temp['nama_skpd'] = res[26]
                temp['nama_skpdsub'] = res[27]
                temp['nama_skpdunit'] = res[28]
                temp['is_external'] = res[29]
                temp['profile_pic'] = res[30]
                temp['logo_skpd'] = res[31]
                temp['agreement_id'] = res[32]
                user.append(temp)

            # check if empty
            if user:
                user = user[0]
                user_pass_ok = HELPER.verify_hash(json_data["password"], user["password"])
                if user_pass_ok:

                    forjwt = {}
                    forjwt["id"] = str(user["id"])
                    forjwt["username"] = user["username"]
                    forjwt["email"] = user["email"]
                    forjwt["name"] = user["name"]
                    forjwt["regional_level"] = user["regional_level"]
                    forjwt["kode_kemendagri"] = user["kode_kemendagri"]
                    forjwt["nama_kemendagri"] = user["nama_kemendagri"]

                    forjwt["profile_pic"] = user["profile_pic"]
                    if not forjwt["profile_pic"]:
                        forjwt["profile_pic"] = ""
                    else:
                        forjwt["profile_pic"] = user["profile_pic"]

                    forjwt["logo_skpd"] = user["logo_skpd"]
                    if not forjwt["logo_skpd"]:
                        forjwt["logo_skpd"] = ""
                    else:
                        forjwt["logo_skpd"] = user["logo_skpd"]

                    forjwt["kode_skpd"] = user["kode_skpd"]
                    if forjwt["kode_skpd"] == "":
                        forjwt["nama_skpd"] = ""
                    else:
                        forjwt["nama_skpd"] = user["nama_skpd"]

                    forjwt["kode_skpdsub"] = user["kode_skpdsub"]
                    if forjwt["kode_skpdsub"] == "":
                        forjwt["nama_skpdsub"] = ""
                    else:
                        forjwt["nama_skpdsub"] = user["nama_skpdsub"]

                    forjwt["kode_skpdunit"] = user["kode_skpdunit"]
                    if forjwt["kode_skpdunit"] == "":
                        forjwt["nama_skpdunit"] = ""
                    else:
                        forjwt["nama_skpdunit"] = user["nama_skpdunit"]
                    forjwt["is_external"] = user["is_external"]

                    forjwt2 = {}
                    forjwt2["id"] = str(user["role_id"])
                    forjwt2["name"] = str(user["role_name"])
                    forjwt3 = {}
                    forjwt3["id"] = str(user["app_id"])
                    forjwt3["name"] = str(user["app_name"])
                    forjwt3["code"] = str(user["app_code"])
                    forjwt3["url"] = str(user["app_url"])
                    forjwt["app"] = forjwt3
                    forjwt["role"] = forjwt2
                    jwt = HELPER.jwt_encode(forjwt)

                    result = {}
                    result["id"] = str(user["id"])
                    result["username"] = user["username"]
                    result["email"] = user["email"]
                    result["name"] = user["name"]
                    result["regional_level"] = user["regional_level"]
                    result["kode_kemendagri"] = user["kode_kemendagri"]
                    result["nama_kemendagri"] = user["nama_kemendagri"]

                    result["profile_pic"] = user["profile_pic"]
                    if not result["profile_pic"]:
                        result["profile_pic"] = ""
                    else:
                        result["profile_pic"] = user["profile_pic"]

                    result["logo_skpd"] = user["logo_skpd"]
                    if not result["logo_skpd"]:
                        result["logo_skpd"] = ""
                    else:
                        result["logo_skpd"] = user["logo_skpd"]

                    result["kode_skpd"] = user["kode_skpd"]
                    if result["kode_skpd"] == "":
                        result["nama_skpd"] = ""
                    else:
                        result["nama_skpd"] = user["nama_skpd"]

                    result["kode_skpdsub"] = user["kode_skpdsub"]
                    if result["kode_skpdsub"] == "":
                        result["nama_skpdsub"] = ""
                    else:
                        result["nama_skpdsub"] = user["nama_skpdsub"]

                    result["kode_skpdunit"] = user["kode_skpdunit"]
                    if result["kode_skpdunit"] == "":
                        result["nama_skpdunit"] = ""
                    else:
                        result["nama_skpdunit"] = user["nama_skpdunit"]

                    result["cdate"] = user["cdate"]
                    result["last_login"] = user["last_login"]
                    result["is_deleted"] = user["is_deleted"]
                    result["is_external"] = user["is_external"]

                    result2 = {}
                    result2["id"] = str(user["role_id"])
                    result2["name"] = str(user["role_name"])
                    result3 = {}
                    result3["id"] = str(user["app_id"])
                    result3["name"] = str(user["app_name"])
                    result3["code"] = str(user["app_code"])
                    result3["url"] = str(user["app_url"])
                    result["app"] = result3
                    result["role"] = result2
                    result["jwt"] = str(jwt)
                    result["agreement_id"] = user['agreement_id']

                    if result['agreement_id']:
                        result_agreement = db.session.query(AgreementModel)
                        result_agreement = result_agreement.get(result['agreement_id'])
                        if result_agreement:
                            result_agreement = result_agreement.__dict__
                            if result_agreement['is_active']:
                                result['is_agree'] = True
                            else:
                                result['is_agree'] = False
                        else:
                            result['is_agree'] = False
                    else:
                        result['is_agree'] = False
                        
                    set_user({"id": user["id"], "email": user["email"], "username": user["username"]})

                    return True, result, ""

                    # else:
                    #     return False, {}, "Role not found"
                else:
                    return False, {}, "Password not match"
            else:
                return False, {}, "Username or App Code not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def login2(self, json_data: dict):
        ''' Doc: function init  '''
        try:
            # query postgresql
            if "username" not in json_data:
                raise ErrorMessage("Username is missing", 500, 1, {})

            if "password" not in json_data:
                raise ErrorMessage("Password is missing", 500, 1, {})

            # query
            result = db.session.query(
                UserModel.id, UserModel.email, UserModel.username, UserModel.password, UserModel.nip,
                UserModel.name, UserModel.profile_pic, UserModel.notes, UserModel.last_login,
                UserModel.cdate, UserModel.cuid, UserModel.is_active, UserModel.is_deleted,
                UserModel.profile_pic, SkpdModel.logo, UserModel.agreement_id,
                UserModel.satuan_kerja_id, UserModel.lv1_unit_kerja_id, UserModel.lv2_unit_kerja_id, UserModel.lv3_unit_kerja_id, UserModel.lv4_unit_kerja_id,
                UserModel.level_unit_kerja, UserModel.jabatan_id,
                UserModel.role_id, RoleModel.name.label('role_name'), RoleModel.title.label('role_title'),
                RegionalLevelModel.name.label('regional_level'), UserModel.kode_kemendagri, RegionalModel.nama_kemendagri,
                UserModel.kode_skpd, SkpdModel.nama_skpd, SkpdModel.is_external, UserModel.dash_role_id
            )
            result = result.outerjoin(RoleModel, UserModel.role_id == RoleModel.id)
            result = result.outerjoin(RegionalModel, UserModel.kode_kemendagri == RegionalModel.kode_kemendagri)
            result = result.outerjoin(RegionalLevelModel, RegionalModel.regional_level_id == RegionalLevelModel.id)
            result = result.outerjoin(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.filter(UserModel.username == json_data['username'])
            result = result.filter(UserModel.is_deleted == False)
            result = result.filter(UserModel.is_active == True)
            result = result.all()

            user = {}
            for res in result:
                user['id'] = res[0]
                user['email'] = res[1]
                user['username'] = res[2]
                user['password'] = res[3]
                user['nip'] = res[4]
                user['name'] = res[5]
                user['profile_pic'] = res[6]
                user['notes'] = ""
                if res[8]:
                    user['last_login'] = res[8].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    user['last_login'] = ""
                if res[9]:
                    user['cdate'] = res[9].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    user['cdate'] = ""
                user['cuid'] = res[10]
                user['is_active'] = res[11]
                user['is_deleted'] = res[12]
                user['profile_pic'] = res[13]
                user['logo_skpd'] = res[14]
                user['agreement_id'] = res[15]

                user['satuan_kerja_id'] = res[16]
                user['lv1_unit_kerja_id'] = res[17]
                user['lv2_unit_kerja_id'] = res[18]
                user['lv3_unit_kerja_id'] = res[19]
                user['lv4_unit_kerja_id'] = res[20]
                user['level_unit_kerja'] = res[21]
                user['jabatan_id'] = res[22]

                user['role_id'] = res[23]
                user['role_name'] = res[24]
                user['role_title'] = res[25]
                user['regional_level'] = res[26]
                user['kode_kemendagri'] = res[27]
                user['nama_kemendagri'] = res[28]
                user['role'] = {
                    'id': res[23],
                    'name': res[24],
                    'title': res[25]
                }
                user['kode_skpd'] = res[29]
                user['nama_skpd'] = res[30]
                user['is_external'] = res[31]

                # struktur organisasi
                if user['jabatan_id']:
                    # cek count
                    count_struktur = db.session.query(StrukturOrganisasiModel)
                    count_struktur = count_struktur.filter(getattr(StrukturOrganisasiModel, "jabatan_id") == user['jabatan_id'])
                    count_struktur = count_struktur.count()

                    if count_struktur > 0:
                        # get relation struktur organisasi
                        data_struktur = db.session.query(StrukturOrganisasiModel)
                        data_struktur = data_struktur.filter(getattr(StrukturOrganisasiModel, "jabatan_id") == user['jabatan_id'])
                        data_struktur = data_struktur.limit(1)
                        data_struktur = data_struktur.all()
                        if data_struktur:
                            ds_ = data_struktur[0].__dict__
                            user['satuan_kerja_nama'] = ds_['satuan_kerja_nama']
                            user['lv1_unit_kerja_nama'] = ds_['lv1_unit_kerja_nama']
                            user['lv2_unit_kerja_nama'] = ds_['lv2_unit_kerja_nama']
                            user['lv3_unit_kerja_nama'] = ds_['lv3_unit_kerja_nama']
                            user['lv4_unit_kerja_nama'] = ds_['lv4_unit_kerja_nama']
                            user['jabatan_nama'] = ds_['jabatan_nama']
                        else:
                            user['satuan_kerja_nama'] = ''
                            user['lv1_unit_kerja_nama'] = ''
                            user['lv2_unit_kerja_nama'] = ''
                            user['lv3_unit_kerja_nama'] = ''
                            user['lv4_unit_kerja_nama'] = ''
                            user['jabatan_nama'] = ''

                    else:
                        # get relation jabatan
                        data_jabatan = db.session.query(JabatanModel)
                        data_jabatan = data_jabatan.filter(getattr(JabatanModel, "jabatan_id") == user['jabatan_id'])
                        data_jabatan = data_jabatan.limit(1)
                        data_jabatan = data_jabatan.all()
                        if data_jabatan:
                            dj_ = data_jabatan[0].__dict__
                            user['satuan_kerja_nama'] = dj_['satuan_kerja_nama']
                            user['jabatan_nama'] = dj_['jabatan_nama']
                        else:
                            user['satuan_kerja_nama'] = ''
                            user['jabatan_nama'] = ''

                        # get relation lv1_unit_kerja
                        if user['lv1_unit_kerja_id']:
                            data_lv1 = db.session.query(JabatanModel)
                            data_lv1 = data_lv1.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv1_unit_kerja_id'])
                            data_lv1 = data_lv1.limit(1)
                            data_lv1 = data_lv1.all()
                            if data_lv1:
                                dl1_ = data_lv1[0].__dict__
                                user['lv1_unit_kerja_nama'] = dl1_['unit_kerja_nama']
                            else:
                                user['lv1_unit_kerja_nama'] = ''
                        else:
                            user['lv1_unit_kerja_nama'] = ''

                        # get relation lv2_unit_kerja
                        if user['lv2_unit_kerja_id']:
                            data_lv2 = db.session.query(JabatanModel)
                            data_lv2 = data_lv2.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv2_unit_kerja_id'])
                            data_lv2 = data_lv2.limit(1)
                            data_lv2 = data_lv2.all()
                            if data_lv2:
                                dl2_ = data_lv2[0].__dict__
                                user['lv2_unit_kerja_nama'] = dl2_['unit_kerja_nama']
                            else:
                                user['lv2_unit_kerja_nama'] = ''
                        else:
                            user['lv2_unit_kerja_nama'] = ''

                        # get relation lv3_unit_kerja
                        if user['lv3_unit_kerja_id']:
                            data_lv3 = db.session.query(JabatanModel)
                            data_lv3 = data_lv3.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv3_unit_kerja_id'])
                            data_lv3 = data_lv3.limit(1)
                            data_lv3 = data_lv3.all()
                            if data_lv3:
                                dl3_ = data_lv3[0].__dict__
                                user['lv3_unit_kerja_nama'] = dl3_['unit_kerja_nama']
                            else:
                                user['lv3_unit_kerja_nama'] = ''
                        else:
                            user['lv3_unit_kerja_nama'] = ''

                        # get relation lv4_unit_kerja
                        if user['lv4_unit_kerja_id']:
                            data_lv4 = db.session.query(JabatanModel)
                            data_lv4 = data_lv4.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv4_unit_kerja_id'])
                            data_lv4 = data_lv4.limit(1)
                            data_lv4 = data_lv4.all()
                            if data_lv4:
                                dl4_ = data_lv4[0].__dict__
                                user['lv4_unit_kerja_nama'] = dl4_['unit_kerja_nama']
                            else:
                                user['lv4_unit_kerja_nama'] = ''
                        else:
                            user['lv4_unit_kerja_nama'] = ''

                else:
                    user['satuan_kerja_nama'] = ''
                    user['lv1_unit_kerja_nama'] = ''
                    user['lv2_unit_kerja_nama'] = ''
                    user['lv3_unit_kerja_nama'] = ''
                    user['lv4_unit_kerja_nama'] = ''
                    user['jabatan_nama'] = ''

                # role dashboard
                user['role_dashboard'] = {
                    'id': None,
                    'name': None,
                    'title': None,
                }
                user['permission_dashboard'] = []
                if res[32]:
                    # role
                    dashboard_role = db.session.query(DashboardRoleModel).get(res[32])
                    if dashboard_role:
                        dashboard_role = dashboard_role.__dict__
                        dashboard_role.pop('_sa_instance_state', None)
                        user['role_dashboard'] = {
                            'id': res[32],
                            'name': dashboard_role['name'],
                            'title': dashboard_role['title'],
                        }

                    # role_permission
                    dashboard_role_permission = db.session.query(
                        sqlalchemy.distinct(DashboardRolePermissionModel.id).label('id'),
                        DashboardRolePermissionModel.role_id, DashboardRoleModel.name.label('role_name'), DashboardRoleModel.title.label('role_title'),
                        DashboardRolePermissionModel.app_id, DashboardAppModel.name.label('app_name'), DashboardAppModel.title.label('app_title'), DashboardAppModel.url.label('app_url'), DashboardAppModel.picture.label('app_picture'),
                        DashboardRolePermissionModel.app_service_id, DashboardAppServiceModel.name.label('app_service_name'), DashboardAppServiceModel.controller.label('app_service_controller'),
                        DashboardRolePermissionModel.enable, DashboardRolePermissionModel.notes, DashboardRolePermissionModel.cuid, DashboardRolePermissionModel.cdate,
                    )
                    dashboard_role_permission = dashboard_role_permission.join(DashboardRoleModel, DashboardRoleModel.id == DashboardRolePermissionModel.role_id)
                    dashboard_role_permission = dashboard_role_permission.join(DashboardAppModel, DashboardAppModel.id == DashboardRolePermissionModel.app_id)
                    dashboard_role_permission = dashboard_role_permission.join(DashboardAppServiceModel, DashboardAppServiceModel.id == DashboardRolePermissionModel.app_service_id)
                    dashboard_role_permission = dashboard_role_permission.filter(DashboardRolePermissionModel.role_id == res[32])
                    dashboard_role_permission = dashboard_role_permission.all()
                    for drp in dashboard_role_permission:
                        temp = drp._asdict()
                        temp.pop('_sa_instance_state', None)
                        temp['cdate'] = temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")
                        user['permission_dashboard'].append(temp)


            if user:
                # check password
                user_pass_ok = HELPER.verify_hash(json_data["password"], user["password"])
                del user['password']

                if user_pass_ok:
                    # updade last login
                    where = {'id': user['id']}
                    update_last_login = {'last_login': HELPER.local_date_server()}
                    result_update = db.session.query(UserModel)
                    result_update = result_update.filter(getattr(UserModel, 'id') == user['id'])
                    result_update = result_update.update(update_last_login, synchronize_session='fetch')
                    result_update = db.session.commit()

                    user_to_jwt = user.copy()
                    del user_to_jwt['permission_dashboard']

                    # generate token
                    jwt = HELPER.jwt_encode(user_to_jwt)
                    result = user
                    result["jwt"] = str(jwt)

                    # check aggrement
                    if result['agreement_id']:
                        result_agreement = db.session.query(AgreementModel)
                        result_agreement = result_agreement.get(result['agreement_id'])
                        if result_agreement:
                            result_agreement = result_agreement.__dict__
                            if result_agreement['is_active']:
                                result['is_agree'] = True
                            else:
                                result['is_agree'] = False
                        else:
                            result['is_agree'] = False
                    else:
                        result['is_agree'] = False
                        
                    set_user({"id": user["id"], "email": user["email"], "username": user["username"]})

                    return True, result, "Login"

                else:
                    return False, {}, "Username or Password not found. (2)"
            else:
                return False, {}, "Username or Password not found. (1)"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def autologin(self, username: string):
        ''' Doc: function init  '''
        try:
            # query postgresql
            if not username:
                raise ErrorMessage("Username is missing", 500, 1, {})

            # query
            result = db.session.query(
                UserModel.id, UserModel.email, UserModel.username, UserModel.password, UserModel.nip,
                UserModel.name, UserModel.profile_pic, UserModel.notes, UserModel.last_login,
                UserModel.cdate, UserModel.cuid, UserModel.is_active, UserModel.is_deleted,
                UserModel.profile_pic, SkpdModel.logo, UserModel.agreement_id,
                UserModel.satuan_kerja_id, UserModel.lv1_unit_kerja_id, UserModel.lv2_unit_kerja_id, UserModel.lv3_unit_kerja_id, UserModel.lv4_unit_kerja_id,
                UserModel.level_unit_kerja, UserModel.jabatan_id,
                UserModel.role_id, RoleModel.name.label('role_name'), RoleModel.title.label('role_title'),
                RegionalLevelModel.name.label('regional_level'), UserModel.kode_kemendagri, RegionalModel.nama_kemendagri,
                UserModel.kode_skpd, SkpdModel.nama_skpd, SkpdModel.is_external,
            )
            result = result.outerjoin(RoleModel, UserModel.role_id == RoleModel.id)
            result = result.outerjoin(RegionalModel, UserModel.kode_kemendagri == RegionalModel.kode_kemendagri)
            result = result.outerjoin(RegionalLevelModel, RegionalModel.regional_level_id == RegionalLevelModel.id)
            result = result.outerjoin(SkpdModel, UserModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.filter(UserModel.username == username)
            result = result.filter(UserModel.is_deleted == False)
            result = result.filter(UserModel.is_active == True)
            result = result.all()

            user = {}
            for res in result:
                user['id'] = res[0]
                user['email'] = res[1]
                user['username'] = res[2]
                user['password'] = ''
                user['nip'] = res[4]
                user['name'] = res[5]
                user['profile_pic'] = res[6]
                user['notes'] = ""
                if res[8]:
                    user['last_login'] = res[8].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    user['last_login'] = ""
                if res[9]:
                    user['cdate'] = res[9].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    user['cdate'] = ""
                user['cuid'] = res[10]
                user['is_active'] = res[11]
                user['is_deleted'] = res[12]
                user['profile_pic'] = res[13]
                user['logo_skpd'] = res[14]
                user['agreement_id'] = res[15]

                user['satuan_kerja_id'] = res[16]
                user['lv1_unit_kerja_id'] = res[17]
                user['lv2_unit_kerja_id'] = res[18]
                user['lv3_unit_kerja_id'] = res[19]
                user['lv4_unit_kerja_id'] = res[20]
                user['level_unit_kerja'] = res[21]
                user['jabatan_id'] = res[22]

                user['role_id'] = res[23]
                user['role_name'] = res[24]
                user['role_title'] = res[25]
                user['regional_level'] = res[26]
                user['kode_kemendagri'] = res[27]
                user['nama_kemendagri'] = res[28]
                user['role'] = {
                    'id': res[23],
                    'name': res[24],
                    'title': res[25]
                }
                user['kode_skpd'] = res[29]
                user['nama_skpd'] = res[30]
                user['is_external'] = res[31]

                # struktur organisasi
                if user['jabatan_id']:
                    # cek count
                    count_struktur = db.session.query(StrukturOrganisasiModel)
                    count_struktur = count_struktur.filter(getattr(StrukturOrganisasiModel, "jabatan_id") == user['jabatan_id'])
                    count_struktur = count_struktur.count()

                    if count_struktur > 0:
                        # get relation struktur organisasi
                        data_struktur = db.session.query(StrukturOrganisasiModel)
                        data_struktur = data_struktur.filter(getattr(StrukturOrganisasiModel, "jabatan_id") == user['jabatan_id'])
                        data_struktur = data_struktur.limit(1)
                        data_struktur = data_struktur.all()
                        if data_struktur:
                            ds_ = data_struktur[0].__dict__
                            user['satuan_kerja_nama'] = ds_['satuan_kerja_nama']
                            user['lv1_unit_kerja_nama'] = ds_['lv1_unit_kerja_nama']
                            user['lv2_unit_kerja_nama'] = ds_['lv2_unit_kerja_nama']
                            user['lv3_unit_kerja_nama'] = ds_['lv3_unit_kerja_nama']
                            user['lv4_unit_kerja_nama'] = ds_['lv4_unit_kerja_nama']
                            user['jabatan_nama'] = ds_['jabatan_nama']
                        else:
                            user['satuan_kerja_nama'] = ''
                            user['lv1_unit_kerja_nama'] = ''
                            user['lv2_unit_kerja_nama'] = ''
                            user['lv3_unit_kerja_nama'] = ''
                            user['lv4_unit_kerja_nama'] = ''
                            user['jabatan_nama'] = ''

                    else:
                        # get relation jabatan
                        data_jabatan = db.session.query(JabatanModel)
                        data_jabatan = data_jabatan.filter(getattr(JabatanModel, "jabatan_id") == user['jabatan_id'])
                        data_jabatan = data_jabatan.limit(1)
                        data_jabatan = data_jabatan.all()
                        if data_jabatan:
                            dj_ = data_jabatan[0].__dict__
                            user['satuan_kerja_nama'] = dj_['satuan_kerja_nama']
                            user['jabatan_nama'] = dj_['jabatan_nama']
                        else:
                            user['satuan_kerja_nama'] = ''
                            user['jabatan_nama'] = ''

                        # get relation lv1_unit_kerja
                        if user['lv1_unit_kerja_id']:
                            data_lv1 = db.session.query(JabatanModel)
                            data_lv1 = data_lv1.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv1_unit_kerja_id'])
                            data_lv1 = data_lv1.limit(1)
                            data_lv1 = data_lv1.all()
                            if data_lv1:
                                dl1_ = data_lv1[0].__dict__
                                user['lv1_unit_kerja_nama'] = dl1_['unit_kerja_nama']
                            else:
                                user['lv1_unit_kerja_nama'] = ''
                        else:
                            user['lv1_unit_kerja_nama'] = ''

                        # get relation lv2_unit_kerja
                        if user['lv2_unit_kerja_id']:
                            data_lv2 = db.session.query(JabatanModel)
                            data_lv2 = data_lv2.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv2_unit_kerja_id'])
                            data_lv2 = data_lv2.limit(1)
                            data_lv2 = data_lv2.all()
                            if data_lv2:
                                dl2_ = data_lv2[0].__dict__
                                user['lv2_unit_kerja_nama'] = dl2_['unit_kerja_nama']
                            else:
                                user['lv2_unit_kerja_nama'] = ''
                        else:
                            user['lv2_unit_kerja_nama'] = ''

                        # get relation lv3_unit_kerja
                        if user['lv3_unit_kerja_id']:
                            data_lv3 = db.session.query(JabatanModel)
                            data_lv3 = data_lv3.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv3_unit_kerja_id'])
                            data_lv3 = data_lv3.limit(1)
                            data_lv3 = data_lv3.all()
                            if data_lv3:
                                dl3_ = data_lv3[0].__dict__
                                user['lv3_unit_kerja_nama'] = dl3_['unit_kerja_nama']
                            else:
                                user['lv3_unit_kerja_nama'] = ''
                        else:
                            user['lv3_unit_kerja_nama'] = ''

                        # get relation lv4_unit_kerja
                        if user['lv4_unit_kerja_id']:
                            data_lv4 = db.session.query(JabatanModel)
                            data_lv4 = data_lv4.filter(getattr(JabatanModel, "unit_kerja_id") == user['lv4_unit_kerja_id'])
                            data_lv4 = data_lv4.limit(1)
                            data_lv4 = data_lv4.all()
                            if data_lv4:
                                dl4_ = data_lv4[0].__dict__
                                user['lv4_unit_kerja_nama'] = dl4_['unit_kerja_nama']
                            else:
                                user['lv4_unit_kerja_nama'] = ''
                        else:
                            user['lv4_unit_kerja_nama'] = ''

                else:
                    user['satuan_kerja_nama'] = ''
                    user['lv1_unit_kerja_nama'] = ''
                    user['lv2_unit_kerja_nama'] = ''
                    user['lv3_unit_kerja_nama'] = ''
                    user['lv4_unit_kerja_nama'] = ''
                    user['jabatan_nama'] = ''

            if user:
                # updade last login
                where = {'id': user['id']}
                update_last_login = {'last_login': HELPER.local_date_server()}
                result_update = db.session.query(UserModel)
                result_update = result_update.filter(getattr(UserModel, 'id') == user['id'])
                result_update = result_update.update(update_last_login, synchronize_session='fetch')
                result_update = db.session.commit()

                # generate token
                jwt = HELPER.jwt_encode(user)
                result = user
                result["jwt"] = str(jwt)

                # check aggrement
                if result['agreement_id']:
                    result_agreement = db.session.query(AgreementModel)
                    result_agreement = result_agreement.get(result['agreement_id'])
                    if result_agreement:
                        result_agreement = result_agreement.__dict__
                        if result_agreement['is_active']:
                            result['is_agree'] = True
                        else:
                            result['is_agree'] = False
                    else:
                        result['is_agree'] = False
                else:
                    result['is_agree'] = False
                    
                set_user({"id": user["id"], "email": user["email"], "username": user["username"]})

                return True, result, "Login"

            else:
                return False, {}, "Username or Password not found. (1)"

        except Exception as err:
            # fail response
            print(err)
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def reset_password(self, json_request):
        ''' Doc: function init  '''
        try:
            result = db.session.query(UserModel)
            result = result.filter(UserModel.username == json_request['username'])
            result = result.filter(UserModel.email == json_request['email'])

            user = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)
                user.append(res['id'])

            if user:
                random_string = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
                USER_CONTROLLER.update({'id':user[0]}, {'password':random_string})

                email = [json_request['email']]
                html = '<body style="background-color: #e9ecef;"><br><br><br><br>'
                html += '<table border="0" cellpadding="0" cellspacing="0" width="100%">'
                html += '<tr><td align="center" bgcolor="#e9ecef"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;"><tr>'
                html += '<td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; border-top: 3px solid #d4dadf;">'
                html += '<h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">Reset Your Password</h1></td></tr></table></td></tr>'
                html += '<tr><td align="center" bgcolor="#e9ecef"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">'
                html += '<tr><td align="left" bgcolor="#ffffff" style="padding: 24px; font-size: 16px; line-height: 24px;">'
                html += '<p style="margin: 0;margin-bottom: -20px;">Berikut adalah password sementara yang telah digenerate oleh system. '
                html += '<strong>'+random_string+'</strong></p></td></tr>'
                html += '<tr><td align="left" bgcolor="#ffffff" style="padding: 24px; font-size: 16px; line-height: 24px;"><p style="margin: 0;"> Segera lakukan perubahan password setelah melakukan Login, <br> dengan cara : <br>1. Masuk ke User Profile<br>2. Klik tombol Ubah Password<br>3. Isi Form berisi Password Lama, Password Baru dan Ulangi Password Baru<br>4. Lakukan Submit</p></td></tr>'
                html += '</table></td></tr></table></td></tr><tr><td align="center" bgcolor="#e9ecef" style="padding: 24px;"></td></tr></table></body></html>'

                # normal send mail
                send = HELPER.send_email("User Reset Password", email, html)
                return True, send

                # using request service
                # data_mail = {
                #     "subject": "User Reset Password",
                #     "email": email,
                #     "message": html
                # }
                # req_ = requests.post(
                #     conf.REQUEST_URL + 'mail',
                #     headers={"Content-Type": conf.MESSAGE_APP_JSON},
                #     data=json.dumps(data_mail)
                # )
                # resp_ = req_.json()['data']
                #
                # return True, resp_
            else:
                return False, "Username and Email Belum Sesuai."

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def insert_login_attempt(self, ip, username, browser):
        try:
            json = {
                'datetime': HELPER.local_date_server(),
                'ip': ip,
                'username':  username,
                'browser': browser
            }
            result = LoginAttemptModel(**json)
            db.session.add(result)
            db.session.commit()

            return result.to_dict()

        except Exception as err:
            print(str(err))
            return {}

    def check_block_username(self, username):
        try:
            now = datetime.datetime.now(timezone('Asia/Jakarta')).strftime('%Y-%m-%d %H:%M:%S')
            ago = (datetime.datetime.now(timezone('Asia/Jakarta')) - timedelta(minutes=5)).strftime('%Y-%m-%d %H:%M:%S')

            result = db.session.query(LoginAttemptModel)
            result = result.filter(LoginAttemptModel.username == username)
            result = result.filter(LoginAttemptModel.datetime >= ago)
            result = result.filter(LoginAttemptModel.datetime <= now)
            result = result.count()

            if result >= 3:
                return False, result
            else:
                return True, result

        except Exception as err:
            # fail response
            print(str(err))
            return False, None

    def check_block_ip(self, ip):
        try:
            now = datetime.datetime.now(timezone('Asia/Jakarta')).strftime('%Y-%m-%d %H:%M:%S')
            ago = (datetime.datetime.now(timezone('Asia/Jakarta')) - timedelta(minutes=5)).strftime('%Y-%m-%d %H:%M:%S')

            result = db.session.query(LoginAttemptModel)
            result = result.filter(LoginAttemptModel.ip == ip)
            result = result.filter(LoginAttemptModel.datetime >= ago)
            result = result.filter(LoginAttemptModel.datetime <= now)
            result = result.count()

            if result >= 5:
                return False, result
            else:
                return True, result

        except Exception as err:
            # fail response
            print(str(err))
            return False, None

    def check_recaptcha(self, json_data: dict):
        try:
            if 'app_code' in json_data:
                secretkey = ''
                if json_data['app_code'] == 'satudata-frontend':
                    secretkey = conf.RECAPTCHA_SATUDATA_SECRETKEY
                elif json_data['app_code'] == 'admin-frontend':
                    secretkey = conf.RECAPTCHA_ADMIN_SECRETKEY

                if 'token' in json_data:
                    url = "https://www.google.com/recaptcha/api/siteverify"
                    payload = {
                        'secret': secretkey,
                        'response': json_data['token']
                    }
                    headers = {}

                    request = requests.post(url, headers=headers, data=payload)
                    response = request.json()

                    if 'success' in response:
                        if response['success'] == True :
                            return True, 'Response reCaptcha ok'
                        else:
                            return False, 'Response reCaptcha failed, ' + ' '.join(response['error-codes'])
                    else:
                        return False, 'Response reCaptcha error.'
                else:
                    return False, 'reCaptcha token not found in body.'
            else:
                return False, 'app_code not found in body.'
        except Exception as err:
            print(str(err))
            return False, str(err)
