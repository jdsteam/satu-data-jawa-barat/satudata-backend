''' Doc: controller history request private  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryRequestPrivateModel
from models import UserModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk
import json
import requests
from settings import configuration as conf
from controllers.notification import NotificationController
from controllers.user import UserController

NOTIFICATION_CONTROLLER = NotificationController()
USER_CONTROLLER = UserController()
CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
MSESSAGE_NO_TIKET = "Nomor tiket : "
MESSAGE_PERMOHONAN_DATA = "Permohonan data terkait <strong>"

# pylint: disable=singleton-comparison, unused-variable, unused-argument
class HistoryRequestPrivateController(object):
    ''' Doc: class history request private  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(HistoryRequestPrivateModel)

            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryRequestPrivateModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryRequestPrivateModel, attr) == value)

            if search.lower() == 'persetujuan walidata':
                categorys = '1'
            elif search.lower() == 'dataset sedang diproses':
                categorys = '2'
            elif search.lower() == 'dataset tersedia':
                categorys = '4'
            elif search.lower() == 'dataset ditolak':
                categorys = '3'
            else:
                categorys = search

            result = result.filter(or_(
                cast(getattr(HistoryRequestPrivateModel, "status"), sqlalchemy.String).ilike('%'+categorys+'%'),
                cast(getattr(HistoryRequestPrivateModel, "notes"), sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("history_request_private."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_request = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})

                # populate user
                user = db.session.query(UserModel)
                user = user.get(temp['cuid'])
                if user:
                    user = user.__dict__
                    temp['username'] = user['username']
                    temp['name'] = user['name']
                else:
                    temp['username'] = ''
                    temp['name'] = ''

                history_request.append(temp)

            # check if empty
            if history_request:
                return True, history_request
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # check if empty
            if result:
                return True, result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            result = self.query({'id':_id}, '')
            result = result.all()

            # change into dict
            history_request = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})

                # populate user
                user = db.session.query(UserModel)
                user = user.get(temp['cuid'])
                if user:
                    user = user.__dict__
                    temp['username'] = user['username']
                    temp['name'] = user['name']
                else:
                    temp['username'] = ''
                    temp['name'] = ''

                history_request.append(temp)

            # check if empty
            if history_request:
                return True, history_request[0], "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, _json, condition):
        ''' Doc: function post param  '''
        try:
            # generate json data
            json_send = {}
            json_send = _json
            json_send["datetime"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = self.post_param(_json, 'add')
            # prepare data model
            result = HistoryRequestPrivateModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history_request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, history_request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = self.post_param(_json, 'edit')
            try:
                # prepare data model
                result = db.session.query(HistoryRequestPrivateModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryRequestPrivateModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history_request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history_request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryRequestPrivateModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryRequestPrivateModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history_request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def prepare_notification(self, history_request, json_notification, titles):
        ''' Doc: function prepare_notification  '''
        if history_request['status'] == '2' or history_request['status'] == 2:
            json_notification['title'] = MESSAGE_PERMOHONAN_DATA + titles + "</strong> anda sedang diproses. "
            json_notification['content'] = MSESSAGE_NO_TIKET + str(history_request['request_private_id'])
        elif history_request['status'] == '3' or history_request['status'] == 3:
            json_notification['title'] = MESSAGE_PERMOHONAN_DATA + titles + "</strong> telah diakomodir, "
            json_notification['title'] += "lakukan pengecekan data pada Katalog Dataset atau Profil Organisasi. "
            json_notification['content'] = MSESSAGE_NO_TIKET + str(history_request['request_private_id'])
        elif history_request['status'] == '4' or history_request['status'] == 4:
            json_notification['title'] = MESSAGE_PERMOHONAN_DATA + titles
            json_notification['title'] += "</strong> anda telah ditolak oleh Walidata. "
            json_notification['content'] = MSESSAGE_NO_TIKET + str(history_request['request_private_id'])

        result, notification = NOTIFICATION_CONTROLLER.create(json_notification)
        return result, notification

    def send_notification(self, user_id, notification, request):
        ''' Doc: function send_notification  '''
        param = {}
        param['count_notif'] = 0
        res_, user = USER_CONTROLLER.get_by_id(user_id)
        if res_:
            if not user['count_notif']:
                user['count_notif'] = 0
            param['count_notif'] = user['count_notif'] + 1
            # execute database
            result = db.session.query(UserModel)
            result = result.filter(getattr(UserModel, 'id') == user_id)
            result = result.update(param, synchronize_session='fetch')
            result = db.session.commit()

        # try:
        #     socket_url = conf.SOCKET_URL
        #     token = request.headers.get('Authorization', None)
        #     requests.get(
        #         socket_url + 'notification' + '?receiver=' + str(notification['receiver']),
        #         headers={"Authorization":token, "Content-Type": "application/json"},
        #         data=json.dumps(notification)
        #     )
        # except Exception as err:
        #     print(str(err))

    def get_download(self, start_date, end_date):
        ''' Doc: function get download  '''
        try:
            # query postgresql
            if start_date:
                cdate_start_date = "WHERE cdate::date >= '" + start_date + "'"
            else:
                cdate_start_date = ""
            if end_date:
                cdate_end_date = " and cdate::date <= '" + end_date + "'"
            else:
                cdate_end_date = ""

            # change into dict
            sql = text('SELECT ticket, nama, email, telp, '+
            'satuan_kerja_nama, lv1_unit_kerja_nama, lv2_unit_kerja_nama, lv3_unit_kerja_nama, lv4_unit_kerja_nama,' +
            'judul_data, tau_skpd, nama_skpd_tujuan, kebutuhan_data, tujuan_data, ' +
            'status, cdate, mdate, notes '+
            'FROM request_private_view '+ cdate_start_date + ' ' + cdate_end_date

            )
            result = db.engine.execute(sql)

            # check if empty
            request_private = []
            for res in result:
                temps = {}
                temps['Nomor Tiket'] = res['ticket']
                temps['Nama Pemohon'] = res['nama']
                temps['Email'] = res['email']
                temps['Telepon'] = res['telp']
                temps['Organisasi Perangkat Daerah Pemohon'] = res['satuan_kerja_nama']
                temps['Unit Kerja Level 1 Pemohon'] = res['lv1_unit_kerja_nama']
                temps['Unit Kerja Level 2 Pemohon'] = res['lv2_unit_kerja_nama']
                temps['Unit Kerja Level 3 Pemohon'] = res['lv3_unit_kerja_nama']
                temps['Unit Kerja Level 4 Pemohon'] = res['lv4_unit_kerja_nama']
                temps['Judul Data'] = res['judul_data']
                temps['Mengetahui OPD Sumber'] = res['tau_skpd']
                temps['Organisasi Sumber Data'] = res['nama_skpd_tujuan']
                temps['Deskripsi Kebutuhan Dataset'] = res['kebutuhan_data']
                temps['Tujuan Dataset'] = res['tujuan_data']
                temps['Tanggal Dibuat'] = res['cdate'].strftime(FORMAT_DATE)
                temps['Tanggal Diperbarui'] = res['mdate'].strftime(FORMAT_DATE)
                temps['Catatan Terakhir'] = res['notes']
                temps['Status'] = res['status']

                request_private.append(temps)

            # check if empty
            request_private = list(request_private)
            if request_private:
                return True, request_private
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

