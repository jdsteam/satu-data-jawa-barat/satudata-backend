''' Doc: controller dataset type  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import DatasetTypeModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison, unused-variable
class DatasetTypeController(object):
    ''' Doc: class dataset type  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(DatasetTypeModel)
            for attr, value in where.items():
                result = result.filter(getattr(DatasetTypeModel, attr) == value)
            result = result.filter(or_(cast(getattr(DatasetTypeModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in DatasetTypeModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            dataset_type = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                dataset_type.append(temp)

            # check if empty
            dataset_type = list(dataset_type)
            if dataset_type:
                return True, dataset_type
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(DatasetTypeModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(DatasetTypeModel, attr) == value)
            result = result.filter(or_(cast(getattr(DatasetTypeModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in DatasetTypeModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(DatasetTypeModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            dataset_type = result

            # check if empty
            if dataset_type:
                return True, dataset_type
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            # prepare data model
            result = DatasetTypeModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, dataset_type = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, dataset_type
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(DatasetTypeModel)
                for attr, value in where.items():
                    result = result.filter(getattr(DatasetTypeModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, dataset_type = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, dataset_type
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(DatasetTypeModel)
                for attr, value in where.items():
                    result = result.filter(getattr(DatasetTypeModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, dataset_type = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
