''' Doc: controller infographic  '''
import datetime
from helpers import Helper
from helpers.sql_global import SqlGlobal
from exceptions import ErrorMessage
from models import InfographicModel, InfographicDetailModel
from models import SektoralModel, DatasetClassModel, SkpdModel, HighlightModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast, Date
from sqlalchemy.orm import defer
import json
import sqlalchemy, sentry_sdk
import re
from datetime import datetime
from pytz import timezone

current_time = datetime.now(timezone('Asia/Jakarta'))

HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"
TODAY = HELPER.local_date_server()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long, too-many-function-args
class InfographicController(object):
    ''' Doc: class infographic  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def refresh(self):
        try:
            sql_query = "REFRESH MATERIALIZED VIEW suggestion_infographic"
            sql = text(sql_query)
            db.engine.execute(sql)
        except Exception as err:
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(InfographicModel)
            result = result.join(SektoralModel, InfographicModel.sektoral_id == SektoralModel.id)
            result = result.join(DatasetClassModel, InfographicModel.dataset_class_id == DatasetClassModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(InfographicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(InfographicModel, attr) == value)

            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(SektoralModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(InfographicModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(InfographicModel, "description"), sqlalchemy.String).match(src),
                    ))

            return result

        except Exception as err:
            return str(err)

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            result = self.query(where, search)
            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(InfographicModel.dataset_class_id == 3)
                result = result.filter(InfographicModel.is_deleted == False)
                result = result.filter(InfographicModel.is_active == True)
                result = result.filter(InfographicModel.mdate <= "'" + TODAY + "'")
            result = result.order_by(text("infographic.owner desc"))
            result = result.order_by(text("infographic."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            infographic = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATETIME)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATETIME)

                # get relation to keywords
                temp = self.populate_keywords(temp)

                # get relation to dataset_class
                temp = self.populate_dataset_class(temp)

                # get relation to sektoral
                temp = self.populate_sektoral(temp)

                # get relation to license
                temp = self.populate_license(temp)

                # get relation to regional
                temp = self.populate_regional(temp)

                # get relation to infographic_detail
                temp = self.populate_infographic_detail(temp)

                infographic.append(temp)

            # check if empty
            if infographic:
                return True, infographic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_keywords(self, temp):
        ''' Doc: function populate_keywords  '''
        if temp['keywords']:
            if isinstance(json.loads(temp['keywords']), list):
                temp['keywords'] = json.loads(temp['keywords'])
            else:
                temp['keywords'] = json.loads(json.loads(temp['keywords']))
        else:
            temp['keywords'] = []

        return temp

    def populate_dataset_class(self, temp):
        ''' Doc: function populate_dataset_class  '''
        dataset_class = SqlGlobal.dataset_class(temp['dataset_class_id'])

        if dataset_class:
            dataset_class = dataset_class.__dict__
            dataset_class.pop('_sa_instance_state', None)
        else:
            dataset_class = {}

        temp['dataset_class'] = dataset_class
        temp.pop('dataset_class_id', None)

        return temp

    def populate_sektoral(self, temp):
        ''' Doc: function populate_sektoral  '''
        sektoral = SqlGlobal.sektoral(temp['sektoral_id'])

        if sektoral:
            sektoral = sektoral.__dict__
            sektoral.pop('_sa_instance_state', None)
        else:
            sektoral = {}

        temp['sektoral'] = sektoral
        temp.pop('sektoral_id', None)

        return temp

    def populate_license(self, temp):
        ''' Doc: function populate_license  '''
        if temp['license_id']:
            # get relation to license
            licenses = SqlGlobal.licenses(temp['license_id'])

            if licenses:
                licenses = licenses.__dict__
                licenses.pop('_sa_instance_state', None)
            else:
                licenses = {}

            temp['license'] = licenses
            temp.pop('license_id', None)
        else:
            temp['license'] = {}
            temp.pop('license_id', None)

        return temp

    def populate_regional(self, temp):
        ''' Doc: function populate_regional  '''
        regional = SqlGlobal.regional(temp['regional_id'])

        if regional:
            regional = regional.__dict__
            regional.pop('_sa_instance_state', None)
        else:
            regional = {}

        temp['regional'] = regional
        temp.pop('regional_id', None)

        return temp

    def populate_infographic_detail(self, temp):
        ''' Doc: function populate_infographic_detail  '''
        temp['infographic_detail'] = []
        infographic_detail = db.session.query(InfographicDetailModel)
        infographic_detail = infographic_detail.options(
            defer("cuid"),
            defer("muid"))
        infographic_detail = infographic_detail.filter(InfographicDetailModel.infographic_id == temp['id'])
        infographic_detail = infographic_detail.order_by(text("infographic_detail.title asc"))
        infographic_detail = infographic_detail.all()
        for res_id in infographic_detail:
            temp_id = res_id.__dict__
            temp_id.pop('_sa_instance_state', None)
            temp['infographic_detail'].append(temp_id)

        return temp

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(InfographicModel.dataset_class_id == 3)
                result = result.filter(InfographicModel.is_deleted == False)
                result = result.filter(InfographicModel.is_active == True)
                result = result.filter(InfographicModel.mdate <= "'" + TODAY + "'")
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id, where: dict):
        ''' Doc: function get by id  '''
        # handle id=title, integer or string
        try:
            temp = int(_id)
            _id = temp
        except Exception as err:
            temp = _id
            result = db.session.query(InfographicModel.id)
            result = result.filter(InfographicModel.title == temp)
            result = result.all()
            if result:
                result = result[0]
                _id = result[0]
            else:
                _id = 0

        try:
            # execute database
            result = db.session.query(InfographicModel)
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(InfographicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(InfographicModel, attr) == value)

            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(InfographicModel.dataset_class_id == 3)
                result = result.filter(InfographicModel.is_deleted == False)
                result = result.filter(InfographicModel.is_active == True)
                result = result.filter(InfographicModel.mdate <= "'" + TODAY + "'")

            result = result.filter(InfographicModel.id == _id)
            result = result.all()
            if result:
                result = result[0]

            # change into dict
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                result['cdate'] = result['cdate'].strftime(FORMAT_DATETIME)
                result['mdate'] = result['mdate'].strftime(FORMAT_DATETIME)

                # get relation to dataset_class
                result = self.populate_keywords(result)

                # get relation to dataset_class
                result = self.populate_dataset_class(result)

                # get relation to sektoral
                result = self.populate_sektoral(result)

                # get relation to license
                result = self.populate_license(result)

                # get relation to regional
                result = self.populate_regional(result)

                # get relation to infographic_detail
                result = self.populate_infographic_detail(result)

            else:
                result = {}

            # check if empty
            if result:
                return True, result, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            # title
            json_send["title"] = json_send["name"].lower().replace(' ', '-')
            json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(InfographicModel.id)
            res_count = res_count.filter(InfographicModel.name == json_send["name"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            json_send["cuid"] = jwt['id']
            json_send["count_view"] = 0
            json_send["count_rating"] = 0
            json_send["count_share_fb"] = 0
            json_send["count_share_tw"] = 0
            json_send["count_share_wa"] = 0
            json_send["count_share_link"] = 0
            json_send["count_download_img"] = 0
            json_send["count_download_pdf"] = 0
            if "cdate" not in json_send:
                json_send["cdate"] = HELPER.local_date_server()
            # prepare data model

            result = InfographicModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, infographic, message = self.get_by_id(result['id'], {})
            self.refresh()

            # check if exist
            if res:
                return True, infographic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            if 'name' in json_send:
                json_send["title"] = json_send["name"].lower().replace(' ', '-')

            if 'count_view' in _json or 'count_rating' in _json or 'count_share_fb' in _json or 'count_share_tw' in _json or 'count_share_wa' in _json or 'count_share_link' in _json or 'count_download_img' in _json or 'count_download_pdf' in _json:
                json_send = _json
            else:
                if "mdate" not in json_send:
                    json_send["mdate"] = HELPER.local_date_server()
                json_send["muid"] = jwt['id']
            try:
                # prepare data model
                result = db.session.query(InfographicModel)
                for attr, value in where.items():
                    result = result.filter(getattr(InfographicModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, dataset, message = self.get_by_id(where["id"], {})
                self.refresh()

                # check if empty
                if res:
                    return True, dataset
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(InfographicModel)
                for attr, value in where.items():
                    result = result.filter(getattr(InfographicModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, dataset, message = self.get_by_id(where["id"], {})
                self.refresh()

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_owner_all(self):
        ''' Doc: function get owner all  '''
        try:
            # query postgresql
            result = db.session.query(InfographicModel.owner)
            result = result.distinct(InfographicModel.owner)
            result = result.filter(getattr(InfographicModel, 'is_deleted') == False)
            result = result.filter(getattr(InfographicModel, 'is_active') == True)
            result = result.filter(getattr(InfographicModel, 'owner') != None)
            result = result.all()

            result = list(dict.fromkeys(result))

            # change into dict
            infographic = []
            for res in result:
                temp = {}
                temp['owner'] = res[0]
                infographic.append(temp)

            # check if empty
            if infographic:
                return True, infographic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_source_all(self):
        ''' Doc: function get source all  '''
        try:
            # query postgresql
            result_skpd = db.session.query(SkpdModel.nama_skpd)
            result_skpd = result_skpd.distinct(SkpdModel.nama_skpd)
            result_skpd = result_skpd.all()

            result_source = db.session.query(InfographicModel.source)
            result_source = result_source.distinct(InfographicModel.source)
            result_source = result_source.all()

            result = result_skpd + result_source
            result = list(dict.fromkeys(result))

            # change into dict
            infographic = []
            for res in result:
                temp = {}
                temp['source'] = res[0]
                infographic.append(temp)

            # check if empty
            if infographic:
                return True, infographic
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_alls(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get alls  '''
        try:
            # query exclude
            result_exclude = db.session.query(HighlightModel.category_id)
            result_exclude = result_exclude.filter(getattr(HighlightModel, 'category') == 'infografik')
            result_exclude = result_exclude.all()

            # query postgresql
            result = db.session.query(InfographicModel)
            result = result.options(
                defer("cuid"),
                defer("muid"))
            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(InfographicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(InfographicModel, attr) == value)
            # where exclude
            for row_exclude in result_exclude:
                result = result.filter(getattr(InfographicModel, 'id') != row_exclude[0])

            # sort
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            infographic = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATETIME)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATETIME)

                infographic.append(temp)

            # check if empty
            infographic = list(infographic)
            if infographic:
                return True, infographic
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def switch(self, category, infographic):
        ''' Doc: function switch  '''
        if not category:
            category = 'view'
        return getattr(self, 'case_' + str(category), lambda: infographic)(infographic)
    def case_facebook(self, infographic):
        ''' Doc: function switch  '''
        count_share_fb = infographic['count_share_fb']
        if not count_share_fb:
            count_share_fb = '0'
        count_share_fb = int(count_share_fb) + 1
        payload = {'count_share_fb': int(count_share_fb)}
        infographic['count_share_fb'] = count_share_fb
        return infographic, payload
    def case_twitter(self, infographic):
        ''' Doc: function case_twitter  '''
        count_share_tw = infographic['count_share_tw']
        if not count_share_tw:
            count_share_tw = '0'
        count_share_tw = int(count_share_tw) + 1
        payload = {'count_share_tw': int(count_share_tw)}
        infographic['count_share_tw'] = count_share_tw
        return infographic, payload
    def case_whatsapp(self, infographic):
        ''' Doc: function case_whatsapp  '''
        count_share_wa = infographic['count_share_wa']
        if not count_share_wa:
            count_share_wa = '0'
        count_share_wa = int(count_share_wa) + 1
        payload = {'count_share_wa': int(count_share_wa)}
        infographic['count_share_wa'] = count_share_wa
        return infographic, payload
    def case_link(self, infographic):
        ''' Doc: function case_view  '''
        count_share_link = infographic['count_share_link']
        if not count_share_link:
            count_share_link = '0'
        count_share_link = int(count_share_link) + 1
        payload = {'count_share_link': int(count_share_link)}
        infographic['count_share_link'] = count_share_link
        return infographic, payload
    def case_image(self, infographic):
        ''' Doc: function case_view  '''
        count_download_img = infographic['count_download_img']
        if not count_download_img:
            count_download_img = '0'
        count_download_img = int(count_download_img) + 1
        payload = {'count_download_img': int(count_download_img)}
        infographic['count_download_img'] = count_download_img
        return infographic, payload
    def case_pdf(self, infographic):
        ''' Doc: function case_access  '''
        count_download_pdf = infographic['count_download_pdf']
        if not count_download_pdf:
            count_download_pdf = '0'
        count_download_pdf = int(count_download_pdf) + 1
        payload = {'count_download_pdf': int(count_download_pdf)}
        infographic['count_download_pdf'] = count_download_pdf
        return infographic, payload
    def case_view(self, infographic):
        ''' Doc: function case_access  '''
        count_view = infographic['count_view']
        if not count_view:
            count_view = '0'
        count_view = int(count_view) + 1
        payload = {'count_view': int(count_view)}
        infographic['count_view'] = count_view
        return infographic, payload
