''' Doc: controller refresh  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
import sentry_sdk
CONFIGURATION = Configuration()
HELPER = Helper()

class RefreshController(object):
    ''' Doc: class article topic  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def refresh_view(self):
        ''' Doc: function resfresh materialized view  '''
        try:
            sql_query_dataset = 'REFRESH MATERIALIZED VIEW suggestion_dataset'
            sql_query_visualization = "REFRESH MATERIALIZED VIEW suggestion_visualization"
            sql_query_infographic = "REFRESH MATERIALIZED VIEW suggestion_infographic"
            sql_query_article = "REFRESH MATERIALIZED VIEW suggestion_article"

            sql_dataset = text(sql_query_dataset)
            sql_visualization = text(sql_query_visualization)
            sql_infographic = text(sql_query_infographic)
            sql_article = text(sql_query_article)

            db.engine.execute(sql_dataset)
            db.engine.execute(sql_visualization)
            db.engine.execute(sql_infographic)
            db.engine.execute(sql_article)

            return True

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
