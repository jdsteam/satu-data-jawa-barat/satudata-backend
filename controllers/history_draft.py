''' Doc: controller history draft  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryDraftModel, DatasetModel, SkpdModel, UserModel, HistoryDraftViewModel, HistoryDraftAllViewModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=singleton-comparison, unused-variable, unused-argument


class HistoryDraftController(object):
    ''' Doc: class history draft  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def checkExist(self, param: dict):
        ''' Doc: function checkExist  '''
        try:
            result = db.session.query(HistoryDraftModel.id, HistoryDraftModel.type,
                                      HistoryDraftModel.type_id, HistoryDraftModel.category)
            for attr, value in param.items():
                result = result.filter(
                    getattr(HistoryDraftModel, attr) == value)
            return result
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                DatasetModel.id.label('dataset_id'), HistoryDraftModel.id.label('history_id'),
                DatasetModel.name, SkpdModel.nama_skpd_alias, DatasetModel.cdate,
                HistoryDraftModel.datetime, HistoryDraftModel.category, HistoryDraftModel.notes,
                UserModel.username.label('user_username'), UserModel.name.label('user_name')
            )
            result = result.join(DatasetModel, HistoryDraftModel.type_id == DatasetModel.id)
            result = result.join(SkpdModel, DatasetModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.join(UserModel, HistoryDraftModel.user_id == UserModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryDraftModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(HistoryDraftModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(DatasetModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(DatasetModel, "cdate"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftModel, "category"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftModel, "notes"), sqlalchemy.String).ilike('%'+search+'%')
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query_all(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(HistoryDraftAllViewModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryDraftAllViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryDraftAllViewModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(HistoryDraftAllViewModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftAllViewModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftAllViewModel, "cdate"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftAllViewModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftAllViewModel, "category"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftAllViewModel, "notes"), sqlalchemy.String).ilike('%'+search+'%')
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query_all(where, search)
            result = result.order_by(text("history_draft_all_view."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_draft = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATE)})
                if temp['mdate'] is not None:
                    temp.update({"mdate": temp['mdate'].strftime(FORMAT_DATE)})
                if temp['datetime'] is not None:
                    temp.update({"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temps = {}
                temps['id'] = temp['id']
                temps['history_id'] = temp['id']
                temps['type'] = temp['type']
                temps['type_id'] = temp['type_id']
                temps['category'] = temp['category']
                temps['notes'] = temp['notes']
                temps['datetime'] = temp['datetime']
                temps['name'] = temp['name']
                temps['kode_skpd'] = temp['kode_skpd']
                temps['nama_skpd'] = temp['nama_skpd']
                temps['nama_skpd_alias'] = temp['nama_skpd_alias']
                temps['cuid'] = temp['cuid']
                temps['cdate'] = temp['cdate']
                temps['mdate'] = temp['mdate']
                temps['is_active'] = temp['is_active']
                temps['is_deleted'] = temp['is_deleted']
                temps['is_validate'] = temp['is_validate']
                temps['is_discontinue'] = temp['is_discontinue']
                temps['user_id'] = temp['user_id']
                temps['user_username'] = temp['user_username']
                temps['user_name'] = temp['user_name']

                history_draft.append(temps)

            # check if empty
            history_draft = list(history_draft)
            if history_draft:
                return True, history_draft
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query_all(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query_all({'id': _id}, '')
            result = result.all()

            # change into dict
            history_draft = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATE)})
                temp.update({"datetime": temp['datetime'].strftime(FORMAT_DATE)})
                history_draft.append(temp)

            # check if empty
            if history_draft:
                return True, history_draft, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            if "datetime" not in json:
                json_send["datetime"] = HELPER.local_date_server()

            # prepare data model
            result = HistoryDraftModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history_draft, message = self.get_by_id(result['id'])

            # check duplicate
            # res_check = self.checkExist({
            #     'type': json_send["type"], 'type_id': json_send["type_id"],
            #     'category': json_send["category"]
            # })
            # count = res_check.count()
            # existing_data = res_check.all()
            # rows = [r._asdict() for r in existing_data]

            # if (count > 0):
            #     where = {
            #         'type': json_send["type"], 'type_id': json_send["type_id"],
            #         'category': json_send["category"], 'id': rows[0]['id']
            #     }
            #     self.update(where, json_send)
            #     result = rows
            #     res = True
            # else:
            #     save = HistoryDraftModel(**json_send)
            #     # execute database
            #     db.session.add(save)
            #     db.session.commit()
            #     result = save.to_dict()
            #     res = True

            # check if exist
            if res:
                return True, history_draft
            else:
                return False, {}

        except Exception as err:
            # fail response
            print(err)
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            if "datetime" not in json:
                json_send["datetime"] = HELPER.local_date_server()

            try:
                # prepare data model
                result = db.session.query(HistoryDraftModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(HistoryDraftModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history_draft, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history_draft
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryDraftModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(HistoryDraftModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history_draft, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def query_desk(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(HistoryDraftViewModel)

            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryDraftViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryDraftViewModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(HistoryDraftViewModel, "name"),sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftViewModel, "nama_skpd"),sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftViewModel, "nama_skpd_alias"),sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftViewModel, "username"),sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryDraftViewModel, "cdate"),sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all_desk(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query_desk(where, search)
            result = result.order_by(text("history_draft_view."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_draft = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate": temp['cdate'].strftime(FORMAT_DATE)})
                temp.update({"mdate": temp['mdate'].strftime(FORMAT_DATE)})
                if temp['datetime'] is not None:
                    temp.update({"datetime": temp['datetime'].strftime(FORMAT_DATE)})

                temps = {}
                temps['id'] = temp['id']
                temps['type'] = temp['type']
                temps['type_id'] = temp['type_id']
                temps['name'] = temp['name']
                temps['kode_skpd'] = temp['kode_skpd']
                temps['nama_skpd'] = temp['nama_skpd']
                temps['nama_skpd_alias'] = temp['nama_skpd_alias']
                temps['cuid'] = temp['cuid']
                temps['username'] = temp['username']
                temps['cdate'] = temp['cdate']
                temps['mdate'] = temp['mdate']
                temps['datetime'] = None
                temps['is_active'] = temp['is_active']
                temps['is_deleted'] = temp['is_deleted']
                temps['is_validate'] = temp['is_validate']
                temps['is_discontinue'] = temp['is_discontinue']
                temps['history_draft'] = {}
                temps['history_drafts'] = []

                if temps["is_validate"] is not None:
                    try:
                        res_hd = db.session.query(HistoryDraftModel)
                        res_hd = res_hd.filter(getattr(HistoryDraftModel, "type") == temps['type'])
                        res_hd = res_hd.filter(getattr(HistoryDraftModel, "type_id") == temps["type_id"])
                        res_hd = res_hd.order_by(text("id desc"))
                        res_hd = res_hd.all()

                        for hd in res_hd:
                            temp_hd = hd.__dict__
                            temp_hd.pop("_sa_instance_state", None)
                            temp_hd["datetime"] = temp_hd["datetime"].strftime(DATETIME_FORMAT)
                            temps['history_drafts'].append(temp_hd)

                        if len(temps['history_drafts']) > 0:
                            temps['history_draft'] = temps['history_drafts'][0]
                            temps['datetime'] = temps['datetime']

                    except Exception as e:
                        print(e)

                temps['discontinue_active'] = None
                temps['discontinue_archive'] = None
                if temps['is_discontinue']:
                    try:
                        sql_active = text('''SELECT datetime FROM history_draft WHERE ''' +
                                "type = '"+temps['type']+"' AND " +
                                "type_id = '"+str(temps['type_id'])+"' AND " +
                                "category = 'discontinue-active' ORDER BY id asc")
                        res_hd_active = db.engine.execute(sql_active)
                        for hd_active in res_hd_active:
                            temp_hd_active = dict(hd_active)
                            temps['discontinue_active'] = temp_hd_active["datetime"].strftime(DATETIME_FORMAT)
                    except Exception as err:
                        print(err)

                    try:
                        sql_archive = text('''SELECT datetime FROM history_draft WHERE ''' +
                                "type = '"+temps['type']+"' AND " +
                                "type_id = '"+str(temps['type_id'])+"' AND " +
                                "category = 'discontinue-archive' ORDER BY id asc")
                        res_hd_archive = db.engine.execute(sql_archive)
                        for hd_archive in res_hd_archive:
                            temp_hd_archive = dict(hd_archive)
                            temps['discontinue_archive'] = temp_hd_archive["datetime"].strftime(DATETIME_FORMAT)
                    except Exception as err:
                        print(err)

                history_draft.append(temps)

            # check if empty
            history_draft = list(history_draft)
            if history_draft:
                return True, history_draft
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count_desk(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query_desk(where, search)
            result = result.count()

            # change into dict
            history_draft = {}
            history_draft['count'] = result

            # check if empty
            if history_draft:
                return True, history_draft
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
