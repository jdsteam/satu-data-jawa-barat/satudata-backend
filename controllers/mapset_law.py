''' Doc: controller mapset law  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetLawModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MapsetLawController(object):
    ''' Doc: class mapset law  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''
    
    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(MapsetLawModel)
            for attr, value in where.items():
                result = result.filter(getattr(MapsetLawModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetLawModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MapsetLawModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            mapset_law = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                
                if temp['cdate'] is not None:
                    temp['cdate'] =  temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")

                if temp['mdate'] is not None:
                    temp['mdate'] =  temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")
                mapset_law.append(temp)

            # check if empty
            mapset_law = list(mapset_law)
            if mapset_law:
                return True, mapset_law
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(MapsetLawModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(MapsetLawModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(MapsetLawModel, attr) == value)
            result = result.filter(or_(cast(getattr(MapsetLawModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in MapsetLawModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(MapsetLawModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['cdate'] is not None:
                    result['cdate'] =  result['cdate'].strftime("%Y-%m-%d %H:%M:%S")

                if result['mdate'] is not None:
                    result['mdate'] =  result['mdate'].strftime("%Y-%m-%d %H:%M:%S")
            else:
                result = {}

            # change into dict
            mapset_law = result

            # check if empty
            if mapset_law:
                return True, mapset_law
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            jwt = HELPER.read_jwt()
            
            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = MapsetLawModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, mapset_law = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, mapset_law
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()
            
            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(MapsetLawModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetLawModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, mapset_law = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, mapset_law
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(MapsetLawModel)
                for attr, value in where.items():
                    result = result.filter(getattr(MapsetLawModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, mapset_law = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
