''' Doc: controller indikator category  '''
from helpers import Helper
from exceptions import ErrorMessage
from models import IndikatorCategoryModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk

HELPER = Helper()

# pylint: disable=singleton-comparison, unused-variable, unused-argument
class IndikatorCategoryController(object):
    ''' Doc: class indikator category  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(IndikatorCategoryModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(IndikatorCategoryModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(IndikatorCategoryModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(IndikatorCategoryModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(IndikatorCategoryModel, "notes"), sqlalchemy.String).ilike('%'+search+'%')
            ))
            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("indikator_category."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            indikator_category = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                indikator_category.append(temp)

            # check if empty
            if indikator_category:
                return True, indikator_category
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(IndikatorCategoryModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

            else:
                result = {}
            # check if empty
            if result:
                return True, result, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, _json, condition):
        ''' Doc: function post param  '''
        try:
            # generate json data
            json_send = {}
            json_send = _json

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # prepare data model
            param = self.post_param(json, 'add')
            result = IndikatorCategoryModel(**param)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            res, request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            param = self.post_param(json, 'edit')

            try:
                # prepare data model
                result = db.session.query(IndikatorCategoryModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorCategoryModel, attr) == value)

                # execute database
                result = result.update(param, synchronize_session='fetch')
                result = db.session.commit()
                res, request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(IndikatorCategoryModel)
                for attr, value in where.items():
                    result = result.filter(getattr(IndikatorCategoryModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
