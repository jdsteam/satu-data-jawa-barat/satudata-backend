''' Doc: controller user   '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import UserModel, SkpdModel, SkpdSubModel, SkpdUnitModel, RoleModel, StrukturOrganisasiModel, JabatanModel, AgreementModel
from models import UserViewModel
from helpers.postgre_alchemy import postgre_alchemy as db
import helpers.postgre_psycopg as db2
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
from threading import Thread
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable, singleton-comparison, broad-except


class UserController(object):
    ''' Doc: controller user  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(UserViewModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(UserViewModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(UserViewModel, attr) == value)
            result = result.filter(or_(
                cast(getattr(UserViewModel, "username"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "email"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "role_title"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "role_name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "jabatan_nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "satuan_kerja_nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
            ))
            result = result.order_by(text(sort[0]+" "+sort[1] + ", id desc"))
            result = result.offset(skip).limit(limit)
            result = result.options(
                defer("password"))
            result = result.all()
            # result = result.distinct()

            # change into dict
            user = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)

                temp = {}
                temp['id'] = res['id']
                temp['username'] = res['username']
                temp['nip'] = res['nip']
                temp['email'] = res['email']
                temp['name'] = res['name']
                temp['kode_kemendagri'] = res['kode_kemendagri']
                temp['kode_skpd'] = res['kode_skpd']
                temp['kode_skpdsub'] = res['kode_skpdsub']
                temp['kode_skpdunit'] = res['kode_skpdunit']
                temp['profile_pic'] = res['profile_pic']
                if res['last_login']:
                    temp['last_login'] = res['last_login'].strftime("%Y-%m-%d")
                else:
                    temp['last_login'] = None
                temp['notes'] = res['notes']
                temp['cuid'] = res['cuid']
                if res['cdate']:
                    temp['cdate'] = res['cdate'].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    temp['cdate'] = None
                temp['muid'] = res['muid']
                if res['mdate']:
                    temp['mdate'] = res['mdate'].strftime("%Y-%m-%d %H:%M:%S")
                else:
                    temp['mdate'] = None
                temp['is_active'] = res['is_active']
                temp['is_deleted'] = res['is_deleted']
                temp['is_agree'] = res['is_agree']
                temp['count_notif'] = res['count_notif']
                temp['agreement_id'] = res['agreement_id']
                temp['agreement'] = {}
                temp['agreement']['id'] = res['agreement_id']
                temp['agreement']['agreement_text'] = ""
                temp['agreement']['version'] = res['agreement_version']
                temp['agreement']['notes'] = res['agreement_notes']
                temp['agreement']['is_active'] = res['agreement_is_active']
                temp['agreement']['is_deleted'] = res['agreement_is_deleted']
                temp['agreement']['cuid'] = res['agreement_cuid']
                if res['agreement_cdate']:
                    temp['agreement']['cdate'] = res['agreement_cdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                else:
                    temp['agreement']['cdate'] = None
                temp['agreement']['muid'] = res['agreement_muid']
                if res['agreement_mdate']:
                    temp['agreement']['mdate'] = res['agreement_mdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                else:
                    temp['agreement']['mdate'] = None
                temp['skpd'] = {}
                temp['skpd']['id'] = res['skpd_id']
                temp['skpd']['title'] = res['skpd_title']
                temp['skpd']['kode_skpd'] = res['skpd_kode_skpd']
                temp['skpd']['nama_skpd'] = res['skpd_nama_skpd']
                temp['skpd']['nama_skpd_alias'] = res['skpd_nama_skpd_alias']
                temp['skpd']['logo'] = res['skpd_logo']
                temp['skpd']['description'] = res['skpd_description']
                temp['skpd']['address'] = res['skpd_address']
                temp['skpd']['phone'] = res['skpd_phone']
                temp['skpd']['email'] = res['skpd_email']
                temp['skpd']['regional_id'] = res['skpd_regional_id']
                temp['skpd']['count_dataset'] = res['skpd_count_dataset']
                temp['skpd']['count_dataset_public'] = res['skpd_count_dataset_public']
                temp['skpd']['count_indikator'] = res['skpd_count_indikator']
                temp['skpd']['count_indikator_public'] = res['skpd_count_indikator_public']
                temp['skpd']['count_infographic'] = res['skpd_count_infographic']
                temp['skpd']['count_infographic_public'] = res['skpd_count_infographic_public']
                temp['skpd']['count_visualization'] = res['skpd_count_visualization']
                temp['skpd']['count_visualization_public'] = res['skpd_count_visualization_public']
                temp['skpd']['is_deleted'] = res['skpd_is_deleted']
                temp['skpd']['is_external'] = res['skpd_is_external']
                temp['skpd']['is_opendata'] = res['skpd_is_opendata']
                temp['skpd']['is_satudata'] = res['skpd_is_satudata']
                temp['skpd']['is_satupeta'] = res['skpd_is_satupeta']
                temp['skpd']['media_facebook'] = res['skpd_media_facebook']
                temp['skpd']['media_instagram'] = res['skpd_media_instagram']
                temp['skpd']['media_twitter'] = res['skpd_media_twitter']
                temp['skpd']['media_website'] = res['skpd_media_website']
                temp['skpd']['media_youtube'] = res['skpd_media_youtube']
                temp['skpdsub'] = {}
                temp['skpdsub']['id'] = res['skpdsub_id']
                temp['skpdsub']['kode_skpd'] = res['skpdsub_kode_skpd']
                temp['skpdsub']['kode_skpdsub'] = res['skpdsub_kode_skpdsub']
                temp['skpdsub']['nama_skpd'] = res['skpdsub_nama_skpd']
                temp['skpdsub']['nama_skpdsub'] = res['skpdsub_nama_skpdsub']
                temp['skpdsub']['skpd_id'] = res['skpdsub_skpd_id']
                temp['skpdunit'] = {}
                temp['skpdunit']['id'] = res['skpdunit_id']
                temp['skpdunit']['kode_skpd'] = res['skpdunit_kode_skpd']
                temp['skpdunit']['kode_skpdsub'] = res['skpdunit_kode_skpdsub']
                temp['skpdunit']['kode_skpdunit'] = res['skpdunit_kode_skpdunit']
                temp['skpdunit']['nama_skpd'] = res['skpdunit_nama_skpd']
                temp['skpdunit']['nama_skpdsub'] = res['skpdunit_nama_skpdsub']
                temp['skpdunit']['nama_skpdunit'] = res['skpdunit_nama_skpdunit']
                temp['skpdunit']['skpdsub_id'] = res['skpdunit_skpdsub_id']
                temp['role_id'] = res['role_id']
                temp['role_name'] = res['role_name']
                temp['role_title'] = res['role_title']
                temp['jabatan_id'] = res['jabatan_id']
                temp['jabatan_nama'] = res['jabatan_nama']
                temp['satuan_kerja_id'] = res['satuan_kerja_id']
                temp['satuan_kerja_nama'] = res['satuan_kerja_nama']
                temp['lv1_unit_kerja_id'] = res['lv1_unit_kerja_id']
                temp['lv1_unit_kerja_nama'] = res['lv1_unit_kerja_nama']
                temp['lv2_unit_kerja_id'] = res['lv2_unit_kerja_id']
                temp['lv2_unit_kerja_nama'] = res['lv2_unit_kerja_nama']
                temp['lv3_unit_kerja_id'] = res['lv3_unit_kerja_id']
                temp['lv3_unit_kerja_nama'] = res['lv3_unit_kerja_nama']
                temp['lv4_unit_kerja_id'] = res['lv4_unit_kerja_id']
                temp['lv4_unit_kerja_nama'] = res['lv4_unit_kerja_nama']
                temp['level_unit_kerja'] = res['level_unit_kerja']
                temp['dash_role_id'] = res['dash_role_id']
                temp['dash_role_name'] = res['dash_role_name']
                temp['dash_role_title'] = res['dash_role_title']

                user.append(temp)

            # check if empty
            user = list(user)
            if user:
                return True, user
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(UserViewModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(UserViewModel, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(UserViewModel, attr) == value)
            result = result.filter(or_(
                cast(getattr(UserViewModel, "username"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "email"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "role_title"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "role_name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "jabatan_nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "satuan_kerja_nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
            ))
            result = result.distinct()
            result = result.count()

            # change into dict
            user = {}
            user['count'] = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(UserViewModel)
            result = result.options(
                defer("password"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                temp = {}
                temp['id'] = result['id']
                temp['username'] = result['username']
                temp['nip'] = result['nip']
                temp['email'] = result['email']
                temp['name'] = result['name']
                temp['kode_kemendagri'] = result['kode_kemendagri']
                temp['kode_skpd'] = result['kode_skpd']
                temp['kode_skpdsub'] = result['kode_skpdsub']
                temp['kode_skpdunit'] = result['kode_skpdunit']
                temp['profile_pic'] = result['profile_pic']
                if result['last_login']:
                    temp['last_login'] = result['last_login'].strftime(
                        "%Y-%m-%d")
                else:
                    temp['last_login'] = None
                temp['notes'] = result['notes']
                temp['cuid'] = result['cuid']
                if result['cdate']:
                    temp['cdate'] = result['cdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                else:
                    temp['cdate'] = None
                temp['muid'] = result['muid']
                if result['mdate']:
                    temp['mdate'] = result['mdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                else:
                    temp['mdate'] = None
                temp['is_active'] = result['is_active']
                temp['is_deleted'] = result['is_deleted']
                temp['is_agree'] = result['is_agree']
                temp['count_notif'] = result['count_notif']
                temp['agreement_id'] = result['agreement_id']
                temp['agreement'] = {}
                temp['agreement']['id'] = result['agreement_id']
                temp['agreement']['agreement_text'] = result['agreement_text']
                temp['agreement']['version'] = result['agreement_version']
                temp['agreement']['notes'] = result['agreement_notes']
                temp['agreement']['is_active'] = result['agreement_is_active']
                temp['agreement']['is_deleted'] = result['agreement_is_deleted']
                temp['agreement']['cuid'] = result['agreement_cuid']
                if result['agreement_cdate']:
                    temp['agreement']['cdate'] = result['agreement_cdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                else:
                    temp['agreement']['cdate'] = None
                temp['agreement']['muid'] = result['agreement_muid']
                if result['agreement_mdate']:
                    temp['agreement']['mdate'] = result['agreement_mdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                else:
                    temp['agreement']['mdate'] = None
                temp['skpd'] = {}
                temp['skpd']['id'] = result['skpd_id']
                temp['skpd']['title'] = result['skpd_title']
                temp['skpd']['kode_skpd'] = result['skpd_kode_skpd']
                temp['skpd']['nama_skpd'] = result['skpd_nama_skpd']
                temp['skpd']['nama_skpd_alias'] = result['skpd_nama_skpd_alias']
                temp['skpd']['logo'] = result['skpd_logo']
                temp['skpd']['description'] = result['skpd_description']
                temp['skpd']['address'] = result['skpd_address']
                temp['skpd']['phone'] = result['skpd_phone']
                temp['skpd']['email'] = result['skpd_email']
                temp['skpd']['regional_id'] = result['skpd_regional_id']
                temp['skpd']['count_dataset'] = result['skpd_count_dataset']
                temp['skpd']['count_dataset_public'] = result['skpd_count_dataset_public']
                temp['skpd']['count_indikator'] = result['skpd_count_indikator']
                temp['skpd']['count_indikator_public'] = result['skpd_count_indikator_public']
                temp['skpd']['count_infographic'] = result['skpd_count_infographic']
                temp['skpd']['count_infographic_public'] = result['skpd_count_infographic_public']
                temp['skpd']['count_visualization'] = result['skpd_count_visualization']
                temp['skpd']['count_visualization_public'] = result['skpd_count_visualization_public']
                temp['skpd']['is_deleted'] = result['skpd_is_deleted']
                temp['skpd']['is_external'] = result['skpd_is_external']
                temp['skpd']['is_opendata'] = result['skpd_is_opendata']
                temp['skpd']['is_satudata'] = result['skpd_is_satudata']
                temp['skpd']['is_satupeta'] = result['skpd_is_satupeta']
                temp['skpd']['media_facebook'] = result['skpd_media_facebook']
                temp['skpd']['media_instagram'] = result['skpd_media_instagram']
                temp['skpd']['media_twitter'] = result['skpd_media_twitter']
                temp['skpd']['media_website'] = result['skpd_media_website']
                temp['skpd']['media_youtube'] = result['skpd_media_youtube']
                temp['skpdsub'] = {}
                temp['skpdsub']['id'] = result['skpdsub_id']
                temp['skpdsub']['kode_skpd'] = result['skpdsub_kode_skpd']
                temp['skpdsub']['kode_skpdsub'] = result['skpdsub_kode_skpdsub']
                temp['skpdsub']['nama_skpd'] = result['skpdsub_nama_skpd']
                temp['skpdsub']['nama_skpdsub'] = result['skpdsub_nama_skpdsub']
                temp['skpdsub']['skpd_id'] = result['skpdsub_skpd_id']
                temp['skpdunit'] = {}
                temp['skpdunit']['id'] = result['skpdunit_id']
                temp['skpdunit']['kode_skpd'] = result['skpdunit_kode_skpd']
                temp['skpdunit']['kode_skpdsub'] = result['skpdunit_kode_skpdsub']
                temp['skpdunit']['kode_skpdunit'] = result['skpdunit_kode_skpdunit']
                temp['skpdunit']['nama_skpd'] = result['skpdunit_nama_skpd']
                temp['skpdunit']['nama_skpdsub'] = result['skpdunit_nama_skpdsub']
                temp['skpdunit']['nama_skpdunit'] = result['skpdunit_nama_skpdunit']
                temp['skpdunit']['skpdsub_id'] = result['skpdunit_skpdsub_id']
                temp['role_id'] = result['role_id']
                temp['role_name'] = result['role_name']
                temp['role_title'] = result['role_title']
                temp['jabatan_id'] = result['jabatan_id']
                temp['jabatan_nama'] = result['jabatan_nama']
                temp['satuan_kerja_id'] = result['satuan_kerja_id']
                temp['satuan_kerja_nama'] = result['satuan_kerja_nama']
                temp['lv1_unit_kerja_id'] = result['lv1_unit_kerja_id']
                temp['lv1_unit_kerja_nama'] = result['lv1_unit_kerja_nama']
                temp['lv2_unit_kerja_id'] = result['lv2_unit_kerja_id']
                temp['lv2_unit_kerja_nama'] = result['lv2_unit_kerja_nama']
                temp['lv3_unit_kerja_id'] = result['lv3_unit_kerja_id']
                temp['lv3_unit_kerja_nama'] = result['lv3_unit_kerja_nama']
                temp['lv4_unit_kerja_id'] = result['lv4_unit_kerja_id']
                temp['lv4_unit_kerja_nama'] = result['lv4_unit_kerja_nama']
                temp['level_unit_kerja'] = result['level_unit_kerja']
                temp['dash_role_id'] = result['dash_role_id']
                temp['dash_role_name'] = result['dash_role_name']
                temp['dash_role_title'] = result['dash_role_title']

            else:
                temp = {}

            # change into dict
            user = temp

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id_old(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(UserModel)
            result = result.options(
                defer("password"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['last_login']:
                    result['last_login'] = result['last_login'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                if result['cdate']:
                    result['cdate'] = result['cdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")
                if result['mdate']:
                    result['mdate'] = result['mdate'].strftime(
                        "%Y-%m-%d %H:%M:%S")

                # get relation to skpd
                try:
                    if result['kode_skpd']:
                        if result['kode_skpd'].split():
                            skpd = db.session.query(SkpdModel)
                            skpd = skpd.options(
                                defer("cuid"),
                                defer("cdate"),
                                defer("muid"),
                                defer("mdate"),
                                defer("notes"))
                            skpd = skpd.filter(
                                getattr(SkpdModel, "kode_skpd") == result['kode_skpd'])
                            skpd = skpd.one()

                            if skpd:
                                skpd = skpd.__dict__
                                skpd.pop('_sa_instance_state', None)
                            else:
                                skpd = {}

                            result['skpd'] = skpd
                        else:
                            result['skpd'] = {}
                    else:
                        result['skpd'] = {}
                except Exception as err:
                    result['skpd'] = {}

                # get relation to skpdsub
                try:
                    if result['kode_skpdsub']:
                        if result['kode_skpdsub'].split():
                            skpdsub = db.session.query(SkpdSubModel)
                            skpdsub = skpdsub.options(
                                defer("cuid"),
                                defer("cdate"))
                            skpdsub = skpdsub.filter(
                                getattr(SkpdSubModel, "kode_skpd") == result['skpd']['kode_skpd'])
                            skpdsub = skpdsub.filter(
                                getattr(SkpdSubModel, "kode_skpdsub") == result['kode_skpdsub'])
                            skpdsub = skpdsub.limit(1)
                            skpdsub = skpdsub.one()

                            if skpdsub:
                                skpdsub = skpdsub.__dict__
                                skpdsub.pop('_sa_instance_state', None)
                            else:
                                skpdsub = {}

                            result['skpdsub'] = skpdsub
                        else:
                            result['skpdsub'] = {}
                    else:
                        result['skpdsub'] = {}
                except Exception as err:
                    result['skpdsub'] = {}

                # get relation to skpdunit
                try:
                    if result['kode_skpdunit']:
                        if result['kode_skpdunit'].split():
                            skpdunit = db.session.query(SkpdUnitModel)
                            skpdunit = skpdunit.join(
                                SkpdSubModel, SkpdUnitModel.kode_skpdsub == SkpdSubModel.kode_skpdsub)
                            skpdunit = skpdunit.options(
                                defer("cuid"),
                                defer("cdate"))
                            skpdunit = skpdunit.filter(
                                getattr(SkpdSubModel, "kode_skpd") == result['skpd']['kode_skpd'])
                            skpdunit = skpdunit.filter(
                                getattr(SkpdSubModel, "kode_skpdsub") == result['skpdsub']['kode_skpdsub'])
                            skpdunit = skpdunit.filter(
                                getattr(SkpdUnitModel, "kode_skpdunit") == result['kode_skpdunit'])
                            skpdunit = skpdunit.limit(1)
                            skpdunit = skpdunit.one()

                            if skpdunit:
                                skpdunit = skpdunit.__dict__
                                skpdunit.pop('_sa_instance_state', None)
                            else:
                                skpdunit = {}

                            result['skpdunit'] = skpdunit
                        else:
                            result['skpdunit'] = {}
                    else:
                        result['skpdunit'] = {}
                except Exception as err:
                    result['skpdunit'] = {}

                # role
                try:
                    if result['role_id']:
                        role_data = db.session.query(RoleModel)
                        role_data = role_data.filter(
                            getattr(RoleModel, "id") == result['role_id'])
                        role_data = role_data.one()

                        if role_data:
                            role_data = role_data.__dict__
                            role_data.pop('_sa_instance_state', None)
                            result['role_name'] = role_data['name']
                            result['role_title'] = role_data['title']
                        else:
                            result['role_name'] = ''
                            result['role_title'] = ''
                    else:
                        result['role_name'] = ''
                        result['role_title'] = ''
                except Exception as err:
                    result['role_name'] = ''
                    result['role_title'] = ''

                # struktur organisasi
                try:
                    if result['jabatan_id']:
                        # cek count
                        count_struktur = db.session.query(
                            StrukturOrganisasiModel)
                        count_struktur = count_struktur.filter(
                            getattr(StrukturOrganisasiModel, "jabatan_id") == result['jabatan_id'])
                        count_struktur = count_struktur.count()

                        if count_struktur > 0:
                            # get relation struktur organisasi
                            data_struktur = db.session.query(
                                StrukturOrganisasiModel)
                            data_struktur = data_struktur.filter(
                                getattr(StrukturOrganisasiModel, "jabatan_id") == result['jabatan_id'])
                            data_struktur = data_struktur.one()
                            if data_struktur:
                                ds_ = data_struktur.__dict__
                                ds_.pop('_sa_instance_state', None)
                                result['satuan_kerja_nama'] = ds_[
                                    'satuan_kerja_nama']
                                result['lv1_unit_kerja_nama'] = ds_[
                                    'lv1_unit_kerja_nama']
                                result['lv2_unit_kerja_nama'] = ds_[
                                    'lv2_unit_kerja_nama']
                                result['lv3_unit_kerja_nama'] = ds_[
                                    'lv3_unit_kerja_nama']
                                result['lv4_unit_kerja_nama'] = ds_[
                                    'lv4_unit_kerja_nama']
                                result['jabatan_nama'] = ds_['jabatan_nama']
                            else:
                                result['satuan_kerja_nama'] = ''
                                result['lv1_unit_kerja_nama'] = ''
                                result['lv2_unit_kerja_nama'] = ''
                                result['lv3_unit_kerja_nama'] = ''
                                result['lv4_unit_kerja_nama'] = ''
                                result['jabatan_nama'] = ''

                        else:
                            # get relation jabatan
                            data_jabatan = db.session.query(JabatanModel)
                            data_jabatan = data_jabatan.filter(
                                getattr(JabatanModel, "jabatan_id") == result['jabatan_id'])
                            data_jabatan = data_jabatan.one()
                            if data_jabatan:
                                dj_ = data_jabatan.__dict__
                                result['satuan_kerja_nama'] = dj_[
                                    'satuan_kerja_nama']
                                result['jabatan_nama'] = dj_['jabatan_nama']
                            else:
                                result['satuan_kerja_nama'] = ''
                                result['jabatan_nama'] = ''

                            # get relation lv1_unit_kerja
                            if result['lv1_unit_kerja_id']:
                                data_lv1 = db.session.query(JabatanModel)
                                data_lv1 = data_lv1.filter(
                                    getattr(JabatanModel, "unit_kerja_id") == result['lv1_unit_kerja_id'])
                                data_lv1 = data_lv1.limit(1)
                                data_lv1 = data_lv1.one()
                                if data_lv1:
                                    dl1_ = data_lv1.__dict__
                                    result['lv1_unit_kerja_nama'] = dl1_[
                                        'unit_kerja_nama']
                                else:
                                    result['lv1_unit_kerja_nama'] = ''
                            else:
                                result['lv1_unit_kerja_nama'] = ''

                            # get relation lv2_unit_kerja
                            if result['lv2_unit_kerja_id']:
                                data_lv2 = db.session.query(JabatanModel)
                                data_lv2 = data_lv2.filter(
                                    getattr(JabatanModel, "unit_kerja_id") == result['lv2_unit_kerja_id'])
                                data_lv2 = data_lv2.limit(1)
                                data_lv2 = data_lv2.one()
                                if data_lv2:
                                    dl2_ = data_lv2.__dict__
                                    result['lv2_unit_kerja_nama'] = dl2_[
                                        'unit_kerja_nama']
                                else:
                                    result['lv2_unit_kerja_nama'] = ''
                            else:
                                result['lv2_unit_kerja_nama'] = ''

                            # get relation lv3_unit_kerja
                            if result['lv3_unit_kerja_id']:
                                data_lv3 = db.session.query(JabatanModel)
                                data_lv3 = data_lv3.filter(
                                    getattr(JabatanModel, "unit_kerja_id") == result['lv3_unit_kerja_id'])
                                data_lv3 = data_lv3.limit(1)
                                data_lv3 = data_lv3.one()
                                if data_lv3:
                                    dl3_ = data_lv3.__dict__
                                    result['lv3_unit_kerja_nama'] = dl3_[
                                        'unit_kerja_nama']
                                else:
                                    result['lv3_unit_kerja_nama'] = ''
                            else:
                                result['lv3_unit_kerja_nama'] = ''

                            # get relation lv4_unit_kerja
                            if result['lv4_unit_kerja_id']:
                                data_lv4 = db.session.query(JabatanModel)
                                data_lv4 = data_lv4.filter(
                                    getattr(JabatanModel, "unit_kerja_id") == result['lv4_unit_kerja_id'])
                                data_lv4 = data_lv4.limit(1)
                                data_lv4 = data_lv4.one()
                                if data_lv4:
                                    dl4_ = data_lv4.__dict__
                                    result['lv4_unit_kerja_nama'] = dl4_[
                                        'unit_kerja_nama']
                                else:
                                    result['lv4_unit_kerja_nama'] = ''
                            else:
                                result['lv4_unit_kerja_nama'] = ''
                    else:
                        result['satuan_kerja_nama'] = ''
                        result['lv1_unit_kerja_nama'] = ''
                        result['lv2_unit_kerja_nama'] = ''
                        result['lv3_unit_kerja_nama'] = ''
                        result['lv4_unit_kerja_nama'] = ''
                        result['jabatan_nama'] = ''
                except Exception as err:
                    result['satuan_kerja_nama'] = ''
                    result['lv1_unit_kerja_nama'] = ''
                    result['lv2_unit_kerja_nama'] = ''
                    result['lv3_unit_kerja_nama'] = ''
                    result['lv4_unit_kerja_nama'] = ''
                    result['jabatan_nama'] = ''

                # agreement
                try:
                    if result['agreement_id']:
                        result_agreement = db.session.query(AgreementModel)
                        result_agreement = result_agreement.get(
                            result['agreement_id'])
                        if result_agreement:
                            result_agreement = result_agreement.__dict__
                            result_agreement.pop('_sa_instance_state', None)
                            result_agreement['cdate'] = result_agreement['cdate'].strftime(
                                "%Y-%m-%d %H:%M:%S")
                            result_agreement['mdate'] = result_agreement['mdate'].strftime(
                                "%Y-%m-%d %H:%M:%S")
                            if result_agreement['is_active']:
                                result['is_agree'] = True
                            else:
                                result['is_agree'] = False
                            result['agreement'] = result_agreement
                        else:
                            result['is_agree'] = False
                            result["agreement"] = {
                                "agreement_text": "",
                                "cdate": "",
                                "id": "",
                                "mdate": "",
                                "is_active": "",
                                "is_deleted": "",
                                "version": "",
                                "notes": "",
                                "cuid": "",
                                "muid": "",
                            }
                    else:
                        result['is_agree'] = False
                        result["agreement"] = {
                            "agreement_text": "",
                            "cdate": "",
                            "id": "",
                            "mdate": "",
                            "is_active": "",
                            "is_deleted": "",
                            "version": "",
                            "notes": "",
                            "cuid": "",
                            "muid": "",
                        }
                except Exception as err:
                    result['is_agree'] = False
                    result["agreement"] = {
                        "agreement_text": "",
                        "cdate": "",
                        "id": "",
                        "mdate": "",
                        "is_active": "",
                        "is_deleted": "",
                        "version": "",
                        "notes": "",
                        "cuid": "",
                        "muid": "",
                    }

            else:
                result = {}

            # change into dict
            user = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as err:
            # fail response
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_username(self, username):
        ''' Doc: function get by username  '''
        try:
            # where
            where = {'username': username}

            # query postgresql
            result = db.session.query(UserModel)
            for attr, value in where.items():
                result = result.filter(getattr(UserModel, attr) == value)
            result = result.first()

            # check if empty
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                return True, result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            if 'password' in json_send:
                json_send['password'] = HELPER.generate_hash(json_send['password'])

            # find if exist
            exist, data = self.get_by_username(json_send["username"])

            # execute database
            if not exist:
                # prepare data model
                result = UserModel(**json_send)

                # execute database
                db.session.add(result)
                db.session.commit()
                result = result.to_dict()
                res, user = self.get_by_id(result['id'])

                # check if exist
                if res:
                    return True, user, ""
                else:
                    return False, {}, ""

            else:
                return False, {}, "username already exist"

        except Exception as err:
            # fail response
            print(err)
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            if "password" in json_send:
                json_send["password"] = HELPER.generate_hash(json['password'])
                # json_send["mdate"] = helper.local_date_server()

            try:
                # prepare data model
                result = db.session.query(UserModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()

                # get data
                res, user = self.get_by_id(where["id"])

                # update materialized view
                # thread = Thread(target=self.refresh_materialized_view)
                # thread.daemon = True
                # thread.start()

                # check if empty
                if res:
                    return True, user
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(UserModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, user = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_password(self, _id):
        ''' Doc: function get password  '''
        try:
            # execute database
            result = db.session.query(UserModel.password)
            result = result.filter(UserModel.id == _id)
            result = result.one()

            output = {}
            output['pwd'] = result[0]

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def change(self, _id, json):
        ''' Doc: function change  '''
        try:
            ret, res = self.get_password(_id)
            param = {}
            user_pass_ok = HELPER.verify_hash(json["old"], res["pwd"])

            if user_pass_ok:
                param["password"] = json['password']
                status, result = self.update({'id': _id}, param)
                return True, result, "Success update password"
            else:
                return False, {}, "Old password not match"

        except Exception as err:
            return str(err)

    def get_download(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(UserViewModel)
            for attr, value in where.items():
                result = result.filter(getattr(UserViewModel, attr) == value)
            result = result.filter(
                getattr(UserViewModel, 'is_deleted') == False)
            result = result.filter(or_(
                cast(getattr(UserViewModel, "username"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "email"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "role_title"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "role_name"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "jabatan_nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserViewModel, "satuan_kerja_nama"),
                     sqlalchemy.String).ilike('%'+search+'%'),
            ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.options(
                defer("password"))
            result = result.all()
            # result = result.distinct()

            # change into dict
            user = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)

                temp = {}
                temp['id'] = res['id']
                temp['username'] = res['username']
                temp['nip'] = res['nip']
                temp['email'] = res['email']
                temp['name'] = res['name']
                temp['kode_kemendagri'] = res['kode_kemendagri']
                temp['kode_skpd'] = res['kode_skpd']
                temp['kode_skpdsub'] = res['kode_skpdsub']
                temp['kode_skpdunit'] = res['kode_skpdunit']
                temp['profile_pic'] = res['profile_pic']
                if res['last_login']:
                    temp['last_login'] = res['last_login'].strftime("%Y-%m-%d")
                else:
                    temp['last_login'] = None
                temp['notes'] = res['notes']
                temp['cuid'] = res['cuid']
                if res['cdate']:
                    temp['cdate'] = res['cdate'].strftime("%Y-%m-%d")
                else:
                    temp['cdate'] = None
                temp['muid'] = res['muid']
                if res['mdate']:
                    temp['mdate'] = res['mdate'].strftime("%Y-%m-%d")
                else:
                    temp['mdate'] = None
                temp['is_active'] = res['is_active']
                temp['is_deleted'] = res['is_deleted']
                temp['is_agree'] = res['is_agree']
                temp['count_notif'] = res['count_notif']
                temp['agreement_id'] = res['agreement_id']
                temp['agreement_version'] = res['agreement_version']
                temp['agreement_notes'] = res['agreement_notes']
                temp['agreement_is_active'] = res['agreement_is_active']
                temp['agreement_is_deleted'] = res['agreement_is_deleted']
                temp['agreement_cuid'] = res['agreement_cuid']
                if res['agreement_cdate']:
                    temp['agreement_cdate'] = res['agreement_cdate'].strftime(
                        "%Y-%m-%d")
                else:
                    temp['agreement_cdate'] = None
                temp['agreement_muid'] = res['agreement_muid']
                if res['agreement_mdate']:
                    temp['agreement_mdate'] = res['agreement_mdate'].strftime(
                        "%Y-%m-%d")
                else:
                    temp['agreement_mdate'] = None
                temp['skpd_id'] = res['skpd_id']
                temp['skpd_title'] = res['skpd_title']
                temp['skpd_kode_skpd'] = res['skpd_kode_skpd']
                temp['skpd_nama_skpd'] = res['skpd_nama_skpd']
                temp['skpd_nama_skpd_alias'] = res['skpd_nama_skpd_alias']
                temp['skpd_logo'] = res['skpd_logo']
                temp['skpd_description'] = res['skpd_description']
                temp['skpd_address'] = res['skpd_address']
                temp['skpd_phone'] = res['skpd_phone']
                temp['skpd_email'] = res['skpd_email']
                temp['skpdsub_id'] = res['skpdsub_id']
                temp['skpdsub_kode_skpd'] = res['skpdsub_kode_skpd']
                temp['skpdsub_kode_skpdsub'] = res['skpdsub_kode_skpdsub']
                temp['skpdsub_nama_skpd'] = res['skpdsub_nama_skpd']
                temp['skpdsub_nama_skpdsub'] = res['skpdsub_nama_skpdsub']
                temp['skpdsub_skpd_id'] = res['skpdsub_skpd_id']
                temp['skpdunit_id'] = res['skpdunit_id']
                temp['skpdunit_kode_skpd'] = res['skpdunit_kode_skpd']
                temp['skpdunit_kode_skpdsub'] = res['skpdunit_kode_skpdsub']
                temp['skpdunit_kode_skpdunit'] = res['skpdunit_kode_skpdunit']
                temp['skpdunit_nama_skpd'] = res['skpdunit_nama_skpd']
                temp['skpdunit_nama_skpdsub'] = res['skpdunit_nama_skpdsub']
                temp['skpdunit_nama_skpdunit'] = res['skpdunit_nama_skpdunit']
                temp['skpdunit_skpdsub_id'] = res['skpdunit_skpdsub_id']
                temp['role_id'] = res['role_id']
                temp['role_name'] = res['role_name']
                temp['role_title'] = res['role_title']
                temp['jabatan_id'] = res['jabatan_id']
                temp['jabatan_nama'] = res['jabatan_nama']
                temp['satuan_kerja_id'] = res['satuan_kerja_id']
                temp['satuan_kerja_nama'] = res['satuan_kerja_nama']
                temp['lv1_unit_kerja_id'] = res['lv1_unit_kerja_id']
                temp['lv1_unit_kerja_nama'] = res['lv1_unit_kerja_nama']
                temp['lv2_unit_kerja_id'] = res['lv2_unit_kerja_id']
                temp['lv2_unit_kerja_nama'] = res['lv2_unit_kerja_nama']
                temp['lv3_unit_kerja_id'] = res['lv3_unit_kerja_id']
                temp['lv3_unit_kerja_nama'] = res['lv3_unit_kerja_nama']
                temp['lv4_unit_kerja_id'] = res['lv4_unit_kerja_id']
                temp['lv4_unit_kerja_nama'] = res['lv4_unit_kerja_nama']
                temp['level_unit_kerja'] = res['level_unit_kerja']

                user.append(temp)

            # check if empty
            user = list(user)
            metadata = [
                'id', 'username', 'nip', 'email', 'name', 'kode_kemendagri', 'kode_skpd', 'kode_skpdsub', 'kode_skpdunit',
                'profile_pic', 'last_login', 'notes', 'cuid', 'cdate', 'muid', 'mdate', 'is_active', 'is_deleted', 'is_agree', 'count_notif',
                'agreement_id', 'agreement_version', 'agreement_notes', 'agreement_is_active', 'agreement_is_deleted',
                'agreement_cuid', 'agreement_cdate', 'agreement_cdate', 'agreement_muid', 'agreement_mdate',
                'skpd_id', 'skpd_title', 'skpd_kode_skpd', 'skpd_nama_skpd', 'skpd_nama_skpd_alias', 'skpd_logo', 'skpd_description',
                'skpd_address', 'skpd_phone', 'skpd_email',
                'skpdsub_id', 'skpdsub_kode_skpd', 'skpdsub_kode_skpdsub', 'skpdsub_nama_skpd', 'skpdsub_nama_skpdsub', 'skpdsub_skpd_id',
                'skpdunit_id', 'skpdunit_kode_skpd', 'skpdunit_kode_skpdsub', 'skpdunit_kode_skpdunit', 'skpdunit_nama_skpd', 'skpdunit_nama_skpdsub',
                'skpdunit_nama_skpdunit', 'skpdunit_skpdsub_id',
                'role_id', 'role_name', 'role_title', 'jabatan_id', 'jabatan_nama', 'satuan_kerja_id', 'satuan_kerja_nama',
                'lv1_unit_kerja_id', 'lv1_unit_kerja_nama', 'lv2_unit_kerja_id', 'lv2_unit_kerja_nama',
                'lv3_unit_kerja_id', 'lv3_unit_kerja_nama', 'lv4_unit_kerja_id', 'lv4_unit_kerja_nama', 'level_unit_kerja'
            ]
            if user:
                return True, user, metadata
            else:
                return False, [], metadata

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    # def refresh_materialized_view(self):
    #     ''' Doc: function refresh_materialized_view  '''
    #     print("Task #1 started!")
    #     try:
    #         is_success, result = db2.commit(
    #             'REFRESH MATERIALIZED VIEW user_view')
    #     except Exception as err:
    #         print(err)
    #     print("Task #1 is done!")
