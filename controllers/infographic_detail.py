''' Doc: controller infographic detail  '''
from helpers import Helper
from exceptions import ErrorMessage
from models import InfographicDetailModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
from sqlalchemy.orm import defer
import sqlalchemy, sentry_sdk
import re

HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument
class InfographicDetailController(object):
    ''' Doc: class infographic detail  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(InfographicDetailModel)
            result = result.options(
                defer("cuid"),
                defer("muid"))
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(InfographicDetailModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(InfographicDetailModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(InfographicDetailModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(InfographicDetailModel, "description"), sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            result = self.query(where, search)
            # check public or private
            jwt = HELPER.read_jwt()
            if not jwt:
                result = result.filter(InfographicDetailModel.dataset_class_id == 3)
            result = result.order_by(text("infographic_detail."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            infographic_detail = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATETIME)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATETIME)

                infographic_detail.append(temp)

            # check if empty
            if infographic_detail:
                return True, infographic_detail
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(InfographicDetailModel)

            # check public or private
            jwt = HELPER.read_jwt()
            if jwt:
                result = result.get(_id)
            else:
                result = result.filter(InfographicDetailModel.id == _id)
                result = result.all()
                if result:
                    result = result[0]

            # change into dict
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                result['cdate'] = result['cdate'].strftime(FORMAT_DATETIME)
                result['mdate'] = result['mdate'].strftime(FORMAT_DATETIME)

            else:
                result = {}

            # check if empty
            if result:
                return True, result, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cuid"] = jwt['id']
            json_send["cdate"] = HELPER.local_date_server()

            # title
            json_send["title"] = json_send["name"].lower().replace(' ', '-')
            json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(InfographicDetailModel.id)
            res_count = res_count.filter(InfographicDetailModel.title == json_send["title"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            result = InfographicDetailModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, infographic_detail, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, infographic_detail
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            if 'name' in json_send:
                json_send["title"] = json_send['name'].lower().replace(" ", "-")
                json_send["mdate"] = HELPER.local_date_server()
                json_send["muid"] = jwt['id']


            try:
                # prepare data model
                result = db.session.query(InfographicDetailModel)
                for attr, value in where.items():
                    result = result.filter(getattr(InfographicDetailModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, dataset, message = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, dataset
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(InfographicDetailModel)
                for attr, value in where.items():
                    result = result.filter(getattr(InfographicDetailModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, dataset, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
