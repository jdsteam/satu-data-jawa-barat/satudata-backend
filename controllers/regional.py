''' Doc: controller regional  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import RegionalModel
from models import RegionalLevelModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class RegionalController(object):
    ''' Doc: controller regional  '''

    def __init__(self, **kwargs):
        ''' Doc: function init '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all '''
        try:
            # query postgresql
            result = db.session.query(RegionalModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"))
            for attr, value in where.items():
                result = result.filter(getattr(RegionalModel, attr) == value)
            result = result.filter(or_(cast(getattr(RegionalModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in RegionalModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            regional = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                # get relation to regional_level
                regional_level = db.session.query(RegionalLevelModel)
                regional_level = regional_level.get(temp['regional_level_id'])

                if regional_level:
                    regional_level = regional_level.__dict__
                    regional_level.pop('_sa_instance_state', None)
                else:
                    regional_level = {}

                temp['regional_level'] = regional_level

                regional.append(temp)

            # check if empty
            regional = list(regional)
            if regional:
                return True, regional
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count '''
        try:
            # query postgresql
            result = db.session.query(RegionalModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(RegionalModel, attr) == value)
            result = result.filter(or_(cast(getattr(RegionalModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in RegionalModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id '''
        try:
            # execute database
            result = db.session.query(RegionalModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to regional
                regional_level = db.session.query(RegionalLevelModel)
                regional_level = regional_level.get(result['regional_level_id'])

                if regional_level:
                    regional_level = regional_level.__dict__
                    regional_level.pop('_sa_instance_state', None)
                else:
                    regional_level = {}

                result['regional_level'] = regional_level

            else:
                result = {}

            # change into dict
            regional = result

            # check if empty
            if regional:
                return True, regional
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = RegionalModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, regional = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, regional
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(RegionalModel)
                for attr, value in where.items():
                    result = result.filter(getattr(RegionalModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, regional = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, regional
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete '''
        try:

            try:
                # prepare data model
                result = db.session.query(RegionalModel)
                for attr, value in where.items():
                    result = result.filter(getattr(RegionalModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, regional = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
