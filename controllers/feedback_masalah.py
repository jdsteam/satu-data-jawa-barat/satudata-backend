''' Doc: controller feedback masalah  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import FeedbackMasalahModel
from models import FeedbackModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, func, cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=singleton-comparison, unused-variable
class FeedbackMasalahController(object):
    ''' Doc: class feedback masalah  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                FeedbackMasalahModel.id, FeedbackMasalahModel.datetime, FeedbackMasalahModel.feedback_id,
                FeedbackMasalahModel.masalah, FeedbackMasalahModel.is_active, FeedbackMasalahModel.is_deleted
            )
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(FeedbackMasalahModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(FeedbackMasalahModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(FeedbackMasalahModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(FeedbackMasalahModel, "masalah"), sqlalchemy.String).ilike('%'+search+'%')
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("feedback_masalah."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            feedback_masalah = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})

                feedback_masalah.append(temp)

            # check if empty
            feedback_masalah = list(feedback_masalah)
            if feedback_masalah:
                return True, feedback_masalah
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id':_id}, '')
            result = result.all()

            # change into dict
            feedback_masalah = []
            for res in result:
                temp = res._asdict()

                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                feedback_masalah.append(temp)

            # check if empty
            if feedback_masalah:
                return True, feedback_masalah, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["datetime"] = HELPER.local_date_server()
            # prepare data model
            result = FeedbackMasalahModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, feedback_masalah, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, feedback_masalah
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["user_id"] = jwt['id']
            json_send["datetime"] = HELPER.local_date_server()

            try:
                # prepare data model
                result = db.session.query(FeedbackMasalahModel)
                for attr, value in where.items():
                    result = result.filter(getattr(FeedbackMasalahModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, feedback_masalah, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, feedback_masalah
                else:
                    return False, {}

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(FeedbackMasalahModel)
                for attr, value in where.items():
                    result = result.filter(getattr(FeedbackMasalahModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, feedback_masalah, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                sentry_sdk.capture_exception(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_chart_masalah(self, start_date, end_date):
        ''' Doc: function get chart masalah  '''
        try:
            # query postgresql
            result = db.session.query(FeedbackMasalahModel.masalah, func.count(FeedbackMasalahModel.id).label('count'))
            result = result.join(FeedbackModel, FeedbackModel.id == FeedbackMasalahModel.feedback_id)
            if start_date:
                result = result.filter(text("feedback.datetime::date >='" + start_date + "'"))
            if end_date:
                result = result.filter(text("feedback.datetime::date <='" + end_date + "'"))
            result = result.filter(getattr(FeedbackModel, 'is_active') == True)
            result = result.filter(getattr(FeedbackModel, 'is_deleted') == False)
            result = result.group_by(FeedbackMasalahModel.masalah)
            result = result.order_by(text("feedback_masalah.masalah asc"))
            result = result.all()

            # change into dict
            history_download = []
            for res in result:
                temp = res._asdict()
                history_download.append(temp)

            # check if empty
            history_download = list(history_download)
            if history_download:
                return True, history_download
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
