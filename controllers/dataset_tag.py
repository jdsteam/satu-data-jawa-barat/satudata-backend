''' Doc: controller dataset_tag  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import DatasetTagModel
from models import DatasetModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import distinct
from sqlalchemy import or_
from sqlalchemy import func
from sqlalchemy import cast
import sqlalchemy
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison, unused-variable


class DatasetTagController(object):
    ''' Doc: class dataset tag  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip, distinct_, admin):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            if distinct_:
                sort[0] = "tag"
                result = db.session.query(func.distinct(
                    func.lower(DatasetTagModel.tag)).label('tag'))
                # result = result.join(
                # DatasetModel, DatasetModel.id == DatasetTagModel.dataset_id)
                # result = result.filter(
                # getattr(DatasetModel, 'is_deleted') == False)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(DatasetTagModel, attr) == value)
                result = result.filter(or_(cast(getattr(DatasetTagModel, col.name), sqlalchemy.String).ilike(
                    '%'+search+'%') for col in DatasetTagModel.__table__.columns))
                result = result.order_by(text('tag'+" "+'asc'))
                result = result.order_by(text(sort[0]+" "+sort[1]))
                if admin:
                    result = result.offset(skip).limit(limit)
                result = result.all()
            else:
                result = db.session.query(DatasetTagModel)
                # result = result.join(
                # DatasetModel, DatasetModel.id == DatasetTagModel.dataset_id)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(DatasetTagModel, attr) == value)
                result = result.filter(or_(cast(getattr(DatasetTagModel, col.name), sqlalchemy.String).ilike(
                    '%'+search+'%') for col in DatasetTagModel.__table__.columns))
                result = result.order_by(text('tag'+" "+'asc'))
                result = result.order_by(text(sort[0]+" "+sort[1]))
                result = result.offset(skip).limit(limit)

            # change into dict
            dataset_tag = []
            if distinct_:
                for res in result:
                    temp = {}
                    temp['tag'] = res[0]
                    dataset_tag.append(temp)
            else:
                for res in result:
                    temp = res.__dict__
                    temp.pop('_sa_instance_state', None)
                    temp['tag'] = temp['tag'].lower()
                    dataset_tag.append(temp)

            # check if empty
            dataset_tag = list(dataset_tag)
            if dataset_tag:
                return True, dataset_tag
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_distinct_skpd(self, kode_skpd, where: dict):
        ''' Doc: function get dinstinct skpd  '''
        try:
            # query postgresql
            result = db.session.query(
                distinct(func.lower(DatasetTagModel.tag)))
            result = result.join(
                DatasetModel, DatasetModel.id == DatasetTagModel.dataset_id)
            result = result.filter(
                getattr(DatasetModel, "kode_skpd") == kode_skpd)
            result = result.filter(
                getattr(DatasetModel, 'is_deleted') == False)
            for attr, value in where.items():
                result = result.filter(getattr(DatasetModel, attr) == value)
            result = result.all()

            dataset_tag = []
            for res in result:
                temp = {}
                temp['tag'] = res[0].lower()

                dataset_tag.append(temp)

            # check if empty
            dataset_tag = list(dataset_tag)
            if dataset_tag:
                return True, dataset_tag
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search, distinct_, admin):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            if distinct_:
                result = db.session.query(func.distinct(
                    func.lower(DatasetTagModel.tag)).label('tag'))
            else:
                result = db.session.query(DatasetTagModel.id)

            for attr, value in where.items():
                result = result.filter(getattr(DatasetTagModel, attr) == value)
            result = result.filter(or_(cast(getattr(DatasetTagModel, col.name), sqlalchemy.String).ilike(
                '%'+search+'%') for col in DatasetTagModel.__table__.columns))
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(DatasetTagModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            dataset_tag = result

            # check if empty
            if dataset_tag:
                return True, dataset_tag
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            # prepare data model
            result = DatasetTagModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, dataset_tag = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, dataset_tag
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = DatasetTagModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, dataset_tag = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(dataset_tag)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json

            try:
                # prepare data model
                result = db.session.query(DatasetTagModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(DatasetTagModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, dataset_tag = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, dataset_tag
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_multiple(self, dataset_id, json: dict):
        ''' Doc: function update multiple  '''
        try:
            # filter data
            # where = {'dataset_id': dataset_id}

            result = db.session.query(DatasetTagModel)
            result = result.filter(DatasetTagModel.dataset_id == dataset_id)
            result = result.all()

            for res in result:
                db.session.delete(res)
                db.session.commit()

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

        try:
            jsons = json
            results = []

            for json in jsons:

                try:
                    # generate json data
                    json_send = {}
                    json_send = json

                    # prepare data model
                    result = DatasetTagModel(**json_send)

                    # execute database
                    db.session.add(result)
                    db.session.commit()
                    result = result.to_dict()
                    res, dataset_tag = self.get_by_id(result['id'])

                    # check if exist
                    if res:
                        results.append(dataset_tag)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(DatasetTagModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(DatasetTagModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, dataset_tag = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete_tag(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(DatasetTagModel)
                for attr, value in where.items():
                    result = result.filter(
                        getattr(DatasetTagModel, attr) == value)
                result = result.all()

                # execute database
                for res in result:
                    db.session.delete(res)
                    db.session.commit()

                return True

            except Exception as err:
                # fail response
                print(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_tag(self, where: dict):
        ''' Doc: function update  '''
        try:

            try:
                # prepare data model
                result = db.session.query(DatasetTagModel)
                result = result.filter(
                    getattr(DatasetTagModel, 'tag') == where['old_tag'])
                result = result.update(
                    {'tag': where['new_tag']}, synchronize_session='fetch')
                result = db.session.commit()

                return True

            except Exception as err:
                # fail response
                print(err)
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
