''' Doc: controller visualization access  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import VisualizationAccessModel, UserModel, SkpdModel, RoleModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, func
from sqlalchemy.orm import defer
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class VisualizationAccessController(object):
    ''' Doc: class visualization access  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(VisualizationAccessModel)
            for attr, value in where.items():
                result = result.filter(getattr(VisualizationAccessModel, attr) == value)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            visualization_dataset = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp.update({"cdate":temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                visualization_dataset.append(temp)

            # check if empty
            visualization_dataset = list(visualization_dataset)
            if visualization_dataset:
                return True, visualization_dataset
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(VisualizationAccessModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(VisualizationAccessModel, attr) == value)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(VisualizationAccessModel)
            result = result.options(
                defer("cuid"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                result['cdate'] = result['cdate'].strftime("%Y-%m-%d %H:%M:%S")
            else:
                result = {}

            # change into dict
            visualization_dataset = result

            # check if empty
            if visualization_dataset:
                return True, visualization_dataset, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_skpd(self, visualization_id):
        ''' Doc: function get skpd  '''
        try:
            # query postgresql
            result_access = db.session.query(VisualizationAccessModel)
            result_access = result_access.filter(getattr(VisualizationAccessModel, 'visualization_id') == visualization_id)
            result_access = result_access.all()

            # change into dict
            visualization_access = []
            data_skpd = []
            if result_access:
                for res in result_access:
                    temp = res.__dict__
                    temp.pop('_sa_instance_state', None)

                    # get kode skpd user
                    get_skpd = db.session.query(func.distinct(func.lower(UserModel.kode_skpd)).label('kode_skpd'))
                    get_skpd = get_skpd.filter(getattr(UserModel, 'id') == temp['user_id'])
                    get_skpd = get_skpd.all()

                    for skpd in get_skpd:
                        skpds = {}
                        skpds['kode_skpd'] = skpd[0]
                        visualization_access.append(skpds)

            newlist = []
            for i in visualization_access:
                if i not in newlist:
                    newlist.append(i)

            # query postgresql
            for nl_ in newlist:
                result_skpd = db.session.query(SkpdModel)
                result_skpd = result_skpd.filter(getattr(SkpdModel, 'kode_skpd') == nl_['kode_skpd'])
                result_skpd = result_skpd.one()

                result_skpd = result_skpd.__dict__
                result_skpd.pop('_sa_instance_state', None)

                data_skpd.append(result_skpd)

            if data_skpd:
                return True, data_skpd
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = VisualizationAccessModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()

            return True, json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_single(self, json: dict):
        ''' Doc: function create single  '''
        try:

            user_permission = []
            res = False

            result_user = db.session.query(UserModel)
            result_user = result_user.filter(getattr(UserModel, 'kode_skpd') == json['kode_skpd'])
            result_user = result_user.all()

            temp_send = []

            self.insert_walidata(json['visualization_id'])

            for res_us in result_user:
                temp_user = res_us.__dict__
                temp = {}
                temp['visualization_id'] = json['visualization_id']
                temp['user_id'] = temp_user['id']
                temp['enable'] = True
                temp_send.append(temp)

            for ts_ in temp_send:
                re_, da_ = self.create(ts_)
                res = re_
                if re_:
                    user_permission.append(da_)
            # check if exist
            if res:
                return True, user_permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_multiple(self, json: dict):
        ''' Doc: function create multiple  '''
        try:
            jsons = json
            results = []
            temp_send = []
            for json in jsons:
                try:
                    result_user = db.session.query(UserModel)
                    result_user = result_user.filter(getattr(UserModel, 'kode_skpd') == json['kode_skpd'])
                    result_user = result_user.all()

                    temp_send = []
                    for res_us in result_user:
                        temp_user = res_us.__dict__
                        temp = {}
                        temp['visualization_id'] = json['visualization_id']
                        temp['user_id'] = temp_user['id']
                        temp['enable'] = True
                        temp_send.append(temp)

                except Exception as err:
                    # fail response
                    sentry_sdk.capture_exception(err)
                    raise ErrorMessage(str(err), 500, 1, {})

                for ts_ in temp_send:
                    self.insert_walidata(ts_['visualization_id'])
                    re_, da_ = self.create(ts_)
                    if re_:
                        results.append(da_)

            return True, results

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(VisualizationAccessModel)
                for attr, value in where.items():
                    result = result.filter(getattr(VisualizationAccessModel, attr) == value)
                result = result.delete()

                # execute database
                db.session.commit()

                return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def insert_walidata(self, visualization_id):
        ''' Doc: function insert walidata  '''
        try:
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)
            data_user = data_user.filter(RoleModel.name == 'walidata')
            data_user = data_user.all()

            for res_us in data_user:
                res_us = res_us.__dict__
                res_us.pop('_sa_instance_state', None)

                temp = {}
                temp['user_id'] = res_us['id']
                temp['visualization_id'] = visualization_id
                temp['enable'] = True

                self.create(temp)

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
