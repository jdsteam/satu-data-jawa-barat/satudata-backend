''' Doc: controller dashboard  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
import sentry_sdk
from datetime import timedelta
from dateutil import parser
from collections import Counter

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison
class DashboardRequestPublicController(object):
    ''' Doc: constructor dashboard  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def filter_type(self, start_date, end_date, types):
        ''' Doc: function filter date type  '''
        try:
            start = parser.parse(start_date)
            end = parser.parse(end_date)
            diff = end - start
            distance_day = (diff.days + 1)
            max_days = 30

            parse_growth_end = parser.parse(start_date)
            parser_end_date = parse_growth_end - timedelta(days = 1)
            end_date_growth = parser_end_date.strftime("%Y-%m-%d")

            if types == "default":
                growth_date = start - timedelta(days = max_days)
                total_days = max_days
            else:
                growth_date = start - timedelta(days = distance_day)
                total_days = distance_day

            start_date_growth = growth_date.strftime("%Y-%m-%d")

            return start_date_growth, total_days, end_date_growth
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            return err, False

    def filter_trend(self, group):
        ''' Doc: function filter query trend  '''
        try:
            if group == "month":
                select = "date_trunc('month',datetime) as date, "
                group_by = "group by date_trunc('month',datetime) "
                order_by = "order by date asc"
            elif group == "week":
                select = "date_trunc('week',datetime) as date, "
                group_by = "group by date_trunc('week',datetime) "
                order_by = "order by date asc"
            elif group == "day":
                select = "date_trunc('day',datetime) as date, "
                group_by = "group by date_trunc('day',datetime) "
                order_by = "order by date asc"
            else:
                select = "date(datetime), "
                group_by = "group by date(datetime) "
                order_by = "order by date(datetime) asc"

            return select, group_by, order_by
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            return err

    def get_total(self, start_date, end_date, types):
        ''' Doc: function get request_public total  '''
        try:
            start, distance_day, end = self.filter_type(start_date, end_date, types)
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)

            # total & growth
            sql = text(
                "select count(*) as total, "+
                "(select count(*) from request_public "+
                "where is_active = true and is_deleted = false "+
                "and datetime::date >= '" + start + "'" +
                "and datetime::date <= '" + end + "'" +
                ") as growth from request_public " +
                "where is_active = true and is_deleted = false "+ filters
            )
            result = db.engine.execute(sql)
            res = [row for row in result]
            try:
                total_growth = ((res[0].total - res[0].growth) / res[0].growth) * 100
            except ZeroDivisionError:
                total_growth = 0

            # count per status
            sql_status = text(
                "select 'Diajukan' as status, count(*) from request_public where is_active = true and is_deleted = false and status = 1 " + filters +
                "union " +
                "select 'Diproses' as status, count(*) from request_public where is_active = true and is_deleted = false and status = 2 " + filters +
                "union " +
                "select 'Ditolak' as status, count(*) from request_public where is_active = true and is_deleted = false and status = 3 " + filters +
                "union " +
                "select 'Diakomodir' as status, count(*) from request_public where is_active = true and is_deleted = false and status = 4 " + filters
            )
            result_status = db.engine.execute(sql_status)
            output_status = []
            for res_status in [dict(row_status) for row_status in result_status]:
                row_append = res_status
                output_status.append(row_append)

            # check if empty
            app_service = {
                "total": res[0].total,
                "growth": total_growth,
                "status": output_status,
                "message": "dalam %s hari terakhir" % distance_day
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_tau_skpd(self, start_date, end_date, types):
        ''' Doc: function get request_public tau skpd  '''
        try:
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)

            # count per tau skpd
            sql_status = text(
                "select 'Mengetahui' as status, count(*) from request_public where is_active = true and is_deleted = false and tau_skpd = 't' " + filters +
                "union " +
                "select 'Tidak Mengetahui' as status, count(*) from request_public where is_active = true and is_deleted = false and tau_skpd = 'f' " + filters
            )
            result_status = db.engine.execute(sql_status)
            output_status = []
            for res_status in [dict(row_status) for row_status in result_status]:
                row_append = res_status
                output_status.append(row_append)

            # check if empty
            if output_status:
                return True, output_status
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_skpd_tujuan(self, start_date, end_date, types):
        ''' Doc: function get request_public skpd tujuan  '''
        try:
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)
            sql = text(
                "select nama_skpd, count(*) " +
                "from request_public " +
                "where is_active = true and is_deleted = false "+ filters + " " +
                "GROUP BY nama_skpd ORDER BY count desc"
            )
            result = db.engine.execute(sql)
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_pekerjaan(self, start_date, end_date, types):
        ''' Doc: function get request_public pekerjaan  '''
        try:
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)

            # query
            value_condition = [
                'Pelajar/ Mahasiswa/ Akademisi',
                'Profesional',
                'Wirausaha',
                'Pemerintahan'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select pekerjaan, count(pekerjaan) as total from request_public "
            select_from_not = "select 'Lainnya' as pekerjaan, count(pekerjaan) as total from request_public "
            condition = "where request_public.is_active = true and request_public.is_deleted = false " +filters + "and pekerjaan "
            group_by = "group by pekerjaan "
            sql = text(
                select_from_in + condition + " in " + "(" +manipulate_array+ ")" +
                group_by + " union " + select_from_not  + condition + " not in " + "(" +manipulate_array+ ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_tujuan_data(self, start_date, end_date, types, pekerjaan):
        ''' Doc: function get request_public tujuan_data  '''
        try:
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)

            # query
            value_condition = [
                'Referensi Kajian Bisnis',
                'Referensi Pembuatan Kebijakan',
                'Referensi Pembuatan Kurikulum/ Bahan Ajar',
                'Referensi Tugas/ Karya Ilmiah',
                'Referensi Pribadi'
            ]
            manipulate_array = ', '.join(f"'{w}'" for w in value_condition)
            select_from_in = "select tujuan_data, count(tujuan_data) as total from request_public "
            select_from_not = "select 'Lainnya' as tujuan_data, count(tujuan_data) as total from request_public "
            condition = "where request_public.is_active = true and request_public.is_deleted = false " +filters
            if pekerjaan:
                condition += "and pekerjaan = '"+pekerjaan+"' "
            condition += "and tujuan_data "
            group_by = "group by tujuan_data "
            sql = text(
                select_from_in + condition + " in " + "(" +manipulate_array+ ")" +
                group_by + " union " + select_from_not  + condition + " not in " + "(" +manipulate_array+ ")" +
                " order by total desc;"
            )
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_bidang_ilmu_usaha(self, start_date, end_date, types, pekerjaan):
        ''' Doc: function get request_public bidang_ilmu_usaha  '''
        try:
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)

            # query
            bidang_ilmu = [
                'Agama dan Filsafat',
                'Bahasa, Sosial, dan Humaniora',
                'Ekonomi',
                'Hewani',
                'Kedokteran dan Kesehatan',
                'Matematika dan Pengetahuan Alam',
                'Pendidikan',
                'Seni, Desain, dan Media',
                'Tanaman',
                'Teknik',
            ]
            bidang_usaha = [
                'Pertanian (Agrikultur)',
                'Pertambangan',
                'Pabrikasi (Manufaktur)',
                'Kontruksi',
                'Perdagangan',
                'Jasa Keuangan',
                'Jasa Perorangan',
                'Jasa Umum',
                'Jasa Wisata',
            ]
            if pekerjaan == 'Pelajar/ Mahasiswa/ Akademisi':
                manipulate_array = ', '.join(f"'{w}'" for w in bidang_ilmu)
            elif pekerjaan == 'Profesional':
                manipulate_array = ', '.join(f"'{w}'" for w in bidang_usaha)
            elif pekerjaan == 'Wirausaha':
                manipulate_array = ', '.join(f"'{w}'" for w in bidang_usaha)
            elif pekerjaan == 'Pemerintahan':
                manipulate_array = ''
            else:
                manipulate_array = ''
            select_from_in = "select bidang, count(bidang) as total from request_public "
            select_from_not = "select 'Lainnya' as bidang, count(bidang) as total from request_public "
            condition = "where request_public.is_active = true and request_public.is_deleted = false " +filters + " "
            if pekerjaan:
                condition += "and pekerjaan = '"+pekerjaan+"' "
            group_by = " group by bidang "
            if manipulate_array:
                sql = text(
                    select_from_in + condition + " and bidang  in " + "(" +manipulate_array+ ")" +
                    group_by + " union " + select_from_not  + condition + " and bidang not in " + "(" +manipulate_array+ ")" +
                    " order by total desc;"
                )
            else:
                sql = text(
                    select_from_in + condition +
                    group_by + " union " + select_from_not  + condition +
                    " order by total desc;"
                )
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_trend(self, start_date, end_date, group):
        ''' Doc: function get request_public trend  '''
        try:
            # query default
            sql_day = '''
            SELECT rps.date,
            (select count(rp.id) from request_public rp where rp.status = 1 and rp.datetime::date = rps.date) as "diajukan",
            (select count(rp.id) from request_public rp where rp.status = 2 and rp.datetime::date = rps.date) as "diproses",
            (select count(rp.id) from request_public rp where rp.status = 3 and rp.datetime::date = rps.date) as "ditolak",
            (select count(rp.id) from request_public rp where rp.status = 4 and rp.datetime::date = rps.date) as "diakomodir"
            FROM request_public_series_view rps
            '''
            filters = " WHERE date >='"+start_date+"' and date <='"+end_date+"' "

            # check group
            if group == 'day':
                sql = sql_day + filters
            elif group == 'week':
                sql = '''
                SELECT (date_trunc('week'::text, (request_public_series_view.date)::timestamp with time zone))::date AS date,
                sum(request_public_series_view.diajukan) as "diajukan",
                sum(request_public_series_view.diproses) as "diproses",
                sum(request_public_series_view.ditolak) as "ditolak",
                sum(request_public_series_view.diakomodir) as "diakomodir"
                FROM
                ( ''' + sql_day + ''') as request_public_series_view ''' + filters + '''
                GROUP BY ((date_trunc('week'::text, (request_public_series_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('week'::text, (request_public_series_view.date)::timestamp with time zone))::date)
                '''
            elif group == 'month':
                sql = '''
                SELECT (date_trunc('month'::text, (request_public_series_view.date)::timestamp with time zone))::date AS date,
                sum(request_public_series_view.diajukan) as "diajukan",
                sum(request_public_series_view.diproses) as "diproses",
                sum(request_public_series_view.ditolak) as "ditolak",
                sum(request_public_series_view.diakomodir) as "diakomodir"
                FROM
                ( ''' + sql_day + ''') as request_public_series_view ''' + filters + '''
                GROUP BY ((date_trunc('month'::text, (request_public_series_view.date)::timestamp with time zone))::date)
                ORDER BY ((date_trunc('month'::text, (request_public_series_view.date)::timestamp with time zone))::date)
                '''
            result = db.engine.execute(sql)

            # maping object
            output = []
            for res in [dict(row) for row in result]:
                row_append = res
                row_append.update({"date": res['date'].strftime("%Y-%m-%d")})
                row_append.update({"diajukan": round(res['diajukan'])})
                row_append.update({"diproses": round(res['diproses'])})
                row_append.update({"ditolak": round(res['ditolak'])})
                row_append.update({"diakomodir": round(res['diakomodir'])})

                output.append(row_append)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_keyword(self, start_date, end_date, types):
        ''' Doc: function get request_public keyword  '''
        try:
            if types == "default":
                filters = ""
            else:
                filters = "and datetime::date >='%s' and datetime::date <='%s'" % (start_date, end_date)

            # get all data
            sql = text(
                "select judul_data from request_public " +
                "where 1 = 1 "+ filters
            )
            result = db.engine.execute(sql)

            # split by word
            keyword = []
            for res in [dict(row) for row in result]:
                array = res['judul_data'].split()
                for arr in array:
                    keyword.append(arr.lower())

            # add counter
            counts = Counter(keyword)
            counts = counts.most_common()

            # check if empty
            result_data = []
            for count in counts:
                result_data.append({'keyword': count[0], 'count': count[1]})

            # clean word
            dikecualikan = [
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'indeks', 'index', 'tingkat',
                'dataset', 'data', 'dan', 'atau', 'di', 'ke', 'test', 'tes', 'contoh', 'yang', 'sampai',
                'pada', 'daerah', 'tahun', 'jumlah', 'total', 'jawa', 'barat', 'jabar',
                'kabupaten', 'kota', 'kab', 'kabupaten/kota', 'prov', 'provinsi',
                'berdasarkan', 'per', 'menurut', '-', '/', '&',

                'terbaru','jenis','setiap','standar','cair','diperbolehkan','dilepaskan','minimal','untuk',
                'jenjang','nama','sudah','memiliki','triwulan','atas','dasar','berlaku','sesuai','teknis',
                'dalam','pengembangan','berat','berdasar','kelompok','sektor','baku','hasil','kapasitas',
                'dengan','angka','melek','huruf','seluruh','bulan','dipakai','tipe','analisis','soulsi',
                'tentang','wajib','lama','penyebab','cabang','metode','terkait','kosong','kecil',
                'menengah','rata-rata','pertahun','harian','positif','tiap','komponen','masa',
                'menerima','kondisi','khusus','tidak','ada','tempat','status','sebaran','lokasi',
                'para','pencari','gagal','bergerak','dibidang','penerimaan','2020/2021','1997-2021',
                'cakupan','masalah','dari','secara','rinci','kerangan','bidang','kejadian','target',
                'realisasi','2016-2020','meminta','terdaftar','taksiran','kontak','terbuka','khususnya',
                '2019-2020','menggunakan','mulai','sekarang','2006-2012','terhadap','bentuk','dimasa',
                'segi','kriteria','sub','masing','permohonan','klasifikasi','_','faktor-faktor',
                'berhubungan','angkatan','penurunan','dll','meliputi','kedalalman','kepercayaan',
                'melalui','langsung','tersedia','memenuhi','kategori','dilayani','upaya','kurun',
                'waktu','terakhir','(2015-2021)','banyaknya','persentase','beberapa','periode','sebelum',
                'setidaknya','melakukan','se','terdiri','daftar','telah','beralih','awal','level',
                'kasus','presentase','detil','laporan','ingin','mengetahui','data2','ter','update',
                'Katalog','keperluan','tugas','akhir','panjang','bersumber','by','names','address',
                'tren','tersebar','selama','500','fungsi','tersebut','laju','berikut','permintaan',
                'informasi',')','(','penyandang','estimasi','tes','asal','Kab.','masyarakat','wilayah',
                'hingga','as,dnsnd','A','detail','lainnya','garis',':)','penerima','komposisi','mis',

            ]
            result_data_clean = []
            for rd in result_data:
                if rd['keyword'] not in dikecualikan:
                    result_data_clean.append(rd)

            # get top 10
            result_data_final = []
            i = 0
            for rdf in result_data_clean:
                i += 1
                if i <= 20:
                    result_data_final.append(rdf)

            # check if empty
            if result_data_final:
                return True, result_data_final
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
