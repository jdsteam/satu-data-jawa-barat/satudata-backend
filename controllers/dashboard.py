''' Doc: controller dashboard  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import DatasetModel, ReviewModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, func
import sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, singleton-comparison
class DashboardController(object):
    ''' Doc: constructor dashboard  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, start_date, end_date):
        ''' Doc: function get all data  '''
        try:
            if start_date:
                filter_start_date = "dataset.cdate::date >= '" + start_date + "'"
            if end_date:
                filter_end_date = "dataset.cdate::date <= '" + end_date + "'"

            # query postgresql count public
            result_public = db.session.query(DatasetModel.dataset_class_id)
            result_public = result_public.filter(DatasetModel.is_deleted == False)
            result_public = result_public.filter(DatasetModel.is_active == True)
            result_public = result_public.filter(DatasetModel.is_validate == 3)
            result_public = result_public.filter(DatasetModel.dataset_class_id == 3)
            if start_date:
                result_public = result_public.filter(text(filter_start_date))
            if start_date and end_date:
                result_public = result_public.filter(text(filter_start_date))
                result_public = result_public.filter(text(filter_end_date))
            result_public = result_public.count()

            # count private
            result_private = db.session.query(DatasetModel.dataset_class_id)
            result_private = result_private.filter(DatasetModel.is_deleted == False)
            result_private = result_private.filter(DatasetModel.is_active == True)
            result_private = result_private.filter(DatasetModel.is_validate == 3)
            result_private = result_private.filter(DatasetModel.dataset_class_id == 4)
            if start_date:
                result_private = result_private.filter(text(filter_start_date))
            if start_date and end_date:
                result_private = result_private.filter(text(filter_start_date))
                result_private = result_private.filter(text(filter_end_date))
            result_private = result_private.count()

            # count internal
            result_internal = db.session.query(DatasetModel.dataset_class_id)
            result_internal = result_internal.filter(DatasetModel.is_deleted == False)
            result_internal = result_internal.filter(DatasetModel.is_active == True)
            result_internal = result_internal.filter(DatasetModel.is_validate == 3)
            result_internal = result_internal.filter(DatasetModel.dataset_class_id == 5)
            if start_date:
                result_internal = result_internal.filter(text(filter_start_date))
            if start_date and end_date:
                result_internal = result_internal.filter(text(filter_start_date))
                result_internal = result_internal.filter(text(filter_end_date))
            result_internal = result_internal.count()

            # change into dict
            app_service = {}
            app_service['count_public'] = result_public
            app_service['count_private'] = result_private
            app_service['count_internal'] = result_internal

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_top_opd(self, start_date, end_date):
        ''' Doc: function get top opd  '''
        try:
            # query postgresql Produsen
            if start_date:
                cdate_start_date = " and cdate::date >= '" + start_date + "'"
            if end_date:
                cdate_end_date = " and cdate::date <= '" + end_date + "'"

            if start_date:
                date_produsen = cdate_start_date
            elif start_date and end_date:
                date_produsen = cdate_start_date
                date_produsen += cdate_end_date
            else:
                date_produsen = ''
            sql = text(
                'SELECT id, regional_id, kode_skpd, nama_skpd, nama_skpd_alias, '+
                '(SELECT count(id) from dataset where dataset.is_active = true and dataset.kode_skpd = skpd_head.kode_skpd and is_deleted = false' + date_produsen + ') as count_produsen ' +
                'from skpd as skpd_head ' +
                'where is_external = false '+
                'order by count_produsen desc ' +
                'LIMIT 10'
                )
            result_produsen = db.engine.execute(sql)

            # Konsumen
            if start_date:
                history_start_date = " and history.datetime::date >= '" + start_date + "'"
            if end_date:
                history_end_date = " and history.datetime::date <= '" + end_date + "'"

            if start_date:
                date_konsumen = history_start_date
            elif start_date and end_date:
                date_konsumen = history_start_date
                date_konsumen += history_end_date
            else:
                date_konsumen = ''
            sql = text(
                'SELECT id, regional_id, kode_skpd, nama_skpd, nama_skpd_alias, ' +
                '(SELECT count(history.id) from history ' +
                'INNER JOIN users ON history.user_id = users.id ' +
                'INNER JOIN skpd ON users.kode_skpd = skpd.kode_skpd ' +
                'where skpd.kode_skpd = skpd_head.kode_skpd and history.category = \'access\' ' + date_konsumen + ') as count_konsumen ' +
                'from skpd as skpd_head ' +
                'where is_external = false ' +
                'order by count_konsumen desc ' +
                'LIMIT 10'
                )
            result_konsumen = db.engine.execute(sql)

            # change into dict
            app_service = {
                'opd_produsen': [dict(row) for row in result_produsen],
                'opd_konsumen': [dict(row) for row in result_konsumen]
            }

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_top_dataset(self, sort, start_date, end_date):
        ''' Doc: function get top dataset  '''
        try:
            if start_date:
                filter_start_date = "dataset.cdate::date >= '" + start_date + "'"
            if end_date:
                filter_end_date = "dataset.cdate::date <= '" + end_date + "'"

            # query postgresql count public
            result = db.session.query(
                DatasetModel.id, DatasetModel.name, DatasetModel.title, DatasetModel.count_view_private,
                DatasetModel.count_access_private, DatasetModel.count_rating
                )
            if start_date:
                result = result.filter(text(filter_start_date))
            if start_date and end_date:
                result = result.filter(text(filter_start_date))
                result = result.filter(text(filter_end_date))
            result = result.filter(DatasetModel.is_active == True)
            result = result.order_by(text(sort[0] + " " + sort[1]))
            result = result.limit(10)
            result = result.all()

            # change into dict
            output = []

            for row in result:
                row_one = row._asdict()
                count_review = db.session.query(func.count(ReviewModel.id.distinct()).label('count_review'))
                count_review = count_review.filter(ReviewModel.type == 'dataset')
                count_review = count_review.filter(ReviewModel.type_id == row_one['id'])
                if sort[0] == 'count_rating':
                    count_review = count_review.order_by(text('count_review ' + sort[1]))
                count_review = count_review.all()

                for res in count_review:
                    row_one.update(res._asdict())
                output.append(row_one)

            # check if empty
            if output:
                return True, output
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
