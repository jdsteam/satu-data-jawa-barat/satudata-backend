''' Doc: controller app  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import AppModel
from models import SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable
class AppController(object):
    ''' Doc: controller app  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(AppModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),
                defer("is_deleted"))
            for attr, value in where.items():
                result = result.filter(getattr(AppModel, attr) == value)
            result = result.filter(or_(cast(getattr(AppModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AppModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            app = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)

                if temp['kode_skpd']:
                    # get relation to skpd
                    skpd = db.session.query(SkpdModel)
                    skpd = skpd.filter(getattr(SkpdModel, 'kode_skpd') == temp['kode_skpd'])
                    skpd = skpd.one()

                    if skpd:
                        skpd = skpd.__dict__
                        skpd.pop('_sa_instance_state', None)
                    else:
                        skpd = {}

                    temp['skpd'] = skpd
                else:
                    temp['skpd'] = {}

                app.append(temp)

            # check if empty
            app = list(app)
            if app:
                return True, app
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(AppModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(AppModel, attr) == value)
            result = result.filter(or_(cast(getattr(AppModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in AppModel.__table__.columns))
            result = result.count()

            # change into dict
            app = {}
            app['count'] = result

            # check if empty
            if app:
                return True, app
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(AppModel)
            result = result.options(
                defer("cuid"),
                defer("cdate"),
                defer("muid"),
                defer("mdate"),
                defer("is_deleted"))
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                if result['kode_skpd']:
                    # get relation to skpd
                    skpd = db.session.query(SkpdModel)
                    skpd = skpd.filter(getattr(SkpdModel, 'kode_skpd') == result['kode_skpd'])
                    skpd = skpd.one()

                    if skpd:
                        skpd = skpd.__dict__
                        skpd.pop('_sa_instance_state', None)
                    else:
                        skpd = {}

                    result['skpd'] = skpd
                else:
                    result['skpd'] = {}

            else:
                result = {}

            # change into dict
            app = result

            # check if empty
            if app:
                return True, app
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["cdate"] = helper.local_date_server()

            # prepare data model
            result = AppModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, app = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, app
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = helper.local_date_server()

            try:
                # prepare data model
                result = db.session.query(AppModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AppModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, app = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, app
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found, " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(AppModel)
                for attr, value in where.items():
                    result = result.filter(getattr(AppModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, app = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
