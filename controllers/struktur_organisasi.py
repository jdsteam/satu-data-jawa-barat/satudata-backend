''' Doc: controller struktur_organisasi  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import StrukturOrganisasiModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=line-too-long, unused-variable, singleton-comparison
class StrukturOrganisasiController(object):
    ''' Doc: controller struktur_organisasi  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(StrukturOrganisasiModel)
            for attr, value in where.items():
                result = result.filter(getattr(StrukturOrganisasiModel, attr) == value)
            result = result.filter(or_(cast(getattr(StrukturOrganisasiModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in StrukturOrganisasiModel.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            struktur_organisasi = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)
                struktur_organisasi.append(res)

            # check if empty
            struktur_organisasi = list(struktur_organisasi)
            if struktur_organisasi:
                return True, struktur_organisasi
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(StrukturOrganisasiModel.id)
            for attr, value in where.items():
                result = result.filter(getattr(StrukturOrganisasiModel, attr) == value)
            result = result.filter(or_(cast(getattr(StrukturOrganisasiModel, col.name), sqlalchemy.String).ilike('%'+search+'%') for col in StrukturOrganisasiModel.__table__.columns))
            result = result.count()

            # change into dict
            struktur_organisasi = {}
            struktur_organisasi['count'] = result

            # check if empty
            if struktur_organisasi:
                return True, struktur_organisasi
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(StrukturOrganisasiModel).get(_id)
            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            struktur_organisasi = result

            # check if empty
            if struktur_organisasi:
                return True, struktur_organisasi
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
