''' Doc: controller article topic  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import ArticleTopicModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, re, sentry_sdk

CONFIGURATION = Configuration()
HELPER = Helper()

class ArticleTopicController(object):
    ''' Doc: class article topic  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
              ArticleTopicModel.id, ArticleTopicModel.name, ArticleTopicModel.title,
              ArticleTopicModel.image, ArticleTopicModel.notes, ArticleTopicModel.is_active,
              ArticleTopicModel.is_deleted, ArticleTopicModel.cuid, ArticleTopicModel.cdate,
              ArticleTopicModel.muid, ArticleTopicModel.mdate
            )
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(ArticleTopicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(ArticleTopicModel, attr) == value)

            result = result.filter(or_(
                cast(getattr(ArticleTopicModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(ArticleTopicModel, "title"), sqlalchemy.String).ilike('%'+search+'%'),
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("article_topic."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            count = result.count()
            result = result.all()

            if result:
                result_data = []
                for res in result:
                    temp = res._asdict()
                    result_data.append(temp)
                return True, result_data, count
            else:
                return False, [], 0

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = self.query({'id':_id}, '')
            result = result.first()
            # convert to dict result._asdict()

            # check if empty
            if result:
                return True, result._asdict(), "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            # title
            json_send["title"] = json_send["name"].lower().replace(' ', '-')
            json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])
            res_count = db.session.query(ArticleTopicModel.id)
            res_count = res_count.filter(ArticleTopicModel.title == json_send["title"])
            res_count = res_count.count()
            if res_count > 0:
                json_send["title"] = json_send["title"] + '-' + str(res_count + 1)

            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = ArticleTopicModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()

            if result:
                return True, json_send
            else:
                return False, {}

        except Exception as err:
            # fail response
            if (err.__class__.__name__ == 'IntegrityError'):
                err = "duplicate name %s" % json_send["name"]

            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = _json

            if 'name' in json_send:
                json_send["title"] = json_send["name"].lower().replace(' ', '-')
                json_send["title"] = re.sub('[^a-zA-Z0-9-]', '', json_send["title"])

            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            # prepare data model
            result = db.session.query(ArticleTopicModel)
            for attr, value in where.items():
                result = result.filter(getattr(ArticleTopicModel, attr) == value)

            # execute database
            result = result.update(json_send, synchronize_session='fetch')
            check = result
            result = db.session.commit()
            # check if empty
            if check:
                return True, json_send
            else:
                return False, "Id %s not found" % where['id']

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:
            json_send = {}
            json_send['is_deleted'] = True

            jwt = HELPER.read_jwt()
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            status, result = self.update(where, json_send)

            return status, result
        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
