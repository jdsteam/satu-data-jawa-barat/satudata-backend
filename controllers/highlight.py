''' Doc: controller highlight  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HighlightModel
from models import HighlightViewModel
from models import DatasetModel
from models import VisualizationModel
from models import InfographicModel
from models import ArticleModel
from models import SektoralModel
from models import MapsetModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast, extract
import sqlalchemy, sentry_sdk
from datetime import datetime, timedelta


CONFIGURATION = Configuration()
HELPER = Helper()
FORMAT_DATE = "%Y-%m-%d %H:%M:%S"
FORMAT_TIME = "0000-00-00 00:00:00"
DEFAULT_IMAGE = "static/upload/default-image.png"
DEFAULT_COUNT_VIEW = "count_view desc"
one_month_ago = datetime.today() - timedelta(days = 30)
# pylint: disable=singleton-comparison, unused-variable, unused-argument
class HighlightController(object):
    ''' Doc: class highlight  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(HighlightViewModel)

            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HighlightViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HighlightViewModel, attr) == value)
            # search
            result = result.filter(or_(
                cast(getattr(HighlightViewModel, "name"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HighlightViewModel, "category"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HighlightViewModel, "notes"), sqlalchemy.String).ilike('%'+search+'%')
            ))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            highlight = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                temp['cdate'] = temp['cdate'].strftime(FORMAT_DATE)
                temp['mdate'] = temp['mdate'].strftime(FORMAT_DATE)

                # populate category
                if temp['category'] == 'dataset':
                    category_data = self.populate_category_dataset(temp)

                elif temp['category'] == 'infografik':
                    category_data = self.populate_category_infografik(temp)

                elif temp['category'] == 'visualisasi':
                    category_data = self.populate_category_visualisasi(temp)

                elif temp['category'] == 'artikel':
                    category_data = self.populate_category_article(temp)

                elif temp['category'] == 'mapset':
                    category_data = self.populate_category_mapset(temp)

                else:
                    category_data = {}

                temp['category_data'] = category_data
                if temp['category_data']:
                    highlight.append(temp)

            # check if empty
            highlight = list(highlight)
            if highlight:
                return True, highlight
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_category_dataset(self, temp):
        ''' Doc: function populate_category_dataset  '''
        category_data = {}
        temp_data = db.session.query(
            DatasetModel.id, DatasetModel.name, DatasetModel.title,
            DatasetModel.sektoral_id, DatasetModel.cdate, DatasetModel.mdate
        )
        temp_data = temp_data.filter(getattr(DatasetModel, "id") == temp['category_id'])
        temp_data = temp_data.filter(getattr(DatasetModel, "is_active") == True)
        temp_data = temp_data.filter(getattr(DatasetModel, "is_deleted") == False)
        temp_data = temp_data.all()
        if temp_data:
            for td_ in temp_data:
                category_data['id'] = td_[0]
                category_data['name'] = td_[1]
                category_data['title'] = td_[2]
                category_data['cdate'] = td_[4].strftime(FORMAT_DATE)
                category_data['mdate'] = td_[5].strftime(FORMAT_DATE)

                sektoral = db.session.query(SektoralModel)
                sektoral = sektoral.get(int(td_[3]))
                if sektoral:
                    sektoral = sektoral.__dict__
                    sektoral.pop('_sa_instance_state', None)
                else:
                    sektoral = {}
                rand_code = (category_data['id'] % 5) + 1
                rand_thumb = 'picture_thumbnail_' + str(rand_code)
                category_data['image'] = sektoral[rand_thumb]
        else:
            category_data = {}
            category_data['id'] = 0
            category_data['name'] = 'Dataset Telah Dihapus'
            category_data['title'] = 'dataset-telah-dihapus'
            category_data['cdate'] = FORMAT_TIME
            category_data['mdate'] = FORMAT_TIME
            category_data['image'] = DEFAULT_IMAGE

        return category_data

    def populate_category_infografik(self, temp):
        ''' Doc: function populate_category_infografik  '''
        category_data = {}
        temp_data = db.session.query(
            InfographicModel.id, InfographicModel.name, InfographicModel.title,
            InfographicModel.image, InfographicModel.cdate, InfographicModel.mdate
        )
        temp_data = temp_data.filter(getattr(InfographicModel, "id") == temp['category_id'])
        temp_data = temp_data.filter(getattr(InfographicModel, "is_active") == True)
        temp_data = temp_data.filter(getattr(InfographicModel, "is_deleted") == False)
        temp_data = temp_data.all()
        if temp_data:
            for td_ in temp_data:
                category_data['id'] = td_[0]
                category_data['name'] = td_[1]
                category_data['title'] = td_[2]
                category_data['image'] = td_[3]
                category_data['cdate'] = td_[4].strftime(FORMAT_DATE)
                category_data['mdate'] = td_[5].strftime(FORMAT_DATE)
        else:
            category_data = {}
            category_data['id'] = 0
            category_data['name'] = 'Infografik Telah Dihapus'
            category_data['title'] = 'infografik-telah-dihapus'
            category_data['cdate'] = FORMAT_TIME
            category_data['mdate'] = FORMAT_TIME
            category_data['image'] = DEFAULT_IMAGE

        return category_data

    def populate_category_visualisasi(self, temp):
        ''' Doc: function populate_category_visualisasi  '''
        category_data = {}
        temp_data = db.session.query(
            VisualizationModel.id, VisualizationModel.name, VisualizationModel.title,
            VisualizationModel.image, VisualizationModel.cdate, VisualizationModel.mdate
        )
        temp_data = temp_data.filter(getattr(VisualizationModel, "id") == temp['category_id'])
        temp_data = temp_data.filter(getattr(VisualizationModel, "is_active") == True)
        temp_data = temp_data.filter(getattr(VisualizationModel, "is_deleted") == False)
        temp_data = temp_data.all()
        if temp_data:
            for td_ in temp_data:
                category_data['id'] = td_[0]
                category_data['name'] = td_[1]
                category_data['title'] = td_[2]
                category_data['image'] = td_[3]
                category_data['cdate'] = td_[4].strftime(FORMAT_DATE)
                category_data['mdate'] = td_[5].strftime(FORMAT_DATE)
        else:
            category_data = {}
            category_data['id'] = 0
            category_data['name'] = 'Visualisasi Telah Dihapus'
            category_data['title'] = 'visualisasi-telah-dihapus'
            category_data['cdate'] = FORMAT_TIME
            category_data['mdate'] = FORMAT_TIME
            category_data['image'] = DEFAULT_IMAGE

        return category_data

    def populate_category_article(self, temp):
        ''' Doc: function populate_category_article  '''
        category_data = {}
        temp_data = db.session.query(
            ArticleModel.id, ArticleModel.name, ArticleModel.title,
            ArticleModel.image, ArticleModel.cdate, ArticleModel.mdate
        )
        temp_data = temp_data.filter(getattr(ArticleModel, "id") == temp['category_id'])
        temp_data = temp_data.filter(getattr(ArticleModel, "is_active") == True)
        temp_data = temp_data.filter(getattr(ArticleModel, "is_deleted") == False)
        temp_data = temp_data.all()
        if temp_data:
            for td_ in temp_data:
                category_data['id'] = td_[0]
                category_data['name'] = td_[1]
                category_data['title'] = td_[2]
                category_data['image'] = td_[3]
                category_data['cdate'] = td_[4].strftime(FORMAT_DATE)
                category_data['mdate'] = td_[5].strftime(FORMAT_DATE)
        else:
            category_data = {}
            category_data['id'] = 0
            category_data['name'] = 'Artikel Telah Dihapus'
            category_data['title'] = 'artikel-telah-dihapus'
            category_data['cdate'] = FORMAT_TIME
            category_data['mdate'] = FORMAT_TIME
            category_data['image'] = DEFAULT_IMAGE

        return category_data

    def populate_category_mapset(self, temp):
        ''' Doc: function populate_category_mapset  '''
        category_data = {}
        temp_data = db.session.query(
            MapsetModel.id, MapsetModel.name, MapsetModel.title,
            MapsetModel.app_link, MapsetModel.cdate, MapsetModel.mdate
        )
        temp_data = temp_data.filter(getattr(MapsetModel, "id") == temp['category_id'])
        temp_data = temp_data.filter(getattr(MapsetModel, "is_active") == True)
        temp_data = temp_data.filter(getattr(MapsetModel, "is_deleted") == False)
        temp_data = temp_data.all()
        if temp_data:
            for td_ in temp_data:
                category_data['id'] = td_[0]
                category_data['name'] = td_[1]
                category_data['title'] = td_[2]
                category_data['image'] = td_[3]
                category_data['cdate'] = td_[4].strftime(FORMAT_DATE)
                category_data['mdate'] = td_[5].strftime(FORMAT_DATE)
        else:
            category_data = {}
            category_data['id'] = 0
            category_data['name'] = 'Mapset Telah Dihapus'
            category_data['title'] = 'mapset-telah-dihapus'
            category_data['cdate'] = FORMAT_TIME
            category_data['mdate'] = FORMAT_TIME
            category_data['image'] = DEFAULT_IMAGE

        return category_data

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = db.session.query(HighlightViewModel)

            # where
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HighlightViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HighlightViewModel, attr) == value)
            result = result.count()

            # change into dict
            highlight = {}
            highlight['count'] = result

            # check if empty
            if highlight:
                return True, highlight
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(HighlightModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                result['cdate'] = result['cdate'].strftime(FORMAT_DATE)
                result['mdate'] = result['mdate'].strftime(FORMAT_DATE)

                # populate category
                if result['category'] == 'dataset':
                    category_data = self.populate_category_dataset(result)

                elif result['category'] == 'infografik':
                    category_data = self.populate_category_infografik(result)

                elif result['category'] == 'visualisasi':
                    category_data = self.populate_category_visualisasi(result)

                elif result['category'] == 'artikel':
                    category_data = self.populate_category_article(result)

                elif result['category'] == 'mapset':
                    category_data = self.populate_category_mapset(result)

                else:
                    category_data = {}

                result['category_data'] = category_data

            else:
                result = {}

            # change into dict
            highlight = result

            # check if empty
            if highlight:
                return True, highlight, "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            # prepare data model
            result = HighlightModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, highlight, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, highlight
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json

            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(HighlightModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HighlightModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, highlight, message = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, highlight
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found " + str(err), 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HighlightModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HighlightModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, highlight, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_dataset_highlight(self):
        ''' Doc: function get dataset highlight  '''
        try:
            # query postgresql
            result = db.session.query(
                DatasetModel.id, DatasetModel.name, DatasetModel.title,
                DatasetModel.sektoral_id, DatasetModel.cdate, DatasetModel.mdate
            )
            result = result.filter(getattr(DatasetModel, "is_active") == True)
            result = result.filter(getattr(DatasetModel, "is_deleted") == False)
            result = result.filter(getattr(DatasetModel, "dataset_class_id") == 3)
            result = result.filter(DatasetModel.cdate >= one_month_ago)
            result = result.filter(DatasetModel.cdate <= datetime.today())
            result = result.order_by(text(DEFAULT_COUNT_VIEW))
            result = result.offset(0).limit(2)
            result = result.all()

            # change into dict
            highlight = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['name'] = res[1]
                temp['title'] = res[2]
                temp['cdate'] = res[4].strftime(FORMAT_DATE)
                temp['mdate'] = res[5].strftime(FORMAT_DATE)

                sektoral = db.session.query(SektoralModel)
                sektoral = sektoral.get(int(res[3]))
                if sektoral:
                    sektoral = sektoral.__dict__
                    sektoral.pop('_sa_instance_state', None)
                else:
                    sektoral = {}
                rand_code = (temp['id'] % 5) + 1
                rand_thumb = 'picture_thumbnail_' + str(rand_code)
                temp['image'] = sektoral[rand_thumb]

                highlight.append(temp)

            # check if empty
            highlight = list(highlight)
            if highlight:
                return True, highlight
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_infografik_highlight(self):
        ''' Doc: function get infografik highlight  '''
        try:
            # query postgresql
            result = db.session.query(
                InfographicModel.id, InfographicModel.name, InfographicModel.title,
                InfographicModel.image, InfographicModel.cdate, InfographicModel.mdate
            )
            result = result.filter(getattr(InfographicModel, "is_active") == True)
            result = result.filter(getattr(InfographicModel, "is_deleted") == False)
            result = result.filter(getattr(InfographicModel, "dataset_class_id") == 3)
            result = result.filter(InfographicModel.cdate >= one_month_ago)
            result = result.filter(InfographicModel.cdate <= datetime.today())
            result = result.order_by(text(DEFAULT_COUNT_VIEW))
            result = result.offset(0).limit(2)
            result = result.all()

            # change into dict
            highlight = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['name'] = res[1]
                temp['title'] = res[2]
                temp['image'] = res[3]
                temp['cdate'] = res[4].strftime(FORMAT_DATE)
                temp['mdate'] = res[5].strftime(FORMAT_DATE)

                highlight.append(temp)

            # check if empty
            highlight = list(highlight)
            if highlight:
                return True, highlight
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_visualisasi_highlight(self):
        ''' Doc: function get visualisasi highlight  '''
        try:
            # query postgresql
            result = db.session.query(
                VisualizationModel.id, VisualizationModel.name, VisualizationModel.title,
                VisualizationModel.image, VisualizationModel.cdate, VisualizationModel.mdate
            )
            result = result.filter(getattr(VisualizationModel, "is_active") == True)
            result = result.filter(getattr(VisualizationModel, "is_deleted") == False)
            result = result.filter(getattr(VisualizationModel, "dataset_class_id") == 3)
            result = result.filter(VisualizationModel.cdate >= one_month_ago)
            result = result.filter(VisualizationModel.cdate <= datetime.today())
            result = result.order_by(text(DEFAULT_COUNT_VIEW))
            result = result.offset(0).limit(2)
            result = result.all()

            # change into dict
            highlight = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['name'] = res[1]
                temp['title'] = res[2]
                temp['image'] = res[3]
                temp['cdate'] = res[4].strftime(FORMAT_DATE)
                temp['mdate'] = res[5].strftime(FORMAT_DATE)

                highlight.append(temp)

            # check if empty
            highlight = list(highlight)
            if highlight:
                return True, highlight
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_article_highlight(self):
        ''' Doc: function get visualisasi article  '''
        try:
            # query postgresql
            result = db.session.query(
                ArticleModel.id, ArticleModel.name, ArticleModel.title,
                ArticleModel.image, ArticleModel.cdate, ArticleModel.mdate
            )
            result = result.filter(getattr(ArticleModel, "is_active") == True)
            result = result.filter(getattr(ArticleModel, "is_deleted") == False)
            result = result.filter(ArticleModel.cdate >= one_month_ago)
            result = result.filter(ArticleModel.cdate <= datetime.today())
            result = result.order_by(text(DEFAULT_COUNT_VIEW))
            result = result.offset(0).limit(2)
            result = result.all()

            # change into dict
            highlight = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['name'] = res[1]
                temp['title'] = res[2]
                temp['image'] = res[3]
                temp['cdate'] = res[4].strftime(FORMAT_DATE)
                temp['mdate'] = res[5].strftime(FORMAT_DATE)

                highlight.append(temp)

            # check if empty
            highlight = list(highlight)
            if highlight:
                return True, highlight
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_mapset_highlight(self):
        ''' Doc: function get visualisasi article  '''
        try:
            # query postgresql
            result = db.session.query(
                MapsetModel.id, MapsetModel.name, MapsetModel.title,
                MapsetModel.app_link, MapsetModel.cdate, MapsetModel.mdate
            )
            result = result.filter(getattr(MapsetModel, "is_active") == True)
            result = result.filter(getattr(MapsetModel, "is_deleted") == False)
            result = result.filter(MapsetModel.cdate >= one_month_ago)
            result = result.filter(MapsetModel.cdate <= datetime.today())
            result = result.order_by(text(DEFAULT_COUNT_VIEW))
            result = result.offset(0).limit(2)
            result = result.all()

            # change into dict
            highlight = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['name'] = res[1]
                temp['title'] = res[2]
                temp['image'] = res[3]
                temp['cdate'] = res[4].strftime(FORMAT_DATE)
                temp['mdate'] = res[5].strftime(FORMAT_DATE)

                highlight.append(temp)

            # check if empty
            highlight = list(highlight)
            if highlight:
                return True, highlight
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
