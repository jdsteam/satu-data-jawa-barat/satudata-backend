''' Doc: controller mapset thematic  '''
from models.mapset_story import MapsetStoryModel
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import MapsetThematicModel
from models import SektoralModel
from models import BidangUrusanModel
from models import MapsetThematicSourceModel
from models import MapsetThematicMetadataModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
from sqlalchemy import select
from sqlalchemy import desc
from sqlalchemy import null
import sqlalchemy, sentry_sdk

from controllers.mapset_thematic_source import MapsetThematicSourceController
from controllers.mapset_thematic_metadata import MapsetThematicMetadataController

MAPSET_THEMATIC_SOURCE_CONTROLLER = MapsetThematicSourceController()
MAPSET_THEMATIC_METADATA_CONTROLLER = MapsetThematicMetadataController()

CONFIGURATION = Configuration()
HELPER = Helper()

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class MapsetThematicStorySearchController(object):
    ''' Doc: class mapset thematic  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''
      
    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            thematic = db.session.query(
              MapsetThematicModel.id,
              MapsetThematicModel.sektoral_id,
              MapsetThematicModel.title.label('title'), 
              MapsetThematicModel.url,
              MapsetThematicModel.image, 
              MapsetThematicModel.description, 
              MapsetThematicModel.year, 
              MapsetThematicModel.map_type.label('source'), 
              MapsetThematicModel.count_view.label('count_view'), 
              MapsetThematicModel.is_active, 
              MapsetThematicModel.is_deleted,
              MapsetThematicModel.cdate, 
              MapsetThematicModel.mdate.label('mdate'),
              SektoralModel.name.label('sektoral_name'), 
              SektoralModel.notes.label('sektoral_notes'),
              SektoralModel.picture.label('sektoral_picture'), 
              SektoralModel.picture_thumbnail.label('sektoral_picture_thumbnail'),
            )
            thematic = thematic.join(
                SektoralModel, MapsetThematicModel.sektoral_id == SektoralModel.id, isouter=True)
            
            story = db.session.query(
              MapsetStoryModel.id,
              MapsetStoryModel.sektoral_id,
              MapsetStoryModel.name.label('title'), 
              MapsetStoryModel.url, 
              MapsetStoryModel.image, 
              MapsetStoryModel.description, 
              null().label('year'),
              db.literal('storymaps').label('source'),
              MapsetStoryModel.count_view.label('count_view'), 
              MapsetStoryModel.is_active, 
              MapsetStoryModel.is_deleted,
              MapsetStoryModel.cdate, 
              MapsetStoryModel.mdate.label('mdate'),
              SektoralModel.name.label('sektoral_name'), 
              SektoralModel.notes.label('sektoral_notes'),
              SektoralModel.picture.label('sektoral_picture'), 
              SektoralModel.picture_thumbnail.label('sektoral_picture_thumbnail'),
            )
            story = story.join(
                SektoralModel, MapsetStoryModel.sektoral_id == SektoralModel.id, isouter=True)
            result = thematic.union_all(story)
            
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(MapsetThematicModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(MapsetThematicModel, attr) == value)
                    
            if search:
                search = search.split(' ')
                for src in search:
                    result = result.filter(or_(
                        cast(getattr(MapsetThematicModel, "title"), sqlalchemy.String).match(src),
                        cast(getattr(MapsetThematicModel, "description"), sqlalchemy.String).match(src),
                        cast(getattr(MapsetThematicModel, "map_type"), sqlalchemy.String).match(src),
                        cast(getattr(MapsetStoryModel, "name"), sqlalchemy.String).match(src),
                        cast(getattr(MapsetStoryModel, "description"), sqlalchemy.String).match(src),
                    ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def populate_data(self, temp):
      ''' Doc: function populate data '''
      try:
        temp['cdate'] = temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")
        temp['mdate'] = temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")
      
        # get relation to sektoral
        temp['sektoral'] = {}
        temp['sektoral']['id'] = temp['sektoral_id']
        temp['sektoral']['name'] = temp['sektoral_name']
        temp['sektoral']['picture'] = temp['sektoral_picture']
        temp['sektoral']['picture_thumbnail'] = temp['sektoral_picture_thumbnail']
        temp.pop('sektoral_name', None)
        temp.pop('sektoral_notes', None)
        temp.pop('sektoral_is_opendata', None)
        temp.pop('sektoral_is_satudata', None)
        temp.pop('sektoral_picture', None)
        temp.pop('sektoral_picture_thumbnail', None)
        
        return temp
      except Exception as err:
        # fail response
        sentry_sdk.capture_exception(err)
        raise ErrorMessage(str(err), 500, 1, {})
    
    
    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            mapset_thematic = [res._asdict() for res in result]
            
            # check if empty
            if mapset_thematic:
                return True, mapset_thematic
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            
            result = self.query(where, search)
            result = result.count()

            # change into dict
            app_service = {}
            app_service['count'] = result

            # check if empty
            if app_service:
                return True, app_service
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
