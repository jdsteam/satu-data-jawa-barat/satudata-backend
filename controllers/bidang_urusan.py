''' Doc: controller bidang_urusan  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import BidangUrusanModel, BidangUrusanViewModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
import helpers.postgre_psycopg as db2
CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class BidangUrusanController(object):
    ''' Doc: class bidang_urusan  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''


    def get_all_walidata(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(BidangUrusanViewModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(BidangUrusanViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(BidangUrusanViewModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanViewModel, col.name), sqlalchemy.String).ilike('%' + search + '%') for col in BidangUrusanViewModel.__table__.columns))
            result = result.order_by(text(sort[0] + " " + sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            datas = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                if temp['cdate']:
                    temp.update({"cdate":temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                if temp['mdate']:
                    temp.update({"mdate":temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")})
                datas.append(temp)

            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count_walidata(self, where: dict, search):
        ''' Doc: function count  '''
        try:
            # query postgresql
            result = db.session.query(BidangUrusanViewModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(BidangUrusanViewModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(BidangUrusanViewModel, attr) == value)
            result = result.filter(or_(cast(getattr(BidangUrusanViewModel, col.name), sqlalchemy.String).ilike('%' + search + '%') for col in BidangUrusanViewModel.__table__.columns))
            result = result.count()

            # change into dict
            datas = {}
            datas['count'] = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_all_opd(self, where: dict, search, sort, limit, skip, kode_skpd):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            sql_count_indikator = f'''
                (
                    SELECT count(bidang_urusan_indikator.id)
                    FROM bidang_urusan_indikator
                    join bidang_urusan_indikator_skpd on bidang_urusan_indikator_skpd.kode_indikator = bidang_urusan_indikator.kode_indikator
                    WHERE bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                    and bidang_urusan_indikator_skpd.kode_skpd = '{kode_skpd}' '''
            for attr, value in where.items():
                if attr == 'status_assignment':
                    sql_count_indikator += f'''AND {attr} = '{value}' '''
            sql_count_indikator += f''' ) AS count_indikator,
            '''

            sql = f'''
                select
                distinct bidang_urusan.*, '''
            sql += sql_count_indikator
            sql += f'''
                bidang_urusan_indikator_skpd.kode_skpd,
                skpd.nama_skpd
                from bidang_urusan_indikator
                join bidang_urusan on bidang_urusan.kode_bidang_urusan = bidang_urusan_indikator.kode_bidang_urusan
                join bidang_urusan_indikator_skpd on bidang_urusan_indikator_skpd.kode_indikator = bidang_urusan_indikator.kode_indikator
                join skpd on skpd.kode_skpd = bidang_urusan_indikator_skpd.kode_skpd
                where bidang_urusan_indikator_skpd.kode_skpd = '{kode_skpd}'
            '''

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += f'''AND (LOWER(bidang_urusan.nama_bidang_urusan) LIKE %{search} '''

            if sort:
                if sort[0] == 'id':
                    sort[0] = 'bidang_urusan.nama_bidang_urusan'
                sql += f'''ORDER BY {str(sort[0])} {str(sort[1])} '''

            sql += f'''OFFSET {str(skip)} LIMIT {str(limit)} '''
            # print(sql)

            result = db.session.execute(sql)
            # print(result)

            # change into dict
            results = []
            for re in result:
                tmp = dict(re)
                results.append(tmp)
            # print(results)

            # change date format
            datas = []
            for res in results:
                # print(res)
                temp = res
                if temp['cdate']:
                    temp['cdate'] = temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")
                if temp['mdate']:
                    temp['mdate']  = temp['mdate'].strftime("%Y-%m-%d %H:%M:%S")
                datas.append(temp)

            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count_opd(self, where: dict, search, kode_skpd):
        ''' Doc: function count  '''
        try:
            # query postgresql
            sql = f'''
                select count(distinct bidang_urusan.*)
                from bidang_urusan_indikator
                join bidang_urusan on bidang_urusan.kode_bidang_urusan = bidang_urusan_indikator.kode_bidang_urusan
                join bidang_urusan_indikator_skpd on bidang_urusan_indikator_skpd.kode_indikator = bidang_urusan_indikator.kode_indikator
                join skpd on skpd.kode_skpd = bidang_urusan_indikator_skpd.kode_skpd
                where bidang_urusan_indikator_skpd.kode_skpd = '{kode_skpd}' '''

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += f'''AND (LOWER(bidang_urusan.nama_bidang_urusan) LIKE %{search} '''

            result = db.session.execute(sql)
            # print(result)

            # change into dict
            for re in result:
                tmp = dict(re)
                results = tmp

            # change into dict
            datas = results

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_progress(self, where: dict, search, sort, limit, skip, tahun, skpd):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            where_tahun = ""
            parameters = {}
            if tahun:
                where_tahun = " AND ewalidata_data.tahun = %(tahun)s "
                parameters['tahun'] = tahun

            sql = f'''
                    SELECT
                        bidang_urusan.*,
                        skpd.nama_skpd,
                        (
                            NULLIF(
                                (SELECT count(*) FROM ewalidata_data
                                INNER JOIN bidang_urusan_indikator ON bidang_urusan_indikator.kode_indikator=ewalidata_data.kode_indikator
                                WHERE bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                                AND bidang_urusan_indikator.status = 'AKTIF' {where_tahun})
                            ,0)
                            /
                            NULLIF(
                                (SELECT count(*) FROM bidang_urusan_indikator
                                WHERE kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                                AND status = 'AKTIF')
                            , 0)
                        )::float * 100 AS progress,
                        (
                            SELECT max(ewalidata_data.mdate) FROM ewalidata_data
                            INNER JOIN bidang_urusan_indikator ON bidang_urusan_indikator.kode_indikator = ewalidata_data.kode_indikator
                            WHERE bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                        ) AS last_update
                    FROM bidang_urusan
                    LEFT JOIN bidang_urusan_skpd ON bidang_urusan_skpd.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
                    LEFT JOIN skpd ON skpd.kode_skpd = bidang_urusan_skpd.kode_skpd
                    WHERE 1 = 1
                '''

            if skpd:
                sql += '''AND bidang_urusan_skpd.kode_skpd = %(skpd)s '''
                parameters['skpd'] = skpd

            for attr, value in where.items():
                sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += '''AND (LOWER(bidang_urusan.nama_bidang_urusan) LIKE :search OR LOWER(skpd.nama_skpd) LIKE %(search)s) '''
                parameters['search'] = f"%{search.lower()}%"

            if sort:
                sql += f'''ORDER BY {str(sort[0])} {str(sort[1])} '''

            sql += '''OFFSET %(skip)s LIMIT %(limit)s '''
            parameters['skip'] = skip
            parameters['limit'] = limit

            # execute the query with bind parameters
            success, result = db2.query_dict_array(sql, parameters)

            if success:
                # change into dict
                bidang_urusan = []
                for res in result:
                    temp = {}
                    temp['id'] = res['id']
                    temp['kode_bidang_urusan'] = res['kode_bidang_urusan']
                    temp['nama_bidang_urusan'] = res['nama_bidang_urusan']
                    temp['nama_skpd'] = res['nama_skpd']
                    temp['notes'] = res['notes']
                    if res['progress']:
                        temp['progress'] = round(res['progress'], 2)
                    else:
                        temp['progress'] = 0
                    temp['nama_skpd'] = res['nama_skpd']
                    if res['cdate']:
                        temp['cdate'] = res['cdate'].strftime(DATETIME_FORMAT)
                    if res['mdate']:
                        temp['mdate'] = res['mdate'].strftime(DATETIME_FORMAT)
                    if res['last_update']:
                        temp['last_update'] = res['last_update'].strftime(DATETIME_FORMAT)
                    bidang_urusan.append(temp)

                # check if empty
                bidang_urusan = list(bidang_urusan)
                if bidang_urusan:
                    return True, bidang_urusan
                else:
                    return False, []
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_progress_count(self, where: dict, search, tahun,skpd):
        ''' Doc: function get count  '''
        try:
            sql = f'''
                SELECT count(DISTINCT bidang_urusan.kode_bidang_urusan)
                from bidang_urusan
                LEFT join bidang_urusan_skpd on bidang_urusan_skpd.kode_bidang_urusan=bidang_urusan.kode_bidang_urusan
                LEFT JOIN skpd on skpd.kode_skpd=bidang_urusan_skpd.kode_skpd
                '''

            if skpd:
                sql = sql + f''' where bidang_urusan_skpd.kode_skpd = '{skpd}' '''
            for attr, value in where.items():
                sql = sql + f''' and {attr} = '{value}' '''
            if search:
                sql = sql + f''' and (lower(bidang_urusan.nama_bidang_urusan) like '%{search.lower()}%' or lower(skpd.nama_skpd) like '%{search.lower()}%') '''

            success, result = db2.query_dict_single(sql)

            # change into dict
            if success:
                data = {}
                data['count'] = result['count']
                return True, data
            else:
                data = {}
                data['count'] = 0
                return False, data

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_monitor(self, where: dict, search, sort, limit, skip, tahun, kode_skpd):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            sql_count_total = f'''
                select count(*) from bidang_urusan_indikator where bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
            '''
            sql_count_filter = f'''
                select count(*) from bidang_urusan_indikator_skpd as buis
                join bidang_urusan_indikator as bui on bui.kode_indikator = buis.kode_indikator
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                where bui.kode_bidang_urusan =  bidang_urusan.kode_bidang_urusan
            '''
            sql_count_diisi = f'''
                select count(*) from ewalidata_data as ed
                join bidang_urusan_indikator as bui on bui.kode_indikator = ed.kode_indikator
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                where bui.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan and ed.tahun = '{tahun}'
            '''
            sql_nama_skpd = f'''
                array(
                    select skpd.nama_skpd from bidang_urusan_indikator_skpd as buis
                    join bidang_urusan_indikator as bui on bui.kode_indikator = buis.kode_indikator
                    join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                    join skpd on skpd.kode_skpd = buis.kode_skpd
                    where bui.kode_bidang_urusan =  bidang_urusan.kode_bidang_urusan
                    GROUP BY skpd.nama_skpd
                )
            '''
            sql_kode_skpd = f'''
                array(
                    select skpd.kode_skpd from bidang_urusan_indikator_skpd as buis
                    join bidang_urusan_indikator as bui on bui.kode_indikator = buis.kode_indikator
                    join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                    join skpd on skpd.kode_skpd = buis.kode_skpd
                    where bui.kode_bidang_urusan =  bidang_urusan.kode_bidang_urusan
                    GROUP BY skpd.kode_skpd
                )
            '''
            sql_last_update = f'''
                select max(ed.mdate) from ewalidata_data as ed
                join bidang_urusan_indikator as bui on bui.kode_indikator = ed.kode_indikator
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                where bui.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan and ed.tahun = '{tahun}'
            '''

            sql = f'''
                    select
                    bidang_urusan.id,
                    bidang_urusan.kode_bidang_urusan,
                    bidang_urusan.nama_bidang_urusan,
                    {tahun} as tahun,
                    ({sql_count_total}) as count_total,
                    ({sql_count_filter} and status_assignment = 'Ditugaskan') as count_ditugaskan,
                    ({sql_count_filter} and status_assignment = 'Dikembalikan') as count_dikembalikan,
                    ({sql_count_filter} and status_assignment = 'Diterima') as count_diterima,
                    ({sql_count_filter} and status_assignment = 'Pengajuan Nonaktif') as count_pengajuan_nonaktif,
                    ({sql_count_filter} and status_assignment = 'Nonaktif') as count_nonaktif,
                    (
                        ({sql_count_total}) -
                        ({sql_count_filter} and status_assignment = 'Ditugaskan') -
                        ({sql_count_filter} and status_assignment = 'Dikembalikan') -
                        ({sql_count_filter} and status_assignment = 'Diterima') -
                        ({sql_count_filter} and status_assignment = 'Pengajuan Nonaktif') -
                        ({sql_count_filter} and status_assignment = 'Nonaktif')
                    ) as count_belum_ditugaskan,
                    ({sql_count_diisi}) as count_diisi,
                    CASE
                        WHEN ({sql_count_total}) > 0 THEN
                            ( ({sql_count_diisi})::FLOAT  / ({sql_count_total})::FLOAT ) * 100
                        ELSE 0.0
                    END as count_diisi_persentase,
                    ({sql_kode_skpd}) as kode_skpd,
                    ({sql_nama_skpd}) as nama_skpd,
                    ({sql_last_update}) as last_update
                    from bidang_urusan
                '''

            sql += f'''WHERE 1=1 '''

            if kode_skpd:
                sql += f'''AND ({sql_kode_skpd})::TEXT ILIKE '%{kode_skpd}%' '''

            for attr, value in where.items():
                if attr == 'nama_skpd':
                    sql += f'''AND ({sql_nama_skpd})::TEXT ILIKE '%{value}%' '''
                elif attr == 'kode_skpd':
                    sql += f'''AND ({sql_kode_skpd})::TEXT ILIKE '%{value}%' '''
                elif attr == 'status_assignment':
                    sql += f'''AND ({sql_count_filter} and {attr} = '{value}') > 0 '''
                else:
                    sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += f'''AND nama_bidang_urusan ILIKE '%{search}%' OR ({sql_nama_skpd})::TEXT ILIKE '%{search}%' '''

            if sort:
                sql += f'''ORDER BY {str(sort[0])} {str(sort[1])} '''

            sql += f'''OFFSET {skip} LIMIT {limit} '''

            # execute the query with bind parameters
            success, result = db2.query_dict_array(sql, None)

            if success:
                # change into dict
                bidang_urusan = []
                for res in result:
                    temp = res
                    if res['last_update']:
                        temp['last_update'] = res['last_update'].strftime(DATETIME_FORMAT)
                    bidang_urusan.append(temp)

                # check if empty
                bidang_urusan = list(bidang_urusan)

                if bidang_urusan:
                    return True, bidang_urusan
                else:
                    return False, []
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_monitor_count(self, where: dict, search, tahun, kode_skpd):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            sql_count_total = f'''
                select count(*) from bidang_urusan_indikator where bidang_urusan_indikator.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan
            '''
            sql_count_filter = f'''
                select count(*) from bidang_urusan_indikator_skpd as buis
                join bidang_urusan_indikator as bui on bui.kode_indikator = buis.kode_indikator
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                where bui.kode_bidang_urusan =  bidang_urusan.kode_bidang_urusan
            '''
            sql_count_diisi = f'''
                select count(*) from ewalidata_data as ed
                join bidang_urusan_indikator as bui on bui.kode_indikator = ed.kode_indikator
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                where bui.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan and ed.tahun = '{tahun}'
            '''
            sql_nama_skpd = f'''
                array(
                    select skpd.nama_skpd from bidang_urusan_indikator_skpd as buis
                    join bidang_urusan_indikator as bui on bui.kode_indikator = buis.kode_indikator
                    join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                    join skpd on skpd.kode_skpd = buis.kode_skpd
                    where bui.kode_bidang_urusan =  bidang_urusan.kode_bidang_urusan
                    GROUP BY skpd.nama_skpd
                )
            '''
            sql_kode_skpd = f'''
                array(
                    select skpd.kode_skpd from bidang_urusan_indikator_skpd as buis
                    join bidang_urusan_indikator as bui on bui.kode_indikator = buis.kode_indikator
                    join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                    join skpd on skpd.kode_skpd = buis.kode_skpd
                    where bui.kode_bidang_urusan =  bidang_urusan.kode_bidang_urusan
                    GROUP BY skpd.kode_skpd
                )
            '''
            sql_last_update = f'''
                select max(ed.mdate) from ewalidata_data as ed
                join bidang_urusan_indikator as bui on bui.kode_indikator = ed.kode_indikator
                join bidang_urusan as bu on bu.kode_bidang_urusan = bui.kode_bidang_urusan
                where bui.kode_bidang_urusan = bidang_urusan.kode_bidang_urusan and ed.tahun = '{tahun}'
            '''

            sql = f'''
                    select count(bidang_urusan.id)
                    from bidang_urusan
                '''

            sql += f'''WHERE 1=1 '''

            if kode_skpd:
                sql += f'''AND ({sql_kode_skpd})::TEXT ILIKE '%{kode_skpd}%' '''

            for attr, value in where.items():
                if attr == 'nama_skpd':
                    sql += f'''AND ({sql_nama_skpd})::TEXT ILIKE '%{value}%' '''
                elif attr == 'kode_skpd':
                    sql += f'''AND ({sql_kode_skpd})::TEXT ILIKE '%{value}%' '''
                elif attr == 'status_assignment':
                    sql += f'''AND ({sql_count_filter} and {attr} = '{value}') > 0 '''
                else:
                    sql += f'''AND {attr} = '{value}' '''

            if search:
                sql += f'''AND nama_bidang_urusan ILIKE '%{search}%' OR ({sql_nama_skpd})::TEXT ILIKE '%{search}%' '''

            success, result = db2.query_dict_single(sql)

            # change into dict
            if success:
                data = {}
                data['count'] = result['count']
                return True, data
            else:
                data = {}
                data['count'] = 0
                return False, data

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(BidangUrusanModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['cdate']:
                    result['cdate'] = result['cdate'].strftime(DATETIME_FORMAT)
                if result['mdate']:
                    result['mdate'] = result['mdate'].strftime(DATETIME_FORMAT)
            else:
                result = {}

            # change into dict
            bidang_urusan = result

            # check if empty
            if bidang_urusan:
                return True, bidang_urusan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = BidangUrusanModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, bidang_urusan = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, bidang_urusan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["mdate"] = HELPER.local_date_server()
            json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(BidangUrusanModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, bidang_urusan = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, bidang_urusan
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(BidangUrusanModel)
                for attr, value in where.items():
                    result = result.filter(getattr(BidangUrusanModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, bidang_urusan = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
