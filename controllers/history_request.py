''' Doc: class history request  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryRequestModel, RequestModel, SkpdModel, UserModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text, or_, cast
import sqlalchemy, sentry_sdk
import json
import requests
from settings import configuration as conf
from controllers.notification import NotificationController
from controllers.user import UserController

NOTIFICATION_CONTROLLER = NotificationController()
USER_CONTROLLER = UserController()

CONFIGURATION = Configuration()
HELPER = Helper()
MESSAGE_NO_TIKET = "Nomor tiket : "
MESSAGE_PERMOHONAN_DATA = "Permohonan data terkait <strong>"

# pylint: disable=singleton-comparison, unused-variable, unused-argument, broad-except
class HistoryRequestController(object):
    ''' Doc: class history request  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    def query(self, where: dict, search):
        ''' Doc: function query  '''
        try:
            # query postgresql
            result = db.session.query(
                RequestModel.id.label('request_id'), HistoryRequestModel.id.label('history_id'), RequestModel.title,
                SkpdModel.kode_skpd, SkpdModel.nama_skpd, SkpdModel.nama_skpd_alias,
                HistoryRequestModel.datetime, HistoryRequestModel.category,
                HistoryRequestModel.notes, UserModel.username)
            result = result.join(RequestModel, HistoryRequestModel.type_id == RequestModel.id)
            result = result.join(SkpdModel, RequestModel.kode_skpd == SkpdModel.kode_skpd)
            result = result.join(UserModel, HistoryRequestModel.user_id == UserModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryRequestModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryRequestModel, attr) == value)

            if search.lower() == 'persetujuan walidata':
                categorys = '1'
            elif search.lower() == 'proses opd':
                categorys = '2'
            elif search.lower() == 'ditolak':
                categorys = '4'
            elif search.lower() == 'Data Tersedia':
                categorys = '3'
            else:
                categorys = search

            result = result.filter(or_(
                cast(getattr(RequestModel, "title"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(SkpdModel, "nama_skpd_alias"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryRequestModel, "datetime"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryRequestModel, "notes"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(UserModel, "username"), sqlalchemy.String).ilike('%'+search+'%'),
                cast(getattr(HistoryRequestModel, "category"), sqlalchemy.String).ilike('%'+categorys+'%')
            ))

            return result

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.order_by(text("history_request."+sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            history_request = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})

                history_request.append(temp)

            # check if empty
            if history_request:
                return True, history_request
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, where: dict, search):
        ''' Doc: function get count  '''
        try:
            # query postgresql
            result = self.query(where, search)
            result = result.count()

            # check if empty
            if result:
                return True, result
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            result = self.query({'id':_id}, '')
            result = result.all()

            # change into dict
            history_request = []
            for res in result:
                temp = res._asdict()
                temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                history_request.append(temp)

            # check if empty
            if history_request:
                return True, history_request[0], "Get detail data successfull"
            else:
                return False, {}, "Data not found"

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def post_param(self, _json, condition):
        ''' Doc: function post param  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()
            # generate json data
            json_send = {}
            json_send = _json
            json_send["user_id"] = jwt['id']
            json_send["datetime"] = HELPER.local_date_server()

            return json_send

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create(self, _json: dict):
        ''' Doc: function create  '''
        try:
            # generate json data
            json_send = self.post_param(_json, 'add')
            # prepare data model
            result = HistoryRequestModel(**json_send)
            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, history_request, message = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, history_request
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update(self, where: dict, _json: dict):
        ''' Doc: function update  '''
        try:
            # generate json data
            json_send = self.post_param(_json, 'edit')
            try:
                # prepare data model
                result = db.session.query(HistoryRequestModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryRequestModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, history_request, msg = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, history_request
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryRequestModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryRequestModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, history_request, message = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def prepare_notification(self, history_request, json_notification, titles):
        ''' Doc: function delete  '''
        if history_request['category'] == '2' or history_request['category'] == 2:
            json_notification['title'] = MESSAGE_PERMOHONAN_DATA + titles
            json_notification['title'] += "</strong> anda sedang diproses. "
            json_notification['content'] = MESSAGE_NO_TIKET + str(history_request['request_id'])
        elif history_request['category'] == '3' or history_request['category'] == 3:
            json_notification['title'] = MESSAGE_PERMOHONAN_DATA + titles + "</strong> telah diakomodir, "
            json_notification['title'] += "lakukan pengecekan data pada Katalog Dataset atau Profil Organisasi. "
            json_notification['content'] = MESSAGE_NO_TIKET + str(history_request['request_id'])
        elif history_request['category'] == '4' or history_request['category'] == 4:
            json_notification['title'] = MESSAGE_PERMOHONAN_DATA + titles
            json_notification['title'] += "</strong> anda telah ditolak oleh Walidata. "
            json_notification['content'] = MESSAGE_NO_TIKET + str(history_request['request_id'])

        result, notification = NOTIFICATION_CONTROLLER.create(json_notification)
        return result, notification

    def send_notification(self, user_id, notification, request):
        ''' Doc: function delete  '''
        param = {}
        param['count_notif'] = 0
        res_, user = USER_CONTROLLER.get_by_id(user_id)
        if res_:
            if not user['count_notif']:
                user['count_notif'] = 0
            param['count_notif'] = user['count_notif'] + 1
            # execute database
            result = db.session.query(UserModel)
            result = result.filter(getattr(UserModel, 'id') == user_id)
            result = result.update(param, synchronize_session='fetch')
            result = db.session.commit()

        # try:
        #     socket_url = conf.SOCKET_URL
        #     token = request.headers.get('Authorization', None)
        #     requests.get(
        #         socket_url + 'notification' + '?receiver=' + str(notification['receiver']),
        #         headers={"Authorization":token, "Content-Type": "application/json"},
        #         data=json.dumps(notification)
        #     )
        # except Exception as err:
        #     print(str(err))
