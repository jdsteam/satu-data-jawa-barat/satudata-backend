'''bigdata controller'''
from sqlalchemy.sql.expression import true
from exceptions import ErrorMessage
import sqlalchemy
import re
import datetime
import json
import time
import pytz
import sentry_sdk
import pandas as pd
from sqlalchemy import text
from sqlalchemy import or_
from sqlalchemy import cast
from sqlalchemy import func
from sqlalchemy.orm import defer
from settings import configuration
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
import helpers.postgre_psycopg_bigdata as db2
from models import BaseModel
from models import DatasetModel
import contextlib
from psycopg2 import OperationalError

import logging
import re
from typing import Any, Dict, List, Tuple, Union

import numpy as np
import pandas as pd
import uuid
from sqlalchemy import (
    BigInteger,
    Boolean,
    Column,
    Float,
    Integer,
    String,
    Text,
    inspect,
    text,
)
from sqlalchemy.dialects.postgresql import BIGINT, JSONB
from sqlalchemy.exc import SQLAlchemyError
from settings.custom_session import bigdata_session, bigdata_engine

import logging
import re
from typing import List, Dict, Union, Tuple, Any

import pandas as pd
import numpy as np
import uuid

from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    Boolean,
    Text,
    inspect,
    BigInteger,
)
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy import  text
from sqlalchemy.exc import SQLAlchemyError
from flask import jsonify
from flask import current_app
from werkzeug.exceptions import HTTPException
from http import HTTPStatus
from sqlalchemy.exc import OperationalError, DatabaseError
import time


CONFIG = configuration.Configuration()
HELPER = Helper()
# pylint: disable=line-too-long, unused-variable, unused-argument, singleton-comparison, broad-except


class BigdataController(object):
    '''Bigdata Controller'''

    def __init__(self, **kwargs):
        pass

    def generate_metadata(self, schema, table):
        '''Generate metadata from database'''
        models = []
        metadatas = []
        try:
            if schema and table:
                engine = sqlalchemy.create_engine(
                    CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI, pool_size=configuration.SQLALCHEMY_POOL_SIZE, max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW)
                engine.connect()
                metadata = sqlalchemy.MetaData()
                sqlalchemy.Table(table, metadata, autoload=True, autoload_with=engine, schema=schema)

                string1 = repr(metadata.tables[schema+'.'+table])
                string2 = string1[6:-1]
                string3 = string2.replace("'"+table+"'", "")
                string4 = string3.replace(", schema='"+schema+"'", "")
                string5 = string4.replace(", MetaData(), ", "")
                string6 = string5.replace(", table=<"+table+">", "")

                array3 = string6.split('Column')
                array4 = []
                for arr in array3:
                    arr = arr.replace(')), ', ')')
                    if arr != '':
                        array4.append(arr)

                for arr in array4:
                    arr = arr.split(', ')
                    temp = {}
                    temp['column'] = arr[0].replace("'", "")
                    temp['column'] = temp['column'].replace("(", "")
                    temp['data_type'] = arr[1]
                    temp['data_type'] = temp['data_type'].replace('length=', '')
                    temp['data_type'] = temp['data_type'].replace(
                        'TEXT()', 'String(255)')
                    temp['data_type'] = temp['data_type'].replace(
                        'VARCHAR', 'String')
                    temp['data_type'] = temp['data_type'].replace('CHAR', 'String')
                    temp['data_type'] = temp['data_type'].replace(
                        'INTEGER', 'Integer')
                    temp['data_type'] = temp['data_type'].replace(
                        'BIGINT', 'Integer')
                    temp['data_type'] = temp['data_type'].replace(
                        'SMALLINT', 'Integer')
                    temp['data_type'] = temp['data_type'].replace(
                        'BOOLEAN', 'Boolean')
                    # temp['data_type'] = temp['data_type'].replace('DATE', 'DateTime(True)')
                    temp['data_type'] = temp['data_type'].replace(
                        'JSONB(astext_type=Text)', 'JSON')
                    if "DOUBLE_PRECISION" in temp['data_type']:
                        temp['data_type'] = 'Float'
                    if "REAL" in temp['data_type']:
                        temp['data_type'] = 'Float'
                    # if "TIMESTAMP" in temp['data_type']:
                        # temp['data_type'] = 'DateTime(True)'
                    temp['data_type'] = temp['data_type'].replace('()', '')
                    temp['is_primary'] = False

                    for a_r in arr:
                        is_primary = "primary_key" in a_r
                        if is_primary:
                            temp['is_primary'] = True

                    if temp['column'] != '':
                        models.append(temp)
                        metadatas.append(temp['column'])

                return models, metadatas

            return models, metadatas

        except Exception as err:
            print('exception generate metadata')
            print(err)
            return models, metadatas

    def generate_model(self, schema, table, model):
        '''generate model'''
        try:
            if schema and table and model:
                # add atribut model
                attr_dict = {}
                attr_dict['__bind_key__'] = 'bigdata'
                attr_dict['__tablename__'] = table
                attr_dict['__table_args__'] = {'schema': schema, 'extend_existing': True, "info": {"skip_autogenerate": True}}
                attr_dict['id'] = db.Column(db.Integer, primary_key=True)
                for mdl in model:
                    # if not id
                    if mdl['column'] != 'id':
                        # append
                        if mdl['data_type'] == 'String(255)':
                            attr_dict[mdl['column']] = db.Column(db.String(255))
                        elif mdl['data_type'] == 'Integer':
                            attr_dict[mdl['column']] = db.Column(db.Integer)
                        elif mdl['data_type'] == 'Float':
                            attr_dict[mdl['column']] = db.Column(db.Float)
                        # elif mdl['data_type'] == 'DateTime(True)':
                            # attr_dict[mdl['column']] = db.Column(db.DateTime(True))
                        elif mdl['data_type'] == 'Boolean':
                            attr_dict[mdl['column']] = db.Column(db.Boolean)
                        elif mdl['data_type'] == 'JSON':
                            attr_dict[mdl['column']] = db.Column(db.JSON)
                        else:
                            attr_dict[mdl['column']] = db.Column(db.String)

                # create model
                BaseModel.metadata.clear()
                bigdata_model = type('BigdataModel', (BaseModel, db.Model), attr_dict)

                return bigdata_model

            else:
                return None

        except Exception as err:
            print('exception generate model')
            print(str(err))
            return None

    def get_all(self, schema, table, where: dict, where_or: dict, search, sort, limit, skip, exclude):
        '''get all'''
        try:
            # generate model & metadata
            model, metadata = self.generate_metadata(schema, table)

            # create dynamic model class
            bigdata_model = self.generate_model(schema, table, model)
            # exclude
            for ex in exclude:
                metadata.remove(ex)

            # query data
            result = db.session.query(bigdata_model)
            for ex in exclude:
                result = result.options(defer(ex))
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(bigdata_model, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(bigdata_model, attr) == value)
            result = result.filter(or_(cast(getattr(bigdata_model, attr), sqlalchemy.String).ilike(
                '%'+str(value)+'%') for attr, value in where_or.items()))
            result = result.filter(or_(cast(getattr(bigdata_model, col.name), sqlalchemy.String).ilike(
                '%'+search+'%') for col in bigdata_model.__table__.columns))
            result = result.order_by(text(sort[0]+" "+sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            app = []
            for res in result:
                # check if data exist
                if res:
                    temp = res.__dict__
                    temp.pop('_sa_instance_state', None)

                    # set datetime
                    # for meta in metadata:
                    #     if temp[meta]:
                    #         for mdl in model:
                    #             if meta == mdl['column']:
                    #                 if mdl['data_type'] == 'DateTime(True)':
                    #                     temp[meta] = temp[meta].strftime('%Y-%m-%d %H:%M:%S')

                    app.append(temp)

            apps = []
            for ap_ in app:
                final_temp = {}

                for meta in metadata:
                    final_temp[meta] = ap_[meta]

                apps.append(final_temp)

            # check if empty
            apps = list(apps)
            if apps:
                return True, apps
            else:
                return False, []

        except Exception as err:
            # fail response
            if '(psycopg2.errors.UndefinedTable)' in str(err):
                raise ErrorMessage(
                    "Schema, table or controller not found.", 500, 1, {})
            else:
                print('exception get all')
                sentry_sdk.capture_exception(err)
                raise ErrorMessage(str(err), 500, 1, {})

    def get_count(self, schema, table, where: dict, where_or: dict, search):
        '''get count'''
        try:
            # generate model & metadata
            model, metadata = self.generate_metadata(schema, table)

            # create dynamic model class
            bigdata_model = self.generate_model(schema, table, model)

            # query data
            result = db.session.query(bigdata_model.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(
                        or_(getattr(bigdata_model, attr) == val for val in value))
                else:
                    result = result.filter(
                        getattr(bigdata_model, attr) == value)
            result = result.filter(or_(cast(getattr(bigdata_model, attr), sqlalchemy.String).ilike(
                '%'+str(value)+'%') for attr, value in where_or.items()))
            result = result.filter(or_(cast(getattr(bigdata_model, col.name), sqlalchemy.String).ilike(
                '%'+search+'%') for col in bigdata_model.__table__.columns))
            result = result.count()

            # change into dict
            app = {}
            app['count'] = result

            # check if empty
            if app:
                return True, app
            else:
                return False, {}

        except Exception as err:
            # fail response
            print('exception get count')
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_by_id(self, schema, table, ids):
        '''get by id'''
        try:
            # generate model & metadata
            model, metadata = self.generate_metadata(schema, table)

            # create dynamic model class
            bigdata_model = self.generate_model(schema, table, model)

            # query data
            result = db.session.query(bigdata_model)
            result = result.get(ids)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # set datetime
                # for meta in metadata:
                #     if result[meta]:
                #         for mdl in model:
                #             if meta == mdl['column']:
                #                 if mdl['data_type'] == 'DateTime(True)':
                #                     result[meta] = result[meta].strftime('%Y-%m-%d %H:%M:%S')

            else:
                result = {}

            # change into dict
            app = result

            apps = {}
            for meta in metadata:
                apps[meta] = app[meta]

            # check if empty
            if apps:
                return True, apps
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def get_metadata_filter(self, schema, table, metadata):
        '''get_metadata_filter'''
        try:
            # generate model & metadata
            model, metadata = self.generate_metadata(schema, table)

            # create dynamic model class
            bigdata_model = self.generate_model(schema, table, model)

            # count
            # count = db.session.query(bigdata_model.id)
            # count = count.count()

            result = []
            for md_ in metadata:
                temp = {}
                temp['key'] = md_
                temp['type'] = ''
                temp['value'] = []

                sql_count = db.session.query(
                    func.distinct(getattr(bigdata_model, md_)))
                sql_count = sql_count.count()
                if 'jumlah' in md_:
                    temp['type'] = 'free text'
                elif 'total' in md_:
                    temp['type'] = 'free text'
                elif 'nilai' in md_:
                    temp['type'] = 'free text'
                elif 'persen' in md_:
                    temp['type'] = 'free text'
                elif 'dengan' in md_:
                    temp['type'] = 'free text'
                elif 'tanpa' in md_:
                    temp['type'] = 'free text'
                elif 'terpakai' in md_:
                    temp['type'] = 'free text'
                elif 'tersedia' in md_:
                    temp['type'] = 'free text'
                else:
                    # if sql_count < count * 0.25:
                    if sql_count < 30:
                        temp['type'] = 'select'
                    else:
                        temp['type'] = 'free text'

                # group by
                if temp['type'] == 'select':
                    sql_group = db.session.query(getattr(bigdata_model, md_))
                    sql_group = sql_group.group_by(getattr(bigdata_model, md_))
                    sql_group = sql_group.order_by(text(md_ + ' asc'))
                    sql_group = sql_group.all()
                    for row in sql_group:
                        res = row._asdict()
                        temp['value'].append(res[md_])

                result.append(temp)

            return result

        except Exception as err:
            # fail response
            if '(psycopg2.errors.UndefinedTable)' in str(err):
                raise ErrorMessage(
                    "Schema, table or controller not found.", 500, 1, {})
            else:
                sentry_sdk.capture_exception(err)
                raise ErrorMessage(str(err), 500, 1, {})

    def create_dataset(self, dataset_id, dataset_name):
        '''doc string'''
        try:
            table_name = re.sub('[^a-zA-Z0-9- ]', '', dataset_name)
            table_name = 'od_' + \
                str(dataset_id) + '_' + \
                table_name.lower().replace(" ", "_").replace("-", "_")
            table_name = table_name.replace("(", "").replace(")", "").replace(
                ",", "").replace("_di_jawa_barat", "")
            table_name = table_name.replace(
                "jawa_barat", "jabar").replace('__', '_')
            if len(table_name) > 59:
                table_name = table_name.replace(
                    "jumlah", "jml").replace("berdasarkan", "brdsrkn")
                table_name = table_name.replace(
                    "jenis_kelamin", "jk").replace("pertumbuhan", "prtmbhn")
                table_name = table_name.replace(
                    "kesejahteraan", "ksjhtrn").replace("kesejahtetaan", "ksjhtrn")
                table_name = table_name.replace("potensi", "ptns").replace(
                    "sumber", "smbr").replace("_dan", "")
                table_name = table_name.replace("_atas", "").replace(
                    "sosial", "ssl").replace("berbasis", "brbss")
                table_name = table_name.replace(
                    "masyarakat", "msyrkt").replace("investasi", "invsts")
                table_name = table_name.replace(
                    "penanaman", "pnnmn").replace("penyerapan", "pnyrpn")
                table_name = table_name.replace(
                    "produksi", "prdks").replace("tanaman", "tnmn")
                table_name = table_name.replace(
                    "fasilitas", "fslts").replace('sasaran', 'ssrn')
                table_name = table_name.replace(
                    "prioritas", "prrts").replace("pembangunan", "pmbngnn")
                table_name = table_name.replace("usaha", "ush").replace(
                    "provinsi", "prov").replace("kedinasan", "kdnsn")
                table_name = table_name.replace(
                    "jml_ptns_smbr_ksjhtrn", "potsum").replace("kegiatan", "kgtn")
                table_name = table_name.replace(
                    "tenaga_kerja", "tk").replace("perpustakaan", "prpstkn")
                table_name = table_name.replace(
                    "brdsrkn", "").replace("kendaraan", "kndrn")
                table_name = table_name.replace("bermotor", "brmtr").replace(
                    "dewan_perwakilan_rakyat_daerah", "dprd")
                table_name = table_name.replace(
                    "implementasi", "implmnts").replace("akuntabilitas", "akntblts")
                table_name = table_name.replace(
                    "hasil_evaluasi", "eval").replace("domestik", "dmstk")
                table_name = table_name.replace(
                    "kesesuaian", "sesuai").replace('melakukan', 'mlkkn')
                table_name = table_name.replace(
                    '_yang', '').replace('sekretaris', 'skrtrs')
                table_name = table_name.replace(
                    'daerah', 'drh').replace('gubernur', 'gbrnr')
                table_name = table_name.replace(
                    'kementerian', 'kmntrn').replace('keamanan', 'aman')
                table_name = table_name.replace(
                    'informasi', 'infms').replace('administrasi', 'admnstrs')
                table_name = table_name.replace(
                    'pemerintah', 'pem').replace('penilaian', 'nilai')
                table_name = table_name.replace(
                    'kesiapan', 'siap').replace('tahun', 'thn')
                if len(table_name) > 59:
                    table_name = table_name[0:59]

            # create versioning table
            # stat_version, result_version = db2.query_dict_single(f"SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '%{table_name}%' ORDER BY TABLE_NAME desc LIMIT 1")
            # new_table = ''
            # if stat_version:
            #     if result_version:
            #         if '_v' in result_version[2]:
            #             versions = result_version[2].split('_v')
            #             version_number = None
            #             try:
            #                 version_number = int(versions[1]) + 1
            #             except Exception:
            #                 version_number = 1
            #             new_table = versions[0] + '_v' + str(version_number)
            #         else:
            #             new_table = table_name + '_v1'
            #     else:
            #         new_table = table_name + '_v1'
            # else:
            #     new_table = table_name + '_v1'

            return table_name

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_json(self, data: dict, metadata: list, dataset_id):
        ''' doc string '''

        try:
            # json variable
            temp = {}
            temp['data'] = data
            temp['metadata'] = metadata

            # filename timestamp
            server_time = datetime.datetime.utcnow()
            server_time = server_time.replace(tzinfo=pytz.utc).astimezone(
                pytz.timezone("Asia/Jakarta")).strftime('%Y%m%d_%H%M%S')
            filename = 'dataset-' + \
                str(dataset_id) + '-' + server_time + '.json'

            # write file
            with open('static/json/' + filename, "wt") as fi_:
                json.dump(temp, fi_, sort_keys=True, indent=4)

            return filename

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_table(self, data: dict, metadata: list, schema, table):
        ''' doc string '''

        try:
            # create dataframe
            df0 = pd.DataFrame.from_dict(
                pd.json_normalize(data), orient='columns')

            # check if any date type
            # check1 = [pd.to_datetime(df0[x]) if df0[x].astype(str).str.match(r'\d{4}-\d{2}-\d{2}').all() else df0[x] for x in df0.columns]
            # df1 = pd.concat(check1, axis=1, keys=[s.name for s in check1])

            # check2 = [pd.to_datetime(df1[x]) if df1[x].astype(str).str.match(r'\d{4}-\d{2}-\d{2} \d{2}\:\d{2}\:\d{2}').all() else df1[x] for x in df1.columns]
            # df2 = pd.concat(check2, axis=1, keys=[s.name for s in check2])

            # check if undefined
            df3 = df0
            indexlistid = []
            for col in df3.columns:
                if col == 'id':
                    indexlistid = df3.index[df3[col] == 'undefined'].tolist()
                else:
                    indexlist = df3.index[df3[col] == 'undefined'].tolist()
                    for il_ in indexlist:
                        df3[col][il_] = None
            for ili in indexlistid:
                df3 = df3.drop(df3.index[ili])

            # check if containing space in last character
            col = df3.columns.tolist()
            for i, row in df3.iterrows():
                for j in col:
                    df3.at[i, j] = self.remove_end_spaces(row[j])

            # change 'jumlah' to double
            df4 = df3
            for col in df4.columns:
                if 'id' in col.lower():
                    try:
                        del df4['id']
                    except Exception as err:
                        pass
                elif 'jumlah' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass
                elif 'nilai' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass
                elif 'luas' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass
                elif 'indeks' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass

            # data type
            df_type = dict(df4.dtypes)

            # connection
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI, pool_size=configuration.SQLALCHEMY_POOL_SIZE, max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW)

            if 'id' in df_type:
                df4.to_sql(table, con=engine, schema=schema, index=False)
            else:
                df4.index += 1
                df4.to_sql(table, con=engine, schema=schema, index=True, index_label='id')

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def update_table(self, data: dict, metadata: list, schema, table):
        ''' doc string '''

        try:
            # create dataframe
            df0 = pd.DataFrame.from_dict(
                pd.json_normalize(data), orient='columns')

            # check if any date type
            # check1 = [pd.to_datetime(df0[x]) if df0[x].astype(str).str.match(r'\d{4}-\d{2}-\d{2}').all() else df0[x] for x in df0.columns]
            # df1 = pd.concat(check1, axis=1, keys=[s.name for s in check1])

            # check2 = [pd.to_datetime(df1[x]) if df1[x].astype(str).str.match(r'\d{4}-\d{2}-\d{2} \d{2}\:\d{2}\:\d{2}').all() else df1[x] for x in df1.columns]
            # df2 = pd.concat(check2, axis=1, keys=[s.name for s in check2])

            # check if undefined
            df3 = df0
            indexlistid = []
            for col in df3.columns:
                if col == 'id':
                    indexlistid = df3.index[df3[col] == 'undefined'].tolist()
                else:
                    indexlist = df3.index[df3[col] == 'undefined'].tolist()
                    for il_ in indexlist:
                        df3[col][il_] = None
            for ili in indexlistid:
                df3 = df3.drop(df3.index[ili])

            # check if containing space in last character
            col = df3.columns.tolist()
            for i, row in df3.iterrows():
                for j in col:
                    df3.at[i, j] = self.remove_end_spaces(row[j])

            # change 'jumlah' to double
            df4 = df3
            for col in df4.columns:
                if 'id' in col.lower():
                    try:
                        del df4['id']
                    except Exception as err:
                        pass
                elif 'jumlah' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass
                elif 'nilai' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass
                elif 'luas' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass
                elif 'indeks' in col.lower():
                    try:
                        df4[col] = pd.to_numeric(df4[col], errors='coerce')
                    except Exception as err:
                        pass

            # data type
            df_type = dict(df4.dtypes)

            # kill locked process postgres
            # with contextlib.suppress(Exception):
            #     # pg_cancel_backend
            #     # pg_terminate_backend
            #     stat_terminate, result_terminate = db2.commit("""
            #         SELECT pg_terminate_backend(pg_stat_activity.pid)
            #         FROM pg_stat_activity
            #         WHERE pg_stat_activity.datname = 'bigdata' AND pid <> pg_backend_pid();
            #     """)

            # connection
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI,
                pool_size=configuration.SQLALCHEMY_POOL_SIZE,
                max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW
            )
            # create table
            if 'id' in df_type:
                df4.to_sql(table, con=engine, schema=schema, if_exists='replace', index=False)
            else:
                df4.index += 1
                df4.to_sql(table, con=engine, schema=schema, if_exists='replace', index=True, index_label='id')

            return {'schema': schema, 'table': table}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})

    def create_column(self, data: dict, metadata: list):
        ''' doc string '''
        try:
            temp_metadata = []
            temp_data = []

            # handle metadata
            for md in metadata:
                md_new = re.sub('[^a-zA-Z0-9-_ ]', '', md)
                md_new = md.lower().replace(" ", "_").replace("-", "_")
                md_new = md_new.replace("(", "").replace(
                    ")", "").replace(",", "").replace(".", "")
                temp_metadata.append(md_new)

            # handle data
            for dt in data:
                dt_new = {}
                for key, value in dt.items():
                    key_new = re.sub('[^a-zA-Z0-9-_ ]', '', key)
                    key_new = key_new.lower().replace(" ", "_").replace("-", "_")
                    key_new = key_new.replace("(", "").replace(
                        ")", "").replace(",", "").replace(".", "")
                    dt_new[key_new] = value
                temp_data.append(dt_new)

            return temp_data, temp_metadata

        except Exception as err:
            # fail response
            return data, metadata

    def force_restart(self):
        try:
            f = open("settings/reload.py", "a")
            f.write("\n")
            f.close()
            return True
        except Exception as err:
            # fail response
            print(err)
            return False

    def get_schema(self):
        '''Generate get_schema'''
        schemas = []
        try:
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI, pool_size=configuration.SQLALCHEMY_POOL_SIZE, max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW)
            insp = sqlalchemy.inspect(engine)
            schema = insp.get_schema_names()
            for sch in schema:
                schemas.append({'schema': sch})

            return schemas

        except Exception as err:
            print('exception get_schema')
            print(err)
            return schemas

    def get_table(self, param_schema, is_used):
        '''Generate get_table'''
        tables = []
        tables_final = []
        try:
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI, pool_size=configuration.SQLALCHEMY_POOL_SIZE, max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW)
            insp = sqlalchemy.inspect(engine)
            schema = insp.get_schema_names()
            for sch in schema:
                for tbl in insp.get_table_names(schema=sch):
                    if param_schema:
                        if param_schema == sch:
                            tables.append({'schema': sch, 'table': tbl})
                    else:
                        tables.append({'schema': sch, 'table': tbl})

            for tbl in tables:
                result = db.session.query(DatasetModel)
                result = result.filter(
                    getattr(DatasetModel, 'schema') == tbl['schema'])
                result = result.filter(
                    getattr(DatasetModel, 'table') == tbl['table'])
                result = result.count()
                if result > 0:
                    tbl['is_used'] = True
                else:
                    tbl['is_used'] = False

                if is_used == "true" and tbl['is_used'] == True:
                    tables_final.append(tbl)
                elif is_used == "false" and tbl['is_used'] == False:
                    tables_final.append(tbl)
                elif is_used == "":
                    tables_final.append(tbl)

            return tables_final

        except Exception as err:
            print('exception get_table')
            print(err)
            return tables

    def remove_end_spaces(self, string):
        '''remove_end_spaces'''
        if isinstance(string, str):
            new_string = "".join(string.rstrip())
            try:
                new_string2 = float(new_string)
                return new_string2
            except ValueError:
                return new_string
        else:
            return string

    def remove_first_end_spaces(self, string):
        '''remove_first_end_spaces'''
        return "".join(string.rstrip().lstrip())

    # Remove all spaces
    def remove_all_spaces(self, string):
        '''remove_all_spaces'''
        return "".join(string.split())

    def remove_all_extra_spaces(self, string):
        '''remove_all_extra_spaces'''
        return " ".join(string.split())

    def get_dashboard_insight(self, schema, table):
        ''' generate mini dashboard'''
        try:
            engine = sqlalchemy.create_engine(
                CONFIG.SQLALCHEMY_DATABASE_BIGDATA_URI,
                pool_size=configuration.SQLALCHEMY_POOL_SIZE,
                max_overflow=configuration.SQLALCHEMY_MAX_OVERFLOW
            )
            engine.connect()

            model, column_list = self.generate_metadata(schema, table)
            column_data = self.get_metadata_filter(schema, table, column_list)

            is_percentage = False
            div_percentage = 1

            result_metadata = {}
            # result_metadata['column_list'] = column_list
            # result_metadata['column_filter'] = column_data
            result_metadata['columns'] = model
            result_metadata['year'] = {}
            result_metadata['year']['success'] = False
            result_metadata['year']['message'] = 'Tidak ada tahun'
            result_metadata['key'] = {}
            result_metadata['key']['success'] = False
            result_metadata['key']['message'] = 'Tidak ada value'
            result_metadata['city'] = {}
            result_metadata['city']['success'] = False
            result_metadata['city']['message'] = 'Tidak ada value'

            result_data = {}
            result_data['total_by_year'] = {}
            result_data['total_by_year']['success'] = False
            result_data['total_by_year']['message'] = 'Tidak ada total'
            result_data['total_by_year']['value'] = {}
            result_data['total_by_year']['value']['data'] = []
            result_data['average_all_year'] = {}
            result_data['average_all_year']['success'] = False
            result_data['average_all_year']['message'] = 'Tidak ada rata-rata'
            result_data['average_all_year']['value'] = {}
            result_data['average_all_year']['value']['total_all_year'] = 0
            result_data['average_all_year']['value']['number_of_year'] = 0
            result_data['average_all_year']['value']['average'] = 0
            result_data['total_last_year'] = {}
            result_data['total_last_year']['success'] = False
            result_data['total_last_year']['message'] = 'Tidak ada total tahun'
            result_data['total_last_year']['value'] = {}
            result_data['total_by_city'] = {}
            result_data['total_by_city']['success'] = False
            result_data['total_by_city']['message'] = 'Tidak ada total wilayah'
            result_data['total_by_city']['value'] = {}
            result_data['total_by_city']['value']['data'] = []
            result_data['total_by_city']['value']['year'] = 0
            result_data['average_all_city'] = {}
            result_data['average_all_city']['success'] = False
            result_data['average_all_city']['message'] = 'Tidak ada rata-rata wilayah'
            result_data['average_all_city']['value'] = {}
            result_data['average_all_city']['value']['total_all_city'] = 0
            result_data['average_all_city']['value']['number_of_city'] = 0
            result_data['average_all_city']['value']['average'] = 0
            result_data['average_all_city']['value']['year'] = 0
            result_data['average_all_city']['value2'] = {}
            result_data['average_all_city']['value2']['total_all_city'] = 0
            result_data['average_all_city']['value2']['number_of_city'] = 0
            result_data['average_all_city']['value2']['average'] = 0
            result_data['average_all_city']['value2']['year'] = 0
            result_data['max_of_city'] = {}
            result_data['max_of_city']['success'] = False
            result_data['max_of_city']['message'] = 'Tidak ada max wilayah'
            result_data['max_of_city']['value'] = {}
            result_data['min_of_city'] = {}
            result_data['min_of_city']['success'] = False
            result_data['min_of_city']['message'] = 'Tidak ada min wilayah'
            result_data['min_of_city']['value'] = {}

            # check percentage or not
            percentage_column = ['persentase', 'persentasi', 'indeks', 'index']
            for cl in column_list:
                for pc in percentage_column:
                    if pc in cl:
                        is_percentage = True

            satuan = []
            if 'satuan' in column_list:
                for metafil in column_data:
                    if metafil['key'] == 'satuan':
                        satuan = metafil['value']
            percentage_satuan = ['persentase', 'persentasi', 'persen', 'indeks', 'index', 'PERSENTASE', 'PERSENTASI', 'PERSEN', 'INDEKS', 'INDEX']
            for ps in percentage_satuan:
                for sa in satuan:
                    if ps == sa:
                        is_percentage = True

            # tahun
            if 'tahun' in column_list:
                result_metadata['year']['message'] = 'Ada tahun'
                for metafil in column_data:
                    if metafil['key'] == 'tahun':
                        years = metafil['value']
                        years.sort(reverse=False)
                        result_metadata['year']['value'] = 'tahun'
                        result_metadata['year']['years'] = years
                        result_metadata['year']['last_year'] = years[-1]
                        result_metadata['year']['success'] = True

                        # counter data
                        sql_count = text(f'''
                            SELECT count(*)
                            FROM {schema}.{table}
                            WHERE {result_metadata['year']['value']} = '{result_metadata['year']['last_year']}'
                        ''')
                        result_count = engine.execute(sql_count)
                        result_counts = [dict(row) for row in result_count]
                        if result_count:
                            div_percentage = int(result_counts[0]['count'])

            # key
            # value_except = ['kode_', 'nama_', 'id', 'tahun', 'satuan', 'persentase', 'persentasi', 'indeks', 'index']
            value_except = ['kode_', 'nama_', 'id', 'tahun', 'satuan']
            value_list = [word for word in column_list if not any(bad in word for bad in value_except)]
            if value_list:
                result_metadata['key']['message'] = 'Ada value'
                for mdl in model:
                    if mdl['column'] == value_list[-1]:
                        if mdl['data_type'] == "Integer" or "Float":
                            result_metadata['key']['value'] = value_list[-1]
                            result_metadata['key']['success'] = True
            # if is_percentage:
            #     result_metadata['key']['success'] = False
            #     result_metadata['key']['message'] = 'Tidak ada value'
            #     if 'value' in result_metadata['key']:
            #         del result_metadata['key']['value']

            # kota
            value_kota = ['kota', 'kabupaten', 'provinsi']
            for col in column_list:
                for kot in value_kota:
                    if kot in col:
                        result_metadata['city']['message'] = 'Ada wilayah'
                        result_metadata['city']['value'] = col
                        result_metadata['city']['success'] = True

            if  result_metadata['year']['success'] and result_metadata['key']['success']:
                # total_by_year
                sql1 = text(
                    "SELECT " + result_metadata['year']['value'] + ", COALESCE(sum(" + result_metadata['key']['value'] + "), 0) as " + result_metadata['key']['value'] + " "
                    "FROM " + schema + "." + table + " " +
                    "GROUP BY " + result_metadata['year']['value'] + " " +
                    "ORDER BY " + result_metadata['year']['value'] + " asc "
                )
                sql_result1 = engine.execute(sql1)
                if sql_result1:
                    result_data['total_by_year']['message'] = 'Ada data total tahun'
                    result_data['total_by_year']['success'] = True
                    for sql_res1 in sql_result1:
                        sql_res1 = dict(sql_res1)
                        total = 0
                        if is_percentage:
                            total = round(float(sql_res1[result_metadata['key']['value']]) / float(div_percentage), 2)
                        else:
                            total = float(sql_res1[result_metadata['key']['value']])
                        result_data['total_by_year']['value']['data'].append({
                            result_metadata['year']['value']: sql_res1[result_metadata['year']['value']],
                            result_metadata['key']['value']: total,
                        })
                        result_data['average_all_year']['value']['total_all_year'] = result_data['average_all_year']['value']['total_all_year'] + total

                    # average_all_year
                    result_data['average_all_year']['message'] = 'Ada data rata-rata'
                    result_data['average_all_year']['success'] = True
                    result_data['average_all_year']['value']['number_of_year'] = len(result_metadata['year']['years'])
                    if result_data['average_all_year']['value']['total_all_year'] > 0 and result_data['average_all_year']['value']['number_of_year'] > 0:
                        result_data['average_all_year']['value']['average'] = round(result_data['average_all_year']['value']['total_all_year'] / result_data['average_all_year']['value']['number_of_year'], 2)

                    # total_last_year
                    result_data['total_last_year']['message'] = 'Ada data tahun terakhir'
                    result_data['total_last_year']['success'] = True
                    result_data['total_last_year']['value']['year'] = result_metadata['year']['last_year']
                    result_data['total_last_year']['value']['total'] = round(result_data['total_by_year']['value']['data'][-1][result_metadata['key']['value']], 2)
                    if len(result_metadata['year']['years']) > 1:
                        result_data['total_last_year']['value']['difference'] = round(result_data['total_by_year']['value']['data'][-1][result_metadata['key']['value']] - result_data['total_by_year']['value']['data'][-2][result_metadata['key']['value']], 2)
                        if result_data['total_by_year']['value']['data'][-1][result_metadata['key']['value']] != 0:
                            result_data['total_last_year']['value']['difference_percent'] = round(result_data['total_last_year']['value']['difference'] / result_data['total_by_year']['value']['data'][-1][result_metadata['key']['value']] * 100, 2)
                        else:
                            result_data['total_last_year']['value']['difference_percent'] = 0
                    else:
                        result_data['total_last_year']['value']['difference'] = round(result_data['total_by_year']['value']['data'][-1][result_metadata['key']['value']], 2)
                        result_data['total_last_year']['value']['difference_percent'] = 100
                else:
                    result_data['total_by_year']['message'] = 'Gagal query tahun'
                    result_data['total_by_year']['success'] = False
            else:
                result_data['total_by_year']['message'] = 'Tidak ada kolom Tahun / Value'
                result_data['total_by_year']['success'] = False

            if result_metadata['city']['success'] and result_metadata['key']['success']:
                # total_by_city
                sql2 = text(
                    "SELECT " + result_metadata['city']['value'] + ", " + result_metadata['key']['value'] + " "
                    "FROM " + schema + "." + table + " " +
                    "WHERE " + result_metadata['year']['value'] + " = " + str(result_metadata['year']['last_year']) + " "
                    "ORDER BY " + result_metadata['city']['value'] + " asc "
                )
                sql_result2 = engine.execute(sql2)
                if sql_result2:
                    result_data['total_by_city']['message'] = 'Ada data total wilayah'
                    result_data['total_by_city']['success'] = True
                    result_data['total_by_city']['value']['year'] = result_metadata['year']['last_year']
                    for sql_res2 in sql_result2:
                        result_data['total_by_city']['value']['data'].append({
                            result_metadata['city']['value']: sql_res2[result_metadata['city']['value']],
                            result_metadata['key']['value']: float(sql_res2[result_metadata['key']['value']]),
                        })

                        result_data['average_all_city']['value']['total_all_city'] = result_data['average_all_city']['value']['total_all_city'] + float(sql_res2[result_metadata['key']['value']])
                        if sql_res2[result_metadata['key']['value']] > 0:
                            result_data['average_all_city']['value2']['total_all_city'] = result_data['average_all_city']['value2']['total_all_city'] + float(sql_res2[result_metadata['key']['value']])
                            result_data['average_all_city']['value2']['number_of_city'] += 1

                    # average_all_city
                    result_data['average_all_city']['message'] = 'Ada data rata-rata wilayah'
                    result_data['average_all_city']['success'] = True
                    result_data['average_all_city']['value']['number_of_city'] = len(result_data['total_by_city']['value']['data'])
                    result_data['average_all_city']['value']['average'] = round(result_data['average_all_city']['value']['total_all_city'] / result_data['average_all_city']['value']['number_of_city'], 2)
                    result_data['average_all_city']['value']['year'] = result_metadata['year']['last_year']
                    result_data['average_all_city']['value2']['average'] = round(result_data['average_all_city']['value2']['total_all_city'] / result_data['average_all_city']['value2']['number_of_city'], 2)
                    result_data['average_all_city']['value2']['year'] = result_metadata['year']['last_year']

                    # max min
                    result_data['max_of_city']['message'] = 'Ada data max wilayah'
                    result_data['max_of_city']['success'] = True
                    result_data['max_of_city']['value']['year'] = result_metadata['year']['last_year']
                    result_data['max_of_city']['value']['data'] = max( result_data['total_by_city']['value']['data'], key=lambda x:x[result_metadata['key']['value']])
                    result_data['min_of_city']['message'] = 'Ada data min wilayah'
                    result_data['min_of_city']['success'] = True
                    result_data['min_of_city']['value']['year'] = result_metadata['year']['last_year']
                    result_data['min_of_city']['value']['data'] = min( result_data['total_by_city']['value']['data'], key=lambda x:x[result_metadata['key']['value']])

                else:
                    result_data['total_by_year']['message'] = 'Gagal query wilayah'
                    result_data['total_by_year']['success'] = False
            else:
                result_data['total_by_year']['message'] = 'Tidak ada kolom wilayah'
                result_data['total_by_year']['success'] = False

            return True, result_metadata, result_data

        except Exception as err:
            print('exception query dashboard')
            print(err)
            return False, {}, {}


app_logger = logging.getLogger("app.bigdata")


data_type_mapping = {
    "String(255)": String(255),
    "Integer": Integer,
    "Float": Float,
    "Boolean": Boolean,
    "JSON": JSONB,
    "Text": Text,
}


def get_columns(session, schema: str, table: str):
    try:
        result = session.execute(
            text(
                f"""
            SELECT column_name, data_type, column_default, is_nullable
            FROM information_schema.columns
            WHERE table_schema = :schema AND table_name = :table
            """
            ),
            {"schema": schema, "table": table},
        )
        columns = result.mappings().fetchall()
        return columns
    except SQLAlchemyError as e:
        app_logger.error(f"SQLAlchemyError when getting columns: {str(e)}", exc_info=True)
        raise


def generate_bigdata_model(session, schema: str, table: str) -> type:
    attr_dict = {
        "__tablename__": table,
        "__table_args__": {"schema": schema, "extend_existing": True, "info": {"skip_autogenerate": True}},
        "id": Column(Integer, primary_key=True),
    }
    _metadata = []

    try:
        columns = get_columns(session, schema, table)
        attributes = [
            {
                "column": column["column_name"],
                "data_type": column["data_type"],
                "is_primary": column["column_name"] == "id",
            }
            for column in columns
        ]
    except SQLAlchemyError as e:
        app_logger.error(f"SQLAlchemyError when inspecting columns: {str(e)}", exc_info=True)
        raise

    BaseModel.metadata.clear()

    for attr in attributes:
        column_name = attr["column"]
        if column_name != "id":
            sa_data_type = data_type_mapping.get(attr["data_type"], String(255))
            attr_dict[column_name] = Column(sa_data_type, primary_key=attr["is_primary"])
        _metadata.append(column_name)

    model = type("BigDataModel", (BaseModel,), attr_dict)
    model.__columns__ = _metadata
    model.attributes = attributes

    return model


def generate_column(data: dict) -> List[Dict[str, Union[str, float, int]]]:
    try:
        return [
            {re.sub("[^a-zA-Z0-9_ ]", "", key.lower().replace("-", "_")): value for key, value in dt.items()}
            for dt in data
        ]
    except Exception as e:
        app_logger.error(f"Error when generating column: {e}", exc_info=True)
        return []


def get_schema() -> List[str]:
    try:
        insp = inspect(bigdata_engine)
        return insp.get_schema_names()
    except SQLAlchemyError as e:
        app_logger.error(f"SQLAlchemyError when getting schema names: {str(e)}", exc_info=True)
        current_app.logger.error(f"SQLAlchemyError when getting schema names: {str(e)}")
        raise HTTPException(
            description=f"Gagal mengambil daftar schema database: {str(e)}",
            response=jsonify({"message": f"Gagal mengambil daftar schema database: {str(e)}"}),
        ) from e


def custom_to_numeric(x: Union[str, float, int]) -> Union[str, float, int]:
    if isinstance(x, str):
        x = x.strip().replace(",", ".")
        try:
            return float(x) if "." in x or "e" in x.lower() else int(x)
        except ValueError:
            return x
    return x


def detect_data_type(series: pd.Series) -> Union[String, Integer, Float, JSONB, BigInteger]:
    """Deteksi tipe data kolom secara cerdas."""
    # Hapus NaN untuk analisis yang lebih akurat
    clean_series = series.dropna()
    if clean_series.empty:
        return String(255)

    # Periksa apakah ada tipe data yang berbeda
    data_types = clean_series.apply(type).unique()
    if len(data_types) > 1:
        return String(255)

    # Cek apakah JSON
    if clean_series.dtype == "object":
        json_count = sum(1 for x in clean_series if isinstance(x, str) and x.strip().startswith("{"))
        if json_count / len(clean_series) > 0.5:
            return JSONB

    # Cek apakah numerik
    numeric_pattern = r"^-?\d*\.?\d+$"
    numeric_count = sum(
        1
        for x in clean_series
        if isinstance(x, (int, float)) or (isinstance(x, str) and re.match(numeric_pattern, x.strip()))
    )

    if numeric_count / len(clean_series) > 0.8:
        # Cek apakah integer atau float
        float_pattern = r"\d*\.\d+"
        float_count = sum(
            1
            for x in clean_series
            if isinstance(x, float) or (isinstance(x, str) and re.match(float_pattern, x.strip()))
        )

        if float_count > 0:
            return Float
        else:
            # Cek apakah perlu BIGINT
            try:
                max_val = max(abs(float(x)) for x in clean_series if pd.notna(x))
                if max_val > 2147483647:  # Batas maksimum INTEGER PostgreSQL
                    return BIGINT
                return Integer
            except (ValueError, TypeError):
                return Integer

    return String(255)


def smart_numeric_conversion(x: Any) -> Union[float, int, str, None]:
    """Konversi nilai ke numerik dengan penanganan kasus khusus."""
    if pd.isna(x) or x is None:
        return None

    if isinstance(x, (int, float)):
        return x

    if isinstance(x, str):
        x = x.strip().replace(",", ".")

        if "%" in x:
            try:
                return float(x.replace("%", "")) / 100
            except ValueError:
                return x

        currency_pattern = r"[Rp$€£¥]"
        if re.match(currency_pattern, x):
            try:
                cleaned = re.sub(r"[Rp$€£¥,\s]", "", x)
                return float(cleaned)
            except ValueError:
                return x

        try:
            if "." in x or "e" in x.lower():
                return float(x)
            return int(x)
        except ValueError:
            return x

    return x


def sanitize_table_name(table_name: str) -> str:
    """
    Mengubah string menjadi nama tabel yang valid untuk PostgreSQL.

    Rules:
    1. Hanya menggunakan huruf, angka, dan underscore
    2. Harus dimulai dengan huruf
    3. Maksimal 63 karakter
    4. Lowercase semua karakter
    """
    if not table_name:
        return f"table_{uuid.uuid4().hex[:10]}"

    sanitized = table_name.lower().strip().replace(" ", "_")
    sanitized = re.sub(r"[^a-z0-9_]", "", sanitized)

    if not sanitized or not sanitized[0].isalpha():
        sanitized = "tbl_" + sanitized

    if len(sanitized) > 63:
        sanitized = sanitized[:63]

    return sanitized


def calculate_optimal_chunk_size(rows: int, cols: int) -> int:
    """Menghitung ukuran chunk yang optimal berdasarkan jumlah baris dan kolom."""
    base_size = 500
    col_factor = max(0.5, min(1.0, 10 / cols if cols != 0 else 0.5))
    row_factor = 1.0

    if rows > 100000:
        row_factor = 0.5
    elif rows > 500000:
        row_factor = 0.25

    optimal_size = int(base_size * col_factor * row_factor)
    return max(100, min(1000, optimal_size))


def prepare_data(
    data: Union[List[Dict[str, Union[str, float, int]]], pd.DataFrame], schema: str
) -> Tuple[pd.DataFrame, Dict[str, str]]:
    """
    Mempersiapkan data untuk dimasukkan ke database.

    Melakukan validasi, konversi tipe data, dan sanitasi.
    """
    if not isinstance(schema, str) or not schema.strip():
        current_app.logger.error("Schema harus berupa string yang valid")
        raise HTTPException(
            description="Schema harus berupa string yang valid",
            response=jsonify({"message": "Schema harus berupa string yang valid"}),
        )

    try:
        df = pd.DataFrame(data) if not isinstance(data, pd.DataFrame) else data.copy()
    except Exception as e:
        current_app.logger.error(f"Gagal mengkonversi data ke DataFrame: {e}", exc_info=True)
        raise HTTPException(
            description="Gagal mengkonversi data ke DataFrame",
            response=jsonify({"message": "Gagal mengkonversi data ke DataFrame"}),
        ) from e

    if df.empty:
        current_app.logger.error("Data tidak boleh kosong")
        raise HTTPException(
            description="Data tidak boleh kosong",
            response=jsonify({"message": "Data tidak boleh kosong"}),
        )

    df.columns = df.columns.map(lambda x: re.sub(r"[^a-zA-Z0-9_]", "", x.lower().replace(" ", "_").replace("-", "_")))

    duplicate_cols = df.columns[df.columns.duplicated()].tolist()
    if duplicate_cols:
        current_app.logger.error(f"Ditemukan kolom duplikat: {duplicate_cols}")
        raise HTTPException(
            description=f"Ditemukan kolom duplikat: {duplicate_cols}",
            response=jsonify({"message": f"Ditemukan kolom duplikat: {duplicate_cols}"}),
        )

    try:
        df.replace(["undefined", "null", "nan", "NaN", "", "-"], np.nan, inplace=True)
        df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)
    except Exception as e:
        current_app.logger.error(f"Gagal membersihkan data: {str(e)}", exc_info=True)
        raise HTTPException(
            description=f"Gagal membersihkan data: {str(e)}",
            response=jsonify({"message": f"Gagal membersihkan data: {str(e)}"}),
        ) from e

    numeric_keywords = {
        "jumlah": 0.9,
        "nilai": 0.9,
        "total": 0.9,
        "harga": 0.9,
        "persen": 0.8,
        "rate": 0.8,
        "rasio": 0.8,
        "luas": 0.7,
        "berat": 0.7,
        "volume": 0.7,
        "index": 0.6,
        "score": 0.6,
    }

    conversion_errors = []
    for col in df.columns:
        confidence = max((conf for keyword, conf in numeric_keywords.items() if keyword in col.lower()), default=0)

        if confidence > 0:
            try:
                converted_series = df[col].apply(smart_numeric_conversion)
                if converted_series.apply(lambda x: isinstance(x, (int, float))).mean() >= confidence:
                    df[col] = converted_series
            except Exception as e:
                conversion_errors.append(f"Kolom '{col}': {str(e)}")

    if conversion_errors:
        app_logger.warning(f"Konversi kolom gagal: {', '.join(conversion_errors)}")

    column_types = None
    # try:
    #     column_types = {col: detect_data_type(df[col]) for col in df.columns}
    # except Exception as e:
    #     current_app.logger.error(f"Gagal mendeteksi tipe data kolom: {str(e)}", exc_info=True)
    #     raise HTTPException(
    #         description=f"Gagal mendeteksi tipe data kolom: {str(e)}",
    #         response=jsonify({"message": f"Gagal mendeteksi tipe data kolom: {str(e)}"}),
    #     ) from e

    # id_keywords = ["id", "code", "kode", "nomor", "no"]

    # for col in df.columns:
    #     col_lower = col.lower()
    #     if any(keyword in col_lower for keyword in id_keywords):
    #         column_types[col] = detect_data_type(df[col])
    #         if isinstance(column_types[col], Integer):
    #             column_types[col] = BIGINT

    new_index = range(1, len(df) + 1)
    df["id"] = new_index
    df.set_index("id", inplace=True)

    try:
        with bigdata_engine.connect() as conn:
            conn.execute(text("BEGIN"))
            try:
                if not conn.dialect.has_schema(conn, schema):
                    conn.execute(text(f"CREATE SCHEMA IF NOT EXISTS {schema}"))
                conn.execute(text("COMMIT"))
            except Exception:
                conn.execute(text("ROLLBACK"))
                raise
    except SQLAlchemyError as e:
        current_app.logger.error(f"Gagal membuat schema database: {str(e)}", exc_info=True)
        raise HTTPException(
            description=f"Gagal membuat schema database: {str(e)}",
            response=jsonify({"message": f"Gagal membuat schema database: {str(e)}"}),
        ) from e

    return df, column_types


def force_drop_table(conn, schema: str, table: str) -> None:
    full_table_name = f"{schema}.{table}"
    max_retries = 3
    retry_delay = 2  # detik

    for retry in range(max_retries):
        try:
            # 1. Set Timeout untuk Lock dan Statement
            conn.execute(text("SET lock_timeout TO '3s'"))  # Timeout untuk lock
            conn.execute(text("SET statement_timeout TO '5000'"))  # Timeout eksekusi (5 detik)

            # 2. Coba DROP TABLE
            conn.execute(text(f"DROP TABLE IF EXISTS {full_table_name} CASCADE"))
            app_logger.info(f"Tabel {full_table_name} berhasil dihapus.")
            return

        except OperationalError as oe:
            
            app_logger.warning(f"Gagal menghapus tabel (attempt {retry + 1}): {str(oe)}")

            # 3. Cari dan terminasi semua proses yang mengunci tabel
            query = f"""
                SELECT 
                    pg_stat_activity.pid,
                    pg_stat_activity.query,
                    pg_stat_activity.state
                FROM pg_locks
                JOIN pg_stat_activity ON pg_locks.pid = pg_stat_activity.pid
                JOIN pg_class ON pg_locks.relation = pg_class.oid
                WHERE pg_class.relname = '{table}'
                AND pg_stat_activity.pid <> pg_backend_pid();  -- Exclude current PID
            """
            locks = conn.execute(text(query)).fetchall()

            if not locks:
                app_logger.warning("Tidak ada proses lain yang mengunci tabel.")
                continue

            app_logger.warning(f"Menemukan {len(locks)} proses mengunci tabel. Mencoba terminasi...")
            for lock in locks:
                pid = lock[0]
                try:
                    # Terminasi proses dengan paksa
                    conn.execute(text(f"SELECT pg_terminate_backend({pid})"))
    
                    app_logger.info(f"Proses PID {pid} dihentikan.")
                except Exception as e:
                    
                    app_logger.error(f"Gagal terminasi PID {pid}: {str(e)}")

            # 4. Bersihkan session state
            try:
                conn.execute(text("DISCARD ALL"))  # Reset session state

            except Exception as e:
                
                app_logger.error(f"Gagal reset session: {str(e)}")

            time.sleep(retry_delay)

    # Jika masih gagal setelah semua percobaan
    app_logger.error(f"Gagal menghapus tabel {full_table_name} setelah {max_retries} percobaan.")
    raise RuntimeError(f"Gagal memaksa drop tabel {full_table_name}")
    
def create_table(
    data: Union[List[Dict[str, Union[str, float, int]]], pd.DataFrame], schema: str, table: str, max_attempts: int = 3
) -> Tuple[bool, str]:
    try:
        # Persiapkan data dan dapatkan tipe kolom

        df, _ = prepare_data(data, schema)

        # Validasi dan sanitasi nama tabel
        original_table = table
        # table = sanitize_table_name(table)

        # Hitung chunk size optimal
        optimal_chunk_size = calculate_optimal_chunk_size(len(df), len(df.columns))

        for attempt in range(max_attempts):
            # Generate nama tabel yang unik untuk percobaan selanjutnya
            if attempt > 0:
                suffix = uuid.uuid4().hex[:8]
                table = f"{table}_{suffix}"
                if len(table) > 63:
                    base_name = table[:54]
                    table = f"{base_name}_{suffix}"

            # Nama tabel temporary untuk proses ini
            temp_table = f"temp_{uuid.uuid4().hex}"

            with bigdata_engine.connect().execution_options(autocommit=True) as conn:
                try:
                    # Konfigurasi dasar untuk to_sql
                    base_settings = {
                        "schema": schema,
                        "con": bigdata_engine,
                        "if_exists": "replace",
                        "index": True,
                        "index_label": "id",
                        # "dtype": column_types,
                    }
                    # Create schema if not exists
                    if not conn.dialect.has_schema(conn, schema):
                        conn.execute(text(f"CREATE SCHEMA IF NOT EXISTS {schema}"))
                    try:
                        # Step 1: Coba simpan ke tabel temporary
                        df.to_sql(name=temp_table, chunksize=optimal_chunk_size, **base_settings)

                        # Step 2: Buat transaksi untuk penggantian tabel
                        force_drop_table(conn, schema, table)
                        conn.execute(text(f"ALTER TABLE {schema}.{temp_table} RENAME TO {table}"))
        
                        # Log jika nama tabel berubah
                        if original_table != table:
                            app_logger.info(f"Nama tabel diubah dari '{original_table}' menjadi '{table}'")
                            
                        return True, table

                    except SQLAlchemyError as e:
                        # Coba dengan chunk size yang lebih kecil jika gagal
                        fallback_chunk_sizes = [optimal_chunk_size // 2, optimal_chunk_size // 4, 100]

                        for chunk_size in fallback_chunk_sizes:
                            try:
                                # Hapus tabel temporary jika ada
                                conn.execute(text(f"DROP TABLE IF EXISTS {schema}.{temp_table} CASCADE"))

                                df.to_sql(name=table, chunksize=chunk_size, **base_settings)
                
                                return True, table

                            except SQLAlchemyError:
                                
                                if chunk_size == fallback_chunk_sizes[-1]:
                                    if attempt == max_attempts - 1:
                                        raise HTTPException(
                                            description="Terjadi kesalahan melalui percobaan tabel, silakan periksa kesalahan dalam proses pembuatan tabel",
                                            response=jsonify({"message": "Terjadi kesalahan melalui percobaan tabel"}),
                                        ) from e
                                    break
                                continue

                    finally:
                        # Pastikan tabel temporary dihapus
                        try:
                            conn.execute(text(f"DROP TABLE IF EXISTS {schema}.{temp_table} CASCADE"))
                        except:
                            pass

                except HTTPException as e:
                    # Jika bukan masalah naming, raise exception
                    if not any(err in str(e).lower() for err in ["nama", "name", "already exists"]):
                        raise

                    # Jika ini percobaan terakhir, raise exception
                    if attempt == max_attempts - 1:
                        app_logger.error(f"Kesalahan tidak terduga dalam create_table: {str(e)}", exc_info=True)
                        raise HTTPException(
                            description="Terjadi kesalahan melalui percobaan tabel, silakan periksa kesalahan dalam proses pembuatan tabel",
                            response=jsonify({"message": "Terjadi kesalahan melalui percobaan tabel"}),
                        ) from e

                    continue
                finally:
                    conn.close()

    except HTTPException:
        raise

    except Exception as e:
        app_logger.error(f"Kesalahan tidak terduga dalam create_table: {str(e)}", exc_info=True)
        raise HTTPException(
            description="Terjadi kesalahan internal yang tidak terduga saat membuat tabel",
            response=jsonify({"message": "Terjadi kesalahan internal yang tidak terduga saat membuat tabel"}),
        ) from e

def validate_data_row(
         data_json: Union[Dict[str, Any], List[Dict[str, Any]]]
    ) -> Union[Dict[str, Any], List[Dict[str, Any]]]:
        if not data_json:
            raise HTTPException(
                description="Baris data tidak boleh kosong",
                response=jsonify({"message": "Baris data tidak boleh kosong"}),
            )

        if isinstance(data_json, dict):
            data_json = [data_json]

        for index, data in enumerate(data_json):
            for key, value in data.items():
                if not key:
                    raise HTTPException(
                        description=f"Kolom {key} tidak boleh kosong",
                        response=jsonify({"message": f"Kolom {key} tidak boleh kosong"}),
                    )
                if not re.match(r"^[a-z_][a-z0-9_]*$", key, re.I):
                    raise HTTPException(
                        description=f"Kolom {key} tidak valid",
                        response=jsonify({"message": f"Kolom {key} tidak valid"}),
                    )
                if value is None or value == "":
                    raise HTTPException(
                        description=f"Kolom {key} baris data {index + 1} tidak boleh kosong",
                        response=jsonify({"message": f"Kolom {key} baris data {index + 1} tidak boleh kosong"}),
                    )
                if not isinstance(value, (int, float, str, bool)):
                    raise HTTPException(
                        description=f"Kolom {key} baris data {index + 1} memiliki tipe data yang tidak valid untuk tabel",
                        response=jsonify({"message": f"Kolom {key} baris data {index + 1} memiliki tipe data yang tidak valid untuk tabel"}),
                    )

        return data_json