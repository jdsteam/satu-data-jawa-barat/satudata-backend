''' Doc: controller bidang_urusan  '''
from settings.configuration import Configuration
from helpers import Helper
from exceptions import ErrorMessage
from models import HistoryEwalidataAssignmentModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import text
from sqlalchemy.orm import defer
from sqlalchemy import or_
from sqlalchemy import cast
import sqlalchemy, sentry_sdk
import helpers.postgre_psycopg as db2
CONFIGURATION = Configuration()
HELPER = Helper()
DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

# pylint: disable=broad-except, unused-variable, unused-argument, singleton-comparison, line-too-long
class HistoryEwalidataAssignmentController(object):
    ''' Doc: class bidang_urusan  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''


    def get_all(self, where: dict, search, sort, limit, skip):
        ''' Doc: function get all  '''
        try:
            # query postgresql
            result = db.session.query(HistoryEwalidataAssignmentModel)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryEwalidataAssignmentModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryEwalidataAssignmentModel, attr) == value)
            result = result.filter(or_(cast(getattr(HistoryEwalidataAssignmentModel, col.name), sqlalchemy.String).ilike('%' + search + '%') for col in HistoryEwalidataAssignmentModel.__table__.columns))
            result = result.order_by(text(sort[0] + " " + sort[1]))
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            datas = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                if temp['cdate']:
                    temp.update({"cdate":temp['cdate'].strftime("%Y-%m-%d %H:%M:%S")})
                if temp['datetime']:
                    temp.update({"datetime":temp['datetime'].strftime("%Y-%m-%d %H:%M:%S")})
                datas.append(temp)

            # check if empty
            datas = list(datas)
            if datas:
                return True, datas
            else:
                return False, []

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_count(self, where: dict, search):
        ''' Doc: function count  '''
        try:
            # query postgresql
            result = db.session.query(HistoryEwalidataAssignmentModel.id)
            for attr, value in where.items():
                is_array = isinstance(value, list)
                if is_array:
                    result = result.filter(or_(getattr(HistoryEwalidataAssignmentModel, attr) == val for val in value))
                else:
                    result = result.filter(getattr(HistoryEwalidataAssignmentModel, attr) == value)
            result = result.filter(or_(cast(getattr(HistoryEwalidataAssignmentModel, col.name), sqlalchemy.String).ilike('%' + search + '%') for col in HistoryEwalidataAssignmentModel.__table__.columns))
            result = result.count()

            # change into dict
            datas = {}
            datas['count'] = result

            # check if empty
            if datas:
                return True, datas
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def get_by_id(self, _id):
        ''' Doc: function get by id  '''
        try:
            # execute database
            result = db.session.query(HistoryEwalidataAssignmentModel)
            result = result.get(_id)

            if result:
                result = result.__dict__
                result.pop('_sa_instance_state', None)
                if result['cdate']:
                    result['cdate'] = result['cdate'].strftime(DATETIME_FORMAT)
                if result['datetime']:
                    result['datetime'] = result['datetime'].strftime(DATETIME_FORMAT)
            else:
                result = {}

            # change into dict
            bidang_urusan = result

            # check if empty
            if bidang_urusan:
                return True, bidang_urusan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def create(self, json: dict):
        ''' Doc: function create  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            json_send["cdate"] = HELPER.local_date_server()
            json_send["cuid"] = jwt['id']

            # prepare data model
            result = HistoryEwalidataAssignmentModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, bidang_urusan = self.get_by_id(result['id'])

            # check if exist
            if res:
                return True, bidang_urusan
            else:
                return False, {}

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def update(self, where: dict, json: dict):
        ''' Doc: function update  '''
        try:
            # get jwt payload
            jwt = HELPER.read_jwt()

            # generate json data
            json_send = {}
            json_send = json
            # json_send["mdate"] = HELPER.local_date_server()
            # json_send["muid"] = jwt['id']

            try:
                # prepare data model
                result = db.session.query(HistoryEwalidataAssignmentModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryEwalidataAssignmentModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, bidang_urusan = self.get_by_id(where["id"])

                # check if empty
                if res:
                    return True, bidang_urusan
                else:
                    return False, {}

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})


    def delete(self, where: dict):
        ''' Doc: function delete  '''
        try:

            try:
                # prepare data model
                result = db.session.query(HistoryEwalidataAssignmentModel)
                for attr, value in where.items():
                    result = result.filter(getattr(HistoryEwalidataAssignmentModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, bidang_urusan = self.get_by_id(where["id"])

                # check if exist
                if res:
                    return False
                else:
                    return True

            except Exception as err:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as err:
            # fail response
            sentry_sdk.capture_exception(err)
            raise ErrorMessage(str(err), 500, 1, {})
