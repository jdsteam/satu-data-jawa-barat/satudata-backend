FROM python:3.11-slim-bullseye AS base

ENV PYTHONHASHSEED=0 \
    PYTHONPATH=/app \
    PYTHONWRITEBYTECODE=1

WORKDIR /app

RUN apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends \
    libpq-dev \
    locales \
    locales-all \
    libmagic1 && \
    rm -rf /var/lib/apt/lists/*

FROM base AS builder

RUN apt-get update --fix-missing && \
    apt-get install -y --no-install-recommends \
    build-essential && \
    rm -rf /var/lib/apt/lists/*

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

RUN apt-get autoremove -y && \
    apt-get purge -y build-essential && \
    apt-get clean -y && \
    rm -rf /root/.cache /var/lib/apt/lists/*

FROM base AS app-image

COPY --from=builder /usr/local/lib/python3.11/site-packages/ /usr/local/lib/python3.11/site-packages/
COPY --from=builder /usr/local/bin/ /usr/local/bin/

COPY . /app

EXPOSE 5002
ENTRYPOINT ["gunicorn", "run:app", "-c", "./gunicorn_config.py"]
