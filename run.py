from gevent import monkey
monkey.patch_all()

from settings.http import  create_app
from settings.configuration import config

app = create_app()
port = int(config.PORT)
debug = int(config.DEBUG)
threaded = int(config.THREADED)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=port, debug=debug, threaded=threaded)
