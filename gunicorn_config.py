import multiprocessing

# Gunicorn configuration file

# Socket binding
bind = "0.0.0.0:5002"  # Replace with your desired host and port

# Worker processes
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = "gevent"
threads = 4

# Logging
loglevel = "info"
accesslog = "-"  # or a file path like "/var/log/gunicorn/access.log"
errorlog = "-"  # or a file path like "/var/log/gunicorn/error.log"

# Process options
timeout = 60
keepalive = 2
max_requests = 10000
max_requests_jitter = 1000

# Server mechanics
reload = False
preload_app = False

# Worker tuning
worker_connections = 2000
backlog = 2048
graceful_timeout = 30

# For debugging/development
#spew = True # Dump lots of information about each request
