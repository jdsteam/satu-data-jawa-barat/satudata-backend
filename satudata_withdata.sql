/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL Local
 Source Server Type    : PostgreSQL
 Source Server Version : 90613
 Source Host           : localhost:5432
 Source Catalog        : satudata
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 90613
 File Encoding         : 65001

 Date: 22/10/2019 10:03:04
*/


-- ----------------------------
-- Sequence structure for app_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."app_id_seq";
CREATE SEQUENCE "public"."app_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for app_service_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."app_service_id_seq";
CREATE SEQUENCE "public"."app_service_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for data_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."data_type_id_seq";
CREATE SEQUENCE "public"."data_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for indikator_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."indikator_type_id_seq";
CREATE SEQUENCE "public"."indikator_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for level_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."level_id_seq";
CREATE SEQUENCE "public"."level_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for regional_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."regional_id_seq";
CREATE SEQUENCE "public"."regional_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."role_id_seq";
CREATE SEQUENCE "public"."role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for sektoral_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."sektoral_id_seq";
CREATE SEQUENCE "public"."sektoral_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for skpd_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."skpd_id_seq";
CREATE SEQUENCE "public"."skpd_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for skpdsub_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."skpdsub_id_seq";
CREATE SEQUENCE "public"."skpdsub_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for upload_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."upload_id_seq";
CREATE SEQUENCE "public"."upload_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_id_seq";
CREATE SEQUENCE "public"."user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_permission_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_permission_id_seq";
CREATE SEQUENCE "public"."user_permission_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_type_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_type_id_seq";
CREATE SEQUENCE "public"."user_type_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for app
-- ----------------------------
DROP TABLE IF EXISTS "public"."app";
CREATE TABLE "public"."app" (
  "id" int8 NOT NULL DEFAULT nextval('app_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "url" text COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8,
  "is_active" bool,
  "is_deleted" bool
)
;

-- ----------------------------
-- Table structure for app_service
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_service";
CREATE TABLE "public"."app_service" (
  "id" int8 NOT NULL DEFAULT nextval('app_service_id_seq'::regclass),
  "app_id" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" text COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8,
  "is_active" bool,
  "is_deleted" bool
)
;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS "public"."article";
CREATE TABLE "public"."article" (
  "id" int8 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "image" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8,
  "is_active" bool,
  "is_deleted" bool
)
;

-- ----------------------------
-- Table structure for data_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."data_type";
CREATE TABLE "public"."data_type" (
  "id" int8 NOT NULL DEFAULT nextval('data_type_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for dataset
-- ----------------------------
DROP TABLE IF EXISTS "public"."dataset";
CREATE TABLE "public"."dataset" (
  "id" int8 NOT NULL,
  "indikator_type_id" int4 NOT NULL,
  "sektoral_id" int8 NOT NULL,
  "kode_skpd" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "kode_skpdsub" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "kode_kemendagri" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "app_id" int8,
  "app_service_id" int8,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "year" int2,
  "image" text COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "owner" varchar(255) COLLATE "pg_catalog"."default",
  "owner_email" varchar(255) COLLATE "pg_catalog"."default",
  "maintener" varchar(255) COLLATE "pg_catalog"."default",
  "maintener_email" varchar(255) COLLATE "pg_catalog"."default",
  "notes" text COLLATE "pg_catalog"."default",
  "cuid" int8,
  "cdate" date,
  "is_active" bool,
  "is_deleted" bool,
  "is_validate" bool,
  "is_indikator" bool
)
;

-- ----------------------------
-- Table structure for indikator_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."indikator_type";
CREATE TABLE "public"."indikator_type" (
  "id" int8 NOT NULL DEFAULT nextval('indikator_type_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for level
-- ----------------------------
DROP TABLE IF EXISTS "public"."level";
CREATE TABLE "public"."level" (
  "id" int8 NOT NULL DEFAULT nextval('level_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for metadata
-- ----------------------------
DROP TABLE IF EXISTS "public"."metadata";
CREATE TABLE "public"."metadata" (
  "id" int8 NOT NULL,
  "data_type" varchar(255) COLLATE "pg_catalog"."default",
  "dataset_id" int8 NOT NULL,
  "indikator_id" int8,
  "key" varchar(255) COLLATE "pg_catalog"."default",
  "datatype" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for regional
-- ----------------------------
DROP TABLE IF EXISTS "public"."regional";
CREATE TABLE "public"."regional" (
  "id" int8 NOT NULL DEFAULT nextval('regional_id_seq'::regclass),
  "level" int4 NOT NULL,
  "kode_bps" int8,
  "nama_bps" varchar(255) COLLATE "pg_catalog"."default",
  "kode_kemendagri" varchar(255) COLLATE "pg_catalog"."default",
  "nama_kemendagri" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8
)
;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS "public"."role";
CREATE TABLE "public"."role" (
  "id" int8 NOT NULL DEFAULT nextval('role_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO "public"."role" VALUES (1, 'superadmin', NULL);

-- ----------------------------
-- Table structure for sektoral
-- ----------------------------
DROP TABLE IF EXISTS "public"."sektoral";
CREATE TABLE "public"."sektoral" (
  "id" int8 NOT NULL DEFAULT nextval('sektoral_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8
)
;

-- ----------------------------
-- Table structure for skpd
-- ----------------------------
DROP TABLE IF EXISTS "public"."skpd";
CREATE TABLE "public"."skpd" (
  "id" int8 NOT NULL DEFAULT nextval('skpd_id_seq'::regclass),
  "regional_id" int8 NOT NULL,
  "kode_skpd" varchar(255) COLLATE "pg_catalog"."default",
  "nama_skpd" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8
)
;

-- ----------------------------
-- Table structure for skpdsub
-- ----------------------------
DROP TABLE IF EXISTS "public"."skpdsub";
CREATE TABLE "public"."skpdsub" (
  "id" int8 NOT NULL DEFAULT nextval('skpdsub_id_seq'::regclass),
  "regional_id" int8 NOT NULL,
  "skpd_id" int4 NOT NULL,
  "kode_skpd" varchar(255) COLLATE "pg_catalog"."default",
  "kode_skpdsub" varchar(255) COLLATE "pg_catalog"."default",
  "nama_skpd" varchar(255) COLLATE "pg_catalog"."default",
  "nama_skpdsub" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8
)
;

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS "public"."tag";
CREATE TABLE "public"."tag" (
  "id" int8 NOT NULL,
  "data_type" varchar(255) COLLATE "pg_catalog"."default",
  "dataset_id" int8,
  "indikator_id" int8,
  "tag" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for upload
-- ----------------------------
DROP TABLE IF EXISTS "public"."upload";
CREATE TABLE "public"."upload" (
  "id" int8 NOT NULL DEFAULT nextval('upload_id_seq'::regclass),
  "user_id" int8 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "path" text COLLATE "pg_catalog"."default",
  "type" varchar(255) COLLATE "pg_catalog"."default",
  "notes" text COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8
)
;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('user_id_seq'::regclass),
  "role_id" int8 NOT NULL,
  "username" varchar(255) COLLATE "pg_catalog"."default",
  "email" varchar(255) COLLATE "pg_catalog"."default",
  "password" varchar(255) COLLATE "pg_catalog"."default",
  "nip" varchar(255) COLLATE "pg_catalog"."default",
  "user_type" varchar(255) COLLATE "pg_catalog"."default",
  "kode_kemendagri" varchar(255) COLLATE "pg_catalog"."default",
  "kode_skpd" varchar(255) COLLATE "pg_catalog"."default",
  "kode_skpdsub" varchar(255) COLLATE "pg_catalog"."default",
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "profile_pic" varchar(255) COLLATE "pg_catalog"."default",
  "last_login" date,
  "notes" text COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8,
  "is_active" bool,
  "is_deleted" bool
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (3, 1, '123', NULL, '$pbkdf2-sha256$29000$fk8JIcRYy5mTco5xTgnB2A$N6L.HMs0cOvo/FNoKrOC13whAHuAh7EKUIX0vKUEiZM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users_permission
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_permission";
CREATE TABLE "public"."users_permission" (
  "id" int8 NOT NULL DEFAULT nextval('user_permission_id_seq'::regclass),
  "user_id" int8 NOT NULL,
  "app_id" int8 NOT NULL,
  "service_id" int8 NOT NULL,
  "action" varchar(255) COLLATE "pg_catalog"."default",
  "enable" bool,
  "notes" text COLLATE "pg_catalog"."default",
  "cdate" date,
  "cuid" int8
)
;

-- ----------------------------
-- Table structure for users_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_type";
CREATE TABLE "public"."users_type" (
  "id" int8 NOT NULL DEFAULT nextval('user_type_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "notes" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."app_id_seq"
OWNED BY "public"."app"."id";
SELECT setval('"public"."app_id_seq"', 2, false);
ALTER SEQUENCE "public"."app_service_id_seq"
OWNED BY "public"."app_service"."id";
SELECT setval('"public"."app_service_id_seq"', 2, false);
ALTER SEQUENCE "public"."data_type_id_seq"
OWNED BY "public"."data_type"."id";
SELECT setval('"public"."data_type_id_seq"', 2, false);
ALTER SEQUENCE "public"."indikator_type_id_seq"
OWNED BY "public"."indikator_type"."id";
SELECT setval('"public"."indikator_type_id_seq"', 2, false);
ALTER SEQUENCE "public"."level_id_seq"
OWNED BY "public"."level"."id";
SELECT setval('"public"."level_id_seq"', 2, false);
ALTER SEQUENCE "public"."regional_id_seq"
OWNED BY "public"."regional"."id";
SELECT setval('"public"."regional_id_seq"', 2, false);
ALTER SEQUENCE "public"."role_id_seq"
OWNED BY "public"."role"."id";
SELECT setval('"public"."role_id_seq"', 2, false);
ALTER SEQUENCE "public"."sektoral_id_seq"
OWNED BY "public"."sektoral"."id";
SELECT setval('"public"."sektoral_id_seq"', 2, false);
ALTER SEQUENCE "public"."skpd_id_seq"
OWNED BY "public"."skpd"."id";
SELECT setval('"public"."skpd_id_seq"', 2, false);
ALTER SEQUENCE "public"."skpdsub_id_seq"
OWNED BY "public"."skpdsub"."id";
SELECT setval('"public"."skpdsub_id_seq"', 2, false);
ALTER SEQUENCE "public"."upload_id_seq"
OWNED BY "public"."upload"."id";
SELECT setval('"public"."upload_id_seq"', 2, false);
ALTER SEQUENCE "public"."user_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."user_id_seq"', 4, true);
ALTER SEQUENCE "public"."user_permission_id_seq"
OWNED BY "public"."users_permission"."id";
SELECT setval('"public"."user_permission_id_seq"', 2, false);
ALTER SEQUENCE "public"."user_type_id_seq"
OWNED BY "public"."users_type"."id";
SELECT setval('"public"."user_type_id_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table app
-- ----------------------------
ALTER TABLE "public"."app" ADD CONSTRAINT "app_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table app_service
-- ----------------------------
ALTER TABLE "public"."app_service" ADD CONSTRAINT "app_service_pkey" PRIMARY KEY ("id", "app_id");

-- ----------------------------
-- Primary Key structure for table article
-- ----------------------------
ALTER TABLE "public"."article" ADD CONSTRAINT "article_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table data_type
-- ----------------------------
ALTER TABLE "public"."data_type" ADD CONSTRAINT "data_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table dataset
-- ----------------------------
ALTER TABLE "public"."dataset" ADD CONSTRAINT "dataset_pkey" PRIMARY KEY ("id", "kode_skpd", "kode_skpdsub", "kode_kemendagri", "sektoral_id", "indikator_type_id");

-- ----------------------------
-- Primary Key structure for table indikator_type
-- ----------------------------
ALTER TABLE "public"."indikator_type" ADD CONSTRAINT "indikator_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table level
-- ----------------------------
ALTER TABLE "public"."level" ADD CONSTRAINT "level_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table metadata
-- ----------------------------
ALTER TABLE "public"."metadata" ADD CONSTRAINT "metadata_pkey" PRIMARY KEY ("id", "dataset_id");

-- ----------------------------
-- Primary Key structure for table regional
-- ----------------------------
ALTER TABLE "public"."regional" ADD CONSTRAINT "regional_pkey" PRIMARY KEY ("id", "level");

-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE "public"."role" ADD CONSTRAINT "role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sektoral
-- ----------------------------
ALTER TABLE "public"."sektoral" ADD CONSTRAINT "sektoral_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table skpd
-- ----------------------------
ALTER TABLE "public"."skpd" ADD CONSTRAINT "skpd_pkey" PRIMARY KEY ("id", "regional_id");

-- ----------------------------
-- Primary Key structure for table skpdsub
-- ----------------------------
ALTER TABLE "public"."skpdsub" ADD CONSTRAINT "skpdsub_pkey" PRIMARY KEY ("id", "regional_id", "skpd_id");

-- ----------------------------
-- Primary Key structure for table tag
-- ----------------------------
ALTER TABLE "public"."tag" ADD CONSTRAINT "tag_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table upload
-- ----------------------------
ALTER TABLE "public"."upload" ADD CONSTRAINT "upload_pkey" PRIMARY KEY ("id", "user_id");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id", "role_id");

-- ----------------------------
-- Primary Key structure for table users_permission
-- ----------------------------
ALTER TABLE "public"."users_permission" ADD CONSTRAINT "user_permission_pkey" PRIMARY KEY ("id", "user_id", "app_id", "service_id");

-- ----------------------------
-- Primary Key structure for table users_type
-- ----------------------------
ALTER TABLE "public"."users_type" ADD CONSTRAINT "user_type_pkey" PRIMARY KEY ("id");
