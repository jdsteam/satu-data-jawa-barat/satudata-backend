''' Doc: model indikator history '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorHistoryModel(BaseModel, db.Model):
    """Model for the indikator_history_class table"""
    __tablename__ = 'indikator_history_class'

    id = db.Column(db.Integer, primary_key=True)
    indikator_id = db.Column(db.Integer)
    indikator_class_id = db.Column(db.Integer)

def __init__(self, indikator_id, indikator_class_id):
    self.indikator_id = indikator_id
    self.indikator_class_id = indikator_class_id
