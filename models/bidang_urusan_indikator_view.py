''' Doc: model bidang_urusan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy.orm import relationship

class BidangUrusanIndikatorViewModel(BaseModel, db.Model):
    """Model for the bidang_urusan table"""
    __tablename__ = 'bidang_urusan_indikator_view'

    id = db.Column(db.Integer, primary_key=True)
    kode_bidang_urusan = db.Column(db.String(255))
    kode_indikator = db.Column(db.String(255))
    nama_indikator = db.Column(db.String(255))
    kode_skpd = db.Column(db.String(255))
    nama_skpd = db.Column(db.Text)
    satuan = db.Column(db.String(255))
    definisi_operasional = db.Column(db.String(255))
    uraian_indikator = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    id_assignment = db.Column(db.Integer)
    status = db.Column(db.String(255))
    status_assignment = db.Column(db.String(255))
    status_available = db.Column(db.Integer)

def __init__(self, kode_bidang_urusan, kode_indikator):
    self.kode_bidang_urusan = kode_bidang_urusan
    self.kode_indikator = kode_indikator
