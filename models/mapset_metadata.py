''' Doc: model mapset metadata '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class MapsetMetadataModel(BaseModel, db.Model):
    """Model for the mapset_metadata table"""
    __tablename__ = 'mapset_metadata'

    id = db.Column(db.Integer, primary_key=True)
    mapset_metadata_type_id = db.Column(db.Integer, db.ForeignKey("mapset_metadata_type.id"))
    mapset_id = db.Column(db.Integer, db.ForeignKey("mapset.id"))
    key = db.Column(db.String(255))
    value = db.Column(db.String(255))
    description = db.Column(db.String(255))

def __init__(self, mapset_metadata_type_id, mapset_id, key, value):
    self.mapset_metadata_type_id = mapset_metadata_type_id
    self.mapset_id = mapset_id
    self.key = key
    self.value = value
