''' Doc: model indikator category '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorCategoryModel(BaseModel, db.Model):
    """Model for the indikator_category table"""
    __tablename__ = 'indikator_category'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, name, notes):
    self.name = name
    self.notes = notes
