''' Doc: model history download '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class CsatModel(BaseModel, db.Model):
    """Model for the csat table"""
    __tablename__ = 'csat'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    type = db.Column(db.String(250))
    email = db.Column(db.String(250))
    category = db.Column(db.String(250))
    category_id = db.Column(db.Integer)
    tujuan = db.Column(db.String(250))
    sektor = db.Column(db.String(250))
    data_ditemukan = db.Column(db.Boolean, default=True)
    data_masalah = db.Column(db.String(250))
    mudah_didapatkan = db.Column(db.Boolean, default=True)
    saran = db.Column(db.Text)
    score = db.Column(db.Integer)
    notes = db.Column(db.Text)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, _type, email, category, category_id, tujuan, sektor, data_ditemukan, data_masalah, mudah_didapatkan, saran, score):
    self.type = _type
    self.email = email
    self.category = category
    self.category_id = category_id
    self.tujuan = tujuan
    self.sektor = sektor
    self.data_ditemukan = data_ditemukan
    self.data_masalah = data_masalah
    self.mudah_didapatkan = mudah_didapatkan
    self.saran = saran
    self.score = score
