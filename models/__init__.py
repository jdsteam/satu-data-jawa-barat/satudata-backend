''' Doc: model init '''
from .base import BaseModel
from .app import AppModel
from .app_service import AppServiceModel
from .sektoral import SektoralModel
from .regional import RegionalModel
from .regional_level import RegionalLevelModel
from .skpd import SkpdModel
from .skpdsub import SkpdSubModel
from .skpdunit import SkpdUnitModel
from .dataset_class import DatasetClassModel
from .dataset_type import DatasetTypeModel
from .dataset_tag import DatasetTagModel
from .dataset import DatasetModel
from .metadata_type import MetadataTypeModel
from .metadata import MetadataModel
from .license import LicenseModel
from .notification import NotificationModel
from .review import ReviewModel
from .history import HistoryModel
from .search import SearchModel
from .user import UserModel
from .mapset import MapsetModel
from .mapset_source import MapsetSourceModel
from .mapset_type import MapsetTypeModel
from .mapset_kugi import MapsetKugiModel
from .mapset_kugisub import MapsetKugiSubModel
from .user_permission import UserPermissionModel
from .upload import UploadModel
from .visualization import VisualizationModel
from .visualization_dataset import VisualizationDatasetModel
from .visualization_access import VisualizationAccessModel
from .metadata_visualization import MetadataVisualizationModel
from .user_app_role import UserAppRoleModel
from .role import RoleModel
from .history_login import HistoryLoginModel
from .history_draft import HistoryDraftModel
from .history_request import HistoryRequestModel
from .request import RequestModel
from .dataset_view import DatasetViewModel
from .dataset_view_all import DatasetViewAllModel
from .indikator_category import IndikatorCategoryModel
from .indikator_class import IndikatorClassModel
from .indikator_history import IndikatorHistoryModel
from .indikator import IndikatorModel
from .indikator_progress import IndikatorProgressModel
from .satuan import SatuanModel
from .infographic import InfographicModel
from .infographic_detail import InfographicDetailModel
from .highlight import HighlightModel
from .highlight_view import HighlightViewModel
from .history_download import HistoryDownloadModel
from .feedback import FeedbackModel
from .feedback_masalah import FeedbackMasalahModel
from .mapset_metadata import MapsetMetadataModel
from .mapset_metadata_type import MapsetMetadataTypeModel
from .request_public import RequestPublicModel
from .history_request_public import HistoryRequestPublicModel
from .request_private import RequestPrivateModel
from .history_request_private import HistoryRequestPrivateModel
from .feedback_private import FeedbackPrivateModel
from .feedback_map import FeedbackMapModel
from .feedback_map_masalah import FeedbackMapMasalahModel
from .agreement import AgreementModel
from .struktur_organisasi import StrukturOrganisasiModel
from .dataset_permission import DatasetPermissionModel
from .visualization_permission import VisualizationPermissionModel
from .article_topic import ArticleTopicModel
from .article import ArticleModel
from .jabatan import JabatanModel
from .user_view import UserViewModel
from .mapset_program import MapsetProgramModel
from .bkd_jabatan_extract import BKDJabatanExtract
from .mapset_law import MapsetLawModel
from .mapset_award import MapsetAwardModel
from .mapset_story import MapsetStoryModel
from .bidang_urusan import BidangUrusanModel
from .mapset_thematic import MapsetThematicModel
from .mapset_thematic_source import MapsetThematicSourceModel
from .mapset_thematic_metadata import MapsetThematicMetadataModel
from .dataset_quality_result import DatasetQualityResultModel
from .dataset_quality_result_history import DatasetQualityResultHistoryModel
from .nps import NpsModel
from .csat import CsatModel
from .mapset_area_coverage import MapsetAreaCoverageModel
from .history_draft_view import HistoryDraftViewModel
from .history_draft_all_view import HistoryDraftAllViewModel
from .bidang_urusan_skpd import BidangUrusanSkpdModel
from .bidang_urusan_indikator import BidangUrusanIndikatorModel
from .bidang_urusan_indikator_skpd import BidangUrusanIndikatorSkpdModel
from .ewalidata_data import EwalidataDataModel
from .mapset_metadata_kugi import MapsetMetadataKugiModel
from .bidang_urusan_view import BidangUrusanViewModel
from .bidang_urusan_indikator_view import BidangUrusanIndikatorViewModel
from .history_ewalidata_assignment import HistoryEwalidataAssignmentModel
from .indikator_program import IndikatorProgramModel
from .indikator_terhubung import IndikatorTerhubungModel
from .indikator_dataset import IndikatorDatasetModel
from .login_attempt import LoginAttemptModel
from .indikator_datadasar import IndikatorDatadasarModel
from .dashboard_role import DashboardRoleModel
from .dashboard_role_permission import DashboardRolePermissionModel
from .dashboard_app import DashboardAppModel
from .dashboard_app_service import DashboardAppServiceModel
from .mapset_webgis import MapsetWebgisModel
from .changelog import ChangelogModel

__all__ = [
    "BaseModel", "AppModel", "AppServiceModel", "SektoralModel", "RegionalModel",
    "RegionalLevelModel", "SkpdModel", "SkpdSubModel", "SkpdUnitModel",
    "DatasetClassModel", "DatasetTypeModel", "DatasetTagModel", "DatasetModel",
    "MetadataTypeModel", "MetadataModel", "LicenseModel", "NotificationModel",
    "ReviewModel", "HistoryModel", "SearchModel", "MapsetModel", "MapsetSourceModel",
    "MapsetTypeModel", "MapsetKugiModel", "MapsetKugiSubModel", "UserModel", "UserPermissionModel", "UploadModel", "VisualizationModel",
    "VisualizationDatasetModel", "VisualizationAccessModel", "MetadataVisualizationModel", "UserAppRoleModel", "RoleModel",
    "IndikatorModel", "IndikatorProgressModel",
    "HistoryLoginModel", "HistoryDraftModel", "HistoryRequestModel", "RequestModel", "DatasetViewModel","DatasetViewAllModel",
    "IndikatorCategoryModel", "IndikatorClassModel", "IndikatorHistoryModel", "SatuanModel",
    "InfographicModel", "InfographicDetailModel", "HighlightModel", "HighlightViewModel", "HistoryDownloadModel",
    "FeedbackModel", "FeedbackMasalahModel", "MapsetMetadataModel", "MapsetMetadataTypeModel",
    "RequestPublicModel", "HistoryRequestPublicModel", "RequestPrivateModel", "HistoryRequestPrivateModel",
    "FeedbackPrivateModel", "FeedbackMapModel", "FeedbackMapMasalahModel", "AgreementModel",
    "StrukturOrganisasiModel", "DatasetPermissionModel", "VisualizationPermissionModel", "ArticleTopicModel",
    "ArticleModel", "JabatanModel", "UserViewModel", "MapsetProgramModel", "BKDJabatanExtract", "MapsetLawModel", "MapsetAwardModel", "MapsetStoryModel",
    "BidangUrusanModel", "MapsetThematicModel", "MapsetThematicSourceModel", "MapsetThematicMetadataModel", "DatasetQualityResultModel", "DatasetQualityResultHistoryModel",
    "NpsModel", "CsatModel", "MapsetAreaCoverageModel", "HistoryDraftViewModel", "HistoryDraftAllViewModel","BidangUrusanSkpdModel", "BidangUrusanIndikatorModel", "BidangUrusanIndikatorSkpdModel", "EwalidataDataModel", "MapsetMetadataKugiModel",
    "BidangUrusanViewModel", "BidangUrusanIndikatorViewModel", "HistoryEwalidataAssignmentModel",
    "IndikatorProgramModel", "IndikatorTerhubungModel", "IndikatorDatasetModel", "LoginAttemptModel", "IndikatorDatadasarModel",
    "DashboardRoleModel", "DashboardRolePermissionModel", "DashboardAppModel", "DashboardAppServiceModel", "MapsetWebgisModel", "ChangelogModel"
]
