''' Doc: model history draft '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryDraftModel(BaseModel, db.Model):
    """Model for the history_draft table"""
    __tablename__ = 'history_draft'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(50))
    type_id = db.Column(db.Integer)
    category = db.Column(db.String(50))
    notes = db.Column(db.Text)
    user_id = db.Column(db.Integer)
    datetime = db.Column(db.DateTime(True), default=db.func.now())

def __init__(self, _type, type_id, category, notes, user_id):
    self.type = _type
    self.type_id = type_id
    self.category = category
    self.notes = notes
    self.user_id = user_id
