''' Doc: model history '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryModel(BaseModel, db.Model):
    """Model for the history table"""
    __tablename__ = 'history'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(255))
    type_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    category = db.Column(db.String(255))
    datetime = db.Column(db.DateTime(True), default=db.func.now())

def __init__(self, _type, type_id, user_id, category, datetime):
    self.type = _type
    self.type_id = type_id
    self.user_id = user_id
    self.category = category
    self.datetime = datetime
