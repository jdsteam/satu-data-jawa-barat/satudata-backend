''' Doc: model role_permission '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db


class DashboardRolePermissionModel(BaseModel, db.Model):
    """Model for the role_permission table"""
    __table_args__ = {'schema': 'dashboard'}
    __tablename__ = 'role_permission'

    id = db.Column(db.Integer, primary_key=True)
    role_id = db.Column(db.Integer)
    app_id = db.Column(db.Integer)
    app_service_id = db.Column(db.Integer)
    enable = db.Column(db.Boolean, default=False)
    notes = db.Column(db.Text)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)


def __init__(self, role_id, app_id, app_service_id):
    self.role_id = role_id
    self.app_id = app_id
    self.app_service_id = app_service_id
