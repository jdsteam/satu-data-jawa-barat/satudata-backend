''' Doc: model reqview '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class ReviewModel(BaseModel, db.Model):
    """Model for the review table"""
    __tablename__ = 'review'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(255))
    type_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    rating = db.Column(db.Integer)
    review = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, _type, type_id, user_id, rating, review):
    self.type = _type
    self.type_id = type_id
    self.user_id = user_id
    self.rating = rating
    self.review = review
