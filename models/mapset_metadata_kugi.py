''' Doc: model metadata type '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel


class MapsetMetadataKugiModel(BaseModel, db.Model):
    '''Model for the mapset_metadata_kugi table'''
    __tablename__ = 'mapset_metadata_kugi'

    id = db.Column(db.Integer, primary_key=True)
    ptMemberName = db.Column(db.String(255))
    ptDefinition = db.Column(db.String(255))


def __init__(self, ptMemberName, ptDefinition):
    self.ptMemberName = ptMemberName
    self.ptDefinition = ptDefinition
