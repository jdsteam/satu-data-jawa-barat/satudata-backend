''' Doc: model metadata type '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel


class MapsetMetadataTypeModel(BaseModel, db.Model):
    '''Model for the mapset_metadata_type table'''
    __tablename__ = 'mapset_metadata_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))


def __init__(self, name, notes):
    self.name = name
    self.notes = notes
