from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db


class RegionalIntegrationModel(BaseModel, db.Model):
    __tablename__ = "regional_integration_list"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    kode_wilayah = db.Column(db.String(255))
    nama_wilayah = db.Column(db.String(255))
    endpoint = db.Column(db.String(255))
