''' Doc: model indikator history '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorProgramModel(BaseModel, db.Model):
    """Model for the indikator_program table"""
    __tablename__ = 'indikator_program'

    id = db.Column(db.Integer, primary_key=True)
    indikator_id = db.Column(db.Integer)
    kode_program = db.Column(db.Text)
    id_program = db.Column(db.Integer)
    notes = db.Column(db.Text)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, indikator_id, kode_program):
    self.indikator_id = indikator_id
    self.kode_program = kode_program
