''' Doc: model indikator history '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorTerhubungModel(BaseModel, db.Model):
    """Model for the indikator_terhubung table"""
    __tablename__ = 'indikator_terhubung'

    id = db.Column(db.Integer, primary_key=True)
    indikator_id = db.Column(db.Integer)
    indikator_terhubung_id = db.Column(db.Integer)
    notes = db.Column(db.Text)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, indikator_id, indikator_terhubung_id):
    self.indikator_id = indikator_id
    self.indikator_terhubung_id = indikator_terhubung_id
