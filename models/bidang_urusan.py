''' Doc: model bidang_urusan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy.orm import relationship

class BidangUrusanModel(BaseModel, db.Model):
    """Model for the bidang_urusan table"""
    __tablename__ = 'bidang_urusan'

    id = db.Column(db.Integer, primary_key=True)
    kode_bidang_urusan = db.Column(db.Integer)
    nama_bidang_urusan = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

    skpd = relationship('BidangUrusanSkpdModel', back_populates='bidang_urusan')

def __init__(self, kode_bidang_urusan, nama_bidang_urusan ):
    self.kode_bidang_urusan = kode_bidang_urusan
    self.nama_bidang_urusan = nama_bidang_urusan
