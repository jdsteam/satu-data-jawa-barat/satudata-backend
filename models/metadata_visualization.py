''' Doc: model metadata visualization '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class MetadataVisualizationModel(BaseModel, db.Model):
    """Model for the metadata_visualization table"""
    __tablename__ = 'metadata_visualization'

    id = db.Column(db.Integer, primary_key=True)
    metadata_type_id = db.Column(db.Integer, db.ForeignKey("metadata_type.id"))
    visualization_id = db.Column(db.Integer, db.ForeignKey("visualization.id"))
    key = db.Column(db.String(255))
    value = db.Column(db.String(255))
    description = db.Column(db.String(255))

def __init__(self, metadata_type_id, visualization_id, key, value):
    self.metadata_type_id = metadata_type_id
    self.visualization_id = visualization_id
    self.key = key
    self.value = value
