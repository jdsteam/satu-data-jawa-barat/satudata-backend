''' Doc: model upload '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class UploadModel(BaseModel, db.Model):
    """Model for the upload table"""
    __tablename__ = 'upload'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    type = db.Column(db.String(255))
    ext = db.Column(db.String(255))
    size = db.Column(db.Integer)
    hash = db.Column(db.String(255))
    url = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, name, _type):
    self.name = name
    self.type = _type
