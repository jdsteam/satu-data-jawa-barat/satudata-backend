''' Doc: model app service '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class AppServiceModel(BaseModel, db.Model):
    """Model for the app_service table"""
    __tablename__ = 'app_service'

    id = db.Column(db.Integer, primary_key=True)
    app_id = db.Column(db.Integer, db.ForeignKey("app.id"))
    controller = db.Column(db.String(255))
    action = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    is_backend = db.Column(db.Boolean, default=True)
    dataset_class_id = db.Column(db.Integer, db.ForeignKey("dataset_class.id"))


def __init__(self, app_id, controller, action):
    self.app_id = app_id
    self.controller = controller
    self.action = action
