''' Doc: model history login '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class LoginAttemptModel(BaseModel, db.Model):
    """Model for the login_attempt table"""
    __tablename__ = 'login_attempt'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    ip = db.Column(db.String(50))
    username = db.Column(db.String(50))
    browser = db.Column(db.String(50))
    success = db.Column(db.Boolean, default=False)

def __init__(self, datetime, ip, username, browser, success):
    self.datetime = datetime
    self.ip = ip
    self.username = username
    self.browser = browser
    self.success = success
