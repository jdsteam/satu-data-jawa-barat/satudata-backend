''' Doc: model visualization '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class VisualizationModel(BaseModel, db.Model):
    """Model for the visualization table"""
    __tablename__ = 'visualization'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(150))
    name = db.Column(db.String(150))
    year = db.Column(db.Integer)
    description = db.Column(db.String(150))
    keywords = db.Column(db.String)
    image = db.Column(db.String(150))
    url = db.Column(db.String(150))
    regional_id = db.Column(db.Integer)
    kode_skpd = db.Column(db.Integer)
    kode_skpdsub = db.Column(db.String(150))
    kode_skpdunit = db.Column(db.String(150))
    sektoral_id = db.Column(db.Integer)
    dataset_class_id = db.Column(db.Integer)
    license_id = db.Column(db.Integer)
    owner = db.Column(db.String(100))
    owner_email = db.Column(db.String(100))
    maintainer = db.Column(db.String(100))
    maintainer_email = db.Column(db.String(100))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Boolean, default=False)
    count_rating = db.Column(db.Integer, default=0)
    # start count opendata
    count_view = db.Column(db.Integer, default=0)
    count_share_fb = db.Column(db.Integer, default=0)
    count_share_tw = db.Column(db.Integer, default=0)
    count_share_wa = db.Column(db.Integer, default=0)
    count_share_link = db.Column(db.Integer, default=0)
    count_download_img = db.Column(db.Integer, default=0)
    count_download_pdf = db.Column(db.Integer, default=0)
    # end count opendata
    # start count satudata
    count_view_private = db.Column(db.Integer, default=0)
    count_download_img_private = db.Column(db.Integer, default=0)
    count_download_pdf_private = db.Column(db.Integer, default=0)
    # end count satudata
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)


def __init__(
        self, title, description, image, url, sektoral_id,
        kode_skpd, dataset_class_id, permission, dataset_id, year, owner
    ):
    self.title = title
    self.description = description
    self.image = image
    self.url = url
    self.sektoral_id = sektoral_id
    self.kode_skpd = kode_skpd
    self.dataset_class_id = dataset_class_id
    self.permission = permission
    self.dataset_id = dataset_id
    self.year = year
    self.owner = owner
