''' Doc: model app_service '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db


class DashboardAppServiceModel(BaseModel, db.Model):
    """Model for the app_service table"""
    __table_args__ = {'schema': 'dashboard'}
    __tablename__ = 'app_service'

    id = db.Column(db.Integer, primary_key=True)
    app_id = db.Column(db.Integer)
    controller = db.Column(db.String(255))
    action = db.Column(db.String(255))
    notes = db.Column(db.Text)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(255))
    embed_id = db.Column(db.Text)
    embed_url = db.Column(db.Text)
    dashboard_type = db.Column(db.Text)


def __init__(self, app_id, controller, name):
    self.app_id = app_id
    self.controller = controller
    self.name = name
