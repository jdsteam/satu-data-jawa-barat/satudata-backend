''' Doc: model dataset class '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class DatasetClassModel(BaseModel, db.Model):
    """Model for the dataset_class table"""
    __tablename__ = 'dataset_class'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, name, notes):
    self.name = name
    self.notes = notes
