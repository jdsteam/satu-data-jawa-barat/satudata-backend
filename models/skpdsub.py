''' Doc: model skpdsub '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class SkpdSubModel(BaseModel, db.Model):
    """Model for the skpdsub table"""
    __tablename__ = 'skpdsub'

    id = db.Column(db.Integer, primary_key=True)
    regional_id = db.Column(db.Integer, db.ForeignKey("regional.id"))
    skpd_id = db.Column(db.Integer, db.ForeignKey("skpd.id"))
    kode_skpd = db.Column(db.Integer, db.ForeignKey("skpd.kode_skpd"))
    kode_skpdsub = db.Column(db.String(255))
    nama_skpdsub = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, skpd_id, kode_skpdsub, nama_skpdsub):
    self.skpd_id = skpd_id
    self.kode_skpdsub = kode_skpdsub
    self.nama_skpdsub = nama_skpdsub
