''' Doc: model infographic '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class InfographicModel(BaseModel, db.Model):
    """Model for the infographic table"""
    __tablename__ = 'infographic'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(150))
    name = db.Column(db.String(150))
    description = db.Column(db.String(150))
    keywords = db.Column(db.String)
    image = db.Column(db.String(150))
    regional_id = db.Column(db.Integer)
    sektoral_id = db.Column(db.Integer)
    dataset_class_id = db.Column(db.Integer)
    license_id = db.Column(db.Integer)
    source = db.Column(db.String(150))
    owner = db.Column(db.String(100))
    owner_email = db.Column(db.String(100))
    maintainer = db.Column(db.String(100))
    maintainer_email = db.Column(db.String(100))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Boolean, default=False)
    count_view = db.Column(db.Integer, default=0)
    count_rating = db.Column(db.Integer, default=0)
    count_share_fb = db.Column(db.Integer, default=0)
    count_share_tw = db.Column(db.Integer, default=0)
    count_share_wa = db.Column(db.Integer, default=0)
    count_share_link = db.Column(db.Integer, default=0)
    count_download_img = db.Column(db.Integer, default=0)
    count_download_pdf = db.Column(db.Integer, default=0)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)


def __init__(
        self, name, title, description, image,
        regional_id, kode_skpd, sektoral_id, dataset_class_id, owner
    ):
    self.name = name
    self.title = title
    self.description = description
    self.image = image
    self.regional_id = regional_id
    self.kode_skpd = kode_skpd
    self.sektoral_id = sektoral_id
    self.dataset_class_id = dataset_class_id
    self.owner = owner
