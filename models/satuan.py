''' Doc: model satuan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class SatuanModel(BaseModel, db.Model):
    """Model for the satuan table"""
    __tablename__ = 'satuan'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))

def __init__(self, name):
    self.name = name
