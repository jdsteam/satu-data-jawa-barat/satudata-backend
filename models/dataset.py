''' Doc: model dataset  '''
from unicodedata import category
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db


class DatasetModel(BaseModel, db.Model):
    """Model for the dataset table"""
    __tablename__ = 'dataset'

    id = db.Column(db.Integer, primary_key=True)
    dataset_type_id = db.Column(db.Integer, db.ForeignKey("dataset_type.id"))
    dataset_class_id = db.Column(db.Integer, db.ForeignKey("dataset_class.id"))
    app_id = db.Column(db.Integer, db.ForeignKey("app.id"))
    app_service_id = db.Column(db.Integer, db.ForeignKey("app_service.id"))
    sektoral_id = db.Column(db.Integer, db.ForeignKey("sektoral.id"))
    regional_id = db.Column(db.Integer, db.ForeignKey("regional.id"))
    license_id = db.Column(db.Integer, db.ForeignKey("license.id"))
    kode_skpd = db.Column(db.String(255))
    kode_skpdsub = db.Column(db.String(255))
    kode_skpdunit = db.Column(db.String(255))
    name = db.Column(db.String(255))
    title = db.Column(db.String(255))
    year = db.Column(db.Integer)
    image = db.Column(db.String(255))
    description = db.Column(db.String(255))
    owner = db.Column(db.String(255))
    owner_email = db.Column(db.String(255))
    maintainer = db.Column(db.String(255))
    maintainer_email = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    count_column = db.Column(db.Integer, default=0)
    count_row = db.Column(db.Integer, default=0)
    count_rating = db.Column(db.String(255))
    # start count opendata
    count_view = db.Column(db.Integer, default=0)
    count_access = db.Column(db.Integer, default=0)
    count_share_fb = db.Column(db.Integer, default=0)
    count_share_tw = db.Column(db.Integer, default=0)
    count_share_wa = db.Column(db.Integer, default=0)
    count_share_link = db.Column(db.Integer, default=0)
    count_download_xls = db.Column(db.Integer, default=0)
    count_download_csv = db.Column(db.Integer, default=0)
    count_download_api = db.Column(db.Integer, default=0)
    count_download_xls_filter = db.Column(db.Integer, default=0)
    count_download_csv_filter = db.Column(db.Integer, default=0)
    count_download_copy = db.Column(db.Integer, default=0)
    # end count opendata
    # start count satudata
    count_view_private = db.Column(db.Integer, default=0)
    count_access_private = db.Column(db.Integer, default=0)
    count_download_xls_private = db.Column(db.Integer, default=0)
    count_download_csv_private = db.Column(db.Integer, default=0)
    count_download_api_private = db.Column(db.Integer, default=0)
    # end count satudata
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Integer, default=1)
    is_permanent = db.Column(db.Boolean, default=False)
    is_realtime = db.Column(db.Boolean, default=False)
    is_discontinue = db.Column(db.Integer, default=0)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    schema = db.Column(db.String(255))
    table = db.Column(db.String(255))
    json = db.Column(db.String(255))
    category = db.Column(db.String(255))
    period = db.Column(db.DateTime(True), default=db.func.now())
    kode_bidang_urusan = db.Column(db.String(255))
    kode_indikator = db.Column(db.String(255))


def __init__(
    self, dataset_type_id, dataset_class_id, app_id, app_service_id,
    sektoral_id, regional_id, license_id, kode_skpd, name, title
):
    self.dataset_type_id = dataset_type_id
    self.dataset_class_id = dataset_class_id
    self.app_id = app_id
    self.app_service_id = app_service_id
    self.sektoral_id = sektoral_id
    self.regional_id = regional_id
    self.license_id = license_id
    self.kode_skpd = kode_skpd
    self.name = name
    self.title = title
