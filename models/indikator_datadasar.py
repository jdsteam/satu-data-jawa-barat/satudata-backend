''' Doc: model indikator datadasar '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorDatadasarModel(BaseModel, db.Model):
    """Model for the indikator_datadasar table"""
    __tablename__ = 'indikator_datadasar'

    id = db.Column(db.Integer, primary_key=True)
    indikator_id = db.Column(db.Integer)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, name, indikator_id):
    self.name = name
    self.indikator_id = indikator_id
