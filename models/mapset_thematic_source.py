''' Doc: model mapset thematic '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetThematicSourceModel(BaseModel, db.Model):
    '''Model for the mapset_thematic_source table'''
    __tablename__ = 'mapset_thematic_source'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    datatype = db.Column(db.String(255))
    kode_skpd = db.Column(db.String(255))
    year = db.Column(db.String(4))
    mapset_thematic_id = db.Column(db.Integer)
    external_org_name = db.Column(db.String(255))
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
