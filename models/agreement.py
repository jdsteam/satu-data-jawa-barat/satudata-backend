''' Doc: model agreement '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class AgreementModel(BaseModel, db.Model):
    """Model for the agreement table"""
    __tablename__ = 'agreement'

    id = db.Column(db.Integer, primary_key=True)
    version = db.Column(db.String(255))
    agreement_text = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)


def __init__(self, version, agreement_text):
    self.version = version
    self.agreement_text = agreement_text
