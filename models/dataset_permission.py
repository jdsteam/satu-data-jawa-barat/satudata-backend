''' Doc: model dataset  '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class DatasetPermissionModel(BaseModel, db.Model):
    """Model for the dataset_permission table"""
    __tablename__ = 'dataset_permission'

    id = db.Column(db.Integer, primary_key=True)
    dataset_id = db.Column(db.Integer)
    jabatan_id = db.Column(db.String(255))
    enable = db.Column(db.Boolean, default=True)
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, dataset_id, jabatan_id):
    self.dataset_id = dataset_id
    self.jabatan_id = jabatan_id
