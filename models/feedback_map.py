''' Doc: model feedback map '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class FeedbackMapModel(BaseModel, db.Model):
    """Model for the feedback_map table"""
    __tablename__ = 'feedback_map'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    score = db.Column(db.Integer)
    tujuan = db.Column(db.String(250))
    sektor = db.Column(db.String(250))
    tujuan_tercapai = db.Column(db.Boolean, default=False)
    tujuan_mudah_ditemukan = db.Column(db.Boolean, default=False)
    saran = db.Column(db.Text)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, score, tujuan, sektor, tujuan_tercapai, tujuan_mudah_ditemukan, saran, is_active, is_deleted):
    self.score = score
    self.tujuan = tujuan
    self.sektor = sektor
    self.tujuan_tercapai = tujuan_tercapai
    self.tujuan_mudah_ditemukan = tujuan_mudah_ditemukan
    self.saran = saran
    self.is_active = is_active
    self.is_deleted = is_deleted
