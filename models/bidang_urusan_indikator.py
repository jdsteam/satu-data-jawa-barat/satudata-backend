''' Doc: model bidang_urusan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class BidangUrusanIndikatorModel(BaseModel, db.Model):
    """Model for the bidang_urusan_indikator table"""
    __tablename__ = 'bidang_urusan_indikator'

    id = db.Column(db.Integer, primary_key=True)
    kode_bidang_urusan = db.Column(db.Integer, db.ForeignKey('bidang_urusan.kode_bidang_urusan'))
    kode_indikator = db.Column(db.String(255))
    nama_indikator = db.Column(db.Text)
    satuan = db.Column(db.String(255))
    definisi_operasional = db.Column(db.Text)
    uraian_indikator = db.Column(db.Text)
    status = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, kode_bidang_urusan, kode_indikator,nama_indikator):
    self.kode_bidang_urusan = kode_bidang_urusan
    self.kode_indikator = kode_indikator
    self.nama_indikator = nama_indikator
