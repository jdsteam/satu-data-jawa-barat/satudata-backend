''' Doc: model dataset tag '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class DatasetTagModel(BaseModel, db.Model):
    """Model for the dataset_tag table"""
    __tablename__ = 'dataset_tag'

    id = db.Column(db.Integer, primary_key=True)
    dataset_id = db.Column(db.Integer)
    tag = db.Column(db.String(255))

def __init__(self, dataset_id, tag):
    self.dataset_id = dataset_id
    self.tag = tag
