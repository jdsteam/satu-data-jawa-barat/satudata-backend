''' Doc: model history_ewalidata_assignment '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy.orm import relationship

class HistoryEwalidataAssignmentModel(BaseModel, db.Model):
    """Model for the history_ewalidata_assignment table"""
    __tablename__ = 'history_ewalidata_assignment'

    id = db.Column(db.Integer, primary_key=True)
    bidang_urusan_indikator_skpd_id = db.Column(db.Integer)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    status_assignment = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)


def __init__(self, bidang_urusan_indikator_skpd_id, status_assignment):
    self.bidang_urusan_indikator_skpd_id = bidang_urusan_indikator_skpd_id
    self.status_assignment = status_assignment
