''' Doc: model sektoral '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class SektoralModel(BaseModel, db.Model):
    """Model for the sektoral table"""
    __tablename__ = 'sektoral'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    picture = db.Column(db.String(255))
    picture_thumbnail = db.Column(db.String(255))
    picture_thumbnail_1 = db.Column(db.String(255))
    picture_thumbnail_2 = db.Column(db.String(255))
    picture_thumbnail_3 = db.Column(db.String(255))
    picture_thumbnail_4 = db.Column(db.String(255))
    picture_thumbnail_5 = db.Column(db.String(255))
    picture_banner_1 = db.Column(db.String(255))
    picture_banner_2 = db.Column(db.String(255))
    picture_banner_3 = db.Column(db.String(255))
    picture_banner_4 = db.Column(db.String(255))
    picture_banner_5 = db.Column(db.String(255))
    picture_satupeta = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_deleted = db.Column(db.Boolean, default=False)
    is_opendata = db.Column(db.Boolean, default=False)
    is_satudata = db.Column(db.Boolean, default=False)
    is_satupeta = db.Column(db.Boolean, default=False)

def __init__(self, name):
    self.name = name
