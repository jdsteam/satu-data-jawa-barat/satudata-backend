''' Doc: model metadata '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class MetadataModel(BaseModel, db.Model):
    """Model for the metadata table"""
    __tablename__ = 'metadata'

    id = db.Column(db.Integer, primary_key=True)
    metadata_type_id = db.Column(db.Integer, db.ForeignKey("metadata_type.id"))
    dataset_id = db.Column(db.Integer, db.ForeignKey("dataset.id"))
    key = db.Column(db.String(255))
    value = db.Column(db.String(255))
    description = db.Column(db.String(255))

def __init__(self, metadata_type_id, dataset_id, key, value):
    self.metadata_type_id = metadata_type_id
    self.dataset_id = dataset_id
    self.key = key
    self.value = value
