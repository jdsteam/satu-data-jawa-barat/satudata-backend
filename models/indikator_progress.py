''' Doc: model indikator progress '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorProgressModel(BaseModel, db.Model):
    """Model for the indikator_progress table"""
    __tablename__ = 'indikator_progress'

    id = db.Column(db.Integer, primary_key=True)
    indikator_id = db.Column(db.Integer)
    year = db.Column(db.Integer)
    target = db.Column(db.String(255))
    capaian = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    index = db.Column(db.Integer)
    is_final = db.Column(db.Boolean, default=False)

def __init__(self, indikator_id, year, target, capaian, notes):
    self.indikator_id = indikator_id
    self.year = year
    self.target = target
    self.capaian = capaian
    self.notes = notes
