''' Doc: model history request private '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryRequestPrivateModel(BaseModel, db.Model):
    """Model for the history_request_private table"""
    __tablename__ = 'history_request_private'

    id = db.Column(db.Integer, primary_key=True)
    request_private_id = db.Column(db.Integer)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    status = db.Column(db.Integer)
    notes = db.Column(db.Text)
    cuid = db.Column(db.Integer)

def __init__(self, request_private_id, status):
    self.request_private_id = request_private_id
    self.status = status
