''' Doc: model history draft '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryDraftViewModel(BaseModel, db.Model):
    """Model for the history_draft table"""
    __tablename__ = 'history_draft_view'

    id = db.Column(db.String, primary_key=True)
    type = db.Column(db.String(50))
    type_id = db.Column(db.Integer)
    name = db.Column(db.String(50))
    kode_skpd = db.Column(db.String(50))
    nama_skpd = db.Column(db.String(50))
    nama_skpd_alias = db.Column(db.String(50))
    cuid = db.Column(db.Integer)
    username = db.Column(db.String(50))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Integer)
    is_discontinue = db.Column(db.Integer)

def __init__(self, _type, type_id):
    self.type = _type
    self.type_id = type_id
