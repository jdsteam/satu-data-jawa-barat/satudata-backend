''' Doc: model visualization dataset '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class VisualizationDatasetModel(BaseModel, db.Model):
    """Model for the visualization_dataset table"""
    __tablename__ = 'visualization_dataset'

    id = db.Column(db.Integer, primary_key=True)
    visualization_id = db.Column(db.Integer)
    dataset_id = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)


def __init__(self, visualization_id, dataset_id):
    self.visualization_id = visualization_id
    self.dataset_id = dataset_id
