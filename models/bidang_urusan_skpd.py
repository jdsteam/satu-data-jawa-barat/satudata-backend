''' Doc: model bidang_urusan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class BidangUrusanSkpdModel(BaseModel, db.Model):
    """Model for the bidang_urusan_skpd table"""
    __tablename__ = 'bidang_urusan_skpd'

    id = db.Column(db.Integer, primary_key=True)
    kode_bidang_urusan = db.Column(db.Integer, db.ForeignKey('bidang_urusan.kode_bidang_urusan'))
    kode_skpd = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

    bidang_urusan = db.relationship('BidangUrusanModel', back_populates='skpd')

def __init__(self, kode_bidang_urusan, kode_skpd):
    self.kode_bidang_urusan = kode_bidang_urusan
    self.kode_skpd = kode_skpd
