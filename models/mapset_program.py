''' Doc: model mapset_program '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class MapsetProgramModel(BaseModel, db.Model):
    """Model for the mapset_program table"""
    __tablename__ = 'mapset_program'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    picture = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, name):
    self.name = name
