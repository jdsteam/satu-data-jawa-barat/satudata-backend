''' Doc: model metadata type '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class MetadataTypeModel(BaseModel, db.Model):
    """Model for the metadata_type table"""
    __tablename__ = 'metadata_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, name, notes):
    self.name = name
    self.notes = notes
