''' Doc: model infographic detail '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class InfographicDetailModel(BaseModel, db.Model):
    """Model for the infographic_detail table"""
    __tablename__ = 'infographic_detail'

    id = db.Column(db.Integer, primary_key=True)
    infographic_id = db.Column(db.Integer)
    title = db.Column(db.String(150))
    name = db.Column(db.String(150))
    description = db.Column(db.String(150))
    image = db.Column(db.String(150))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)


def __init__(self, infographic_id, name, title, description, image):
    self.infographic_id = infographic_id
    self.name = name
    self.title = title
    self.description = description
    self.image = image
