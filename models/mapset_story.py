''' Doc: model mapset story '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetStoryModel(BaseModel, db.Model):
    '''Model for the mapset_story table'''
    __tablename__ = 'mapset_story'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    url = db.Column(db.Text)
    description = db.Column(db.String(255))
    image = db.Column(db.Text)
    count_view = db.Column(db.Integer, default=0)
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    sektoral_id = db.Column(db.Integer)
    
def __init__(self, id):
    self.id = id
