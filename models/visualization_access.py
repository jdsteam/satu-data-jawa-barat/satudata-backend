''' Doc: model visualization access '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class VisualizationAccessModel(BaseModel, db.Model):
    """Model for the visualization_access table"""
    __tablename__ = 'visualization_access'

    id = db.Column(db.Integer, primary_key=True)
    visualization_id = db.Column(db.Integer, db.ForeignKey("visualization.id"))
    user_id = db.Column(db.Integer)
    enable = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, visualization_id, user_id, enable):
    self.visualization_id = visualization_id
    self.user_id = user_id
    self.enable = enable
