''' Doc: model dataset  '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db


class DatasetQualityResultModel(BaseModel, db.Model):
    """Model for the dataset table"""
    __tablename__ = 'dataset_quality_results'

    id = db.Column(db.Integer, primary_key=True)
    unit = db.Column(db.Text)
    unit_column = db.Column(db.Text)
    value_column = db.Column(db.Text)

    name = db.Column(db.Text)
    schema = db.Column(db.Text)
    table = db.Column(db.Text)
    tag = db.Column(db.Text)
    description = db.Column(db.Text)
    metadata_ = db.Column("metadata", db.Text)
    category = db.Column(db.Text)
    code_area_level = db.Column(db.Text)
    column_time_series = db.Column(db.Text)
    error = db.Column(db.Text)
    is_config_complete = db.Column(db.Text)
    time_series_type = db.Column(db.Text)

    # final
    final_result = db.Column(db.Float)
    # conformity
    conformity_result = db.Column(db.Float)
    conformity_code_area_total_rows = db.Column(db.Integer)
    conformity_code_area_total_columns = db.Column(db.Integer)
    conformity_code_area_total_cells = db.Column(db.Integer)
    conformity_code_area_total_valid = db.Column(db.Integer)
    conformity_code_area_total_not_valid = db.Column(db.Integer)
    conformity_code_area_warning = db.Column(db.Text)
    conformity_code_area_quality_result = db.Column(db.Float)
    conformity_explain_columns_total_rows = db.Column(db.Integer)
    conformity_explain_columns_total_columns = db.Column(db.Integer)
    conformity_explain_columns_total_cells = db.Column(db.Integer)
    conformity_explain_columns_total_valid = db.Column(db.Integer)
    conformity_explain_columns_total_not_valid = db.Column(db.Integer)
    conformity_explain_columns_warning = db.Column(db.Text)
    conformity_explain_columns_quality_result = db.Column(db.Float)
    conformity_measurement_total_rows = db.Column(db.Integer)
    conformity_measurement_total_columns = db.Column(db.Integer)
    conformity_measurement_total_cells = db.Column(db.Integer)
    conformity_measurement_total_valid = db.Column(db.Integer)
    conformity_measurement_total_not_valid = db.Column(db.Integer)
    conformity_measurement_warning = db.Column(db.Text)
    conformity_measurement_quality_result = db.Column(db.Float)
    conformity_serving_rate_total_rows = db.Column(db.Integer)
    conformity_serving_rate_total_columns = db.Column(db.Integer)
    conformity_serving_rate_total_cells = db.Column(db.Integer)
    conformity_serving_rate_total_valid = db.Column(db.Integer)
    conformity_serving_rate_total_not_valid = db.Column(db.Integer)
    conformity_serving_rate_warning = db.Column(db.Text)
    conformity_serving_rate_quality_result = db.Column(db.Float)
    conformity_scope_total_rows = db.Column(db.Integer)
    conformity_scope_total_columns = db.Column(db.Integer)
    conformity_scope_total_cells = db.Column(db.Integer)
    conformity_scope_total_valid = db.Column(db.Integer)
    conformity_scope_total_not_valid = db.Column(db.Integer)
    conformity_scope_warning = db.Column(db.Text)
    conformity_scope_quality_result = db.Column(db.Float)
    # uniqueness
    uniqueness_result = db.Column(db.Float)
    uniqueness_duplicated_total_rows = db.Column(db.Integer)
    uniqueness_duplicated_total_columns = db.Column(db.Integer)
    uniqueness_duplicated_total_cells = db.Column(db.Integer)
    uniqueness_duplicated_total_valid = db.Column(db.Integer)
    uniqueness_duplicated_total_not_valid = db.Column(db.Integer)
    uniqueness_duplicated_warning = db.Column(db.Text)
    uniqueness_duplicated_quality_result = db.Column(db.Float)
    # completeness
    completeness_result = db.Column(db.Float)
    completeness_filled_total_rows = db.Column(db.Integer)
    completeness_filled_total_columns = db.Column(db.Integer)
    completeness_filled_total_cells = db.Column(db.Integer)
    completeness_filled_total_valid = db.Column(db.Integer)
    completeness_filled_total_not_valid = db.Column(db.Integer)
    completeness_filled_warning = db.Column(db.Text)
    completeness_filled_quality_result = db.Column(db.Float)
    # consistency
    consistency_result = db.Column(db.Float)
    consistency_unit_total_rows = db.Column(db.Integer)
    consistency_unit_total_columns = db.Column(db.Integer)
    consistency_unit_total_cells = db.Column(db.Integer)
    consistency_unit_total_valid = db.Column(db.Integer)
    consistency_unit_total_not_valid = db.Column(db.Integer)
    consistency_unit_warning = db.Column(db.Text)
    consistency_unit_quality_result = db.Column(db.Float)
    consistency_listing_province_total_rows = db.Column(db.Integer)
    consistency_listing_province_total_columns = db.Column(db.Integer)
    consistency_listing_province_total_cells = db.Column(db.Integer)
    consistency_listing_province_total_valid = db.Column(db.Integer)
    consistency_listing_province_total_not_valid = db.Column(db.Integer)
    consistency_listing_province_warning = db.Column(db.Text)
    consistency_listing_province_quality_result = db.Column(db.Float)
    consistency_time_series_total_rows = db.Column(db.Integer)
    consistency_time_series_total_columns = db.Column(db.Integer)
    consistency_time_series_total_cells = db.Column(db.Integer)
    consistency_time_series_total_valid = db.Column(db.Integer)
    consistency_time_series_total_not_valid = db.Column(db.Integer)
    consistency_time_series_warning = db.Column(db.Text)
    consistency_time_series_quality_result = db.Column(db.Float)
    # timeliness
    timeliness_result = db.Column(db.Float)
    timeliness_updated_total_rows = db.Column(db.Integer)
    timeliness_updated_total_columns = db.Column(db.Integer)
    timeliness_updated_total_cells = db.Column(db.Integer)
    timeliness_updated_total_valid = db.Column(db.Integer)
    timeliness_updated_total_not_valid = db.Column(db.Integer)
    timeliness_updated_warning = db.Column(db.Text)
    timeliness_updated_quality_result = db.Column(db.Float)

def __init__(self, final_result):
    self.id = id
    self.final_result = final_result
