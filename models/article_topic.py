''' Doc: model visualization access '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class ArticleTopicModel(BaseModel, db.Model):
    """Model for the article_topic table"""
    __tablename__ = 'article_topic'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    title = db.Column(db.String(255))
    image = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, name, title, image):
    self.name = name
    self.title = title
    self.image = image
