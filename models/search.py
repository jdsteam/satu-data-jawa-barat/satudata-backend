''' Doc: model search '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class SearchModel(BaseModel, db.Model):
    """Model for the search table"""
    __tablename__ = 'search'

    id = db.Column(db.Integer, primary_key=True)
    search = db.Column(db.String(255))
    user_id = db.Column(db.Integer)
    datetime = db.Column(db.DateTime(True), default=db.func.now())

def __init__(self, search, user_id, datetime):
    self.search = search
    self.user_id = user_id
    self.datetime = datetime
