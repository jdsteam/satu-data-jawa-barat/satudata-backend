''' Doc: model skpd '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class SkpdModel(BaseModel, db.Model):
    """Model for the skpd table"""
    __tablename__ = 'skpd'

    id = db.Column(db.Integer, primary_key=True)
    regional_id = db.Column(db.Integer, db.ForeignKey("regional.id"), default='1')
    kode_skpd = db.Column(db.String(255))
    nama_skpd = db.Column(db.String(255))
    nama_skpd_alias = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    description = db.Column(db.String(255))
    address = db.Column(db.String(255))
    phone = db.Column(db.String(255))
    email = db.Column(db.String(255))
    media_website = db.Column(db.String(255))
    media_facebook = db.Column(db.String(255))
    media_twitter = db.Column(db.String(255))
    media_instagram = db.Column(db.String(255))
    media_youtube = db.Column(db.String(255))
    count_dataset = db.Column(db.Integer, default=0)
    count_visualization = db.Column(db.Integer, default=0)
    count_indikator = db.Column(db.Integer, default=0)
    count_infographic = db.Column(db.Integer, default=0)
    count_dataset_public = db.Column(db.Integer, default=0)
    count_visualization_public = db.Column(db.Integer, default=0)
    count_indikator_public = db.Column(db.Integer, default=0)
    count_infographic_public = db.Column(db.Integer, default=0)
    logo = db.Column(db.String(255), default='static/upload/default-image.png')
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    is_external = db.Column(db.Boolean, default=False)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_deleted = db.Column(db.Boolean, default=False)
    title = db.Column(db.String(255))
    is_opendata = db.Column(db.Boolean, default=False)
    is_satudata = db.Column(db.Boolean, default=False)
    is_satupeta = db.Column(db.Boolean, default=False)

def __init__(self, kode_skpd, nama_skpd):
    self.kode_skpd = kode_skpd
    self.nama_skpd = nama_skpd
