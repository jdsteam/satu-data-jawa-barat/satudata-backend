''' Doc: model role '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class RoleModel(BaseModel, db.Model):
    """Model for the role table"""
    __tablename__ = 'role'

    id = db.Column(db.Integer, primary_key=True)
    app_id = db.Column(db.Integer, db.ForeignKey("app.id"))
    name = db.Column(db.String(255))
    title = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, app_id, name):
    self.app_id = app_id
    self.name = name
