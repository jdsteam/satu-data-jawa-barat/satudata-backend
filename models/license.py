''' Doc: model license '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class LicenseModel(BaseModel, db.Model):
    """Model for the license table"""
    __tablename__ = 'license'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, name):
    self.name = name
