''' Doc: model mapset law '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetLawModel(BaseModel, db.Model):
    '''Model for the mapset_law table'''
    __tablename__ = 'mapset_law'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    year = db.Column(db.String(4))
    url = db.Column(db.String(255))
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, name):
    self.name = name
