''' Doc: model skpdunit '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class SkpdUnitModel(BaseModel, db.Model):
    """Model for the skpdunit table"""
    __tablename__ = 'skpdunit'

    id = db.Column(db.Integer, primary_key=True)
    regional_id = db.Column(db.Integer, db.ForeignKey("regional.id"))
    skpd_id = db.Column(db.Integer, db.ForeignKey("skpd.id"))
    skpdsub_id = db.Column(db.Integer, db.ForeignKey("skpdsub.id"))
    kode_skpd = db.Column(db.Integer, db.ForeignKey("skpd.kode_skpd"))
    kode_skpdsub = db.Column(db.Integer, db.ForeignKey("skpdsub.kode_skpdsub"))
    kode_skpdunit = db.Column(db.String(255))
    nama_skpdunit = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, skpd_id, skpdsub_id, kode_skpdunit, nama_skpdunit):
    self.skpd_id = skpd_id
    self.skpdsub_id = skpdsub_id
    self.kode_skpdunit = kode_skpdunit
    self.nama_skpdunit = nama_skpdunit
