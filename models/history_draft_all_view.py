''' Doc: model history draft '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryDraftAllViewModel(BaseModel, db.Model):
    """Model for the history_draft table"""
    __tablename__ = 'history_draft_all_view'

    id = db.Column(db.String, primary_key=True)
    type = db.Column(db.String(50))
    type_id = db.Column(db.Integer)
    category = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    name = db.Column(db.String(50))
    kode_skpd = db.Column(db.String(50))
    nama_skpd = db.Column(db.String(50))
    nama_skpd_alias = db.Column(db.String(50))
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    is_validate = db.Column(db.Integer)
    is_discontinue = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    user_username = db.Column(db.String(50))
    user_name = db.Column(db.String(50))

def __init__(self, _type, type_id):
    self.type = _type
    self.type_id = type_id
