''' Doc: model struktur_organisasi '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class StrukturOrganisasiModel(BaseModel, db.Model):
    """Model for the struktur_organisasi table"""
    __tablename__ = 'struktur_organisasi'

    id = db.Column(db.Integer, primary_key=True)
    satuan_kerja_id = db.Column(db.String(255))
    satuan_kerja_nama = db.Column(db.String(255))
    lv1_unit_kerja_id = db.Column(db.String(255))
    lv1_unit_kerja_nama = db.Column(db.String(255))
    lv2_unit_kerja_id = db.Column(db.String(255))
    lv2_unit_kerja_nama = db.Column(db.String(255))
    lv3_unit_kerja_id = db.Column(db.String(255))
    lv3_unit_kerja_nama = db.Column(db.String(255))
    lv4_unit_kerja_id = db.Column(db.String(255))
    lv4_unit_kerja_nama = db.Column(db.String(255))
    level = db.Column(db.String(255))
    parent_id = db.Column(db.String(255))
    jabatan_id = db.Column(db.String(255))
    jabatan_nama = db.Column(db.String(255))
    eselon_id = db.Column(db.String(255))
    eselon_nm = db.Column(db.String(255))
    gol_id_minimum = db.Column(db.String(255))
    kode_skpd = db.Column(db.String(255))

def __init__(self, satuan_kerja_id, satuan_kerja_nama):
    self.satuan_kerja_id = satuan_kerja_id
    self.satuan_kerja_nama = satuan_kerja_nama
