''' Doc: model bidang_urusan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class BidangUrusanIndikatorSkpdModel(BaseModel, db.Model):
    """Model for the bidang_urusan_indikator_skpd table"""
    __tablename__ = 'bidang_urusan_indikator_skpd'

    id = db.Column(db.Integer, primary_key=True)
    kode_indikator = db.Column(db.Integer, db.ForeignKey('bidang_urusan_indikator.kode_indikator'))
    kode_skpd = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    status_assignment = db.Column(db.String(255))
    notes = db.Column(db.Text)

def __init__(self, kode_indikator, kode_skpd):
    self.kode_indikator = kode_indikator
    self.kode_skpd = kode_skpd
