''' Doc: model feedback map masalah '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class FeedbackMapMasalahModel(BaseModel, db.Model):
    """Model for the feedback_map_masalah table"""
    __tablename__ = 'feedback_map_masalah'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    feedback_map_id = db.Column(db.Integer)
    masalah = db.Column(db.String(250))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, feedback_map_id, masalah, is_active, is_deleted):
    self.feedback_map_id = feedback_map_id
    self.masalah = masalah
    self.is_active = is_active
    self.is_deleted = is_deleted
