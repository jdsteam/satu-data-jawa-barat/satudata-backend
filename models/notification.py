''' Doc: model notification '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class NotificationModel(BaseModel, db.Model):
    """Model for the notification table"""
    __tablename__ = 'notification'

    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(255))
    type_id = db.Column(db.Integer)
    sender = db.Column(db.Integer)
    receiver = db.Column(db.Integer)
    title = db.Column(db.String(255))
    content = db.Column(db.String(255))
    is_read = db.Column(db.Boolean, default=False)
    is_enable = db.Column(db.Boolean, default=True)
    feature = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    notes = db.Column(db.Text)

    # notification feature
    # 1 ➝ ulasan dataset
    # 2 ➝ membutuhkan verifikasi
    # 3 ➝ request dataset satudata
    # 4 ➝ request dataset opendata
    # 5 ➝ ewalidata

def __init__(self, _type, type_id, receiver, title, content):
    self.type = _type
    self.type_id = type_id
    self.receiver = receiver
    self.title = title
    self.content = content
