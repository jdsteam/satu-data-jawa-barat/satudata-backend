''' Doc: model app '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db


class DashboardAppModel(BaseModel, db.Model):
    """Model for the app table"""
    __table_args__ = {'schema': 'dashboard'}
    __tablename__ = 'app'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    url = db.Column(db.String(255))
    title = db.Column(db.String(255))
    notes = db.Column(db.Text)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    kode_skpd = db.Column(db.String(255))
    picture = db.Column(db.Text)


def __init__(self, name, url, title):
    self.name = name
    self.url = url
    self.title = title
