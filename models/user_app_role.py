''' Doc: model user_app_role '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class UserAppRoleModel(BaseModel, db.Model):
    """Model for the users_app_role table"""
    __tablename__ = 'users_app_role'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    app_id = db.Column(db.Integer, db.ForeignKey("app.id"))
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)


def __init__(self, user_id, app_id, role_id):
    self.user_id = user_id
    self.app_id = app_id
    self.role_id = role_id
