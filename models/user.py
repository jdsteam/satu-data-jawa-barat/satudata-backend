''' Doc: model user '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class UserModel(BaseModel, db.Model):
    """Model for the users table"""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255))
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    nip = db.Column(db.String(255))
    kode_kemendagri = db.Column(db.String(255))
    kode_skpd = db.Column(db.String(255))
    kode_skpdsub = db.Column(db.String(255))
    kode_skpdunit = db.Column(db.String(255))
    name = db.Column(db.String(255))
    profile_pic = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    last_login = db.Column(db.DateTime(True))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    count_notif = db.Column(db.Integer, default=0)
    agreement_id = db.Column(db.Integer, default=0)
    satuan_kerja_id = db.Column(db.Integer)
    lv1_unit_kerja_id = db.Column(db.String(255))
    lv2_unit_kerja_id = db.Column(db.String(255))
    lv3_unit_kerja_id = db.Column(db.String(255))
    lv4_unit_kerja_id = db.Column(db.String(255))
    level_unit_kerja = db.Column(db.Integer)
    jabatan_id = db.Column(db.String(255))
    role_id = db.Column(db.Integer)
    dash_role_id = db.Column(db.Integer)
    json_data = db.Column(db.String(255))


def __init__(self, username, password):
    self.username = username
    self.password = password
