''' Doc: model history '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class NpsModel(BaseModel, db.Model):
    """Model for the history table"""
    __tablename__ = 'nps'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    score = db.Column(db.Integer)
    message = db.Column(db.String(255))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    page_source = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, datetime, score, message):
    self.datetime = datetime
    self.score = score
    self.message = message
