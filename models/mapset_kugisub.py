''' Doc: model mapset kugisub '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetKugiSubModel(BaseModel, db.Model):
    '''Model for the mapset_kugisub table'''
    __tablename__ = 'mapset_kugisub'

    id = db.Column(db.Integer, primary_key=True)
    mapset_kugi_id = db.Column(db.Integer, db.ForeignKey("mapset_kugi.id"))
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))


def __init__(self, name, notes):
    self.name = name
    self.notes = notes
