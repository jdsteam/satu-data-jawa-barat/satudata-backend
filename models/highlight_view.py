''' Doc: model highlight view '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HighlightViewModel(BaseModel, db.Model):
    """Model for the highlight_view table"""
    __tablename__ = 'highlight_view'

    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.String(255))
    category_id = db.Column(db.Integer)
    notes = db.Column(db.String(255))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    name = db.Column(db.String(255))


def __init__(self, category, category_id, notes, is_active, is_deleted):
    self.category = category
    self.category_id = category_id
    self.notes = notes
    self.is_active = is_active
    self.is_deleted = is_deleted
