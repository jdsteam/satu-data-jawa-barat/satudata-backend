''' Doc: model article '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class ArticleModel(BaseModel, db.Model):
    """Model for the article table"""
    __tablename__ = 'article'

    id = db.Column(db.Integer, primary_key=True)
    article_topic = db.Column(db.String(255))
    creator = db.Column(db.String(255))
    name = db.Column(db.String(255))
    title = db.Column(db.String(255))
    image = db.Column(db.Text)
    content_short = db.Column(db.String(255))
    content_long = db.Column(db.Text)
    notes = db.Column(db.Text)
    keywords = db.Column(db.String)
    organization = db.Column(db.String(100))
    count_view = db.Column(db.Integer, default=0)
    count_share_fb = db.Column(db.Integer, default=0)
    count_share_tw = db.Column(db.Integer, default=0)
    count_share_wa = db.Column(db.Integer, default=0)
    count_share_link = db.Column(db.Integer, default=0)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)


def __init__(self, name):
    self.name = name
