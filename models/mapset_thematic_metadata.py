''' Doc: model mapset thematic '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetThematicMetadataModel(BaseModel, db.Model):
    '''Model for the mapset_thematic_metadata table'''
    __tablename__ = 'mapset_thematic_metadata'

    id = db.Column(db.Integer, primary_key=True)
    mapset_thematic_id = db.Column(
        db.Integer, db.ForeignKey("mapset_thematic.id"))
    key = db.Column(db.String(255))
    value = db.Column(db.String(255))
    description = db.Column(db.String(255))
