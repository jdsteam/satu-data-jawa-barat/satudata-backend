''' Doc: model regional '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class RegionalModel(BaseModel, db.Model):
    """Model for the regional table"""
    __tablename__ = 'regional'

    id = db.Column(db.Integer, primary_key=True)
    regional_level_id = db.Column(db.Integer, db.ForeignKey("regional_level.id"))
    kode_bps = db.Column(db.String(255))
    nama_bps = db.Column(db.String(255))
    kode_kemendagri = db.Column(db.String(255))
    nama_kemendagri = db.Column(db.String(255))
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    image = db.Column(db.String(255))
    status_interoperabilitas = db.Column(db.String(255))
    dataset_count = db.Column(db.Integer)
    date_update = db.Column(db.DateTime(True))
    date_pipeline = db.Column(db.DateTime(True))

def __init__(self, regional_level_id, kode_bps, nama_bps, kode_kemendagri, nama_kemendagri):
    self.regional_level_id = regional_level_id
    self.kode_bps = kode_bps
    self.nama_bps = nama_bps
    self.kode_kemendagri = kode_kemendagri
    self.nama_kemendagri = nama_kemendagri
