''' Doc: model regional level '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class RegionalLevelModel(BaseModel, db.Model):
    """Model for the regional_level table"""
    __tablename__ = 'regional_level'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, name, notes):
    self.name = name
    self.notes = notes
