''' Doc: model feedback masalah '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class FeedbackMasalahModel(BaseModel, db.Model):
    """Model for the feedback_masalah table"""
    __tablename__ = 'feedback_masalah'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    feedback_id = db.Column(db.Integer)
    masalah = db.Column(db.String(250))
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, feedback_id, masalah, is_active, is_deleted):
    self.feedback_id = feedback_id
    self.masalah = masalah
    self.is_active = is_active
    self.is_deleted = is_deleted
