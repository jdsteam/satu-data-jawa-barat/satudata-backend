''' Doc: model mapset thematic '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetThematicModel(BaseModel, db.Model):
    '''Model for the mapset_thematic table'''
    __tablename__ = 'mapset_thematic'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    image = db.Column(db.Text)
    description = db.Column(db.Text)
    year = db.Column(db.String(4))
    map_type = db.Column(db.String(255))
    kode_bidang_urusan = db.Column(db.String(255))
    sektoral_id = db.Column(db.Integer)
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    count_view = db.Column(db.Integer, default=0)
    url = db.Column(db.Text)
    data_processor = db.Column(db.Text)