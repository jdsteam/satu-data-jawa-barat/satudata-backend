''' Doc: model mapset webgis '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetWebgisModel(BaseModel, db.Model):
    '''Model for the mapset_webgis table'''
    __tablename__ = 'mapset_webgis'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    url = db.Column(db.Text)
    skpd_id = db.Column(db.Integer)
    count_view = db.Column(db.Integer, default=0)
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)

def __init__(self, id):
    self.id = id
