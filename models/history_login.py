''' Doc: model history login '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryLoginModel(BaseModel, db.Model):
    """Model for the history_login table"""
    __tablename__ = 'history_login'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    ip = db.Column(db.String(50))
    browser = db.Column(db.String(50))
    country_code = db.Column(db.String(30))
    country = db.Column(db.String(20))
    region = db.Column(db.String(100))
    lat = db.Column(db.String(50))
    long = db.Column(db.String(50))
    type = db.Column(db.String(50))

def __init__(self, user_id, datetime, _ip, browser, country_code, country, region, lat, long, _type):
    self.user_id = user_id
    self.datetime = datetime
    self.ip = _ip
    self.browser = browser
    self.country_code = country_code
    self.country = country
    self.region = region
    self.lat = lat
    self.long = long
    self.type = _type
