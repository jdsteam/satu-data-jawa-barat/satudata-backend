''' Doc: model history request public '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryRequestPublicModel(BaseModel, db.Model):
    """Model for the history_request_public table"""
    __tablename__ = 'history_request_public'

    id = db.Column(db.Integer, primary_key=True)
    request_public_id = db.Column(db.Integer)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    status = db.Column(db.Integer)
    cuid = db.Column(db.Integer)
    notes = db.Column(db.Text)
    notes_type = db.Column(db.String(250))
    dataset_source = db.Column(db.String(250))
    odj_link = db.Column(db.Text)
    odj_notes = db.Column(db.Text)
    jd_judul = db.Column(db.String(250))
    jd_link = db.Column(db.Text)
    jd_topik = db.Column(db.String(250))
    jd_tahun = db.Column(db.Integer)
    jd_wewenang_prov = db.Column(db.Boolean, default=False)
    jd_organisasi = db.Column(db.String(250))
    jd_organisasi_kontak = db.Column(db.String(250))
    jd_notes = db.Column(db.Text)
    sl_notes = db.Column(db.Text)

def __init__(self, request_public_id, status):
    self.request_public_id = request_public_id
    self.status = status
