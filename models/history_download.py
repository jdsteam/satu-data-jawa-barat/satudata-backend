''' Doc: model history download '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class HistoryDownloadModel(BaseModel, db.Model):
    """Model for the history_download table"""
    __tablename__ = 'history_download'

    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    type = db.Column(db.String(250))
    email = db.Column(db.String(250))
    pekerjaan = db.Column(db.String(250))
    bidang = db.Column(db.String(250))
    tujuan = db.Column(db.String(250))
    lokasi = db.Column(db.String(250))
    category = db.Column(db.String(250))
    category_id = db.Column(db.Integer)
    notes = db.Column(db.Text)

def __init__(self, _type, email, pekerjaan, bidang, tujuan, lokasi, notes, category, category_id):
    self.type = _type
    self.email = email
    self.pekerjaan = pekerjaan
    self.bidang = bidang
    self.tujuan = tujuan
    self.lokasi = lokasi
    self.notes = notes
    self.category = category
    self.category_id = category_id
