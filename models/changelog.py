from sys import version
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
from helpers.jwtmanager import JwtManager


class ChangelogModel(BaseModel, db.Model):
    __tablename__ = "changelog"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    description = db.Column(db.Text)
    version = db.Column(db.String(255))
    timestamp = db.Column(db.DateTime)
    is_public = db.Column(db.Boolean)
    is_deleted = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime, default=db.func.current_timestamp())
    mdate = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())
    cuid = db.Column(db.Integer, default=JwtManager.get_user_id())
    muid = db.Column(db.Integer, default=JwtManager.get_user_id(), onupdate=JwtManager.get_user_id())

    def __init__(self, title, description, version, timestamp, is_public):
        self.title = title
        self.description = description
        self.version = version
        self.timestamp = timestamp
        self.is_public = is_public

    def __repr__(self):
        return '<Changelog %r>' % self.title
    
