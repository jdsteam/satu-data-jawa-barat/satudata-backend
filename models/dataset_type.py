''' Doc: model dataset type '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class DatasetTypeModel(BaseModel, db.Model):
    """Model for the dataset_type table"""
    __tablename__ = 'dataset_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, name, notes):
    self.name = name
    self.notes = notes
