''' Doc: model bidang_urusan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class EwalidataDataModel(BaseModel, db.Model):
    """Model for the ewalidata_data table"""
    __tablename__ = 'ewalidata_data'

    id = db.Column(db.Integer, primary_key=True)
    kode_indikator = db.Column(db.Integer, db.ForeignKey('bidang_urusan_indikator.kode_indikator'))
    kode_pemda = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    data = db.Column(db.String(255))
    verified = db.Column(db.String(255))
    status_verifikasi_walidata = db.Column(db.String(255))
    catatan_verifikasi_walidata = db.Column(db.String(255))
    tanggal_verifikasi_walidata = db.Column(db.DateTime(True), default=db.func.now())
    status = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)

def __init__(self, kode_indikator, kode_pemda,data):
    self.kode_indikator = kode_indikator
    self.kode_pemda = kode_pemda
    self.data = data
