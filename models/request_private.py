''' Doc: model request private '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class RequestPrivateModel(BaseModel, db.Model):
    """Model for the request_private table"""
    __tablename__ = 'request_private'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    nama = db.Column(db.String(250))
    telp = db.Column(db.String(250))
    email = db.Column(db.String(250))
    judul_data = db.Column(db.String(250))
    tau_skpd = db.Column(db.Boolean, default=False)
    nama_skpd = db.Column(db.String(250))
    kebutuhan_data = db.Column(db.Text)
    tujuan_data = db.Column(db.String(250))
    datetime = db.Column(db.DateTime(True), default=db.func.now())
    status = db.Column(db.Integer, default=1)
    notes = db.Column(db.Text)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    mdate = db.Column(db.DateTime(True), default=db.func.now())

def __init__(self, nama, email, judul_data, tujuan_data):
    self.nama = nama
    self.email = email
    self.judul_data = judul_data
    self.tujuan_data = tujuan_data
