''' Doc: model mapset award '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetAreaCoverageModel(BaseModel, db.Model):
    '''Model for the mapset_area_coverage table'''
    __tablename__ = 'mapset_area_coverage'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    picture = db.Column(db.String(255))
    notes = db.Column(db.Text)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())

def __init__(self, id, name):
    self.id = id
    self.name = name
