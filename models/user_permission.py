''' Doc: model user permission '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class UserPermissionModel(BaseModel, db.Model):
    """Model for the users_permission table"""
    __tablename__ = 'users_permission'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    app_id = db.Column(db.Integer, db.ForeignKey("app.id"))
    app_service_id = db.Column(db.Integer, db.ForeignKey("app_service.id"))
    enable = db.Column(db.Boolean, default=False)
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)


def __init__(self, user_id, app_id, app_service_id, enable):
    self.user_id = user_id
    self.app_id = app_id
    self.app_service_id = app_service_id
    self.enable = enable
