''' Doc: model request '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class RequestModel(BaseModel, db.Model):
    """Model for the request table"""
    __tablename__ = 'request'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    title = db.Column(db.String(100))
    kode_skpd = db.Column(db.String(100))
    notes = db.Column(db.Text)
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())

def __init__(self, user_id, title, kode_skpd, notes):
    self.user_id = user_id
    self.title = title
    self.kode_skpd = kode_skpd
    self.notes = notes
