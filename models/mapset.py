''' Doc: model mapset '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetModel(BaseModel, db.Model):
    '''Model for the mapset table'''
    __tablename__ = 'mapset'

    id = db.Column(db.Integer, primary_key=True)
    app_id = db.Column(db.Integer)
    app_service_id = db.Column(db.Integer)
    dataset_class_id = db.Column(db.Integer, db.ForeignKey('dataset_class.id'))
    mapset_type_id = db.Column(db.Integer, db.ForeignKey('mapset_type.id'))
    mapset_source_id = db.Column(db.Integer, db.ForeignKey('mapset_source.id'))
    mapset_kugi_id = db.Column(db.Integer, db.ForeignKey('mapset_kugi.id'))
    mapset_kugisub_id = db.Column(db.Integer, db.ForeignKey('mapset_kugisub.id'))
    regional_id = db.Column(db.Integer, db.ForeignKey('regional.id'))
    sektoral_id = db.Column(db.Integer, db.ForeignKey('sektoral.id'))
    app_link = db.Column(db.Text)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)
    description = db.Column(db.String(255))
    kode_skpd = db.Column(db.String(255))
    kode_skpdunit = db.Column(db.String(255))
    kode_skpdsub = db.Column(db.String(255))
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    name = db.Column(db.String(255))
    title = db.Column(db.String(255))
    is_popular = db.Column(db.Boolean, default=False)
    is_active = db.Column(db.Boolean, default=True)
    is_deleted = db.Column(db.Boolean, default=False)
    satuan = db.Column(db.String(255))
    tahun = db.Column(db.String(255))
    frekuensi = db.Column(db.String(255))
    metadata_xml = db.Column(db.Text)
    notes = db.Column(db.Text)
    owner = db.Column(db.String(255))
    owner_email = db.Column(db.String(255))
    maintainer = db.Column(db.String(255))
    maintainer_email = db.Column(db.String(255))
    mapset_program_id = db.Column(db.Integer)
    skpd_id = db.Column(db.Integer)
    scale = db.Column(db.String(255))
    update_period = db.Column(db.String(255))
    revision_date = db.Column(db.String(255))
    metadata_created_date = db.Column(db.String(255))
    data_version = db.Column(db.String(255))
    owner_address = db.Column(db.String(255))
    owner_telephone = db.Column(db.String(255))
    organization_manual = db.Column(db.String(255))
    area_coverage = db.Column(db.String(255))
    access_organization = db.Column(db.String)
    shp = db.Column(db.Text)
    is_permanent = db.Column(db.Boolean, default=True)
    is_validate = db.Column(db.Integer, default=0)
    mapset_area_coverage_id = db.Column(db.Integer)
    count_view = db.Column(db.Integer, default=0)
    count_access = db.Column(db.Integer, default=0)
    count_view_private = db.Column(db.Integer, default=0)
    count_access_private = db.Column(db.Integer, default=0)
    count_download_dataset = db.Column(db.Integer, default=0)
    count_download_image = db.Column(db.Integer, default=0)
    count_download_shp = db.Column(db.Integer, default=0)
    count_download_geojson = db.Column(db.Integer, default=0)


def __init__(
        self, dataset_class_id, mapset_type_id,
        mapset_source_id, sektoral_id, regional_id, name, title
    ):
    self.mapset_type_id = mapset_type_id
    self.mapset_source_id = mapset_source_id
    self.regional_id = regional_id
    self.dataset_class_id = dataset_class_id
    self.sektoral_id = sektoral_id
    self.name = name
    self.title = title
