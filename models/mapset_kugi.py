''' Doc: model mapset kugi '''
from helpers.postgre_alchemy import postgre_alchemy as db
from models import BaseModel

class MapsetKugiModel(BaseModel, db.Model):
    '''Model for the mapset_kugi table'''
    __tablename__ = 'mapset_kugi'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    notes = db.Column(db.String(255))


def __init__(self, name, notes):
    self.name = name
    self.notes = notes
