''' Doc: model jabatan '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class JabatanModel(BaseModel, db.Model):
    """Model for the jabatan table"""
    __tablename__ = 'jabatan'

    id = db.Column(db.String(255), primary_key=True)
    jabatan_id = db.Column(db.String(255))
    jabatan_nama = db.Column(db.String(255))
    jabatan_kelas = db.Column(db.String(255))
    jabatan_jenis = db.Column(db.String(255))
    satuan_kerja_id = db.Column(db.String(255))
    satuan_kerja_nama = db.Column(db.String(255))
    unit_kerja_id = db.Column(db.String(255))
    unit_kerja_nama = db.Column(db.String(255))
    eselon_id = db.Column(db.String(255))
    eselon_nm = db.Column(db.String(255))
    atasan_jabatan_id = db.Column(db.String(255))
    jf_id = db.Column(db.String(255))
    jfu_id = db.Column(db.String(255))
    notes = db.Column(db.String(255))

def __init__(self, jabatan_id, jabatan_nama):
    self.jabatan_id = jabatan_id
    self.jabatan_nama = jabatan_nama
