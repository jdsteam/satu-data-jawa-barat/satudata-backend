''' Doc: model indikator '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class IndikatorModel(BaseModel, db.Model):
    """Model for the indikator table"""
    __tablename__ = 'indikator'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255))
    name = db.Column(db.String(255))
    dataset_type_id = db.Column(db.Integer)
    indikator_category_id = db.Column(db.Integer)
    regional_id = db.Column(db.Integer)
    kode_skpd = db.Column(db.String(100))
    kode_skpd_sub = db.Column(db.String(100))
    kode_skpd_unit = db.Column(db.String(100))
    data_dasar = db.Column(db.String(100))
    rumus = db.Column(db.String(100))
    satuan_id = db.Column(db.Integer)
    description = db.Column(db.String(50))
    source = db.Column(db.String(250), default='-')
    interpretation = db.Column(db.String(250), default='-')
    datetime = db.Column(db.DateTime(True))
    notes = db.Column(db.String(255))
    cuid = db.Column(db.Integer)
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    muid = db.Column(db.Integer)
    mdate = db.Column(db.DateTime(True), default=db.func.now())
    is_active = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=True)
    is_validate = db.Column(db.Integer, default=3)
    count_view = db.Column(db.Integer, default=0)
    count_access = db.Column(db.Integer, default=0)

def __init__(
        self, title, name, dataset_type_id, indikator_category_id,
        regional_id, kode_skpd, kode_skpd_sub, kode_skpd_unit, data_dasar
    ):
    self.title = title
    self.name = name
    self.dataset_type_id = dataset_type_id
    self.indikator_category_id = indikator_category_id
    self.regional_id = regional_id
    self.kode_skpd = kode_skpd
    self.kode_skpd_sub = kode_skpd_sub
    self.kode_skpd_unit = kode_skpd_unit
    self.data_dasar = data_dasar
