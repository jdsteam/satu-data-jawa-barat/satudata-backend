''' Doc: model visualization  '''
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db

class VisualizationPermissionModel(BaseModel, db.Model):
    """Model for the visualization_permission table"""
    __tablename__ = 'visualization_permission'

    id = db.Column(db.Integer, primary_key=True)
    visualization_id = db.Column(db.Integer)
    jabatan_id = db.Column(db.String(255))
    enable = db.Column(db.Boolean, default=True)
    notes = db.Column(db.String(255))
    cdate = db.Column(db.DateTime(True), default=db.func.now())
    cuid = db.Column(db.Integer)

def __init__(self, visualization_id, jabatan_id):
    self.visualization_id = visualization_id
    self.jabatan_id = jabatan_id
