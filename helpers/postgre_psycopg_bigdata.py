''' Doc: helper postgre '''
import psycopg2
import psycopg2.extras
from psycopg2.extras import DictCursor
from psycopg2.extras import RealDictCursor
from settings import configuration

# pylint: disable=broad-except
def query(qry):
    ''' Doc: class connect '''
    try:
        connection = psycopg2.connect(
            host=configuration.POSTGRE_HOST,
            port=configuration.POSTGRE_PORT,
            database=configuration.POSTGRE_DB_BIGDATA,
            user=configuration.POSTGRE_USER,
            password=configuration.POSTGRE_PASSWORD,
        )

        try:
            cursor = connection.cursor()
            cursor.execute(qry)
            results = cursor.fetchall()
        except Exception as err:
            connection.close()
            return False, str(err)

        connection.close()
        return True, results

    except Exception as err:
        return False, str(err)

def query_dict_array(qry,params=None):
    ''' Doc: class connect '''
    try:
        connection = psycopg2.connect(
            host=configuration.POSTGRE_HOST,
            port=configuration.POSTGRE_PORT,
            database=configuration.POSTGRE_DB_BIGDATA,
            user=configuration.POSTGRE_USER,
            password=configuration.POSTGRE_PASSWORD,
        )

        try:
            cursor = connection.cursor(cursor_factory=RealDictCursor)
            if params:
                cursor.execute(qry,params)
            else:
                cursor.execute(qry)
            results = cursor.fetchall()
        except Exception as err:
            connection.close()
            return False, str(err)

        connection.close()
        return True, results

    except Exception as err:
        return False, str(err)

def query_dict_single(query, params=None):
    try:
        connection = psycopg2.connect(
            host=configuration.POSTGRE_HOST,
            port=configuration.POSTGRE_PORT,
            database=configuration.POSTGRE_DB_BIGDATA,
            user=configuration.POSTGRE_USER,
            password=configuration.POSTGRE_PASSWORD,
        )
        try:
            cursor = connection.cursor(cursor_factory=DictCursor)
            if params:
                cursor.execute(query, params)
            else:
                cursor.execute(query)
            result = cursor.fetchone()
        except Exception as e:
            connection.close()
            return False, str(e)

        connection.close()
        return True, result

    except Exception as e:
        return False, str(e)

def commit(qry):
    ''' Doc: class connect '''
    try:
        connection = psycopg2.connect(
            host=configuration.POSTGRE_HOST,
            port=configuration.POSTGRE_PORT,
            database=configuration.POSTGRE_DB_BIGDATA,
            user=configuration.POSTGRE_USER,
            password=configuration.POSTGRE_PASSWORD,
        )

        try:
            cursor = connection.cursor()
            cursor.execute(qry)
            connection.commit()
            results = cursor.fetchall()
        except Exception as err:
            connection.close()
            return False, str(err)

        connection.close()
        return True, results

    except Exception as err:
        return False, str(err)
