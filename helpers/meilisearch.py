import meilisearch
from settings.configuration import (
    MEILISEARCH_HOST,
    MEILISEARCH_API_KEY,
    MEILISEARCH_INDEX,
)
from helpers.postgre_alchemy import postgre_alchemy as db


class AdvanceSearch:
    def __init__(self):
        self.client = meilisearch.Client(MEILISEARCH_HOST, MEILISEARCH_API_KEY)
        self.index = self.client.index(MEILISEARCH_INDEX)
        self.settings = {
            "searchableAttributes": ["name", "sektoral_name", "description", "metadata"],
            "displayedAttributes": ["*"],
            "sortableAttributes": ["id", "name", "sektor", "count_view", "mdate", "cdate"],
            "pagination": {"maxTotalHits": self.get_total()},
            "faceting": {"maxValuesPerFacet": 200},
            "filterableAttributes": [
                "id",
                "title",
                "name",
                "regional_id",
                "dataset_class_id",
                "dataset_class_name",
                "kode_bps",
                "kode_skpd",
                "picture_thumbnail",
                "count_view",
                "count_rating",
                "count_view_private",
                "count_access_private",
                "skpd_title",
                "nama_skpd",
                "regional_nama_kemendagri",
                "metadata",
                "dataset_url",
                "is_realtime",
                "is_permanent",
                "mdate",
                "cdate",
                "description",
                "sektoral_id",
                "sektoral_name",
                "quality_score_point",
                "quality_score_label"
            ],
        }
        self.index.update_settings(self.settings)

    def do(self, query: str, options: dict) -> list:
        return self.index.search(query=query, opt_params=options)

    def update_settings(self, settings: dict):
        self.index.update_settings(settings)

    def schedule_update_settings(self):
        self.settings["pagination"]["maxTotalHits"] = self.get_total()
        self.index.update_settings(self.settings)

    def get_total(self):
        return self.index.get_stats().__dict__["_IndexStats__dict"]["numberOfDocuments"]
