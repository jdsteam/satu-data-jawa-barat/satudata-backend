''' Doc: helper decorator  '''
import json
import jwt
from flask import request
from functools import wraps
from helpers import Helper
from flask import Response
from helpers.jsonencoder import JSONEncoder
from settings import configuration
from exceptions import ErrorMessage
from controllers import AuthController
from controllers import UserController
from models import AppModel
from models import AppServiceModel
from models import UserPermissionModel
from models import DatasetClassModel
from models import DatasetModel
from models import DatasetPermissionModel
from helpers.postgre_alchemy import postgre_alchemy as db

AUTH_CONTROLLER = AuthController()
USER_CONTROLLER = UserController()
HELPER = Helper()
MESSAGE_MIMETYPE = 'application/json'

# pylint: disable=broad-except, unused-argument, unused-variable
# def jwt_check_old():
#     ''' Doc: function jwt check  '''

#     def wrapper(func):
#         ''' Doc: function wrapper  '''

#         @wraps(func)
#         def decorator(*args, **kwargs):
#             ''' Doc: function decorator  '''

#             token = request.headers.get('Authorization', None)
#             app_code = configuration.APP_CODE
#             controller = request.path
#             action = request.method

#             if controller[0] == '/':
#                 controller = controller[(len(controller)-1)*-1:]

#             controller = controller.split("/")
#             controller = controller[0]

#             return check_backend(app_code, controller, action, token)

#         return decorator

#     return wrapper

def jwt_check():
    ''' Doc: function jwt check  '''

    def wrapper(func):
        ''' Doc: function wrapper  '''

        @wraps(func)
        def decorator(*args, **kwargs):
            ''' Doc: function decorator  '''

            token = request.headers.get('Authorization', None)
            controller = request.path
            action = request.method

            if controller[0] == '/':
                controller = controller[(len(controller)-1)*-1:]

            controller = controller.split("/")
            controller = controller[0]

            res_token, data_token = check_role_token(token)
            if res_token:
                res_user, data_user = check_role_user(token, controller, action)
                if res_user:
                    # check pass
                    return func(*args, **kwargs)
                else:
                    return Response(json.dumps(data_token, cls=JSONEncoder), mimetype='application/json'), 403
            else:
                return Response(json.dumps(data_token, cls=JSONEncoder), mimetype='application/json'), 403

        return decorator

    return wrapper

def jwt_check_coredata():
    ''' Doc: function jwt check  '''

    def wrapper(func):
        ''' Doc: function wrapper  '''

        @wraps(func)
        def decorator(*args, **kwargs):
            ''' Doc: function decorator  '''

            token = request.headers.get('Authorization', None)
            controller = request.path

            if controller[0] == '/':
                controller = controller[(len(controller)-1)*-1:]
            controller = controller.split("/")

            # bigdata
            if controller[0] == 'bigdata':
                schema = controller[1]
                table = controller[2]
            elif controller[0] == 'geojson':
                schema = request.args.get("schema", "")
                table = request.args.get("table", "")
            else:
                schema = ''
                table = ''

            # schema
            if not schema:
                message = {"message": "Schema not found.", "error": 1, "data": {}}
                return Response(json.dumps(message, cls=JSONEncoder), mimetype='application/json'), 403
            # table
            if not table:
                message = {"message": "Table not found.", "error": 1, "data": {}}
                return Response(json.dumps(message, cls=JSONEncoder), mimetype='application/json'), 403

            res, message = check_bigdata('bigdata', schema, table, token)
            if res:
                # check pass
                return func(*args, **kwargs)
            else:
                return Response(json.dumps(message, cls=JSONEncoder), mimetype='application/json'), 403

        return decorator

    return wrapper



# old version
# def check_backend(app_code, controller, action, token):
#     ''' Doc: function check is backend'''
#     try:
#         # get permission is backend
#         res, permission = get_backend(app_code, controller, action)

#         if res:
#             # is backend
#             if permission['is_backend']:
#                 return check_user(app_code, controller, action, token)

#             # is bigdata
#             else:
#                 # classification publik
#                 if (permission['dataset_class']['name'].lower() == 'publik') or (permission['dataset_class']['id'] == 3):
#                     response = {"message": "Access Granted, Data is public", "error": 0, "data": {}}
#                     return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

#                 # classification internal
#                 elif (permission['dataset_class']['name'].lower() == 'internal') or (permission['dataset_class']['id'] == 5):
#                     if token is not None:
#                         respon, payload = check_token(token)
#                         if respon:
#                             # user is dinas
#                             if not payload['is_external']:
#                                 response = {"message": "Access Granted, Data is internal", "error": 0, "data": {}}
#                                 return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
#                             # user is external
#                             else:
#                                 return check_user(app_code, controller, action, token)
#                         else:
#                             return Response(json.dumps(payload, cls=JSONEncoder), mimetype='application/json'), 403
#                     else:
#                         # jika ga ada token
#                         response = {"message": "Token Missing", "error": 1, "data": {}}
#                         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#                 # classification dikecualikan
#                 elif((permission['dataset_class']['name'].lower() == 'dikecualikan') or (permission['dataset_class']['id'] == 4)):
#                     return check_user(app_code, controller, action, token)

#                 # not found
#                 else:
#                     return check_user(app_code, controller, action, token)
#         else:
#             # code tidak ditemukan
#             response = {"message": "App Code, Controller, or Action not found", "error": 1, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#     except Exception as err:
#         # fail query
#         response = {"message": "Failed get detail service, " + str(err), "error": 1, "data": {}}
#         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

# def get_backend(app_code: str, controller: str, action: str):
#     ''' Doc: function init  '''
#     try:
#         # query postgresql
#         result = db.session.query(
#             AppModel.id, AppModel.name, AppModel.code, AppModel.url,
#             AppServiceModel.controller, AppServiceModel.action, AppServiceModel.is_backend, AppServiceModel.dataset_class_id
#         )
#         result = result.join(AppServiceModel, AppModel.id == AppServiceModel.app_id)
#         result = result.filter(AppModel.code == str(app_code))
#         result = result.filter(AppServiceModel.controller == str(controller))
#         result = result.filter(AppServiceModel.action == str(action))
#         # result = result.limit(1)
#         result = result.all()


#         results = []
#         for res in result:
#             temp = {}
#             temp['id'] = res[0]
#             temp['name'] = res[1]
#             temp['code'] = res[2]
#             temp['url'] = res[3]
#             temp['controller'] = res[4]
#             temp['action'] = res[5]
#             temp['is_backend'] = res[6]
#             temp['dataset_class_id'] = res[7]

#             if temp['dataset_class_id']:
#                 # get relation to skpd
#                 dataset_class = db.session.query(DatasetClassModel)
#                 dataset_class = dataset_class.get(temp['dataset_class_id'])

#                 if dataset_class:
#                     dataset_class = dataset_class.__dict__
#                     dataset_class.pop('_sa_instance_state', None)
#                 else:
#                     dataset_class = {}

#                 temp['dataset_class'] = dataset_class

#             else:
#                 temp['dataset_class'] = {}

#             results.append(temp)


#         # check if empty
#         if results:
#             return True, results[0]
#         else:
#             return False, {}

#     except Exception as err:
#         # fail response
#         print(err)
#         raise ErrorMessage(str(err), 500, 1, {})

# def check_token(token):
#     ''' Doc: function check token'''
#     if token:
#         try:
#             # decode jwt
#             token = token.split(" ")
#             payload = jwt.decode(token[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])
#             return True, payload

#         except jwt.DecodeError:
#             # jika token salah (secret key / algoitmanya)
#             response = {"message": "Token Invalid", "error": 1, "data": {}}
#             return False, response

#         except jwt.ExpiredSignatureError:
#             # jika token sudah expired
#             response = {"message": "Token Expired", "error": 1, "data": {}}
#             return False, response

#         except Exception as err:
#             # jika lainnya
#             response = {"message":  str(err), "error": 1, "data": {}}
#             return False, response

#     else:
#         # jika ga ada token
#         response = {"message": "Token Missing", "error": 1, "data": {}}
#         return False, response

# def check_user(app_code, controller, action, token):
#     ''' Doc: function check user'''
#     try:
#         if token:

#             # decode jwt
#             token = token.split(" ")
#             payload = jwt.decode(token[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])

#             # pencarian user
#             res, user = USER_CONTROLLER.get_by_id(payload["id"])

#             # cek user
#             try:
#                 if ("id" in payload) and ("username" in payload) and ("role" in payload) and ("id" in payload["role"]):
#                     # cek username dan id
#                     if user["username"] == payload["username"]:
#                         return check_permission(app_code, controller, action, payload)

#                     else:
#                         # username tidak cocok dengan id
#                         response = {"message": "User not match", "error": 1, "data": {}}
#                         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403
#                 else:
#                     # id tidak ditemukan
#                     response = {"message": "User not found", "error": 1, "data": {}}
#                     return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#             except Exception as err:
#                 # id tidak ditemukan
#                 response = {"message": "User not valid", "error": 1, "data": {}}
#                 return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#         else:
#             response = {"message": "Token Missing", "error": 1, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#     except Exception as err:
#         # id tidak ditemukan
#         response = {"message": "Failed get user, " + str(err), "error": 1, "data": {}}
#         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

# def check_permission(app_code, controller, action, payload):
#     ''' Doc: function check permission '''
#     try:
#         res, roles = get_permission(payload["id"], app_code, controller, action)

#         # jika ada
#         if res:
#             # jika boleh
#             if roles[0]["enable"]:
#                 # url yg diakses sesuai dengan rolenya
#                 response = {"message": "Access Granted", "error": 0, "data": {}}
#                 return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

#             else:
#                 # url yg diakses tidak sesuai dengan rolenya
#                 response = {"message": "Access Forbidden", "error": 1, "data": {}}
#                 return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#         else:
#             # url yg diakses tidak sesuai dengan rolenya
#             response = {"message": "URL App, Controller, or Action not found", "error": 1, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

#     except Exception as err:
#         # id tidak ditemukan
#         response = {"message": "Failed get permission. " + str(err), "error": 1, "data": {}}
#         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

# def get_permission(_id: str, app_code: str, controller: str, action: str):
#     ''' Doc: function init  '''
#     try:
#         # query postgresql
#         result = db.session.query(
#             UserPermissionModel.id, UserPermissionModel.user_id, UserPermissionModel.app_id,
#             AppModel.name, AppModel.url, AppModel.code.label('app_code'),
#             UserPermissionModel.app_service_id, AppServiceModel.controller, AppServiceModel.action,
#             UserPermissionModel.enable, UserPermissionModel.notes, UserPermissionModel.cdate, UserPermissionModel.cuid
#         )
#         result = result.join(AppModel, UserPermissionModel.app_id == AppModel.id)
#         result = result.join(AppServiceModel, UserPermissionModel.app_service_id == AppServiceModel.id)
#         result = result.filter(UserPermissionModel.user_id == str(_id))
#         result = result.filter(AppModel.code == str(app_code))
#         result = result.filter(AppServiceModel.controller == str(controller))
#         result = result.filter(AppServiceModel.action == str(action))
#         result = result.all()

#         permission = []
#         for res in result:
#             temp = {}
#             temp['id'] = res[0]
#             temp['user_id'] = res[1]
#             temp['app_id'] = res[2]
#             temp['name'] = res[3]
#             temp['url'] = res[4]
#             temp['app_code'] = res[5]
#             temp['app_service_id'] = res[6]
#             temp['controller'] = res[7]
#             temp['action'] = res[8]
#             temp['enable'] = res[9]
#             temp['notes'] = res[10]
#             temp['cdate'] = res[11]
#             temp['cuid'] = res[12]
#             permission.append(temp)

#         # check if empty
#         if permission:
#             return True, permission
#         else:
#             return False, {}

#     except Exception as err:
#         # fail response
#         print(err)
#         raise ErrorMessage(str(err), 500, 1, {})


# new version
def check_role_token(token):
    ''' Doc: function check token'''
    if token:
        try:
            # decode jwt
            token = token.split(" ")
            payload = jwt.decode(token[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])
            return True, payload

        except jwt.DecodeError:
            # jika token salah (secret key / algoitmanya)
            response = {"message": "Token Invalid", "error": 1, "data": {}}
            return False, response

        except jwt.ExpiredSignatureError:
            # jika token sudah expired
            response = {"message": "Token Expired", "error": 1, "data": {}}
            return False, response

        except Exception as err:
            # jika lainnya
            response = {"message":  str(err), "error": 1, "data": {}}
            return False, response

    else:
        # jika ga ada token
        response = {"message": "Token Missing", "error": 1, "data": {}}
        return False, response

def check_role_user(token, controller, action):
    ''' Doc: function check user'''
    try:
        if token:

            # decode jwt
            token = token.split(" ")
            payload = jwt.decode(token[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])

            # pencarian user
            res, user = USER_CONTROLLER.get_by_id(payload["id"])

            try:
                # cek user
                if ("id" in payload) and ("username" in payload) and ("role_id" in payload):
                    # cek username dan id
                    if user["username"] == payload["username"]:
                        # sukses
                        response = {"message": "Access Granted", "error": 0, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                    else:
                        # username tidak cocok dengan id
                        response = {"message": "User not match", "error": 1, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403
                else:
                    # id tidak ditemukan
                    response = {"message": "User not found", "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

            except Exception as err:
                # id tidak ditemukan
                response = {"message": "User not valid", "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        else:
            response = {"message": "Token Missing", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

    except Exception as err:
        # id tidak ditemukan
        response = {"message": "Failed get user, " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

def check_bigdata(bigdata, schema, table, token):
    ''' Doc: function check token'''
    if bigdata == 'bigdata':
        try:
            # check dataset class
            result = db.session.query(DatasetModel.id, DatasetModel.dataset_class_id, DatasetModel.kode_skpd)
            result = result.filter(DatasetModel.schema == str(schema))
            result = result.filter(DatasetModel.table == str(table))
            result = result.all()

            if result:
                for res in result:
                    if res[1] == 3:
                        response = {"message": "Dataset public.", "error": 0, "data": {}}
                        return True, response
                    elif res[1] == 5:
                        res_token, data_token = check_role_token(token)
                        return res_token, data_token
                    else:
                        if token:
                            token = token.split(" ")
                            payload = jwt.decode(token[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])
                            if payload['role_name'] == 'walidata':
                                # jika dibolehkan
                                response = {"message": "Access Granted, Walidata", "error": 0, "data": {}}
                                return True, response
                            elif payload['role_name'] == 'opd' and (res[2] == payload['kode_skpd']):
                                # jika dibolehkan
                                response = {"message": "Access Granted, OPD Pengelola", "error": 0, "data": {}}
                                return True, response
                            elif payload['jabatan_id']:
                                # check permission
                                result_permission = db.session.query(DatasetPermissionModel.id, DatasetPermissionModel.enable)
                                result_permission = result_permission.filter(DatasetPermissionModel.dataset_id == res[0])
                                result_permission = result_permission.filter(DatasetPermissionModel.jabatan_id == payload['jabatan_id'])
                                result_permission = result_permission.all()
                                permission_enable = False
                                if result_permission:
                                    for res_perm in result_permission:
                                        if res_perm[1]:
                                            permission_enable = True
                                    if permission_enable:
                                        # jika dibolehkan
                                        response = {"message": "Access Granted", "error": 0, "data": {}}
                                        return True, response
                                    else:
                                        # jika tidak boleh
                                        response = {"message": "Access Denied", "error": 1, "data": {}}
                                        return False, response
                                else:
                                    # jika tidak ada akses
                                    response = {"message": "Access not found", "error": 1, "data": {}}
                                    return False, response
                            else:
                                # jika gak ada jabatan
                                response = {"message": "Jabatan not found", "error": 1, "data": {}}
                                return False, response
                        else:
                            # jika gak ada jabatan
                            response = {"message": "Token not found", "error": 1, "data": {}}
                            return False, response
            else:
                # jika gak ada jabatan
                response = {"message": "Dataset not found", "error": 1, "data": {}}
                return False, response

        except Exception as err:
            # jika lainnya
            response = {"message":  str(err), "error": 1, "data": {}}
            return False, response

    else:
        # jika ga ada token
        response = {"message": "Wrong bigdata Endpoint", "error": 1, "data": {}}
        return False, response
