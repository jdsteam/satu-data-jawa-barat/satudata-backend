''' Doc: helper sql global  '''
from models import DatasetModel, DatasetTagModel, DatasetClassModel, SektoralModel
from models import LicenseModel, RegionalModel, SkpdModel, SkpdSubModel, SkpdUnitModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import func
from sqlalchemy.orm import defer
import sentry_sdk

# pylint: disable=broad-except
class SqlGlobal(object):
    ''' Doc: class sql global  '''

    def __init__(self, **kwargs):
        ''' Doc: function init  '''

    @staticmethod
    def dataset(param):
        ''' Doc: function dataset  '''
        try:
            dataset = db.session.query(DatasetModel)
            dataset = dataset.get(param)
            return dataset
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def dataset_tag(param):
        ''' Doc: function dataset tag  '''
        try:
            dataset_tag = db.session.query(func.distinct(func.lower(DatasetTagModel.tag)).label('tag'))
            dataset_tag = dataset_tag.filter(getattr(DatasetTagModel, "dataset_id") == param)
            dataset_tag = dataset_tag.all()
            return dataset_tag
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def dataset_class(param):
        ''' Doc: function dataset class  '''
        try:
            dataset_class = db.session.query(DatasetClassModel)
            dataset_class = dataset_class.get(param)
            return dataset_class
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def sektoral(param):
        ''' Doc: function sektoral  '''
        try:
            sektoral = db.session.query(SektoralModel)
            sektoral = sektoral.options(defer("cuid"), defer("cdate"))
            sektoral = sektoral.get(param)
            return sektoral
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def licenses(param):
        ''' Doc: function licenses  '''
        try:
            licenses = db.session.query(LicenseModel)
            licenses = licenses.get(param)
            return licenses
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def regional(param):
        ''' Doc: function regional  '''
        try:
            regional = db.session.query(RegionalModel)
            regional = regional.options(defer("cuid"), defer("cdate"))
            regional = regional.get(param)
            return regional
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def skpd(param):
        ''' Doc: function skpd  '''
        try:
            skpd = db.session.query(SkpdModel)
            skpd = skpd.options(defer("cuid"), defer("cdate"))
            skpd = skpd.filter(getattr(SkpdModel, "kode_skpd") == param)
            skpd = skpd.one()
            return skpd
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def skpd_sub(param):
        ''' Doc: function skpd sub  '''
        try:
            skpdsub = db.session.query(SkpdSubModel)
            skpdsub = skpdsub.options(defer("cuid"), defer("cdate"))
            skpdsub = skpdsub.filter(getattr(SkpdSubModel, "kode_skpdsub") == param)
            skpdsub = skpdsub.one()
            return skpdsub
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def skpd_unit(paramskpd, paramsub, paramunit):
        ''' Doc: function skpdunit  '''
        try:
            skpdunit = db.session.query(SkpdUnitModel)
            skpdunit = skpdunit.options(defer("cuid"), defer("cdate"))
            skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpd") == paramskpd)
            skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdsub") == paramsub)
            skpdunit = skpdunit.filter(getattr(SkpdUnitModel, "kode_skpdunit") == paramunit)
            skpdunit = skpdunit.one()
            return skpdunit
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def debugQuery(result):
        ''' Doc: function debugQuery sql  '''
        try:
            return print(str(result.statement.compile(compile_kwargs={"literal_binds": True})))
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)
