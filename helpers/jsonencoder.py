''' Doc: helper jsonencoder '''
import json
import datetime
from bson import ObjectId
# from uuid import UUID
class JSONEncoder(json.JSONEncoder):
    ''' Doc: class jsonencoder '''

    def default(self, o):
        ''' Doc: function default '''
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return o.strftime("%Y-%m-%d %H:%M:%S")
        # if isinstance(o, UUID):
        #     return o.hex
        return json.JSONEncoder.default(self, o)
