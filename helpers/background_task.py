import threading

class BackgroundTasks:
    def __init__(self):
        self.tasks = []
    
    def add_task(self, task_func, *args, **kwargs):
        self.tasks.append((task_func, args, kwargs))
    
    def run_tasks(self):
        def run_all_tasks():
            for task_func, args, kwargs in self.tasks:
                try:
                    task_func(*args, **kwargs)
                except Exception as e:
                    print(f"Error in background task: {e}")
        
        thread = threading.Thread(target=run_all_tasks)
        thread.daemon = True
        thread.start()
