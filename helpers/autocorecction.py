''' Doc: setting autocorrection'''
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import textdistance as textdistance

import os as os
import sentry_sdk
from dotenv import load_dotenv, find_dotenv
from settings.custom_session import session, engine
from sqlalchemy import text
# pylint: disable=invalid-envvar-default
# Load environment
load_dotenv(find_dotenv())
POSTGRE_HOST = os.getenv("POSTGRE_HOST")
POSTGRE_PORT = os.getenv("POSTGRE_PORT")
POSTGRE_DB = os.getenv("POSTGRE_DB")
POSTGRE_USER = os.getenv("POSTGRE_USER")
POSTGRE_PASSWORD = os.getenv("POSTGRE_PASSWORD")

conn = engine.raw_connection() 

 
SEKTORAL_SQL = pd.read_sql_query("SELECT name FROM sektoral;", con=conn)


INFOGRAFIC_SQL = pd.read_sql_query("SELECT name, sektoral_name FROM suggestion_infographic", con=conn)


SKPD_SQL = pd.read_sql_query("SELECT nama_skpd FROM skpd WHERE regional_id = 1;", con=conn)


INDICATOR_SQL = pd.read_sql_query("SELECT name FROM indikator;", con=conn)


VISUALIZATION_SQL = pd.read_sql_query("SELECT name, sektoral_name, nama_skpd FROM suggestion_visualization", con=conn)


DATASET_SQL = pd.read_sql_query("SELECT name, sektoral_name, nama_skpd FROM suggestion_dataset", con=conn)


ARTICLE_SQL = pd.read_sql_query("SELECT name, topic FROM suggestion_article", con=conn)


class AutoCorecction(object):
    ''' Doc: Class Autocorecction Helper'''

    @staticmethod
    def logic_corecction(list_total, input_word):
        ''' Doc: logic correction word'''
        try:
            name = pd.DataFrame(list_total, columns=["name"])
            count_vec = CountVectorizer(ngram_range=(1, 5), stop_words=[ "dinas", "yang", "dan", "atau", "di", "per", "berdasarkan"])
            count_data = count_vec.fit_transform(name['name'])
            list_word_pandas = pd.DataFrame(count_data.toarray(), columns=count_vec.get_feature_names_out())
            list_word = list(list_word_pandas.columns)

            input_word = input_word.lower()
            similarities = [1-(textdistance.Jaccard(qval=2).distance(v, input_word)) for v in list_word]
            data_frame = pd.DataFrame({'word': list_word})
            data_frame['Similarity'] = similarities
            output = data_frame.sort_values(['Similarity'], ascending=False).head(1)
            list_similartities = list(output['Similarity'])
            list_word = list(output['word'])

            if (list_similartities[0] >= 0.6):
                return list_word[0]
            else:
                return input_word
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def dataset(input_word):
        ''' Doc: function autocorecction dataset  '''
        try:
            global DATASET_SQL
            # Source kata dan kalimat untuk Autocorrect
            list_dataset = list(DATASET_SQL['name'])
            list_topic = list(DATASET_SQL['sektoral_name'].unique())
            list_organization = list(DATASET_SQL['nama_skpd'].unique())
            list_total = list_dataset+list_topic+list_organization

            output = AutoCorecction.logic_corecction(list_total, input_word)

            if output == input_word:
                condition = False
            else:
                condition = True

            return condition, output
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def visualization(input_word):
        ''' Doc: function autocorecction visualization  '''
        try:
            global VISUALIZATION_SQL
            # Source kata dan kalimat untuk Autocorrect
            list_visualizations = list(VISUALIZATION_SQL['name'])
            list_topik = list(VISUALIZATION_SQL['sektoral_name'].unique())
            list_organization = list(VISUALIZATION_SQL['nama_skpd'].unique())
            list_total = list_visualizations+list_topik+list_organization

            output = AutoCorecction.logic_corecction(list_total, input_word)

            if output == input_word:
                condition = False
            else:
                condition = True

            return condition, output
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def infografic(input_word):
        ''' Doc: function autocorecction indicator  '''
        try:
            global INFOGRAFIC_SQL
            # Source kata dan kalimat untuk Autocorrect
            list_infografic = list(INFOGRAFIC_SQL['name'])
            list_topik = list(INFOGRAFIC_SQL['sektoral_name'].unique())
            list_total = list_infografic+list_topik
            output = AutoCorecction.logic_corecction(list_total, input_word)

            if output == input_word:
                condition = False
            else:
                condition = True

            return condition, output
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def skpd(input_word):
        ''' Doc: function autocorecction indicator  '''
        try:
            global SKPD_SQL
            # Source kata dan kalimat untuk Autocorrect
            list_skpd = list(SKPD_SQL['nama_skpd'])

            output = AutoCorecction.logic_corecction(list_skpd, input_word)

            if output == input_word:
                condition = False
            else:
                condition = True

            return condition, output
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def indicator(input_word):
        ''' Doc: function autocorecction indicator  '''
        try:
            global INDICATOR_SQL
            # Source kata dan kalimat untuk Autocorrect
            list_indicator = list(INDICATOR_SQL['name'])

            output = AutoCorecction.logic_corecction(list_indicator, input_word)

            if output == input_word:
                condition = False
            else:
                condition = True

            return condition, output
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)

    @staticmethod
    def article(input_word):
        ''' Doc: function autocorecction article  '''
        try:
            global ARTICLE_SQL
            # Source kata dan kalimat untuk Autocorrect
            list_article = list(ARTICLE_SQL['name'])
            list_topic = list(ARTICLE_SQL['topic'].unique())
            list_total = list_article+list_topic

            output = AutoCorecction.logic_corecction(list_total, input_word)

            if output == input_word:
                condition = False
            else:
                condition = True

            return condition, output
        except Exception as err:
            sentry_sdk.capture_exception(err)
            return str(err)
