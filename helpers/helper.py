''' Doc: helper  '''
import time
import os
import datetime
import pytz
import json
import jwt
import csv
import math
from passlib.hash import pbkdf2_sha256
from pytz import timezone
from settings import configuration
from flask import request
from models import AppModel
from models import AppServiceModel
from models import UserPermissionModel
from helpers.postgre_alchemy import postgre_alchemy as db
import pandas as pd
from pandas import ExcelWriter
from flask_mail import Message, Mail
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Paragraph, TableStyle, Table, SimpleDocTemplate
from reportlab.platypus.flowables import Spacer
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from sqlalchemy import inspect

# pylint: disable=broad-except, line-too-long, unused-argument, unused-wildcard-import, redefined-builtin
class Helper(object):
    ''' Doc: class helper  '''

    def date_to_timestamp(self, date: str):
        ''' Doc: function date to timestamp '''

        timestamp = time.mktime(datetime.datetime.strptime(
            date, "%Y-%m-%dT%H:%M:%S%z").timetuple())

        return int(timestamp)

    @staticmethod
    def generate_hash(password):
        ''' Doc: function generate hash '''
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify_hash(password, _hash):
        ''' Doc: function verify hash '''
        return pbkdf2_sha256.verify(password, _hash)

    @staticmethod
    def jwt_encode(payload: dict):
        ''' Doc: function jwt encode '''
        payload["iat"] = datetime.datetime.utcnow()
        payload["exp"] = datetime.datetime.utcnow() + \
            datetime.timedelta(days=1)
        return jwt.encode(payload, configuration.JWT_SECRET_KEY, algorithm=configuration.JWT_ALGORITHM).decode("utf-8")

    @staticmethod
    def read_jwt():
        ''' Doc: function read jwt '''
        try:
            token = request.headers.get('Authorization', None)
            tokens = token.split(" ")
            payload = jwt.decode(tokens[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])
            return payload
        except Exception:
            return {}
        
    

    @staticmethod
    def can_access_dataset(app, app_service):
        ''' Doc: function can access dataset '''
        token = request.headers.get('Authorization', None)
        if token:
            token_temp = token.split(" ")
            payload = jwt.decode(token_temp[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])

            # classification publik
            if((app_service['dataset_class']['name'].lower() == 'publik') or (app_service['dataset_class']['id'] == 3)):
                response = {"message": "Access Granted, Data is public",
                            "error": 0, "data": {}}
                return True, response

            # classification internal
            elif((app_service['dataset_class']['name'].lower() == 'internal') or (app_service['dataset_class']['id'] == 5)):
                # user is dinas
                if not payload['is_external']:
                    response = {"message": "Access Granted, Data is internal",
                                "error": 0, "data": {}}
                    return True, response
                # user is external
                else:
                    return Helper.check_permission(app['code'], app_service['controller'], app_service['action'], payload)

            # classification dikecualikan
            elif((app_service['dataset_class']['name'].lower() == 'dikecualikan') or (app_service['dataset_class']['id'] == 4)):
                return Helper.check_permission(app['code'], app_service['controller'], app_service['action'], payload)

            # not found
            else:
                return Helper.check_permission(app['code'], app_service['controller'], app_service['action'], payload)

        else:
            return False, {'data': {}, 'error': 1, 'message': 'Only Public'}

    @staticmethod
    def check_permission(app_code, controller, action, payload):
        ''' Doc: function check permission '''
        try:
            res, roles = Helper.permission_by_userid(payload["id"], app_code, controller, action)

            # jika ada
            if res:
                # jika boleh
                if roles[0]["enable"]:
                    # url yg diakses sesuai dengan rolenya
                    response = {"message": "Access Granted",
                                "error": 0, "data": {}}
                    return True, response

                else:
                    # url yg diakses tidak sesuai dengan rolenya
                    response = {"message": "Access Forbidden",
                                "error": 1, "data": {}}
                    return False, response

            else:
                # url yg diakses tidak sesuai dengan rolenya
                response = {"message": "URL App, Controller, or Action not found",
                            "error": 1, "data": {}}
                return False, response

        except Exception:
            # id tidak ditemukan
            response = {"message": "Failed get permission",
                        "error": 1, "data": {}}
            return False, response

    @staticmethod
    def permission_by_userid(_id: str, app_code: str, controller: str, action: str):
        ''' Doc: function permission by userid '''
        try:
            # query postgresql
            result = db.session.query(
                UserPermissionModel.id, UserPermissionModel.user_id, UserPermissionModel.app_id,
                AppModel.name, AppModel.url, AppModel.code.label('app_code'),
                UserPermissionModel.app_service_id, AppServiceModel.controller, AppServiceModel.action,
                UserPermissionModel.enable, UserPermissionModel.notes, UserPermissionModel.cdate, UserPermissionModel.cuid)
            result = result.join(AppModel, UserPermissionModel.app_id == AppModel.id)
            result = result.join(AppServiceModel, UserPermissionModel.app_service_id == AppServiceModel.id)
            result = result.filter(UserPermissionModel.user_id == str(_id))
            result = result.filter(AppModel.code == str(app_code))
            result = result.filter(AppServiceModel.controller == str(controller))
            result = result.filter(AppServiceModel.action == str(action))
            result = result.all()

            permission = []
            for res in result:
                temp = {}
                temp['id'] = res[0]
                temp['user_id'] = res[1]
                temp['app_id'] = res[2]
                temp['name'] = res[3]
                temp['url'] = res[4]
                temp['app_code'] = res[5]
                temp['app_service_id'] = res[6]
                temp['controller'] = res[7]
                temp['action'] = res[8]
                temp['enable'] = res[9]
                temp['notes'] = res[10]
                temp['cdate'] = res[11]
                temp['cuid'] = res[12]
                permission.append(temp)
            # print(permission)

            # check if empty
            if permission:
                return True, permission
            else:
                return False, {}

        except Exception as err:
            # fail response
            print(err)

    @staticmethod
    def convert_xls(filename, data, columns_titles):
        ''' Doc: function convert xls '''
        try:
            df_ = pd.DataFrame(data)
            df_ = df_.reindex(columns=columns_titles)
            writer = ExcelWriter(filename) # pylint: disable=abstract-class-instantiated
            df_.to_excel(writer, 'data', index=False)
            writer.save()
        except Exception as err:
            print("I/O error", err)

        return filename

    @staticmethod
    def convert_csv(filename, data, metadata):
        ''' Doc: function convert csv '''
        try:
            with open(filename, 'w', newline='', encoding="utf8") as fl_:
                writer = csv.DictWriter(fl_, fieldnames=metadata)
                writer.writeheader()
                writer.writerows(data)
        except Exception as err:
            print("I/O error", err)

        return filename


    @staticmethod
    def convert_pdf(filename):
        ''' doc string '''

        # alternatif pdf lib
        # https://pyfpdf.readthedocs.io/en/latest/reference/write_html/index.html

        # Data from CSV
        with open(filename, "r") as csvfile:
            data = list(csv.reader(csvfile))

        elements = []

        # PDF Text
        # PDF Text - Styles
        styles = getSampleStyleSheet()
        style_normal = styles['Normal']

        # PDF Text - Content
        name = filename.split('/')
        name = name[2]
        names = name.replace('-', ' - ').replace('od_', '').replace('_', ' ').replace('.csv', '')
        names = names.upper()
        line1 = 'DATA - ' + names
        line2 = 'Tanggal Cetak {}'.format(datetime.datetime.now().strftime("%d-%m-%Y"))
        line3 = 'Diunduh dari http://data.jabarprov.go.id'
        elements.append(Paragraph(line1, style_normal))
        elements.append(Paragraph(line2, style_normal))
        elements.append(Paragraph(line3, style_normal))
        elements.append(Spacer(inch, .25 * inch))

        # PDF Table
        # PDF Table - Styles
        table_style = TableStyle([
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
            ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
        ])

        # Add table to elements
        tbl = Table(data, style=table_style, hAlign='LEFT')
        elements.append(tbl)

        # Generate PDF
        archivo_pdf = SimpleDocTemplate(
            filename[:-3]+'pdf',
            pagesize=letter,
            rightMargin=40,
            leftMargin=40,
            topMargin=40,
            bottomMargin=28,)
        archivo_pdf.build(elements)

        return filename[:-3]+'pdf'

    @staticmethod
    def convert_pdf_metadata(filename, data):
        ''' doc string '''

        elements = []
        styles_normal = ParagraphStyle(
            'Normal',
            fontName='Helvetica',
            fontSize=9
        )
        styles_bold = ParagraphStyle(
            'Bold',
            fontName='Helvetica-Bold',
            fontSize=11
        )

        for row in data:
            if row['value'] is not None:
                msg_str = row['value']
                msg_str = msg_str.replace(' ', '&nbsp;')
                msg_str = msg_str.replace('\n', '<br />')
                msg_str = msg_str.replace('\t', '&nbsp;&nbsp;&nbsp;&nbsp;')
            else:
                msg_str = ''
            elements.append(Paragraph(row['key'], styles_bold))
            elements.append(Spacer(inch, .05 * inch))
            elements.append(Paragraph(msg_str, styles_normal))
            elements.append(Spacer(inch, .25 * inch))

        archivo_pdf = SimpleDocTemplate(
            filename,
            pagesize=letter,
            rightMargin=40,
            leftMargin=40,
            topMargin=40,
            bottomMargin=28,)
        archivo_pdf.build(elements)

        return filename

    @staticmethod
    def local_date():
        ''' Doc: function local date '''
        try:
            server_time = (datetime.datetime.utcnow() + datetime.timedelta(minutes=18, seconds=34)).replace(tzinfo=pytz.utc).astimezone(pytz.timezone("Asia/Jakarta")).strftime('%Y-%m-%d %H:%M:%S')
            return server_time

        except Exception as err:
            print(err)

    @staticmethod
    def local_date_server():
        ''' Doc: function local date '''
        try:
            current_time = datetime.datetime.now(timezone('Asia/Jakarta'))
            current_time_utc = current_time.strftime('%Y-%m-%d %H:%M:%S')
            # current_time_utc = current_time.astimezone(timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%I.000Z')
            return current_time_utc
        except Exception as err:
            print(err)


    @staticmethod
    def sort_list(lst):
        ''' Doc: function sortlist '''
        try:
            lst = [str(i) for i in lst]
            lst.sort()
            lst = [int(i) if i.isdigit() else i for i in lst]
            return lst
        except Exception as err:
            print(err)

    @staticmethod
    def send_email(subject, recipients=dict, html=None):
        ''' Doc: function send email '''
        mail = Mail()
        msg = Message()
        try:

            msg.subject = subject
            msg.sender = os.getenv("MAIL_SENDER")
            msg.html = html
            msg.recipients = recipients
            return mail.send(msg)

        except Exception as err:
            # fail response
            print(err)

    @staticmethod
    def _meta(count:int, skip:int, limit:int):
        try:
            meta = {}
            meta['skip'] = int(skip)
            meta['limit'] = int(limit)
            meta['total_record'] = int(count)
            if int(limit) > 0:
                meta['total_page'] = math.ceil(int(count) / int(limit))
            else:
                meta['total_page'] = 0
            return meta
        except Exception as err:
            return print(err)


    @staticmethod
    def serialize(instance):
        """
        Serialize a single SQLAlchemy model instance into a dictionary

        Args:
            instance: SQLAlchemy model instance
        Returns:
            dict containing model data
        """
        return {column.key: getattr(instance, column.key)
                for column in inspect(instance).mapper.column_attrs}