''' Doc: helper jwt manager '''
from flask_jwt_extended import (JWTManager)
from settings import configuration
from flask import request
import jwt



# pylint: disable=invalid-name
jwtmanager = JWTManager()

class JwtManager:
    
    @staticmethod
    def decode_jwt():
        ''' Doc: function read jwt '''
        try:
            token = request.headers.get('Authorization', None)
            tokens = token.split(" ")
            payload = jwt.decode(tokens[1], configuration.JWT_SECRET_KEY, algorithm=[configuration.JWT_ALGORITHM])
            return payload
        except Exception:
            return {}
        
    @classmethod
    def get_user_id(cls):
        ''' Doc: function get user id '''
        try:
            payload = cls.decode_jwt()
            return payload['id']
        except Exception:            
            return None