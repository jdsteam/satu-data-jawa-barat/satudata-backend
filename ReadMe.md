# Satudata-API (using python)



## About

This is api service for satudata app



## Installation

If you are using **windows**,

First include the python to venv

```
py -3 -m venv venv
```

And activate the venv

```
venv\Scripts\activate
pip install -r requirement.txt
```



If you are using **linux**,

First include the python to venv

```
virtualenv -p python3.5 venv --no-site-packages
```

And activate the venv

```
source venv/bin/activate
pip install -r requirement.txt
```



Open .env file than change the config environment

``` 
POSTGRE_DB=
JWT_SECRET_KEY=
PORT=
DEBUG=
```



# Usage

Run the script

```
python run.py
```

You can see which port the api server running
