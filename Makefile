
FLASK_APP := run.py
FLASK_ENV := development
FLASK_DEBUG :=1

export FLASK_APP
export FLASK_ENV
export FLASK_DEBUG

watch:
	flask run --port 5002
