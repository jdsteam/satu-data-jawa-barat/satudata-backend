''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
from passlib.hash import pbkdf2_sha256
from requests.auth import HTTPBasicAuth
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# conn digiteam
conn_digiteam = psycopg2.connect(
    host='',
    user='',
    password='',
    port='5432',
    dbname='authserver',
    options='-c search_path=public',
)
cursor_digiteam = conn_digiteam.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

conn_satudata = psycopg2.connect(
    host='',
    user='',
    password='',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
cursor_satudata = conn_satudata.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

def get_data():
    try:
        query = "SELECT * from accounts_account where is_active = true AND manager_category = 'Tim Pengelola Layanan Digital' ORDER BY divisi"
        cursor_digiteam.execute(query)
        results = cursor_digiteam.fetchall()
        for res in results:
            # print(res)
            print(res['email'])
            check_data_exist(res)
    except Exception as err:
        print(err)
        print()

def check_data_exist(data):
    try:
        query = f"SELECT * from users where email = '{data['email']}'"
        cursor_satudata.execute(query)
        results = cursor_satudata.fetchall()
        if len(results) > 0:
            print('data exist, not insert')
            return True
        else:
            print('data not exist, insert')
            insert_data(data)
            return False
    except Exception as err:
        print(err)
        print()
        return False

def insert_data(data):
    try:
        query = "insert into users ("
        query += "username, email, nip, password, name, profile_pic, "
        query += "kode_kemendagri, kode_skpd, kode_skpdsub, "
        query += "jabatan_id, satuan_kerja_id, lv1_unit_kerja_id, "
        query += "role_id, dash_role_id, "
        query += "cdate, cuid, is_active, is_deleted, notes"
        query += ") values ("
        query += "%s, %s, %s, %s, %s, %s, "
        query += "'32', '1.16.00.01', '1.16.00.01.00.00.01', "
        query += "'100041383', '1022', '102210000000', "
        query += "24, 9, "
        query += "'2024-01-16 10:30:00', 1, True, False, 'Generate digiteam'"
        query += ")"
        value = (
            data['username'], data['email'], data['email'], pbkdf2_sha256.hash(data['username']), data['first_name'] + ' ' + data['last_name'], data['photo']
        )
        print(query)
        print(value)
        cursor_satudata.execute(query, value)
        conn_satudata.commit()
        print()
    except Exception as err:
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
