''' Doc '''
import time
import psycopg2
import psycopg2.extras
import requests
import urllib3
import urllib
import json
from requests.auth import HTTPBasicAuth
from psycopg2.extras import DictCursor
from psycopg2.extras import RealDictCursor
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# conn
connection = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
cursor = connection.cursor(cursor_factory=RealDictCursor)

def get_data():
    ''' Doc '''
    try:
        query = '''
            SELECT mapset.id, mapset.name, mapset_source.name as mapset_source, mapset.app_link, (select count(*) from mapset_metadata where mapset_metadata.mapset_id = mapset.id)
            FROM mapset
            JOIN mapset_source on mapset_source.id = mapset.mapset_source_id
            ORDER BY mapset.id asc
        '''
        cursor.execute(query)
        result = cursor.fetchall()
        # print(result)

        for res in result:
            if (res['count'] == 0):
                print()
                print(res['id'])
                print(res['mapset_source'])
                print(res['app_link'])
                metadata = []
                if res['mapset_source'] == 'Arc GIS':
                    metadata = get_arcgis(res['app_link'])
                elif res['mapset_source'] == 'Geo Server':
                    metadata = get_geojson(res['app_link'])
                # print(metadata)

                if metadata:
                    metadata_kugi = get_metadata_kugi(metadata)
                    print(metadata_kugi)
                    for mk in metadata_kugi:
                        insert_data(res['id'], mk)


    except Exception as err:
        print(err)
        print()

def get_metadata_kugi(metadata):
    try:
        metadata_kugi = []
        for md in metadata:
            # parsing
            attr = ''
            value_arcgis = ''

            for key, value in md.items():
                if key == 'name':
                    attr = value
                if key == 'alias':
                    value_arcgis = value

            # get metadata kugi
            query = f'SELECT * FROM mapset_metadata_kugi WHERE "ptMemberName" = \'{attr.upper()}\''
            # print(query)
            cursor.execute(query)
            result = cursor.fetchall()
            # print(result)

            if result:
                for res in result:
                    # print(res)
                    metadata_kugi.append({'key': attr, 'value': res['ptDefinition'], 'value_arcgis': value_arcgis})
            else:
                metadata_kugi.append({'key': attr, 'value': '', 'value_arcgis': value_arcgis})
        return metadata_kugi

    except Exception as err:
        print(err)

def get_arcgis(url):
    try:
        if '?f=pjson' in url:
            url = url
        else:
            url = url + '?f=pjson'
        req = requests.get(url, headers={'Content-Type': 'application/json'})
        res = req.json()
        if res:
            if 'fields' in res:
                metadata = res['fields']
                return metadata
            else :
                return []
        else:
            return []

    except Exception as err:
        print(err)
        print()
        return []

def get_geojson(url):
    try:
        url_parsed = urllib.parse.urlparse(url)
        url_only = url_parsed._replace(query=None).geturl()
        url_only_split = url_only.split('/')
        params = urllib.parse.parse_qs(url_parsed.query)

        urls = '/'.join(url_only_split[:-1]) + '/ows' + '?service=WFS' + '&version=1.0.0' + '&request=GetFeature'
        urls += '&typeName=' + params['layers'][0] + '&outputFormat=application/json'

        req = requests.get(urls, headers={'Content-Type': 'application/json'})
        res = req.json()
        if res:
            metadata = res['features'][0]['properties']
            return metadata
        else:
            return []

    except Exception as err:
        print(err)
        print()
        return []

def insert_data(id, data):
    ''' Doc '''
    try:
        query = "insert into mapset_metadata ("
        query += "mapset_metadata_type_id, mapset_id, key, value"
        query += ") values ("
        query += "%s, %s, %s, %s "
        query += ")"
        value = (
            6, id, data['key'], data['value']
        )
        # print(query)
        # print(value)
        cursor.execute(query, value)
        connection.commit()
    except Exception as err:
        print(err)
        print()


if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
