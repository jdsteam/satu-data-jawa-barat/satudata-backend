''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
# conn
CONNECTION = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
CURSOR = CONNECTION.cursor()
# conn 2
CONNECTION2 = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='bigdata',
    options='-c search_path=public',
)
CURSOR2 = CONNECTION2.cursor()

def insert_dataset_permission(dataset_id, kode_skpd):
    ''' Doc '''
    try:
        # select skpd_id
        query_skpd = "SELECT id FROM skpd where kode_skpd = '"+ str(kode_skpd) +"' order by id desc limit 1"
        CURSOR.execute(query_skpd)
        results_skpd = CURSOR.fetchall()
        for res_skpd in results_skpd:

            try:
                # select jabatan_id
                query_jabatan = "SELECT jabatan_id FROM struktur_organisasi where satuan_kerja_id = '"+ str(res_skpd[0]) +"'"
                CURSOR.execute(query_jabatan)
                results_jabatan = CURSOR.fetchall()
                for res_jabatan in results_jabatan:

                    try:
                        # insert dataset permission
                        query_insert = "insert into dataset_permission (dataset_id, jabatan_id, enable) "
                        query_insert += "values (%s, %s, %s)"
                        values_insert = (dataset_id, res_jabatan[0], True)
                        CURSOR.execute(query_insert, values_insert)
                        CONNECTION.commit()
                        print()
                    except Exception as err:
                        print('update dataset')
                        print(err)

            except Exception as err:
                print('update dataset')
                print(err)

    except Exception as err:
        print('get app_json')
        print(err)

def get_dataset():
    ''' Doc '''
    try:
        query = '''SELECT id, kode_skpd from dataset where dataset_class_id=4'''
        CURSOR.execute(query)
        results = CURSOR.fetchall()

        for res in results:
            print(res[0])
            insert_dataset_permission(res[0], res[1])
    except Exception as err:
        print('get dataset')
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_dataset()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
