''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
# conn
CONNECTION = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
CURSOR = CONNECTION.cursor()
# conn 2
CONNECTION2 = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='bigdata',
    options='-c search_path=public',
)
CURSOR2 = CONNECTION2.cursor()

def update_schema_table(dataset_id):
    ''' Doc '''
    try:
        query_user = "SELECT * FROM app_json WHERE dataset_id = "+ str(dataset_id) +" order by id desc limit 1"
        CURSOR2.execute(query_user)
        results_user = CURSOR2.fetchall()

        for res_us in results_user:

            # update unit kerja
            try:
                query_update = "update dataset set schema=%s, \"table\"=%s where id=%s "
                values_update = (res_us[5], res_us[6], dataset_id)
                CURSOR.execute(query_update, values_update)
                CONNECTION.commit()
                print()
            except Exception as err:
                print('update dataset')
                print(err)

    except Exception as err:
        print('get app_json')
        print(err)

def get_dataset():
    ''' Doc '''
    try:
        query = '''SELECT id, name from dataset '''
        CURSOR.execute(query)
        results = CURSOR.fetchall()

        for res in results:
            print(res[0])
            update_schema_table(res[0])
    except Exception as err:
        print('get dataset')
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_dataset()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
