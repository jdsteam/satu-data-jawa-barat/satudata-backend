''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
# conn
CONNECTION = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
CURSOR = CONNECTION.cursor()
# conn 2
CONNECTION2 = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='bigdata',
    options='-c search_path=public',
)
CURSOR2 = CONNECTION2.cursor()

def update_json(dataset_id):
    ''' Doc '''
    try:
        # select json
        query_json = "select json_path from app_json where dataset_id='"+ str(dataset_id) +"' order by id desc limit 1"
        print(query_json)
        CURSOR2.execute(query_json)
        results_json = CURSOR2.fetchall()
        for res_json in results_json:
            print(res_json)

            try:
                # insert dataset permission
                query_insert = "update dataset set json=%s where id='%s'"
                values_insert = (res_json[0], dataset_id)
                print(res_json)
                print(values_insert)
                CURSOR.execute(query_insert, values_insert)
                CONNECTION.commit()
                print()
            except Exception as err:
                print('update dataset')
                print(err)

    except Exception as err:
        print('get app_json')
        print(err)

def get_dataset():
    ''' Doc '''
    try:
        query = '''SELECT id from dataset '''
        CURSOR.execute(query)
        results = CURSOR.fetchall()

        for res in results:
            print(res[0])
            update_json(res[0])
    except Exception as err:
        print('get dataset')
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_dataset()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
