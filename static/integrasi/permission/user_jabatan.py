''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
# conn
CONNECTION = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
CURSOR = CONNECTION.cursor()

def update_jabatan(nip):
    ''' Doc '''
    try:
        query_user = '''
            SELECT
            bkd_pegawai.peg_nip,
            bkd_pegawai.peg_nama,
            struktur_organisasi.*
            FROM bkd_pegawai
            INNER JOIN struktur_organisasi ON bkd_pegawai.jabatan_id = struktur_organisasi.jabatan_id
        '''
        query_user += "WHERE peg_nip ='"+ nip +"'"

        CURSOR.execute(query_user)
        results_user = CURSOR.fetchall()

        for res_us in results_user:

            # update unit kerja
            try:
                query_update = "update users set satuan_kerja_id=%s, lv1_unit_kerja_id=%s, lv2_unit_kerja_id=%s, lv3_unit_kerja_id=%s, lv4_unit_kerja_id=%s, level_unit_kerja=%s, jabatan_id=%s where nip=%s "
                values_update = (res_us[3], res_us[5], res_us[7], res_us[9], res_us[11], res_us[13], res_us[15], nip)
                print(query_update)
                print(values_update)
                CURSOR.execute(query_update, values_update)
                CONNECTION.commit()
                print()
            except Exception as err:
                print('update_jabatan')
                print(err)

    except Exception as err:
        print('get struktur organisasi')
        print(err)

def get_data():
    ''' Doc '''
    try:
        query = '''SELECT nip from users where nip is not null and nip !='' '''
        CURSOR.execute(query)
        results = CURSOR.fetchall()

        for res in results:
            print(res[0])
            update_jabatan(res[0])
    except Exception as err:
        print('get data')
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
