''' Doc '''
import time
import psycopg2
import psycopg2.extras
import requests
import urllib3
from requests.auth import HTTPBasicAuth
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
def insert_data(data):
    ''' Doc '''
    # conn
    connection = psycopg2.connect(
        host='localhost',
        user='postgres',
        password='password',
        port='5432',
        dbname='satudata',
        options='-c search_path=public',
    )
    cursor = connection.cursor()

    try:
        query = "insert into bkd_struktur_organisasi ("
        query += "id, lv0_jabatan_id, lv0_jabatan_nama, lv0_eselon_nm, "
        query += "lv1_unit_kerja_id, lv1_unit_kerja_nama, lv1_jabatan_id, lv1_jabatan_nama, lv1_eselon_nm, "
        query += "lv2_unit_kerja_id, lv2_unit_kerja_nama, lv2_jabatan_id, lv2_jabatan_nama, lv2_eselon_nm, "
        query += "lv3_unit_kerja_id, lv3_unit_kerja_nama, lv3_jabatan_id, lv3_jabatan_nama, lv3_eselon_nm, "
        query += "lv4_unit_kerja_id, lv4_unit_kerja_nama, lv4_jabatan_id, lv4_jabatan_nama, lv4_eselon_nm, "
        query += "lv5_unit_kerja_id, lv5_unit_kerja_nama, lv5_jabatan_id, lv5_jabatan_nama, lv5_eselon_nm, "
        query += "lv6_unit_kerja_id, lv6_unit_kerja_nama, lv6_jabatan_id, lv6_jabatan_nama, lv6_eselon_nm, "
        query += "lv7_unit_kerja_id, lv7_unit_kerja_nama, lv7_jabatan_id, lv7_jabatan_nama, lv7_eselon_nm, "
        query += "level, parent_id, jabatan_id, jabatan_nama, "
        query += "unit_kerja_id_baru, unit_kerja_kode, unit_kerja_nama, kode_unit_kerja, "
        query += "satuan_kerja_id, satuan_kerja_nama, eselon_id, eselon_nm, "
        query += "gol_id_minimum, alamat, latitude, longitude, m_kabkot_id, nama_kota"
        query += ") values ("
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s"
        query += ")"
        value = (
            data['id'], data['lv0_jabatan_id'], data['lv0_jabatan_nama'], data['lv0_eselon_nm'],
            data['lv1_unit_kerja_id'], data['lv1_unit_kerja_nama'], data['lv1_jabatan_id'], data['lv1_jabatan_nama'], data['lv1_eselon_nm'],
            data['lv2_unit_kerja_id'], data['lv2_unit_kerja_nama'], data['lv2_jabatan_id'], data['lv2_jabatan_nama'], data['lv2_eselon_nm'],
            data['lv3_unit_kerja_id'], data['lv3_unit_kerja_nama'], data['lv3_jabatan_id'], data['lv3_jabatan_nama'], data['lv3_eselon_nm'],
            data['lv4_unit_kerja_id'], data['lv4_unit_kerja_nama'], data['lv4_jabatan_id'], data['lv4_jabatan_nama'], data['lv4_eselon_nm'],
            data['lv5_unit_kerja_id'], data['lv5_unit_kerja_nama'], data['lv5_jabatan_id'], data['lv5_jabatan_nama'], data['lv5_eselon_nm'],
            data['lv6_unit_kerja_id'], data['lv6_unit_kerja_nama'], data['lv6_jabatan_id'], data['lv6_jabatan_nama'], data['lv6_eselon_nm'],
            data['lv7_unit_kerja_id'], data['lv7_unit_kerja_nama'], data['lv7_jabatan_id'], data['lv7_jabatan_nama'], data['lv7_eselon_nm'],
            data['level'], data['parent_id'], data['jabatan_id'], data['jabatan_nama'],
            data['unit_kerja_id_baru'], data['unit_kerja_kode'], data['unit_kerja_nama'], data['kode_unit_kerja'],
            data['satuan_kerja_id'], data['satuan_kerja_nama'], data['eselon_id'], data['eselon_nm'],
            data['gol_id_minimum'], data['alamat'], data['latitude'], data['longitude'], data['m_kabkot_id'], data['nama_kota']
            )
        # print(query)
        # print(value)
        cursor.execute(query, value)
        connection.commit()
        print()
    except Exception as err:
        print(err)
        print()

def get_data(page, perpage):
    ''' Doc '''
    try:
        url = 'https://siap.jabarprov.go.id/integrasi/api/v1/struktur-organisasi?page='+str(page)+'&perpage='+str(perpage)
        req = requests.get(url, headers={"Content-Type": "application/json"}, verify=False, auth=HTTPBasicAuth('satudata', 'satudata2021'))
        response = req.json()
        if response:
            print("PAGE " + str(page))
            for data in response['data']:
                print(data)
                insert_data(data)

            if response['count'] > (page * perpage):
                get_data(page + 1, perpage)
    except Exception as err:
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data(1, 1000)

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
