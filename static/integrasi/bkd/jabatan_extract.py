''' Doc '''
import time
import psycopg2
import psycopg2.extras
import requests
import urllib3
from requests.auth import HTTPBasicAuth
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# conn
connection = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
cursor = connection.cursor()


def is_level1(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_count = "SELECT count(*) from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv1_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_count)
        result_count = cursor.fetchall()
        return result_count[0][0]

    except Exception as err:
        print(err)
        print()
        return 0

def is_level2(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_count = "SELECT count(*) from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv2_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_count)
        result_count = cursor.fetchall()
        return result_count[0][0]

    except Exception as err:
        print(err)
        print()
        return 0

def is_level3(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_count = "SELECT count(*) from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv3_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_count)
        result_count = cursor.fetchall()
        return result_count[0][0]

    except Exception as err:
        print(err)
        print()
        return 0

def is_level4(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_count = "SELECT count(*) from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv4_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_count)
        result_count = cursor.fetchall()
        return result_count[0][0]

    except Exception as err:
        print(err)
        print()
        return 0

def nama_level1(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_nama = "SELECT lv1_unit_kerja_id, lv1_unit_kerja_nama from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv1_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_nama)
        result_nama = cursor.fetchall()
        return result_nama[0][0], result_nama[0][1]

    except Exception as err:
        print(err)
        print()
        return "", ""

def nama_level2(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_nama = "SELECT lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv2_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_nama)
        result_nama = cursor.fetchall()
        return result_nama[0][0], result_nama[0][1], result_nama[0][2], result_nama[0][3]

    except Exception as err:
        print(err)
        print()
        return "", "", "", ""

def nama_level3(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_nama = "SELECT lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama, lv3_unit_kerja_id, lv3_unit_kerja_nama from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv3_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_nama)
        result_nama = cursor.fetchall()
        return result_nama[0][0], result_nama[0][1], result_nama[0][2], result_nama[0][3], result_nama[0][4], result_nama[0][5]

    except Exception as err:
        print(err)
        print()
        return "", "", "", "", "", ""

def nama_level4(satuan_kerja_id, unit_kerja_id):
    ''' Doc '''
    try:
        query_nama = "SELECT lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama, lv3_unit_kerja_id, lv3_unit_kerja_nama, lv4_unit_kerja_id, lv4_unit_kerja_nama from bkd_struktur_organisasi where satuan_kerja_id = '"+satuan_kerja_id+"' and lv4_unit_kerja_id = '"+unit_kerja_id+"'"
        cursor.execute(query_nama)
        result_nama = cursor.fetchall()
        return result_nama[0][0], result_nama[0][1], result_nama[0][2], result_nama[0][3], result_nama[0][4], result_nama[0][5], result_nama[0][6], result_nama[0][7]

    except Exception as err:
        print(err)
        print()
        return "", "", "", "", "", "", "", ""

def get_data():
    ''' Doc '''
    try:
        query_user = "SELECT * from bkd_jabatan"
        cursor.execute(query_user)
        result_jabatan = cursor.fetchall()

        for res_jab in result_jabatan:
            jabatan_id = res_jab[0]
            jabatan_nama = res_jab[1]
            satuan_kerja_id = res_jab[6]
            satuan_kerja_nama = res_jab[7]
            unit_kerja_id = res_jab[4]
            unit_kerja_nama = res_jab[5]

            level_unit_kerja = 0
            lv1_unit_kerja_id = ""
            lv1_unit_kerja_nama = ""
            lv2_unit_kerja_id = ""
            lv2_unit_kerja_nama = ""
            lv3_unit_kerja_id = ""
            lv3_unit_kerja_nama = ""
            lv4_unit_kerja_id = ""
            lv4_unit_kerja_nama = ""

            lv4 = is_level4(satuan_kerja_id, unit_kerja_id)
            if lv4 > 0:
                level_unit_kerja = 4
                lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama, lv3_unit_kerja_id, lv3_unit_kerja_nama, lv4_unit_kerja_id, lv4_unit_kerja_nama = nama_level4(satuan_kerja_id, unit_kerja_id)

            lv3 = is_level3(satuan_kerja_id, unit_kerja_id)
            if lv3 > 0:
                level_unit_kerja = 3
                lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama, lv3_unit_kerja_id, lv3_unit_kerja_nama = nama_level3(satuan_kerja_id, unit_kerja_id)

            lv2 = is_level2(satuan_kerja_id, unit_kerja_id)
            if lv2 > 0:
                level_unit_kerja = 2
                lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama = nama_level2(satuan_kerja_id, unit_kerja_id)

            lv1 = is_level1(satuan_kerja_id, unit_kerja_id)
            if lv1 > 0:
                level_unit_kerja = 1
                lv1_unit_kerja_id, lv1_unit_kerja_nama = nama_level1(satuan_kerja_id, unit_kerja_id)

            print(jabatan_id)
            print(jabatan_nama)
            print(satuan_kerja_id)
            print(satuan_kerja_nama)
            # print(unit_kerja_id)
            # print(unit_kerja_nama)
            print(level_unit_kerja)
            print(lv1_unit_kerja_id)
            print(lv1_unit_kerja_nama)
            print(lv2_unit_kerja_id)
            print(lv2_unit_kerja_nama)
            print(lv3_unit_kerja_id)
            print(lv3_unit_kerja_nama)
            print(lv4_unit_kerja_id)
            print(lv4_unit_kerja_nama)
            print()
            update_data(lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama, lv3_unit_kerja_id, lv3_unit_kerja_nama, lv4_unit_kerja_id, lv4_unit_kerja_nama, jabatan_id)

    except Exception as err:
        print(err)
        print()

def update_data(lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama, lv3_unit_kerja_id, lv3_unit_kerja_nama, lv4_unit_kerja_id, lv4_unit_kerja_nama, jabatan_id):
    ''' Doc '''
    try:
        query = "update bkd_view set "
        query += "lv1_unit_kerja_id=%s, lv1_unit_kerja_nama=%s, "
        query += "lv2_unit_kerja_id=%s, lv2_unit_kerja_nama=%s, "
        query += "lv3_unit_kerja_id=%s, lv3_unit_kerja_nama=%s, "
        query += "lv4_unit_kerja_id=%s, lv4_unit_kerja_nama=%s "
        query += "where jabatan_id=%s"
        value = (
            lv1_unit_kerja_id,
            lv1_unit_kerja_nama,
            lv2_unit_kerja_id,
            lv2_unit_kerja_nama,
            lv3_unit_kerja_id,
            lv3_unit_kerja_nama,
            lv4_unit_kerja_id,
            lv4_unit_kerja_nama,
            jabatan_id
        )
        # print(query)
        # print(value)
        cursor.execute(query, value)
        connection.commit()
        print()
    except Exception as err:
        print(err)
        print()


if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
