''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
from passlib.hash import pbkdf2_sha256
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
# conn
CONNECTION = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
CURSOR = CONNECTION.cursor()

def deactive_user():
    ''' Doc '''
    try:
        query = "update users set is_active = false, notes = null where notes is not null"
        # print(query)
        CURSOR.execute(query)
        CONNECTION.commit()
        print()
    except Exception as err:
        print(err)
        print()

def check_data(data):
    ''' Doc '''
    try:
        # print(data)
        print(data['nip'])
        query = "SELECT * from users where nip='"+ data['nip'] +"'"
        CURSOR.execute(query)
        results = CURSOR.fetchall()
        # print(results)
        if results:
            print("update data")
            update_data(data)
        else:
            print("insert data")
            insert_data(data)

        set_permission(data)

    except Exception as err:
        print(err)
        print()

def insert_data(data):
    ''' Doc '''
    try:
        query = "insert into users ("
        query += "username, email, password, nip, "
        query += "kode_kemendagri, kode_skpd, kode_skpdsub, kode_skpdunit, "
        query += "name, profile_pic, is_active, is_deleted, notes, "
        query += "cdate, cuid "
        query += ") values ("
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, "
        query += "%s, %s "
        query += ")"
        value = (
            data['username'],
            data['email'],
            data['password'],
            data['nip'],
            data['kode_kemendagri'],
            data['kode_skpd'],
            data['kode_skpdsub'],
            data['kode_skpdunit'],
            data['name'],
            data['profile_pic'],
            data['is_active'],
            data['is_deleted'],
            data['notes'],
            data['date'],
            data['uid']
        )
        CURSOR.execute(query, value)
        CONNECTION.commit()
        print()
    except Exception as err:
        print(err)
        print()

def update_data(data):
    ''' Doc '''

    try:
        query = "update users set "
        query += "username=%s, email=%s, nip=%s, "
        query += "kode_kemendagri=%s, kode_skpd=%s, kode_skpdsub=%s, kode_skpdunit=%s, "
        query += "name=%s, profile_pic=%s, is_active=%s, is_deleted=%s, notes=%s, "
        query += "mdate=%s, muid=%s where nip=%s"
        value = (
            data['username'],
            data['email'],
            data['nip'],
            data['kode_kemendagri'],
            data['kode_skpd'],
            data['kode_skpdsub'],
            data['kode_skpdunit'],
            data['name'],
            data['profile_pic'],
            data['is_active'],
            data['is_deleted'],
            data['notes'],
            data['date'],
            data['uid'],
            data['nip']
        )
        # print(query)
        # print(value)
        CURSOR.execute(query, value)
        CONNECTION.commit()
        print()
    except Exception as err:
        print(err)
        print()

def set_permission(data):
    ''' Doc '''
    try:
        query_user = "SELECT id from users where nip='"+ data['nip'] +"'"
        CURSOR.execute(query_user)
        results_user = CURSOR.fetchall()

        for res_us in results_user:

            # delete user permission
            try:
                query = "delete from users_permission where user_id=" + str(res_us[0])
                CURSOR.execute(query)
                CONNECTION.commit()
            except Exception as err:
                print(err)

            # get app_service
            try:
                query_app = 'select app_id, id as app_service_id from app_service where app_id = 1 or app_id = 2 order by app_id asc'
                CURSOR.execute(query_app)
                results_app = CURSOR.fetchall()

                for res_app in results_app:
                    temp = {}
                    temp['user_id'] = res_us[0]
                    temp['app_id'] = res_app[0]
                    temp['app_service_id'] = res_app[1]
                    temp['enable'] = True
                    temp['date'] = "2021-07-15 10:18:00"
                    temp['uid'] = 1
                    temp['notes'] = 'generate 20210715'

                    # insert user_permission
                    try:
                        query_insert = "insert into users_permission(user_id, app_id, app_service_id, enable, cdate, cuid, notes) "
                        query_insert += "values (%s, %s, %s, %s, %s, %s, %s)"
                        values_insert = (temp['user_id'], temp['app_id'], temp['app_service_id'], temp['enable'], temp['date'], temp['uid'], temp['notes'])
                        CURSOR.execute(query_insert, values_insert)
                        CONNECTION.commit()
                    except Exception as err:
                        print('insert user')
                        print(err)

            except Exception as err:
                print('get app_service')
                print(err)

    except Exception as err:
        print('get user')
        print(err)

def get_data():
    ''' Doc '''
    try:
        query = '''
            SELECT
	        struktur_organisasi.id as struktur_organisasi_id,
	        skpd.kode_skpd,
            skpd.nama_skpd,
            skpdsub.kode_skpdsub,
            skpdsub.nama_skpdsub,
            skpdunit.kode_skpdunit,
            skpdunit.nama_skpdunit,
            bkd_pegawai.peg_nip,
            bkd_pegawai.peg_nama,
            bkd_pegawai.peg_nama_lengkap,
            bkd_pegawai.peg_email,
            bkd_pegawai.peg_email_resmi,
            bkd_pegawai.peg_foto_url
            FROM struktur_organisasi
	        LEFT JOIN skpd ON struktur_organisasi.satuan_kerja_id::text = skpd."id"::text
	        LEFT JOIN skpdsub ON struktur_organisasi.lv1_unit_kerja_id::text = skpdsub."id"::text
	        LEFT JOIN skpdunit ON struktur_organisasi.lv2_unit_kerja_id::text = skpdunit."id"::text
	        LEFT JOIN bkd_pegawai ON struktur_organisasi.jabatan_id = bkd_pegawai.jabatan_id
            WHERE bkd_pegawai.peg_status = 'true' and  struktur_organisasi.satuan_kerja_id = '1018'
            ORDER BY struktur_organisasi_id asc
        '''
        CURSOR.execute(query)
        results = CURSOR.fetchall()

        for res in results:
            data = {}
            data['username'] = res[7]
            if res[11]:
                data['email'] = res[11]
            else:
                data['email'] = res[10]
            data['password'] = pbkdf2_sha256.hash(res[7][-5:])
            data['nip'] = res[7]
            data['kode_kemendagri'] = "32"
            data['kode_skpd'] = res[1]
            data['kode_skpdsub'] = res[3]
            data['kode_skpdunit'] = res[5]
            data['name'] = res[8]
            data['profile_pic'] = res[12]
            data['is_active'] = True
            data['is_deleted'] = False
            data['date'] = "2021-07-15 10:18:00"
            data['uid'] = 1
            data['notes'] = 'generate 20210715'
            # print(data)
            check_data(data)
    except Exception as err:
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    # deactive_user()
    get_data()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
