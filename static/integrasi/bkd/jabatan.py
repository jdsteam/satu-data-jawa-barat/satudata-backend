''' Doc '''
import time
import psycopg2
import psycopg2.extras
import requests
import urllib3
from requests.auth import HTTPBasicAuth
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement
def insert_data(data):
    ''' Doc '''
    # conn
    connection = psycopg2.connect(
        host='localhost',
        user='postgres',
        password='password',
        port='5432',
        dbname='satudata',
        options='-c search_path=public',
    )
    cursor = connection.cursor()

    try:
        query = "insert into bkd_jabatan ("
        query += "jabatan_id, jabatan_nama, jabatan_kelas, jabatan_jenis, "
        query += "unit_kerja_id, unit_kerja_nama, satuan_kerja_id, satuan_kerja_nama, "
        query += "eselon_id, eselon_nm, atasan_jabatan_id, jf_id, jfu_id"
        query += ") values ("
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s"
        query += ")"
        value = (
            data['jabatan_id'], data['jabatan_nama'], data['jabatan_kelas'], data['jabatan_jenis'],
            data['unit_kerja_id'], data['unit_kerja_nama'], data['satuan_kerja_id'], data['satuan_kerja_nama'],
            data['eselon_id'], data['eselon_nm'], data['atasan_jabatan_id'], data['jf_id'], data['jfu_id']
            )
        # print(query)
        # print(value)
        cursor.execute(query, value)
        connection.commit()
        print()
    except Exception as err:
        print(err)
        print()

def get_data(page, perpage):
    ''' Doc '''
    try:
        url = 'https://siap.jabarprov.go.id/integrasi/api/v1/jabatan/list?page='+str(page)+'&perpage='+str(perpage)
        req = requests.get(url, headers={"Content-Type": "application/json"}, verify=False, auth=HTTPBasicAuth('satudata', 'satudata2021'))
        response = req.json()
        if response:
            print("PAGE " + str(page))
            for data in response['data']:
                print(data)
                insert_data(data)

            if response['count'] > (page * perpage):
                get_data(page + 1, perpage)
    except Exception as err:
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data(1, 1000)

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
