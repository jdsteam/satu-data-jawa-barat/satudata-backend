''' Doc '''
import time
import psycopg2
import psycopg2.extras
import requests
import urllib3
from requests.auth import HTTPBasicAuth
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# pylint: disable=broad-except, pointless-string-statement, line-too-long
def insert_data(data):
    ''' Doc '''
    # conn
    connection = psycopg2.connect(
        host='localhost',
        user='postgres',
        password='password',
        port='5432',
        dbname='satudata',
        options='-c search_path=public',
    )
    cursor = connection.cursor()

    try:
        query = "insert into bkd_pegawai ("
        query += "peg_id, last_update, "
        query += "peg_nip, peg_nip_lama, peg_nama, peg_nama_lengkap, peg_lahir_tanggal, peg_usia, "
        query += "peg_jenis_kelamin, peg_rumah_alamat, peg_kodepos, peg_foto_url, peg_status, "
        query += "peg_ketstatus, peg_ktp, peg_umur_pensiun, peg_jabatan_tmt, peg_eselon_tmt, "
        query += "peg_skpd_tmt, peg_npwp, peg_telp, peg_telp_hp, id_agama, nm_agama, "
        query += "jabatan_id, jf_id, jfu_id, jabatan_jenis, jabatan_nama, "
        query += "eselon_id, eselon_nm, unit_kerja_id, unit_kerja_nama, satuan_kerja_id, satuan_kerja_nama, "
        query += "gol_id_awal, nm_gol_awal, nm_pkt_awal, gol_id_akhir, nm_gol_akhir, nm_pkt_akhir, "
        query += "id_pend_awal, nm_pend_awal, kat_nama_pend_awal, nm_tingpend_awal, "
        query += "id_pend_akhir, nm_pend_akhir, kat_nama_pend_akhir, nm_tingpend_akhir, "
        query += "riwayat_pendidikan, riwayat_jabatan, unit_kerja_parent_nama, kedudukan_pegawai, kedudukan_pns, "
        query += "kode_jabatan, peg_tmt_pensiun, masa_kerja_tahun, masa_kerja_bulan, "
        query += "masa_kerja_golongan_tahun, masa_kerja_golongan_bulan, peg_status_kepegawaian_id, peg_status_kepegawaian, "
        query += "tugas_tambahan_jabatan_id, tugas_tambahan_jenis, daftar_spesialisasi, tugas_tambahan_jabatan_nama, "
        query += "tmt_tugas_tambahan, peg_instansi_dpk, daftar_spesialisasi_nama, peg_jenis_pns, "
        query += "jf_nama, jfu_nama, peg_gol_akhir_tmt, peg_ketstatus_tgl, peg_cpns_tmt, peg_pns_tmt, "
        query += "nuptk, id_goldar, nm_goldar, peg_email, peg_bapertarum, peg_no_askes, "
        query += "id_status_kepegawaian, peg_lahir_tempat, peg_status_perkawinan, unit_kerja_level, unit_kerja_parent, "
        query += "peg_kel_desa, kecamatan_nm, jabatan_kelas, peg_karpeg, peg_pend_akhir_th, peg_nama_jenis_pns, peg_jft_tmt, "
        query += "id_kota, id_kec, id_kel, id_provinsi, nama_kel, nama_kec, nama_kota, nama_provinsi, "
        query += "tugas_tambahan2_jabatan_id, tugas_tambahan2_jenis, tugas_tambahan2_jabatan_nama, tmt_tugas_tambahan2, "
        query += "peg_alamat_latitude, peg_alamat_longitude, is_gtk, "
        query += "peg_alamat_rt, peg_alamat_rw, peg_alamat_rt_ktp, peg_alamat_rw_ktp, peg_rumah_alamat_ktp, peg_kodepos_ktp, "
        query += "nama_kel_ktp, nama_kec_ktp, nama_kota_ktp, nama_provinsi_ktp, "
        query += "nip_atasan, nama_atasan, nip_atasan_bayangan, nama_atasan_bayangan, "
        query += "peg_kgb_yad_akhir, unit_kerja_nama_full, organisasi_nama_alias, organisasi_id, "
        query += "peg_no_rekening, peg_masa_kerja_golongan, peg_jumlah_anak, peg_jumlah_anak_tunjangan, "
        query += "peg_jumlah_pasangan_tunjangan, peg_email_resmi, peg_tmt_kgb"
        query += ") values ("
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s"
        query += ")"
        value = (
            data['peg_id'], data['last_update'],
            data['peg_nip'], data['peg_nip_lama'], data['peg_nama'], data['peg_nama_lengkap'], data['peg_lahir_tanggal'], data['peg_usia'],
            data['peg_jenis_kelamin'], data['peg_rumah_alamat'], data['peg_kodepos'], data['peg_foto_url'], data['peg_status'],
            data['peg_ketstatus'], data['peg_ktp'], data['peg_umur_pensiun'], data['peg_jabatan_tmt'], data['peg_eselon_tmt'],
            data['peg_skpd_tmt'], data['peg_npwp'], data['peg_telp'], data['peg_telp_hp'], data['id_agama'], data['nm_agama'],
            data['jabatan_id'], data['jf_id'], data['jfu_id'], data['jabatan_jenis'], data['jabatan_nama'], data['eselon_id'],
            data['eselon_nm'], data['unit_kerja_id'], data['unit_kerja_nama'], data['satuan_kerja_id'], data['satuan_kerja_nama'],
            data['gol_id_awal'], data['nm_gol_awal'], data['nm_pkt_awal'], data['gol_id_akhir'], data['nm_gol_akhir'],
            data['nm_pkt_akhir'], data['id_pend_awal'], data['nm_pend_awal'], data['kat_nama_pend_awal'], data['nm_tingpend_awal'],
            data['id_pend_akhir'], data['nm_pend_akhir'], data['kat_nama_pend_akhir'], data['nm_tingpend_akhir'], data['riwayat_pendidikan'],
            data['riwayat_jabatan'], data['unit_kerja_parent_nama'], data['kedudukan_pegawai'], data['kedudukan_pns'], data['kode_jabatan'],
            data['peg_tmt_pensiun'], data['masa_kerja_tahun'], data['masa_kerja_bulan'], data['masa_kerja_golongan_tahun'],
            data['masa_kerja_golongan_bulan'], data['peg_status_kepegawaian_id'], data['peg_status_kepegawaian'],
            data['tugas_tambahan_jabatan_id'], data['tugas_tambahan_jenis'], data['daftar_spesialisasi'],
            data['tugas_tambahan_jabatan_nama'], data['tmt_tugas_tambahan'], data['peg_instansi_dpk'],
            data['daftar_spesialisasi_nama'], data['peg_jenis_pns'], data['jf_nama'], data['jfu_nama'],
            data['peg_gol_akhir_tmt'], data['peg_ketstatus_tgl'], data['peg_cpns_tmt'], data['peg_pns_tmt'],
            data['nuptk'], data['id_goldar'], data['nm_goldar'], data['peg_email'], data['peg_bapertarum'],
            data['peg_no_askes'], data['id_status_kepegawaian'], data['peg_lahir_tempat'], data['peg_status_perkawinan'],
            data['unit_kerja_level'], data['unit_kerja_parent'], data['peg_kel_desa'], data['kecamatan_nm'],
            data['jabatan_kelas'], data['peg_karpeg'], data['peg_pend_akhir_th'], data['peg_nama_jenis_pns'],
            data['peg_jft_tmt'], data['id_kota'], data['id_kec'], data['id_kel'], data['id_provinsi'],
            data['nama_kel'], data['nama_kec'], data['nama_kota'], data['nama_provinsi'], data['tugas_tambahan2_jabatan_id'],
            data['tugas_tambahan2_jenis'], data['tugas_tambahan2_jabatan_nama'], data['tmt_tugas_tambahan2'], data['peg_alamat_latitude'],
            data['peg_alamat_longitude'], data['is_gtk'], data['peg_alamat_rt'], data['peg_alamat_rw'], data['peg_alamat_rt_ktp'],
            data['peg_alamat_rw_ktp'], data['peg_rumah_alamat_ktp'], data['peg_kodepos_ktp'], data['nama_kel_ktp'],
            data['nama_kec_ktp'], data['nama_kota_ktp'], data['nama_provinsi_ktp'], data['nip_atasan'], data['nama_atasan'],
            data['nip_atasan_bayangan'], data['nama_atasan_bayangan'], data['peg_kgb_yad_akhir'], data['unit_kerja_nama_full'],
            data['organisasi_nama_alias'], data['organisasi_id'], data['peg_no_rekening'], data['peg_masa_kerja_golongan'],
            data['peg_jumlah_anak'], data['peg_jumlah_anak_tunjangan'], data['peg_jumlah_pasangan_tunjangan'], data['peg_email_resmi'],
            data['peg_tmt_kgb']
            )
        # print(query)
        # print(value)
        cursor.execute(query, value)
        connection.commit()
        print()
    except Exception as err:
        print(err)
        print()

def get_data(page, perpage):
    ''' Doc '''
    try:
        url = 'https://siap.jabarprov.go.id/integrasi/api/v1/pegawai/data?page='+str(page)+'&perpage='+str(perpage)
        req = requests.get(url, headers={"Content-Type": "application/json"}, verify=False, auth=HTTPBasicAuth('satudata', 'satudata2021'))
        response = req.json()
        if response:
            print("PAGE " + str(page))
            for data in response['data']:
                print(data)
                insert_data(data)

            if response['count'] > (page * perpage):
                get_data(page + 1, perpage)
    except Exception as err:
        print(err)
        print()

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data(1, 1000)

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
