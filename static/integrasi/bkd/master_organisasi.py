''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# conn
connection = psycopg2.connect(
    host='localhost',
    user='postgres',
    password='password',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
cursor = connection.cursor()


def skpd():
    ''' Doc '''
    try:
        query_get = '''select distinct(satuan_kerja_id), satuan_kerja_nama, skpd.kode_skpd
            from jabatan_extract
            left join skpd on skpd.id = satuan_kerja_id::int
            where satuan_kerja_id is not null and (lv1_unit_kerja_id is null or lv1_unit_kerja_id = '')
            ORDER BY satuan_kerja_nama asc'''
        cursor.execute(query_get)
        result_get = cursor.fetchall()

        for res_get in result_get:
            satuan_kerja_id = res_get[0]
            satuan_kerja_nama = res_get[1]
            kode_skpd = res_get[2]

            print("=============")
            print(satuan_kerja_id)
            print(kode_skpd)
            print(satuan_kerja_nama)

            query_skpd = '''select * from skpd where id = ''' + satuan_kerja_id
            cursor.execute(query_skpd)
            result_skpd = cursor.fetchall()
            if result_skpd:
                query_update = "update skpd set "
                query_update += "nama_skpd=%s, notes=%s "
                query_update += "where id=%s"
                value_update = (
                    satuan_kerja_nama,
                    'new master data 20211118',
                    satuan_kerja_id,
                )
                print(query_update)
                print(value_update)
                cursor.execute(query_update, value_update)
                connection.commit()

    except Exception as err:
        print(err)
        print()

def skpd_sub():
    ''' Doc '''
    try:
        query_get = '''select distinct satuan_kerja_id, satuan_kerja_nama, skpd.kode_skpd, lv1_unit_kerja_id, lv1_unit_kerja_nama
            from jabatan_extract
            left join skpd on skpd.id = satuan_kerja_id::int
            where satuan_kerja_id is not null and lv1_unit_kerja_id != '' and (lv2_unit_kerja_id is null or lv2_unit_kerja_id = '')
            ORDER BY satuan_kerja_nama asc, lv1_unit_kerja_nama asc'''
        cursor.execute(query_get)
        result_get = cursor.fetchall()

        for res_get in result_get:
            satuan_kerja_id = res_get[0]
            satuan_kerja_nama = res_get[1]
            kode_skpd = res_get[2]
            lv1_unit_kerja_id = res_get[3]
            lv1_unit_kerja_nama = res_get[4]

            print("=============")
            print(satuan_kerja_id)
            print(satuan_kerja_nama)
            print(kode_skpd)
            print(lv1_unit_kerja_id)
            print(lv1_unit_kerja_nama)

            query_update = "insert into skpdsub (regional_id, notes, "
            query_update += "kode_skpd, skpd_id, nama_skpd, "
            query_update += "kode_skpdsub, id, nama_skpdsub)"
            query_update += "values (%s, %s, "
            query_update += "%s, %s, %s, "
            query_update += "%s, %s, %s)"
            value_update = (
                '1', 'new master data 20211118',
                kode_skpd, satuan_kerja_id, satuan_kerja_nama,
                lv1_unit_kerja_id, lv1_unit_kerja_id, lv1_unit_kerja_nama
            )
            print(query_update)
            print(value_update)
            cursor.execute(query_update, value_update)
            connection.commit()

    except Exception as err:
        print(err)
        print()

def skpd_unit():
    ''' Doc '''
    try:
        query_get = '''select distinct satuan_kerja_id, satuan_kerja_nama, skpd.kode_skpd, lv1_unit_kerja_id, lv1_unit_kerja_nama, lv2_unit_kerja_id, lv2_unit_kerja_nama
            from jabatan_extract
            left join skpd on skpd.id = satuan_kerja_id::int
            where satuan_kerja_id is not null and lv1_unit_kerja_id != '' and  lv2_unit_kerja_id != '' and (lv3_unit_kerja_id is null or lv3_unit_kerja_id = '')
            ORDER BY satuan_kerja_nama asc, lv1_unit_kerja_nama asc, lv2_unit_kerja_nama asc'''
        cursor.execute(query_get)
        result_get = cursor.fetchall()

        for res_get in result_get:
            satuan_kerja_id = res_get[0]
            satuan_kerja_nama = res_get[1]
            kode_skpd = res_get[2]
            lv1_unit_kerja_id = res_get[3]
            lv1_unit_kerja_nama = res_get[4]
            lv2_unit_kerja_id = res_get[5]
            lv2_unit_kerja_nama = res_get[6]

            print("=============")
            print(satuan_kerja_id)
            print(satuan_kerja_nama)
            print(kode_skpd)
            print(lv1_unit_kerja_id)
            print(lv1_unit_kerja_nama)
            print(lv2_unit_kerja_id)
            print(lv2_unit_kerja_nama)

            query_update = "insert into skpdunit (regional_id, notes, "
            query_update += "kode_skpd, skpd_id, nama_skpd, "
            query_update += "kode_skpdsub, skpdsub_id, nama_skpdsub, "
            query_update += "kode_skpdunit, id, nama_skpdunit)"
            query_update += "values (%s, %s, "
            query_update += "%s, %s, %s, "
            query_update += "%s, %s, %s, "
            query_update += "%s, %s, %s)"
            value_update = (
                '1', 'new master data 20211118',
                kode_skpd, satuan_kerja_id, satuan_kerja_nama,
                lv1_unit_kerja_id, lv1_unit_kerja_id, lv1_unit_kerja_nama,
                lv2_unit_kerja_id, lv2_unit_kerja_id, lv2_unit_kerja_nama
            )
            print(query_update)
            print(value_update)
            cursor.execute(query_update, value_update)
            connection.commit()

    except Exception as err:
        print(err)
        print()


if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    # skpd()
    # skpd_sub()
    skpd_unit()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
