import os
import PIL
from PIL import Image
import shutil

storage_source_path = "D:/SERVER/WORK/JDS/SATUDATA/satudata-backend/static/upload/"

files = []
for (dirpath, dirnames, filenames) in os.walk(storage_source_path):
    # print(dirpath)
    # print(dirnames)
    files.extend(filenames)
    break

for file in files:
    print(file)

    try:
        if file.rsplit('.', 1)[1].lower() in ['png', 'jpg', 'jpeg', 'gif']:
            # resize 700
            fixed_width = 700
            image = Image.open(os.path.join(storage_source_path, file))
            width_percent = (fixed_width / float(image.size[0]))
            height_size = int((float(image.size[1]) * float(width_percent)))
            image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
            image.save(os.path.join(storage_source_path + '700/', file))

            # resize 500
            fixed_width = 500
            image = Image.open(os.path.join(storage_source_path, file))
            width_percent = (fixed_width / float(image.size[0]))
            height_size = int((float(image.size[1]) * float(width_percent)))
            image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
            image.save(os.path.join(storage_source_path + '500/', file))

            # resize 300
            fixed_width = 300
            image = Image.open(os.path.join(storage_source_path, file))
            width_percent = (fixed_width / float(image.size[0]))
            height_size = int((float(image.size[1]) * float(width_percent)))
            image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
            image.save(os.path.join(storage_source_path + '300/', file))

            # resize 100
            fixed_width = 100
            image = Image.open(os.path.join(storage_source_path, file))
            width_percent = (fixed_width / float(image.size[0]))
            height_size = int((float(image.size[1]) * float(width_percent)))
            image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
            image.save(os.path.join(storage_source_path + '100/', file))

        elif file.rsplit('.', 1)[1].lower() in ['svg']:
            shutil.copy(
                os.path.join(storage_source_path, file),
                os.path.join(storage_source_path + '700/', file)
            )
            shutil.copy(
                os.path.join(storage_source_path, file),
                os.path.join(storage_source_path + '500/', file)
            )
            shutil.copy(
                os.path.join(storage_source_path, file),
                os.path.join(storage_source_path + '300/', file)
            )
            shutil.copy(
                os.path.join(storage_source_path, file),
                os.path.join(storage_source_path + '100/', file)
            )

    except Exception as err:
        print(err)
