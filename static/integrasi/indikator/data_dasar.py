''' Doc '''
import time
import psycopg2
import psycopg2.extras
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

conn_satudata = psycopg2.connect(
    host='',
    user='',
    password='',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
cursor_satudata = conn_satudata.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

def get_data():
    try:
        query = "SELECT id, data_dasar, cuid, cdate from indikator"
        cursor_satudata.execute(query)
        results = cursor_satudata.fetchall()
        for res in results:
            # print(res)
            print(res['id'])
            data_dasar_arr = []
            if res['data_dasar']:
                print(res['data_dasar'])
                data_dasar_arr = res['data_dasar'].splitlines()
                print(data_dasar_arr)
            for dda in data_dasar_arr:
                print(dda.replace('*   ', ''))
                insert_data(res['id'], dda.replace('*   ', '').replace('* ', '').replace('*', '').replace('- ', ''), res['cuid'], res['cdate'].strftime("%Y-%m-%d %H:%M:%S"))
            print('===========')

        cursor_satudata.close()
        conn_satudata.close()

    except Exception as err:
        print(err)
        print()

def insert_data(indikator_id, data_dasar, cuid, cdate):
    try:
        query = "insert into indikator_datadasar ("
        query += "indikator_id, name, cuid, cdate"
        query += ") values ("
        query += "%s, %s, %s, %s "
        query += ")"
        value = (
            indikator_id, data_dasar, cuid, cdate
        )
        print(query)
        print(value)
        cursor_satudata.execute(query, value)
        conn_satudata.commit()
        print()
    except Exception as err:
        print(err)
        print()
        conn_satudata.rollback();

if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    get_data()

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
