import time
import psycopg2
import psycopg2.extras
import requests
import urllib3
import pandas as pd
from bs4 import BeautifulSoup
from claming import Cleansing, Matching
from datasae import Comformity, Uniqueness, Consistency, Completeness, Timeliness
import simplejson as json
import requests
import unidecode
import datetime
from pytz import timezone
from requests.auth import HTTPBasicAuth
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

token = ''

# conn
connection_bigdata = psycopg2.connect(
    host='',
    user='',
    password='',
    port='5432',
    dbname='bigdata',
    options='-c search_path=public',
)
cursor_bigdata = connection_bigdata.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

connection_satudata = psycopg2.connect(
    host='',
    user='',
    password='',
    port='5432',
    dbname='satudata',
    options='-c search_path=public',
)
cursor_satudata = connection_satudata.cursor(cursor_factory = psycopg2.extras.RealDictCursor)

def dataset_get_by_id_table(_id):
    try:
        url = 'https://data.jabarprov.go.id/api-backend/dataset/'+str(_id)
        req = requests.get(url, headers={"Content-Type": "application/json", "Authorization": "Bearer " + token}, verify=False)
        response = req.json()
        if response:
            return response['error'] * -1 , response['data'], response['message']
    except Exception as err:
        print(err)
        print()

def bigdata_get_count(schema, table):
    try:
        url = 'https://data.jabarprov.go.id/api-backend/bigdata/'+schema+'/'+table+'?count=true'
        req = requests.get(url, headers={"Content-Type": "application/json", "Authorization": "Bearer " + token}, verify=False)
        response = req.json()
        if response:
            return response['error'] * -1 , response['data']['count'], response['metadata']
    except Exception as err:
        print(err)
        print()

def bigdata_get_all(schema, table, limit, metadata):
    try:
        query = "SELECT * from "+schema+"."+table+" limit "+str(limit)
        cursor_bigdata.execute(query)
        result = cursor_bigdata.fetchall()
        if result:
            final_result = []
            for res in result:
                temp = {}
                for meta in metadata:
                    temp[meta] = res[meta]
                final_result.append(temp)
            return True, final_result
        else:
            return False, {}
    except Exception as err:
        print(err)
        print()

def create_quality_score(dataset_info, data_, metadata_):
    try:
        # kode bps
        kode_bps = [{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3201","nama_kabupaten_kota":"KABUPATEN BOGOR"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3202","nama_kabupaten_kota":"KABUPATEN SUKABUMI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3203","nama_kabupaten_kota":"KABUPATEN CIANJUR"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3204","nama_kabupaten_kota":"KABUPATEN BANDUNG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3205","nama_kabupaten_kota":"KABUPATEN GARUT"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3206","nama_kabupaten_kota":"KABUPATEN TASIKMALAYA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3207","nama_kabupaten_kota":"KABUPATEN CIAMIS"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3208","nama_kabupaten_kota":"KABUPATEN KUNINGAN"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3209","nama_kabupaten_kota":"KABUPATEN CIREBON"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3210","nama_kabupaten_kota":"KABUPATEN MAJALENGKA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3211","nama_kabupaten_kota":"KABUPATEN SUMEDANG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3212","nama_kabupaten_kota":"KABUPATEN INDRAMAYU"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3213","nama_kabupaten_kota":"KABUPATEN SUBANG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3214","nama_kabupaten_kota":"KABUPATEN PURWAKARTA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3215","nama_kabupaten_kota":"KABUPATEN KARAWANG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3216","nama_kabupaten_kota":"KABUPATEN BEKASI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3217","nama_kabupaten_kota":"KABUPATEN BANDUNG BARAT"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3218","nama_kabupaten_kota":"KABUPATEN PANGANDARAN"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3271","nama_kabupaten_kota":"KOTA BOGOR"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3272","nama_kabupaten_kota":"KOTA SUKABUMI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3273","nama_kabupaten_kota":"KOTA BANDUNG"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3274","nama_kabupaten_kota":"KOTA CIREBON"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3275","nama_kabupaten_kota":"KOTA BEKASI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3276","nama_kabupaten_kota":"KOTA DEPOK"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3277","nama_kabupaten_kota":"KOTA CIMAHI"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3278","nama_kabupaten_kota":"KOTA TASIKMALAYA"},{"kode_provinsi":"32","nama_provinsi":"JAWA BARAT","kode_kabupaten_kota":"3279","nama_kabupaten_kota":"KOTA BANJAR"}]

        # read dataset
        dataset = pd.DataFrame(data_)

        # cleansing
        try:
            dataset["kode_provinsi"] = dataset["kode_provinsi"].astype("str")
            dataset["kode_kabupaten_kota"] = dataset["kode_kabupaten_kota"].astype("str")
        except Exception as err:
            error = "Error cleansing, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish cleansing')

        # read metadata
        try:
            metadata = {}
            for i in metadata_:
                if i["key"] in ["Pengukuran Dataset", "Tingkat Penyajian Dataset", "Cakupan Dataset", "Satuan Dataset", "Frekuensi Dataset"]:
                    var = {i["key"].lower().replace(" ", "_"): i["value"]}
                    metadata.update(var)
        except Exception as err:
            error = "Error read metadata, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish read metadata')

        # generate config
        try:
            columns = dataset.columns
            config = {
                "name": dataset_info['name'],
                "description": clean_description(dataset_info['description']),
                "tag": dataset_info['dataset_tags'],
                "category": dataset_info['category'],
                "metadata": metadata,
                "unit": metadata['satuan_dataset'].split(',') if 'satuan_dataset' in metadata.keys() else None,
                "unit_column": get_unit_column(columns),
                "value_column": get_value_column(columns, metadata['pengukuran_dataset']) if 'pengukuran_dataset' in metadata.keys() else None,
                "time_series_type": time_series(columns, metadata['frekuensi_dataset'])['time_series_type'] if 'frekuensi_dataset' in metadata.keys() else None,
                "column_time_series": time_series(columns, metadata['frekuensi_dataset'])['column_time_series'] if 'frekuensi_dataset' in metadata.keys() else {'years_column': None, 'months_column': None, 'dates_column': None},
                "code_area": kode_bps,
                "code_area_level": get_code_area_level(columns)
            }
        except Exception as err:
            error = "Error create config, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish generate config')

        try:
            comformity = Comformity(
                data=dataset,
                title=config["name"],
                description=config["description"],
                tag=config["tag"],
                metadata=config["metadata"],
                category=config["category"],
                code_area=config["code_area"],
                code_area_level=config["code_area_level"]
            )
            comformity_quality = comformity.comformity(
                comformity_explain_columns=20,
                comformity_code_area=20,
                comformity_measurement=20,
                comformity_serving_rate=20,
                comformity_scope=20
            )
            # print(comformity_quality)
        except Exception as err:
            error = "Error create conformity, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish comformity')

        try:
            uniqueness = Uniqueness(
                data=dataset
            )
            uniqueness_quality = uniqueness.uniqueness(
                uniqeness_duplicated=100
            )
            # print(uniqueness_quality)
        except Exception as err:
            error = "Error create uniqueness, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish uniqueness')

        try:
            consistency = Consistency(
                data=dataset,
                unit=config["unit"],
                unit_column=config["unit_column"],
                value_column=config["value_column"],
                time_series_type=config["time_series_type"],
                column_time_series=config["column_time_series"]
            )
            consistency_quality = consistency.consistency(
                consistency_unit=40,
                consistency_time_series=20,
                consistency_listing_province=40
            )
            # print(consistency_quality)
        except Exception as err:
            error = "Error create consistency, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish consistency')

        try:
            completeness = Completeness(
                data=dataset
            )
            completeness_quality = completeness.completeness(
                completeness_filled=100
            )
            # print(completeness_quality)
        except Exception as err:
            error = "Error create completeness, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish completeness')

        try:
            timeliness = Timeliness(
                data=dataset,
                time_series_type=config["time_series_type"],
                column_time_series=config["column_time_series"]
            )
            timeliness_quality = timeliness.timeliness(
                timeliness_updated=100
            )
            # print(timeliness_quality)
        except Exception as err:
            error = "Error create timeliness, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish timeliness')

        try:
            try:
                comformity_quality
            except NameError:
                comformity_quality = {}
                comformity_quality["result"] = 0
            try:
                uniqueness_quality
            except NameError:
                uniqueness_quality = {}
                uniqueness_quality["result"] = 0
            try:
                consistency_quality
            except NameError:
                consistency_quality = {}
                consistency_quality["result"] = 0
            try:
                completeness_quality
            except NameError:
                completeness_quality = {}
                completeness_quality["result"] = 0
            try:
                timeliness_quality
            except NameError:
                timeliness_quality = {}
                timeliness_quality["result"] = 0

            quality_result = (
                (comformity_quality["result"] * 0.30) +
                (uniqueness_quality["result"] * 0.25) +
                (consistency_quality["result"] * 0.25) +
                (completeness_quality["result"] * 0.10) +
                (timeliness_quality["result"] * 0.10)
            )

            final_result = {}
            final_result.update({'final_result': quality_result})
            final_result.update(comformity_quality)
            final_result.update(consistency_quality)
            final_result.update(completeness_quality)
            final_result.update(timeliness_quality)
            final_result.update(uniqueness_quality)

            return save_or_update(dataset_info, config, final_result)

        except Exception as err:
            error = "Error create result, " + str(err)
            generate_exception_message(dataset_info, metadata_, error)
        print('finish save')

    except Exception as err:
        # fail response
        # raise ErrorMessage(str(err), 500, 1, {})
        error = "Error create quality, " + str(err)
        generate_exception_message(dataset_info, metadata_, error)
        return False, {}, "Error create quality, " + str(err)

def generate_exception_message(dataset_info, metadata_, err):
    temp = {}
    temp['id'] = dataset_info['id']
    temp['name'] = dataset_info['name']
    temp['schema'] = dataset_info['schema']
    temp['table'] = dataset_info['table']
    temp['tag'] = dataset_info['dataset_tags']
    temp['description'] = dataset_info['description']
    temp['metadata_'] = json.dumps(metadata_)
    temp['category'] = dataset_info['category']
    temp['error'] = err
    temp['is_config_complete'] = False

    try:
        query_count = "SELECT count(*) from dataset_quality_results where id = " + str(temp['id'])
        cursor_satudata.execute(query_count)
        result = cursor_satudata.fetchall()
        if result[0]['count'] > 0:
            # update
            try:
                query = "update dataset_quality_results set "
                query += '"id"=%s, "name"=%s, "schema"=%s, "table"=%s, "tag"=%s, '
                query += '"description"=%s, "metadata"=%s, "category"=%s, "error"=%s, "is_config_complete"=%s '
                query += "where id=%s"
                value = (
                    temp['id'], temp['name'], temp['schema'], temp['table'], temp['tag'],
                    temp['description'], temp['metadata_'], temp['category'], temp['error'], temp['is_config_complete'],
                    temp['id']
                )
                # print(query)
                # print(value)
                cursor_satudata.execute(query, value)
                connection_satudata.commit()
            except Exception as err:
                print(err)
                connection_satudata.rollback()
        else:
            # insert
            try:
                query = "insert into dataset_quality_results ("
                query += '"id", "name", "schema", "table", "tag", "description", "metadata", "category", "error", "is_config_complete"'
                query += ") values ("
                query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s"
                query += ")"
                value = (
                    temp['id'], temp['name'], temp['schema'], temp['table'], temp['tag'],
                    temp['description'], temp['metadata_'], temp['category'], temp['error'], temp['is_config_complete']
                )
                print(query)
                # print(value)
                cursor_satudata.execute(query, value)
                connection_satudata.commit()
            except Exception as err:
                print(err)
                connection_satudata.rollback()
    except Exception as e:
        print(e)
        connection_satudata.rollback()

def save_or_update(dataset_info, config, quality_result):
    try:
        temp = {}
        temp['id'] = dataset_info['id']
        temp['unit'] = config['unit']
        temp['unit_column'] = config['unit_column']
        temp['value_column'] = config['value_column']
        temp['name'] = config['name']
        temp['schema'] = dataset_info['schema']
        temp['table'] = dataset_info['table']
        temp['tag'] = config['tag']
        temp['description'] = config['description']
        temp['metadata_'] = json.dumps(config['metadata'])
        temp['category'] = config['category']
        temp['code_area_level'] = config['code_area_level']
        temp['column_time_series'] = json.dumps(config['column_time_series'])
        temp['time_series_type'] = config['time_series_type']
        temp['error'] = ''
        temp['is_config_complete'] = True

        try:
            temp['conformity_code_area_total_rows'] = quality_result['comformity_code_area']['total_rows']
            temp['conformity_code_area_total_columns'] = quality_result['comformity_code_area']['total_columns']
            temp['conformity_code_area_total_cells'] = quality_result['comformity_code_area']['total_cells']
            temp['conformity_code_area_total_valid'] = quality_result['comformity_code_area']['total_valid']
            temp['conformity_code_area_total_not_valid'] = quality_result['comformity_code_area']['total_not_valid']
            temp['conformity_code_area_warning'] = quality_result['comformity_code_area']['warning']
            temp['conformity_code_area_quality_result'] = quality_result['comformity_code_area']['quality_result']
        except Exception as err:
            quality_result['comformity_code_area'] = {}
            quality_result['comformity_code_area']['quality_result'] = 0

        try:
            temp['conformity_explain_columns_total_rows'] = quality_result['comformity_explain_columns']['total_rows']
            temp['conformity_explain_columns_total_columns'] = quality_result['comformity_explain_columns']['total_columns']
            temp['conformity_explain_columns_total_cells'] = quality_result['comformity_explain_columns']['total_cells']
            temp['conformity_explain_columns_total_valid'] = quality_result['comformity_explain_columns']['total_valid']
            temp['conformity_explain_columns_total_not_valid'] = quality_result['comformity_explain_columns']['total_not_valid']
            temp['conformity_explain_columns_warning'] = quality_result['comformity_explain_columns']['warning']
            temp['conformity_explain_columns_quality_result'] = quality_result['comformity_explain_columns']['quality_result']
        except Exception as err:
            quality_result['comformity_explain_columns'] = {}
            quality_result['comformity_explain_columns']['quality_result'] = 0

        try:
            temp['conformity_measurement_total_rows'] = quality_result['comformity_measurement']['total_rows']
            temp['conformity_measurement_total_columns'] = quality_result['comformity_measurement']['total_columns']
            temp['conformity_measurement_total_cells'] = quality_result['comformity_measurement']['total_cells']
            temp['conformity_measurement_total_valid'] = quality_result['comformity_measurement']['total_valid']
            temp['conformity_measurement_total_not_valid'] = quality_result['comformity_measurement']['total_not_valid']
            temp['conformity_measurement_warning'] = quality_result['comformity_measurement']['warning']
            temp['conformity_measurement_quality_result'] = quality_result['comformity_measurement']['quality_result']
        except Exception as err:
            quality_result['comformity_measurement'] = {}
            quality_result['comformity_measurement']['quality_result'] = 0

        try:
            temp['conformity_serving_rate_total_rows'] = quality_result['comformity_serving_rate']['total_rows']
            temp['conformity_serving_rate_total_columns'] = quality_result['comformity_serving_rate']['total_columns']
            temp['conformity_serving_rate_total_cells'] = quality_result['comformity_serving_rate']['total_cells']
            temp['conformity_serving_rate_total_valid'] = quality_result['comformity_serving_rate']['total_valid']
            temp['conformity_serving_rate_total_not_valid'] = quality_result['comformity_serving_rate']['total_not_valid']
            temp['conformity_serving_rate_warning'] = quality_result['comformity_serving_rate']['warning']
            temp['conformity_serving_rate_quality_result'] = quality_result['comformity_serving_rate']['quality_result']
        except Exception as err:
            quality_result['comformity_serving_rate'] = {}
            quality_result['comformity_serving_rate']['quality_result'] = 0

        try:
            temp['conformity_scope_total_rows'] = quality_result['comformity_scope']['total_rows']
            temp['conformity_scope_total_columns'] = quality_result['comformity_scope']['total_columns']
            temp['conformity_scope_total_cells'] = quality_result['comformity_scope']['total_cells']
            temp['conformity_scope_total_valid'] = quality_result['comformity_scope']['total_valid']
            temp['conformity_scope_total_not_valid'] = quality_result['comformity_scope']['total_not_valid']
            temp['conformity_scope_warning'] = quality_result['comformity_scope']['warning']
            temp['conformity_scope_quality_result'] = quality_result['comformity_scope']['quality_result']
        except Exception as err:
            quality_result['comformity_scope'] = {}
            quality_result['comformity_scope']['quality_result'] = 0

        try:
            temp['uniqueness_duplicated_total_rows'] = quality_result['uniqeness_duplicated']['total_rows']
            temp['uniqueness_duplicated_total_columns'] = quality_result['uniqeness_duplicated']['total_columns']
            temp['uniqueness_duplicated_total_cells'] = quality_result['uniqeness_duplicated']['total_cells']
            temp['uniqueness_duplicated_total_valid'] = quality_result['uniqeness_duplicated']['total_valid']
            temp['uniqueness_duplicated_total_not_valid'] = quality_result['uniqeness_duplicated']['total_not_valid']
            temp['uniqueness_duplicated_warning'] = quality_result['uniqeness_duplicated']['warning']
            temp['uniqueness_duplicated_quality_result'] = quality_result['uniqeness_duplicated']['quality_result']
        except Exception as err:
            quality_result['uniqeness_duplicated'] = {}
            quality_result['uniqeness_duplicated']['quality_result'] = 0

        try:
            temp['completeness_filled_total_rows'] = quality_result['completeness_filled']['total_rows']
            temp['completeness_filled_total_columns'] = quality_result['completeness_filled']['total_columns']
            temp['completeness_filled_total_cells'] = quality_result['completeness_filled']['total_cells']
            temp['completeness_filled_total_valid'] = quality_result['completeness_filled']['total_valid']
            temp['completeness_filled_total_not_valid'] = quality_result['completeness_filled']['total_not_valid']
            temp['completeness_filled_warning'] = quality_result['completeness_filled']['warning']
            temp['completeness_filled_quality_result'] = quality_result['completeness_filled']['quality_result']
        except Exception as err:
            quality_result['completeness_filled'] = {}
            quality_result['completeness_filled']['quality_result'] = 0

        try:
            temp['consistency_unit_total_rows'] = quality_result['consistency_unit']['total_rows']
            temp['consistency_unit_total_columns'] = quality_result['consistency_unit']['total_columns']
            temp['consistency_unit_total_cells'] = quality_result['consistency_unit']['total_cells']
            temp['consistency_unit_total_valid'] = quality_result['consistency_unit']['total_valid']
            temp['consistency_unit_total_not_valid'] = quality_result['consistency_unit']['total_not_valid']
            temp['consistency_unit_warning'] = quality_result['consistency_unit']['warning']
            temp['consistency_unit_quality_result'] = quality_result['consistency_unit']['quality_result']
        except Exception as err:
            quality_result['consistency_unit'] = {}
            quality_result['consistency_unit']['quality_result'] = 0

        try:
            temp['consistency_listing_province_total_cells'] = quality_result['consistency_listing_province']['total_rows']
            temp['consistency_listing_province_total_columns'] = quality_result['consistency_listing_province']['total_columns']
            temp['consistency_listing_province_total_not_valid'] = quality_result['consistency_listing_province']['total_cells']
            temp['consistency_listing_province_total_rows'] = quality_result['consistency_listing_province']['total_valid']
            temp['consistency_listing_province_total_valid'] = quality_result['consistency_listing_province']['total_not_valid']
            temp['consistency_listing_province_warning'] = quality_result['consistency_listing_province']['warning']
            temp['consistency_listing_province_quality_result'] = quality_result['consistency_listing_province']['quality_result']
        except Exception as err:
            quality_result['consistency_listing_province'] = {}
            quality_result['consistency_listing_province']['quality_result'] = 0

        try:
            temp['consistency_time_series_total_rows'] = quality_result['consistency_time_series']['total_rows']
            temp['consistency_time_series_total_columns'] = quality_result['consistency_time_series']['total_columns']
            temp['consistency_time_series_total_cells'] = quality_result['consistency_time_series']['total_cells']
            temp['consistency_time_series_total_valid'] = quality_result['consistency_time_series']['total_valid']
            temp['consistency_time_series_total_not_valid'] = quality_result['consistency_time_series']['total_not_valid']
            temp['consistency_time_series_warning'] = quality_result['consistency_time_series']['warning']
            temp['consistency_time_series_quality_result'] = quality_result['consistency_time_series']['quality_result']
        except Exception as err:
            quality_result['consistency_time_series'] = {}
            quality_result['consistency_time_series']['quality_result'] = 0

        try:
            temp['timeliness_updated_total_rows'] = quality_result['timeliness_updated']['total_rows']
            temp['timeliness_updated_total_columns'] = quality_result['timeliness_updated']['total_columns']
            temp['timeliness_updated_total_cells'] = quality_result['timeliness_updated']['total_cells']
            temp['timeliness_updated_total_valid'] = quality_result['timeliness_updated']['total_valid']
            temp['timeliness_updated_total_not_valid'] = quality_result['timeliness_updated']['total_not_valid']
            temp['timeliness_updated_warning'] = quality_result['timeliness_updated']['warning']
            temp['timeliness_updated_quality_result'] = quality_result['timeliness_updated']['quality_result']
        except Exception as err:
            quality_result['timeliness_updated'] = {}
            quality_result['timeliness_updated']['quality_result'] = 0

        temp['conformity_result'] = (0.2 * quality_result['comformity_code_area']['quality_result']) + (0.2 * quality_result['comformity_explain_columns']['quality_result'])
        temp['conformity_result'] += (0.2 * quality_result['comformity_measurement']['quality_result']) + (0.2 * quality_result['comformity_serving_rate']['quality_result'])
        temp['conformity_result'] += (0.2 * quality_result['comformity_scope']['quality_result'])
        temp['uniqueness_result'] = quality_result['uniqeness_duplicated']['quality_result']
        temp['completeness_result'] = quality_result['completeness_filled']['quality_result']
        temp['consistency_result'] = (quality_result['consistency_unit']['quality_result'] * 0.4) + (quality_result['consistency_listing_province']['quality_result'] * 0.4) + (quality_result['consistency_time_series']['quality_result'] * 0.2)
        temp['timeliness_result'] = quality_result['timeliness_updated']['quality_result']
        temp['final_result'] = (0.30 * temp['conformity_result']) + (0.25 * temp['uniqueness_result']) + (0.10 * temp['completeness_result']) + (0.25 * temp['consistency_result']) + (0.10 * temp['timeliness_result'])

        try:
            query_count = "SELECT count(*) from dataset_quality_results where id = " + str(temp['id'])
            cursor_satudata.execute(query_count)
            result = cursor_satudata.fetchall()
            if result[0]['count'] > 0:
                # update
                try:
                    query = "update dataset_quality_results set "
                    query += '"unit"=%s, '
                    query += '"unit_column"=%s, '
                    query += '"value_column"=%s, '
                    query += '"name"=%s, '
                    query += '"schema"=%s, '
                    query += '"table"=%s, '
                    query += '"tag"=%s, '
                    query += '"description"=%s, '
                    query += '"metadata"=%s, '
                    query += '"category"=%s, '
                    query += '"code_area_level"=%s, '
                    query += '"column_time_series"=%s, '
                    query += '"error"=%s, '
                    query += '"is_config_complete"=%s, '
                    query += '"time_series_type"=%s, '
                    query += '"final_result"=%s, '
                    query += '"conformity_result"=%s, '
                    query += '"conformity_code_area_total_rows"=%s, '
                    query += '"conformity_code_area_total_columns"=%s, '
                    query += '"conformity_code_area_total_cells"=%s, '
                    query += '"conformity_code_area_total_valid"=%s, '
                    query += '"conformity_code_area_total_not_valid"=%s, '
                    query += '"conformity_code_area_warning"=%s, '
                    query += '"conformity_code_area_quality_result"=%s, '
                    query += '"conformity_explain_columns_total_rows"=%s, '
                    query += '"conformity_explain_columns_total_columns"=%s, '
                    query += '"conformity_explain_columns_total_cells"=%s, '
                    query += '"conformity_explain_columns_total_valid"=%s, '
                    query += '"conformity_explain_columns_total_not_valid"=%s, '
                    query += '"conformity_explain_columns_warning"=%s, '
                    query += '"conformity_explain_columns_quality_result"=%s, '
                    query += '"conformity_measurement_total_rows"=%s, '
                    query += '"conformity_measurement_total_columns"=%s, '
                    query += '"conformity_measurement_total_cells"=%s, '
                    query += '"conformity_measurement_total_valid"=%s, '
                    query += '"conformity_measurement_total_not_valid"=%s, '
                    query += '"conformity_measurement_warning"=%s, '
                    query += '"conformity_measurement_quality_result"=%s, '
                    query += '"conformity_serving_rate_total_rows"=%s, '
                    query += '"conformity_serving_rate_total_columns"=%s, '
                    query += '"conformity_serving_rate_total_cells"=%s, '
                    query += '"conformity_serving_rate_total_valid"=%s, '
                    query += '"conformity_serving_rate_total_not_valid"=%s, '
                    query += '"conformity_serving_rate_warning"=%s, '
                    query += '"conformity_serving_rate_quality_result"=%s, '
                    query += '"conformity_scope_total_rows"=%s, '
                    query += '"conformity_scope_total_columns"=%s, '
                    query += '"conformity_scope_total_cells"=%s, '
                    query += '"conformity_scope_total_valid"=%s, '
                    query += '"conformity_scope_total_not_valid"=%s, '
                    query += '"conformity_scope_warning"=%s, '
                    query += '"conformity_scope_quality_result"=%s, '
                    query += '"uniqueness_result"=%s, '
                    query += '"uniqueness_duplicated_total_rows"=%s, '
                    query += '"uniqueness_duplicated_total_columns"=%s, '
                    query += '"uniqueness_duplicated_total_cells"=%s, '
                    query += '"uniqueness_duplicated_total_valid"=%s, '
                    query += '"uniqueness_duplicated_total_not_valid"=%s, '
                    query += '"uniqueness_duplicated_warning"=%s, '
                    query += '"uniqueness_duplicated_quality_result"=%s, '
                    query += '"completeness_result"=%s, '
                    query += '"completeness_filled_total_rows"=%s, '
                    query += '"completeness_filled_total_columns"=%s, '
                    query += '"completeness_filled_total_cells"=%s, '
                    query += '"completeness_filled_total_valid"=%s, '
                    query += '"completeness_filled_total_not_valid"=%s, '
                    query += '"completeness_filled_warning"=%s, '
                    query += '"completeness_filled_quality_result"=%s, '
                    query += '"consistency_result"=%s, '
                    query += '"consistency_unit_total_rows"=%s, '
                    query += '"consistency_unit_total_columns"=%s, '
                    query += '"consistency_unit_total_cells"=%s, '
                    query += '"consistency_unit_total_valid"=%s, '
                    query += '"consistency_unit_total_not_valid"=%s, '
                    query += '"consistency_unit_warning"=%s, '
                    query += '"consistency_unit_quality_result"=%s, '
                    query += '"consistency_listing_province_total_rows"=%s, '
                    query += '"consistency_listing_province_total_columns"=%s, '
                    query += '"consistency_listing_province_total_cells"=%s, '
                    query += '"consistency_listing_province_total_valid"=%s, '
                    query += '"consistency_listing_province_total_not_valid"=%s, '
                    query += '"consistency_listing_province_warning"=%s, '
                    query += '"consistency_listing_province_quality_result"=%s, '
                    query += '"consistency_time_series_total_rows"=%s, '
                    query += '"consistency_time_series_total_columns"=%s, '
                    query += '"consistency_time_series_total_cells"=%s, '
                    query += '"consistency_time_series_total_valid"=%s, '
                    query += '"consistency_time_series_total_not_valid"=%s, '
                    query += '"consistency_time_series_warning"=%s, '
                    query += '"consistency_time_series_quality_result"=%s, '
                    query += '"timeliness_result"=%s, '
                    query += '"timeliness_updated_total_rows"=%s, '
                    query += '"timeliness_updated_total_columns"=%s, '
                    query += '"timeliness_updated_total_cells"=%s, '
                    query += '"timeliness_updated_total_valid"=%s, '
                    query += '"timeliness_updated_total_not_valid"=%s, '
                    query += '"timeliness_updated_warning"=%s, '
                    query += '"timeliness_updated_quality_result"=%s '
                    query += "where id=%s"
                    value = (
                        temp['unit'],
                        temp['unit_column'],
                        temp['value_column'],
                        temp['name'],
                        temp['schema'],
                        temp['table'],
                        temp['tag'],
                        temp['description'],
                        temp['metadata_'],
                        temp['category'],
                        temp['code_area_level'],
                        temp['column_time_series'],
                        temp['error'],
                        temp['is_config_complete'],
                        temp['time_series_type'],
                        temp['final_result'],
                        temp['conformity_result'],
                        temp['conformity_code_area_total_rows'],
                        temp['conformity_code_area_total_columns'],
                        temp['conformity_code_area_total_cells'],
                        temp['conformity_code_area_total_valid'],
                        temp['conformity_code_area_total_not_valid'],
                        temp['conformity_code_area_warning'],
                        temp['conformity_code_area_quality_result'],
                        temp['conformity_explain_columns_total_rows'],
                        temp['conformity_explain_columns_total_columns'],
                        temp['conformity_explain_columns_total_cells'],
                        temp['conformity_explain_columns_total_valid'],
                        temp['conformity_explain_columns_total_not_valid'],
                        temp['conformity_explain_columns_warning'],
                        temp['conformity_explain_columns_quality_result'],
                        temp['conformity_measurement_total_rows'],
                        temp['conformity_measurement_total_columns'],
                        temp['conformity_measurement_total_cells'],
                        temp['conformity_measurement_total_valid'],
                        temp['conformity_measurement_total_not_valid'],
                        temp['conformity_measurement_warning'],
                        temp['conformity_measurement_quality_result'],
                        temp['conformity_serving_rate_total_rows'],
                        temp['conformity_serving_rate_total_columns'],
                        temp['conformity_serving_rate_total_cells'],
                        temp['conformity_serving_rate_total_valid'],
                        temp['conformity_serving_rate_total_not_valid'],
                        temp['conformity_serving_rate_warning'],
                        temp['conformity_serving_rate_quality_result'],
                        temp['conformity_scope_total_rows'],
                        temp['conformity_scope_total_columns'],
                        temp['conformity_scope_total_cells'],
                        temp['conformity_scope_total_valid'],
                        temp['conformity_scope_total_not_valid'],
                        temp['conformity_scope_warning'],
                        temp['conformity_scope_quality_result'],
                        temp['uniqueness_result'],
                        temp['uniqueness_duplicated_total_rows'],
                        temp['uniqueness_duplicated_total_columns'],
                        temp['uniqueness_duplicated_total_cells'],
                        temp['uniqueness_duplicated_total_valid'],
                        temp['uniqueness_duplicated_total_not_valid'],
                        temp['uniqueness_duplicated_warning'],
                        temp['uniqueness_duplicated_quality_result'],
                        temp['completeness_result'],
                        temp['completeness_filled_total_rows'],
                        temp['completeness_filled_total_columns'],
                        temp['completeness_filled_total_cells'],
                        temp['completeness_filled_total_valid'],
                        temp['completeness_filled_total_not_valid'],
                        temp['completeness_filled_warning'],
                        temp['completeness_filled_quality_result'],
                        temp['consistency_result'],
                        temp['consistency_unit_total_rows'],
                        temp['consistency_unit_total_columns'],
                        temp['consistency_unit_total_cells'],
                        temp['consistency_unit_total_valid'],
                        temp['consistency_unit_total_not_valid'],
                        temp['consistency_unit_warning'],
                        temp['consistency_unit_quality_result'],
                        temp['consistency_listing_province_total_rows'],
                        temp['consistency_listing_province_total_columns'],
                        temp['consistency_listing_province_total_cells'],
                        temp['consistency_listing_province_total_valid'],
                        temp['consistency_listing_province_total_not_valid'],
                        temp['consistency_listing_province_warning'],
                        temp['consistency_listing_province_quality_result'],
                        temp['consistency_time_series_total_rows'],
                        temp['consistency_time_series_total_columns'],
                        temp['consistency_time_series_total_cells'],
                        temp['consistency_time_series_total_valid'],
                        temp['consistency_time_series_total_not_valid'],
                        temp['consistency_time_series_warning'],
                        temp['consistency_time_series_quality_result'],
                        temp['timeliness_result'],
                        temp['timeliness_updated_total_rows'],
                        temp['timeliness_updated_total_columns'],
                        temp['timeliness_updated_total_cells'],
                        temp['timeliness_updated_total_valid'],
                        temp['timeliness_updated_total_not_valid'],
                        temp['timeliness_updated_warning'],
                        temp['timeliness_updated_quality_result'],
                        temp['id']
                    )
                    # print(query)
                    # print(value)
                    cursor_satudata.execute(query, value)
                    connection_satudata.commit()
                except Exception as err:
                    print('gagal update result dataset quality')
                    print(err)
                    connection_satudata.rollback()
                # insert history
                save_history(temp)
                return True, temp, 'Sukses Update Dataset Quality Result'
            else:
                try:
                    query = "insert into dataset_quality_results ("
                    query += '"id", '
                    query += '"unit", '
                    query += '"unit_column", '
                    query += '"value_column", '
                    query += '"name", '
                    query += '"schema", '
                    query += '"table", '
                    query += '"tag", '
                    query += '"description", '
                    query += '"metadata", '
                    query += '"category", '
                    query += '"code_area_level", '
                    query += '"column_time_series", '
                    query += '"error", '
                    query += '"is_config_complete", '
                    query += '"time_series_type", '
                    query += '"final_result", '
                    query += '"conformity_result", '
                    query += '"conformity_code_area_total_rows", '
                    query += '"conformity_code_area_total_columns", '
                    query += '"conformity_code_area_total_cells", '
                    query += '"conformity_code_area_total_valid", '
                    query += '"conformity_code_area_total_not_valid", '
                    query += '"conformity_code_area_warning", '
                    query += '"conformity_code_area_quality_result", '
                    query += '"conformity_explain_columns_total_rows", '
                    query += '"conformity_explain_columns_total_columns", '
                    query += '"conformity_explain_columns_total_cells", '
                    query += '"conformity_explain_columns_total_valid", '
                    query += '"conformity_explain_columns_total_not_valid", '
                    query += '"conformity_explain_columns_warning", '
                    query += '"conformity_explain_columns_quality_result", '
                    query += '"conformity_measurement_total_rows", '
                    query += '"conformity_measurement_total_columns", '
                    query += '"conformity_measurement_total_cells", '
                    query += '"conformity_measurement_total_valid", '
                    query += '"conformity_measurement_total_not_valid", '
                    query += '"conformity_measurement_warning", '
                    query += '"conformity_measurement_quality_result", '
                    query += '"conformity_serving_rate_total_rows", '
                    query += '"conformity_serving_rate_total_columns", '
                    query += '"conformity_serving_rate_total_cells", '
                    query += '"conformity_serving_rate_total_valid", '
                    query += '"conformity_serving_rate_total_not_valid", '
                    query += '"conformity_serving_rate_warning", '
                    query += '"conformity_serving_rate_quality_result", '
                    query += '"conformity_scope_total_rows", '
                    query += '"conformity_scope_total_columns", '
                    query += '"conformity_scope_total_cells", '
                    query += '"conformity_scope_total_valid", '
                    query += '"conformity_scope_total_not_valid", '
                    query += '"conformity_scope_warning", '
                    query += '"conformity_scope_quality_result", '
                    query += '"uniqueness_result", '
                    query += '"uniqueness_duplicated_total_rows", '
                    query += '"uniqueness_duplicated_total_columns", '
                    query += '"uniqueness_duplicated_total_cells", '
                    query += '"uniqueness_duplicated_total_valid", '
                    query += '"uniqueness_duplicated_total_not_valid", '
                    query += '"uniqueness_duplicated_warning", '
                    query += '"uniqueness_duplicated_quality_result", '
                    query += '"completeness_result", '
                    query += '"completeness_filled_total_rows", '
                    query += '"completeness_filled_total_columns", '
                    query += '"completeness_filled_total_cells", '
                    query += '"completeness_filled_total_valid", '
                    query += '"completeness_filled_total_not_valid", '
                    query += '"completeness_filled_warning", '
                    query += '"completeness_filled_quality_result", '
                    query += '"consistency_result", '
                    query += '"consistency_unit_total_rows", '
                    query += '"consistency_unit_total_columns", '
                    query += '"consistency_unit_total_cells", '
                    query += '"consistency_unit_total_valid", '
                    query += '"consistency_unit_total_not_valid", '
                    query += '"consistency_unit_warning", '
                    query += '"consistency_unit_quality_result", '
                    query += '"consistency_listing_province_total_rows", '
                    query += '"consistency_listing_province_total_columns", '
                    query += '"consistency_listing_province_total_cells", '
                    query += '"consistency_listing_province_total_valid", '
                    query += '"consistency_listing_province_total_not_valid", '
                    query += '"consistency_listing_province_warning", '
                    query += '"consistency_listing_province_quality_result", '
                    query += '"consistency_time_series_total_rows", '
                    query += '"consistency_time_series_total_columns", '
                    query += '"consistency_time_series_total_cells", '
                    query += '"consistency_time_series_total_valid", '
                    query += '"consistency_time_series_total_not_valid", '
                    query += '"consistency_time_series_warning", '
                    query += '"consistency_time_series_quality_result", '
                    query += '"timeliness_result", '
                    query += '"timeliness_updated_total_rows", '
                    query += '"timeliness_updated_total_columns", '
                    query += '"timeliness_updated_total_cells", '
                    query += '"timeliness_updated_total_valid", '
                    query += '"timeliness_updated_total_not_valid", '
                    query += '"timeliness_updated_warning", '
                    query += '"timeliness_updated_quality_result" '
                    query += ") values ("
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
                    query += "%s, %s, %s, %s, %s, %s, %s, %s, %s "
                    query += ")"
                    value = (
                        temp['id'],
                        temp['unit'],
                        temp['unit_column'],
                        temp['value_column'],
                        temp['name'],
                        temp['schema'],
                        temp['table'],
                        temp['tag'],
                        temp['description'],
                        temp['metadata_'],
                        temp['category'],
                        temp['code_area_level'],
                        temp['column_time_series'],
                        temp['error'],
                        temp['is_config_complete'],
                        temp['time_series_type'],
                        temp['final_result'],
                        temp['conformity_result'],
                        temp['conformity_code_area_total_rows'],
                        temp['conformity_code_area_total_columns'],
                        temp['conformity_code_area_total_cells'],
                        temp['conformity_code_area_total_valid'],
                        temp['conformity_code_area_total_not_valid'],
                        temp['conformity_code_area_warning'],
                        temp['conformity_code_area_quality_result'],
                        temp['conformity_explain_columns_total_rows'],
                        temp['conformity_explain_columns_total_columns'],
                        temp['conformity_explain_columns_total_cells'],
                        temp['conformity_explain_columns_total_valid'],
                        temp['conformity_explain_columns_total_not_valid'],
                        temp['conformity_explain_columns_warning'],
                        temp['conformity_explain_columns_quality_result'],
                        temp['conformity_measurement_total_rows'],
                        temp['conformity_measurement_total_columns'],
                        temp['conformity_measurement_total_cells'],
                        temp['conformity_measurement_total_valid'],
                        temp['conformity_measurement_total_not_valid'],
                        temp['conformity_measurement_warning'],
                        temp['conformity_measurement_quality_result'],
                        temp['conformity_serving_rate_total_rows'],
                        temp['conformity_serving_rate_total_columns'],
                        temp['conformity_serving_rate_total_cells'],
                        temp['conformity_serving_rate_total_valid'],
                        temp['conformity_serving_rate_total_not_valid'],
                        temp['conformity_serving_rate_warning'],
                        temp['conformity_serving_rate_quality_result'],
                        temp['conformity_scope_total_rows'],
                        temp['conformity_scope_total_columns'],
                        temp['conformity_scope_total_cells'],
                        temp['conformity_scope_total_valid'],
                        temp['conformity_scope_total_not_valid'],
                        temp['conformity_scope_warning'],
                        temp['conformity_scope_quality_result'],
                        temp['uniqueness_result'],
                        temp['uniqueness_duplicated_total_rows'],
                        temp['uniqueness_duplicated_total_columns'],
                        temp['uniqueness_duplicated_total_cells'],
                        temp['uniqueness_duplicated_total_valid'],
                        temp['uniqueness_duplicated_total_not_valid'],
                        temp['uniqueness_duplicated_warning'],
                        temp['uniqueness_duplicated_quality_result'],
                        temp['completeness_result'],
                        temp['completeness_filled_total_rows'],
                        temp['completeness_filled_total_columns'],
                        temp['completeness_filled_total_cells'],
                        temp['completeness_filled_total_valid'],
                        temp['completeness_filled_total_not_valid'],
                        temp['completeness_filled_warning'],
                        temp['completeness_filled_quality_result'],
                        temp['consistency_result'],
                        temp['consistency_unit_total_rows'],
                        temp['consistency_unit_total_columns'],
                        temp['consistency_unit_total_cells'],
                        temp['consistency_unit_total_valid'],
                        temp['consistency_unit_total_not_valid'],
                        temp['consistency_unit_warning'],
                        temp['consistency_unit_quality_result'],
                        temp['consistency_listing_province_total_rows'],
                        temp['consistency_listing_province_total_columns'],
                        temp['consistency_listing_province_total_cells'],
                        temp['consistency_listing_province_total_valid'],
                        temp['consistency_listing_province_total_not_valid'],
                        temp['consistency_listing_province_warning'],
                        temp['consistency_listing_province_quality_result'],
                        temp['consistency_time_series_total_rows'],
                        temp['consistency_time_series_total_columns'],
                        temp['consistency_time_series_total_cells'],
                        temp['consistency_time_series_total_valid'],
                        temp['consistency_time_series_total_not_valid'],
                        temp['consistency_time_series_warning'],
                        temp['consistency_time_series_quality_result'],
                        temp['timeliness_result'],
                        temp['timeliness_updated_total_rows'],
                        temp['timeliness_updated_total_columns'],
                        temp['timeliness_updated_total_cells'],
                        temp['timeliness_updated_total_valid'],
                        temp['timeliness_updated_total_not_valid'],
                        temp['timeliness_updated_warning'],
                        temp['timeliness_updated_quality_result']
                    )
                    # print(query)
                    # print(value)
                    cursor_satudata.execute(query, value)
                    connection_satudata.commit()
                except Exception as err:
                    print('gagal insert result dataset quality')
                    print(err)
                    connection_satudata.rollback()
                # insert history
                save_history(temp)
                return True, temp, 'Sukses Insert Dataset Quality Result'
        except Exception as e:
            print('gagal count dataset quality')
            print(e)
            connection_satudata.rollback()

    except Exception as err:
        # fail response
        return False, {}, "Error save quality, " + str(err)

def save_history(temp):
    current_time = datetime.datetime.now(timezone('Asia/Jakarta'))
    current_time_utc = current_time.strftime('%Y-%m-%d %H:%M:%S')
    temp['cdate'] = current_time_utc
    temp['mdate'] = current_time_utc
    try:
        query = "insert into dataset_quality_results_history ("
        query += '"dataset_id", '
        query += '"cdate", '
        query += '"mdate", '
        query += '"unit", '
        query += '"unit_column", '
        query += '"value_column", '
        query += '"name", '
        query += '"schema", '
        query += '"table", '
        query += '"tag", '
        query += '"description", '
        query += '"metadata", '
        query += '"category", '
        query += '"code_area_level", '
        query += '"column_time_series", '
        query += '"error", '
        query += '"is_config_complete", '
        query += '"time_series_type", '
        query += '"final_result", '
        query += '"conformity_result", '
        query += '"conformity_code_area_total_rows", '
        query += '"conformity_code_area_total_columns", '
        query += '"conformity_code_area_total_cells", '
        query += '"conformity_code_area_total_valid", '
        query += '"conformity_code_area_total_not_valid", '
        query += '"conformity_code_area_warning", '
        query += '"conformity_code_area_quality_result", '
        query += '"conformity_explain_columns_total_rows", '
        query += '"conformity_explain_columns_total_columns", '
        query += '"conformity_explain_columns_total_cells", '
        query += '"conformity_explain_columns_total_valid", '
        query += '"conformity_explain_columns_total_not_valid", '
        query += '"conformity_explain_columns_warning", '
        query += '"conformity_explain_columns_quality_result", '
        query += '"conformity_measurement_total_rows", '
        query += '"conformity_measurement_total_columns", '
        query += '"conformity_measurement_total_cells", '
        query += '"conformity_measurement_total_valid", '
        query += '"conformity_measurement_total_not_valid", '
        query += '"conformity_measurement_warning", '
        query += '"conformity_measurement_quality_result", '
        query += '"conformity_serving_rate_total_rows", '
        query += '"conformity_serving_rate_total_columns", '
        query += '"conformity_serving_rate_total_cells", '
        query += '"conformity_serving_rate_total_valid", '
        query += '"conformity_serving_rate_total_not_valid", '
        query += '"conformity_serving_rate_warning", '
        query += '"conformity_serving_rate_quality_result", '
        query += '"conformity_scope_total_rows", '
        query += '"conformity_scope_total_columns", '
        query += '"conformity_scope_total_cells", '
        query += '"conformity_scope_total_valid", '
        query += '"conformity_scope_total_not_valid", '
        query += '"conformity_scope_warning", '
        query += '"conformity_scope_quality_result", '
        query += '"uniqueness_result", '
        query += '"uniqueness_duplicated_total_rows", '
        query += '"uniqueness_duplicated_total_columns", '
        query += '"uniqueness_duplicated_total_cells", '
        query += '"uniqueness_duplicated_total_valid", '
        query += '"uniqueness_duplicated_total_not_valid", '
        query += '"uniqueness_duplicated_warning", '
        query += '"uniqueness_duplicated_quality_result", '
        query += '"completeness_result", '
        query += '"completeness_filled_total_rows", '
        query += '"completeness_filled_total_columns", '
        query += '"completeness_filled_total_cells", '
        query += '"completeness_filled_total_valid", '
        query += '"completeness_filled_total_not_valid", '
        query += '"completeness_filled_warning", '
        query += '"completeness_filled_quality_result", '
        query += '"consistency_result", '
        query += '"consistency_unit_total_rows", '
        query += '"consistency_unit_total_columns", '
        query += '"consistency_unit_total_cells", '
        query += '"consistency_unit_total_valid", '
        query += '"consistency_unit_total_not_valid", '
        query += '"consistency_unit_warning", '
        query += '"consistency_unit_quality_result", '
        query += '"consistency_listing_province_total_rows", '
        query += '"consistency_listing_province_total_columns", '
        query += '"consistency_listing_province_total_cells", '
        query += '"consistency_listing_province_total_valid", '
        query += '"consistency_listing_province_total_not_valid", '
        query += '"consistency_listing_province_warning", '
        query += '"consistency_listing_province_quality_result", '
        query += '"consistency_time_series_total_rows", '
        query += '"consistency_time_series_total_columns", '
        query += '"consistency_time_series_total_cells", '
        query += '"consistency_time_series_total_valid", '
        query += '"consistency_time_series_total_not_valid", '
        query += '"consistency_time_series_warning", '
        query += '"consistency_time_series_quality_result", '
        query += '"timeliness_result", '
        query += '"timeliness_updated_total_rows", '
        query += '"timeliness_updated_total_columns", '
        query += '"timeliness_updated_total_cells", '
        query += '"timeliness_updated_total_valid", '
        query += '"timeliness_updated_total_not_valid", '
        query += '"timeliness_updated_warning", '
        query += '"timeliness_updated_quality_result" '
        query += ") values ("
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, "
        query += "%s"
        query += ")"
        value = (
            temp['id'],
            temp['cdate'],
            temp['mdate'],
            temp['unit'],
            temp['unit_column'],
            temp['value_column'],
            temp['name'],
            temp['schema'],
            temp['table'],
            temp['tag'],
            temp['description'],
            temp['metadata_'],
            temp['category'],
            temp['code_area_level'],
            temp['column_time_series'],
            temp['error'],
            temp['is_config_complete'],
            temp['time_series_type'],
            temp['final_result'],
            temp['conformity_result'],
            temp['conformity_code_area_total_rows'],
            temp['conformity_code_area_total_columns'],
            temp['conformity_code_area_total_cells'],
            temp['conformity_code_area_total_valid'],
            temp['conformity_code_area_total_not_valid'],
            temp['conformity_code_area_warning'],
            temp['conformity_code_area_quality_result'],
            temp['conformity_explain_columns_total_rows'],
            temp['conformity_explain_columns_total_columns'],
            temp['conformity_explain_columns_total_cells'],
            temp['conformity_explain_columns_total_valid'],
            temp['conformity_explain_columns_total_not_valid'],
            temp['conformity_explain_columns_warning'],
            temp['conformity_explain_columns_quality_result'],
            temp['conformity_measurement_total_rows'],
            temp['conformity_measurement_total_columns'],
            temp['conformity_measurement_total_cells'],
            temp['conformity_measurement_total_valid'],
            temp['conformity_measurement_total_not_valid'],
            temp['conformity_measurement_warning'],
            temp['conformity_measurement_quality_result'],
            temp['conformity_serving_rate_total_rows'],
            temp['conformity_serving_rate_total_columns'],
            temp['conformity_serving_rate_total_cells'],
            temp['conformity_serving_rate_total_valid'],
            temp['conformity_serving_rate_total_not_valid'],
            temp['conformity_serving_rate_warning'],
            temp['conformity_serving_rate_quality_result'],
            temp['conformity_scope_total_rows'],
            temp['conformity_scope_total_columns'],
            temp['conformity_scope_total_cells'],
            temp['conformity_scope_total_valid'],
            temp['conformity_scope_total_not_valid'],
            temp['conformity_scope_warning'],
            temp['conformity_scope_quality_result'],
            temp['uniqueness_result'],
            temp['uniqueness_duplicated_total_rows'],
            temp['uniqueness_duplicated_total_columns'],
            temp['uniqueness_duplicated_total_cells'],
            temp['uniqueness_duplicated_total_valid'],
            temp['uniqueness_duplicated_total_not_valid'],
            temp['uniqueness_duplicated_warning'],
            temp['uniqueness_duplicated_quality_result'],
            temp['completeness_result'],
            temp['completeness_filled_total_rows'],
            temp['completeness_filled_total_columns'],
            temp['completeness_filled_total_cells'],
            temp['completeness_filled_total_valid'],
            temp['completeness_filled_total_not_valid'],
            temp['completeness_filled_warning'],
            temp['completeness_filled_quality_result'],
            temp['consistency_result'],
            temp['consistency_unit_total_rows'],
            temp['consistency_unit_total_columns'],
            temp['consistency_unit_total_cells'],
            temp['consistency_unit_total_valid'],
            temp['consistency_unit_total_not_valid'],
            temp['consistency_unit_warning'],
            temp['consistency_unit_quality_result'],
            temp['consistency_listing_province_total_rows'],
            temp['consistency_listing_province_total_columns'],
            temp['consistency_listing_province_total_cells'],
            temp['consistency_listing_province_total_valid'],
            temp['consistency_listing_province_total_not_valid'],
            temp['consistency_listing_province_warning'],
            temp['consistency_listing_province_quality_result'],
            temp['consistency_time_series_total_rows'],
            temp['consistency_time_series_total_columns'],
            temp['consistency_time_series_total_cells'],
            temp['consistency_time_series_total_valid'],
            temp['consistency_time_series_total_not_valid'],
            temp['consistency_time_series_warning'],
            temp['consistency_time_series_quality_result'],
            temp['timeliness_result'],
            temp['timeliness_updated_total_rows'],
            temp['timeliness_updated_total_columns'],
            temp['timeliness_updated_total_cells'],
            temp['timeliness_updated_total_valid'],
            temp['timeliness_updated_total_not_valid'],
            temp['timeliness_updated_warning'],
            temp['timeliness_updated_quality_result']
        )
        # print(query)
        # print(value)
        cursor_satudata.execute(query, value)
        connection_satudata.commit()
    except Exception as err:
        print('gagal save history dataset quality')
        print(err)
        connection_satudata.rollback()

def get_unit_column(columns):
    unit_column = 'satuan' if 'satuan' in columns else None
    return unit_column

def get_value_column(columns, pengukuran_dataset):
    match = Matching()
    must_word = [
        "agregat", "akumulasi", "alokasi", "anggaran", "angka", "average", "bed", "dana",
        "distribusi", "gini", "gross", "indeks", "jumlah", "kepadatan", "keterisian", "laju",
        "laporan", "lebar", "luas", "net", "nilai", "ntp", "panjang", "peningkatan", "perbandingan",
        "perkembangan", "persentase", "pertumbuhan", "populasi", "produk", "produksi", "produktivitas",
        "proporsi", "proyeksi", "rasio", "rata_rata", "realisasi", "rekap", "rekapitulasi", "tingkat",
        "tren", "turn", "volume",
        "jarak", "waktu", "frekuensi"
    ]
    columns_filter = []
    for word in must_word:
        for column in columns:
            if word in column:
                columns_filter.append(column)
    result_match = [match.levenshtein_match(column, pengukuran_dataset)['score'] for column in columns_filter]
    try:
        indices = [index for index, item in enumerate(result_match) if item == max(result_match)][0]
        return columns_filter[indices]
    except Exception as e:
        print(e)
        return None

def filter_columns_time_series(columns, keyword):
    list_column = [col for col in columns if keyword in col]
    if len(list_column) > 1:
        list_column = [col for col in list_column if 'kode' not in col]
    filtered_columns = list_column[0] if len(list_column) > 0 else None
    return filtered_columns

def time_series(columns, frekuensi_dataset):
    if 'tahun' in frekuensi_dataset.lower() or 'semester' in frekuensi_dataset.lower():
        result = {
            'time_series_type':'years',
            'column_time_series': {
                'years_column':  filter_columns_time_series(columns, 'tahun'),
                'months_column': None,
                'dates_column': None
            }
        }
    elif 'bulan' in frekuensi_dataset.lower():
        result = {
            'time_series_type':'months',
            'column_time_series': {
                'years_column':  filter_columns_time_series(columns, 'tahun'),
                'months_column': filter_columns_time_series(columns, 'bulan'),
                'dates_column': None
            }
        }
    elif 'hari' in frekuensi_dataset.lower():
        result = {
            'time_series_type':'dates',
            'column_time_series': {
                'years_column': None,
                'months_column': None,
                'dates_column': filter_columns_time_series(columns, 'tanggal')
            }
        }
    else:
        result = {
            'time_series_type': None,
            'column_time_series': {
                'years_column': None,
                'months_column': None,
                'dates_column': None
            }
        }
    return result

def get_code_area_level(columns):
    if 'nama_kabupaten_kota' in columns and 'kode_kabupaten_kota' in columns:
        code_area_level = 'city'
    elif 'nama_provinsi' in columns and 'kode_provinsi' in columns:
        code_area_level = 'province'
    else:
        code_area_level = None
    return code_area_level

def clean_description(description):
    description = BeautifulSoup(description, features="html.parser").get_text().strip()
    description = unidecode.unidecode(description).split()
    return ' '.join(description)


if __name__ == '__main__':
    ''' Doc '''
    # star timer
    START_TIME = time.time()
    print("--- Please wait, processing data ---")
    print("")

    _id = 20128
    try:
        # get dataset
        res, dataset, message = dataset_get_by_id_table(_id)

        # get meta
        meta = {}
        res_count, counts, column_list = bigdata_get_count(dataset['schema'], dataset['table'])
        meta['total_record'] = int(counts)

        # get bidata
        res, bigdata = bigdata_get_all(dataset['schema'], dataset['table'], meta['total_record'], column_list)

        # create quality score
        (res_score, quality_score, msg_score) = create_quality_score(dataset, bigdata, dataset['metadata'])

        response = {
            "message": "sukses",
            "error": 0,
            "data": {
                'res_score': res_score,
                'quality_score': quality_score,
                'msg_score': msg_score,
            },
            "meta": {},
        }
        print(response)
    except Exception as err:
        # fail response
        response = {
            "message": "error " + str(err),
            "error": 1,
            "data": [],
            "meta": {},
        }
        print(response)

    # print execution time
    print("")
    print("--- Finish in %s seconds ---" % (time.time() - START_TIME))
