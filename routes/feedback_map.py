''' Doc: route feedback map'''
import json
from flask import request, Blueprint, Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import FeedbackMapController
from controllers import FeedbackMapMasalahController
from settings import configuration as conf

BP = Blueprint('feedback_map', __name__)
FEEDBACK_MAP_CONTROLLER = FeedbackMapController()
FEEDBACK_MAP_MASALAH_CONTROLLER = FeedbackMapMasalahController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable


@BP.route("/feedback_map", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, feedback_map, message = FEEDBACK_MAP_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map", methods=["POST"])
# @jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if 'masalah' in json_request:
            masalahs = json_request['masalah']
            json_request.pop('masalah', None)

        # insert history draft
        res, feedback_map = FEEDBACK_MAP_CONTROLLER.create(json_request)

        # response
        if res:

            # list feedback_map masalah
            feedback_map[0]['masalah'] = masalahs

            # list masalah
            if isinstance(masalahs, list):
                for msl in masalahs:
                    json_masalah = {}
                    json_masalah['feedback_map_id'] = feedback_map[0]['id']
                    json_masalah['masalah'] = msl
                    res, feedback_map_masalah = FEEDBACK_MAP_MASALAH_CONTROLLER.create(json_masalah)

            # success response
            response = {"message": "Create data successfull", "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history draft
        res, feedback_map = FEEDBACK_MAP_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history draft
        res = FEEDBACK_MAP_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/info", methods=["GET"])
# @jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        feedback_map = {}
        feedback_map['tujuan'] = []
        feedback_map['sektor'] = []
        feedback_map['masalah'] = []

        feedback_map['tujuan'].append({'nama': 'Mencari informasi data geospasial terbuka Pemdaprov Jawa Barat untuk kepentingan bisnis, perumusan kebijakan, atau referensi pribadi lainnya.'})
        feedback_map['tujuan'].append({'nama': 'Mencari informasi data geospasial terbuka Pemdaprov Jawa Barat untuk kepentingan bahan ajar/kurikulum/tugas akademik.'})
        feedback_map['tujuan'].append({'nama': 'Mempelajari lebih lanjut terkait transparansi data geospasial yang dimiliki oleh Pemdaprov Jabar.'})

        feedback_map['sektor'].append({'nama': 'Peneliti/Akademisi'})
        feedback_map['sektor'].append({'nama': 'Pemerintahan'})
        feedback_map['sektor'].append({'nama': 'Media'})
        feedback_map['sektor'].append({'nama': 'Industri/Bisnis'})
        feedback_map['sektor'].append({'nama': 'Organisasi Non Profit/Sosial'})

        feedback_map['masalah'].append({'nama': 'Dataset/Peta tidak dapat diakses'})
        feedback_map['masalah'].append({'nama': 'Informasi dirasa terlalu membingungkan'})
        feedback_map['masalah'].append({'nama': 'Informasi yang dicari tidak dapat ditemukan'})
        feedback_map['masalah'].append({'nama': 'Tidak tau jika informasi yang tersedia akurat atau terbaru'})

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_total", methods=["GET"])
@jwt_check()
def get_chart_total():
    ''' Doc: function get chart total'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_total(start_date, end_date)

        # response
        if res:
            # perhitungan
            result = {}
            result['total'] = feedback_map

            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_score", methods=["GET"])
@jwt_check()
def get_chart_score():
    ''' Doc: function get chart score'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_score(start_date, end_date)

        # response
        if res:
            # perhitungan
            result = {}
            result['detail'] = feedback_map
            result['count'] = 0
            result['max'] = 0
            result['total'] = 0
            result['average'] = 0

            # perhitungan
            for fm_ in feedback_map:
                result['count'] += fm_['count']
                result['total'] += fm_['score'] * fm_['count']

            result['max'] = result['count'] * 4
            result['average'] = round((result['total'] / result['max']) * 100, 2)

            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_tujuan", methods=["GET"])
@jwt_check()
def get_chart_tujuan():
    ''' Doc: function get chart tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_tujuan(start_date, end_date)

        feedback_maps = []
        feedback_maps.append({"count":0, "tujuan": "Mencari informasi data geospasial terbuka Pemdaprov Jawa Barat untuk kepentingan bisnis, perumusan kebijakan, atau referensi pribadi lainnya."})
        feedback_maps.append({"count":0, "tujuan": "Mencari informasi data geospasial terbuka Pemdaprov Jawa Barat untuk kepentingan bahan ajar/kurikulum/tugas akademik."})
        feedback_maps.append({"count":0, "tujuan": "Mempelajari lebih lanjut terkait transparansi data geospasial yang dimiliki oleh Pemdaprov Jabar."})
        feedback_maps.append({"count":0, "tujuan": "Lainnya"})

        for fb_ in feedback_map:
            if fb_['tujuan'] == "Mencari informasi data geospasial terbuka Pemdaprov Jawa Barat untuk kepentingan bisnis, perumusan kebijakan, atau referensi pribadi lainnya.":
                feedback_maps[0]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mencari informasi data geospasial terbuka Pemdaprov Jawa Barat untuk kepentingan bahan ajar/kurikulum/tugas akademik.":
                feedback_maps[1]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mempelajari lebih lanjut terkait transparansi data geospasial yang dimiliki oleh Pemdaprov Jabar.":
                feedback_maps[2]['count'] = fb_['count']
            else:
                feedback_maps[3]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_maps}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_tujuan_tercapai", methods=["GET"])
@jwt_check()
def get_chart_tujuan_tercapai():
    ''' Doc: function get chart tujuan tercapai'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_tujuan_tercapai(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_tujuan_mudah_ditemukan", methods=["GET"])
@jwt_check()
def get_chart_tujuan_mudah_ditemukan():
    ''' Doc: function get chart tujuan mudah ditemukan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_tujuan_mudah_ditemukan(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_sektor", methods=["GET"])
@jwt_check()
def get_chart_sektor():
    ''' Doc: function get chart sektor'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_sektor(start_date, end_date)

        feedback_maps = []
        feedback_maps.append({"count":0, "sektor": "Peneliti/Akademisi"})
        feedback_maps.append({"count":0, "sektor": "Pemerintahan"})
        feedback_maps.append({"count":0, "sektor": "Media"})
        feedback_maps.append({"count":0, "sektor": "Industri/Bisnis"})
        feedback_maps.append({"count":0, "sektor": "Organisasi Non Profit/Sosial"})
        feedback_maps.append({"count":0, "sektor": "Lainnya"})

        for fb_ in feedback_map:
            if fb_['sektor'] == "Peneliti/Akademisi":
                feedback_maps[0]['count'] = fb_['count']
            elif fb_['sektor'] == "Pemerintahan":
                feedback_maps[1]['count'] = fb_['count']
            elif fb_['sektor'] == "Media":
                feedback_maps[2]['count'] = fb_['count']
            elif fb_['sektor'] == "Industri/Bisnis":
                feedback_maps[3]['count'] = fb_['count']
            elif fb_['sektor'] == "Organisasi Non Profit/Sosial":
                feedback_maps[4]['count'] = fb_['count']
            else:
                feedback_maps[5]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_maps}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/chart_harian", methods=["GET"])
@jwt_check()
def get_chart_harian():
    ''' Doc: function get chart harian'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_chart_harian(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": feedback_map}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_map/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback_map = FEEDBACK_MAP_CONTROLLER.get_download(start_date, end_date)
        metadata = ["Id", "Datetime", "Score", "Tujuan", "Tujuan Tercapai", "Tujuan Mudah Ditemukan", "Sektor", "Saran",
                    "Dataset/Peta tidak dapat diakses", "Informasi dirasa terlalu membingungkan",
                    "Informasi yang dicari tidak dapat ditemukan", "Tidak tau jika informasi yang tersedia akurat atau terbaru",
                    "Lainnya", "is_active", "is_deleted"]

        # response
        if res:
            # success response
            filename = HELPER.convert_xls(
                'static/download/feedback_map.xlsx', feedback_map, metadata)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='feedback_map.xlsx',
                as_attachment=True
            )
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
