''' Doc: route regional level'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import RegionalLevelController
from settings import configuration as conf

BP = Blueprint('regional_level', __name__)
REGIONAL_LEVEL_CONTROLLER = RegionalLevelController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/regional_level", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, regional_level = REGIONAL_LEVEL_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, regional_level = REGIONAL_LEVEL_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": regional_level}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/regional_level/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, regional_level = REGIONAL_LEVEL_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": regional_level}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/regional_level", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert regional_level
        res, regional_level = REGIONAL_LEVEL_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": regional_level}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/regional_level/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update regional_level
        res, regional_level = REGIONAL_LEVEL_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": regional_level}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/regional_level/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete regional_level
        res = REGIONAL_LEVEL_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
