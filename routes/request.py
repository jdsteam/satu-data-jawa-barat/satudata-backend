''' Doc: route request'''
import json
import requests
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
from settings import configuration
from controllers import UserController
from controllers import RequestController
from controllers import HistoryRequestController
from controllers import NotificationController
from models import RoleModel
from models import UserModel
from settings import configuration as conf

BP = Blueprint('request', __name__)
USER_CONTROLLER = UserController()
REQUEST_CONTROLLER = RequestController()
HISTORY_REQUEST_CONTROLLER = HistoryRequestController()
NOTIFICATION_CONTROLLER = NotificationController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/request", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, result_request = REQUEST_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, result_request = REQUEST_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/request/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, result_request, message = REQUEST_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": result_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/request", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert request
        res, result_request = REQUEST_CONTROLLER.create(json_request)

        # insert history request
        if res:
            json_history_request = {}
            json_history_request['type_id'] = result_request['id']
            json_history_request['type'] = 'request'
            json_history_request['category'] = 1
            HISTORY_REQUEST_CONTROLLER.create(json_history_request)

        # insert to notification
        if res:
            jwt = HELPER.read_jwt()
            # get user
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)
            data_user = data_user.filter(RoleModel.name == 'walidata')
            data_user = data_user.all()

            for dr_ in data_user:
                temp = dr_._asdict()
                user_id = temp['id']
                json_notification = {}
                json_notification['feature'] = 3
                json_notification['type'] = 'request'
                json_notification['type_id'] = result_request['id']
                json_notification['sender'] = jwt['id']
                json_notification['receiver'] = user_id
                json_notification['title'] = jwt['nama_skpd'] + " mengajukan permohonan data "
                json_notification['content'] = " kepada " + result_request['nama_skpd']
                json_notification['is_read'] = False
                json_notification['cdate'] = HELPER.local_date_server()
                json_notification['mdate'] = HELPER.local_date_server()
                result, notification = NOTIFICATION_CONTROLLER.create(json_notification)

                # update notif
                param = {}
                param['count_notif'] = 0
                res_, user = USER_CONTROLLER.get_by_id(user_id)
                if res_:
                    if not user['count_notif']:
                        user['count_notif'] = 0
                    param['count_notif'] = user['count_notif'] + 1
                    # execute database
                    result = db.session.query(UserModel)
                    result = result.filter(getattr(UserModel, 'id') == user_id)
                    result = result.update(param, synchronize_session='fetch')
                    result = db.session.commit()

                try:
                    socket_url = configuration.SOCKET_URL
                    token = request.headers.get('Authorization', None)
                    requests.get(
                        socket_url + 'notification' + '?receiver=' + str(notification['receiver']),
                        headers={"Authorization":token, "Content-Type": "application/json"},
                        data=json.dumps(notification)
                    )
                except Exception as err:
                    print(err)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": result_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/request/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update request
        res, result_request = REQUEST_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": result_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/request/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete request
        res = REQUEST_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
