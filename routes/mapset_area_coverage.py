''' Doc: route mapset award'''
import json
from flask import Blueprint, request, Response
from controllers import MapsetAreaCoverageController
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from settings import configuration as conf

BP = Blueprint('mapset_area_coverage', __name__)
MAPSET_AREA_COVERAGE = MapsetAreaCoverageController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route('/mapset_area_coverage', methods=['GET'])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get('where', '{}')
        sort = request.args.get('sort', 'id:ASC')
        limit = request.args.get('limit', 100)
        skip = request.args.get('skip', 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", '"')
        where = json.loads(where)

        if count:
            # call function get
            res, mapset_area_coverage = MAPSET_AREA_COVERAGE.get_count(where, search)
        else:
            # call function get
            res, mapset_area_coverage = MAPSET_AREA_COVERAGE.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {'message': conf.MESSAGE_DATA_SUCCESS, 'error': 0, 'data': mapset_area_coverage}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_area_coverage/<_id>', methods=['GET'])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, mapset_area_coverage = MAPSET_AREA_COVERAGE.get_by_id(_id)

        # response
        if res:
            # success response
            response = {'message': 'Get detail data successfull', 'error': 0, 'data': mapset_area_coverage}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_area_coverage', methods=['POST'])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert mapset_area_coverage
        res, mapset_area_coverage = MAPSET_AREA_COVERAGE.create(json_request)

        # response
        if res:
            # success response
            response = {'message': 'Create data successfull', 'error': 0, 'data': mapset_area_coverage}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Create data failed, ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_area_coverage/<_id>', methods=['PUT'])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update mapset_area_coverage
        res, mapset_area_coverage = MAPSET_AREA_COVERAGE.update(where, json_request)

        # response
        if res:
            # success response
            response = {'message': 'Update data successfull', 'error': 0, 'data': mapset_area_coverage}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Update data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_area_coverage/<_id>', methods=['DELETE'])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete mapset_area_coverage
        res = MAPSET_AREA_COVERAGE.delete(where)

        # response
        if res:
            # success response
            response = {'message': 'Delete data successfull', 'error': 0, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Delete data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
