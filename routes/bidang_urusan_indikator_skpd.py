''' Doc: route bidang_urusan_indikator'''
import json
import math
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import BidangUrusanIndikatorSkpdController
from settings import configuration as conf

BP = Blueprint('bidang_urusan_indikator_skpd', __name__)
BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER = BidangUrusanIndikatorSkpdController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/bidang_urusan_indikator_skpd", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, datas = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, datas = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.get_all(where, search, sort, limit, skip)

        res_count, count = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.get_count(where, search)
        meta = {}
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": datas, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, datas = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert bidang_urusan_indikator
        res, datas, msg = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": msg, "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": msg, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd/multiple", methods=["POST"])
@jwt_check()
def create_multiple():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # insert data
            datas = []
            for json_req in json_request:
                res, data, msg = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.create(json_req)
                datas.append(data)
        else:
            # insert data
            res, datas, msg = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": msg, "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": msg, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update metadata
        res, datas = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd/multiple", methods=["PUT"])
@jwt_check()
def update_multiple():
    ''' Doc: function update'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # insert data
            datas = []
            for json_req in json_request:
                # update data
                where = {
                    'id': int(json_req['id'])
                }
                res, data = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.update(where, json_req)
                datas.append(data)
        else:
            # update data
            where = {
                'id': int(json_request['id'])
            }
            res, datas = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.update(where, json_request)


        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete metadata
        res = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/bidang_urusan_indikator_skpd/assign", methods=["POST"])
@jwt_check()
def assign():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)
        # insert bidang_urusan_indikator
        res, datas, msg = BIDANG_URUSAN_INDIKATOR_SKPD_CONTROLLER.assign(json_request)

        # response
        if res:
            # success response
            response = {"message": msg, "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": msg, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

