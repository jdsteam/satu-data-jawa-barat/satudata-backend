''' Doc: route skpd'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
import math
from controllers import SkpdController
from settings import configuration as conf
from helpers.autocorecction import AutoCorecction

BP = Blueprint('skpd', __name__)
SKPD_CONTROLLER = SkpdController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/skpd", methods=["GET"])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'nama_skpd:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        suggestion = request.args.get("suggestion", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        correction_data = {}
        correction_data['status'] = False
        correction_data['suggestion_text'] = ""
        correction_data['original_text'] = search
        if search and suggestion == "on":
            status, data_corecction = AutoCorecction.skpd(search)
            correction_data['status'] = status
            correction_data['suggestion_text'] = data_corecction
            correction_data['original_text'] = search
            search = data_corecction
        else:
            search = search

        if count:
            # call function get
            res, skpd = SKPD_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, skpd = SKPD_CONTROLLER.get_all(where, search, sort, limit, skip)

        meta = {}
        res_count, count = SKPD_CONTROLLER.get_count(where, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": skpd, "meta": meta,
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {},
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {},
            "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
            "original_text" : correction_data['original_text']
        }
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/skpd/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, skpd = SKPD_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": skpd}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/skpd", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert skpd
        res, skpd = SKPD_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": skpd}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/skpd/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update skpd
        res, skpd = SKPD_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": skpd}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/skpd/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete skpd
        res = SKPD_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/skpd/trending", methods=["GET"])
@jwt_check()
def get_trending():
    ''' Doc: function get trending'''
    try:
        sort = request.args.get("sort", 'count:desc')
        sort = sort.split(':')

        res, trending = SKPD_CONTROLLER.get_trending(sort)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": trending}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
