''' Doc: route dashboard'''
import json
from flask import Blueprint, request, Response
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DashboardHistoryDownloadController
from settings import configuration as conf
from datetime import datetime, timedelta

BP = Blueprint('dashboard/history_download', __name__)
DASHBOARD_HISTORY_DOWNLOAD = DashboardHistoryDownloadController()

HELPER = Helper()
one_month_ago_days = 30
one_days_ago = 1
today = datetime.today().strftime("%Y-%m-%d")
get_month_ago = datetime.today() - timedelta(days=one_month_ago_days)
get_day_ago = datetime.today() - timedelta(days=one_days_ago)
one_month_ago = get_month_ago.strftime("%Y-%m-%d")
one_day_ago = get_day_ago.strftime("%Y-%m-%d")
# pylint: disable=broad-except


@BP.route("/dashboard/history_download/total", methods=["GET"])
@jwt_check()
def get_total():
    ''' Doc: function get history_download total'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_total(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/average", methods=["GET"])
@jwt_check()
def get_average():
    ''' Doc: function get history_download average'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_day_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_average(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/trend", methods=["GET"])
@jwt_check()
def get_trend():
    ''' Doc: function get history_download trend'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        group = request.args.get("group", 'day')
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_trend(
            start_date, end_date, group)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/tujuan", methods=["GET"])
@jwt_check()
def get_tujuan():
    ''' Doc: function get history_download tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_tujuan(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/pekerjaan", methods=["GET"])
@jwt_check()
def get_pekerjaan():
    ''' Doc: function get history_download pekerjaan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_pekerjaan(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/bidang_ilmu", methods=["GET"])
@jwt_check()
def get_bidang_ilmu():
    ''' Doc: function get history_download bidang_ilmu'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_bidang_ilmu(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/bidang_usaha", methods=["GET"])
@jwt_check()
def get_bidang_usaha():
    ''' Doc: function get history_download bidang_usaha'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_bidang_usaha(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/history_download/top_download", methods=["GET"])
@jwt_check()
def get_top_download():
    ''' Doc: function get history_download top_download'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_HISTORY_DOWNLOAD.get_top_download(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
