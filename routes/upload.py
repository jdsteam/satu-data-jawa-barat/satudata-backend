''' Doc: route upload'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
import os
from werkzeug.utils import secure_filename
from hashlib import sha256
from controllers import UploadController
from settings import configuration as conf

from PIL import Image
import os
import PIL
import glob
import re
import shutil

BP = Blueprint('upload', __name__)
UPLOAD_CONTROLLER = UploadController()
HELPER = Helper()
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'svg', 'xml', 'zip'])
# pylint: disable=broad-except, line-too-long, unused-variable,


def allowed_file(filename):
    ''' Doc: function allowed file'''
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@BP.route("/upload", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, upload = UPLOAD_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, upload = UPLOAD_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": upload}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no dat
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/upload/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, upload = UPLOAD_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": upload}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/upload", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # get jwt payload
        jwt = HELPER.read_jwt()

        if 'file' not in request.files:
            response = {"message": "No file part in the request ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400

        file = request.files['file']
        if file.filename == '':
            response = {"message": "No file selected for uploading", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400

        if file and not allowed_file(file.filename):
            response = {"message": "Allowed file types are txt, pdf, png, jpg, jpeg, gif, xml, zip", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400
        else:

            # upload original name
            original_file = secure_filename(file.filename)
            file.save(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))

            filename, fileext = os.path.splitext(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))
            filesize = os.path.getsize(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))

            before_hash = HELPER.local_date_server() + '_' + filename + fileext
            after_hash = sha256(before_hash.encode('ascii')).hexdigest()

            # upload rename
            if request.form.get('name'):
                new_name = request.form.get('name') + fileext
            else:
                new_name = secure_filename(file.filename)

            next_id = UPLOAD_CONTROLLER.get_next_id()
            new_name = new_name.lower().replace(' ', '-')
            new_name = re.sub('[^a-zA-Z0-9-.]', '', new_name)
            new_name = str(next_id) + '_' + new_name

            old = os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file)
            new = os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp/', ''), new_name)
            shutil.copy(old, new)

            # upload resize
            if original_file.rsplit('.', 1)[1].lower() in ['png', 'jpg', 'jpeg', 'gif']:

                # resize 700
                fixed_width = 700
                image = Image.open(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))
                width_percent = (fixed_width / float(image.size[0]))
                height_size = int((float(image.size[1]) * float(width_percent)))
                image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
                image.save(os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp', '700'), new_name))

                # resize 500
                fixed_width = 500
                image = Image.open(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))
                width_percent = (fixed_width / float(image.size[0]))
                height_size = int((float(image.size[1]) * float(width_percent)))
                image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
                image.save(os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp', '500'), new_name))

                # resize 300
                fixed_width = 300
                image = Image.open(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))
                width_percent = (fixed_width / float(image.size[0]))
                height_size = int((float(image.size[1]) * float(width_percent)))
                image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
                image.save(os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp', '300'), new_name))

                # resize 100
                fixed_width = 100
                image = Image.open(os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file))
                width_percent = (fixed_width / float(image.size[0]))
                height_size = int((float(image.size[1]) * float(width_percent)))
                image = image.resize((fixed_width, height_size), PIL.Image.NEAREST)
                image.save(os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp', '100'), new_name))

            elif original_file.rsplit('.', 1)[1].lower() in ['svg']:
                shutil.copy(
                    os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file),
                    os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp/', '700/'), new_name)
                )
                shutil.copy(
                    os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file),
                    os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp/', '500/'), new_name)
                )
                shutil.copy(
                    os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file),
                    os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp/', '300/'), new_name)
                )
                shutil.copy(
                    os.path.join(conf.MESSAGE_UPLOAD_PATH, original_file),
                    os.path.join(conf.MESSAGE_UPLOAD_PATH.replace('temp/', '100/'), new_name)
                )

            # prepare json data
            json_request = {}
            json_request['name'] = new_name.split('.')[0]
            json_request['type'] = ''
            json_request['ext'] = fileext
            json_request['size'] = filesize
            json_request['hash'] = after_hash
            json_request['url'] = 'static/upload/' + new_name
            json_request['cuid'] = jwt['id']
            json_request['cdate'] = HELPER.local_date_server()

            # insert upload
            res, upload = UPLOAD_CONTROLLER.create(json_request)

            # response
            if res:
                # success response
                response = {"message": "Create data successfull", "error": 0, "data": upload}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
            else:
                # success response but no data
                response = {"message": "Create data failed, ", "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/upload/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update upload
        res, upload = UPLOAD_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": upload}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/upload/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete upload
        res = UPLOAD_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/upload/trending", methods=["GET"])
@jwt_check()
def get_trending():
    ''' Doc: function get trending'''
    try:
        sort = request.args.get("sort", 'count:desc')
        sort = sort.split(':')

        res, trending = UPLOAD_CONTROLLER.get_trending(sort)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": trending}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
