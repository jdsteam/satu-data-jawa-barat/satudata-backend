''' Doc: route article'''
import json
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import ArticleController
from controllers import HighlightController
from settings import configuration as conf
from helpers.autocorecction import AutoCorecction

BP = Blueprint('article', __name__)
CONTROLLER = ArticleController()
HIGHLIGHT_CONTROLLER = HighlightController()
HELPER = Helper()

@BP.route("/article", methods=["GET"])
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        suggestion = request.args.get("suggestion", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        correction_data = {}
        correction_data['status'] = False
        correction_data['suggestion_text'] = ""
        correction_data['original_text'] = search

        if search and suggestion == "on":
            status, data_corecction = AutoCorecction.article(search)
            correction_data['status'] = status
            correction_data['suggestion_text'] = data_corecction
            correction_data['original_text'] = search
            search = data_corecction
        else:
            search = search

        status_count, result_count = CONTROLLER.get_count(where, search)
        # call function get
        count_data = result_count['count']
        result = result_count

        status, result = CONTROLLER.get_all(where, search, sort, limit, skip)

        meta = Helper._meta(count_data, skip, limit)
        # response
        if status:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS, "error": 0,
                "data": result, "meta": meta, "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {},
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [],
            "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
            "original_text" : correction_data['original_text']
        }
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/recomendation", methods=["GET"])
def get_random():
    ''' Doc: function get random article'''
    try:
        # filter data

        status, result = CONTROLLER.get_random()
        # response
        if status:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS, "error": 0,
                "data": result
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/counter/<_id>", methods=["GET"])
def counter(_id):
    ''' Doc: function counter'''
    category = request.args.get("category", "")

    # insert counter
    status, result = CONTROLLER.counter(_id, category)

    # response
    if status:
        # success response
        response = {"message": "Create counter successfull", "error": 0, "data": result}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    else:
        # success response but no data
        response = {"message": "Create counter failed ", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/<_id>", methods=["GET"])
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        status, result, message = CONTROLLER.get_by_id(_id)

        # response
        if status:
            # success response
            response = {"message": message, "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert article
        status, result = CONTROLLER.create(json_request)

        # response
        if status:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update article
        status, result = CONTROLLER.update(where, json_request)

        # trigger update highlight
        if 'is_active' in json_request or 'is_deleted' in json_request:
            where_hi = {}
            where_hi['category'] = 'artikel'
            where_hi['category_id'] = _id
            res_hi, highlight = HIGHLIGHT_CONTROLLER.get_all(where_hi, "", ['id', 'asc'], 1, 0)
            if res_hi:
                where_hi['id'] = highlight[0]['id']
                json_request_hi = {}
                json_request_hi['is_active'] = result['is_deleted']
                # json_request_hi['is_deleted'] = result['is_deleted']
                res_hi, highlight = HIGHLIGHT_CONTROLLER.update(where_hi, json_request_hi)

        # response
        if status:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id, 'is_deleted': False}

        # delete article
        status, result = CONTROLLER.delete(where)

        # response
        if status:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/organization", methods=["GET"])
def get_organization():
    try:
        # call function get
        res, organization = CONTROLLER.get_organization_all()

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": organization}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/article/topic", methods=["GET"])
def get_topic():
    try:
        # call function get
        res, topic = CONTROLLER.get_topic_all()

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": topic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
