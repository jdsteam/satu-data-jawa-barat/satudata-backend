''' Doc: route dashboard'''
import json
from flask import Blueprint, request, Response
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DashboardCsatController
from settings import configuration as conf
from datetime import datetime, timedelta

BP = Blueprint('dashboard/csat', __name__)
DASHBOARD_CSAT = DashboardCsatController()

HELPER = Helper()
one_month_ago_days = 30
one_days_ago = 1
today = datetime.today().strftime("%Y-%m-%d")
get_month_ago = datetime.today() - timedelta(days=one_month_ago_days)
get_day_ago = datetime.today() - timedelta(days=one_days_ago)
one_month_ago = get_month_ago.strftime("%Y-%m-%d")
one_day_ago = get_day_ago.strftime("%Y-%m-%d")
# pylint: disable=broad-except


@BP.route("/dashboard/csat/total", methods=["GET"])
@jwt_check()
def get_csat_total():
    ''' Doc: function get_csat_total'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_total(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/average", methods=["GET"])
@jwt_check()
def get_csat_average():
    ''' Doc: function get_csat_average'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_average(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/trend", methods=["GET"])
@jwt_check()
def get_csat_trend():
    ''' Doc: function get_csat_trend'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        group = request.args.get("group", 'day')
        res, data = DASHBOARD_CSAT.get_csat_trend(start_date, end_date, group)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/csat_total", methods=["GET"])
@jwt_check()
def get_csat_csat_total():
    ''' Doc: function get_csat_csat'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_csat_total(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/csat_trend", methods=["GET"])
@jwt_check()
def get_csat_csat_trend():
    ''' Doc: function get_csat_csat_trend'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        group = request.args.get("group", 'day')
        res, data = DASHBOARD_CSAT.get_csat_csat_trend(start_date, end_date, group)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/tujuan", methods=["GET"])
@jwt_check()
def get_csat_tujuan():
    ''' Doc: function get_csat_tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_tujuan(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/sektor", methods=["GET"])
@jwt_check()
def get_csat_sektor():
    ''' Doc: function get_csat_sektor'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_sektor(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/data_ditemukan", methods=["GET"])
@jwt_check()
def get_csat_data_ditemukan():
    ''' Doc: function get_csat_data_ditemukan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_data_ditemukan(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/data_masalah", methods=["GET"])
@jwt_check()
def get_csat_data_masalah():
    ''' Doc: function get_csat_data_masalah'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_data_masalah(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/mudah_didapatkan", methods=["GET"])
@jwt_check()
def get_csat_mudah_didapatkan():
    ''' Doc: function get_csat_mudah_didapatkan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_mudah_didapatkan(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/csat/top_download", methods=["GET"])
@jwt_check()
def get_csat_top_download():
    ''' Doc: function get_csat_top_download'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, data = DASHBOARD_CSAT.get_csat_top_download(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
