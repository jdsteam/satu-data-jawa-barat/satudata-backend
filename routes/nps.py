''' Doc: route nps'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import NpsController
from settings import configuration as conf

BP = Blueprint('nps', __name__)
NPS_CONTROLLER = NpsController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/nps", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, nps = NPS_CONTROLLER.get_count(where, search, start_date, end_date)
        else:
            # call function get
            res, nps = NPS_CONTROLLER.get_all(where, search, sort, limit, skip, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": nps}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/nps/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, nps = NPS_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": nps}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/nps", methods=["POST"])
# @jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert nps
        res, nps = NPS_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": nps}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/nps/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update nps
        res, nps = NPS_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": nps}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/nps/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete nps
        res = NPS_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/nps/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, nps = NPS_CONTROLLER.get_download(start_date, end_date)
        metadata = ["Id", "Datetime", "Score", "Message", "Page Source", "is_active", "is_deleted"]

        # response
        if res:
            # success response
            filename = HELPER.convert_xls('static/download/nps.xlsx', nps, metadata)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='nps.xlsx',
                as_attachment=True
            )
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
