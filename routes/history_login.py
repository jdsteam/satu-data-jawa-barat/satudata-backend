''' Doc: route history login'''
import json
import requests
from flask import request, Blueprint, Response, send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import HistoryLoginController
from settings import configuration as conf

BP = Blueprint('history_login', __name__)
HISTORY_LOGIN_CONTROLLER = HistoryLoginController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/history_login", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        download = request.args.get("download", '')
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if download and download.lower() == 'xls':
                res_count, count_login = HISTORY_LOGIN_CONTROLLER.get_count(where, search, start_date, end_date)
                res, history_login = HISTORY_LOGIN_CONTROLLER.get_all(where, search, sort, count_login['count'], skip, start_date, end_date)
                output_xls = []
                number = 0
                for row in history_login:
                    number += 1
                    row.update({'No':number})
                    row.update({'Waktu':row['datetime']})
                    row.update({'Username':row['username']})
                    row.update({'Organisasi':row['nama_skpd']})
                    del row['id'], row['user_id'], row['kode_skpd'], row['datetime'], row['username'], row['nama_skpd']
                    output_xls.append(row)

                columns_title = ['No', 'Waktu', 'Username', 'Organisasi']
                filename = HELPER.convert_xls('static/download/history-login.xlsx', output_xls, columns_title)
                return send_file(
                    '../' + filename,
                    mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    attachment_filename='history-login.xlsx',
                    as_attachment=True
                )

        if count:
            # call function get
            res, history_login = HISTORY_LOGIN_CONTROLLER.get_count(where, search, start_date, end_date)
        else:
            # call function get
            res, history_login = HISTORY_LOGIN_CONTROLLER.get_all(where, search, sort, limit, skip, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": history_login}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_login/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, history_login, message = HISTORY_LOGIN_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": history_login}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_login", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)
        json_request['ip'] = request.remote_addr
        json_request['browser'] = request.headers.get('User-Agent')

        # normal request
        url = 'http://www.geoplugin.net/json.gp?ip='+request.remote_addr
        res_ = requests.get(url)
        js_ = json.loads(res_.text)

        # using request service
        # url = 'http://www.geoplugin.net/json.gp?ip='+request.remote_addr
        # data_autologin = {
        #     "url": url,
        #     "headers": {
        #         "Content-Type": "application/json"
        #     },
        # }
        # res_ = requests.post(
        #     conf.REQUEST_URL + 'http',
        #     headers={"Content-Type": conf.MESSAGE_APP_JSON},
        #     data=json.dumps(data_autologin)
        # )
        # js_ = res_.json()['data']

        json_request['country_code'] = js_['geoplugin_countryCode']
        json_request['country'] = js_['geoplugin_countryName']
        json_request['region'] = js_['geoplugin_region']
        json_request['lat'] = js_['geoplugin_latitude']
        json_request['long'] = js_['geoplugin_longitude']

        # insert history login
        res, history_login = HISTORY_LOGIN_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": history_login}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_login/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update History Login
        res, history_login = HISTORY_LOGIN_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": history_login}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_login/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history login
        res = HISTORY_LOGIN_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
