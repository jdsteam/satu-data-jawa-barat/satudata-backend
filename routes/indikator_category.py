''' Doc: route indikator category'''
import json
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import IndikatorCategoryController
from settings import configuration as conf

BP = Blueprint('indikator_category', __name__)
INDIKATOR_CATEGORY_CONTROLLER = IndikatorCategoryController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/indikator_category", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, indikator_category = INDIKATOR_CATEGORY_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, indikator_category = INDIKATOR_CATEGORY_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": indikator_category}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_category/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, indikator_category, message = INDIKATOR_CATEGORY_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": indikator_category}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_category", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert indikator category
        res, indikator_category = INDIKATOR_CATEGORY_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": indikator_category}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_category/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update indikator category
        res, indikator_category = INDIKATOR_CATEGORY_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": indikator_category}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_category/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete indikator category
        res = INDIKATOR_CATEGORY_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
