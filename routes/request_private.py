''' Doc: route request private'''
import json
import requests
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
from settings import configuration
from controllers import RequestPrivateController
from controllers import HistoryRequestPrivateController
from controllers import SkpdController
from controllers import UserController
from controllers import NotificationController
from models import UserModel
from models import RoleModel
from settings import configuration as conf

BP = Blueprint('request_private', __name__)
REQUEST_PRIVATE_CONTROLLER = RequestPrivateController()
HISTORY_REQUEST_PRIVATE_CONTROLLER = HistoryRequestPrivateController()
SKPD_CONTROLLER = SkpdController()
USER_CONTROLLER = UserController()
NOTIFICATION_CONTROLLER = NotificationController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable,


@BP.route("/request_private", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, request_private = REQUEST_PRIVATE_CONTROLLER.get_count(
                where, search)
        else:
            # call function get
            res, request_private = REQUEST_PRIVATE_CONTROLLER.get_all(
                where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": request_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_private/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, request_private, message = REQUEST_PRIVATE_CONTROLLER.get_by_id(
            _id)

        # response
        if res:
            # success response
            response = {"message": message,
                        "error": 0, "data": request_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_private", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        jwt = HELPER.read_jwt()

        # prepare json data
        json_request = request.get_json(silent=True)

        # insert request
        res, request_private = REQUEST_PRIVATE_CONTROLLER.create(json_request)

        # insert history request
        if res:
            json_history_request = {}
            json_history_request['request_private_id'] = request_private['id']
            json_history_request['status'] = '1'
            json_history_request['cuid'] = jwt['id']
            res_hisotry, history = HISTORY_REQUEST_PRIVATE_CONTROLLER.create(
                json_history_request)

            request_private['history'].append(history)

        # insert notification table
        if res:
            # get user
            data_user = db.session.query(UserModel.id)
            data_user = data_user.join(
                RoleModel, RoleModel.id == UserModel.role_id)
            data_user = data_user.filter(RoleModel.name == 'walidata')
            data_user = data_user.all()

            for dr_ in data_user:
                temp = dr_._asdict()
                user_id = temp['id']
                json_notification = {}
                json_notification['feature'] = 3
                json_notification['type'] = 'request_private'
                json_notification['type_id'] = request_private['id']
                json_notification['sender'] = request_private['user_id']
                json_notification['receiver'] = user_id
                json_notification['title'] = "Terdapat permohonan dataset "
                json_notification['content'] = "<b>" + \
                    request_private['judul_data'] + "</b> dari " + \
                    "<b>" + request_private['nama_skpd'] + '</b>.'
                json_notification['is_read'] = False
                json_notification['cdate'] = HELPER.local_date_server()
                json_notification['mdate'] = HELPER.local_date_server()
                result, notification = NOTIFICATION_CONTROLLER.create(
                    json_notification)

                # update notif
                param = {}
                param['count_notif'] = 0
                res_, user = USER_CONTROLLER.get_by_id(user_id)
                if res_:
                    if not user['count_notif']:
                        user['count_notif'] = 0
                    param['count_notif'] = user['count_notif'] + 1
                    # execute database
                    result = db.session.query(UserModel)
                    result = result.filter(getattr(UserModel, 'id') == user_id)
                    result = result.update(param, synchronize_session='fetch')
                    result = db.session.commit()

                try:
                    socket_url = configuration.SOCKET_URL
                    token = request.headers.get('Authorization', None)
                    requests.get(
                        socket_url + 'notification' + '?receiver=' +
                        str(notification['receiver']),
                        headers={"Authorization": token,
                                 "Content-Type": "application/json"},
                        data=json.dumps(notification)
                    )
                except Exception as err:
                    print(err)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": request_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_private/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update request
        res, request_private = REQUEST_PRIVATE_CONTROLLER.update(
            where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": request_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_private/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete request
        res = REQUEST_PRIVATE_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_private/info", methods=["GET"])
# @jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        request_private = {}
        request_private['tujuan'] = []

        request_private['tujuan'].append({'nama': 'Referensi Pembuatan Kebijakan'})
        request_private['tujuan'].append({'nama': 'Referensi Kajian'})
        request_private['tujuan'].append({'nama': 'Referensi Pribadi'})
        request_private['tujuan'].append({'nama': 'Lainnya'})

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": request_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
