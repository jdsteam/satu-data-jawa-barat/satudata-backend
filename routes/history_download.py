''' Doc: route history download'''
import json
from flask import request, Blueprint, Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import HistoryDownloadController
from settings import configuration as conf

BP = Blueprint('history_download', __name__)
HISTORY_DOWNLOAD_CONTROLLER = HistoryDownloadController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable


@BP.route("/history_download", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_count(
                where, search, start_date, end_date)
        else:
            # call function get
            res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_all(
                where, search, sort, limit, skip, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, history_download, message = HISTORY_DOWNLOAD_CONTROLLER.get_by_id(
            _id)

        # response
        if res:
            # success response
            response = {"message": message,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download", methods=["POST"])
# @jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert history draft
        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.create(
            json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history draft
        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.update(
            where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history draft
        res = HISTORY_DOWNLOAD_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/info", methods=["GET"])
# @jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        history_download = {}
        history_download['pekerjaan'] = []
        history_download['tujuan'] = []

        bidang1 = []
        bidang1.append({'nama': 'Agama dan Filsafat'})
        bidang1.append({'nama': 'Bahasa, Sosial, dan Humaniora'})
        bidang1.append({'nama': 'Ekonomi'})
        bidang1.append({'nama': 'Hewani'})
        bidang1.append({'nama': 'Kedokteran dan Kesehatan'})
        bidang1.append({'nama': 'Matematika dan Pengetahuan Alam'})
        bidang1.append({'nama': 'Pendidikan'})
        bidang1.append({'nama': 'Seni, Desain, dan Media'})
        bidang1.append({'nama': 'Tanaman'})
        bidang1.append({'nama': 'Teknik'})

        bidang2 = []
        bidang2.append({'nama': 'Pertanian (Agrikultur)'})
        bidang2.append({'nama': 'Pertambangan'})
        bidang2.append({'nama': 'Pabrikasi (Manufaktur)'})
        bidang2.append({'nama': 'Kontruksi'})
        bidang2.append({'nama': 'Perdagangan'})
        bidang2.append({'nama': 'Jasa Keuangan'})
        bidang2.append({'nama': 'Jasa Perorangan'})
        bidang2.append({'nama': 'Jasa Umum'})
        bidang2.append({'nama': 'Jasa Wisata'})

        pekerjaan1 = {}
        pekerjaan1['nama'] = 'Pelajar/ Mahasiswa/ Akademisi'
        pekerjaan1['bidang'] = bidang1

        pekerjaan2 = {}
        pekerjaan2['nama'] = 'Profesional'
        pekerjaan2['bidang'] = bidang2

        pekerjaan3 = {}
        pekerjaan3['nama'] = 'Wirausaha'
        pekerjaan3['bidang'] = bidang2

        history_download['pekerjaan'].append(pekerjaan1)
        history_download['pekerjaan'].append(pekerjaan2)
        history_download['pekerjaan'].append(pekerjaan3)

        history_download['tujuan'].append({'nama': 'Referensi Kajian Bisnis'})
        history_download['tujuan'].append(
            {'nama': 'Referensi Pembuatan Kebijakan'})
        history_download['tujuan'].append(
            {'nama': 'Referensi Pembuatan Kurikulum/ Bahan Ajar'})
        history_download['tujuan'].append(
            {'nama': 'Referensi Tugas/ Karya Ilmiah'})
        history_download['tujuan'].append({'nama': 'Referensi Pribadi'})

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/search_email/<email>", methods=["GET"])
# @jwt_check()
def get_search_email(email):
    ''' Doc: function get search email'''
    try:
        res, history_download, message = HISTORY_DOWNLOAD_CONTROLLER.get_search_email(
            email)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/chart_pekerjaan", methods=["GET"])
@jwt_check()
def get_chart_pekerjaan():
    ''' Doc: function get chart pekerjaan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_chart_pekerjaan(
            start_date, end_date)

        history_downloads = []
        history_downloads.append(
            {"count": 0, "pekerjaan": "Pelajar/ Mahasiswa/ Akademisi"})
        history_downloads.append({"count": 0, "pekerjaan": "Profesional"})
        history_downloads.append({"count": 0, "pekerjaan": "Wirausaha"})
        history_downloads.append({"count": 0, "pekerjaan": "Lainnya"})

        for fb_ in history_download:
            if fb_['pekerjaan'] == "Pelajar/ Mahasiswa/ Akademisi":
                history_downloads[0]['count'] = fb_['count']
            elif fb_['pekerjaan'] == "Profesional":
                history_downloads[1]['count'] = fb_['count']
            elif fb_['pekerjaan'] == "Wirausaha":
                history_downloads[2]['count'] = fb_['count']
            else:
                history_downloads[3]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_downloads}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/chart_bidang", methods=["GET"])
@jwt_check()
def get_chart_bidang():
    ''' Doc: function get chart bidang'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_chart_bidang(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/chart_tujuan", methods=["GET"])
@jwt_check()
def get_chart_tujuan():
    ''' Doc: function get chart tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_chart_tujuan(
            start_date, end_date)

        history_downloads = []
        history_downloads.append(
            {"count": 0, "tujuan": "Referensi Kajian Bisnis"})
        history_downloads.append(
            {"count": 0, "tujuan": "Referensi Pembuatan Kebijakan"})
        history_downloads.append(
            {"count": 0, "tujuan": "Referensi Pembuatan Kurikulum/ Bahan Ajar"})
        history_downloads.append(
            {"count": 0, "tujuan": "Referensi Tugas/ Karya Ilmiah"})
        history_downloads.append({"count": 0, "tujuan": "Referensi Pribadi"})
        history_downloads.append({"count": 0, "tujuan": "Lainnya"})

        for fb_ in history_download:
            if fb_['tujuan'] == "Referensi Kajian Bisnis":
                history_downloads[0]['count'] = fb_['count']
            elif fb_['tujuan'] == "Referensi Pembuatan Kebijakan":
                history_downloads[1]['count'] = fb_['count']
            elif fb_['tujuan'] == "Referensi Pembuatan Kurikulum/ Bahan Ajar":
                history_downloads[2]['count'] = fb_['count']
            elif fb_['tujuan'] == "Referensi Tugas/ Karya Ilmiah":
                history_downloads[3]['count'] = fb_['count']
            elif fb_['tujuan'] == "Referensi Pribadi":
                history_downloads[4]['count'] = fb_['count']
            else:
                history_downloads[5]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_downloads}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/chart_harian", methods=["GET"])
@jwt_check()
def get_chart_harian():
    ''' Doc: function get cbart harian'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_chart_harian(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/chart_dataset", methods=["GET"])
@jwt_check()
def get_chart_dataset():
    ''' Doc: function get chart dataset'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_chart_dataset(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": history_download}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_download/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        download = request.args.get("download", '')
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, history_download = HISTORY_DOWNLOAD_CONTROLLER.get_download(
            start_date, end_date)
        metadata = [
            "id", "datetime", "type", "email", "pekerjaan", "bidang", "tujuan",
            "lokasi", "category", "category_id"
        ]

        # response
        if res:
            if download.lower() == 'csv':
                filename = HELPER.convert_csv(
                    'static/download/history_download.csv', history_download, metadata)
                return send_file(
                    '../' + filename,
                    mimetype='application/x-csv',
                    attachment_filename='history_download.csv',
                    as_attachment=True
                )
            elif download.lower() == 'xls':
                filename = HELPER.convert_xls(
                    'static/download/history_download.xlsx', history_download, metadata)
                return send_file(
                    '../' + filename,
                    mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    attachment_filename='history_download.xlsx',
                    as_attachment=True
                )
            else:
                # success response
                response = {"message": conf.MESSAGE_DATA_SUCCESS,
                            "error": 0, "data": history_download}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
