''' Doc: route visualization'''
import json
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from models import SkpdModel
from models import VisualizationModel
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers import SearchController
from controllers import HistoryController
from controllers import VisualizationController
from controllers import HighlightController
import math
from settings import configuration as conf
from helpers.autocorecction import AutoCorecction

BP = Blueprint('visualization', __name__)
VISUALIZATION_CONTROLLER = VisualizationController()
HISTORY_CONTROLLER = HistoryController()
SEARCH_CONTROLLER = SearchController()
HIGHLIGHT_CONTROLLER = HighlightController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/visualization", methods=["GET"])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        suggestion = request.args.get("suggestion", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        correction_data = {}
        correction_data['status'] = False
        correction_data['suggestion_text'] = ""
        correction_data['original_text'] = search
        if search and suggestion == "on":
            status, data_corecction = AutoCorecction.visualization(search)
            correction_data['status'] = status
            correction_data['suggestion_text'] = data_corecction
            correction_data['original_text'] = search
            search = data_corecction
        else:
            search = search

        if count:
            # call function get
            res, visualization = VISUALIZATION_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, visualization = VISUALIZATION_CONTROLLER.get_all(where, search, sort, limit, skip)

        # if search:
        #     try:
        #         jwt = HELPER.read_jwt()
        #         payload = {}
        #         if jwt:
        #             payload['user_id'] = jwt['id']
        #         else:
        #             payload['user_id'] = '0'
        #         payload['search'] = search
        #         res2, data2 = SEARCH_CONTROLLER.create(payload)
        #     except Exception as err:
        #         pass

        meta = {}
        res_count, count = VISUALIZATION_CONTROLLER.get_count(where, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": visualization, "meta": meta,
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {},
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {},
            "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
            "original_text" : correction_data['original_text']
        }
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        where = request.args.get("where", "{}")

        where = where.replace("'", "\"")
        where = json.loads(where)

        res, visualization, message = VISUALIZATION_CONTROLLER.get_by_id(_id, where)

        # update count view
        try:
            jwt = HELPER.read_jwt()
            if visualization['skpd']['kode_skpd'] != jwt['kode_skpd']:
                count_view = visualization['count_view']
                if not count_view:
                    count_view = '0'
                count_view = int(count_view) + 1
                where = {'id': int(_id)}
                payload = {'count_view': int(count_view)}
                visualization['count_view'] = count_view
                res1, data1 = VISUALIZATION_CONTROLLER.update(where, payload)
        except Exception as err:
            pass

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": visualization}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/visualization/counter/<_id>", methods=["GET"])
# @jwt_check()
def counter(_id):
    ''' Doc: function counter'''
    # category
    category = request.args.get("category", "")

    # handle id=title, integer or string
    try:
        temp = int(_id)
        _id = temp
    except Exception as err:
        temp = _id
        result = db.session.query(VisualizationModel.id)
        result = result.filter(VisualizationModel.title == temp)
        result = result.all()
        if result:
            result = result[0]
            _id = result[0]
        else:
            _id = 0

    try:
        res, visualization, message = VISUALIZATION_CONTROLLER.get_by_id(_id, {})

        # category counter
        try:
            visualization, payload = VISUALIZATION_CONTROLLER.switch(category, visualization)
            where = {'id': int(_id)}
            res, data1 = VISUALIZATION_CONTROLLER.update(where, payload)
        except Exception as err:
            pass

        # insert to history
        try:
            jwt = HELPER.read_jwt()
            if 'view' in category:
                payload = {}
                payload['type'] = 'visualization'
                payload['type_id'] = _id
                payload['user_id'] = jwt['id']
                payload['category'] = 'view'
                res2, data2 = HISTORY_CONTROLLER.create(payload)
            elif 'image' in category or 'pdf' in category:
                payload = {}
                payload['type'] = 'visualization'
                payload['type_id'] = _id
                payload['user_id'] = jwt['id']
                payload['category'] = 'access'
                res2, data2 = HISTORY_CONTROLLER.create(payload)
        except Exception as err:
            pass

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": visualization}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/visualization", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        temp_jabatan_access = []
        if 'jabatan_access' in json_request:
            temp_jabatan_access = json_request['jabatan_access']
            del json_request['jabatan_access']

        # insert visualization
        res, visualization = VISUALIZATION_CONTROLLER.create(json_request)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'visualization'
            json_history['type_id'] = visualization['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'create'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # update count_visualization
        if 'kode_skpd' in json_request:
            # public only
            result, count = VISUALIZATION_CONTROLLER.get_count(
                {
                    'kode_skpd': json_request['kode_skpd'],
                    'is_deleted': False,
                    'is_active': True,
                    'dataset_class_id': 3
                }, '')
            updates = db.session.query(SkpdModel)
            updates = updates.filter(getattr(SkpdModel, 'kode_skpd') == json_request['kode_skpd'])
            updates = updates.update({'count_visualization_public': count['count']}, synchronize_session='fetch')
            db.session.commit()
            # with private
            result, count = VISUALIZATION_CONTROLLER.get_count(
                {
                    'kode_skpd': json_request['kode_skpd'],
                    'is_deleted': False,
                    'is_active': True
                }, '')
            updates = db.session.query(SkpdModel)
            updates = updates.filter(getattr(SkpdModel, 'kode_skpd') == json_request['kode_skpd'])
            updates = updates.update({'count_visualization': count['count']}, synchronize_session='fetch')
            db.session.commit()

        if temp_jabatan_access:
            VISUALIZATION_CONTROLLER.route_create_jabatan_access(visualization, temp_jabatan_access)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": visualization}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        temp_jabatan_access = []
        if 'jabatan_access' in json_request:
            temp_jabatan_access = json_request['jabatan_access']
            del json_request['jabatan_access']

        if 'kode_skpd' in json_request:
            # get kode_skpd
            result, visualization, message = VISUALIZATION_CONTROLLER.get_by_id(_id, {})
            old_skpd = visualization['skpd']['kode_skpd']
            new_skpd = json_request['kode_skpd']
        else:
            # get kode_skpd
            result, visualization, message = VISUALIZATION_CONTROLLER.get_by_id(_id, {})
            old_skpd = visualization['skpd']['kode_skpd']
            new_skpd = visualization['skpd']['kode_skpd']
            json_request['kode_skpd'] = visualization['skpd']['kode_skpd']

        if temp_jabatan_access:
            VISUALIZATION_CONTROLLER.route_update_jabatan_access(visualization, temp_jabatan_access)

        # update visualization
        res, visualization = VISUALIZATION_CONTROLLER.update(where, json_request)

        # update count_visualization
        if 'kode_skpd' in json_request:
            # old skpd
            # public only
            result1, count1 = VISUALIZATION_CONTROLLER.get_count(
                {
                    'kode_skpd': old_skpd,
                    'is_deleted': False,
                    'is_active': True,
                    'dataset_class_id': 3
                }, '')
            update1 = db.session.query(SkpdModel)
            update1 = update1.filter(getattr(SkpdModel, 'kode_skpd') == old_skpd)
            update1 = update1.update({'count_visualization_public': count1['count']}, synchronize_session='fetch')
            db.session.commit()
            # with private
            result1, count1 = VISUALIZATION_CONTROLLER.get_count(
                {
                    'kode_skpd': old_skpd,
                    'is_deleted': False,
                    'is_active': True
                }, '')
            update1 = db.session.query(SkpdModel)
            update1 = update1.filter(getattr(SkpdModel, 'kode_skpd') == old_skpd)
            update1 = update1.update({'count_visualization': count1['count']}, synchronize_session='fetch')
            db.session.commit()

            # new skpd
            # public only
            result2, count2 = VISUALIZATION_CONTROLLER.get_count(
                {
                    'kode_skpd': new_skpd,
                    'is_deleted': False,
                    'is_active': True,
                    'dataset_class_id': 3
                }, '')
            update2 = db.session.query(SkpdModel)
            update2 = update2.filter(getattr(SkpdModel, 'kode_skpd') == new_skpd)
            update2 = update2.update({'count_visualization_public': count2['count']}, synchronize_session='fetch')
            db.session.commit()
            # with private
            result2, count2 = VISUALIZATION_CONTROLLER.get_count(
                {
                    'kode_skpd': new_skpd,
                    'is_deleted': False,
                    'is_active': True
                }, '')
            update2 = db.session.query(SkpdModel)
            update2 = update2.filter(getattr(SkpdModel, 'kode_skpd') == new_skpd)
            update2 = update2.update({'count_visualization': count2['count']}, synchronize_session='fetch')
            db.session.commit()

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'visualization'
            json_history['type_id'] = visualization['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'update'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)


        # trigger update highlight
        if 'is_active' in json_request or 'is_deleted' in json_request:
            where_hi = {}
            where_hi['category'] = 'visualisasi'
            where_hi['category_id'] = visualization['id']
            res_hi, highlight = HIGHLIGHT_CONTROLLER.get_all(where_hi, "", ['id', 'asc'], 1, 0)
            if res_hi:
                where_hi['id'] = highlight[0]['id']
                json_request_hi = {}
                json_request_hi['is_active'] = visualization['is_deleted']
                # json_request_hi['is_deleted'] = visualization['is_deleted']
                res_hi, highlight = HIGHLIGHT_CONTROLLER.update(where_hi, json_request_hi)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": visualization}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete visualization
        res = VISUALIZATION_CONTROLLER.delete(where)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'visualization'
            json_history['type_id'] = _id
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'delete'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization/all", methods=["GET"])
@jwt_check()
def get_alls():
    ''' Doc: function get alls'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 1000)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        res, visualization = VISUALIZATION_CONTROLLER.get_alls(where, search, sort, limit, skip)
        meta = {}

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": visualization, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
