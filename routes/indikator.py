''' Doc: route indikator'''
import json
from flask import request, Blueprint, Response, send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
import math
from controllers import IndikatorController, HistoryController, SearchController
from models import SkpdModel
from models import IndikatorModel
from models import IndikatorProgramModel
from models import IndikatorTerhubungModel
from models import IndikatorDatasetModel
from models import IndikatorDatadasarModel
from settings import configuration as conf
from helpers.autocorecction import AutoCorecction

BP = Blueprint('indikator', __name__)
INDIKATOR_CONTROLLER = IndikatorController()
SEARCH_CONTROLLER = SearchController()
HISTORY_CONTROLLER = HistoryController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/indikator", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        download = request.args.get("download", '')
        suggestion = request.args.get("suggestion", "")

        # prepare data filter
        sort = sort.split(':')
        jwt = HELPER.read_jwt()

        where_data = where
        where_data = where_data.replace("'", "\"")
        where_data = json.loads(where_data)

        where_count = where
        where_count = where_count.replace("'", "\"")
        where_count = json.loads(where_count)

        correction_data = {}
        correction_data['status'] = False
        correction_data['suggestion_text'] = ""
        correction_data['original_text'] = search
        if search and suggestion == "on":
            status, data_corecction = AutoCorecction.indicator(search)
            correction_data['status'] = status
            correction_data['suggestion_text'] = data_corecction
            correction_data['original_text'] = search
            search = data_corecction
        else:
            search = search

        if count:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_count(where_data, search)
        else:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_all(where_data, search, sort, limit, skip)

            # download
            # download_function(result_indikator, download, jwt)
            if result_indikator:
                ids = result_indikator[0]['id']
                if download:
                    result = INDIKATOR_CONTROLLER.export_bulk(result_indikator, jwt['kode_skpd'])
                    columns = INDIKATOR_CONTROLLER.column_title()
                    columns_first = INDIKATOR_CONTROLLER.column_first(ids)
                    columns_target = INDIKATOR_CONTROLLER.column_target(ids)
                    columns_result = INDIKATOR_CONTROLLER.column_result(ids)
                    columns_last = INDIKATOR_CONTROLLER.column_last(ids)
                    columns_titles = columns + columns_first + columns_target + columns_result + columns_last

                    if download.lower() == 'xls' or download.lower() == 'excel':
                        filename = HELPER.convert_xls('static/download/indikator.xlsx', result, columns_titles)
                        return send_file(
                            '../' + filename,
                            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            attachment_filename='indikator.xlsx',
                            as_attachment=True
                        )
                    else:
                        filename = HELPER.convert_csv('static/download/indikator.csv', result, columns_titles)
                        return send_file(
                            '../' + filename,
                            mimetype='application/x-csv',
                            attachment_filename='indikator.csv',
                            as_attachment=True
                        )

        if search:
            try:
                jwt = HELPER.read_jwt()
                payload = {}
                payload['search'] = search
                payload['user_id'] = jwt['id']
                res2, data2 = SEARCH_CONTROLLER.create(payload)
            except Exception as err:
                pass

        meta = {}
        res_count, count = INDIKATOR_CONTROLLER.get_count(where_count, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result_indikator, "meta": meta,
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {},
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        jwt = HELPER.read_jwt()
        download = request.args.get("download", '')
        where = request.args.get("where", "{}")

        where_data = where
        where_data = where_data.replace("'", "\"")
        where_data = json.loads(where_data)

        res, result_indikator, message = INDIKATOR_CONTROLLER.get_by_id(_id, where_data)

        if download:
            if result_indikator[0]['kode_skpd'] != jwt['kode_skpd']:

                count_access = result_indikator[0]['count_access']
                count_access = int(count_access) + 1
                where = {'id': int(_id)}
                payload = {'count_access': int(count_access)}
                INDIKATOR_CONTROLLER.update(where, payload)

            result = INDIKATOR_CONTROLLER.export_by_id(_id, result_indikator)

            columns = INDIKATOR_CONTROLLER.column_title()
            columns_first = INDIKATOR_CONTROLLER.column_first(_id)
            columns_target = INDIKATOR_CONTROLLER.column_target(_id)
            columns_result = INDIKATOR_CONTROLLER.column_result(_id)
            columns_last = INDIKATOR_CONTROLLER.column_last(_id)
            columns_titles = columns + columns_first + columns_target + columns_result + columns_last

            if download.lower() == 'xls' or download.lower() == 'excel':
                filename = HELPER.convert_xls('static/download/indikator.xlsx', result, columns_titles)
                return send_file(
                    '../' + filename,
                    mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    attachment_filename='indikator.xlsx',
                    as_attachment=True
                )
            else:
                filename = HELPER.convert_csv('static/download/indikator.csv', result, columns_titles)
                return send_file(
                    '../' + filename,
                    mimetype='application/x-csv',
                    attachment_filename='indikator.csv',
                    as_attachment=True
                )

        if result_indikator and result_indikator[0]['kode_skpd'] != jwt['kode_skpd']:
            count_view = result_indikator[0]['count_view']
            if not count_view:
                count_view = 0
            count_view = int(count_view) + 1
            where = {'id': int(_id)}
            payload = {'count_view': int(count_view)}
            res1, data1 = INDIKATOR_CONTROLLER.update(where, payload)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": result_indikator[0]}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator/counter/<_id>", methods=["GET"])
# @jwt_check()
def counter(_id):
    ''' Doc: function counter'''
    # category
    category = request.args.get("category", "")

    # handle id=title, integer or string
    try:
        temp = int(_id)
        _id = temp
    except Exception as err:
        temp = _id
        result = db.session.query(IndikatorModel.id)
        result = result.filter(IndikatorModel.title == temp)
        result = result.all()
        if result:
            result = result[0]
            _id = result[0]
        else:
            _id = 0

    try:
        res, indikator, message = INDIKATOR_CONTROLLER.get_by_id(_id, {})

        # category counter
        INDIKATOR_CONTROLLER.counter(category, indikator)

        # insert to history
        INDIKATOR_CONTROLLER.insert_history(category)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": indikator}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)
        indikator_program = []
        indikator_terhubung = []
        indikator_dataset = []
        indikator_data_dasar = []

        if 'indikator_program' in json_request:
            indikator_program = json_request['indikator_program']
            del json_request['indikator_program']

        if 'indikator_terhubung' in json_request:
            indikator_terhubung = json_request['indikator_terhubung']
            del json_request['indikator_terhubung']

        if 'indikator_dataset' in json_request:
            indikator_dataset = json_request['indikator_dataset']
            del json_request['indikator_dataset']

        if 'indikator_data_dasar' in json_request:
            indikator_data_dasar = json_request['indikator_data_dasar']
            del json_request['indikator_data_dasar']
            json_request['data_dasar'] = ""
            for data_dasar in indikator_data_dasar:
                json_request['data_dasar'] += data_dasar + ", "

        # insert indikator
        res, result_indikator = INDIKATOR_CONTROLLER.create(json_request)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'indikator'
            json_history['type_id'] = result_indikator[0]['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'create'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

            # insert indikator_program
            for program in indikator_program:
                res_program, data_program = INDIKATOR_CONTROLLER.get_program_all({'id_program': program}, '', ['kode_program','ASC'], '500', 0)
                if res_program:
                    json_program = {}
                    json_program['indikator_id'] = result_indikator[0]['id']
                    json_program['kode_program'] = data_program[0]['kode_program']
                    json_program['id_program'] = data_program[0]['id_program']
                    json_program['cuid'] = jwt['id']
                    json_program['cdate'] = HELPER.local_date_server()
                    result_program = IndikatorProgramModel(**json_program)
                    db.session.add(result_program)
                    db.session.commit()

            # insert indikator_terhubung
            for terhubung in indikator_terhubung:
                json_terhubung = {}
                json_terhubung['indikator_id'] = result_indikator[0]['id']
                json_terhubung['indikator_terhubung_id'] = terhubung
                json_terhubung['cuid'] = jwt['id']
                json_terhubung['cdate'] = HELPER.local_date_server()
                result_terhubung = IndikatorTerhubungModel(**json_terhubung)
                db.session.add(result_terhubung)
                db.session.commit()

            # insert indikator_dataset
            for dataset in indikator_dataset:
                json_dataset = {}
                json_dataset['indikator_id'] = result_indikator[0]['id']
                json_dataset['dataset_id'] = dataset
                json_dataset['cuid'] = jwt['id']
                json_dataset['cdate'] = HELPER.local_date_server()
                result_dataset = IndikatorDatasetModel(**json_dataset)
                db.session.add(result_dataset)
                db.session.commit()

            # insert indikator_datadasar
            for data_dasar in indikator_data_dasar:
                json_data_dasar = {}
                json_data_dasar['indikator_id'] = result_indikator[0]['id']
                json_data_dasar['name'] = data_dasar
                json_data_dasar['cuid'] = jwt['id']
                json_data_dasar['cdate'] = HELPER.local_date_server()
                result_data_dasar = IndikatorDatadasarModel(**json_data_dasar)
                db.session.add(result_data_dasar)
                db.session.commit()

        # update count_indikator
        if 'kode_skpd' in json_request:
            # public only
            result, count = INDIKATOR_CONTROLLER.get_count(
                {
                    'kode_skpd': json_request['kode_skpd'],
                    'is_deleted': False,
                    'is_active': True
                }, '')
            update_ = db.session.query(SkpdModel)
            update_ = update_.filter(getattr(SkpdModel, 'kode_skpd') == json_request['kode_skpd'])
            update_ = update_.update(
                {
                    'count_indikator_public': count['count'],
                    'count_indikator': count['count']
                }, synchronize_session='fetch')
            db.session.commit()

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": result_indikator[0]}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)
        indikator_program = []
        indikator_terhubung = []
        indikator_dataset = []
        indikator_data_dasar = []

        if 'indikator_program' in json_request:
            indikator_program = json_request['indikator_program']
            del json_request['indikator_program']

        if 'indikator_terhubung' in json_request:
            indikator_terhubung = json_request['indikator_terhubung']
            del json_request['indikator_terhubung']

        if 'indikator_dataset' in json_request:
            indikator_dataset = json_request['indikator_dataset']
            del json_request['indikator_dataset']

        if 'indikator_data_dasar' in json_request:
            indikator_data_dasar = json_request['indikator_data_dasar']
            del json_request['indikator_data_dasar']
            json_request['data_dasar'] = ""
            for data_dasar in indikator_data_dasar:
                json_request['data_dasar'] += data_dasar + ", "

        if 'kode_skpd' in json_request:
            # get kode_skpd
            result, indikator, message = INDIKATOR_CONTROLLER.get_by_id(_id, {})
            old_skpd = indikator[0]['kode_skpd']
            new_skpd = json_request['kode_skpd']
        else:
            # get kode_skpd
            result, indikator, message = INDIKATOR_CONTROLLER.get_by_id(_id, {})
            old_skpd = indikator[0]['kode_skpd']
            new_skpd = indikator[0]['kode_skpd']
            json_request['kode_skpd'] = indikator[0]['kode_skpd']

        # update indiaktor
        res, result_indikator = INDIKATOR_CONTROLLER.update(where, json_request)

        # update count_indikator
        if 'kode_skpd' in json_request:
            # old skpd
            # public only
            result1, count1 = INDIKATOR_CONTROLLER.get_count(
                {
                    'kode_skpd': old_skpd,
                    'is_deleted': False,
                    'is_active': True
                }, '')
            update1 = db.session.query(SkpdModel)
            update1 = update1.filter(getattr(SkpdModel, 'kode_skpd') == old_skpd)
            update1 = update1.update(
                {
                    'count_indikator_public': count1['count'],
                    'count_indikator': count1['count']
                }, synchronize_session='fetch')
            db.session.commit()

            # new skpd
            # public only
            result2, count2 = INDIKATOR_CONTROLLER.get_count(
                {
                    'kode_skpd': new_skpd,
                    'is_deleted': False,
                    'is_active': True
                }, '')
            update2 = db.session.query(SkpdModel)
            update2 = update2.filter(getattr(SkpdModel, 'kode_skpd') == new_skpd)
            update2 = update2.update(
                {
                    'count_indikator_public': count2['count'],
                    'count_indikator': count2['count']
                }, synchronize_session='fetch')
            db.session.commit()

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'indikator'
            json_history['type_id'] = result_indikator[0]['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'update'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

            # delete indikator_program
            if indikator_program:
                rip = db.session.query(IndikatorProgramModel)
                rip = rip.filter(getattr(IndikatorProgramModel, 'indikator_id') == _id)
                rip = rip.all()
                for ip in rip:
                    db.session.delete(ip)
                    db.session.commit()
            # insert indikator_program
            for program in indikator_program:
                res_program, data_program = INDIKATOR_CONTROLLER.get_program_all({'id_program': program}, '', ['kode_program','ASC'], '500', 0)
                if res_program:
                    json_program = {}
                    json_program['indikator_id'] = result_indikator[0]['id']
                    json_program['kode_program'] = data_program[0]['kode_program']
                    json_program['id_program'] = data_program[0]['id_program']
                    json_program['cuid'] = jwt['id']
                    json_program['cdate'] = HELPER.local_date_server()
                    result_program = IndikatorProgramModel(**json_program)
                    db.session.add(result_program)
                    db.session.commit()


            # delete indikator_terhubung
            if indikator_terhubung:
                rit = db.session.query(IndikatorTerhubungModel)
                rit = rit.filter(getattr(IndikatorTerhubungModel, 'indikator_id') == _id)
                rit = rit.all()
                for it in rit:
                    db.session.delete(it)
                    db.session.commit()
            # insert indikator_terhubung
            for terhubung in indikator_terhubung:
                json_terhubung = {}
                json_terhubung['indikator_id'] = result_indikator[0]['id']
                json_terhubung['indikator_terhubung_id'] = terhubung
                json_terhubung['cuid'] = jwt['id']
                json_terhubung['cdate'] = HELPER.local_date_server()
                result_terhubung = IndikatorTerhubungModel(**json_terhubung)
                db.session.add(result_terhubung)
                db.session.commit()

            # delete indikator_dataset
            if indikator_dataset:
                rid = db.session.query(IndikatorDatasetModel)
                rid = rid.filter(getattr(IndikatorDatasetModel, 'indikator_id') == _id)
                rid = rid.all()
                for id in rid:
                    db.session.delete(id)
                    db.session.commit()
            # insert indikator_dataset
            for dataset in indikator_dataset:
                json_dataset = {}
                json_dataset['indikator_id'] = result_indikator[0]['id']
                json_dataset['dataset_id'] = dataset
                json_dataset['cuid'] = jwt['id']
                json_dataset['cdate'] = HELPER.local_date_server()
                result_dataset = IndikatorDatasetModel(**json_dataset)
                db.session.add(result_dataset)
                db.session.commit()

            # delete indikator_data_dasar
            if indikator_data_dasar:
                ridd = db.session.query(IndikatorDatadasarModel)
                ridd = ridd.filter(getattr(IndikatorDatadasarModel, 'indikator_id') == _id)
                ridd = ridd.all()
                for id in ridd:
                    db.session.delete(id)
                    db.session.commit()
            # insert indikator_data_dasar
            for data_dasar in indikator_data_dasar:
                json_data_dasar = {}
                json_data_dasar['indikator_id'] = result_indikator[0]['id']
                json_data_dasar['name'] = data_dasar
                json_data_dasar['cuid'] = jwt['id']
                json_data_dasar['cdate'] = HELPER.local_date_server()
                result_data_dasar = IndikatorDatadasarModel(**json_data_dasar)
                db.session.add(result_data_dasar)
                db.session.commit()

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": result_indikator[0]}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete indikator
        res = INDIKATOR_CONTROLLER.delete(where)

        # insert indikator
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'indikator'
            json_history['type_id'] = _id
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'delete'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)
        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_program", methods=["GET"])
@jwt_check()
def get_program():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'kode_program:ASC')
        limit = request.args.get("limit", 500)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')
        jwt = HELPER.read_jwt()

        where_data = where
        where_data = where_data.replace("'", "\"")
        where_data = json.loads(where_data)

        where_count = where
        where_count = where_count.replace("'", "\"")
        where_count = json.loads(where_count)

        if count:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_program_count(where_data, search)
        else:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_program_all(where_data, search, sort, limit, skip)

        meta = {}
        res_count, count = INDIKATOR_CONTROLLER.get_program_count(where_count, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result_indikator, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_terhubung", methods=["GET"])
@jwt_check()
def get_terhubung():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 500)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')
        jwt = HELPER.read_jwt()

        where_data = where
        where_data = where_data.replace("'", "\"")
        where_data = json.loads(where_data)

        where_count = where
        where_count = where_count.replace("'", "\"")
        where_count = json.loads(where_count)

        if count:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_terhubung_count(where_data, search)
        else:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_terhubung_all(where_data, search, sort, limit, skip)

        meta = {}
        res_count, count = INDIKATOR_CONTROLLER.get_terhubung_count(where_count, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result_indikator, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/indikator_dataset", methods=["GET"])
@jwt_check()
def get_dataset():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 5000)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')
        jwt = HELPER.read_jwt()

        where_data = where
        where_data = where_data.replace("'", "\"")
        where_data = json.loads(where_data)

        where_count = where
        where_count = where_count.replace("'", "\"")
        where_count = json.loads(where_count)

        if count:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_dataset_count(where_data, search)
        else:
            # call function get
            res, result_indikator = INDIKATOR_CONTROLLER.get_dataset_all(where_data, search, sort, limit, skip)

        meta = {}
        res_count, count = INDIKATOR_CONTROLLER.get_dataset_count(where_count, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": result_indikator, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
