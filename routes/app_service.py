''' Doc: route app service'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import AppServiceController

BP = Blueprint('app_service',__name__)
APP_SERVICE_CONTROLLER = AppServiceController()
# pylint: disable=broad-except

@BP.route("/app_service", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        enable_id = request.args.get("enable_id", 0)
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, app_service = APP_SERVICE_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, app_service = APP_SERVICE_CONTROLLER.get_all(where, search, sort, limit, skip, enable_id)

        # response
        if res:
            # success response
            response = {"message": "Get data successfull", "error": 0, "data": app_service}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/app_service/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, app_service = APP_SERVICE_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": app_service}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/app_service", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # insert dataset_tag
            res, app_service = APP_SERVICE_CONTROLLER.create_multiple(json_request)
        else:
            # insert app_service
            res, app_service = APP_SERVICE_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": app_service}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/app_service/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update app_service
        res, app_service = APP_SERVICE_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": app_service}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/app_service/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete app_service
        res = APP_SERVICE_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/app_service/permission", methods=["GET"])
@jwt_check()
def get_all_permission():
    ''' Doc: function get all permission'''
    try:
        # filter data
        app_id = request.args.get("app_id", "")
        user_id = request.args.get("user_id", "")

        if app_id and user_id:
            # call function get
            res, app_service = APP_SERVICE_CONTROLLER.get_all_permission(app_id, user_id)
        else:
            response = {"message": "Parameter app_id and user_id not found. ", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        # response
        if res:
            # success response
            response = {"message": "Get data successfull", "error": 0, "data": app_service}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500
