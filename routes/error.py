''' Doc: route error'''
import logging
import json
from flask import Blueprint
from flask import jsonify
from flask import Response
from helpers.jsonencoder import JSONEncoder
from exceptions import RestException
from settings import configuration as conf

BP = Blueprint( "error", __name__)
LOG = logging.getLogger(__name__)
# pylint:

@BP.app_errorhandler(RestException)
def handle_rest_exception(err):
    """This error will show if RestException raised"""
    LOG.warning(err)
    return Response(json.dumps(err.to_dict(), cls=JSONEncoder), mimetype='application/json')


@BP.app_errorhandler(404)
def page_not_found(err):
    """Not found handle"""
    LOG.warning(err)
    response = {"error": 1, "message": "Resource Not exists", "data": {}}
    return jsonify(response), 404


@BP.app_errorhandler(405)
def method_not_allowed(err):
    """Method not allowed handle"""
    LOG.warning(err)
    response = {"error": 1, "message": "Method not allowed", "data": {}}
    return jsonify(response), 405


@BP.app_errorhandler(500)
def server_error(err):
    """This error will show if error un handle"""
    LOG.error(err)
    response = {"error": 1, "message": "Internal server error", "data": {}}
    return jsonify(response), 500
