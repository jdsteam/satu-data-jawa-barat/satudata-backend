'''app route'''
import json
import math
import datetime
from flask import Blueprint
from flask import request
from flask import Response
from flask import send_file
from flask import render_template
from helpers import Helper
from helpers.decorator import jwt_check_coredata
from helpers.jsonencoder import JSONEncoder
from controllers import BigdataController
from controllers import DatasetController
from controllers import MetadataController
from zipfile import ZipFile
import re

BP = Blueprint('bigdata', __name__)
HELPER = Helper()
BIGDATA_CONTROLLER = BigdataController()
DATASET_CONTROLLER = DatasetController()
METADATA_CONTROLLER = MetadataController()
CLEANR = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
# pylint: disable=broad-except, unused-variable, line-too-long


@BP.route("/bigdata/restart", methods=["GET"])
# @jwt_check()
def get_restart():
    '''Doc: function doc'''
    try:
        # get model
        res = BIGDATA_CONTROLLER.force_restart()
        response = {"message": "Get detail data successfull",
                    "error": 0, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/bigdata/schema", methods=["GET"])
# @jwt_check()
def get_schema():
    '''Doc: function doc'''
    try:
        # get model
        data = BIGDATA_CONTROLLER.get_schema()
        response = {"message": "Get detail data successfull",
                    "error": 0, "data": data}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": [], "metadata": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/bigdata/table", methods=["GET"])
# @jwt_check()
def get_table():
    '''Doc: function doc'''
    try:
        schema = request.args.get("schema", "")
        is_used = request.args.get("is_used", "")

        # get model
        data = BIGDATA_CONTROLLER.get_table(schema, is_used)
        response = {"message": "Get detail data successfull",
                    "error": 0, "data": data}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": [], "metadata": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/bigdata/<schema>/<table>/doc", methods=["GET"])
# @jwt_check()
def get_doc(schema, table):
    '''Doc: function doc'''
    try:
        # get model
        model, metadata = BIGDATA_CONTROLLER.generate_metadata(schema, table)

        # write file
        file_name = table.replace(' ', '_').replace('-', '_')
        controller_name = table.title().replace(
            ' ', '').replace('_', '').replace('-', '')

        old_file = 'static/doc/template.json'
        new_file = 'static/doc/' + schema + '-' + table + '.json'

        with open(old_file, "rt", encoding='UTF8') as f2_:
            new_text = f2_.read()
            new_text = new_text.replace('template', file_name)
            new_text = new_text.replace('Template', controller_name)

            loaded_json = json.loads(new_text)

            loaded_json['components']['schemas']['Model' +
                                                 controller_name+'Body']['properties'] = {}
            loaded_json['components']['schemas']['Model' +
                                                 controller_name+'Response']['properties'] = {}
            for md_ in model:
                column = str(md_['column'])
                data_type = md_['data_type'].split('(')

                if isinstance(data_type, list):
                    data_type = data_type[0].lower()
                else:
                    data_type = md_['data_type'].lower()

                if column != 'id':
                    loaded_json['components']['schemas']['Model' +
                                                         controller_name+'Body']['properties'][column] = {}
                    loaded_json['components']['schemas']['Model'+controller_name +
                                                         'Body']['properties'][column]['type'] = data_type

                loaded_json['components']['schemas']['Model' +
                                                     controller_name+'Response']['properties'][column] = {}
                loaded_json['components']['schemas']['Model'+controller_name +
                                                     'Response']['properties'][column]['type'] = data_type

            loaded_json['servers'][0]['url'] = '/api-backend/bigdata/' + schema + '/'

        with open(new_file, "wt") as f2_:
            json.dump(loaded_json, f2_, sort_keys=True, indent=4)

        # swagger load

        default_config = {
            'app_name': schema.title() + ' ' + table.title() + ' - ',
            'dom_id': '#swagger-ui',
            'url': '/api-backend/static/doc/'+schema+'-'+table+'.json',
            'layout': 'StandaloneLayout'
        }

        fields = {
            'base_url': '/api-backend/static',
            'app_name': default_config.pop('app_name'),
            'config_json': json.dumps(default_config),
        }

        return render_template('swagger.html', **fields)

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": [], "metadata": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/bigdata/<schema>/<table>", methods=["GET"])
@jwt_check_coredata()
def get_all(schema, table):
    '''Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        where_or = request.args.get("where_or", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        download = request.args.get("download", '')
        exclude = request.args.get("exclude", '')

        # prepare data filter
        if sort:
            sort = sort.split(':')
        else:
            sort = []
            sort.append('null')

        if exclude:
            exclude = exclude.split(',')

        where = where.replace("'", "\"")
        where = json.loads(where)

        where_or = where_or.replace("'", "\"")
        where_or = json.loads(where_or)

        # get metadata
        model, metadata = BIGDATA_CONTROLLER.generate_metadata(schema, table)

        # get meta
        meta = {}
        res_count, counts = BIGDATA_CONTROLLER.get_count(
            schema, table, where, where_or, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(counts['count'])
        meta['total_page'] = math.ceil(int(counts['count']) / int(limit))

        # call function get
        if count:
            res, app = BIGDATA_CONTROLLER.get_count(
                schema, table, where, where_or, search)
            metadata_filter = BIGDATA_CONTROLLER.get_metadata_filter(
                schema, table, metadata)
        # call function download
        elif ('csv' in download.lower()) or ('pdf' in download.lower()) or ('xls' in download.lower()):
            if download.lower() == 'csv':
                res, app = BIGDATA_CONTROLLER.get_all(
                    schema, table, where, where_or, search, sort, meta['total_record'], skip, exclude)
                filename = HELPER.convert_csv(
                    'static/download/' + schema + '-' + table + '.csv', app, metadata)
                return send_file('../'+filename, mimetype='application/x-csv', attachment_filename=schema + '-' + table + '.csv', as_attachment=True)
            elif download.lower() == 'xls':
                res, app = BIGDATA_CONTROLLER.get_all(
                    schema, table, where, where_or, search, sort, meta['total_record'], skip, exclude)
                filename = HELPER.convert_xls(
                    'static/download/' + schema + '-' + table + '.xlsx', app, metadata)
                return send_file('../'+filename, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', attachment_filename=schema + '-' + table + '.xlsx', as_attachment=True)
            elif download.lower() == 'pdf':
                res, app = BIGDATA_CONTROLLER.get_all(
                    schema, table, where, where_or, search, sort, meta['total_record'], skip, exclude)
                filename = HELPER.convert_csv(
                    'static/download/' + schema + '-' + table + '.csv', app, metadata)
                filename2 = HELPER.convert_pdf(filename)
                return send_file('../'+filename2, mimetype='application/pdf', attachment_filename=schema + '-' + table + '.pdf', as_attachment=True)
            elif download.lower() == 'csvzip':
                metadata_print = []
                # get dataset
                res_dataset, data_dataset = DATASET_CONTROLLER.get_all(
                    {'schema': schema, 'table': table}, '', ['id', 'desc'], 1, 0)
                if res_dataset:
                    metadata_print.append(
                        {'key': 'Judul Dataset', 'value': data_dataset[0]['name']})
                    metadata_print.append(
                        {'key': 'Deskripsi', 'value': cleanhtml(data_dataset[0]['description'])})
                    metadata_print.append({'key': 'Dataset Diperbarui', 'value': datetime.datetime.strptime(
                        data_dataset[0]['mdate'], '%Y-%m-%d %H:%M:%S').strftime('%d %B %Y %H:%M:%S')})
                    metadata_print.append({'key': 'Dataset Dibuat', 'value': datetime.datetime.strptime(
                        data_dataset[0]['cdate'], '%Y-%m-%d %H:%M:%S').strftime('%d %B %Y %H:%M:%S')})

                    # get metadata
                    res_metadata, data_metadata = METADATA_CONTROLLER.get_all(
                        {'dataset_id': data_dataset[0]['id']}, '', ['key', 'asc'], 100, 0)
                    if res_metadata:
                        for dm_ in data_metadata:
                            metadata_print.append(
                                {'key': dm_['key'], 'value': str(dm_['value'])})

                    # create file
                    res, app = BIGDATA_CONTROLLER.get_all(
                        schema, table, where, where_or, search, sort, meta['total_record'], skip, exclude)
                    filename_data = HELPER.convert_csv(
                        'data.jabarprov.go.id/' + schema + '-' + table + '_data.csv', app, metadata)
                    filename_metadata = HELPER.convert_pdf_metadata(
                        'data.jabarprov.go.id/' + schema + '-' + table + '_metadata.pdf', metadata_print)

                    # create a ZipFile object
                    with ZipFile('data.jabarprov.go.id/' + schema + '-' + table + '.zip', 'w') as zip_obj2:
                        zip_obj2.write(filename_data)
                        zip_obj2.write(filename_metadata)
                    return send_file('../data.jabarprov.go.id/' + schema + '-' + table + '.zip', mimetype='application/zip', attachment_filename=schema + '-' + table + '.zip', as_attachment=True)
                else:
                    response = {"message": "Data not found", "error": 1,
                                "data": [], "metadata": metadata, "meta": meta}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
            elif download.lower() == 'xlszip':
                metadata_print = []
                # get dataset
                res_dataset, data_dataset = DATASET_CONTROLLER.get_all(
                    {'schema': schema, 'table': table}, '', ['id', 'desc'], 1, 0)
                if res_dataset:
                    metadata_print.append(
                        {'key': 'Judul Dataset', 'value': data_dataset[0]['name']})
                    metadata_print.append(
                        {'key': 'Deskripsi', 'value': cleanhtml(data_dataset[0]['description'])})
                    metadata_print.append({'key': 'Dataset Diperbarui', 'value': datetime.datetime.strptime(
                        data_dataset[0]['mdate'], '%Y-%m-%d %H:%M:%S').strftime('%d %B %Y %H:%M:%S')})
                    metadata_print.append({'key': 'Dataset Dibuat', 'value': datetime.datetime.strptime(
                        data_dataset[0]['cdate'], '%Y-%m-%d %H:%M:%S').strftime('%d %B %Y %H:%M:%S')})

                    # get metadata
                    res_metadata, data_metadata = METADATA_CONTROLLER.get_all(
                        {'dataset_id': data_dataset[0]['id']}, '', ['key', 'asc'], 100, 0)
                    if res_metadata:
                        for dm_ in data_metadata:
                            if dm_['key']:
                                metadata_print.append(
                                    {'key': dm_['key'], 'value': str(dm_['value'])})

                    # create file
                    res, app = BIGDATA_CONTROLLER.get_all(
                        schema, table, where, where_or, search, sort, meta['total_record'], skip, exclude)
                    filename_data = HELPER.convert_xls(
                        'data.jabarprov.go.id/' + schema + '-' + table + '_data.xlsx', app, metadata)
                    filename_metadata = HELPER.convert_pdf_metadata(
                        'data.jabarprov.go.id/' + schema + '-' + table + '_metadata.pdf', metadata_print)

                    # create a ZipFile object
                    with ZipFile('data.jabarprov.go.id/' + schema + '-' + table + '.zip', 'w') as zip_obj2:
                        zip_obj2.write(filename_data)
                        zip_obj2.write(filename_metadata)
                    return send_file('../data.jabarprov.go.id/' + schema + '-' + table + '.zip', mimetype='application/zip', attachment_filename=schema + '-' + table + '.zip', as_attachment=True)
                else:
                    response = {"message": "Data not found", "error": 1,
                                "data": [], "metadata": metadata, "meta": meta}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
            else:
                response = {"message": "Parameter not found", "error": 1,
                            "data": [], "metadata": metadata, "meta": meta}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        # call function get
        else:
            res, app_temp = BIGDATA_CONTROLLER.get_all(schema, table, where, where_or, search, sort, limit, skip, exclude)

            # check date field
            date_status = False
            date_field = []
            for m in metadata:
                if 'tanggal' in m:
                    date_status = True
                    date_field.append(m)

            # convert date field
            app = []
            for a in app_temp:
                if date_status:
                    for df in date_field:
                        a[df] = str(a[df])
                app.append(a)


            metadata_filter = BIGDATA_CONTROLLER.get_metadata_filter(schema, table, metadata)

        # response
        if res:
            # success response
            response = {"message": "Get data successfull", "error": 0, "data": app,
                        "metadata": metadata, "meta": meta, "metadata_filter": metadata_filter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": [
            ], "metadata": metadata, "meta": meta, "metadata_filter": metadata_filter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(
            err), "error": 1, "data": [], "metadata": [], "meta": {}, "metadata_filter": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/bigdata/<schema>/<table>/<ids>", methods=["GET"])
@jwt_check_coredata()
def get_by_id(schema, table, ids):
    '''Doc: function get by id'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        exclude = request.args.get("exclude", '')

        if exclude:
            exclude = exclude.split(',')

        where = where.replace("'", "\"")
        where = json.loads(where)

        # call function get by id
        res, app = BIGDATA_CONTROLLER.get_by_id(schema, table, ids)

        # get metadata
        model, metadata = BIGDATA_CONTROLLER.generate_metadata(schema, table)

        # get meta
        meta = {}
        res_count, count = BIGDATA_CONTROLLER.get_count(
            schema, table, where, {}, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        meta['total_page'] = math.ceil(int(count['count']) / int(limit))

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull",
                        "error": 0, "data": app, "metadata": metadata, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1,
                        "data": {}, "metadata": metadata, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": {}, "metadata": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


# @BP.route("/bigdata/<schema>/<table>", methods=["POST"])
# @jwt_check_coredata()
# def create(schema, table):
#     '''Doc: function create'''
#     try:
#         # prepare json data
#         json_request = request.get_json(silent=True)

#         # insert app
#         res, app = BIGDATA_CONTROLLER.create(schema, table, json_request)

#         # response
#         if res:
#             # success response
#             response = {"message": "Create data successfull", "error": 0, "data": app}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
#         else:
#             # success response but no data
#             response = {"message": "Create data failed, ", "error": 1, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

#     except Exception as err:
#         # fail response
#         response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
#         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


# @BP.route("/bigdata/<schema>/<table>/<ids>", methods=["PUT"])
# @jwt_check_coredata()
# def update(schema, table, ids):
#     '''Doc: function update'''
#     try:
#         # filter data
#         where = {'id': ids}

#         # prepare json data
#         json_request = request.get_json(silent=True)

#         # update app
#         res, app = BIGDATA_CONTROLLER.update(schema, table, where, json_request)

#         # response
#         if res:
#             # success response
#             response = {"message": "Update data successfull", "error": 0, "data": app}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
#         else:
#             # success response but no data
#             response = {"message": "Update data failed", "error": 1, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

#     except Exception as err:
#         # fail response
#         response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
#         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


# @BP.route("/bigdata/<schema>/<table>/<ids>", methods=["DELETE"])
# @jwt_check_coredata()
# def delete(schema, table, ids):
#     '''Doc: function delete'''
#     try:
#         # filter data
#         where = {'id': ids}

#         # delete app
#         res = BIGDATA_CONTROLLER.delete(schema, table, where)

#         # response
#         if res:
#             # success response
#             response = {"message": "Delete data successfull", "error": 0, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
#         else:
#             # success response but no data
#             response = {"message": "Delete data failed", "error": 1, "data": {}}
#             return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

#     except Exception as err:
#         # fail response
#         response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
#         return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/bigdata/dashboard/insight/<schema>/<table>", methods=["GET"])
# @jwt_check_coredata()
def get_dashboard_insight(schema, table):
    '''Doc: function get all'''

    try:
        # call controller
        res, result_metadata, result_data = BIGDATA_CONTROLLER.get_dashboard_insight(schema, table)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": result_data, "metadata": result_metadata}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Dashboard not found", "error": 1, "data": {}, "metadata": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}, "metadata": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


def cleanhtml(raw_html):
    cleantext = re.sub(CLEANR, '', raw_html)
    return cleantext
