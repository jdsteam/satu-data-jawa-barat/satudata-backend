''' Doc: route user '''
import json
from flask import Blueprint, request, Response
from flask import send_file
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import UserController

BP = Blueprint('user_management', __name__)
HELPER = Helper()
USER_CONTROLLER = UserController()
# pylint: disable=broad-except, unused-variable


@BP.route("/user", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        download = request.args.get("download", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        # get jwt payload
        jwt = HELPER.read_jwt()
        if jwt['role_id'] != 1:
            where['id'] = jwt['id']

        if count:
            # call function get
            res, user = USER_CONTROLLER.get_count(where, search)
        elif download:
            # download
            local_date = HELPER.local_date_server().replace(
                '-', '').replace(':', '').replace(' ', '.')
            res, count = USER_CONTROLLER.get_count(where, search)
            res, user, metadata = USER_CONTROLLER.get_download(
                where, search, sort, count['count'], skip)
            filename = HELPER.convert_csv(
                'static/download/user_' + local_date + '.csv', user, metadata)
            return send_file('../'+filename, mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', attachment_filename='user_' + local_date + '.csv', as_attachment=True)
        else:
            # call function get
            res, user = USER_CONTROLLER.get_all(
                where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": "Get data successfull",
                        "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/user/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # get jwt payload
        jwt = HELPER.read_jwt()
        if jwt['role_id'] != 1:
            if int(_id) != int(jwt['id']):
                _id = 0

        # call function get by id
        res, user = USER_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull",
                        "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/user", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # get jwt payload
        jwt = HELPER.read_jwt()
        if jwt['role_id'] != 1:
            response = {"message": "Does Not Have Access", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        # prepare json data
        json_request = request.get_json(silent=True)

        # insert user
        res, user, message = USER_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, " +
                        str(message), "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/user/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # get jwt payload
        jwt = HELPER.read_jwt()
        if jwt['role_id'] != 1:
            if int(_id) != int(jwt['id']):
                response = {"message": "Does Not Have Access", "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update user
        res, user = USER_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/user/change/<_id>", methods=["PUT"])
@jwt_check()
def change(_id):
    ''' Doc: function change'''
    try:
        # get jwt payload
        jwt = HELPER.read_jwt()
        if jwt['role_id'] != 1:
            if int(_id) != int(jwt['id']):
                response = {"message": "Does Not Have Access", "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        # prepare json data
        json_request = request.get_json(silent=True)

        # update user
        res, user, msg = USER_CONTROLLER.change(_id, json_request)

        # response
        if res:
            # success response
            response = {"message": msg, "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": msg, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/user/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # get jwt payload
        jwt = HELPER.read_jwt()
        if jwt['role_id'] != 1:
            response = {"message": "Does Not Have Access", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        # filter data
        where = {'id': _id}

        # delete user
        res = USER_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500
