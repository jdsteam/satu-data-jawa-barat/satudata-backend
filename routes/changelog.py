import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import ChangelogController
from settings import configuration as conf
from exceptions import BadRequest

BP = Blueprint('changelog', __name__)
CHANGELOG_CONTROLLER = ChangelogController()

@BP.route("/changelog", methods=["GET"])
def get_all():
    try:
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:asc')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        changelogs = CHANGELOG_CONTROLLER.get_changelogs(where, search, sort, limit, skip, count)
        response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": changelogs["data"], "metadata": changelogs["metadata"]}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except BadRequest as e:
        response = {"message": e.message, "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400
    except Exception as err:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/changelog/<_id>", methods=["GET"])
def get_by_id(_id):
    try:
        changelog = CHANGELOG_CONTROLLER.get_changelog(_id)
        if changelog:
            response = {"message": "Get detail data successfull", "error": 0, "data": changelog}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except BadRequest as e:
        response = {"message": e.message, "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400
    except Exception as err:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/changelog", methods=["POST"])
@jwt_check()
def create():
    try:
        json_request = request.get_json(silent=True)
        changelog = CHANGELOG_CONTROLLER.create_changelog(**json_request)
        if changelog:
            response = {"message": "Create data successfull", "error": 0, "data": changelog}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except BadRequest as e:
        response = {"message": e.message, "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400
    except Exception as err:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/changelog/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    try:
        json_request = request.get_json(silent=True)
        changelog = CHANGELOG_CONTROLLER.update_changelog(_id, json_request)
        if changelog:
            response = {"message": "Update data successfull", "error": 0, "data": changelog}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except BadRequest as e:
        response = {"message": e.message, "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400
    except Exception as err:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/changelog/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    try:
        permanent = request.args.get("permanent", False)
        changelog = CHANGELOG_CONTROLLER.delete_changelog(_id, permanent)
        if changelog:
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except BadRequest as e:
        response = {"message": e.message, "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 400
    except Exception as err:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500