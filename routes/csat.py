''' Doc: route history download'''
import json
from flask import request, Blueprint, Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import CsatController
from settings import configuration as conf

BP = Blueprint('csat', __name__)
CSAT_CONTROLLER = CsatController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable


@BP.route("/csat", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'csat.id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, csat = CSAT_CONTROLLER.get_count(where, search, start_date, end_date)
        else:
            # call function get
            res, csat = CSAT_CONTROLLER.get_all(where, search, sort, limit, skip, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, csat, message = CSAT_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat", methods=["POST"])
# @jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert history draft
        res, csat = CSAT_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history draft
        res, csat = CSAT_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history draft
        res = CSAT_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat/info", methods=["GET"])
# @jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        csat = {}
        csat['tujuan'] = []
        csat['sektor'] = []

        tujuan = []
        tujuan.append({'nama': 'Referensi Kajian Bisnis'})
        tujuan.append({'nama': 'Referensi Pembuatan Kebijakan'})
        tujuan.append({'nama': 'Referensi Pembuatan Kurikulum/ Bahan Ajar'})
        tujuan.append({'nama': 'Referensi Tugas/ Karya Ilmiah'})
        tujuan.append({'nama': 'Referensi Pribadi'})
        tujuan.append({'nama': 'Lainnya (tulis secara spesifik)'})

        sektor = []
        sektor.append({'nama': 'Industri/bisnis'})
        sektor.append({'nama': 'Media'})
        sektor.append({'nama': 'Organisasi non profit/sosial'})
        sektor.append({'nama': 'Pemerintahan'})
        sektor.append({'nama': 'Peneliti/akademisi'})
        sektor.append({'nama': 'Lainnya (tulis secara spesifik)'})

        data_masalah = []
        data_masalah.append({'nama': 'Halaman tidak dapat diakses'})
        data_masalah.append({'nama': 'Informasi dirasa terlalu membingungkan'})
        data_masalah.append({'nama': 'Informasi yang dicari tidak dapat ditemukan'})
        data_masalah.append({'nama': 'Tidak tau jika informasi yang tersedia akurat/terbaru'})
        data_masalah.append({'nama': 'Lainnya (tulis secara spesifik)'})

        yatidak = []
        yatidak.append({'nama': 'Ya'})
        yatidak.append({'nama': 'Tidak'})

        csat['tujuan'] = tujuan
        csat['sektor'] = sektor
        csat['data_ditemukan'] = yatidak
        csat['data_masalah'] = data_masalah
        csat['mudah_didapatkan'] = yatidak

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat/search_email/<email>", methods=["GET"])
# @jwt_check()
def get_search_email(email):
    ''' Doc: function get search email'''
    try:
        res, csat, message = CSAT_CONTROLLER.get_search_email(
            email)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/csat/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        download = request.args.get("download", '')
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, csat = CSAT_CONTROLLER.get_download(start_date, end_date)
        metadata = [
            "id", "datetime", "type", "email", "category", "category_id",
            "tujuan", "sektor", "data_ditemukan", "data_masalah", "mudah_didapatkan",
            "saran", "score", "notes", "is_active", "is_deleted", "category_name"
        ]

        # response
        if res:
            if download.lower() == 'csv':
                filename = HELPER.convert_csv('static/download/csat.csv', csat, metadata)
                return send_file(
                    '../' + filename,
                    mimetype='application/x-csv',
                    attachment_filename='csat.csv',
                    as_attachment=True
                )
            elif download.lower() == 'xls':
                filename = HELPER.convert_xls('static/download/csat.xlsx', csat, metadata)
                return send_file(
                    '../' + filename,
                    mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    attachment_filename='csat.xlsx',
                    as_attachment=True
                )
            else:
                # success response
                response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
