''' Doc: route infographic'''
import json
from flask import request, Blueprint, Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from models import InfographicModel
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers import SearchController
from controllers import HistoryController
from controllers import HighlightController
import math
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Image
from controllers import InfographicController
from settings import configuration as conf
from helpers.autocorecction import AutoCorecction

BP = Blueprint('infographic', __name__)
INFOGRAPHIC_CONTROLLER = InfographicController()
HISTORY_CONTROLLER = HistoryController()
SEARCH_CONTROLLER = SearchController()
HIGHLIGHT_CONTROLLER = HighlightController()
HELPER = Helper()
FORMAT_DATETIME = "%Y-%m-%d %H:%M:%S"
# pylint: disable=broad-except, line-too-long, unused-variable, protected-access

@BP.route("/infographic", methods=["GET"])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        suggestion = request.args.get("suggestion", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        correction_data = {}
        correction_data['status'] = False
        correction_data['suggestion_text'] = ""
        correction_data['original_text'] = search
        if search and suggestion == "on":
            status, data_corecction = AutoCorecction.infografic(search)
            correction_data['status'] = status
            correction_data['suggestion_text'] = data_corecction
            correction_data['original_text'] = search
            search = data_corecction
        else:
            search = search

        if count:
            # call function get
            res, infographic = INFOGRAPHIC_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, infographic = INFOGRAPHIC_CONTROLLER.get_all(where, search, sort, limit, skip)

        # if search:
        #     try:
        #         jwt = HELPER.read_jwt()
        #         payload = {}
        #         if jwt:
        #             payload['user_id'] = jwt['id']
        #         else:
        #             payload['user_id'] = '0'
        #         payload['search'] = search
        #         res2, data2 = SEARCH_CONTROLLER.create(payload)
        #     except Exception as err:
        #         pass

        meta = {}
        res_count, count = INFOGRAPHIC_CONTROLLER.get_count(where, search)
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": infographic, "meta": meta,
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text'],
                "date_now": HELPER.local_date_server()
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {},
                "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
                "original_text" : correction_data['original_text']
            }
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {},
            "suggestion": correction_data['status'], "suggestion_text": correction_data['suggestion_text'],
            "original_text" : correction_data['original_text']
        }
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        where = request.args.get("where", "{}")

        where = where.replace("'", "\"")
        where = json.loads(where)

        res, infographic, message = INFOGRAPHIC_CONTROLLER.get_by_id(_id, where)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": infographic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/counter/<_id>", methods=["GET"])
# @jwt_check()
def counter(_id):
    ''' Doc: function counter'''
    # category
    category = request.args.get("category", "")

    # handle id=title, integer or string
    try:
        temp = int(_id)
        _id = temp
    except Exception as err:
        temp = _id
        result = db.session.query(InfographicModel.id)
        result = result.filter(InfographicModel.title == temp)
        result = result.all()
        if result:
            result = result[0]
            _id = result[0]
        else:
            _id = 0

    try:
        res, infographic, message = INFOGRAPHIC_CONTROLLER.get_by_id(_id, {})

        # category counter
        try:
            infographic, payload = INFOGRAPHIC_CONTROLLER.switch(category, infographic)
            where = {'id': int(_id)}
            res, data1 = INFOGRAPHIC_CONTROLLER.update(where, payload)
        except Exception as err:
            pass

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": infographic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert infographic
        res, infographic = INFOGRAPHIC_CONTROLLER.create(json_request)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'infographic'
            json_history['type_id'] = infographic['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'create'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": infographic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update dataset
        res, infographic = INFOGRAPHIC_CONTROLLER.update(where, json_request)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'infographic'
            json_history['type_id'] = infographic['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'update'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # trigger update highlight
        if 'is_active' in json_request or 'is_deleted' in json_request:
            where_hi = {}
            where_hi['category'] = 'infografik'
            where_hi['category_id'] = infographic['id']
            res_hi, highlight = HIGHLIGHT_CONTROLLER.get_all(where_hi, "", ['id', 'asc'], 1, 0)
            if res_hi:
                where_hi['id'] = highlight[0]['id']
                json_request_hi = {}
                json_request_hi['is_active'] = infographic['is_deleted']
                # json_request_hi['is_deleted'] = infographic['is_deleted']
                res_hi, highlight = HIGHLIGHT_CONTROLLER.update(where_hi, json_request_hi)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": infographic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete infographic
        res = INFOGRAPHIC_CONTROLLER.delete(where)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'infographic'
            json_history['type_id'] = _id
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'delete'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/owner", methods=["GET"])
# @jwt_check()
def get_owner():
    ''' Doc: function get owner'''
    try:
        # call function get
        res, infographic = INFOGRAPHIC_CONTROLLER.get_owner_all()

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": infographic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/source", methods=["GET"])
# @jwt_check()
def get_source():
    ''' Doc: function get source'''
    try:
        # call function get
        res, infographic = INFOGRAPHIC_CONTROLLER.get_source_all()

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": infographic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/all", methods=["GET"])
@jwt_check()
def get_alls():
    ''' Doc: function get alls'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 1000)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        res, infographic = INFOGRAPHIC_CONTROLLER.get_alls(where, search, sort, limit, skip)
        meta = {}

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": infographic, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic/download/<_id>", methods=["GET"])
# @jwt_check()
def download(_id):
    ''' Doc: function get download'''
    # category
    category = request.args.get("category", "")

    # handle id=title, integer or string
    try:
        temp = int(_id)
        _id = temp
    except Exception as err:
        temp = _id
        result = db.session.query(InfographicModel.id)
        result = result.filter(InfographicModel.title == temp)
        result = result.all()
        if result:
            result = result[0]
            _id = result[0]
        else:
            _id = 0

    try:
        # get data
        res, infographic, message = INFOGRAPHIC_CONTROLLER.get_by_id(_id, {})

        # response
        if res:
            if category == 'pdf':
                elements = []
                filename = 'Infografis ' + infographic['name'] + '.pdf'

                for row in infographic['infographic_detail']:
                    fl_ = open(row['image'], 'rb')
                    image = Image(fl_)
                    image._restrictSize(14 * inch, 7 * inch)
                    elements.append(image)

                doc = SimpleDocTemplate(filename, pagesize=letter)
                doc.build(elements)
                return send_file('../'+filename, mimetype='application/pdf', attachment_filename=filename, as_attachment=True)

            else:
                # success response
                response = {"message": message, "error": 0, "data": infographic}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
