''' Doc: route feedback'''
import json
from flask import request, Blueprint, Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import FeedbackController
from controllers import FeedbackMasalahController
from settings import configuration as conf

BP = Blueprint('feedback', __name__)
FEEDBACK_CONTROLLER = FeedbackController()
FEEDBACK_MASALAH_CONTROLLER = FeedbackMasalahController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable


@BP.route("/feedback", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, feedback = FEEDBACK_CONTROLLER.get_count(
                where, search, start_date, end_date)
        else:
            # call function get
            res, feedback = FEEDBACK_CONTROLLER.get_all(
                where, search, sort, limit, skip, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, feedback, message = FEEDBACK_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback", methods=["POST"])
# @jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if 'masalah' in json_request:
            masalahs = json_request['masalah']
            json_request.pop('masalah', None)

        # insert history draft
        res, feedback = FEEDBACK_CONTROLLER.create(json_request)

        # response
        if res:

            # list feedback masalah
            feedback[0]['masalah'] = masalahs

            # list masalah
            if isinstance(masalahs, list):
                for msl in masalahs:
                    json_masalah = {}
                    json_masalah['feedback_id'] = feedback[0]['id']
                    json_masalah['masalah'] = msl
                    res, feedback_masalah = FEEDBACK_MASALAH_CONTROLLER.create(
                        json_masalah)

            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history draft
        res, feedback = FEEDBACK_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history draft
        res = FEEDBACK_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/info", methods=["GET"])
# @jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        feedback = {}
        feedback['tujuan'] = []
        feedback['masalah'] = []
        feedback['sektor'] = []

        feedback['tujuan'].append(
            {'nama': 'Mencari data terbuka Pemdaprov Jawa Barat untuk kepentingan bisnis, perumusan kebijakan, atau referensi pribadi lainnya.'})
        feedback['tujuan'].append(
            {'nama': 'Mencari data terbuka Pemdaprov Jawa Barat untuk kepentingan bahan ajar/kurikum/tugas belajar.'})
        feedback['tujuan'].append(
            {'nama': 'Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu.'})
        feedback['tujuan'].append(
            {'nama': 'Mempelajari lebih lanjut terkait transparansi data dan informasi yang dimiliki oleh Pemdaprov Jawa Barat.'})

        feedback['masalah'].append({'nama': 'Halaman tidak dapat diakses'})
        feedback['masalah'].append(
            {'nama': 'Informasi dirasa terlalu membingungkan'})
        feedback['masalah'].append(
            {'nama': 'Informasi yang dicari tidak dapat ditemukan'})
        feedback['masalah'].append(
            {'nama': 'Tidak tau jika informasi yang tersedia akurat atau terbaru'})

        feedback['sektor'].append({'nama': 'Peneliti/Akademisi'})
        feedback['sektor'].append({'nama': 'Pemerintahan'})
        feedback['sektor'].append({'nama': 'Media'})
        feedback['sektor'].append({'nama': 'Industri/Bisnis'})
        feedback['sektor'].append({'nama': 'Organisasi Non Profit/Sosial'})

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_total", methods=["GET"])
@jwt_check()
def get_chart_total():
    ''' Doc: function get chart total'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_total(
            start_date, end_date)

        # response
        if res:
            # perhitungan
            result = {}
            result['total'] = feedback

            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_score", methods=["GET"])
@jwt_check()
def get_chart_score():
    ''' Doc: function get chart score'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_score(
            start_date, end_date)

        # response
        if res:
            # perhitungan
            result = {}
            result['detail'] = feedback
            result['count'] = 0
            result['max'] = 0
            result['total'] = 0
            result['average'] = 0

            # perhitungan
            for fb_ in feedback:
                result['count'] += fb_['count']
                result['total'] += fb_['score'] * fb_['count']

            result['max'] = result['count'] * 4
            result['average'] = round(
                (result['total'] / result['max']) * 100, 2)

            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_tujuan", methods=["GET"])
@jwt_check()
def get_chart_tujuan():
    ''' Doc: function get chart tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_tujuan(
            start_date, end_date)

        feedbacks = []
        feedbacks.append(
            {"count": 0, "tujuan": "Mencari data terbuka Pemdaprov Jawa Barat untuk kepentingan bisnis, perumusan kebijakan, atau referensi pribadi lainnya."})
        feedbacks.append(
            {"count": 0, "tujuan": "Mencari data terbuka Pemdaprov Jawa Barat untuk kepentingan bahan ajar/kurikum/tugas belajar."})
        feedbacks.append(
            {"count": 0, "tujuan": "Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu."})
        feedbacks.append(
            {"count": 0, "tujuan": "Mempelajari lebih lanjut terkait transparansi data dan informasi yang dimiliki oleh Pemdaprov Jawa Barat."})
        feedbacks.append({"count": 0, "tujuan": "Lainnya"})

        for fb_ in feedback:
            if fb_['tujuan'] == "Mencari data terbuka Pemdaprov Jawa Barat untuk kepentingan bisnis, perumusan kebijakan, atau referensi pribadi lainnya.":
                feedbacks[0]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mencari data terbuka Pemdaprov Jawa Barat untuk kepentingan bahan ajar/kurikum/tugas belajar.":
                feedbacks[1]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu.":
                feedbacks[2]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mempelajari lebih lanjut terkait transparansi data dan informasi yang dimiliki oleh Pemdaprov Jawa Barat.":
                feedbacks[3]['count'] = fb_['count']
            else:
                feedbacks[4]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedbacks}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_tujuan_tercapai", methods=["GET"])
@jwt_check()
def get_chart_tujuan_tercapai():
    ''' Doc: function get chart tujuan tercapai'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_tujuan_tercapai(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_tujuan_mudah_ditemukan", methods=["GET"])
@jwt_check()
def get_chart_tujuan_mudah_ditemukan():
    ''' Doc: function get chart tujuan mudah ditemukan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_tujuan_mudah_ditemukan(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_sektor", methods=["GET"])
@jwt_check()
def get_chart_sektor():
    ''' Doc: function get chart sektor'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_sektor(
            start_date, end_date)

        feedbacks = []
        feedbacks.append({"count": 0, "sektor": "Peneliti/Akademisi"})
        feedbacks.append({"count": 0, "sektor": "Pemerintahan"})
        feedbacks.append({"count": 0, "sektor": "Media"})
        feedbacks.append({"count": 0, "sektor": "Industri/Bisnis"})
        feedbacks.append(
            {"count": 0, "sektor": "Organisasi Non Profit/Sosial"})
        feedbacks.append({"count": 0, "sektor": "Lainnya"})

        for fb_ in feedback:
            if fb_['sektor'] == "Peneliti/Akademisi":
                feedbacks[0]['count'] = fb_['count']
            elif fb_['sektor'] == "Pemerintahan":
                feedbacks[1]['count'] = fb_['count']
            elif fb_['sektor'] == "Media":
                feedbacks[2]['count'] = fb_['count']
            elif fb_['sektor'] == "Industri/Bisnis":
                feedbacks[3]['count'] = fb_['count']
            elif fb_['sektor'] == "Organisasi Non Profit/Sosial":
                feedbacks[4]['count'] = fb_['count']
            else:
                feedbacks[5]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedbacks}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/chart_harian", methods=["GET"])
@jwt_check()
def get_chart_harian():
    ''' Doc: function get chart harian'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_chart_harian(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_CONTROLLER.get_download(start_date, end_date)
        metadata = ["Id", "Datetime", "Score", "Tujuan", "Tujuan Tercapai", "Tujuan Mudah Ditemukan", "Sektor", "Saran", "Email",
                    "Halaman tidak dapat diakses", "Informasi dirasa terlalu membingungkan",
                    "Informasi yang dicari tidak dapat ditemukan", "Tidak tau jika informasi yang tersedia akurat atau terbaru",
                    "Masalah Lainnya", "Sumber Halaman", "Mekanisme", "is_active", "is_deleted"]

        # response
        if res:
            # success response
            filename = HELPER.convert_xls(
                'static/download/feedback_public.xlsx', feedback, metadata)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='feedback_public.xlsx',
                as_attachment=True
            )
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
