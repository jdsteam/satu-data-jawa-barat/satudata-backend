''' Doc: route history request'''
import json
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers import UserController
from controllers import HistoryRequestController
from controllers import NotificationController
from models import RequestModel
from settings import configuration as conf

BP = Blueprint('history_request', __name__)
USER_CONTROLLER = UserController()
HISTORY_REQUEST_CONTROLLER = HistoryRequestController()
NOTIFICATION_CONTROLLER = NotificationController()
HELPER = Helper()
MESSAGE_NO_TIKET = "Nomor tiket : "
MESSAGE_PERMOHONAN_DATA = "Permohonan data terkait <strong>"
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/history_request", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, history_request = HISTORY_REQUEST_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, history_request = HISTORY_REQUEST_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, history_request, message = HISTORY_REQUEST_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert history request
        res, history_request = HISTORY_REQUEST_CONTROLLER.create(json_request)

        # insert to notification
        if res:
            jwt = HELPER.read_jwt()
            if jwt['role']['name'] == 'walidata':
                # get user
                data_request = db.session.query(RequestModel)
                data_request = data_request.get(history_request['request_id'])

                if data_request:
                    data_request = data_request.__dict__
                    user_id = data_request['user_id']

                    json_notification = {}
                    json_notification['feature'] = 3
                    json_notification['type'] = 'request'
                    json_notification['type_id'] = history_request['request_id']
                    json_notification['sender'] = jwt['id']
                    json_notification['receiver'] = data_request['user_id']
                    json_notification['is_read'] = False
                    json_notification['cdate'] = HELPER.local_date_server()
                    json_notification['mdate'] = HELPER.local_date_server()

                    titles = '"{}"'.format(str(data_request['title']))

                    # prepare notif
                    result, notification = HISTORY_REQUEST_CONTROLLER.prepare_notif(history_request, json_notification, titles)

                    # update notif
                    HISTORY_REQUEST_CONTROLLER.send_notif(user_id, notification, request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history request
        res, history_request = HISTORY_REQUEST_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history request
        res = HISTORY_REQUEST_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
