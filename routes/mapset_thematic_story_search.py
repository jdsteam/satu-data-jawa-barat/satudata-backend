''' Doc: route mapset visualization'''
import json
from flask import Blueprint, request, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from settings import configuration as conf

from controllers import MapsetThematicStorySearchController

BP = Blueprint('mapset_thematic_story_search', __name__)
MAPSET_THEMATIC_STORY_SEARCH_CONTROLLER = MapsetThematicStorySearchController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route('/mapset_thematic_story/search', methods=['GET'])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get('where', '{}')
        sort = request.args.get('sort', 'mdate:DESC')
        limit = request.args.get('limit', 100)
        skip = request.args.get('skip', 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        if where:
            where = where.replace("'", '"')
            where = json.loads(where)
        else:
            where = {}

        if count:
            # call function get
            res, mapset_thematic_story_search = MAPSET_THEMATIC_STORY_SEARCH_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, mapset_thematic_story_search = MAPSET_THEMATIC_STORY_SEARCH_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {'message': conf.MESSAGE_DATA_SUCCESS, 'error': 0, 'data': mapset_thematic_story_search}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

