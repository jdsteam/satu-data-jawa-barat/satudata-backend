''' Doc: route app '''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import StrukturOrganisasiController
from controllers import SkpdController
import pandas as pd

BP = Blueprint('struktur_organisasi',  __name__)
STRUKTUR_ORGANISASI_CONTROLLER = StrukturOrganisasiController()
SKPD_CONTROLLER = SkpdController()
# pylint: disable=broad-except, unused-variable

@BP.route("/struktur_organisasi", methods=["GET"])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, struktur_organisasi = STRUKTUR_ORGANISASI_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, struktur_organisasi = STRUKTUR_ORGANISASI_CONTROLLER.get_all(where, search, sort, limit, skip)

            # struktur_organisasi2 = []
            # res2, skpd = SKPD_CONTROLLER.get_all({}, "", ['nama_skpd', 'ASC'], 100, 0)

            # if 'level' in where:
            #     if where['level'] == '0':
            #         for sk in skpd:
            #             struktur_organisasi2.append({
            #                 "eselon_id": None,
            #                 "eselon_nm": None,
            #                 "gol_id_minimum": None,
            #                 "id": str(sk['id']),
            #                 "jabatan_id": None,
            #                 "jabatan_nama": "KEPALA " + sk['nama_skpd'].upper(),
            #                 "kode_skpd": sk['kode_skpd'],
            #                 "level": "0",
            #                 "lv1_unit_kerja_id": None,
            #                 "lv1_unit_kerja_nama": None,
            #                 "lv2_unit_kerja_id": None,
            #                 "lv2_unit_kerja_nama": None,
            #                 "lv3_unit_kerja_id": None,
            #                 "lv3_unit_kerja_nama": None,
            #                 "lv4_unit_kerja_id": None,
            #                 "lv4_unit_kerja_nama": None,
            #                 "parent_id": None,
            #                 "satuan_kerja_id": str(sk['id']),
            #                 "satuan_kerja_nama": sk['nama_skpd']
            #             })

            # df_1 = pd.DataFrame(struktur_organisasi)
            # df_2 = pd.DataFrame(struktur_organisasi2)
            # df_3 = (pd.concat([df_1, df_2], ignore_index=True, sort=False).drop_duplicates(['satuan_kerja_id'], keep='first'))
            # struktur_organisasi = df_3.to_dict("records")

        # response
        if res:
            # success response
            response = {"message": "Get data successfull", "error": 0, "data": struktur_organisasi}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/struktur_organisasi/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, struktur_organisasi = STRUKTUR_ORGANISASI_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": struktur_organisasi}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500
