''' Doc: route infographic detail'''
import json
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import SearchController
from controllers import HistoryController
from controllers import InfographicDetailController
from settings import configuration as conf

BP = Blueprint('infographic_detail', __name__)
INFOGRAPHIC_DETAIL_CONTROLLER = InfographicDetailController()
HISTORY_CONTROLLER = HistoryController()
SEARCH_CONTROLLER = SearchController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/infographic_detail", methods=["GET"])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, infographic_detail = INFOGRAPHIC_DETAIL_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, infographic_detail = INFOGRAPHIC_DETAIL_CONTROLLER.get_all(where, search, sort, limit, skip)

        if search:
            try:
                jwt = HELPER.read_jwt()
                payload = {}
                payload['search'] = search
                payload['user_id'] = jwt['id']
                res2, data2 = SEARCH_CONTROLLER.create(payload)
            except Exception as err:
                pass

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": infographic_detail}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/infographic_detail/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, infographic_detail, message = INFOGRAPHIC_DETAIL_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": infographic_detail}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/infographic_detail", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert infographic_detail
        res, infographic_detail = INFOGRAPHIC_DETAIL_CONTROLLER.create(json_request)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'infographic_detail'
            json_history['type_id'] = infographic_detail['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'create'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": infographic_detail}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic_detail/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update dataset
        res, infographic_detail = INFOGRAPHIC_DETAIL_CONTROLLER.update(where, json_request)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'infographic_detail'
            json_history['type_id'] = infographic_detail['id']
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'update'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": infographic_detail}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/infographic_detail/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete infographic_detail
        res = INFOGRAPHIC_DETAIL_CONTROLLER.delete(where)

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history['type'] = 'infographic_detail'
            json_history['type_id'] = _id
            json_history['user_id'] = jwt['id']
            json_history['datetime'] = HELPER.local_date_server()
            json_history['category'] = 'delete'
            result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
