''' Doc: route dataset tag'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DatasetTagController
from settings import configuration as conf

BP = Blueprint('dataset_tag', __name__)
DATASET_TAG_CONTROLLER = DatasetTagController()
# pylint: disable=broad-except


@BP.route("/dataset_tag", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'dataset_tag_id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        distinct = request.args.get("distinct", False)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        admin = request.args.get("admin", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        kode_skpd = request.args.get("kode_skpd")
        if kode_skpd:
            if count:
                # call function get
                res, dataset_tag = DATASET_TAG_CONTROLLER.get_count(
                    where, search, distinct, admin)
            else:
                # call function get
                res, dataset_tag = DATASET_TAG_CONTROLLER.get_distinct_skpd(
                    kode_skpd, where)
        else:
            if count:
                # call function get
                res, dataset_tag = DATASET_TAG_CONTROLLER.get_count(
                    where, search, distinct, admin)
            else:
                # call function get
                res, dataset_tag = DATASET_TAG_CONTROLLER.get_all(
                    where, search, sort, limit, skip, distinct, admin)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": dataset_tag}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''

    is_int = True
    key_id = 0
    key_tag = ''
    try:
        temp = int(_id)
        key_id = temp
        is_int = True

    except Exception:
        key_tag = _id
        is_int = False

    try:
        # call function get by id
        if is_int:
            res, dataset_tag = DATASET_TAG_CONTROLLER.get_by_id(key_id)
        else:
            res_count, res_data = DATASET_TAG_CONTROLLER.get_count(
                {'tag': key_tag}, '', False, False)
            res = True
            dataset_tag = {
                'tag': key_tag,
                'count': res_data['count']
            }

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull",
                        "error": 0, "data": dataset_tag}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # insert dataset_tag
            res, dataset_tag = DATASET_TAG_CONTROLLER.create_multiple(
                json_request)
        else:
            # insert dataset_tag
            res, dataset_tag = DATASET_TAG_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": dataset_tag}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag", methods=["PUT"])
@jwt_check()
def update_by_dataset():
    ''' Doc: function update by dataset'''
    try:
        # filter data
        dataset_id = request.args.get("dataset_id", 0)

        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # update dataset_tag
            res, dataset_tag = DATASET_TAG_CONTROLLER.update_multiple(
                dataset_id, json_request)
        else:
            # update dataset_tag
            pass

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": dataset_tag}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update dataset_tag
        res, dataset_tag = DATASET_TAG_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": dataset_tag}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete dataset_tag
        res = DATASET_TAG_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag/delete", methods=["POST"])
@jwt_check()
def delete_tag_delete():
    ''' Doc: function delete'''
    try:
        # filter data
        where = request.get_json(silent=True)

        # delete dataset_tag
        res = DATASET_TAG_CONTROLLER.delete_tag(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dataset_tag/edit", methods=["POST"])
@jwt_check()
def delete_tag_update():
    ''' Doc: function update'''
    try:
        # filter data
        where = request.get_json(silent=True)

        if 'old_tag' in where and 'new_tag' in where:

            # delete dataset_tag
            res = DATASET_TAG_CONTROLLER.update_tag(where)

            # response
            if res:
                # success response
                response = {"message": "Update data successfull",
                            "error": 0, "data": {'tag': where['old_tag']}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
            else:
                # success response but no data
                response = {"message": "Update data failed",
                            "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

        else:
            # success response but no data
            response = {"message": "'old_tag' & 'new_tag' not found",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
