''' Doc: route mapset story'''
import json
from flask import Blueprint, request, Response
from controllers import MapsetStoryController
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from settings import configuration as conf

BP = Blueprint('mapset_story', __name__)
MAPSET_STORY_CONTROLLER = MapsetStoryController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route('/mapset_story', methods=['GET'])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get('where', '{}')
        sort = request.args.get('sort', 'mdate:DESC')
        limit = request.args.get('limit', 100)
        skip = request.args.get('skip', 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", '"')
        where = json.loads(where)

        if count:
            # call function get
            res, mapset_story = MAPSET_STORY_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, mapset_story = MAPSET_STORY_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {'message': conf.MESSAGE_DATA_SUCCESS, 'error': 0, 'data': mapset_story}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_story/<_id>', methods=['GET'])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, mapset_story = MAPSET_STORY_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {'message': 'Get detail data successfull', 'error': 0, 'data': mapset_story}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_story', methods=['POST'])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert mapset_story
        res, mapset_story = MAPSET_STORY_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {'message': 'Create data successfull', 'error': 0, 'data': mapset_story}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Create data failed, ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_story/<_id>', methods=['PUT'])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update mapset_story
        res, mapset_story = MAPSET_STORY_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {'message': 'Update data successfull', 'error': 0, 'data': mapset_story}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Update data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_story/<_id>', methods=['DELETE'])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete mapset_story
        res = MAPSET_STORY_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {'message': 'Delete data successfull', 'error': 0, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Delete data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/mapset_story/counter/<_id>", methods=["GET"])
def counter(_id):
    ''' Doc: function counter'''

    # insert counter
    status, result = MAPSET_STORY_CONTROLLER.counter(_id)

    # response
    if status:
        # success response
        response = {"message": "Create counter successfull", "error": 0, "data": result}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    else:
        # success response but no data
        response = {"message": "Create counter failed ", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
