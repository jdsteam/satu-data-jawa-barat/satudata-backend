''' Doc: route visualization access'''
import json
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import VisualizationAccessController
from settings import configuration as conf

BP = Blueprint('visualization_access', __name__)
VISUALIZATION_ACCESS_CONTROLLER = VisualizationAccessController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/visualization_access", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": visualization_access}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, visualization, message = VISUALIZATION_ACCESS_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": visualization}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert visualization access
        VISUALIZATION_ACCESS_CONTROLLER.insert_walidata(json_request['visualization_id'])
        res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": visualization_access}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        where = {'visualization_id':_id}
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert delete visualization access
        VISUALIZATION_ACCESS_CONTROLLER.delete(where)
        res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": visualization_access}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access/access", methods=["POST"])
@jwt_check()
def create_multiple():
    ''' Doc: function create multiple'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert visualization access
        if isinstance(json_request, list):
            # insert visualization access
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.create_multiple(json_request)
        else:
            # insert visualization access
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": visualization_access}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access/access", methods=["GET"])
@jwt_check()
def get_access():
    ''' Doc: function get access'''
    try:
        visualization_id = request.args.get("visualization_id", "")
        if visualization_id:
            # call function get
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.get_skpd(visualization_id)
        else:
            # success response but no data
            response = {"message": "Parameter visualization not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": visualization_access}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access/access/<_id>", methods=["PUT"])
@jwt_check()
def update_multiple(_id):
    ''' Doc: function update multiple'''
    try:
        where = {'visualization_id':_id}
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert visualization access
        if isinstance(json_request, list):
            # delete insert visualization access
            VISUALIZATION_ACCESS_CONTROLLER.delete(where)
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.create_multiple(json_request)
        else:
            # delete insert visualization access
            VISUALIZATION_ACCESS_CONTROLLER.delete(where)
            res, visualization_access = VISUALIZATION_ACCESS_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": visualization_access}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/visualization_access/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete visualization
        res = VISUALIZATION_ACCESS_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
