''' Doc: route mapset thematic'''
import json
from flask import Blueprint, request, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from settings import configuration as conf

from controllers import MapsetThematicController

BP = Blueprint('mapset_thematic', __name__)
MAPSET_THEMATIC_CONTROLLER = MapsetThematicController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route('/mapset_thematic', methods=['GET'])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get('where', '{}')
        sort = request.args.get('sort', 'mdate:DESC')
        limit = request.args.get('limit', 100)
        skip = request.args.get('skip', 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        if where:
            where = where.replace("'", '"')
            where = json.loads(where)
        else:
            where = {}

        if count:
            # call function get
            res, mapset_thematic = MAPSET_THEMATIC_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, mapset_thematic = MAPSET_THEMATIC_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {'message': conf.MESSAGE_DATA_SUCCESS, 'error': 0, 'data': mapset_thematic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_thematic/<_id>', methods=['GET'])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, mapset_thematic = MAPSET_THEMATIC_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {'message': 'Get detail data successfull', 'error': 0, 'data': mapset_thematic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_thematic', methods=['POST'])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # temporary variable
        temp_source = []
        temp_metadata = []

        if 'metadata' in json_request:
            temp_metadata = json_request['metadata']
            del json_request['metadata']
        if 'source' in json_request:
            temp_source = json_request['source']
            del json_request['source']

        # insert mapset_thematic
        res, mapset_thematic = MAPSET_THEMATIC_CONTROLLER.create(json_request)

        # create mapset thematic source
        MAPSET_THEMATIC_CONTROLLER.route_create_mapset_thematic_source(
            temp_source, mapset_thematic)

        # create mapset thematic source
        MAPSET_THEMATIC_CONTROLLER.route_create_mapset_thematic_metadata(
            temp_metadata, mapset_thematic)

        # response
        if res:
            # success response
            response = {'message': 'Create data successfull', 'error': 0, 'data': mapset_thematic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Create data failed, ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_thematic/<_id>', methods=['PUT'])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # temporary variable
        temp_metadata = []
        temp_source = []

        if 'metadata' in json_request:
            temp_metadata = json_request['metadata']
            del json_request['metadata']

        if 'source' in json_request:
            temp_source = json_request['source']
            del json_request['source']

        # update mapset_thematic
        res, mapset_thematic = MAPSET_THEMATIC_CONTROLLER.update(where, json_request)

        # update mapset thematic source
        MAPSET_THEMATIC_CONTROLLER.route_update_mapset_thematic_source(
            temp_source, mapset_thematic)

        # update mapset thematic metadata
        MAPSET_THEMATIC_CONTROLLER.route_update_mapset_thematic_metadata(
            temp_metadata, mapset_thematic)

        # response
        if res:
            # success response
            response = {'message': 'Update data successfull', 'error': 0, 'data': mapset_thematic}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Update data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset_thematic/<_id>', methods=['DELETE'])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete mapset_thematic
        res = MAPSET_THEMATIC_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {'message': 'Delete data successfull', 'error': 0, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Delete data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/mapset_thematic/counter/<_id>", methods=["GET"])
def counter(_id):
    ''' Doc: function counter'''

    # insert counter
    status, result = MAPSET_THEMATIC_CONTROLLER.counter(_id)

    # response
    if status:
        # success response
        response = {"message": "Create counter successfull", "error": 0, "data": result}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    else:
        # success response but no data
        response = {"message": "Create counter failed ", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


