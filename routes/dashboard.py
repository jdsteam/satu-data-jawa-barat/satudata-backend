''' Doc: route dashboard'''
import json
from flask import Blueprint, request, Response
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DashboardController
from settings import configuration as conf

HELPER = Helper()
BP = Blueprint('dashboard', __name__)
DASHBOARD_CONTROLLER = DashboardController()
# pylint: disable=broad-except

@BP.route("/dashboard/stat_total", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        res, dashboard = DASHBOARD_CONTROLLER.get_all(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": dashboard}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/top_opd", methods=["GET"])
@jwt_check()
def get_top_opd():
    ''' Doc: function get top opd '''
    try:
        # filter data
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")

        res, dashboard = DASHBOARD_CONTROLLER.get_top_opd(start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": dashboard}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/top_dataset", methods=["GET"])
@jwt_check()
def get_top_dataset():
    ''' Doc: function get top dataset'''
    try:
        # filter data
        start_date = request.args.get("start_date")
        end_date = request.args.get("end_date")
        sort = request.args.get("sort", 'count_rating:DESC')

        sort = sort.split(':')
        res, dataset = DASHBOARD_CONTROLLER.get_top_dataset(sort, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": dataset}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
