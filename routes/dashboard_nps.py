''' Doc: route dashboard'''
import json
from flask import Blueprint, request, Response
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DashboardNpsController
from settings import configuration as conf
from datetime import datetime, timedelta

BP = Blueprint('dashboard_feedback', __name__)
DASHBOARD_NPS = DashboardNpsController()

HELPER = Helper()
this_month = datetime.today().strftime("%m")
metadata_parameter = {
    "filter_type": ["month", "quarter"],
    "filter_value": [1,2,3,4,5,6,7,8,9,10,11,12],
    "formula_nps": "promoters_percent - dectators_percent"
}

@BP.route("/dashboard/nps/score", methods=["GET"])
@jwt_check()
def get_nps_score():
    ''' Doc: function get nps score'''
    try:
        # filter data
        filter_type = request.args.get("filter_type", 'month')
        filter_value = request.args.get("filter_value", this_month)

        if filter_type == 'month' or 'quarter':
            res, score = DASHBOARD_NPS.get_nps_score(filter_type, filter_value)
        else:
            res = False
            score = {}

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": score, "metadata_parameter": metadata_parameter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}, "metadata_parameter": metadata_parameter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}, "metadata_parameter": metadata_parameter}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/nps/distribution", methods=["GET"])
@jwt_check()
def get_nps_distribution():
    ''' Doc: function get nps distribution'''
    try:
        # filter data
        filter_type = request.args.get("filter_type", 'month')
        filter_value = request.args.get("filter_value", this_month)

        if filter_type == 'month' or 'quarter':
            res, distribution = DASHBOARD_NPS.get_nps_distribution(filter_type, filter_value)
        else:
            res = False
            distribution = {}

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": distribution, "metadata_parameter": metadata_parameter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}, "metadata_parameter": metadata_parameter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}, "metadata_parameter": metadata_parameter}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/nps/trend", methods=["GET"])
@jwt_check()
def get_nps_trend():
    ''' Doc: function get nps trend'''
    try:
        # filter data
        filter_type = request.args.get("filter_type", 'month')

        if filter_type == 'month' or 'quarter':
            res, trend = DASHBOARD_NPS.get_nps_trend(filter_type)
        else:
            res = False
            trend = {}

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": trend, "metadata_parameter": metadata_parameter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}, "metadata_parameter": metadata_parameter}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}, "metadata_parameter": metadata_parameter}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

