''' Doc: route dashboard'''
import json
from flask import Blueprint, request, Response
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DashboardFeedbackMapController
from settings import configuration as conf
from datetime import datetime, timedelta

BP = Blueprint('dashboard/feedback_map', __name__)
DASHBOARD_FEEDBACK_MAP = DashboardFeedbackMapController()

HELPER = Helper()
one_month_ago_days = 30
one_days_ago = 1
today = datetime.today().strftime("%Y-%m-%d")
get_month_ago = datetime.today() - timedelta(days=one_month_ago_days)
get_day_ago = datetime.today() - timedelta(days=one_days_ago)
one_month_ago = get_month_ago.strftime("%Y-%m-%d")
one_day_ago = get_day_ago.strftime("%Y-%m-%d")
# pylint: disable=broad-except


@BP.route("/dashboard/feedback_map/total", methods=["GET"])
@jwt_check()
def get_feedback_map_total():
    ''' Doc: function get feedback_map total'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_total(
            start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/average", methods=["GET"])
@jwt_check()
def get_feedback_map_average():
    ''' Doc: function get feedback_map average'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_day_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_average(
            start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/trend", methods=["GET"])
@jwt_check()
def get_feedback_map_trend():
    ''' Doc: function get feedback_map trend'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        group = request.args.get("group", 'day')
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_trend2(
            start_date, end_date, group)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/satisfaction", methods=["GET"])
@jwt_check()
def get_feedback_map_satisfaction():
    ''' Doc: function get feedback_map satisfaction'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        goal = request.args.get("goal", "tujuan_tercapai")
        res, satisfaction = DASHBOARD_FEEDBACK_MAP.get_feedback_satisfaction2(
            start_date, end_date, types, goal)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": satisfaction}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/csat", methods=["GET"])
@jwt_check()
def get_feedback_map_csat():
    ''' Doc: function get feedback_map csat'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, csat = DASHBOARD_FEEDBACK_MAP.get_feedback_csat2(
            start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/csat/trend", methods=["GET"])
@jwt_check()
def get_feedback_map_csat_trend():
    ''' Doc: function get feedback_map csat trend'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        group = request.args.get("group", 'day')
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_csat_trend2(
            start_date, end_date, group)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0,
                        "data": total, "metadata": {"total": "csat dalam persen"}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1,
                        "data": [], "metadata": {"total": "csat dalam persen"}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": [], "metadata": {"total": "csat dalam persen"}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/group", methods=["GET"])
@jwt_check()
def get_feedback_map_group():
    ''' Doc: function get feedback_map tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", 'tujuan')
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_group(
            start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/user/problem", methods=["GET"])
@jwt_check()
def get_feedback_map_user_problem():
    ''' Doc: function get feedback_map kendala user'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_user_problem(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/problem/total", methods=["GET"])
@jwt_check()
def get_feedback_map_problem():
    ''' Doc: function get feedback_map total kendala'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_total_problem(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/feedback_map/problem/user/total", methods=["GET"])
@jwt_check()
def get_feedback_map_total_problem_user():
    ''' Doc: function get feedback_map total kendala user'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        res, total = DASHBOARD_FEEDBACK_MAP.get_feedback_total_problem_user2(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
