''' Doc: route request public'''
import json
import datetime
from flask import request, Blueprint, Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import RequestPublicController
from controllers import HistoryRequestPublicController
from controllers import SkpdController
from controllers import UserController
from controllers import NotificationController
from controllers import SektoralController
from settings import configuration as conf

BP = Blueprint('request_public', __name__)
REQUEST_PUBLIC_CONTROLLER = RequestPublicController()
HISTORY_REQUEST_PUBLIC_CONTROLLER = HistoryRequestPublicController()
SKPD_CONTROLLER = SkpdController()
USER_CONTROLLER = UserController()
NOTIFICATION_CONTROLLER = NotificationController()
SEKTORAL_CONTROLLER = SektoralController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/request_public", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, request_public = REQUEST_PUBLIC_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, request_public = REQUEST_PUBLIC_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/request_public/count", methods=["GET"])
# @jwt_check()
def get_count():
    ''' Doc: function get count'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        search = request.args.get("search", "")

        where = where.replace("'", "\"")
        where = json.loads(where)

        # call function get
        res, request_public = REQUEST_PUBLIC_CONTROLLER.get_count(where, search)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, request_public, message = REQUEST_PUBLIC_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public", methods=["POST"])
# @jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        is_allow_recaptcha, data_allow_recaptcha = REQUEST_PUBLIC_CONTROLLER.check_recaptcha(json_request)
        if not is_allow_recaptcha:
            response = {"message": data_allow_recaptcha, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        # remove recaptcha token from request
        json_request.pop('recaptcha')

        # insert request
        res, request_public = REQUEST_PUBLIC_CONTROLLER.create(json_request)

        # insert history request
        if res:
            json_history_request = {}
            json_history_request['request_public_id'] = request_public['id']
            json_history_request['status'] = '1'
            res_hisotry, history = HISTORY_REQUEST_PUBLIC_CONTROLLER.create(json_history_request)

            request_public['history'].append(history)

        # get data again
        res2, request_public, message = REQUEST_PUBLIC_CONTROLLER.get_by_id(request_public['id'])

        # insert to notification email
        if res:
            REQUEST_PUBLIC_CONTROLLER.send_mail(request_public)

        # insert notification table
        if res:
            REQUEST_PUBLIC_CONTROLLER.insert_notification(request_public, request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        jwt = HELPER.read_jwt()
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update request
        res, request_public = REQUEST_PUBLIC_CONTROLLER.update(where, json_request)

        # update history request
        # if res:
        #     json_history_request = {}
        #     json_history_request['request_public_id'] = request_public['id']
        #     json_history_request['status'] = request_public['status']
        #     json_history_request['notes'] = request_public['notes']
        #     json_history_request['cuid'] = jwt['id']
        #     res, history = HISTORY_REQUEST_PUBLIC_CONTROLLER.create(json_history_request)

        #     request_public['history'].append(history)

        # get data again
        res2, request_public, message = REQUEST_PUBLIC_CONTROLLER.get_by_id(request_public['id'])

        # insert to notification email
        if res:
            REQUEST_PUBLIC_CONTROLLER.send_mail(request_public)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete request
        res = REQUEST_PUBLIC_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public/info", methods=["GET"])
# @jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        request_public = {}
        request_public['pekerjaan'] = []
        request_public['tujuan'] = []

        bidang1 = []
        bidang1.append({'nama': 'Agama dan Filsafat'})
        bidang1.append({'nama': 'Bahasa, Sosial, dan Humaniora'})
        bidang1.append({'nama': 'Ekonomi'})
        bidang1.append({'nama': 'Hewani'})
        bidang1.append({'nama': 'Kedokteran dan Kesehatan'})
        bidang1.append({'nama': 'Matematika dan Pengetahuan Alam'})
        bidang1.append({'nama': 'Pendidikan'})
        bidang1.append({'nama': 'Seni, Desain, dan Media'})
        bidang1.append({'nama': 'Tanaman'})
        bidang1.append({'nama': 'Teknik'})

        bidang2 = []
        bidang2.append({'nama': 'Pertanian (Agrikultur)'})
        bidang2.append({'nama': 'Pertambangan'})
        bidang2.append({'nama': 'Pabrikasi (Manufaktur)'})
        bidang2.append({'nama': 'Kontruksi'})
        bidang2.append({'nama': 'Perdagangan'})
        bidang2.append({'nama': 'Jasa Keuangan'})
        bidang2.append({'nama': 'Jasa Perorangan'})
        bidang2.append({'nama': 'Jasa Umum'})
        bidang2.append({'nama': 'Jasa Wisata'})

        pekerjaan1 = {}
        pekerjaan1['nama'] = 'Pelajar/ Mahasiswa/ Akademisi'
        pekerjaan1['bidang'] = bidang1

        pekerjaan2 = {}
        pekerjaan2['nama'] = 'Profesional'
        pekerjaan2['bidang'] = bidang2

        pekerjaan3 = {}
        pekerjaan3['nama'] = 'Wirausaha'
        pekerjaan3['bidang'] = bidang2

        pekerjaan4 = {}
        pekerjaan4['nama'] = 'Pemerintahan'
        pekerjaan4['bidang'] = []

        request_public['pekerjaan'].append(pekerjaan1)
        request_public['pekerjaan'].append(pekerjaan2)
        request_public['pekerjaan'].append(pekerjaan3)
        request_public['pekerjaan'].append(pekerjaan4)

        request_public['tujuan'].append({'nama': 'Referensi Kajian Bisnis'})
        request_public['tujuan'].append({'nama': 'Referensi Pembuatan Kebijakan'})
        request_public['tujuan'].append({'nama': 'Referensi Pembuatan Kurikulum/ Bahan Ajar'})
        request_public['tujuan'].append({'nama': 'Referensi Tugas/ Karya Ilmiah'})
        request_public['tujuan'].append({'nama': 'Referensi Pribadi'})

        request_public['skpd'] = []

        res2, skpd = SKPD_CONTROLLER.get_all({'regional_id': 1, 'is_deleted': False}, "", ['nama_skpd', 'ASC'], 100, 0)
        if res2:
            for sk_ in skpd:
                temp = {}
                temp['id'] = sk_['id']
                temp['kode_skpd'] = sk_['kode_skpd']
                temp['nama_skpd'] = sk_['nama_skpd']
                temp['nama_skpd_alias'] = sk_['nama_skpd_alias']
                temp['logo'] = sk_['logo']
                request_public['skpd'].append(temp)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public/info_admin", methods=["GET"])
# @jwt_check()
def get_info_admin():
    ''' Doc: function get info admin'''
    try:
        res = True
        request_public = {}
        request_public['alasan_tolak'] = []
        request_public['sumber_dataset'] = []
        request_public['topik'] = []
        request_public['tahun'] = []

        request_public['alasan_tolak'].append({'notes_type': 'Data tidak tersedia'})
        request_public['alasan_tolak'].append({'notes_type': 'Permohonan kurang jelas'})
        request_public['alasan_tolak'].append({'notes_type': 'Di luar kewenangan Pemerintah Provinsi Jawa Barat'})
        request_public['alasan_tolak'].append({'notes_type': 'Data yang dimohonkan bersifat dikecualikan/terbatas'})
        request_public['alasan_tolak'].append({'notes_type': 'Lainnya'})

        request_public['sumber_dataset'].append({'dataset_source': 'Open Data Jabar'})
        request_public['sumber_dataset'].append({'dataset_source': 'Jabar Drive'})
        request_public['sumber_dataset'].append({'dataset_source': 'Sumber Lain'})

        res2, sektoral = SEKTORAL_CONTROLLER.get_all({'is_deleted': False, 'is_opendata': True}, "", ['name', 'ASC'], 100, 0, False)
        if res2:
            for sek in sektoral:
                temp = {}
                temp['jd_topik'] = sek['name']
                request_public['topik'].append(temp)

        today = datetime.date.today()
        for year in range(today.year - 10, today.year+1):
            temp = {}
            temp['jd_tahun'] = year
            request_public['tahun'].append(temp)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/request_public/tracking", methods=["GET"])
# @jwt_check()
def get_tracking():
    ''' Doc: function get_tracking'''
    try:
        email = request.args.get("email", "")
        ticket_number = request.args.get("ticket_number", "")

        if email and ticket_number:
            where = {'id':ticket_number, 'email': email}
            search = ""
            sort = ['id','DESC']
            limit = 1
            skip = 0
            request_result, request_public = REQUEST_PUBLIC_CONTROLLER.get_all(where, search, sort, limit, skip)
            if request_result:
                res = True
                message = 'Data permohonan dataset ditemukan'
                request_public = request_public[0]
            else:
                res = False
                message = 'Data permohonan dataset tidak ditemukan'
        elif not email:
            res = False
            message = "Email tidak sesuai, mohon periksa ulang"
        elif not ticket_number:
            res = False
            message = "Nomor tiket tidak sesuai, mohon periksa ulang"
        else:
            res = False
            message = "Email & nomor tiket tidak sesuai, mohon periksa ulang"

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": request_public}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

