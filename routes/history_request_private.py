''' Doc: route history request private'''
import json
from flask import request, Blueprint, Response, send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
from controllers import HistoryRequestPrivateController
from controllers import NotificationController
from controllers import UserController
from models import RequestPrivateModel
from models import UserModel
from models import RoleModel
from settings import configuration as conf

BP = Blueprint('history_request_private', __name__)
HISTORY_REQUEST_PRIVATE_CONTROLLER = HistoryRequestPrivateController()
NOTIFICATION_CONTROLLER = NotificationController()
USER_CONTROLLER = UserController()
HELPER = Helper()
MSESSAGE_NO_TIKET = "Nomor tiket : "
MESSAGE_PERMOHONAN_DATA = "Permohonan data terkait <strong>"
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/history_request_private", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, history_request = HISTORY_REQUEST_PRIVATE_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, history_request = HISTORY_REQUEST_PRIVATE_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_private/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, history_request, message = HISTORY_REQUEST_PRIVATE_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_private", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        jwt = HELPER.read_jwt()

        # prepare json data
        json_request = request.get_json(silent=True)
        json_request['cuid'] = jwt['id']

        # insert history request
        res, history_request = HISTORY_REQUEST_PRIVATE_CONTROLLER.create(json_request)

        # insert to notification
        if res:
            if jwt['role']['name'] == 'walidata':
                # get user
                data_request = db.session.query(RequestPrivateModel)
                data_request = data_request.get(history_request['request_private_id'])

                if data_request:
                    data_request = data_request.__dict__
                    user_id = data_request['user_id']

                    json_notification = {}
                    json_notification['feature'] = 3
                    json_notification['type'] = 'request_private'
                    json_notification['type_id'] = history_request['request_private_id']
                    json_notification['sender'] = jwt['id']
                    json_notification['receiver'] = data_request['user_id']
                    json_notification['is_read'] = False
                    json_notification['cdate'] = HELPER.local_date_server()
                    json_notification['mdate'] = HELPER.local_date_server()

                    titles = '{}'.format(str(data_request['judul_data']))

                    # prepare notification
                    result, notification = HISTORY_REQUEST_PRIVATE_CONTROLLER.prepare_notification(history_request, json_notification, titles)

                    # send notification
                    HISTORY_REQUEST_PRIVATE_CONTROLLER.send_notification(user_id, notification, request)

            else:
                # get user
                data_user = db.session.query(UserModel.id)
                data_user = data_user.join(RoleModel, RoleModel.id == UserModel.role_id)
                data_user = data_user.filter(RoleModel.name == 'walidata')
                data_user = data_user.all()

                data_request = db.session.query(RequestPrivateModel)
                data_request = data_request.get(history_request['request_private_id'])

                if data_request:
                    data_request = data_request.__dict__

                    for dr_ in data_user:
                        temp = dr_.__dict__
                        user_id = temp['id']
                        json_notification = {}
                        json_notification['feature'] = 3
                        json_notification['type'] = 'request_private'
                        json_notification['type_id'] = data_request['id']
                        json_notification['sender'] = jwt['id']
                        json_notification['receiver'] = user_id
                        json_notification['title'] = data_request['nama'].capitalize() + " mengajukan permohonan data "
                        json_notification['content'] = "<b>" + data_request['judul_data'] + "</b> dari Satu Data Jabar."
                        json_notification['is_read'] = False
                        json_notification['cdate'] = HELPER.local_date_server()
                        json_notification['mdate'] = HELPER.local_date_server()
                        result, notification = NOTIFICATION_CONTROLLER.create(json_notification)

                        # send notification
                        HISTORY_REQUEST_PRIVATE_CONTROLLER.send_notification(user_id, notification, request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_private/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history request
        res, history_request = HISTORY_REQUEST_PRIVATE_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_private/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history request
        res = HISTORY_REQUEST_PRIVATE_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/history_request_private/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = HISTORY_REQUEST_PRIVATE_CONTROLLER.get_download(start_date, end_date)
        metadata = [
            "Nomor Tiket", "Nama Pemohon", "Email", "Telepon",
            "Organisasi Perangkat Daerah Pemohon", "Unit Kerja Level 1 Pemohon", "Unit Kerja Level 2 Pemohon", "Unit Kerja Level 3 Pemohon", "Unit Kerja Level 4 Pemohon",
            "Judul Data", "Mengetahui OPD Sumber", "Organisasi Sumber Data", "Deskripsi Kebutuhan Dataset",
            "Tujuan Dataset", "Tanggal Dibuat", "Tanggal Diperbarui", "Catatan Terakhir", "Status"
        ]

        # response
        if res:
            # success response
            filename = HELPER.convert_xls('static/download/request_dataset_private.xlsx', feedback, metadata)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='request_dataset_private.xlsx',
                as_attachment=True
            )
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
