''' Doc: route ewalidata_data'''
import json
import math
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import EwalidataDataController
from settings import configuration as conf

BP = Blueprint('ewalidata_data', __name__)
EWALIDATA_DATA_CONTROLLER = EwalidataDataController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/ewalidata_data", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            res, ewalidata_data = EWALIDATA_DATA_CONTROLLER.get_count(where, search)
        else:
            res, ewalidata_data = EWALIDATA_DATA_CONTROLLER.get_all(where, search, sort, limit, skip)

        res_count, count = EWALIDATA_DATA_CONTROLLER.get_count(where, search)

        meta = {}
        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": ewalidata_data,"meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [],"meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, ewalidata_data = EWALIDATA_DATA_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": ewalidata_data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert ewalidata_data
        res, ewalidata_data = EWALIDATA_DATA_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": ewalidata_data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data/multiple", methods=["POST"])
@jwt_check()
def create_multiple():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # insert data
            datas = []
            for json_req in json_request:
                res, data = EWALIDATA_DATA_CONTROLLER.create(json_req)
                datas.append(data)
        else:
            # insert data
            res, datas = EWALIDATA_DATA_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update ewalidata_data
        res, ewalidata_data = EWALIDATA_DATA_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": ewalidata_data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data/multiple", methods=["PUT"])
@jwt_check()
def update_multiple():
    ''' Doc: function update'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # insert data
            datas = []
            for json_req in json_request:
                # update data
                where = {
                    'id': int(json_req['id'])
                }
                res, data = EWALIDATA_DATA_CONTROLLER.update(where, json_req)
                datas.append(data)
        else:
            # update data
            where = {
                'id': int(json_request['id'])
            }
            res, datas = EWALIDATA_DATA_CONTROLLER.update(where, json_request)


        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete ewalidata_data
        res = EWALIDATA_DATA_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/ewalidata_data/submit", methods=["POST"])
@jwt_check()
def submit():
    ''' Doc: function submit'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if isinstance(json_request, list):
            # print('array')
            datas = []
            for json_req in json_request:
                if 'id' in json_req:
                    # print('ada id')
                    # update
                    where = {'id': int(json_req['id'])}
                    res, data = EWALIDATA_DATA_CONTROLLER.update(where, json_req)
                    datas.append(data)

                elif 'kode_pemda' in json_req and 'kode_indikator' in json_req and 'tahun' in json_req:
                    # print('ada kode')
                    exist = EWALIDATA_DATA_CONTROLLER.check_data_exist(json_req['kode_pemda'], json_req['kode_indikator'], json_req['tahun'])

                    if exist:
                        # print('update')
                        # update
                        where = {'kode_pemda': json_req['kode_pemda'], 'kode_indikator': json_req['kode_indikator'], 'tahun': json_req['tahun'], }
                        res, data = EWALIDATA_DATA_CONTROLLER.update(where, json_req)
                        datas.append(data)
                    else:
                        # print('create')
                        # create
                        res, data = EWALIDATA_DATA_CONTROLLER.create(json_req)
                        datas.append(data)
                else:
                    res = False
                    msg = 'Missing kode_pemda, kode_indikator, tahun'

            if res:
                # send to server
                res_sipd = EWALIDATA_DATA_CONTROLLER.send_data_to_sipd(json_request)

        else:
            # print('single')
            if 'id' in json_request:
                # print('ada id')
                # update
                where = {'id': int(json_request['id'])}
                res, datas = EWALIDATA_DATA_CONTROLLER.update(where, json_request)

            elif 'kode_pemda' in json_request and 'kode_indikator' in json_request and 'tahun' in json_request:
                # print('ada kode')
                exist = EWALIDATA_DATA_CONTROLLER.check_data_exist(json_request['kode_pemda'], json_request['kode_indikator'], json_request['tahun'])

                if exist:
                    # print('update')
                    # update
                    where = {'kode_pemda': json_request['kode_pemda'], 'kode_indikator': json_request['kode_indikator'], 'tahun': json_request['tahun'], }
                    res, datas = EWALIDATA_DATA_CONTROLLER.update(where, json_request)
                else:
                    # print('create')
                    # create
                    res, datas = EWALIDATA_DATA_CONTROLLER.create(json_request)
            else:
                res = False
                msg = 'Missing kode_pemda, kode_indikator, tahun'

            if res:
                # send to server
                json_requests = []
                json_requests.append(json_request)
                res_sipd = EWALIDATA_DATA_CONTROLLER.send_data_to_sipd(json_request)

        # response
        if res:
            # success response
            response = {"message": "Submit data successfull", "error": 0, "data": datas}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Submit data failed, " + msg, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
