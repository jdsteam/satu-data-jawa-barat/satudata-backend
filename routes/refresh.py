''' Doc: route article'''
import json
from flask import Blueprint, Response
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import RefreshController
from settings import configuration as conf

BP = Blueprint('article_management', __name__)
CONTROLLER = RefreshController()
HELPER = Helper()

@BP.route("/refresh", methods=["GET"])
def refresh():
    ''' Doc: function refresh'''
    try:
        result = CONTROLLER.refresh_view()
        # response
        response = {
                "message": "refresh materialized view succes",
                "error": 0, "data": result,
        }

        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
