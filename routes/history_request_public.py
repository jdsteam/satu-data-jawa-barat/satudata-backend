''' Doc: route history request public'''
import json
from flask import request, Blueprint, Response, send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import HistoryRequestPublicController
from settings import configuration as conf

BP = Blueprint('history_request_public', __name__)
HISTORY_REQUEST_PUBLIC_CONTROLLER = HistoryRequestPublicController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/history_request_public", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, history_request = HISTORY_REQUEST_PUBLIC_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, history_request = HISTORY_REQUEST_PUBLIC_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_public/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, history_request, message = HISTORY_REQUEST_PUBLIC_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_public", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        jwt = HELPER.read_jwt()

        # prepare json data
        json_request = request.get_json(silent=True)
        json_request['cuid'] = jwt['id']

        if 'odj_link' in json_request:
            if isinstance(json_request['odj_link'], list):
                json_request['odj_link'] = json.dumps(json_request['odj_link'])
            elif isinstance(json_request['odj_link'], str):
                json_request['odj_link'] = json_request['odj_link']
            else:
                json_request['odj_link'] = ""

        # insert history request
        res, history_request = HISTORY_REQUEST_PUBLIC_CONTROLLER.create(json_request)

        if history_request['odj_link']:
            if '"[' in history_request['odj_link']:
                history_request['odj_link'] = history_request['odj_link'].strip('"')
                history_request['odj_link'] = history_request['odj_link'].replace('\\', '')
                history_request.update({"odj_link":json.loads(history_request['odj_link'])})
            elif '["' in history_request['odj_link']:
                history_request.update({"odj_link":json.loads(history_request['odj_link'])})

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_public/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history request
        res, history_request = HISTORY_REQUEST_PUBLIC_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": history_request}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_public/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history request
        res = HISTORY_REQUEST_PUBLIC_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/history_request_public/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")
        sort = request.args.get("sort", 'cdate:ASC')
        sort = sort.split(':')
        res, feedback = HISTORY_REQUEST_PUBLIC_CONTROLLER.get_download(start_date, end_date, sort)
        metadata = [
            "Nomor Tiket", "Nama Pemohon", "Email", "Telepon", "Instansi", "Pekerjaan",
            "Bidang", "Judul Data", "Mengetahui OPD Sumber", "Nama OPD", "Deskripsi Kebutuhan Dataset",
            "Tujuan Dataset", "Status", "Tanggal Dibuat", "Tanggal Diperbarui", "Catatan Terakhir",
            "Bersedia Dihubungi"
        ]

        # response
        if res:
            # success response
            filename = HELPER.convert_xls('static/download/request_dataset_public.xlsx', feedback, metadata)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='request_dataset_public.xlsx',
                as_attachment=True
            )
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
