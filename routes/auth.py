''' Doc: route app '''
import json
import requests
from flask import Blueprint
from flask import request
from flask import Response
from helpers.jsonencoder import JSONEncoder
from helpers import decorator
from controllers import AuthController
from controllers import UserController
from controllers import RoleController
from controllers import SkpdController
from controllers import SkpdSubController
from controllers import SkpdUnitController
from models import BKDJabatanExtract
from models import HistoryLoginModel
from models import SkpdModel
from models import SkpdSubModel
from models import SkpdUnitModel
from models import BKDJabatanExtract
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
from settings import configuration as conf
from requests.auth import HTTPBasicAuth

BP = Blueprint('user', __name__)
AUTH_CONTROLLER = AuthController()
USER_CONTROLLER = UserController()
ROLE_CONTROLLER = RoleController()
SKPD_CONTROLLER = SkpdController()
SKPDSUB_CONTROLLER = SkpdSubController()
SKPDUNIT_CONTROLLER = SkpdUnitController()
HELPER = Helper()
# pylint: disable=broad-except, unused-variable

@BP.route("/auth/login", methods=["POST"])
# @jwt_check()
def login():
    ''' Doc: function login'''
    try:
        # filter data
        platform = request.args.get("platform", "")

        # prepare json data
        json_request = request.get_json(silent=True)

        # login user
        res, user, message = AUTH_CONTROLLER.login2(json_request)

        # log
        try:
            log = {
                'status' : "success" if res else "failed",
                'date_start' : HELPER.local_date_server(),
                'date_end' : HELPER.local_date_server(),
                'user_id' : user['id'] if res else None,
                'user_username' : json_request['username'],
                'user_name' : user['name'] if res else None,
                'user_role' : user['role_name'] if res else None,
                'user_organization_id' : user['satuan_kerja_id'] if res else None,
                'user_organization_name' : user['satuan_kerja_nama'] if res else None,
                'user_position_id' : user['jabatan_id'] if res else None,
                'user_position_name' : user['jabatan_nama'] if res else None ,
                'platform' : platform if platform else "satudata",
                'via' : 'Login Username'
            }

            req_ = requests.post(
                conf.LOG_URL + 'satudata/login',
                headers={"Content-Type": conf.MESSAGE_APP_JSON},
                data=json.dumps(log)
            )
            res_ = req_.json()
        except Exception as err:
            print(str(err))

        # response
        if res:
            # success response
            response = {"message": "Login successfull", "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Login failed, " + message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 401

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

@BP.route("/auth/login2", methods=["POST"])
# @jwt_check()
def login2():
    ''' Doc: function login'''
    try:
        # filter data
        platform = request.args.get("platform", "")

        # prepare json data
        json_request = request.get_json(silent=True)

        # insert login atemp
        # login_atempt_data = AUTH_CONTROLLER.insert_login_attempt(request.remote_addr, json_request['username'], request.headers.get('User-Agent'))

        # check_block_username
        is_allow_block_username, number_attempt = AUTH_CONTROLLER.check_block_username(json_request['username'])
        if not is_allow_block_username:
            response = {"message": "Login diblok, sudah lebih dari 3 kali login menggunakan username yang sama.", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        # check_block_ip
        is_allow_block_ip, number_attempt = AUTH_CONTROLLER.check_block_ip(request.remote_addr)
        if not is_allow_block_ip:
            response = {"message": "Login diblok, sudah lebih dari 5 kali login menggunakan ip yang sama.", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        # check recatpcha
        is_allow_recaptcha, data_allow_recaptcha = AUTH_CONTROLLER.check_recaptcha(json_request)
        if not is_allow_recaptcha:
            response = {"message": data_allow_recaptcha, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

        # login user
        res, user, message = AUTH_CONTROLLER.login2(json_request)

        # log
        try:
            log = {
                'status' : "success" if res else "failed",
                'date_start' : HELPER.local_date_server(),
                'date_end' : HELPER.local_date_server(),
                'user_id' : user['id'] if res else None,
                'user_username' : json_request['username'],
                'user_name' : user['name'] if res else None,
                'user_role' : user['role_name'] if res else None,
                'user_organization_id' : user['satuan_kerja_id'] if res else None,
                'user_organization_name' : user['satuan_kerja_nama'] if res else None,
                'user_position_id' : user['jabatan_id'] if res else None,
                'user_position_name' : user['jabatan_nama'] if res else None ,
                'platform' : platform if platform else "satudata",
                'via' : 'Login Username'
            }

            req_ = requests.post(
                conf.LOG_URL + 'satudata/login',
                headers={"Content-Type": conf.MESSAGE_APP_JSON},
                data=json.dumps(log)
            )
            res_ = req_.json()
        except Exception as err:
            print(str(err))

        # response
        if res:
            # success response
            response = {"message": "Login successfull", "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Login failed, " + message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 401

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

@BP.route("/auth/logout", methods=["GET"])
# @jwt_check()
def logout():
    ''' Doc: function logout'''
    try:
        # jwt
        _jwt = HELPER.read_jwt()

        # insert history login
        json_history = {}
        json_history['ip'] = request.remote_addr
        json_history['browser'] = request.headers.get('User-Agent')
        json_history['type'] = 'logout'
        json_history["user_id"] = _jwt['id']
        json_history["datetime"] = HELPER.local_date_server()
        try:
            # normal request
            url = 'http://www.geoplugin.net/json.gp?ip='+request.remote_addr
            req_ = requests.get(url)
            js_ = json.loads(req_.text)

            # using request service
            # url = 'http://www.geoplugin.net/json.gp?ip='+request.remote_addr
            # data_autologin = {
            #     "url": url,
            #     "headers": {
            #         "Content-Type": "application/json"
            #     },
            # }
            # req_ = requests.post(
            #     conf.REQUEST_URL + 'http',
            #     headers={"Content-Type": conf.MESSAGE_APP_JSON},
            #     data=json.dumps(data_autologin)
            # )
            # js_ = req_.json()['data']

            json_history['country_code'] = js_['geoplugin_countryCode']
            json_history['country'] = js_['geoplugin_countryName']
            json_history['region'] = js_['geoplugin_region']
            json_history['lat'] = js_['geoplugin_latitude']
            json_history['long'] = js_['geoplugin_longitude']
        except Exception as err:
            pass
        # prepare data model
        result = HistoryLoginModel(**json_history)
        # execute database
        db.session.add(result)
        db.session.commit()
        result = result.to_dict()

        # response
        if result:
            # success response
            response = {"message": "Logout successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Logout failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        print(str(err))
        response = {"message": "Internal Server Error. JWT not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

@BP.route("/auth/reset", methods=["POST"])
def reset_password():
    ''' Doc: function reset password'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert user
        status, res = AUTH_CONTROLLER.reset_password(json_request)
        # response
        if status:
            # success response
            response = {"message": "Reset Password Berhasil.", "error": 0, "data": res}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Reset Password Gagal, " + res, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

@BP.route("/auth/check", methods=["GET"])
def check():
    ''' Doc: function check'''
    token = request.headers.get('Authorization', None)
    app_code = request.args.get("app_code")
    controller = request.args.get("controller")
    action = request.args.get("action")

    if controller[0] == '/':
        controller = controller[(len(controller)-1)*-1:]

    controller = controller.split("/")
    controller = controller[0]

    return decorator.check_role_user(token, controller, action)


@BP.route("/auth/verify-token2", methods=["POST"])
def verify2():
    ''' Doc: function check'''
    # prepare json data
    json_request = request.get_json(silent=True)

    if 'username' not in json_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'username' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
    if 'authenticatorKey' not in json_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'authenticatorKey' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
    if 'anotationKey' not in json_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'anotationKey' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


    try:
        username = json_request['username']
        authenticatorKey = json_request['authenticatorKey']
        anotationKey = json_request['anotationKey']

        # normal request
        url_autologin = "https://siap.jabarprov.go.id/autologin/portaldatajabar/verify-token"
        url_autologin += "?username="+username
        url_autologin += "&authenticatorKey="+authenticatorKey
        url_autologin += "&anotationKey="+anotationKey
        req_autologin = requests.get(url_autologin, headers={"Content-Type": "application/json"})
        response_autologin = req_autologin.json()

        # using request service
        # url_autologin = "https://siap.jabarprov.go.id/autologin/portaldatajabar/verify-token"
        # url_autologin += "?username="+username
        # url_autologin += "&authenticatorKey="+authenticatorKey
        # url_autologin += "&anotationKey="+anotationKey
        # data_autologin = {
        #     "url": url_autologin,
        #     "headers": {
        #         "Content-Type": "application/json"
        #     },
        # }
        # req_autologin = requests.post(
        #     conf.REQUEST_URL + 'http',
        #     headers={"Content-Type": conf.MESSAGE_APP_JSON},
        #     data=json.dumps(data_autologin)
        # )
        # response_autologin = req_autologin.json()['data']

        if response_autologin['success']:

            try:
                # normal request
                url_data = 'https://siap.jabarprov.go.id/integrasi/api/v1/pegawai/data?page=1&perpage=1&params={"peg_nip":' + username + '}'
                req_data = requests.get(url_data, auth=HTTPBasicAuth(conf.SIAP_JABAR_USERNAME, conf.SIAP_JABAR_PASSWORD))
                response_data = req_data.json()

                # using request service
                # url_data = 'https://siap.jabarprov.go.id/integrasi/api/v1/pegawai/data?page=1&perpage=1&params={"peg_nip":' + username + '}'
                # data_data = {
                #     "url": url_data,
                #     "headers": {
                #         "Content-Type": "application/json"
                #     },
                #     "auth-basic": {
                #         "username": conf.SIAP_JABAR_USERNAME,
                #         "password": conf.SIAP_JABAR_PASSWORD
                #     }
                # }
                # req_data = requests.post(
                #     conf.REQUEST_URL + 'http',
                #     headers={"Content-Type": conf.MESSAGE_APP_JSON},
                #     data=json.dumps(data_data)
                # )
                # response_data = req_data.json()['data']

                if response_data['count'] > 0:
                    # prepare data
                    data_user = response_data['data'][0]
                    json_request = {}
                    json_request['username'] = data_user['peg_nip']
                    json_request['email'] = data_user['peg_email']
                    json_request['nip'] = data_user['peg_nip']
                    json_request['name'] = data_user['peg_nama_lengkap']
                    json_request['profile_pic'] = data_user['peg_foto_url']
                    json_request['kode_kemendagri'] = '32'
                    json_request['kode_skpd'] = None
                    json_request['kode_skpdsub'] = None
                    json_request['kode_skpdunit'] = None
                    json_request['satuan_kerja_id'] = data_user['satuan_kerja_id']
                    json_request['lv1_unit_kerja_id'] = None
                    json_request['lv2_unit_kerja_id'] = None
                    json_request['lv3_unit_kerja_id'] = None
                    json_request['lv4_unit_kerja_id'] = None
                    json_request['level_unit_kerja'] = 0
                    json_request['jabatan_id'] = data_user['jabatan_id']
                    json_request['count_notif'] = 0
                    json_request['agreement_id'] = None
                    json_request['role_id'] = None
                    json_request['dash_role_id'] = None
                    json_request['last_login'] = HELPER.local_date_server()
                    json_request['is_active'] = True
                    json_request['is_deleted'] = False
                    json_request['notes'] = 'Autologin SIAP Jabar'
                    json_request['json_data'] = json.dumps(data_user)
                    json_request['cdate'] = HELPER.local_date_server()
                    json_request['cuid'] = '1'
                    json_request['mdate'] = HELPER.local_date_server()
                    json_request['muid'] = '1'

                    if data_user['unit_kerja_level']:
                        json_request['level_unit_kerja'] = data_user['unit_kerja_level']

                    # data tambahan
                    try:
                        result_jabatan = db.session.query(BKDJabatanExtract)
                        result_jabatan = result_jabatan.filter(BKDJabatanExtract.jabatan_id == str(data_user['jabatan_id']))
                        result_jabatan = result_jabatan.first()

                        if result_jabatan:
                            jabatan = result_jabatan.__dict__
                            jabatan.pop('_sa_instance_state', None)

                            json_request['satuan_kerja_id'] = jabatan['satuan_kerja_id']
                            json_request['lv1_unit_kerja_id'] = jabatan['lv1_unit_kerja_id']
                            json_request['lv2_unit_kerja_id'] = jabatan['lv2_unit_kerja_id']
                            json_request['lv3_unit_kerja_id'] = jabatan['lv3_unit_kerja_id']
                            json_request['lv4_unit_kerja_id'] = jabatan['lv4_unit_kerja_id']

                            # skpd
                            res_skpd = db.session.query(SkpdModel)
                            res_skpd = res_skpd.get(jabatan['satuan_kerja_id'])

                            if res_skpd:
                                data_skpd = res_skpd.__dict__
                                data_skpd.pop('_sa_instance_state', None)
                                json_request['kode_skpd'] = data_skpd['kode_skpd']

                            # skpdsub
                            if jabatan['lv1_unit_kerja_id']:
                                res_skpdsub = db.session.query(SkpdSubModel)
                                res_skpdsub = res_skpdsub.get(jabatan['lv1_unit_kerja_id'])

                                if res_skpdsub:
                                    data_skpdsub = res_skpdsub.__dict__
                                    data_skpdsub.pop('_sa_instance_state', None)
                                    json_request['kode_skpdsub'] = data_skpdsub['kode_skpdsub']

                            # skpdunit
                            if jabatan['lv2_unit_kerja_id']:
                                res_skpdunit = db.session.query(SkpdUnitModel)
                                res_skpdunit = res_skpdunit.get(jabatan['lv2_unit_kerja_id'])

                                if res_skpdunit:
                                    data_skpdunit = res_skpdunit.__dict__
                                    data_skpdunit.pop('_sa_instance_state', None)
                                    json_request['kode_skpdunit'] = data_skpdunit['kode_skpdunit']

                            # role
                            json_request.pop('dash_role_id')

                    except Exception as err:
                        print(err)

                    # check if data on db
                    res, user_temp = USER_CONTROLLER.get_by_username(username)
                    if res:
                        if user_temp['role_id'] is None:
                            res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd-view'}, '', ['id', 'asc'], 1, 0)
                            if res_role:
                                json_request['role_id'] = data_role[0]['id']
                        else:
                            json_request.pop('role_id')
                            json_request.pop('agreement_id')
                        # data is on db, update data
                        json_request['mdate'] = HELPER.local_date_server()
                        json_request['muid'] = '1'
                        res_update, user_update = USER_CONTROLLER.update({'id': user_temp['id']}, json_request)
                    else:
                        res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd-view'}, '', ['id', 'asc'], 1, 0)
                        if res_role:
                            json_request['role_id'] = data_role[0]['id']
                        # data not exist, insert data
                        json_request['password'] = json_request['nip'][-5:]
                        json_request['count_notif'] = 0
                        json_request['agreement_id'] = None
                        json_request['cdate'] = HELPER.local_date_server()
                        json_request['cuid'] = '1'
                        res_insert, user_insert, mess = USER_CONTROLLER.create(json_request)

                    # login user
                    res, user, message = AUTH_CONTROLLER.autologin(username)

                    # log
                    try:
                        log = {
                            'status' : "success" if res else "failed",
                            'date_start' : HELPER.local_date_server(),
                            'date_end' : HELPER.local_date_server(),
                            'user_id' : user['id'] if res else None,
                            'user_username' : json_request['username'],
                            'user_name' : user['name'] if res else None,
                            'user_role' : user['role_name'] if res else None,
                            'user_organization_id' : user['satuan_kerja_id'] if res else None,
                            'user_organization_name' : user['satuan_kerja_nama'] if res else None,
                            'user_position_id' : user['jabatan_id'] if res else None,
                            'user_position_name' : user['jabatan_nama'] if res else None ,
                            'platform' : "satudata",
                            'via' : 'Autologin SIAP Jabar'
                        }

                        req_ = requests.post(
                            conf.LOG_URL + 'satudata/login',
                            headers={"Content-Type": conf.MESSAGE_APP_JSON},
                            data=json.dumps(log)
                        )
                        res_ = req_.json()
                    except Exception as err:
                        print(str(err))

                    if res:
                        # success response
                        response = {"message": "Login successfull", "error": 0, "data": user}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                    else:
                        # success response but no data
                        response = {"message": "Login failed, " + message, "error": 1, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                else:
                    # fail response
                    response = {"message": "Internal Server Error. " + "Data pegawai not found.", "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

            except Exception as err:
                # fail response
                response = {"message": "Internal Server Error. " + "API SIAP response not valid.", "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        else:
            # fail response
            response = {"message": "Internal Server Error. " +response_autologin['message'], "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + "SIAP JABAR response not valid.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/auth/verify-token-sso", methods=["POST"])
def verify_sso():
    ''' Doc: function check'''
    # prepare json data
    form_request = request.get_json(silent=True)

    if 'client_id' not in form_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'client_id' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
    if 'code' not in form_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'code' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
    if 'redirect_uri' not in form_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'redirect_uri' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

    try:
        # normal request
        url_autologin = "https://sso.jabarprov.go.id/realms/ssojabar/protocol/openid-connect/token"
        payload = "grant_type=authorization_code"
        payload += "&code="+form_request['code']
        payload += "&redirect_uri="+form_request['redirect_uri']

        if 'satudata' in form_request['client_id']:
            payload += "&client_id="+conf.SSO_JABAR_CLIENT_ID
            payload += "&client_secret="+conf.SSO_JABAR_CLIENT_SECRET
        elif 'admin' in form_request['client_id']:
            payload += "&client_id="+conf.SSO_JABAR_EDJ_CLIENT_ID
            payload += "&client_secret="+conf.SSO_JABAR_EDJ_CLIENT_SECRET
        else:
            response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'client_id' not known.", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

        req_autologin = requests.post(url_autologin, headers={"Content-Type": "application/x-www-form-urlencoded"}, data=payload)
        response_autologin = req_autologin.json()

        if 'error' not in response_autologin and 'access_token' in response_autologin:

            try:
                # normal request
                url_data = 'https://sso.jabarprov.go.id/realms/ssojabar/protocol/openid-connect/userinfo'
                req_data = requests.get(url_data, headers={"Authorization": "Bearer " + response_autologin['access_token']})
                response_data = req_data.json()

                if 'preferred_username' in response_data:
                    # prepare data
                    username = response_data['preferred_username']
                    data_user = response_data

                    # get jabatan
                    result = db.session.query(BKDJabatanExtract)
                    result = result.filter(getattr(BKDJabatanExtract, 'jabatan_id') == data_user['jabatan_id'])
                    result = result.first()

                    if result:
                        result = result.__dict__
                        result.pop('_sa_instance_state', None)

                        json_request = {}
                        json_request['username'] = data_user['preferred_username']
                        json_request['email'] = data_user['email']
                        json_request['nip'] = data_user['preferred_username']
                        json_request['name'] = data_user['name']
                        json_request['profile_pic'] = ''
                        json_request['kode_kemendagri'] = '32'
                        json_request['kode_skpd'] = None
                        json_request['kode_skpdsub'] = None
                        json_request['kode_skpdunit'] = None
                        json_request['satuan_kerja_id'] = result['satuan_kerja_id']
                        json_request['lv1_unit_kerja_id'] = None
                        json_request['lv2_unit_kerja_id'] = None
                        json_request['lv3_unit_kerja_id'] = None
                        json_request['lv4_unit_kerja_id'] = None
                        json_request['level_unit_kerja'] = 0
                        json_request['jabatan_id'] = data_user['jabatan_id']
                        json_request['count_notif'] = 0
                        json_request['agreement_id'] = None
                        json_request['role_id'] = None
                        json_request['dash_role_id'] = None
                        json_request['last_login'] = HELPER.local_date_server()
                        json_request['is_active'] = True
                        json_request['is_deleted'] = False
                        json_request['notes'] = 'Autologin SSO Jabar'
                        json_request['json_data'] = json.dumps(data_user)
                        json_request['cdate'] = HELPER.local_date_server()
                        json_request['cuid'] = '1'
                        json_request['mdate'] = HELPER.local_date_server()
                        json_request['muid'] = '1'

                        if result['jabatan_kelas']:
                            json_request['level_unit_kerja'] = result['jabatan_kelas']

                        # data tambahan
                        try:
                            result_jabatan = db.session.query(BKDJabatanExtract)
                            result_jabatan = result_jabatan.filter(BKDJabatanExtract.jabatan_id == str(data_user['jabatan_id']))
                            result_jabatan = result_jabatan.first()

                            if result_jabatan:
                                jabatan = result_jabatan.__dict__
                                jabatan.pop('_sa_instance_state', None)

                                json_request['satuan_kerja_id'] = jabatan['satuan_kerja_id']
                                json_request['lv1_unit_kerja_id'] = jabatan['lv1_unit_kerja_id']
                                json_request['lv2_unit_kerja_id'] = jabatan['lv2_unit_kerja_id']
                                json_request['lv3_unit_kerja_id'] = jabatan['lv3_unit_kerja_id']
                                json_request['lv4_unit_kerja_id'] = jabatan['lv4_unit_kerja_id']

                                # skpd
                                res_skpd = db.session.query(SkpdModel)
                                res_skpd = res_skpd.get(jabatan['satuan_kerja_id'])

                                if res_skpd:
                                    data_skpd = res_skpd.__dict__
                                    data_skpd.pop('_sa_instance_state', None)
                                    json_request['kode_skpd'] = data_skpd['kode_skpd']

                                # skpdsub
                                if jabatan['lv1_unit_kerja_id']:
                                    res_skpdsub = db.session.query(SkpdSubModel)
                                    res_skpdsub = res_skpdsub.get(jabatan['lv1_unit_kerja_id'])

                                    if res_skpdsub:
                                        data_skpdsub = res_skpdsub.__dict__
                                        data_skpdsub.pop('_sa_instance_state', None)
                                        json_request['kode_skpdsub'] = data_skpdsub['kode_skpdsub']

                                # skpdunit
                                if jabatan['lv2_unit_kerja_id']:
                                    res_skpdunit = db.session.query(SkpdUnitModel)
                                    res_skpdunit = res_skpdunit.get(jabatan['lv2_unit_kerja_id'])

                                    if res_skpdunit:
                                        data_skpdunit = res_skpdunit.__dict__
                                        data_skpdunit.pop('_sa_instance_state', None)
                                        json_request['kode_skpdunit'] = data_skpdunit['kode_skpdunit']

                                # role
                                json_request.pop('dash_role_id')

                                # print(json_request)

                        except Exception as err:
                            print(err)

                        # check if data on db
                        res, user_temp = USER_CONTROLLER.get_by_username(username)
                        if res:
                            if user_temp['role_id'] is None:
                                res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd-view'}, '', ['id', 'asc'], 1, 0)
                                if res_role:
                                    json_request['role_id'] = data_role[0]['id']
                            else:
                                json_request.pop('role_id')
                                json_request.pop('agreement_id')
                            # data is on db, update data
                            json_request['mdate'] = HELPER.local_date_server()
                            json_request['muid'] = '1'
                            res_update, user_update = USER_CONTROLLER.update({'id': user_temp['id']}, json_request)
                        else:
                            res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd-view'}, '', ['id', 'asc'], 1, 0)
                            if res_role:
                                json_request['role_id'] = data_role[0]['id']
                            # data not exist, insert data
                            json_request['password'] = json_request['nip'][-5:]
                            json_request['count_notif'] = 0
                            json_request['agreement_id'] = None
                            json_request['cdate'] = HELPER.local_date_server()
                            json_request['cuid'] = '1'
                            res_insert, user_insert, mess = USER_CONTROLLER.create(json_request)

                        # login user
                        res, user, message = AUTH_CONTROLLER.autologin(username)

                        # log
                        try:
                            log = {
                                'status' : "success" if res else "failed",
                                'date_start' : HELPER.local_date_server(),
                                'date_end' : HELPER.local_date_server(),
                                'user_id' : user['id'] if res else None,
                                'user_username' : json_request['username'],
                                'user_name' : user['name'] if res else None,
                                'user_role' : user['role_name'] if res else None,
                                'user_organization_id' : user['satuan_kerja_id'] if res else None,
                                'user_organization_name' : user['satuan_kerja_nama'] if res else None,
                                'user_position_id' : user['jabatan_id'] if res else None,
                                'user_position_name' : user['jabatan_nama'] if res else None ,
                                'platform' : 'admin' if 'admin' in form_request['client_id'] else 'satudata',
                                'via' : 'Autologin SSO Jabar'
                            }

                            req_ = requests.post(
                                conf.LOG_URL + 'satudata/login',
                                headers={"Content-Type": conf.MESSAGE_APP_JSON},
                                data=json.dumps(log)
                            )
                            res_ = req_.json()
                        except Exception as err:
                            print(str(err))

                        if res:
                            # success response
                            response = {"message": "Login successfull", "error": 0, "data": user}
                            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                        else:
                            # success response but no data
                            response = {"message": "Login failed, " + message, "error": 1, "data": {}}
                            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                    else:
                        # success response but no data
                        response = {"message": "Login failed, Jabatan not found", "error": 1, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                else:
                    # fail response
                    response = {"message": "Internal Server Error. Response from SSO Jabar. " + "Data pegawai not found.", "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

            except Exception as err:
                # fail response
                response = {"message": "Internal Server Error. " + "API SSO Jabar response not valid.", "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        else:
            # fail response
            response = {"message": "Internal Server Error. Response from SSO Jabar. " +response_autologin['error_description'], "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + "SSO JABAR response not valid.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/auth/verify-token-digiteam", methods=["POST"])
def verify_digiteam():
    ''' Doc: function check'''
    # prepare json data
    form_request = request.get_json(silent=True)

    if 'client_id' not in form_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'client_id' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
    if 'code' not in form_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'code' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
    if 'redirect_uri' not in form_request:
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'redirect_uri' not found.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

    list_email_edj = [
        'a.indracahyadi@gmail.com', 'adelia.khs@gmail.com', 'adibintangprada@gmail.com', 'afridahafizhatul@gmail.com', 'agistyaanugrah@gmail.com', 'akatgelar@gmail.com',
        'aldynurfebrian@gmail.com', 'alief.11296@gmail.com', 'annisann111@gmail.com', 'ansyariakhyar@gmail.com', 'ardhrizk@gmail.com', 'ariyaagustian@gmail.com',
        'catya.pratiwi@gmail.com', 'daishyririnama@gmail.com', 'dimasaryudaf@gmail.com', 'doniramdhant@gmail.com', 'ekiditapermana@gmail.com', 'ekosistemdatajabar@digitalservice.id',
        'fadilaandini1@gmail.com', 'faisal.nugraha.c@gmail.com', 'fajaralfiandhani@gmail.com', 'fauzanmrabbani@gmail.com', 'fegadermawan@gmail.com', 'firmanramadan97@gmail.com',
        'gandes.goldestan@gmail.com', 'hamayat123@gmail.com', 'hanieffatchudin@gmail.com', 'hustiniasari@gmail.com', 'ibrahimbrata.mib@gmail.com', 'iinkurnia1805@gmail.com',
        'iqra.hardianto@gmail.com', 'irfandary@gmail.com', 'irvan.devv17@gmail.com', 'istiqomah.adenur@gmail.com', 'kimipussari@gmail.com', 'kurniaditia70@gmail.com',
        'lakhsmidewiw@gmail.com', 'lintangaries11@gmail.com', 'mikailandrew@gmail.com', 'misdan.wijaya07@gmail.com', 'nandagustiasih@gmail.com', 'nisrinaraisy@gmail.com',
        'nitafitrianiii@gmail.com', 'nopiyanti2@gmail.com', 'nugumi73@gmail.com', 'pipinfitriadi@gmail.com', 'pujinhidayah@gmail.com', 'putryulwah@gmail.com',
        'rafdighufran@gmail.com', 'rahmanisaa@gmail.com', 'raihanjauhari@gmail.com', 'rbanyufirdaus@gmail.com', 'reffiahmadstudios@gmail.com', 'refinandanur@gmail.com',
        'rendirizkir@gmail.com', 'rian.bahtiar97@gmail.com', 'riskaamalia.mail@gmail.com', 'rizaldifarhan@gmail.com', 'rizalfachrurozy2@gmail.com', 'rizki.rusdiwijaya@gmail.com',
        'rizky.prilian@gmail.com', 'rizqydwip@gmail.com', 'rohmat.hdyt18@gmail.com', 'rr.rizqiramadhan@gmail.com', 'sandirhy@gmail.com', 'shintaprima46@gmail.com', 'silmihanifah1998@gmail.com',
        'suratricki@gmail.com', 'suryakusumaardhani@gmail.com', 'syarifhidayatullah313@gmail.com', 'tryfatur@gmail.com', 'vitaaprillia36@gmail.com', 'yogaswarataufik@gmail.com', 'yunus.baktir@gmail.com'
    ]

    try:
        # normal request
        url_autologin = "https://sso.digitalservice.jabarprov.go.id/realms/jabarprov/protocol/openid-connect/token"
        payload = "grant_type=authorization_code"
        payload += "&code="+form_request['code']
        payload += "&redirect_uri="+form_request['redirect_uri']

        if 'satudata' in form_request['client_id']:
            payload += "&client_id="+conf.SSO_DIGITEAM_CLIENT_ID
            payload += "&client_secret="+conf.SSO_DIGITEAM_CLIENT_SECRET
        elif 'admin' in form_request['client_id']:
            payload += "&client_id="+conf.SSO_DIGITEAM_EDJ_CLIENT_ID
            payload += "&client_secret="+conf.SSO_DIGITEAM_EDJ_CLIENT_SECRET
        else:
            response = {"message": conf.MESSAGE_INTERNAL_ERROR + "Parameter 'client_id' not known.", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

        req_autologin = requests.post(url_autologin, headers={"Content-Type": "application/x-www-form-urlencoded"}, data=payload)
        response_autologin = req_autologin.json()

        if 'error' not in response_autologin and 'access_token' in response_autologin:

            try:
                # normal request
                url_data = 'https://sso.digitalservice.jabarprov.go.id/realms/jabarprov/protocol/openid-connect/userinfo'
                req_data = requests.get(url_data, headers={"Authorization": "Bearer " + response_autologin['access_token']})
                response_data = req_data.json()

                if 'preferred_username' in response_data:
                    # prepare data
                    username = response_data['preferred_username']
                    email = response_data['preferred_username']
                    data_user = response_data
                    data_user['jabatan_id'] = '99913814'
                    data_user['email'] = response_data['preferred_username']

                    # get jabatan
                    result = db.session.query(BKDJabatanExtract)
                    result = result.filter(getattr(BKDJabatanExtract, 'jabatan_id') == data_user['jabatan_id'])
                    result = result.first()

                    if result:
                        result = result.__dict__
                        result.pop('_sa_instance_state', None)

                        json_request = {}
                        json_request['username'] = username
                        json_request['email'] = email
                        json_request['password'] = ''
                        json_request['nip'] = None
                        json_request['name'] = data_user['name']
                        json_request['profile_pic'] = ''
                        json_request['kode_kemendagri'] = '32'
                        json_request['kode_skpd'] = None
                        json_request['kode_skpdsub'] = None
                        json_request['kode_skpdunit'] = None
                        json_request['satuan_kerja_id'] = result['satuan_kerja_id']
                        json_request['lv1_unit_kerja_id'] = None
                        json_request['lv2_unit_kerja_id'] = None
                        json_request['lv3_unit_kerja_id'] = None
                        json_request['lv4_unit_kerja_id'] = None
                        json_request['level_unit_kerja'] = 0
                        json_request['jabatan_id'] = data_user['jabatan_id']
                        json_request['count_notif'] = 0
                        json_request['agreement_id'] = None
                        json_request['role_id'] = None
                        json_request['dash_role_id'] = None
                        json_request['last_login'] = HELPER.local_date_server()
                        json_request['is_active'] = True
                        json_request['is_deleted'] = False
                        json_request['notes'] = 'Autologin Digiteam'
                        json_request['json_data'] = json.dumps(data_user)
                        json_request['cdate'] = HELPER.local_date_server()
                        json_request['cuid'] = '1'
                        json_request['mdate'] = HELPER.local_date_server()
                        json_request['muid'] = '1'

                        # data tambahan
                        try:
                            result_jabatan = db.session.query(BKDJabatanExtract)
                            result_jabatan = result_jabatan.filter(BKDJabatanExtract.jabatan_id == str(data_user['jabatan_id']))
                            result_jabatan = result_jabatan.first()

                            if result_jabatan:
                                jabatan = result_jabatan.__dict__
                                jabatan.pop('_sa_instance_state', None)

                                json_request['satuan_kerja_id'] = jabatan['satuan_kerja_id']
                                json_request['lv1_unit_kerja_id'] = jabatan['lv1_unit_kerja_id']
                                json_request['lv2_unit_kerja_id'] = jabatan['lv2_unit_kerja_id']
                                json_request['lv3_unit_kerja_id'] = jabatan['lv3_unit_kerja_id']
                                json_request['lv4_unit_kerja_id'] = jabatan['lv4_unit_kerja_id']

                                # skpd
                                res_skpd = db.session.query(SkpdModel)
                                res_skpd = res_skpd.get(
                                    jabatan['satuan_kerja_id'])

                                if res_skpd:
                                    data_skpd = res_skpd.__dict__
                                    data_skpd.pop('_sa_instance_state', None)
                                    json_request['kode_skpd'] = data_skpd['kode_skpd']

                                # skpdsub
                                if jabatan['lv1_unit_kerja_id']:
                                    res_skpdsub = db.session.query(SkpdSubModel)
                                    res_skpdsub = res_skpdsub.get(jabatan['lv1_unit_kerja_id'])

                                    if res_skpdsub:
                                        data_skpdsub = res_skpdsub.__dict__
                                        data_skpdsub.pop('_sa_instance_state', None)
                                        json_request['kode_skpdsub'] = data_skpdsub['kode_skpdsub']

                                # skpdunit
                                if jabatan['lv2_unit_kerja_id']:
                                    res_skpdunit = db.session.query(SkpdUnitModel)
                                    res_skpdunit = res_skpdunit.get(jabatan['lv2_unit_kerja_id'])

                                    if res_skpdunit:
                                        data_skpdunit = res_skpdunit.__dict__
                                        data_skpdunit.pop('_sa_instance_state', None)
                                        json_request['kode_skpdunit'] = data_skpdunit['kode_skpdunit']

                                # role
                                json_request.pop('dash_role_id')

                                # print(json_request)

                        except Exception as err:
                            print(err)

                        if 'satudata' in form_request['client_id']:
                            # check if data on db
                            res, user_temp = USER_CONTROLLER.get_by_username(username)
                            print(user_temp)
                            if res:
                                if user_temp['role_id'] is None:
                                    if email in list_email_edj:
                                        res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd'}, '', ['id', 'asc'], 1, 0)
                                    else:
                                        res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd-view'}, '', ['id', 'asc'], 1, 0)
                                    if res_role:
                                        json_request['role_id'] = data_role[0]['id']
                                else:
                                    json_request.pop('role_id')
                                    json_request.pop('agreement_id')
                                # data is on db, update data
                                json_request['mdate'] = HELPER.local_date_server()
                                json_request['muid'] = '1'
                                res_update, user_update = USER_CONTROLLER.update({'id': user_temp['id']}, json_request)
                            else:
                                if email in list_email_edj:
                                    res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd'}, '', ['id', 'asc'], 1, 0)
                                else:
                                    res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd-view'}, '', ['id', 'asc'], 1, 0)
                                if res_role:
                                    json_request['role_id'] = data_role[0]['id']
                                # data not exist, insert data
                                json_request['password'] = email
                                json_request['count_notif'] = 0
                                json_request['agreement_id'] = None
                                json_request['cdate'] = HELPER.local_date_server()
                                json_request['cuid'] = '1'
                                res_insert, user_insert, mess = USER_CONTROLLER.create(json_request)

                        elif 'admin' in form_request['client_id']:
                            # check if data on db
                            res, user_temp = USER_CONTROLLER.get_by_username(username)
                            if res:
                                if user_temp['role_id'] is None:
                                    if email in list_email_edj:
                                        res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd'}, '', ['id', 'asc'], 1, 0)
                                    else:
                                        # success response but no data
                                        response = {"message": "Login failed, Does not have access", "error": 1, "data": {}}
                                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
                                    if res_role:
                                        json_request['role_id'] = data_role[0]['id']
                                else:
                                    json_request.pop('role_id')
                                    json_request.pop('agreement_id')
                                # data is on db, update data
                                json_request['mdate'] = HELPER.local_date_server()
                                json_request['muid'] = '1'
                                res_update, user_update = USER_CONTROLLER.update({'id': user_temp['id']}, json_request)
                            else:
                                if email in list_email_edj:
                                    res_role, data_role = ROLE_CONTROLLER.get_all({'name': 'opd'}, '', ['id', 'asc'], 1, 0)
                                else:
                                    # success response but no data
                                    response = {"message": "Login failed, Does not have access", "error": 1, "data": {}}
                                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
                                if res_role:
                                    json_request['role_id'] = data_role[0]['id']
                                # data not exist, insert data
                                json_request['password'] = email
                                json_request['count_notif'] = 0
                                json_request['agreement_id'] = None
                                json_request['cdate'] = HELPER.local_date_server()
                                json_request['cuid'] = '1'
                                res_insert, user_insert, mess = USER_CONTROLLER.create(json_request)

                        # login user
                        res, user, message = AUTH_CONTROLLER.autologin(username)

                        # log
                        try:
                            log = {
                                'status' : "success" if res else "failed",
                                'date_start' : HELPER.local_date_server(),
                                'date_end' : HELPER.local_date_server(),
                                'user_id' : user['id'] if res else None,
                                'user_username' : json_request['username'],
                                'user_name' : user['name'] if res else None,
                                'user_role' : user['role_name'] if res else None,
                                'user_organization_id' : user['satuan_kerja_id'] if res else None,
                                'user_organization_name' : user['satuan_kerja_nama'] if res else None,
                                'user_position_id' : user['jabatan_id'] if res else None,
                                'user_position_name' : user['jabatan_nama'] if res else None ,
                                'platform' : 'admin' if 'admin' in form_request['client_id'] else 'satudata',
                                'via' : 'Autologin SSO Digiteam'
                            }

                            req_ = requests.post(
                                conf.LOG_URL + 'satudata/login',
                                headers={"Content-Type": conf.MESSAGE_APP_JSON},
                                data=json.dumps(log)
                            )
                            res_ = req_.json()
                        except Exception as err:
                            print(str(err))

                        if res:
                            # success response
                            response = {
                                "message": "Login successfull", "error": 0, "data": user}
                            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                        else:
                            # success response but no data
                            response = {"message": "Login failed, " + message, "error": 1, "data": {}}
                            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                    else:
                        # success response but no data
                        response = { "message": "Login failed, Jabatan not found", "error": 1, "data": {}}
                        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

                else:
                    # fail response
                    response = {"message": "Internal Server Error. Response from SSO Digiteam. " + "Data pegawai not found.", "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

            except Exception as err:
                # fail response
                print(err)
                response = {"message": "Internal Server Error. " + "API SSO Digiteam response not valid. " + err, "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

        else:
            # fail response
            response = {"message": "Internal Server Error. Response from SSO Digiteam. " + response_autologin['error_description'], "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

    except Exception as err:
        # fail response
        print(err)
        response = {"message": "Internal Server Error. " + "SSO Digiteam response not valid.", "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500
