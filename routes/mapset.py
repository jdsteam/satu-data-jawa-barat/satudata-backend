''' Doc: route mapset'''
import json
from flask import Blueprint, request, Response
from controllers import MapsetController
from controllers import UploadController
from controllers import NotificationController
from models import MapsetMetadataKugiModel
from models import MapsetModel
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from settings import configuration as conf
import math
import urllib
import requests
import os
from hashlib import sha256
from helpers import Helper
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy import func
import base64

BP = Blueprint('mapset', __name__)
MAPSET_CONTROLLER = MapsetController()
UPLOAD_CONTROLLER = UploadController()
NOTIFICATION_CONTROLLER = NotificationController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route('/mapset', methods=['GET'])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get('where', '{}')
        sort = request.args.get('sort', 'id:ASC')
        limit = request.args.get('limit', 100)
        skip = request.args.get('skip', 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        groupby = request.args.get("groupby", "")
        distinct = request.args.get("distinct", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", '"')
        where = json.loads(where)

        if distinct:
            where['is_active'] = True

        if count:
            # call function get
            res, mapset = MAPSET_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, mapset = MAPSET_CONTROLLER.get_all(where, search, sort, limit, skip, groupby)

        meta = {}
        res_count, count = MAPSET_CONTROLLER.get_count(where, search)

        meta['skip'] = int(skip)
        meta['limit'] = int(limit)
        meta['total_record'] = int(count['count'])
        if int(limit) > 0:
            meta['total_page'] = math.ceil(int(count['count']) / int(limit))
        else:
            meta['total_page'] = 0

        # response
        if res:
            # success response
            response = {'message': conf.MESSAGE_DATA_SUCCESS, 'error': 0, 'data': mapset, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset/<_id>', methods=['GET'])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''

    # handle id=title, integer or string
    try:
        temp = int(_id)
        _id = temp
    except Exception as err:
        temp = _id
        result = db.session.query(MapsetModel.id)
        result = result.filter(MapsetModel.title == temp)
        result = result.all()
        if result:
            result = result[0]
            _id = result[0]
        else:
            _id = 0

    try:
        where = request.args.get("where", "{}")

        where = where.replace("'", "\"")
        where = json.loads(where)

        # call function get by id
        res, mapset = MAPSET_CONTROLLER.get_by_id(_id, where)

        # response
        if res:
            # success response
            response = {'message': 'Get detail data successfull', 'error': 0, 'data': mapset}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset', methods=['POST'])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # history draft
        temp_history_draft = {}
        if 'history_draft' in json_request:
            temp_history_draft = json_request['history_draft']
            del json_request['history_draft']

        # metadata
        temp_metadata = []
        if 'metadata' in json_request:
            temp_metadata = json_request['metadata']
            del json_request['metadata']

        # insert mapset
        res, mapset = MAPSET_CONTROLLER.create(json_request)

        # insert to history draft
        if temp_history_draft:
            MAPSET_CONTROLLER.route_insert_history_draft(temp_history_draft, mapset)

        # create metadata
        if temp_metadata:
            MAPSET_CONTROLLER.route_create_metadata(temp_metadata, mapset)

        # insert to history
        MAPSET_CONTROLLER.route_insert_history(res, mapset, 'create')

        # insert to notification
        MAPSET_CONTROLLER.route_insert_notification(res, json_request, mapset)

        # response
        if res:
            # success response
            response = {'message': 'Create data successfull', 'error': 0, 'data': mapset}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Create data failed, ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset/<_id>', methods=['PUT'])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}
        param_from = request.args.get("from", "")

        # prepare json data
        json_request = request.get_json(silent=True)

        # history draft
        temp_history_draft = {}
        if 'history_draft' in json_request:
            temp_history_draft = json_request['history_draft']
            del json_request['history_draft']

        # metadata
        temp_metadata = []
        if 'metadata' in json_request:
            temp_metadata = json_request['metadata']
            del json_request['metadata']

        # update mapset
        res, mapset = MAPSET_CONTROLLER.update(where, json_request, True)

        # insert to history draft
        if temp_history_draft:
            MAPSET_CONTROLLER.route_insert_history_draft(temp_history_draft, mapset)

        # create metadata
        if temp_metadata:
            MAPSET_CONTROLLER.route_update_metadata(temp_metadata, mapset)

        # insert to history
        MAPSET_CONTROLLER.route_insert_history(res, mapset, 'update')

        # insert to notification
        MAPSET_CONTROLLER.route_insert_notification_update(res, json_request, mapset, param_from)

        # response
        if res:
            # success response
            response = {'message': 'Update data successfull', 'error': 0, 'data': mapset}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Update data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route('/mapset/<_id>', methods=['DELETE'])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete mapset
        res = MAPSET_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {'message': 'Delete data successfull', 'error': 0, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': 'Delete data failed', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/mapset/counter/<_id>", methods=["GET"])
# @jwt_check()
def counter(_id):
    ''' Doc: function counter'''
    # category
    category = request.args.get("category", "")

    try:
        res, mapset = MAPSET_CONTROLLER.get_by_id(_id, {})

        try:
            if category == 'view':
                count_view = mapset['count_view']
                if not count_view:
                    count_view = '0'
                count_view = int(count_view) + 1
                payload = {'count_view': int(count_view)}
                mapset['count_view'] = count_view
            elif category == 'access':
                count_access = mapset['count_access']
                if not count_access:
                    count_access = '0'
                count_access = int(count_access) + 1
                payload = {'count_access': int(count_access)}
                mapset['count_access'] = count_access
            elif category == 'view_private':
                count_view_private = mapset['count_view_private']
                if not count_view_private:
                    count_view_private = '0'
                count_view_private = int(count_view_private) + 1
                payload = {'count_view_private': int(count_view_private)}
                mapset['count_view_private'] = count_view_private
            elif category == 'access_private':
                count_access_private = mapset['count_access_private']
                if not count_access_private:
                    count_access_private = '0'
                count_access_private = int(count_access_private) + 1
                payload = {'count_access_private': int(count_access_private)}
                mapset['count_access_private'] = count_access_private
            elif category == 'download_dataset':
                count_download_dataset = mapset['count_download_dataset']
                if not count_download_dataset:
                    count_download_dataset = '0'
                count_download_dataset = int(count_download_dataset) + 1
                payload = {'count_download_dataset': int(count_download_dataset)}
                mapset['count_download_dataset'] = count_download_dataset
            elif category == 'download_image':
                count_download_image = mapset['count_download_image']
                if not count_download_image:
                    count_download_image = '0'
                count_download_image = int(count_download_image) + 1
                payload = {'count_download_image': int(count_download_image)}
                mapset['count_download_image'] = count_download_image
            elif category == 'download_shp':
                count_download_shp = mapset['count_download_shp']
                if not count_download_shp:
                    count_download_shp = '0'
                count_download_shp = int(count_download_shp) + 1
                payload = {'count_download_shp': int(count_download_shp)}
                mapset['count_download_shp'] = count_download_shp
            elif category == 'download_geojson':
                count_download_geojson = mapset['count_download_geojson']
                if not count_download_geojson:
                    count_download_geojson = '0'
                count_download_geojson = int(count_download_geojson) + 1
                payload = {'count_download_geojson': int(count_download_geojson)}
                mapset['count_download_geojson'] = count_download_geojson
            where = {'id': int(_id)}
            res, data1 = MAPSET_CONTROLLER.update(where, payload, False)
        except Exception as err:
            pass

        # response
        if res:
            # success response
            response = {'message': 'Get detail data successfull', "error": 0, "data": mapset}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/mapset/statistik", methods=["GET"])
# @jwt_check()
def statistik():
    ''' Doc: function statistik'''
    try:
        res, res_statistik = MAPSET_CONTROLLER.get_statistik()

        # response
        if res:
            # success response
            response = {'message': 'Get detail data successfull', "error": 0, "data": res_statistik}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {'message': conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/mapset/geoserver_upload", methods=["POST"])
@jwt_check()
def geoserver_upload():
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if 'mapset_wms_url' in json_request:

            # default data
            data = {
                'mapset_wms_url': json_request['mapset_wms_url'],
                'mapset_json_url': '',
                'mapset_shp_url': '',
                'metadata_kugi': [],
                'shp_upload_url': ''
            }

            # parsing url
            url_parsed = urllib.parse.urlparse(data['mapset_wms_url'])
            url_only = url_parsed._replace(query=None).geturl()
            url_only_split = url_only.split('/')
            params = urllib.parse.parse_qs(url_parsed.query)

            # get metadata kugi
            data['mapset_json_url'] = '/'.join(url_only_split[:-1]) + '/ows' + '?service=WFS' + '&version=1.0.0' + '&request=GetFeature'
            data['mapset_json_url'] += '&typeName=' + params['layers'][0] + '&outputFormat=application/json'
            
            credentials = f"{conf.GEOSERVER_USERNAME}:{conf.GEOSERVER_PASSWORD}"
            encoded_credentials = base64.b64encode(credentials.encode('ascii')).decode('ascii')

            try:
                result_json = requests.get(data['mapset_json_url'], headers={
                "Authorization": f"Basic {encoded_credentials}",
                "Content-Type": conf.MESSAGE_APP_JSON
                })
                result_json = result_json.json()
                metadata = result_json['features'][0]['properties']
                data['metadata_kugi'] = []
                for attr, value in metadata.items():
                    result_metadata = db.session.query(MapsetMetadataKugiModel)
                    result_metadata = result_metadata.filter(func.lower(MapsetMetadataKugiModel.ptMemberName) == func.lower(attr)).limit(1).all()
                    if result_metadata:
                        for res_md in result_metadata:
                            temp = res_md.__dict__
                            data['metadata_kugi'].append({'key': attr, 'value': temp['ptDefinition']})
                    else:
                        data['metadata_kugi'].append({'key': attr, 'value': ''})

            except Exception as err:
                print(str(err))

            # upload shp
            data['mapset_shp_url'] = '/'.join(url_only_split[:-1]) + '/ows' + '?service=WFS' + '&version=1.0.0' + '&request=GetFeature'
            data['mapset_shp_url'] += '&typeName=' + params['layers'][0] + '&outputFormat=SHAPE-ZIP'

            data['shp_upload_url'] = data['mapset_shp_url']

            # try:
            #     next_id = UPLOAD_CONTROLLER.get_next_id()
            #     fileext = '.zip'
            #     filename = params['layers'][0].replace(':', '-')
            #     filepath = conf.MESSAGE_UPLOAD_PATH.replace('temp/', '') + str(next_id) + '_' + filename + fileext
            #     urllib.request.urlretrieve(data['mapset_shp_url'], filepath)
            #     filesize = os.path.getsize(filepath)
            #     filehash = sha256((str(next_id) + '_' + filename + fileext).encode('ascii')).hexdigest()

            #     # prepare json data
            #     jwt = HELPER.read_jwt()
            #     json_request = {}
            #     json_request['name'] = filename
            #     json_request['type'] = ''
            #     json_request['ext'] = fileext
            #     json_request['size'] = filesize
            #     json_request['hash'] = filehash
            #     json_request['url'] = filepath
            #     json_request['cuid'] = jwt['id']
            #     json_request['cdate'] = HELPER.local_date_server()

            #     data['shp_upload_url'] = filepath

            #     # insert upload
            #     res, upload = UPLOAD_CONTROLLER.create(json_request)

            # except Exception as err:
            #     print(err)

            response = {'message': 'Upload & Get metadata successfull', "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

        else:
            # success response but no data
            response = {'message': 'mapset_wms_url not found ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/mapset/arcgis_upload", methods=["POST"])
@jwt_check()
def arcgis_upload():
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if 'mapset_wms_url' in json_request:

            # default data
            data = {
                'mapset_wms_url': json_request['mapset_wms_url'],
                'mapset_json_url': '',
                'mapset_shp_url': '',
                'metadata_kugi': [],
                'shp_upload_url': ''
            }

            # url json
            if '?f=pjson' in json_request['mapset_wms_url']:
                data['mapset_json_url'] = json_request['mapset_wms_url']
            else:
                data['mapset_json_url'] = json_request['mapset_wms_url'] + '?f=pjson'

            # metadata
            try:
                get_json_url = "https://geojson.klikdesaku.id/get_json_from_url"
                payload = json.dumps({'url': data['mapset_json_url']})
                headers = {'Content-Type': 'application/json'}

                req = requests.post(get_json_url, headers=headers, data=payload)
                res = req.json()

                metadata = res['data']['fields']
                data['metadata_kugi'] = []
                for md in metadata:
                    # parsing
                    attr = ''
                    value_arcgis = ''
                    for key, value in md.items():
                        if key == 'name':
                            attr = value
                        if key == 'alias':
                            value_arcgis = value

                    # get metadata kugi
                    result_metadata = db.session.query(MapsetMetadataKugiModel)
                    result_metadata = result_metadata.filter(func.lower(MapsetMetadataKugiModel.ptMemberName) == func.lower(attr)).limit(1).all()
                    if result_metadata:
                        for res_md in result_metadata:
                            temp = res_md.__dict__
                            data['metadata_kugi'].append({'key': attr, 'value': temp['ptDefinition'], 'value_arcgis': value_arcgis})
                    else:
                        data['metadata_kugi'].append({'key': attr, 'value': '', 'value_arcgis': value_arcgis})

            except Exception as err:
                print(str(err))

            response = {'message': 'Upload & Get metadata successfull', "error": 0, "data": data}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

        else:
            # success response but no data
            response = {'message': 'mapset_wms_url not found ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/mapset/highlight", methods=["POST"])
# @jwt_check()
def mapset_highlight():
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        if 'type' in json_request and 'url' in json_request:

            if json_request['type'] == 'geoserver':
                # type geoserver
                res, datas = MAPSET_CONTROLLER.get_highlight_geoserver(json_request['url'])
                response = {'message': 'Get insight geoserver successfull ', 'error': 0, 'data': datas}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

            elif json_request['type'] == 'arcgis':
                # type arcgis
                # res, datas = MAPSET_CONTROLLER.get_highlight_arcgis(json_request['url'])
                res = False
                datas = {}
                response = {'message': 'Get insight arcgis successfull ', 'error': 0, 'data': datas}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

            else:
                # success response but no data
                response = {'message': 'wrong type url', 'error': 1, 'data': {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

        else:
            # success response but no data
            response = {'message': 'url not found ', 'error': 1, 'data': {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

    except Exception as err:
        # fail response
        response = {'message': conf.MESSAGE_INTERNAL_ERROR + str(err), 'error': 1, 'data': {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
