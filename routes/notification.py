''' Doc: route notification'''
import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers import Helper
from settings import configuration
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from models import UserModel
from helpers.postgre_alchemy import postgre_alchemy as db
import requests
from controllers import NotificationController
from controllers import ReviewController
from controllers import DatasetController
from controllers import UserController
from settings import configuration as conf
import math

HELPER = Helper()
BP = Blueprint('notification', __name__)
NOTIFICATION_CONTROLLER = NotificationController()
REVIEW_CONTROLLER = ReviewController()
DATASET_CONTROLLER = DatasetController()
USER_CONTROLLER = UserController()
# pylint: disable=broad-except, line-too-long, unused-variable,

@BP.route("/notification", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, notification = NOTIFICATION_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, notification = NOTIFICATION_CONTROLLER.get_all(where, search, sort, limit, skip)

        # pagination
        meta = {}
        res_count, count = NOTIFICATION_CONTROLLER.get_count(where, search)
        meta["skip"] = int(skip)
        meta["limit"] = int(limit)
        meta["total_record"] = int(count["count"])
        if int(limit) > 0:
            meta["total_page"] = math.ceil(int(count["count"]) / int(limit))
        else:
            meta["total_page"] = 0

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": notification, "meta": meta}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": [], "meta": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": [], "meta": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        # call function get by id
        res, notification = NOTIFICATION_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": "Get detail data successfull", "error": 0, "data": notification}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert notification
        res, notification = NOTIFICATION_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": notification}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed, ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification", methods=["PUT"])
@jwt_check()
def update_read():
    ''' Doc: function update read'''
    try:
        # filter data
        types = request.args.get("type", "dataset")
        type_id = request.args.get("type_id", 0)
        receiver = request.args.get("receiver", 0)

        # prepare json data
        json_request = request.get_json(silent=True)

        # update notification
        res, notification = NOTIFICATION_CONTROLLER.update_read(types, type_id, receiver, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": notification}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update notification
        res, notification = NOTIFICATION_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": notification}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete notification
        res = NOTIFICATION_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification/trigger/<dataset_id>", methods=["GET"])
@jwt_check()
def trigger(dataset_id):
    ''' Doc: function trigger'''
    token = request.headers.get('Authorization', None)
    jwt = HELPER.read_jwt()
    # check review
    # print('check review')
    try:
        param = {}
        param['type'] = 'dataset'
        param['type_id'] = dataset_id
        param['user_id'] = jwt['id']
        res, count = REVIEW_CONTROLLER.get_count(param, '')

        if count['count'] < 1:

            # post notification
            # print('send notif')
            try:
                res, dataset, message = DATASET_CONTROLLER.get_by_id_table(dataset_id, {})

                if res:
                    payload = {}
                    payload['feature'] = 1
                    payload['type'] = 'dataset'
                    payload['type_id'] = dataset_id
                    payload['receiver'] = jwt['id']
                    payload['title'] = dataset['name']
                    payload['content'] = ' menunggu untuk Anda beri ulasan.'
                    payload['is_read'] = False
                    res2, notif = NOTIFICATION_CONTROLLER.create(payload)

                    # sent to socket
                    try:
                        socket_url = configuration.SOCKET_URL
                        req = requests.get(
                            socket_url + 'notification' + '?receiver=' + str(notif['receiver']),
                            headers={"Authorization":token, "Content-Type": "application/json"},
                            data=json.dumps(notif)
                        )
                        response = req.json()
                    except Exception as err:
                        response = {}
                        response['error'] = 0
                        response['message'] = 'Notification already sent, but socket not response'
                        response['data'] = {}

                    # update notif
                    param = {}
                    param['count_notif'] = 0
                    res, user = USER_CONTROLLER.get_by_id(jwt['id'])
                    if res:
                        if not user['count_notif']:
                            user['count_notif'] = 0
                        param['count_notif'] = user['count_notif'] + 1
                        # execute database
                        result = db.session.query(UserModel)
                        result = result.filter(getattr(UserModel, 'id') == jwt['id'])
                        result = result.update(param, synchronize_session='fetch')
                        result = db.session.commit()
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

                else:
                    response = {"message": conf.MESSAGE_INTERNAL_ERROR + 'Dataset not found.', "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


            except Exception as err:
                # fail response
                response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

        else:
            response = {}
            response['error'] = 0
            response['message'] = 'Notification already sent'
            response['data'] = {}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/notification/clear", methods=["GET"])
@jwt_check()
def clear():
    ''' Doc: function clear'''
    jwt = HELPER.read_jwt()

    try:
        param = {}
        param['count_notif'] = 0

        # execute database
        result = db.session.query(UserModel)
        result = result.filter(getattr(UserModel, 'id') == jwt['id'])
        result = result.update(param, synchronize_session='fetch')
        result = db.session.commit()
        res, user = USER_CONTROLLER.get_by_id(jwt['id'])

        if res:
            response = {}
            response['error'] = 0
            response['message'] = 'Notification clear'
            response['data'] = {}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

        else:
            response = {}
            response['error'] = 0
            response['message'] = 'Notification already clear'
            response['data'] = {}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
