''' Doc: route feedback private'''
import json
from flask import request, Blueprint, Response
from flask import send_file
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import FeedbackPrivateController
from settings import configuration as conf

BP = Blueprint('feedback_private', __name__)
FEEDBACK_PRIVATE_CONTROLLER = FeedbackPrivateController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long


@BP.route("/feedback_private", methods=["GET"])
@jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:ASC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, feedback_private = FEEDBACK_PRIVATE_CONTROLLER.get_count(
                where, search, start_date, end_date)
        else:
            # call function get
            res, feedback_private = FEEDBACK_PRIVATE_CONTROLLER.get_all(
                where, search, sort, limit, skip, start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/<_id>", methods=["GET"])
@jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, feedback_private, message = FEEDBACK_PRIVATE_CONTROLLER.get_by_id(
            _id)

        # response
        if res:
            # success response
            response = {"message": message,
                        "error": 0, "data": feedback_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert history draft
        res, feedback_private = FEEDBACK_PRIVATE_CONTROLLER.create(
            json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull",
                        "error": 0, "data": feedback_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update history draft
        res, feedback_private = FEEDBACK_PRIVATE_CONTROLLER.update(
            where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull",
                        "error": 0, "data": feedback_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete history draft
        res = FEEDBACK_PRIVATE_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull",
                        "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed",
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/info", methods=["GET"])
@jwt_check()
def get_info():
    ''' Doc: function get info'''
    try:
        res = True
        feedback_private = {}
        feedback_private['tujuan'] = []
        feedback_private['masalah'] = []

        feedback_private['tujuan'].append(
            {'nama': 'Mencari data Pemdaprov Jawa Barat untuk kepentingan pelaksanaan program/ kegiatan atau perumusan kebijakan.'})
        feedback_private['tujuan'].append(
            {'nama': 'Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu.'})
        feedback_private['tujuan'].append(
            {'nama': 'Mempelajari lebih lanjut terkait data yang dimiliki oleh organisasi perangkat daerah di Pemdaprov Jawa Barat.'})
        feedback_private['tujuan'].append(
            {'nama': 'Lainnya (tulis secara spesifik).'})

        feedback_private['masalah'].append(
            {'nama': 'Halaman tidak dapat diakses'})
        feedback_private['masalah'].append(
            {'nama': 'Informasi dirasa terlalu membingungkan'})
        feedback_private['masalah'].append(
            {'nama': 'Informasi yang dicari tidak dapat ditemukan'})
        feedback_private['masalah'].append(
            {'nama': 'Tidak tau jika informasi yang tersedia akurat atau terbaru'})
        feedback_private['masalah'].append(
            {'nama': 'Lainnya (tulis secara spesifik)'})

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback_private}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_total", methods=["GET"])
@jwt_check()
def get_chart_total():
    ''' Doc: function get chart total'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_total(
            start_date, end_date)

        # response
        if res:
            # perhitungan
            result = {}
            result['total'] = feedback

            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_score", methods=["GET"])
@jwt_check()
def get_chart_score():
    ''' Doc: function get chart score'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_score(
            start_date, end_date)

        # response
        if res:
            # perhitungan
            result = {}
            result['detail'] = feedback
            result['count'] = 0
            result['max'] = 0
            result['total'] = 0
            result['average'] = 0

            # perhitungan
            for fb_ in feedback:
                result['count'] += fb_['count']
                result['total'] += fb_['score'] * fb_['count']

            result['max'] = result['count'] * 4
            result['average'] = round(
                (result['total'] / result['max']) * 100, 2)

            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": result}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_tujuan", methods=["GET"])
@jwt_check()
def get_chart_tujuan():
    ''' Doc: function get chart tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_tujuan(
            start_date, end_date)

        feedbacks = []
        feedbacks.append(
            {"count": 0, "tujuan": "Mencari data Pemdaprov Jawa Barat untuk kepentingan pelaksanaan program/ kegiatan atau perumusan kebijakan."})
        feedbacks.append(
            {"count": 0, "tujuan": "Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu."})
        feedbacks.append(
            {"count": 0, "tujuan": "Mempelajari lebih lanjut terkait data yang dimiliki oleh organisasi perangkat daerah di Pemdaprov Jawa Barat."})
        feedbacks.append({"count": 0, "tujuan": "Lainnya"})

        for fb_ in feedback:
            if fb_['tujuan'] == "Mencari data Pemdaprov Jawa Barat untuk kepentingan pelaksanaan program/ kegiatan atau perumusan kebijakan.":
                feedbacks[0]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mencari data untuk membuktikan kebenaran atas sebuah isu tertentu.":
                feedbacks[1]['count'] = fb_['count']
            elif fb_['tujuan'] == "Mempelajari lebih lanjut terkait data yang dimiliki oleh organisasi perangkat daerah di Pemdaprov Jawa Barat.":
                feedbacks[2]['count'] = fb_['count']
            else:
                feedbacks[3]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedbacks}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_tujuan_tercapai", methods=["GET"])
@jwt_check()
def get_chart_tujuan_tercapai():
    ''' Doc: function get chart tujuan tercapai'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_tujuan_tercapai(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_tujuan_mudah_ditemukan", methods=["GET"])
@jwt_check()
def get_chart_tujuan_mudah_ditemukan():
    ''' Doc: function get chart tujuan mudah ditemukan'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_tujuan_mudah_ditemukan(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_masalah", methods=["GET"])
@jwt_check()
def get_chart_masalah():
    ''' Doc: function get chart masalah'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_masalah(
            start_date, end_date)

        feedbacks = []
        feedbacks.append(
            {"count": 0, "masalah": "Halaman tidak dapat diakses"})
        feedbacks.append(
            {"count": 0, "masalah": "Informasi dirasa terlalu membingungkan"})
        feedbacks.append(
            {"count": 0, "masalah": "Informasi yang dicari tidak dapat ditemukan"})
        feedbacks.append(
            {"count": 0, "masalah": "Tidak tau jika informasi yang tersedia akurat atau terbaru"})
        feedbacks.append({"count": 0, "masalah": "Lainnya"})

        for fb_ in feedback:
            if fb_['masalah'] == "Halaman tidak dapat diakses":
                feedbacks[0]['count'] = fb_['count']
            elif fb_['masalah'] == "Informasi dirasa terlalu membingungkan":
                feedbacks[1]['count'] = fb_['count']
            elif fb_['masalah'] == "Informasi yang dicari tidak dapat ditemukan":
                feedbacks[2]['count'] = fb_['count']
            elif fb_['masalah'] == "Tidak tau jika informasi yang tersedia akurat atau terbaru":
                feedbacks[3]['count'] = fb_['count']
            else:
                feedbacks[4]['count'] += fb_['count']

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedbacks}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/chart_harian", methods=["GET"])
@jwt_check()
def get_chart_harian():
    ''' Doc: function get chart harian'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_chart_harian(
            start_date, end_date)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS,
                        "error": 0, "data": feedback}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/feedback_private/download", methods=["GET"])
@jwt_check()
def get_download():
    ''' Doc: function get download'''
    try:
        # filter data
        start_date = request.args.get("start_date", "")
        end_date = request.args.get("end_date", "")

        res, feedback = FEEDBACK_PRIVATE_CONTROLLER.get_download(
            start_date, end_date)
        metadata = ["Id", "Datetime", "Score", "Tujuan", "Tujuan Tercapai",
                    "Tujuan Mudah Ditemukan", "Masalah", "Saran", "is_active", "is_deleted"]

        # response
        if res:
            # success response
            filename = HELPER.convert_xls(
                'static/download/feedback_private.xlsx', feedback, metadata)
            return send_file(
                '../' + filename,
                mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                attachment_filename='feedback_private.xlsx',
                as_attachment=True
            )
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND,
                        "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR +
                    str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
