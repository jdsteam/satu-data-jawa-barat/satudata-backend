from helpers.postgre_alchemy import postgre_alchemy as db
from models.regional_integration import RegionalIntegrationModel
from flask import Blueprint, jsonify, request
from helpers.decorator import jwt_check

BP = Blueprint("regional_integration", __name__)

@BP.route("/dcat-regional.json", methods=["GET"])
def get_all_regional_integration():
    regional_integration = RegionalIntegrationModel.query.order_by(RegionalIntegrationModel.kode_wilayah).all()
    regional_integration = [{"id": regional.id, "kode_wilayah": regional.kode_wilayah,"nama_wilayah": regional.nama_wilayah, "endpoint": regional.endpoint} for regional in regional_integration]
    return jsonify(regional_integration), 200


@BP.route("/dcat-regional.json", methods=["POST"])
@jwt_check()
def create_regional_integration():
    data = request.get_json()
    if not data:
        return jsonify({"message": "No input data provided"}), 400
    elif not data.get("kode_wilayah"):
        return jsonify({"message": "kode_wilayah is required"}), 400
    elif not data.get("nama_wilayah"):
        return jsonify({"message": "nama_wilayah is required"}), 400
    elif not data.get("endpoint"):
        return jsonify({"message": "endpoint is required"}), 400
    elif not isinstance(data["nama_wilayah"], str) and not isinstance(data["endpoint"], str):
        return jsonify({"message": "nama_wilayah and endpoint must be string"}), 400
    elif len(data["nama_wilayah"]) > 255 or len(data["endpoint"]) > 255:
        return jsonify({"message": "nama_wilayah and endpoint must be less than 255 characters"}), 400
    regional_integration = RegionalIntegrationModel(**data)
    db.session.add(regional_integration)
    db.session.commit()
    regional_integration = {"id": regional_integration.id, "nama_wilayah": regional_integration.nama_wilayah, "endpoint": regional_integration.endpoint}
    return jsonify(regional_integration,), 201

@BP.route("/dcat-regional.json/<int:id>", methods=["PUT"])
@jwt_check()
def update_regional_integration(id):
    data = request.get_json()
    if not data:
        return jsonify({"message": "No input data provided"}), 400
    if db.session.query(RegionalIntegrationModel).filter(RegionalIntegrationModel.id == id).first():
        return jsonify({"message": "Regional Integration already exists"}), 400
    db.session.query(RegionalIntegrationModel).filter(RegionalIntegrationModel.id == id).update(data)
    db.session.commit()
    regional_integration = {"id": regional_integration.id, "nama_wilayah": regional_integration.nama_wilayah, "endpoint": regional_integration.endpoint}
    return jsonify(regional_integration), 200

@BP.route("/dcat-regional.json/<int:id>", methods=["DELETE"])
@jwt_check()
def delete_regional_integration(id):
    regional_integration = RegionalIntegrationModel.query.get(id)
    if not regional_integration:
        return jsonify({"message": "Regional Integration not found"}), 404
    db.session.delete(regional_integration)
    db.session.commit()
    return jsonify({"message": "Regional Integration deleted"}), 200