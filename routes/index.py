''' Doc: route index'''
from flask import Blueprint
from flask import jsonify
from settings import configuration as conf

BP = Blueprint('index', __name__ )

@BP.route("/is_alive", methods=["GET"])
def is_alive():
    ''' Doc: function delete'''
    # success response format
    response = {"error": 0, "message": "Connected", "data": []}
    return jsonify(response)
