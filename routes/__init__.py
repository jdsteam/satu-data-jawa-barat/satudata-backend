''' Doc: router init  '''

# pylint: disable=redefined-builtin
from . import (
    index, error, sektoral, regional, regional_level,
    skpd, skpdsub, skpdunit,
    dataset_class, dataset_type, dataset_tag, dataset,
    mapset_type, mapset_source, mapset, metadata_type, metadata,
    license, notification, review, history, search,
    dashboard, upload,
    visualization, visualization_dataset, visualization_access, metadata_visualization,
    history_login, history_draft, history_request, request,
    indikator_category, indikator_class, indikator_history, indikator, indikator_progress,
    satuan, infographic, infographic_detail, highlight, history_download,
    feedback, feedback_masalah, mapset_metadata, mapset_metadata_type,
    request_public, history_request_public, request_private, history_request_private,
    feedback_private, feedback_map, feedback_map_masalah,
    agreement, app_service, app, auth, role, user_app_role, user_permission, user,
    struktur_organisasi, bigdata, geojson, article_topic, article, refresh, jabatan,
    mapset_program, dashboard_feedback, dashboard_feedback_private, dashboard_feedback_map,
    dashboard_history_download, dashboard_request_private, dashboard_request_public, mapset_law, mapset_award, mapset_story,
    bidang_urusan, mapset_thematic, nps, dashboard_nps, csat, dashboard_csat, mapset_area_coverage, regional_integration,
    bidang_urusan_skpd, bidang_urusan_indikator, bidang_urusan_indikator_skpd,history_ewalidata_assignment, ewalidata_data,
    indikator_datadasar, mapset_webgis, mapset_thematic_story_search, changelog
)

__all__ = [
    index, error, sektoral, regional, regional_level,
    skpd, skpdsub, skpdunit,
    dataset_class, dataset_type, dataset_tag, dataset,
    mapset_type, mapset_source, mapset, metadata_type, metadata,
    license, notification, review, history, search,
    dashboard, upload,
    visualization, visualization_dataset, visualization_access, metadata_visualization,
    history_login, history_draft, history_request, request,
    indikator_category, indikator_class, indikator_history, indikator, indikator_progress,
    satuan, infographic, infographic_detail, highlight, history_download,
    feedback, feedback_masalah, mapset_metadata, mapset_metadata_type,
    request_public, history_request_public, request_private, history_request_private,
    feedback_private, feedback_map, feedback_map_masalah,
    agreement, app_service, app, auth, role, user_app_role, user_permission, user,
    struktur_organisasi, bigdata, geojson, article_topic, article, refresh, jabatan,
    mapset_program, dashboard_feedback, dashboard_feedback_private, dashboard_feedback_map,
    dashboard_history_download, dashboard_request_private, dashboard_request_public, mapset_law, mapset_award, mapset_story,
    bidang_urusan, mapset_thematic, nps, dashboard_nps, csat, dashboard_csat, mapset_area_coverage, regional_integration,
    bidang_urusan_skpd, bidang_urusan_indikator, bidang_urusan_indikator_skpd, history_ewalidata_assignment, ewalidata_data,
    indikator_datadasar, mapset_webgis, mapset_thematic_story_search, changelog
]
