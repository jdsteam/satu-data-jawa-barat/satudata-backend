""" Doc: route dataset"""
import json
import ast
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from models import SkpdModel
from helpers.postgre_alchemy import postgre_alchemy as db
import helpers.postgre_psycopg_bigdata as db2
import math
from settings import configuration as conf

from controllers import DatasetController
from controllers import HistoryController
from controllers import HistoryDraftController
from controllers import NotificationController
from controllers import SearchController
from controllers import SkpdController
from controllers import UserController
from controllers import MetadataController
from controllers import DatasetTagController
from controllers import BigdataController
from controllers import HighlightController
from controllers import DatasetQualityScoreController
from controllers.bigdata import create_table, validate_data_row

from models import DatasetModel
from helpers.autocorecction import AutoCorecction
from helpers import executor
from flask import render_template

BP = Blueprint('dataset', __name__)
USER_CONTROLLER = UserController()
DATASET_CONTROLLER = DatasetController()
HISTORY_CONTROLLER = HistoryController()
HISTORY_DRAFT_CONTROLLER = HistoryDraftController()
NOTIFICATION_CONTROLLER = NotificationController()
SEARCH_CONTROLLER = SearchController()
SKPD_CONTROLLER = SkpdController()
METADATA_CONTROLLER = MetadataController()
DATASET_TAG_CONTROLLER = DatasetTagController()
BIGDATA_CONTROLLER = BigdataController()
HIGHLIGHT_CONTROLLER = HighlightController()
DATASET_QUALITY_SCORE_CONTROLLER = DatasetQualityScoreController()
HELPER = Helper()
MESSAGE_RECEIVER = "?receiver="
MESSAGE_MEMBUTUHKAN_VERIFIKASI = "</b> membutuhkan verifikasi."
MESSAGE_DARI = " dari <b>"

# pylint: disable=singleton-comparison, broad-except, unused-variable, too-many-lines, pointless-statement, reimported


@BP.route("/dataset", methods=["GET"])
# @jwt_check()
def get_all():
    """Doc: function get all"""
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", "id:DESC")
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)
        suggestion = request.args.get("suggestion", "")

        # prepare data filter
        sort = sort.split(":")

        where_data = where
        where_data = where_data.replace("'", '"')
        where_data = json.loads(where_data)
        if 'tags' in where_data:
            where_data['tag'] = where_data['tags']
            where_data.pop("tags")

        where_count = where
        where_count = where_count.replace("'", '"')
        where_count = json.loads(where_count)
        if 'tags' in where_count:
            where_count['tag'] = where_count['tags']
            where_count.pop("tags")

        correction_data = {}
        correction_data["status"] = False
        correction_data["suggestion_text"] = ""
        correction_data["original_text"] = search
        if search and suggestion == "on":
            status, data_corecction = AutoCorecction.dataset(search)
            correction_data["status"] = status
            correction_data["suggestion_text"] = data_corecction
            correction_data["original_text"] = search
            search = data_corecction
        else:
            search = search

        # check public or private
        jwt = HELPER.read_jwt()
        if not jwt:
            if count:
                # call function get
                res, dataset = DATASET_CONTROLLER.get_count(where_count, search)
            else:
                # call function get
                res, dataset = DATASET_CONTROLLER.get_all(
                    where_data, search, sort, limit, skip
                )
        else:
            if count:
                # call function get
                res, dataset = DATASET_CONTROLLER.get_count_table(where_count, search)
            else:
                # call function get
                res, dataset = DATASET_CONTROLLER.get_all_table(
                    where_data, search, sort, limit, skip
                )

        # if search:
        #     try:
        #         jwt = HELPER.read_jwt()
        #         payload = {}
        #         if jwt:
        #             payload['user_id'] = jwt['id']
        #         else:
        #             payload['user_id'] = '0'
        #         payload['search'] = search
        #         res2, data2 = SEARCH_CONTROLLER.create(payload)
        #     except Exception as err:
        #         pass

        meta = {}
        if not jwt:
            res_count, count = DATASET_CONTROLLER.get_count(where_count, search)
        else:
            res_count, count = DATASET_CONTROLLER.get_count_table(where_count, search)
        meta["skip"] = int(skip)
        meta["limit"] = int(limit)
        meta["total_record"] = int(count["count"])
        if int(limit) > 0:
            meta["total_page"] = math.ceil(int(count["count"]) / int(limit))
        else:
            meta["total_page"] = 0

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS,
                "error": 0,
                "data": dataset,
                "meta": meta,
                "suggestion": correction_data["status"],
                "suggestion_text": correction_data["suggestion_text"],
                "original_text": correction_data["original_text"],
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND,
                "error": 1,
                "data": [],
                "meta": {},
                "suggestion": correction_data["status"],
                "suggestion_text": correction_data["suggestion_text"],
                "original_text": correction_data["original_text"],
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": [],
            "meta": {},
            "suggestion": correction_data["status"],
            "suggestion_text": correction_data["suggestion_text"],
            "original_text": correction_data["original_text"],
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    """Doc: function get by id"""
    try:
        # filter data
        review = request.args.get("review", False)
        where = request.args.get("where", "{}")

        where_data = where
        where_data = where_data.replace("'", '"')
        where_data = json.loads(where_data)

        if review:
            # call function get by id
            res, dataset, message = DATASET_CONTROLLER.review(_id)
        else:
            # call function get by id
            jwt = HELPER.read_jwt()
            if not jwt:
                res, dataset, message = DATASET_CONTROLLER.get_by_id(_id, where_data)
            else:
                res, dataset, message = DATASET_CONTROLLER.get_by_id_table(_id, where_data)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": dataset}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/counter/<_id>", methods=["GET"])
# @jwt_check()
def counter(_id):
    """Doc: function counter"""
    # category
    category = request.args.get("category", "")

    # handle id=title, integer or string
    try:
        temp = int(_id)
        _id = temp
    except Exception as err:
        temp = _id
        result = db.session.query(DatasetModel.id)
        result = result.filter(DatasetModel.title == temp)
        result = result.all()
        if result:
            result = result[0]
            _id = result[0]
        else:
            _id = 0

    try:
        res, dataset, message = DATASET_CONTROLLER.get_by_id_table(_id, {})

        # category counter
        try:
            dataset, payload = DATASET_CONTROLLER.switch(category, dataset)
            where = {"id": int(_id)}
            res, data1 = DATASET_CONTROLLER.update(where, payload)
        except Exception as err:
            pass

        # insert to history
        try:
            jwt = HELPER.read_jwt()
            if "view" in category:
                payload = {}
                payload["type"] = "dataset"
                payload["type_id"] = _id
                payload["user_id"] = jwt["id"]
                payload["category"] = "view"
                res2, data2 = HISTORY_CONTROLLER.create(payload)
            elif "access" in category:
                payload = {}
                payload["type"] = "dataset"
                payload["type_id"] = _id
                payload["user_id"] = jwt["id"]
                payload["category"] = "access"
                res2, data2 = HISTORY_CONTROLLER.create(payload)
        except Exception as err:
            pass

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": dataset}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset", methods=["POST"])
@jwt_check()
def create():
    """Doc: function create"""
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # temporary variable
        temp_json = {}
        temp_json["data"] = []
        temp_json["metadata"] = []
        temp_metadata = []
        temp_tag = []
        temp_user_permission = []
        temp_jabatan_access = []
        temp_update = {}
        temp_history_draft = {}

        if "json" in json_request:
            temp_json = json_request["json"]
            del json_request["json"]
        if "metadata" in json_request:
            temp_metadata = json_request["metadata"]
            del json_request["metadata"]
        if "tag" in json_request:
            temp_tag = json_request["tag"]
            del json_request["tag"]
        if "user_permission_access" in json_request:
            temp_user_permission = json_request["user_permission_access"]
            del json_request["user_permission_access"]
        if "is_realtime" in json_request:
            if json_request['is_realtime'] == True:
                pass
            else:
                if "kode_skpd" in json_request:
                    res_bigdata, data_bigdata = SKPD_CONTROLLER.get_all({"kode_skpd": json_request["kode_skpd"]}, "", ["id", "desc"], 1, 0)
                    json_request["schema"] = data_bigdata[0]["nama_skpd_alias"]
        else:
            if "kode_skpd" in json_request:
                res_bigdata, data_bigdata = SKPD_CONTROLLER.get_all({"kode_skpd": json_request["kode_skpd"]}, "", ["id", "desc"], 1, 0)
                json_request["schema"] = data_bigdata[0]["nama_skpd_alias"]
        if "jabatan_access" in json_request:
            temp_jabatan_access = json_request["jabatan_access"]
            del json_request["jabatan_access"]
        if "history_draft" in json_request:
            temp_history_draft = json_request["history_draft"]
            del json_request["history_draft"]

        # insert dataset
        res, dataset = DATASET_CONTROLLER.create(json_request)

        if "table" not in json_request:
            if not temp_json["data"]:
                temp_json["data"] = [{"id": "1"}]
                temp_json["metadata"] = ["id"]

            # create column
            temp_json["data"], temp_json["metadata"] = BIGDATA_CONTROLLER.create_column(
                temp_json["data"], temp_json["metadata"]
            )

            # create json
            temp_update["json"] = BIGDATA_CONTROLLER.create_json(
                temp_json["data"], temp_json["metadata"], dataset["id"]
            )

            # create table
            json_request["table"] = BIGDATA_CONTROLLER.create_dataset(
                dataset["id"], dataset["name"]
            )
            
            # BIGDATA_CONTROLLER.create_table(
            #     temp_json["data"],
            #     temp_json["metadata"],
            #     json_request["schema"],
            #     json_request["table"],
            # )
            
            data = validate_data_row(temp_json["data"])
            create_table(
                data,
                json_request["schema"],
                json_request["table"]
            )
            DATASET_CONTROLLER.update(
                {"id": dataset["id"]}, {"table": json_request["table"]}
            )

            # update dataset
            res, dataset = DATASET_CONTROLLER.update(
                {"id": dataset["id"]},
                {
                    "json": temp_update["json"],
                    "count_column": len(temp_json["metadata"]),
                    "count_row": len(data),
                },
            )

            # create quality score
            # (
            #     res_score,
            #     quality_score,
            #     msg_score,
            # ) = DATASET_QUALITY_SCORE_CONTROLLER.create_quality_score(
            #     dataset, data, temp_metadata
            # )
            executor.submit(DATASET_QUALITY_SCORE_CONTROLLER.create_quality_score, dataset, data, temp_metadata)
            

        if temp_jabatan_access:
            DATASET_CONTROLLER.route_create_jabatan_access(dataset, temp_jabatan_access)
            # background_tasks.add_task(DATASET_CONTROLLER.route_create_jabatan_access, dataset, temp_jabatan_access)

        # create dataset tag
        DATASET_CONTROLLER.route_create_dataset_tag(temp_tag, dataset)
        # background_tasks.add_task(DATASET_CONTROLLER.route_create_dataset_tag, temp_tag, dataset)

        # create metadata
        DATASET_CONTROLLER.route_create_metadata(temp_metadata, dataset)
        # background_tasks.add_task(DATASET_CONTROLLER.route_create_metadata, temp_metadata, dataset)

        # insert to history draft
        if temp_history_draft:
            DATASET_CONTROLLER.route_insert_history_draft(temp_history_draft, dataset)
            # background_tasks.add_task(DATASET_CONTROLLER.route_insert_history_draft, temp_history_draft, dataset)

        # get dataset after update

        # update count_dataset
        # DATASET_CONTROLLER.route_create_count_dataset(json_request)
        executor.submit(DATASET_CONTROLLER.route_create_count_dataset, json_request)
        
        res, dataset, message = DATASET_CONTROLLER.get_by_id_table(dataset["id"], {})
        

        # insert to history
        DATASET_CONTROLLER.route_insert_history(res, dataset, "create")
        # background_tasks.add_task(DATASET_CONTROLLER.route_insert_history, res, dataset, "create")

        # insert to notification
        # DATASET_CONTROLLER.route_insert_notification(res, json_request, dataset)
        executor.submit(DATASET_CONTROLLER.route_insert_notification, res, json_request, dataset)
        

        
        

        # response
        if res:
            # success response
            response = {
                "message": "Create data successfull",
                "error": 0,
                "data": dataset,
            }
            # BIGDATA_CONTROLLER.force_restart()
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/<string:_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    """Doc: function update"""
    try:
        param_from = request.args.get("from", "")
        json_request = request.get_json(silent=True)

        # Ambil data dataset yang ada
        res, dataset, message = DATASET_CONTROLLER.get_by_id_table(_id, {})
        if not res:
            return Response(
                json.dumps({"message": message, "error": 1, "data": {}}, cls=JSONEncoder),
                mimetype=conf.MESSAGE_APP_JSON,
            ), 404

        # Inisialisasi variabel sementara
        temp_json = json_request.pop("json", {})
        temp_metadata = json_request.pop("metadata", [])
        temp_tag = json_request.pop("tag", [])
        temp_user_permission = json_request.pop("user_permission_access", [])
        temp_jabatan_access = json_request.pop("jabatan_access", [])
        temp_history_draft = json_request.pop("history_draft", {})
        
        if json_request.get("is_realtime") is not True and "kode_skpd" in json_request:
            _, data_bigdata = SKPD_CONTROLLER.get_all(
                {"kode_skpd": json_request["kode_skpd"]}, "", ["id", "desc"], 1, 0
            )
            json_request["schema"] = data_bigdata[0]["nama_skpd_alias"]

        # Proses pembaruan data berdasarkan request
        old_skpd = dataset["skpd"]["kode_skpd"]
        new_skpd = json_request.get("kode_skpd", old_skpd)
        json_request["kode_skpd"] = new_skpd
        
       
        if json_request.get("is_validate") == 3:
            json_request["is_active"] = True

        # Update dataset
        res, dataset = DATASET_CONTROLLER.update({"id": _id}, json_request)

        # Proses BigData jika ada perubahan pada 'data'
        if temp_json:
            temp_data = temp_json.get("data", [])
            temp_metadata_from_json = temp_json.get("metadata", [])
            if temp_data and temp_metadata_from_json:
                temp_data, temp_metadata_from_json = BIGDATA_CONTROLLER.create_column(
                    temp_data, temp_metadata_from_json
                )
                temp_update_json = BIGDATA_CONTROLLER.create_json(
                    temp_data, temp_metadata_from_json, dataset["id"]
                )
                table_name = BIGDATA_CONTROLLER.create_dataset(dataset["id"], dataset["name"])
                _, new_table_data = create_table(temp_data, json_request.get('schema', dataset['schema']), table_name)


                res, dataset = DATASET_CONTROLLER.update(
                    {"id": dataset["id"]},
                    {
                        "json": temp_update_json,
                        "count_column": len(temp_metadata_from_json),
                        "count_row": len(temp_data),
                        "schema": json_request.get('schema', dataset['schema']),
                        "table": new_table_data,
                    },
                )
                executor.submit(DATASET_QUALITY_SCORE_CONTROLLER.create_quality_score, dataset, temp_data, temp_metadata_from_json)

        # Operasi-operasi terkait lainnya
        if temp_jabatan_access:
            DATASET_CONTROLLER.route_update_jabatan_access(dataset, temp_jabatan_access)
        if temp_tag:
            DATASET_CONTROLLER.route_update_dataset_tag(temp_tag, dataset)
        if temp_metadata:
            DATASET_CONTROLLER.route_update_metadata(temp_metadata, dataset)
        if temp_history_draft:
            DATASET_CONTROLLER.route_insert_history_draft(temp_history_draft, dataset)

        res, dataset, message = DATASET_CONTROLLER.get_by_id_table(dataset["id"], {})
        DATASET_CONTROLLER.route_insert_history(res, dataset, "update")
        
        executor.submit(DATASET_CONTROLLER.route_update_count_dataset, json_request, old_skpd, new_skpd)
        executor.submit(DATASET_CONTROLLER.route_insert_notification_update, res, json_request, dataset, param_from)

        # Trigger update highlight
        if "is_active" in json_request or "is_deleted" in json_request:
            where_hi = {"category": "dataset", "category_id": dataset["id"]}
            res_hi, highlight = HIGHLIGHT_CONTROLLER.get_all(
                where_hi, "", ["id", "asc"], 1, 0
            )
            if res_hi:
                HIGHLIGHT_CONTROLLER.update(
                    {"id": highlight[0]["id"]}, {"is_active": dataset["is_deleted"]}
                )

        # Response
        response = {
            "message": "Update data successfull" if res else "Update data failed",
            "error": 0 if res else 1,
            "data": dataset if res else {},
        }
        return Response(
            json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
        ), 200

    except Exception as err:
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": {},
        }
        return Response(
            json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
        ), 500


@BP.route("/dataset/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    """Doc: function delete"""
    try:
        # filter data
        where = {"id": _id}

        # get kode_skpd
        result, dataset, message = DATASET_CONTROLLER.get_by_id_table(_id, {})

        # delete dataset
        res = DATASET_CONTROLLER.delete(where)

        # update count_dataset
        if dataset["skpd"]["kode_skpd"]:
            # public only
            result, count = DATASET_CONTROLLER.get_count(
                {
                    "kode_skpd": dataset["skpd"]["kode_skpd"],
                    "is_deleted": False,
                    "is_active": True,
                    "dataset_class_id": 3,
                    "is_validate": 3,
                },
                "",
            )
            update_ = db.session.query(SkpdModel)
            update_ = update_.filter(
                getattr(SkpdModel, "kode_skpd") == dataset["skpd"]["kode_skpd"]
            )
            update_ = update_.update(
                {"count_dataset_public": count["count"]}, synchronize_session="fetch"
            )
            db.session.commit()

            # with private
            result, count = DATASET_CONTROLLER.get_count(
                {
                    "kode_skpd": dataset["skpd"]["kode_skpd"],
                    "is_deleted": False,
                    "is_active": True,
                    "is_validate": 3,
                },
                "",
            )
            update__ = db.session.query(SkpdModel)
            update__ = update__.filter(
                getattr(SkpdModel, "kode_skpd") == dataset["skpd"]["kode_skpd"]
            )
            update__ = update__.update(
                {"count_dataset": count["count"]}, synchronize_session="fetch"
            )
            db.session.commit()

        # insert to history
        if res:
            jwt = HELPER.read_jwt()
            json_history = {}
            json_history["type"] = "dataset"
            json_history["type_id"] = dataset["id"]
            json_history["user_id"] = jwt["id"]
            json_history["datetime"] = HELPER.local_date_server()
            json_history["category"] = "delete"
            # result, history_draft = HISTORY_CONTROLLER.create(json_history)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/all", methods=["GET"])
@jwt_check()
def get_alls():
    """Doc: function get alls"""
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", "id:DESC")
        limit = request.args.get("limit", 10000)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")

        # prepare data filter
        sort = sort.split(":")

        where = where.replace("'", '"')
        where = json.loads(where)

        res, dataset = DATASET_CONTROLLER.get_alls(where, search, sort, limit, skip)
        meta = {}

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS,
                "error": 0,
                "data": dataset,
                "meta": meta,
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND,
                "error": 1,
                "data": [],
                "meta": {},
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": [],
            "meta": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/opendata_recomendation/<_id>", methods=["GET"])
# @jwt_check()
def get_opendata_recomendation(_id):
    """Doc: function opendata recomendation"""
    try:
        res, dataset = DATASET_CONTROLLER.get_opendata_recomendation(_id)
        meta = {}

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS,
                "error": 0,
                "data": dataset,
                "meta": meta,
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND,
                "error": 0,
                "data": [],
                "meta": {},
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": [],
            "meta": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/generate_dataset_recomendation", methods=["GET"])
# @jwt_check()
def generate_dataset_recomendation():
    """Doc: function generate dataset recommendation"""
    try:
        res, dataset = DATASET_CONTROLLER.generate_dataset_recomendation()
        meta = {}

        # response
        if res:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS,
                "error": 0,
                "data": dataset,
                "meta": meta,
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND,
                "error": 0,
                "data": [],
                "meta": {},
            }
            return (
                Response(
                    json.dumps(response, cls=JSONEncoder),
                    mimetype=conf.MESSAGE_APP_JSON,
                ),
                200,
            )

    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": [],
            "meta": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/dataset/list", methods=["GET"])
def search():
    """Doc: function search"""
    try:
        # filter data
        where = request.args.get("where", None)
        sort = request.args.get("sort", None)
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")

        sort = ast.literal_eval(sort) if sort else ["id:asc"]
        where = ast.literal_eval(where) if where else []

        dataset, meta = DATASET_CONTROLLER.advance_search(
            where, sort, int(limit), int(skip), search
        )

        # response
        if dataset:
            # success response
            response = {
                "message": conf.MESSAGE_DATA_SUCCESS,
                "error": 0,
                "data": dataset,
                "meta": meta,
            }
        else:
            # success response but no data
            response = {
                "message": conf.MESSAGE_DATA_NOTFOUND,
                "error": 1,
                "data": [],
                "meta": {},
            }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder),
                mimetype=conf.MESSAGE_APP_JSON,
            ),
            200,
        )
    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": [],
            "meta": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON
            ),
            500,
        )


@BP.route("/data.json", methods=["GET"])
def get_dcat_schema():
    try:
        dataset = DATASET_CONTROLLER.get_dcat()
        return Response(
            json.dumps(dataset, cls=JSONEncoder),
            mimetype=conf.MESSAGE_APP_JSON,
        )
    except Exception as err:
        return (
            Response(
                json.dumps({"message": str(err), "error": 1, "data": [], "meta": {}}),
                mimetype=conf.MESSAGE_APP_JSON,
            ),
            500,
        )


@BP.route("/quality_score/<_id>", methods=["GET"])
def create_quality_score(_id):
    """Doc: function search"""
    try:
        # get dataset
        res, dataset, message = DATASET_CONTROLLER.get_by_id_table(_id, {})

        # get meta
        meta = {}
        res_count, counts = BIGDATA_CONTROLLER.get_count(
            dataset['schema'], dataset['table'], {}, {}, '')
        meta['total_record'] = int(counts['count'])

        # get bidata
        res, bigdata = BIGDATA_CONTROLLER.get_all(dataset['schema'], dataset['table'], {}, {}, '', ['id', 'asc'], meta['total_record'], 0, {})
        # model, metadata = BIGDATA_CONTROLLER.generate_metadata(dataset['schema'], dataset['table'])
        # metadata_filter = BIGDATA_CONTROLLER.get_metadata_filter(dataset['schema'], dataset['table'], metadata)

        # create quality score
        (res_score, quality_score, msg_score) = DATASET_QUALITY_SCORE_CONTROLLER.create_quality_score(dataset, bigdata, dataset['metadata'])

        response = {
            "message": conf.MESSAGE_DATA_SUCCESS,
            "error": 0,
            "data": {
                'res_score': res_score,
                'quality_score': quality_score,
                'msg_score': msg_score,
            },
            "meta": {},
        }
        return (
            Response(
                json.dumps(response, cls=JSONEncoder),
                mimetype=conf.MESSAGE_APP_JSON,
            ),
            200,
        )
    except Exception as err:
        # fail response
        response = {
            "message": conf.MESSAGE_INTERNAL_ERROR + str(err),
            "error": 1,
            "data": [],
            "meta": {},
        }
        return (Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500,)


@BP.route("/dataset/documentation", methods=["GET"])
def documentation():
    """Doc: function search"""
    return render_template("create-dataset.html" )

