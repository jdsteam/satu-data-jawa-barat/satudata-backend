''' Doc: router geojson '''
import json
import requests
from flask import Blueprint
from flask import request
from flask import Response
from flask import render_template
from helpers.jsonencoder import JSONEncoder
from controllers import GeojsonController
from controllers import BigdataController
from helpers.decorator import jwt_check_coredata

BP = Blueprint('geojson', __name__)
GEOJSON_CONTROLLER = GeojsonController()
BIGDATA_CONTROLLER = BigdataController()
# pylint: disable=line-too-long, unused-variable, unused-argument, singleton-comparison, broad-except

@BP.route("/geojson/doc", methods=["GET"])
def documentation():
    '''doc string'''

    default_config = {
        'app_name': 'Template',
        'dom_id': '#swagger-ui',
        'url': '/api-backend/static/doc/geojson.json',
        'layout': 'StandaloneLayout'
    }

    fields = {
        'base_url': '/api-backend/static',
        'app_name': default_config.pop('app_name'),
        'config_json': json.dumps(default_config),
    }

    return render_template('swagger.html', **fields)


@BP.route("/geojson", methods=["GET"])
@jwt_check_coredata()
def geojson():
    '''doc string'''

    try:
        # filter data
        schema = request.args.get("schema", "")
        table = request.args.get("table", "")
        filter_key = request.args.get("filter_key", "")
        filter_value = request.args.get("filter_value", "")
        group_kode = request.args.get("group_kode", "")
        group_value = request.args.get("group_value", "")

        if schema and table and filter_key and filter_value and group_kode and group_value:

            res1, res_count = BIGDATA_CONTROLLER.get_count(schema, table, {}, {}, '')
            res2, res_data = BIGDATA_CONTROLLER.get_all(schema, table, {}, {}, '', ['id', 'ASC'], res_count['count'], 0, [])
            res3, res_metadata = BIGDATA_CONTROLLER.generate_metadata(schema, table)

            datas = {}
            datas['info'], datas['range'] = GEOJSON_CONTROLLER.create_range(res_metadata, res_data, filter_key, filter_value, group_kode, group_value)

            if res_data:
                # success response
                response = {"message": "Get data successfull", "error": 0, "data": datas}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
            else:
                # success response but no data
                response = {"message": "Data not found", "error": 1, "data": []}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Parameter url, endpoint, filter_key, filter_value, group_kode and group_value  not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


@BP.route("/geojson/generate", methods=["POST"])
# @jwt_check_coredata()
def geojson_generate():
    '''doc string'''

    try:
        # prepare data
        json_request = request.get_json(silent=True)

        if json_request:

            datas = {}
            datas['info'], datas['range'] = GEOJSON_CONTROLLER.generate_range(json_request)

            if datas:
                # success response
                response = {"message": "Get data successfull", "error": 0, "data": datas}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
            else:
                # success response but no data
                response = {"message": "Data not found", "error": 1, "data": []}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Data not found", "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500

    except Exception as err:
        # fail response
        response = {"message": "Internal Server Error. " + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500


