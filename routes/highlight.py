''' Doc: route highlight'''
import json
import requests
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from helpers import Helper
from controllers import HighlightController
from controllers import DatasetController
from controllers import InfographicController
from controllers import VisualizationController
from controllers import ArticleController
from controllers import MapsetController
from settings import configuration as conf

BP = Blueprint('highlight', __name__)
HIGHLIGHT_CONTROLLER = HighlightController()
DATASET_CONTROLLER = DatasetController()
INFOGRAPHIC_CONTROLLER = InfographicController()
VISUALIZATION_CONTROLLER = VisualizationController()
ARTICLE_CONTROLLER = ArticleController()
MAPSET_CONTROLLER = MapsetController()
HELPER = Helper()
# pylint: disable=broad-except, line-too-long, unused-variable

@BP.route("/highlight", methods=["GET"])
# @jwt_check()
def get_all():
    ''' Doc: function get all'''
    try:
        # filter data
        where = request.args.get("where", "{}")
        sort = request.args.get("sort", 'id:DESC')
        limit = request.args.get("limit", 100)
        skip = request.args.get("skip", 0)
        search = request.args.get("search", "")
        count = request.args.get("count", False)

        # prepare data filter
        sort = sort.split(':')

        where = where.replace("'", "\"")
        where = json.loads(where)

        if count:
            # call function get
            res, highlight = HIGHLIGHT_CONTROLLER.get_count(where, search)
        else:
            # call function get
            res, highlight = HIGHLIGHT_CONTROLLER.get_all(where, search, sort, limit, skip)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight/<_id>", methods=["GET"])
# @jwt_check()
def get_by_id(_id):
    ''' Doc: function get by id'''
    try:
        res, highlight, message = HIGHLIGHT_CONTROLLER.get_by_id(_id)

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight/counter/<_id>", methods=["GET"])
# @jwt_check()
def counter(_id):
    ''' Doc: function counter'''
    try:
        res, highlight, message = HIGHLIGHT_CONTROLLER.get_by_id(_id)

        try:
            count_view = highlight['count_view']
            if not count_view:
                count_view = '0'
            count_view = int(count_view) + 1
            where = {'id': int(_id)}
            payload = {'count_view': int(count_view)}
            highlight['count_view'] = count_view
            res, data1 = HIGHLIGHT_CONTROLLER.update(where, payload)
        except Exception as err:
            pass

        # response
        if res:
            # success response
            response = {"message": message, "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": message, "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight", methods=["POST"])
@jwt_check()
def create():
    ''' Doc: function create'''
    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert highlight
        res, highlight = HIGHLIGHT_CONTROLLER.create(json_request)

        # response
        if res:
            # success response
            response = {"message": "Create data successfull", "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Create data failed ", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight/<_id>", methods=["PUT"])
@jwt_check()
def update(_id):
    ''' Doc: function update'''
    try:
        # filter data
        where = {'id': _id}

        # prepare json data
        json_request = request.get_json(silent=True)

        # update highlight
        res, highlight = HIGHLIGHT_CONTROLLER.update(where, json_request)

        # response
        if res:
            # success response
            response = {"message": "Update data successfull", "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Update data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight/<_id>", methods=["DELETE"])
@jwt_check()
def delete(_id):
    ''' Doc: function delete'''
    try:
        # filter data
        where = {'id': _id}

        # delete highlight
        res = HIGHLIGHT_CONTROLLER.delete(where)

        # response
        if res:
            # success response
            response = {"message": "Delete data successfull", "error": 0, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": "Delete data failed", "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight/public", methods=["GET"])
# @jwt_check()
def get_public():
    ''' Doc: function get public'''
    try:
        log_url = conf.LOG_URL
        highlight = []
        list_id_dataset = []
        list_id_visualization = []
        list_id_infographic = []
        list_id_article = []
        list_id_mapset = []
        i = 0

        # call function get
        res, highlight_all = HIGHLIGHT_CONTROLLER.get_all({'is_active': True, 'is_deleted': False}, "", ['mdate', 'desc'], 100, 0)
        res = True
        for ha_ in highlight_all:
            i = i + 1
            ha_['category_data']['id'] = i
            ha_['category_data']['category'] = ha_['category']
            ha_['category_data']['notes'] = 'from highlight'
            if 'telah-dihapus' not in ha_['category_data']['title']:
                highlight.append(ha_['category_data'])

                if ha_['category'] == 'dataset':
                    list_id_dataset.append(ha_['category_id'])
                elif ha_['category'] == 'visualisasi':
                    list_id_visualization.append(ha_['category_id'])
                if ha_['category'] == 'infografik':
                    list_id_infographic.append(ha_['category_id'])
                if ha_['category'] == 'artikel':
                    list_id_article.append(ha_['category_id'])
                if ha_['category'] == 'mapset':
                    list_id_mapset.append(ha_['category_id'])

        # call function get dataset highlight
        try:
            stat_dataset = False
            req_dataset = requests.get(
                log_url + 'opendata/dataset/monthly_top',
                headers={"Content-Type": "application/json"},
                data=json.dumps({})
            )
            res_dataset = req_dataset.json()
            if not res_dataset['error']:
                if res_dataset['data']:
                    for rd_ in res_dataset['data']:
                        temp_res, temp_data, temp_msg = DATASET_CONTROLLER.get_by_id(rd_['dataset_id'], {})
                        if temp_res:
                            # cek data data active, deleted, and not in highlight
                            if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['is_validate'] == 3 and temp_data['id'] not in list_id_dataset:
                                hd_ = {}
                                hd_['id'] = i + 1
                                hd_['category'] = 'dataset'
                                hd_['name'] = temp_data['name']
                                hd_['title'] = temp_data['title']
                                hd_['cdate'] = temp_data['cdate']
                                hd_['mdate'] = temp_data['mdate']
                                hd_['image'] = temp_data['picture_thumbnail']
                                hd_['notes'] = "count " + str(rd_['count'])
                                # if data already push to highlight
                                if not stat_dataset:
                                    highlight.append(hd_)
                                    stat_dataset = True
                                    i = i + 1
        except Exception as err:
            print(err)

        # call function get infografik highlight
        try:
            stat_infographic = False
            req_infographic = requests.get(
                log_url + 'opendata/infographic/monthly_top',
                headers={"Content-Type": "application/json"},
                data=json.dumps({})
            )
            res_infographic = req_infographic.json()
            if not res_infographic['error']:
                if res_infographic['data']:
                    for ri_ in res_infographic['data']:
                        temp_res, temp_data, temp_msg = INFOGRAPHIC_CONTROLLER.get_by_id(ri_['infographic_id'], {})
                        if temp_res:
                            # cek data data active, deleted, and not in highlight
                            if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['id'] not in list_id_infographic:
                                hi_ = {}
                                hi_['id'] = i + 1
                                hi_['category'] = 'infografik'
                                hi_['name'] = temp_data['name']
                                hi_['title'] = temp_data['title']
                                hi_['cdate'] = temp_data['cdate']
                                hi_['mdate'] = temp_data['mdate']
                                hi_['image'] = temp_data['image']
                                hi_['notes'] = "count " + str(ri_['count'])
                                # if data already push to highlight
                                if not stat_infographic:
                                    highlight.append(hi_)
                                    stat_infographic = True
                                    i = i + 1
        except Exception as err:
            print(err)

        # call function get visualisasi highlight
        try:
            stat_visualization = False
            req_visualization = requests.get(
                log_url + 'opendata/visualization/monthly_top',
                headers={"Content-Type": "application/json"},
                data=json.dumps({})
            )
            res_visualization = req_visualization.json()
            if not res_visualization['error']:
                if res_visualization['data']:
                    for rv_ in res_visualization['data']:
                        temp_res, temp_data, temp_msg = VISUALIZATION_CONTROLLER.get_by_id(rv_['visualization_id'], {})
                        if temp_res:
                            # cek data data active, deleted, and not in highlight
                            if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['id'] not in list_id_visualization:
                                hv_ = {}
                                hv_['id'] = i + 1
                                hv_['category'] = 'visualisasi'
                                hv_['name'] = temp_data['name']
                                hv_['title'] = temp_data['title']
                                hv_['cdate'] = temp_data['cdate']
                                hv_['mdate'] = temp_data['mdate']
                                hv_['image'] = temp_data['image']
                                hv_['notes'] = "count " + str(rv_['count'])
                                # if data already push to highlight
                                if not stat_visualization:
                                    highlight.append(hv_)
                                    stat_visualization = True
                                    i = i + 1

        except Exception as err:
            print(err)

        # call function get article highlight
        try:
            stat_article = False
            req_article = requests.get(
                log_url + 'opendata/article/monthly_top',
                headers={"Content-Type": "application/json"},
                data=json.dumps({})
            )
            res_article = req_article.json()
            if not res_article['error']:
                if res_article['data']:
                    for ra_ in res_article['data']:
                        temp_res, temp_data, temp_msg = ARTICLE_CONTROLLER.get_by_ids(ra_['article_id'])
                        if temp_res:
                            # cek data data active, deleted, and not in highlight
                            if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['id'] not in list_id_article:
                                ha_ = {}
                                ha_['id'] = i + 1
                                ha_['category'] = 'artikel'
                                ha_['name'] = temp_data['name']
                                ha_['title'] = temp_data['title']
                                ha_['cdate'] = temp_data['cdate']
                                ha_['mdate'] = temp_data['mdate']
                                ha_['image'] = temp_data['image']
                                ha_['notes'] = "count " + str(ra_['count'])
                                # if data already push to highlight
                                if not stat_article:
                                    highlight.append(ha_)
                                    stat_article = True
                                    i = i + 1
        except Exception as err:
            print(err)

        # call function get mapset highlight
        try:
            stat_mapset = False
            req_mapset = requests.get(
                log_url + 'satupeta/mapset/monthly_top',
                headers={"Content-Type": "application/json"},
                data=json.dumps({})
            )
            res_mapset = req_mapset.json()
            if not res_mapset['error']:
                if res_mapset['data']:
                    for ra_ in res_mapset['data']:
                        temp_res, temp_data = MAPSET_CONTROLLER.get_by_id(ra_['mapset_id'], {})
                        if temp_res:
                            # cek data data active, deleted, and not in highlight
                            if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['id'] not in list_id_mapset:
                                ha_ = {}
                                ha_['id'] = i + 1
                                ha_['category'] = 'mapset'
                                ha_['name'] = temp_data['name']
                                ha_['title'] = temp_data['title']
                                ha_['cdate'] = temp_data['cdate']
                                ha_['mdate'] = temp_data['mdate']
                                ha_['image'] = "static/upload/default-image.png"
                                ha_['notes'] = "count " + str(ra_['count'])
                                # if data already push to highlight
                                if not stat_mapset:
                                    highlight.append(ha_)
                                    stat_mapset = True
                                    i = i + 1
        except Exception as err:
            print(err)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/highlight/public_v2", methods=["GET"])
# @jwt_check()
def get_public_v2():
    ''' Doc: function get public'''
    try:
        log_url = conf.LOG_URL
        highlight = []
        i = 0

        # dataset
        list_dataset = []
        list_dataset_id = []
        list_dataset_left = 3
        res, list_dataset = HIGHLIGHT_CONTROLLER.get_all({'is_active': True, 'is_deleted': False, 'category': 'dataset'}, "", ['mdate', 'desc'], 3, 0)
        for list in list_dataset:
            i += 1
            list_dataset_left -= 1
            list_dataset_id.append(list['category_id'])
            highlight.append({
                "id": i,
                "name": list['category_data']['name'],
                "title": list['category_data']['title'],
                "image": list['category_data']['image'],
                "cdate": list['cdate'],
                "mdate": list['mdate'],
                "category": list['category'],
                "category_id": list['category_id'],
                "notes": "from highlight"
            })
        if list_dataset_left < 3:
            try:
                log_dataset = requests.get(log_url + 'opendata/dataset/monthly_top', headers={"Content-Type": "application/json"}, data=json.dumps({}))
                log_dataset = log_dataset.json()
                for log in log_dataset['data']:
                    if log['dataset_id'] not in list_dataset_id and list_dataset_left > 0:
                        temp_res, temp_data, temp_msg = DATASET_CONTROLLER.get_by_id(log['dataset_id'], {})
                        if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['is_validate'] == 3:
                            i += 1
                            list_dataset_left -= 1
                            highlight.append({
                                "id": i,
                                "name": temp_data['name'],
                                "title": temp_data['title'],
                                "image": temp_data['image'],
                                "cdate": temp_data['cdate'],
                                "mdate": temp_data['mdate'],
                                "category": 'dataset',
                                "category_id": temp_data['id'],
                                "notes": "count " + str(log['count'])
                            })
            except Exception as err:
                print(err)

        # infographic
        list_infographic = []
        list_infographic_id = []
        list_infographic_left = 3
        res, list_infographic = HIGHLIGHT_CONTROLLER.get_all({'is_active': True, 'is_deleted': False, 'category': 'infografik'}, "", ['mdate', 'desc'], 3, 0)
        for list in list_infographic:
            i += 1
            list_infographic_left -= 1
            list_infographic_id.append(list['category_id'])
            highlight.append({
                "id": i,
                "name": list['category_data']['name'],
                "title": list['category_data']['title'],
                "image": list['category_data']['image'],
                "cdate": list['cdate'],
                "mdate": list['mdate'],
                "category": list['category'],
                "category_id": list['category_id'],
                "notes": "from highlight"
            })
        if list_infographic_left < 3:
            try:
                log_infographic = requests.get(log_url + 'opendata/infographic/monthly_top', headers={"Content-Type": "application/json"}, data=json.dumps({}))
                log_infographic = log_infographic.json()
                for log in log_infographic['data']:
                    if log['infographic_id'] not in list_infographic_id and list_infographic_left > 0:
                        temp_res, temp_data, temp_msg = INFOGRAPHIC_CONTROLLER.get_by_id(log['infographic_id'], {})
                        if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['is_validate']:
                            i += 1
                            list_infographic_left -= 1
                            highlight.append({
                                "id": i,
                                "name": temp_data['name'],
                                "title": temp_data['title'],
                                "image": temp_data['image'],
                                "cdate": temp_data['cdate'],
                                "mdate": temp_data['mdate'],
                                "category": 'infografik',
                                "category_id": temp_data['id'],
                                "notes": "count " + str(log['count'])
                            })
            except Exception as err:
                print(err)

        # visualization
        list_visualization = []
        list_visualization_id = []
        list_visualization_left = 3
        res, list_visualization = HIGHLIGHT_CONTROLLER.get_all({'is_active': True, 'is_deleted': False, 'category': 'visualisasi'}, "", ['mdate', 'desc'], 3, 0)
        for list in list_visualization:
            i += 1
            list_visualization_left -= 1
            list_visualization_id.append(list['category_id'])
            highlight.append({
                "id": i,
                "name": list['category_data']['name'],
                "title": list['category_data']['title'],
                "image": list['category_data']['image'],
                "cdate": list['cdate'],
                "mdate": list['mdate'],
                "category": list['category'],
                "category_id": list['category_id'],
                "notes": "from highlight"
            })
        if list_visualization_left < 3:
            try:
                log_visualization = requests.get(log_url + 'opendata/visualization/monthly_top', headers={"Content-Type": "application/json"}, data=json.dumps({}))
                log_visualization = log_visualization.json()
                for log in log_visualization['data']:
                    if log['visualization_id'] not in list_visualization_id and list_visualization_left > 0:
                        temp_res, temp_data, temp_msg = VISUALIZATION_CONTROLLER.get_by_id(log['visualization_id'], {})
                        if temp_data['is_active'] and not temp_data['is_deleted'] and temp_data['is_validate']:
                            i += 1
                            list_visualization_left -= 1
                            highlight.append({
                                "id": i,
                                "name": temp_data['name'],
                                "title": temp_data['title'],
                                "image": temp_data['image'],
                                "cdate": temp_data['cdate'],
                                "mdate": temp_data['mdate'],
                                "category": 'visualisasi',
                                "category_id": temp_data['id'],
                                "notes": "count " + str(log['count'])
                            })
            except Exception as err:
                print(err)

        # article
        list_article = []
        list_article_id = []
        list_article_left = 3
        res, list_article = HIGHLIGHT_CONTROLLER.get_all({'is_active': True, 'is_deleted': False, 'category': 'artikel'}, "", ['mdate', 'desc'], 3, 0)
        for list in list_article:
            i += 1
            list_article_left -= 1
            list_article_id.append(list['category_id'])
            highlight.append({
                "id": i,
                "name": list['category_data']['name'],
                "title": list['category_data']['title'],
                "image": list['category_data']['image'],
                "cdate": list['cdate'],
                "mdate": list['mdate'],
                "category": list['category'],
                "category_id": list['category_id'],
                "notes": "from highlight"
            })
        if list_article_left < 3:
            try:
                log_article = requests.get(log_url + 'opendata/article/monthly_top', headers={"Content-Type": "application/json"}, data=json.dumps({}))
                log_article = log_article.json()
                for log in log_article['data']:
                    if log['article_id'] not in list_article_id and list_article_left > 0:
                        temp_res, temp_data, temp_msg = ARTICLE_CONTROLLER.get_by_ids(log['article_id'])
                        if temp_data['is_active'] and not temp_data['is_deleted']:
                            i += 1
                            list_article_left -= 1
                            highlight.append({
                                "id": i,
                                "name": temp_data['name'],
                                "title": temp_data['title'],
                                "image": temp_data['image'],
                                "cdate": temp_data['cdate'],
                                "mdate": temp_data['mdate'],
                                "category": 'artikel',
                                "category_id": temp_data['id'],
                                "notes": "count " + str(log['count'])
                            })
            except Exception as err:
                print(err)

        # mapset
        list_mapset = []
        list_mapset_id = []
        list_mapset_left = 3
        res, list_mapset = HIGHLIGHT_CONTROLLER.get_all({'is_active': True, 'is_deleted': False, 'category': 'mapset'}, "", ['mdate', 'desc'], 3, 0)
        for list in list_mapset:
            i += 1
            list_mapset_left -= 1
            list_mapset_id.append(list['category_id'])
            highlight.append({
                "id": i,
                "name": list['category_data']['name'],
                "title": list['category_data']['title'],
                "image": list['category_data']['image'],
                "cdate": list['cdate'],
                "mdate": list['mdate'],
                "category": list['category'],
                "category_id": list['category_id'],
                "notes": "from highlight"
            })
        if list_mapset_left < 3:
            try:
                log_mapset = requests.get(log_url + 'satupeta/mapset/monthly_top', headers={"Content-Type": "application/json"}, data=json.dumps({}))
                log_mapset = log_mapset.json()
                for log in log_mapset['data']:
                    if log['mapset_id'] not in list_mapset_id and list_mapset_left > 0:
                        temp_res, temp_data = MAPSET_CONTROLLER.get_by_id(log['mapset_id'], {})
                        if temp_data['is_active'] and not temp_data['is_deleted']:
                            i += 1
                            list_mapset_left -= 1
                            highlight.append({
                                "id": i,
                                "name": temp_data['name'],
                                "title": temp_data['title'],
                                "image": "static/upload/default-image.png",
                                "cdate": temp_data['cdate'],
                                "mdate": temp_data['mdate'],
                                "category": 'mapset',
                                "category_id": temp_data['id'],
                                "notes": "count " + str(log['count'])
                            })
            except Exception as err:
                print(err)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": highlight}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
