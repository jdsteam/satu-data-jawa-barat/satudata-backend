''' Doc: route dashboard'''
import json
from flask import Blueprint, request, Response
from helpers import Helper
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from controllers import DashboardRequestPublicController
from settings import configuration as conf
from datetime import datetime, timedelta

BP = Blueprint('dashboard/request_public', __name__)
DASHBOARD_FEEDBACK_PRIVATE = DashboardRequestPublicController()

HELPER = Helper()
one_month_ago_days = 30
one_days_ago = 1
today = datetime.today().strftime("%Y-%m-%d")
get_month_ago = datetime.today() - timedelta(days = one_month_ago_days)
get_day_ago = datetime.today() - timedelta(days = one_days_ago)
one_month_ago = get_month_ago.strftime("%Y-%m-%d")
one_day_ago = get_day_ago.strftime("%Y-%m-%d")
# pylint: disable=broad-except

@BP.route("/dashboard/request_public/total", methods=["GET"])
@jwt_check()
def get_total():
    ''' Doc: function get request_public total'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, total = DASHBOARD_FEEDBACK_PRIVATE.get_total(start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/request_public/tau_skpd", methods=["GET"])
@jwt_check()
def get_tau_skpd():
    ''' Doc: function get request_public tau skpd'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, csat = DASHBOARD_FEEDBACK_PRIVATE.get_tau_skpd(start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/request_public/skpd_tujuan", methods=["GET"])
@jwt_check()
def get_skpd_tujuan():
    ''' Doc: function get request_public skpd tujuan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, csat = DASHBOARD_FEEDBACK_PRIVATE.get_skpd_tujuan(start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/request_public/pekerjaan", methods=["GET"])
@jwt_check()
def get_pekerjaan():
    ''' Doc: function get request_public pekerjaan'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, csat = DASHBOARD_FEEDBACK_PRIVATE.get_pekerjaan(start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/request_public/tujuan_data", methods=["GET"])
@jwt_check()
def get_tujuan_data():
    ''' Doc: function get request_public tujuan_data'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        pekerjaan = request.args.get("pekerjaan", "")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, csat = DASHBOARD_FEEDBACK_PRIVATE.get_tujuan_data(start_date, end_date, types, pekerjaan)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500


@BP.route("/dashboard/request_public/bidang_ilmu_usaha", methods=["GET"])
@jwt_check()
def get_bidang_ilmu_usaha():
    ''' Doc: function get request_public bidang_ilmu_usaha'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        pekerjaan = request.args.get("pekerjaan", "")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, csat = DASHBOARD_FEEDBACK_PRIVATE.get_bidang_ilmu_usaha(start_date, end_date, types, pekerjaan)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": csat}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/request_public/trend", methods=["GET"])
@jwt_check()
def get_trend():
    ''' Doc: function get request_public trend'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        group = request.args.get("group", 'day')
        res, total = DASHBOARD_FEEDBACK_PRIVATE.get_trend(start_date, end_date, group)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500

@BP.route("/dashboard/request_public/keyword", methods=["GET"])
@jwt_check()
def get_keyword():
    ''' Doc: function get request_public keyword'''
    try:
        # filter data
        start_date = request.args.get("start_date", one_month_ago)
        end_date = request.args.get("end_date", today)
        types = request.args.get("type", "default")
        if start_date and end_date:
            types = 'date'
        else:
            types = 'default'
        res, total = DASHBOARD_FEEDBACK_PRIVATE.get_keyword(start_date, end_date, types)

        # response
        if res:
            # success response
            response = {"message": conf.MESSAGE_DATA_SUCCESS, "error": 0, "data": total}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200
        else:
            # success response but no data
            response = {"message": conf.MESSAGE_DATA_NOTFOUND, "error": 1, "data": []}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 200

    except Exception as err:
        # fail response
        response = {"message": conf.MESSAGE_INTERNAL_ERROR + str(err), "error": 1, "data": []}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype=conf.MESSAGE_APP_JSON), 500
